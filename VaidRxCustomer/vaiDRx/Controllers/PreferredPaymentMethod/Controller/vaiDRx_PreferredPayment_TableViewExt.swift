//
//  vaiDRx_PreferredPayment_TableViewExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 08/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_PreferredPaymentVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_PreferredPaymentTVCell.self), for: indexPath) as! vaiDRx_PreferredPaymentTVCell
        
        switch indexPath.row {
        case 0:
            if profileResponseModel != nil {
                if profileResponseModel?.preferredPayment == 0 {
                    
                    cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
                    cell.selectedCheckImage.image = UIImage(named: "uncheck_unselected")
                } else {
                    
                    cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
                    cell.selectedCheckImage.image = UIImage(named: "check_unselected")
                }
            } else {
                
                cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
                cell.selectedCheckImage.image = UIImage(named: "check_unselected")
            }
            cell.paymentMethodLabel.text = "Self Pay"
            break
        case 1:
            
            if profileResponseModel != nil {
                if (profileResponseModel?.preferredPayment == 1 || profileResponseModel?.preferredPayment == 2) {
                    
                    cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
                    cell.selectedCheckImage.image = UIImage(named: "uncheck_unselected")
                } else {
                    
                    cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
                    cell.selectedCheckImage.image = UIImage(named: "check_unselected")
                }
            } else {
                
                cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
                cell.selectedCheckImage.image = UIImage(named: "check_unselected")
            }
            cell.paymentMethodLabel.text = "Insurance"
            break
        default:
            
            if profileResponseModel != nil {
                if profileResponseModel?.preferredPayment == indexPath.row {
                    
                    cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
                    cell.selectedCheckImage.image = UIImage(named: "uncheck_unselected")
                } else {
                    
                    cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
                    cell.selectedCheckImage.image = UIImage(named: "check_unselected")
                }
            } else {
                
                cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
                cell.selectedCheckImage.image = UIImage(named: "check_unselected")
            }
            cell.paymentMethodLabel.text = "Insurance"
            break
        }
        return cell
    }
}

extension vaiDRx_PreferredPaymentVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadData()
        let selectedCell = tableView.cellForRow(at: indexPath) as! vaiDRx_PreferredPaymentTVCell
        selectedCell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
        selectedCell.selectedCheckImage.image = UIImage(named: "uncheck_unselected")
        
        var IndexPath = indexPath
        
        switch indexPath.row {
        case 0:
            UserDefaults.standard.set("Self Pay", forKey: USER_DEFAULTS.USER.PAYMENTTYPE)
            UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.PAYMENT_TYPE_CHANGED)
            IndexPath.row = 1
            let cell = tableView.cellForRow(at: IndexPath) as! vaiDRx_PreferredPaymentTVCell
            cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
            cell.selectedCheckImage.image = UIImage(named: "check_unselected")
            if isComing != "Profile" {
                registerViewModel.paymentType = PaymentType.SelfPay
                
                //performSegue(withIdentifier: "toAddCard", sender: self)
                performSegue(withIdentifier: "toYesOrNoSCreen", sender: self)
            } else {
                self.serviceRequestToUpdateProfile()
            }
            break
        case 1:
            IndexPath.row = 0
            
            let cell = tableView.cellForRow(at: IndexPath) as! vaiDRx_PreferredPaymentTVCell
            cell.paymentMethodLabel?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
            cell.selectedCheckImage.image = UIImage(named: "check_unselected")
            if isComing != "Profile"{
               registerViewModel.paymentType = PaymentType.Insurance
            }
            performSegue(withIdentifier: "toInsuranceVC", sender: self)
           // performSegue(withIdentifier: "toPreferredInsurance", sender: self)

            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
}

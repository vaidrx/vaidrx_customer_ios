//
//  AppoimtmentLocationModel.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class AppoimtmentLocationModel {
    
    static let sharedInstance = AppoimtmentLocationModel()
    
    var pickupLatitude = 0.0//Selected pick up latitude
    var pickupLongitude = 0.0//Selected pick up longitude
    var pickupAddress = ""//Selected pickup address
    
    var bookingType = BookingType.Default //Selected Booking Type
    var scheduleDate:Date! //Selected Later booking date
    var selectedEventStartTag = 0 //Selected Event Start Time tag
    var bookingRequested = false
}

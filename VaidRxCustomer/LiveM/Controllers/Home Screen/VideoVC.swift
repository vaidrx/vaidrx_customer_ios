//
//  VideoVC.swift
//  AgentWide
//
//  Created by Rohit sandhu on 8/3/21.
//

import UIKit
import JitsiMeetSDK


protocol disconnetCallDelegate {
    func BackToPreviousScreen()
}

class VideoVC: UIViewController,JitsiMeetViewDelegate {
    fileprivate var jitsiMeetView: JitsiMeetView?
    fileprivate var pipViewCoordinator: PiPViewCoordinator?
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var buttonStackview: UIStackView!
    var videoLink = ""
    var roomId = ""
    var friendId = ""
    var friendName = ""
    var callType = ""
    var delegate:disconnetCallDelegate?
    var alreadyHit = ""
    var disconnectCall = DisconnectCallViewModel()
    var apiTag:Int!
    var bookingId:Int64!
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden=true
        
        NotificationCenter.default.addObserver(self, selector: #selector(disconnectCallFunc), name: Notification.Name("disconnectCallFunc"), object: nil)
         setUPCAll()
    }
    
    @objc func disconnectCallFunc(){
        cleanUp()
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.popViewController(animated: false)
    }
   
    func setUPCAll(){
        
        
        let defaultOptions = JitsiMeetConferenceOptions.fromBuilder { (builder) in
            // for JaaS replace url with https://8x8.vc
            builder.serverURL = URL(string: "https://meet.jit.si")
            // for JaaS use the obtained Jitsi JWT
            // builder.token = "SampleJWT"
            builder.setFeatureFlag("welcomepage.enabled", withValue: false)
            // Set different feature flags
            //builder.setFeatureFlag("toolbox.enabled", withBoolean: false)
            //builder.setFeatureFlag("filmstrip.enabled", withBoolean: false)
        }
        
        JitsiMeet.sharedInstance().defaultConferenceOptions = defaultOptions
        
        
        if(roomId.count < 1) {
            return
        }
        
        // create and configure jitsimeet view
        let jitsiMeetView = JitsiMeetView()
        jitsiMeetView.delegate = self
        self.jitsiMeetView = jitsiMeetView
        let options = JitsiMeetConferenceOptions.fromBuilder { [self] (builder) in
            // for JaaS use <tenant>/<roomName> format
            builder.room = roomId
            // Settings for audio and video
            // builder.audioMuted = true;
            // builder.videoMuted = true;
        }
                
        // setup view controller
        let vc = UIViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.view = jitsiMeetView
        
        // join room and display jitsi-call
        jitsiMeetView.join(options)
        present(vc, animated: true, completion: nil)
        
   }
    
    fileprivate func cleanUp() {
        if(jitsiMeetView != nil) {
            dismiss(animated: true, completion: nil)
            jitsiMeetView = nil
        }
    }
    
    
    func disconnectBookingCall() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {

            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        print(roomId)
        disconnectCall.disconnectBookingCallAPICall(bookingid: bookingId) { (statCode, errMsg, dataResp) in

            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.disconnectBookingCall)
        }
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode {
            
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if let dataRes = dataResponse as? String {
                
                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
                self.apiTag = requestType.rawValue
                
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
                case .liveBooking:
                    
                    progressMessage = PROGRESS_MESSAGE.ConfirmBooking
                    
                default:
                    
                    break
                }
                
                self.acessClass.getAcessToken(progressMessage: progressMessage)
                
            }
            
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
            }
            break
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType {
                
            case RequestType.disconnectBookingCall:
                self.delegate?.BackToPreviousScreen()
                self.navigationController?.navigationBar.isHidden=false
                self.navigationController?.popViewController(animated: false)
            default:
                break
            }
            break
        default:
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
            }
            break
        }
        
    }
    
    
    func conferenceTerminated(_ data: [AnyHashable : Any]!) {
        cleanUp()
        disconnectBookingCall()
       
     }
}

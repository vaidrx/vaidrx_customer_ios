//
//  vaiDRx_Home_BookingIDCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class vaiDRx_Home_BookingIDCell: UITableViewCell {

    @IBOutlet weak var bookingId: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  HomeListVCMQTTDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension HomeListViewController:MusiciansListManagerDelegate {
    
    func emptyMusicianList(errMessage:String) {
        
        if arrayOfProvidersModel.count > 0 {
        
            arrayOfProvidersModel = []
            arrayOfMusicians = []
            updateScrollViewConstraint()
        
            //Reload TableView
            self.tableView.reloadData()
        } else {
            
            self.messageLabel.text = musiciansListManager.errMessageString
        }
        
    }
    
    func showInitialMusicianList(arrayOfUpdatedMusiciansModel: [MusicianDetailsModel],arrayOfMusiciansList:[Any]) {
        
        // Load First time
        arrayOfProvidersModel = arrayOfUpdatedMusiciansModel
        arrayOfMusicians = arrayOfMusiciansList
        
        updateScrollViewConstraint()
        
        //Reload TableView
        tableView.reloadData()
        
    }
    
    func updateMusicianList(arrayOfUpdatedMusicians: [MusicianDetailsModel], arrayOfRowsToAdd: [Int], arrayOfRowsToRemove: [Int], arrayOfMusicianIdToRemove:[String], arrayOfMusiciansList:[Any]) {
        
        if arrayOfProvidersModel.isEmpty {
            
            showInitialMusicianList(arrayOfUpdatedMusiciansModel: arrayOfUpdatedMusicians, arrayOfMusiciansList: arrayOfMusiciansList)
            
        } else {
    
            arrayOfMusicians = arrayOfMusiciansList
//            updateScrollViewConstraint()
            
//            var arrayOfRemoveIndexPath:[IndexPath] = []
//            var arrayOfAddIndexPath:[IndexPath] = []
            
            //for i in 0..<arrayOfRowsToRemove.count {
            
            for i in 0..<arrayOfMusicianIdToRemove.count {
//                arrayOfRemoveIndexPath.append(IndexPath.init(row: arrayOfRowsToRemove[i], section: 0))
                
                if #available(iOS 11.0, *) {
                    
                } else {
                    
                    self.tableView.beginUpdates()
                }
                
                if let index = arrayOfProvidersModel.index(where: {$0.providerId == arrayOfMusicianIdToRemove[i]}) {
                    
                    arrayOfProvidersModel.remove(at: index)
                    self.removeRowFromTableView(indexPath: IndexPath.init(row: index, section: 0))
                }
                

                
//                if let musicianCell = self.tableView.cellForRow(at: IndexPath.init(row: arrayOfRowsToRemove[i], section: 0)) as? HomeListTableViewCell {
//
//                    musicianCell.youtubePlayerView.pauseVideo()
//                }
                
            }
            
//            if arrayOfRemoveIndexPath.count > 0 {
//
//                if #available(iOS 11.0, *) {
//
//                    self.tableView.performBatchUpdates({
//                        self.tableView.deleteRows(at: arrayOfRemoveIndexPath, with: UITableViewRowAnimation.none)
//                    }, completion:nil)
//
//                } else {
//
//
//                    // Fallback on earlier versions
//                    self.tableView.deleteRows(at: arrayOfRemoveIndexPath, with: UITableViewRowAnimation.none)
//
//
//                }
//
//
//            }
            

//            updateScrollViewConstraint()
            
            let sortedArrayOfRowsToAdd = arrayOfRowsToAdd.sorted { $0 < $1  }
            
//            for i in 0..<arrayOfRowsToAdd.count {
//
//                arrayOfAddIndexPath.append(IndexPath.init(row: arrayOfRowsToAdd[i], section: 0))
//            }
//
//            if arrayOfAddIndexPath.count > 0 {
//
//                self.tableView.insertRows(at: arrayOfAddIndexPath, with: UITableViewRowAnimation.automatic)
//            }
            
            for i in 0..<sortedArrayOfRowsToAdd.count {
                
                if #available(iOS 11.0, *) {
                    
                } else {
                    
                    self.tableView.beginUpdates()
                }
                
                
                arrayOfProvidersModel.insert(arrayOfUpdatedMusicians[sortedArrayOfRowsToAdd[i]], at: sortedArrayOfRowsToAdd[i])
                self.insertRowFromTableView(indexPath: IndexPath.init(row: sortedArrayOfRowsToAdd[i], section: 0))
                
            }
            
            arrayOfProvidersModel = arrayOfUpdatedMusicians
            
            updateScrollViewConstraint()
            

            let when = DispatchTime.now() + 0.4
            DispatchQueue.main.asyncAfter(deadline: when){
                
                self.updateProviderTableViewData()
                self.updateScrollViewConstraint()
            }
        }
        
    }
    
    func removeRowFromTableView(indexPath:IndexPath) {
        
        if let musicianCell = self.tableView.cellForRow(at: indexPath) as? HomeListTableViewCell {
            
//            musicianCell.youtubePlayerView.pauseVideo()
            
            if #available(iOS 11.0, *) {
                
                self.tableView.performBatchUpdates {
                    self.tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.none)
                } completion: { (res) in
                    
                }
             } else {
                
                // Fallback on earlier versions
                self.tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.none)
                self.tableView.endUpdates()
            }
        }
        
    }
    
    func insertRowFromTableView(indexPath:IndexPath) {
        
        if #available(iOS 11.0, *) {
            
            self.tableView.performBatchUpdates {
                self.tableView.insertRows(at: [indexPath], with: UITableView.RowAnimation.none)
            } completion: { (res) in
                
            }
         } else {
            
            // Fallback on earlier versions
            self.tableView.insertRows(at: [indexPath], with: UITableView.RowAnimation.none)
            self.tableView.endUpdates()
        }
        
    }
    
    
    func updateProviderTableViewData() {
        
//        let arrayOfVisibleRows = self.tableView.indexPathsForVisibleRows
        
//        for indexPath in arrayOfVisibleRows! {
        
        for i in 0..<arrayOfProvidersModel.count {
            
            if let musicianCell = self.tableView.cellForRow(at: IndexPath.init(row: i, section: 0)) as? HomeListTableViewCell {
                
                let musicianDetails = arrayOfProvidersModel[i]
                
                musicianCell.nameLabel.text = musicianDetails.firstName.capitalized + " " + musicianDetails.lastName.capitalized
                musicianCell.distanceLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: musicianDetails.distance, mileageMatric: musicianDetails.mileageMatric)
                
                if musicianDetails.status == 0 {
                    
                    musicianCell.statusImageView.image = #imageLiteral(resourceName: "offline_big")
                    
                } else {
                    
                    musicianCell.statusImageView.image = #imageLiteral(resourceName: "online_big")
                }
                
                musicianCell.ratingView.rating = Float(musicianDetails.overallRating)
                
                musicianCell.priceLabel.text = String(format:"%@ %.2f", musicianDetails.currencySymbol, musicianDetails.amount)
                
                
//                if musicianCell.youtubePlayerView.tag != i {
//
//                    musicianCell.youtubePlayerView.tag = i
//                }
                
                let youtubeVideoId = Helper.extractYoutubeIdFromLink(link:musicianDetails.youtubeVideoLink)
                
                //!(youtubeVideoId?.isEmpty)! &&
                if musicianCell.youtubeVideoURL != youtubeVideoId! {
                    
//                    musicianCell.youtubePlayerView.pauseVideo()
                    
                    self.tableView.reloadRows(at: [IndexPath.init(row: i, section: 0)], with: UITableView.RowAnimation.none)
                    
                    /*let dict = [
                        "playsinline" : 1,
                        "showinfo" : 0,
                        "controls" : 1,
                        "modestbranding": 0,
                        "enablejsapi":1,
                        "autohide":2,
                        "rel":0,
                        "origin" : "http://www.youtube.com",
                        ] as [String : Any]
                    
                    musicianCell.defaultImageView.isHidden = false
                    musicianCell.activityIndicator.startAnimating()
                    musicianCell.youtubePlayerView.isHidden = true
                    
                    musicianCell.youtubeVideoURL = youtubeVideoId!
                    
                    let result = musicianCell.youtubePlayerView.load(withVideoId: youtubeVideoId!, playerVars: dict)
                    
                    musicianCell.youtubePlayerView.webView?.allowsInlineMediaPlayback = true
                    
                    print("Result = \(result)")*/
                    
                    
                }
                
            }
            
        }
    }
    
    
}

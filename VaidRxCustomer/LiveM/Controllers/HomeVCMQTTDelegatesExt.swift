//
//  HomeMQTTDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps

extension HomeScreenViewController:MusiciansListManagerDelegate {
    
    func emptyMusicianList(errMessage:String) {
        
        if arrayOfProvidersModel.count > 0 {
            
            // Clear the Map
            mapView.clear()
            dictOfMarkers = [:]
            
            self.selectedProviderTag = 0
            self.prevSelectedMarkerTag = 0
            self.selectedProviderId = ""
            
            arrayOfProvidersModel = []
            arrayOfMusicians = []
            
            //Reload CollectionView
            reloadCollectionView()
            
        } else {
            
//            if noArtistMessageToShow {
//
//                noArtistMessageToShow = false
            
                if SplashLoading.obj == nil {
                    
                    self.showNoArtistMessage(message: errMessage)
                }
                
//            }
        }
        
    }
    
    func showInitialMusicianList(arrayOfUpdatedMusiciansModel: [MusicianDetailsModel],arrayOfMusiciansList:[Any]) {
        
        // Load First time
        arrayOfProvidersModel = arrayOfUpdatedMusiciansModel
        arrayOfMusicians = arrayOfMusiciansList
        
        // Clear the Map
        mapView.clear()
        dictOfMarkers = [:]
        
        self.selectedProviderTag = 0
        self.prevSelectedMarkerTag = 0
        self.selectedProviderId = ""
        
        
        showMarkersInMapView()
        
        //Reload CollectionView
        reloadCollectionView()
        
        if arrayOfUpdatedMusiciansModel.count > 0 && appointmentLocationModel.pickupLatitude != 0.0 {
            
            self.perform(#selector(self.closeSplashLoading), with: nil, afterDelay: 4)
        }
        
        setInitialCollectionViewscrollPosition()
    }
    
    func updateMusicianList(arrayOfUpdatedMusicians: [MusicianDetailsModel], arrayOfRowsToAdd: [Int], arrayOfRowsToRemove: [Int], arrayOfMusicianIdToRemove:[String], arrayOfMusiciansList:[Any]) {
        
        if arrayOfProvidersModel.isEmpty {
            
            showInitialMusicianList(arrayOfUpdatedMusiciansModel: arrayOfUpdatedMusicians, arrayOfMusiciansList: arrayOfMusiciansList)
            
        } else {
            
            if arrayOfUpdatedMusicians.count > 0 {
                
                self.perform(#selector(self.closeSplashLoading), with: nil, afterDelay: 4)
            }
            
            arrayOfProvidersModel = arrayOfUpdatedMusicians
            arrayOfMusicians = arrayOfMusiciansList
            
            for i in 0..<arrayOfRowsToRemove.count {
                
                if let providerView = self.collectionView.itemView(at: arrayOfRowsToRemove[i]) as? ProviderCollectionView {
                    
//                    providerView.youtubePlayerView.pauseVideo()
                }
                
                self.collectionView.removeItem(at: arrayOfRowsToRemove[i], animated: false)
                
                
                if dictOfMarkers.index(forKey: arrayOfMusicianIdToRemove[i]) != nil {
                    
                    let providerMarker:GMSMarker = dictOfMarkers[arrayOfMusicianIdToRemove[i]] as! GMSMarker
                    removeMarker(marker: providerMarker)
                    dictOfMarkers.removeValue(forKey: arrayOfMusicianIdToRemove[i])
                    
                }
                
            }
            
            for i in 0..<arrayOfRowsToAdd.count {
                
                self.collectionView.insertItem(at: arrayOfRowsToAdd[i], animated: false)
            }
            
            if arrayOfProvidersModel.count != self.collectionView.numberOfItems {
                
                DDLogDebug("\(self.collectionView.numberOfItems)")
            }
            
            let when = DispatchTime.now() + 0.4
            DispatchQueue.main.asyncAfter(deadline: when){
                
                self.showMarkersInMapView()
                self.updateProviderCollectionViewData()
                
                if self.selectedProviderTag >= self.arrayOfProvidersModel.count {
                    
                    self.selectedProviderTag = self.arrayOfProvidersModel.count - 1
                    self.prevSelectedMarkerTag = self.arrayOfProvidersModel.count - 1
                    
                    if let providerView = self.collectionView.itemView(at: self.selectedProviderTag) as? ProviderCollectionView {
                        
                        if providerView.youtubeVideoURL.length > 0 {
                        
                            providerView.nameLabel.textColor = UIColor.white
                            providerView.milesLabel.textColor = UIColor.white
                            
                        } else {

                            providerView.nameLabel.textColor = UIColor.lightGray
                            providerView.milesLabel.textColor = UIColor.lightGray
                        }
                        
                        
                        self.selectedView = providerView
                        
                        self.selectedProviderId = self.selectedView.musicianId
                        
                        self.collectionView.scrollToItem(at: self.selectedProviderTag, animated: true)
                        
                        self.selectMarkerInMapView(markerTag: self.selectedProviderTag, prevMarkerTag:self.prevSelectedMarkerTag)
                        
                        self.showCurrentMusicianCollectionViewAnimation()
                        
                    }
                    
                } else {
                    
                    if self.arrayOfProvidersModel[self.selectedProviderTag].providerId != self.selectedProviderId {
                        
                        if let providerView = self.collectionView.itemView(at: self.selectedProviderTag) as? ProviderCollectionView {
                            
                            if providerView.youtubeVideoURL.length > 0 {
                                
                                providerView.nameLabel.textColor = UIColor.white
                                providerView.milesLabel.textColor = UIColor.white
                                
                            } else {
                                
                                providerView.nameLabel.textColor = UIColor.lightGray
                                providerView.milesLabel.textColor = UIColor.lightGray
                            }
                            
                            self.selectedView = providerView
                            
                            self.selectedProviderId = self.selectedView.musicianId
                            
                            self.collectionView.scrollToItem(at: self.selectedProviderTag, animated: true)
                            
                            self.selectMarkerInMapView(markerTag: self.selectedProviderTag, prevMarkerTag:self.prevSelectedMarkerTag)
                            
                            self.showCurrentMusicianCollectionViewAnimation()
                            
                        }
                        
                    }
                }

            }

            
        }
        
        
    }
    
    
    func updateProviderCollectionViewData() {
        
        for i in 0..<arrayOfProvidersModel.count {
            
            if let providerView = self.collectionView.itemView(at: i) as? ProviderCollectionView {
                
                let musicianDetails = arrayOfProvidersModel[i]
                
                providerView.nameLabel.text = musicianDetails.firstName.capitalized + " " + musicianDetails.lastName.capitalized
                
                providerView.milesLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: musicianDetails.distance, mileageMatric: musicianDetails.mileageMatric)
                
//                if providerView.youtubePlayerView.tag != i {
//                    
//                    providerView.youtubePlayerView.tag = i
//                }
                
                if providerView.musicianId != musicianDetails.providerId {
                    
                    providerView.musicianId = musicianDetails.providerId
                }
                
                let youtubeVideoId = Helper.extractYoutubeIdFromLink(link:musicianDetails.youtubeVideoLink)
                
                //!(youtubeVideoId?.isEmpty)! &&
                if providerView.youtubeVideoURL != youtubeVideoId! {
                    
//                    providerView.youtubePlayerView.pauseVideo()
                    
                    self.collectionView.reloadItem(at: i, animated: false)
                    
                    /*let dict = [
                        "playsinline" : 1,
                        "showinfo" : 0,
                        "controls" : 1,
                        "modestbranding": 0,
                        "enablejsapi":1,
                        "autohide":2,
                        "rel":0,
                        "origin" : "http://www.youtube.com",
                        ] as [String : Any]
                    
                    providerView.defaultImageView.isHidden = false
                    providerView.actvityIndicator.startAnimating()
                    providerView.youtubePlayerView.isHidden = true
                    providerView.blurrView.isHidden = true

                    
                    providerView.youtubeVideoURL = youtubeVideoId!
                    let result = providerView.youtubePlayerView.load(withVideoId: youtubeVideoId!, playerVars: dict)
                    
                    providerView.youtubePlayerView.webView?.allowsInlineMediaPlayback = true
                    
                    print("Result = \(result)")*/
                                        
                }
                
            }
            
        }
    }
    
    
    func reloadCollectionView() {
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            self.collectionView.reloadData()
            
        })
    }
    
    func setInitialCollectionViewscrollPosition() {
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            if self.arrayOfProvidersModel.count > 0 {
                
                if let firstCellView = self.collectionView.itemView(at: 0) {
                    
                    DDLogDebug("FirstCell = \(firstCellView)")
                    
                    let musicianDetail = self.arrayOfProvidersModel[0]
                    
                    CATransaction.begin()
                    CATransaction.setAnimationDuration(2.0)
                    
                    let coordinates = CLLocationCoordinate2DMake(CLLocationDegrees(musicianDetail.latitude), CLLocationDegrees(musicianDetail.longitude))
                    
                    let point = self.mapView.projection.point(for: coordinates)
                    
                    let updatedCamera =  GMSCameraUpdate.setTarget(self.mapView.projection.coordinate(for: point), zoom:MAP_ZOOM_LEVEL)
                    
                    
//                    self.mapView.animate(with: updatedCamera)
                    
                    CATransaction.commit()
                    
                    self.isFocusedMarkerPicked = true
                    
                }
                
                
            }
        })
        
    }
    
}

//
//  InvoiceViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

class InvoiceViewController: UIViewController {
    
    @IBOutlet var navigationTopView: UIView!
    
    @IBOutlet var eventDateLabel: UILabel!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var scrollContentView: UIView!
    
    @IBOutlet var amountLabel: UILabel!
    
    @IBOutlet var detailsButton: UIButton!
    
    @IBOutlet var musicianImageView: UIImageView!
    
    @IBOutlet var musicianNameLabel: UILabel!
    
    @IBOutlet var ratingView: FloatRatingView!
    
    @IBOutlet var reviewTextView: UITextView!
    
    @IBOutlet var submitButtonBackView: UIView!
    
    @IBOutlet var submitButton: UIButtonCustom!
    
    
    @IBOutlet var submitButtonBackViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    
    
    var bookingDetailModel:BookingDetailsModel!
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!
    
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    
    var bookingId:Int64!
    
    var invoiceDetailsScreen:InvoiceDetailsPopUpScreen!
    let invoiceViewModel = InvoiceViewModel()
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getBookingDetailsAPI()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        appDelegate?.keyboardDelegate = self
        acessClass.acessDelegate = self
        
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
        appDelegate?.keyboardDelegate = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        updateScrollViewContent()
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateScrollViewContent() {
        
        if (reviewTextView.frame.size.height + reviewTextView.frame.origin.y) + 30 > scrollView.frame.size.height {
            
            scrollContentViewHeightConstraint.constant = (reviewTextView.frame.size.height + reviewTextView.frame.origin.y) + 30 - scrollView.frame.size.height
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0
        }

    }
    
    
    //MARK: - UIButton Actions -
    
    @IBAction func detailsButtonAction(_ sender: Any) {
        
        invoiceDetailsScreen = InvoiceDetailsPopUpScreen.sharedInstance
        
        invoiceDetailsScreen.bookingDetailModel = bookingDetailModel
        
        WINDOW_DELEGATE??.addSubview(invoiceDetailsScreen)
        
        invoiceDetailsScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.2,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.invoiceDetailsScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { (finished) in
            
            self.invoiceDetailsScreen.showInvoiceDetails()
            
        }

    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        
        submitReviewAPI()
    }
    
}

extension InvoiceViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getParticularApptDetails.rawValue:
            
            getBookingDetailsAPI()
            
            break
            
        case RequestType.reviewAndRating.rawValue:
            
            submitReviewAPI()
            
            break
            
        default:
            break
        }
    }
    
}

extension InvoiceViewController:UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        textView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
        
        if textView.text! == ALERTS.invoiceReviewPlaceholder {
            
            textView.text = ""
        }
        
        return true
        
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        self.scrollView.scrollRectToVisible(CGRect.init(x: self.scrollView.contentSize.width - 1, y: self.scrollView.contentSize.height - 1, width: 1, height: 1), animated: true)
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            
            textView.resignFirstResponder()
            return false
        }
        
        adjustUITextViewHeight(textView: textView, range:range)
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ALERTS.invoiceReviewPlaceholder || textView.text.length == 0 {
            
            textView.text = ALERTS.invoiceReviewPlaceholder
            reviewTextView.textColor = UIColor.lightGray
            
        } else {
            
            reviewTextView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
        }
        
    }
    
    /// Adjust TextView Height
    ///
    /// - Parameter arg: textView
    func adjustUITextViewHeight(textView : UITextView, range:NSRange)
    {
        let fixedWidth = textView.frame.size.width
        
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        
        var newFrame = textView.frame
        
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        if range.length == 0 {//New Character
            
            if newFrame.size.height > textView.frame.size.height {
                
                textView.frame = newFrame
            }
            
        } else {//Back Space
            
            if newFrame.size.height >= 70.0 {
                
                textView.frame = newFrame
                
            } else {
                
                newFrame.size.height = 70.0
                textView.frame = newFrame
            }

            
        }
        
        updateScrollViewContent()
        
        
        self.scrollView.scrollRectToVisible(CGRect.init(x: self.scrollView.contentSize.width - 1, y: self.scrollView.contentSize.height - 1, width: 1, height: 1), animated: true)
        
    }

}


// MARK: - Keyboard Methods -

extension InvoiceViewController:KeyboardDelegate {

    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        let inputViewFrame: CGRect? = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        
        let inputViewFrameInView: CGRect = view.convert(inputViewFrame!, from: nil)
        
        let intersection: CGRect = scrollView.frame.intersection(inputViewFrameInView)
        let ei: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: intersection.size.height, right: 0.0)
        scrollView.scrollIndicatorInsets = ei
        scrollView.contentInset = ei
        
    }


    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
        }
        
    }

}


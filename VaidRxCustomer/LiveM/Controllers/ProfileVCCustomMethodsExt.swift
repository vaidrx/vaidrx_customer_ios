//
//  ProfileVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension ProfileViewController {
    
    // MARK: - Pharmacy selection delegate Method
    func didSelectPharmacy(selectedData: PharmacyItems) {
        self.pharmacyTextField.text = selectedData.pharmacyName
    }
    
    /// Initiall Setup for the controller
    func initiallSetupForAnimation() {
        
        //dobPickerBottomConstraint.constant = -500
        edditingEnabled(false)
        self.view.layoutIfNeeded()
        firstNameBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        lastNameBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        paymentMethodBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        addressBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        musicGenreBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        
//        aboutMeBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
//        musicGenreBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        emailBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        phoneNumBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        dobBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        dobImageHeight.constant = 0
//        aboutMeCountHeight.constant = 0
//        musicGenreButtonHeight.constant = 0
        countryPickerViewHeight.constant = 90
        imageButtonHeight.constant = 0
        changePasswordBGViewHeight.constant = 100
        
        changeButton.transform = CGAffineTransform(translationX: 0, y: changeButton.frame.origin.y + 100)
        logOutButton.transform = CGAffineTransform(translationX: 0, y: logOutButton.frame.origin.y + 140)
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
        
    }
    
    /// method get called when edit button called
    func edditingEnabled(_ value: Bool){
        
        nameTextField.isUserInteractionEnabled = value
        lastNameTextField.isUserInteractionEnabled = value
        musicGenreTextField.isUserInteractionEnabled = value
        emailTextField.isUserInteractionEnabled = false
        mobileNumberTextField.isUserInteractionEnabled = false
//        aboutMeTextView.isUserInteractionEnabled = value
        imageButton.isEnabled = value
        dobButton.isEnabled = value
        musicGenreButton.isEnabled = value
        changeEmailButton.isEnabled = value
        changePhoneNumButton.isEnabled = value
        
        countryPickerButton.isEnabled = false
        imageButtonOutlet.isEnabled = value
        addressButton.isEnabled = value
        billingAddressTextField.isUserInteractionEnabled = false
        pharmacyButton.isEnabled = value
        pharmacyTextField.isUserInteractionEnabled = false
    }
    
    func editButtonAnimation() {
        
        if firstNameBottomView.transform != .identity {
            
            edditingEnabled(true)
            self.view.layoutIfNeeded()
            dobImageHeight.constant = 25
//            aboutMeCountHeight.constant = 15
//            musicGenreButtonHeight.constant = 25
            countryPickerViewHeight.constant = 90
            imageButtonHeight.constant = 20
            changePasswordBGViewHeight.constant = 0
            UIView.animate(withDuration: 0.6, animations: {
                self.view.layoutIfNeeded()
                self.firstNameBottomView.transform = .identity
                self.lastNameBottomView.transform = .identity
                //self.aboutMeBottomView.transform = .identity
                //self.musicGenreBottomView.transform = .identity
                self.emailBottomView.transform = .identity
                self.phoneNumBottomView.transform = .identity
                self.dobBottomView.transform = .identity
                
                self.paymentMethodBottomView.transform = .identity
                self.addressBottomView.transform = .identity
                self.musicGenreBottomView.transform = .identity
                
                self.changeButton.transform = CGAffineTransform(translationX: 0, y: self.changeButton.frame.origin.y + 100)
                self.logOutButton.transform = CGAffineTransform(translationX: 0, y: self.logOutButton.frame.origin.y + 140)
                
            })
            
            self.view.layoutIfNeeded()
            
            if (dobLabel.frame.size.height + dobLabel.frame.origin.y) + 25 > scrollView.frame.size.height {
                
                scrollContentViewHeightConstraint.constant = (dobLabel.frame.size.height + dobLabel.frame.origin.y) + 25 - scrollView.frame.size.height;
            }
            else
            {
                self.scrollContentViewHeightConstraint.constant = 0
            }
            scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
            scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
            
            self.view.layoutIfNeeded()
        }
    }
    
    func animations() {
        UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
            self.changeButton.transform = .identity
            UIView.animate(withDuration: 1, delay: 0.3, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
                self.logOutButton.transform = .identity
            })
        })
    }
    
    func updateUserDefault()  {
        
        DispatchQueue.main.async { [self] in
            AppDelegate().defaults.set(GenericUtility.strForObj(object: nameTextField.text), forKey: USER_DEFAULTS.USER.FIRSTNAME)
            AppDelegate().defaults.set(GenericUtility.strForObj(object: lastNameTextField.text), forKey: USER_DEFAULTS.USER.LASTNAME)
            AppDelegate().defaults.set(GenericUtility.strForObj(object: self.profilePicURL), forKey: USER_DEFAULTS.USER.PIC)
            AppDelegate().defaults.set(GenericUtility.strForObj(object: emailTextField.text), forKey: USER_DEFAULTS.USER.EMAIL)
            AppDelegate().defaults.set(GenericUtility.strForObj(object: musicGenreTextField.text), forKey: USER_DEFAULTS.USER.PAYMENT_TYPE)
            AppDelegate().defaults.synchronize()
            
            LeftMenuTableViewController.sharedInstance().showUserDetails()
        }
        
        
        
    }
    
    /// alert to chose way to upload photo
    func alertView() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: ALERTS.SelectImage,
                                                                             message: ALERTS.SelectImageOption,
                                                                             preferredStyle: .actionSheet)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()

        
        let cancelActionButton = UIAlertAction(title: ALERTS.Cancel, style: .cancel) { _ in
            DDLogDebug("\(ALERTS.Cancel)")
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
        }
        
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: ALERTS.Camera, style: .default)
        { _ in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            self.cameraButtonAction()
        }
        
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title:  ALERTS.Gallery, style: .default)
        { _ in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            self.libraryAction()
        }
        
        actionSheetControllerIOS8.addAction(deleteActionButton)
        
        if self.profilePhoto.image != PROFILE_DEFAULT_IMAGE {
            let deletePhotoButton = UIAlertAction(title: ALERTS.RemoveImage, style: .default)
            { _ in
                
                newAlertWindow.resignKey()
                newAlertWindow.removeFromSuperview()
                self.profilePhoto.image = PROFILE_DEFAULT_IMAGE
                self.profileAlternatePhoto = 2
            }
            
            actionSheetControllerIOS8.addAction(deletePhotoButton)
        }
        newAlertWindow.rootViewController?.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    /// camera button Method
    func cameraButtonAction() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker,animated: true,completion: nil)
            
         } else {
            
            noCamera()
        }
    }
    
    /// Library button Method
    func libraryAction() {
        
        if  UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
                imagePicker.navigationBar.isTranslucent = false
                // Background color
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func noCamera(){
        
        Helper.alertVC(title: ALERTS.NoCamera, message: ALERTS.CameraMissing)

//        let alertVC = UIAlertController(
//            title: ALERTS.NoCamera,
//            message: ALERTS.CameraMissing,
//            preferredStyle: .alert)
//
//        let okAction = UIAlertAction(
//            title: ALERTS.Ok,
//            style:.default,
//            handler: nil)
//        alertVC.addAction(okAction)
//
//        present(
//            alertVC,
//            animated: true,
//            completion: nil)
    }

    
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier) as! MusicGenreViewController
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        dstVC.delegate = self
        dstVC.prevSelectedMusicGenres = genresArray
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    
    /// Adjust TextView Height
    ///
    /// - Parameter arg: textView
    func adjustUITextViewHeight(textView : UITextView)
    {
        let fixedWidth = textView.frame.size.width

        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        
        var newFrame = textView.frame
        
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        textView.frame = newFrame
    }
    
    /// Set Data After Api Call
    ///
    /// - Parameter data: data To be Set
    func setAllData(data: ProfileResponseModel) {
        
        nameTextField.text = data.firstName
        lastNameTextField.text = data.lastName
        emailTextField.text = data.email
        pharmacyTextField.text = data.pharmacyName
        
        let dateFormat = DateFormatter()
        //dddd
        dateFormat.dateFormat = "MM-dd-yyyy"
        
        if let dobDate = dateFormat.date(from: data.dateOfBirth) {
            
            selectedDOB = dobDate
            dobLabel.text = Helper.getDobStringDate(date: selectedDOB)
            
            dateFormat.dateFormat = "dd-MM-yyyy"
           // dobLabel.text = dateFormat.string(from: selectedDOB)
            
           // self.birthdayPicker.setDate(selectedDOB, animated: true)
            
        }
        
        countryCodeLabel.text = data.countryCode
        filtercountry(countryCodeLabel.text!)
       
        
        let myString:NSString = "\(data.phoneNumber)                Verified" as NSString
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Hind", size: 14.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.green, range: NSRange(location:26,length:8))
        mobileNumberTextField.attributedText = myMutableString
        
       // mobileNumberTextField.attributedText = attributedWithTextColor
        //        self.title = data[SERVICE_RESPONSE.FirstName] as? String
        if data.addLine2.contains(data.addLine1) {
            billingAddressTextField.text = data.addLine2
        }
        else {
            billingAddressTextField.text = data.addLine1 + data.addLine2
        }
        dobLabel.text = data.dateOfBirth
        self.title = ""
        
//        let aboutString = data.about
//        if  aboutString.length > 0 && aboutString != ALERTS.aboutMe {
//            
//            aboutMeTextView.text = aboutString
//            aboutMeTextView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
//            
//        }else {
//            
//            aboutMeTextView.text = ALERTS.aboutMe
//            aboutMeTextView.textColor = TEXTFIELD_PLACEHOLDER_DEFAULT_COLOR
//        }
//        
        /*let prevgGenresArray: [Any] = data.genres
         
         genresArray = []
         
         if prevgGenresArray.count > 0 {
         
         var displayGenresName: String = ""
         var displayGenresId: String = ""
         
         for eachGenresData in prevgGenresArray {
         
         let genresDataModel = MusicGenreModel.init(musicGenreDetails: eachGenresData)
         
         genresArray.append(genresDataModel)
         
         if displayGenresName.length == 0 {
         
         displayGenresName = "\(genresDataModel.musicGenreName)"
         displayGenresId = "\(genresDataModel.musicGenreId)"
         
         }else {
         
         displayGenresName = "\(displayGenresName), \(genresDataModel.musicGenreName)"
         displayGenresId = "\(displayGenresId),\(genresDataModel.musicGenreId)"
         }
         }
         musicGenreApi = displayGenresId
         musicGenreTextField.text = displayGenresName
         
         } else {
         
         musicGenreTextField.placeholder = ALERTS.preference
         }*/
        
        //        musicGenreTextField.text = data.musicGenres
        if data.preferredPayment == 0 {
            
            musicGenreTextField.text = "Self pay"
        } else {
            
            musicGenreTextField.text = data.insuranceName 
//            "Insurance"
        }
        
        profilePicURL = data.profilePic
        
        self.profilePhoto.layer.borderWidth = 2
        self.profilePhoto.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x67AFF7).cgColor
        
        
        if !data.profilePic.isEmpty {
            
            profilePhoto.kf.setImage(with: URL(string: data.profilePic),
                                     placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: { receivedSize, totalSize in
            },
                                     completionHandler: nil)
        } else {
            profilePhoto.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
    }
    
    // MARK: - Segue Method -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEgueIdetifiers.profileToChangePass {
            
            if let dstVC = segue.destination as? SetPasswordController {
                dstVC.forgetPass = false
                dstVC.delegate = self
            }
        } else if segue.identifier == SEgueIdetifiers.profileChnageEM{
            if let dstVC = segue.destination as? ChangeEmailMobileViewController {
                
                dstVC.isEmail = isEmail
                dstVC.delegate = self
                
                if isEmail {
                    
                    dstVC.currentEmail = emailTextField.text!
                    
                } else {
                    
                    dstVC.currentCountryCode = countryCodeLabel.text!
                    dstVC.currentCountryImage = countryImage.image
                    dstVC.currentPhoneNumber = mobileNumberTextField.text!
                }
                
                
            }
            
        } else if segue.identifier == "profileToChangePayment" {
            if let dstVC = segue.destination as? vaiDRx_PreferredPaymentVC {
                dstVC.isComing = "Profile"
                dstVC.delegate = self
                dstVC.profileResponseModel = self.profileResponseModel
                
                
//                if aboutMeTextView.text! != ALERTS.aboutMe && (aboutMeTextView.text?.length)! > 0 {
//                    
//                    profileViewModel.aboutMeText.value = aboutMeTextView.text!
//                    
//                } else {
//                    
//                    profileViewModel.aboutMeText.value = ""
//                }
                
                let dateFormat = DateFormatter()
                //dddd
                dateFormat.dateFormat = "MM-dd-yyyy"
                profileViewModel.dobText = dateFormat.string(from: selectedDOB)
                
                profileViewModel.firstNameText.value = nameTextField.text!
                profileViewModel.lastNameText.value = lastNameTextField.text!
                profileViewModel.musicGenereText.value = musicGenreTextField.text!//musicGenreApi
                profileViewModel.profilePicURL = profilePicURL
                
                dstVC.profileViewModel = self.profileViewModel
            }
        }
        
    }
    
    func showViews() {
        topView.isHidden = false
        bottomView.isHidden = false
       // datePickerBackView.isHidden = false
        editButton.isHidden = false
    }
    
    func hideViews() {
        topView.isHidden = true
        bottomView.isHidden = true
        //datePickerBackView.isHidden = true
        editButton.isHidden = true
    }
    
    
}

extension ProfileViewController: PreferredPaymentDelegate {
    
    func paymentSelected(_ selectedPayment: PaymentType, Insurance: String?) {
        
        if selectedPayment.rawValue == 0{
            paymentTypeMethod = 0
        }
        else{
            paymentTypeMethod = 1
        }
        
        
        profileViewModel.paymentType = selectedPayment
        if (Insurance != nil && Insurance != "" ) {
            profileViewModel.insurance = Insurance!
        }
        if selectedPayment.rawValue == 0 {
            
            musicGenreTextField.text = "Self Pay"
        } else {
            
            musicGenreTextField.text = Insurance ?? "Insurance"
        }
    }
}

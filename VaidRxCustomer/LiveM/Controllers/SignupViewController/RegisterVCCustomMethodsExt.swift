//
//  RegisterVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension RegisterViewController
{
    /// initial setup for animations
    func initialSetup() {
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            
            DDLogDebug(countryCode)
            let picker = CountryNameViewController()
            picker.initialSetUp()
            let contry: Country
            contry = picker.localCountry(countryCode)
            countryCodeLabel.text = contry.dial_code
            let imagestring = contry.country_code
            let imagePath = "CountryPicker.bundle/\(imagestring).png"
            countryFlagImage.image = UIImage(named: imagePath)
        }
        
        facebookBackView.transform = CGAffineTransform(translationX: -self.view.frame.size.width, y: 0)
        googleBackView.transform = CGAffineTransform(translationX: -self.view.frame.size.width, y: 0)
        signUpBackView.transform = CGAffineTransform(translationX: 0, y: 1000)
        firstNameView.transform = CGAffineTransform(scaleX: 0, y: 0)
        emailView.transform = CGAffineTransform(scaleX: 0, y: 0)
        passwordView.transform = CGAffineTransform(scaleX: 0, y: 0)
        phoneNumberView.transform = CGAffineTransform(scaleX: 0, y: 0)
        yourBirthdayView.transform = CGAffineTransform(scaleX: 0, y: 0)
        refferalCodeView.transform = CGAffineTransform(scaleX: 0, y: 0)
        facbookBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        self.loadingConstraint.constant = 0
    }
    
    /// animating buttom View
    func animateBottomView() {
        
        if self.firstNameView.transform != .identity {
            UIView.animate(withDuration: 1.4, animations: {
                self.firstNameView.transform = .identity
                self.emailView.transform = .identity
                self.passwordView.transform = .identity
                self.phoneNumberView.transform = .identity
                self.yourBirthdayView.transform = .identity
                self.refferalCodeView.transform = .identity
                self.facbookBottomView.transform = .identity
                self.signUpBackView.transform = .identity
            })
        }
    }
    
    
    /// shows allert
    func alertView() {
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: ALERTS.SelectImage, message: ALERTS.SelectImageOption, preferredStyle: .actionSheet)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        let cancelActionButton = UIAlertAction(title: ALERTS.Cancel, style: .cancel) { _ in
            DDLogDebug("\(ALERTS.Cancel)")
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: ALERTS.Camera, style: .default)
        { _ in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            self.cameraButtonAction()
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: ALERTS.Gallery, style: .default)
        { _ in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            self.libraryAction()
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        if self.profilePhoto.image != PROFILE_DEFAULT_IMAGE {
            let deletePhotoButton = UIAlertAction(title: ALERTS.RemoveImage, style: .default)
            { _ in
                
                newAlertWindow.resignKey()
                newAlertWindow.removeFromSuperview()
                
                self.profilePhoto.image = PROFILE_DEFAULT_IMAGE
                self.profileAlternatePhoto = 2
                
                self.profilePhoto.layer.borderWidth = 1
                self.profilePhoto.layer.borderColor = RED_COLOR.cgColor
            }
            actionSheetControllerIOS8.addAction(deletePhotoButton)
        }
        newAlertWindow.rootViewController?.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    /// camera opened
    func cameraButtonAction() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker,animated: true,completion: nil)
            
        } else {
            
            noCamera()
        }
    }
    
    /// library opened
    func libraryAction() {
        
        if  UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            imagePicker.navigationBar.isTranslucent = false
            // Background color
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func noCamera(){
        
//        let alertVC = UIAlertController(
//            title: ALERTS.NoCamera,
//            message: ALERTS.CameraMissing,
//            preferredStyle: .alert)
        
        Helper.alertVC(title: ALERTS.NoCamera, message: ALERTS.CameraMissing)
        
//        let okAction = UIAlertAction(
//                title: ALERTS.Ok,
//                style:.default,
//               handler: nil)
//            alertVC.addAction(okAction)
//
//            present(
//                   alertVC,
//                animated: true,
//                    completion: nil)
    }
    
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier) as! CountryNameViewController
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        dstVC.countryDelegate = self
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    /// to move back to last controller
    func backButton() {
        
        backButtonActive = true
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        
        self.navigationController?.popViewController(animated: false)
    }
    
    
    
    
    /// Animate Button Loading for user Id password
    ///
    /// - Parameter value: value to decide its percent of loading
    func animateButtonLoading(_ value: Int) {
        
        self.view.layoutIfNeeded()
        
        switch value {
            
        case 0:
            self.loadingConstraint.constant = 0
            
        case 1:
            self.loadingConstraint.constant = (self.sigUpButtonOutlet.frame.size.width / 6) * 1
            
        case 2:
            self.loadingConstraint.constant = (self.sigUpButtonOutlet.frame.size.width / 6) * 2
            
        case 3:
            self.loadingConstraint.constant = (self.sigUpButtonOutlet.frame.size.width / 6) * 3
            
        case 4:
            self.loadingConstraint.constant = (self.sigUpButtonOutlet.frame.size.width / 6) * 4
            
        case 5:
            self.loadingConstraint.constant = (self.sigUpButtonOutlet.frame.size.width / 6) * 5
            
        case 6:
            self.loadingConstraint.constant = self.sigUpButtonOutlet.frame.size.width
            
            
        default:
            break
        }
        
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
        
    }
    
    /// Validating Fields
    func performRegiserProcess() {
        
        self.view.endEditing(true)
        uploadProfileImageToAmazon()
        
    }
    
    // MARK: - Segue Method -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEgueIdetifiers.signinToOtp {
            if let otpVC = segue.destination as? VerifyMobileViewController{
                otpVC.isCommingFrom = "Register"
                otpVC.userLoginType = userLoginType
                otpVC.dataFromRegisterVC = sender as! [String : Any]
                otpVC.userID = sender as! [String : Any]
            }
        }else if segue.identifier == SEgueIdetifiers.registerToLogin {
            if let otpVC = segue.destination as? LoginViewController{
                otpVC.emailFormFb = emailFormFb
                otpVC.registerFormFb = true
            }
        }
    }
    
    /// spring animation for bottomViews
    func springAnimation(){
        UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
            self.facebookBackView.transform = .identity
            UIView.animate(withDuration: 1.2, delay: 0.2, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                self.googleBackView.transform = .identity
            })
        })
    }
    
    func checkFortheField() {
        
        var shakeView: [UIView] = []
        
        if Helper.isValidEmail(testStr: emailTextField.text!) && isEmailValid {
            
            emailMissingHeight.constant = 0
            updateHeight(isIncrese: true)
            
        }else {
            
            shakeView.append(emailTextField)
            emailView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            alertsAtTheBottom(alertText: emailTextField)
        }
        
        if (firstNameTextField.text?.count)! > 0 {
            
            firstNameMissingHeight.constant = 0
            
        }else {
            
            shakeView.append(firstNameTextField)
            firstNameView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            alertsAtTheBottom(alertText: firstNameTextField)
        }
        
        if (lastNameTextField.text?.count)! > 0 {
            
            lastNameMissingHeight.constant = 0
            
        }else {
            
            shakeView.append(lastNameTextField)
            firstNameView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            alertsAtTheBottom(alertText: lastNameTextField)
        }
        
        if (phoneNumerTextField.text?.count)! > 5 && isPhoneNumberValid {
            
            phoneMissingHeight.constant = 0
            updateHeight(isIncrese: true)
            
        }else {
            
            shakeView.append(phoneNumerTextField)
            phoneNumberView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            alertsAtTheBottom(alertText: phoneNumerTextField)
        }
                
        
        if Helper.isValidPassword(password: passwordTextField.text!){
            
            changeLabelColor(passwordTextField, true)
            
        }else {
            
            shakeView.append(passwordTextField)
            passwordView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            alertsAtTheBottom(alertText: passwordTextField)
        }
        
        if (yourBirthdayTextField.text?.count)! > 5 {
            
            changeLabelColor(yourBirthdayTextField, true)
            
        }else {
            
            shakeView.append(yourBirthdayTextField)
            yourBirthdayView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            alertsAtTheBottom(alertText: yourBirthdayTextField)
        }
        
        self.view.resignFirstResponder()
        
        if !shakeView.isEmpty {
            
            shakeFields(view: shakeView)
            UIView.animate(withDuration: 0.6, animations: {
                self.view.layoutIfNeeded()
            })
  
        } else {
            
            performRegiserProcess()
        }
        
    }
    
    func alertsAtTheBottom(alertText: UITextField) {
//        var height = 0
        switch alertText {
        case emailTextField:
            emailMissingHeight.constant = 14
            updateHeight(isIncrese: false)
        case firstNameTextField:
            firstNameMissingHeight.constant = 14
        case lastNameTextField:
            lastNameMissingHeight.constant = 14
        case phoneNumerTextField:
            phoneMissingHeight.constant = 14
            updateHeight(isIncrese: false)
        case passwordTextField:
            changeLabelColor(passwordTextField, false)
            
        case yourBirthdayTextField:
            changeLabelColor(yourBirthdayTextField, false)
            
        default:
            break
        }
        
        if (firstNameTextField != nil) || (lastNameTextField != nil) {
            updateHeight(isIncrese: false)
        }
        
        
    }
    
    func updateHeight(isIncrese: Bool) {
        if isIncrese {
            if heightChange > 0{
                heightChange -= 14
            }
        }else {
            heightChange += 14
        }
    }
    
    func changeLabelColor(_ textField: UITextField,_ valid: Bool)  {
        switch textField {
        case passwordTextField:
            if valid {
                passwordmissingLabel.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
                passwordmissingLabel.text = ""//MissingAlert.password
            }else {
                passwordmissingLabel.textColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)
                passwordmissingLabel.text =  MissingAlert.passwordMissing
            }
        default:
            if valid {
                yourBirthdayView.backgroundColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
                birthdaymissingLabel.textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
                birthdaymissingLabel.text =  MissingAlert.dob
            }else {
                birthdaymissingLabel.textColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)
                birthdaymissingLabel.text =  MissingAlert.dobMissing
            }
        }
    }
    
    
    func shakeFields(view: [UIView]){
        for views in view {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.1
            animation.repeatCount = 5
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: views.center.x - 10, y: views.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: views.center.x + 10, y: views.center.y))
            views.layer.add(animation, forKey: "position")
        }
        
        
    }
    
    
}

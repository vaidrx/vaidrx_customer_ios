//
//  PendingReviewsAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class PendingReviewsAPI {
    
    let disposebag = DisposeBag()
    let pendingReviews_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call get list of bookings service API
    func pendingReviewsServiceAPICall() {
        
        let strURL = API.BASE_URL + API.METHOD.GET_PENDING_REVIEWS
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetPendingReviews)
                }
                
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
            case .GetPendingReviews:
                
                self.pendingReviews_Response.onNext(responseModel)
                
            default:
                break
            }
            
            break
        }
    }
    
}


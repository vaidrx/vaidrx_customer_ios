//
//  ConfigurationFile.swift
//  DayRunner
//
//  Created by Raghavendra V on 17/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

extension String {
    var localized: String {
        
        return NSLocalizedString(self, comment: "")
    }
}

struct ALERTS {

    //Alert Title 
    static let Message = "Message".localized
    static let Oops = "Message".localized
    static let NetworkError = "Network Error!".localized
    static let NoNetwork = "No Network Connection!".localized
    static let Error = "Error!".localized
    static let Missing = "Alert".localized
    static let Wrong = "Wrong".localized
    static let Done = "Done".localized
    static let SessionExpired = "Session Expired".localized
    static let LogOut = "Do you wish to logout ?".localized
    static let SaveDetails = "Do you wish to save any details?".localized
    static let UpdatePassword = "Your password has been changed successfully.".localized
    static let ChangePassword = "The new password and confirm password are a mismatch.".localized
    
    //Login Controller
    static let EmailOrPhoneNumberMissing = "Please enter email or phone number".localized
    static let EmailOrPhoneNumberInvalid = "Invalid email or phone number".localized
    static let PaswordMissing = "Please enter password".localized
    
    
    //Signup Controller
    static let FirstNameMissing = "Please enter first name".localized
    static let LastNameMissing = "Please enter last name".localized
    static let EmailMissing = "Please enter a valid email address".localized
    static let EmailInvalid = "Please enter a valid email address".localized
    static let PasswordInvalid = "Invalid Password, Password should consist of a capital letter, a small letter, a number & minimum of 7 characters.".localized
    static let PhoneNumberMissing = "Please enter phone number".localized
    static let PhoneNumberInvalid = "Please enter phone number".localized
//    static let ReferralCodeInvalid = "Invalid referral code".localized
    static let BirthDateMissing = "Please provide your birth date".localized
    static let InvalidPassword = "Please ensure the password has at least a capital letter, a small letter and a number consisting of minimum 7 characters".localized
    
    
    static let FacebookError = "Please provide your birth date".localized
    static let EmailError = "Your device could not send e-mail.  Please check e-mail configuration and try again.".localized
    
    static let MessageError = "Your device could not send message.  Please check message configuration and try again.".localized


    
    //OTP Controller
    static let OPTMissing = "Please enter the code sent to your registered mobile number".localized
   
    
    //Select Image
    static let SelectImage = "Please Select Image".localized
    static let SelectImageOption = "Option to select".localized
    static let Cancel = "Cancel".localized
    static let Gallery = "Gallery".localized
    static let Camera = "Camera".localized
    static let RemoveImage = "Remove Image".localized
    
    
    static let NoCamera = "No Camera".localized
    static let CameraMissing = "Sorry, this device has no camera".localized
    static let Ok = "Ok".localized
    
    
    //Select Image
    static let ForgotPassword = "Forgot Password".localized
    static let ForgotPasswordMessage = "Help with using".localized
    static let Email = "Email".localized
    static let PhoneNumber = "Phone Number".localized
    
    
    static let edit = "Edit".localized
    static let save = "Save".localized
    static let aboutMe = "We would like to know a little about you.".localized
//    static let preference = "Your genre preference.".localized
    
    
    //Remove Address
//    static let RemoveAddress = "Remove Address?".localized
    static let RemoveAddressMessage = "Are you sure you wish to remove this address?".localized
    static let YES = "YES".localized
    static let NO = "NO".localized
    static let SelectAddressTag = "Please provide a name for the address".localized
    
    
    // Delet CArd Allert
    static let deleteCard = "Card Deleted successfully".localized
    
    //Invite message
    static let ShareMessage = "Hi, i am using LiveM app which is available in AppStore/PlayStore. Signup with refferel Code: ".localized
    
    
    //Call Feature
    static let MissingCallFeature = "Call feature is unavailable!".localized
    
    //Message Feature
    static let MissingMessageFeature = "Message feature is unavailable!".localized
    
    
    //HelpCenter
    static let HelpCenterMessageBoxPlaceHolder = "In my case was not helping to clean Derived Data. I noticed that I have had opened storyboard in two tabs. So closed active one and opened inactive did the trick for me.".localized
    
    //Invoice
    static let invoiceReviewPlaceholder = "how was the performance ?".localized
    
    //Chat
    static let ChatMessage = "You got new message from bookingId:".localized
    
    //ZenDeskMessage
    static let ZenDeskMessage = "You got new message from zendesk".localized

    
    static let VIEW = "VIEW".localized
    
    //Home Screen
    static let NoArtistMessage = "Sorry, Currently We are not Operational in this region. Please Contact LiveM Support.".localized

    
    struct PAYMENT {
        
        static let MissingCardDetails = "Please Enter Valid Card Details".localized
        static let MissingStripeKey   = "Please specify a Stripe Publishable Key".localized
        static let CardAdded          = "Card added successfully".localized
        static let DeleteCardMessage  = "Card added successfully".localized
        
        static let RemovePayment = "Remove Payment?".localized
        static let RemovePaymentMessage = "Are you sure you wish to remove this payment?".localized
        
        static let MakeDefaultPaymentMessage = "Are you sure you want to make this payment default?".localized
        static let ZipcodeMissing = "Enter ZipCode"
    }
    
    
    struct MUSICIAN_DETAILS {
        
        static let MissingGigTime      = "Gig time is not available!".localized
    }
    
    
    struct CONFIRM_BOOKING {
        
        static let BookButton          = "CONFIRM AND BOOK".localized
        static let MissingGigTime      = "Please Select Gig Time".localized
        static let MissingEvent        = "Please select an event".localized
        static let MissingPayment      = "Please select payment".localized
        static let MissingAddress      = "Please select address".localized
        
    }
    
    struct BOOKING_FLOW {
        
        static let Ok                  = "OK".localized
        static let View                = "VIEW".localized
        static let EventID             = "eventID".localized
        
        static let ArtistRequested     = "Artist requested".localized
        static let ArtistAccepted      = "Artist accepted".localized
        static let ArtistOnTheWay      = "Artist on the way".localized
        static let ArtistArrived       = "Artist has arrived".localized
        static let EventStarted        = "Event started".localized
        static let EventCompleted      = "Event completed".localized
        static let InvoiceRaised       = "Invoice raised".localized
        static let IgnoredOrExpired    = "Ignored/Expired".localized
        
    }
    
    struct HOME_SCREEN {
    
        static let MusicianMissing = "No musicians are available!".localized
    }
    
    
    struct CANCEL_BOOKING {
        
        static let CancelReasonMissing = "Please select a cancellation reason".localized
    }
    
}


struct SHARE_MSG {
    
    static let  Twitter = "Sign-up to medical services booking app vaiDRx using my code '%@' and receive 15%% off your first use. Discover doctor in your area now.\n\nDownload Link:- ".localized
    
    static let  Email = "Hi, Download vaiDRx, and our vaid shall assist you when required '%@'.\n\nDownload Link:- %@".localized
    
    static let  Messenger = "Hi, if you sign-up to medical services booking app vaiDRx using the code '%@' you'll receive 15%% off your first booking. Give it a try now and discover great doctors in your local area.\n\nDownload Link:- %@".localized
    
    static let  Facebook = "Sign-up to medical services booking app vaiDRx using my code '%@' and receive 15%% off your first use. Discover doctors in your area now.".localized
    
    static let  Whatsapp = "Hi, if you sign-up to new live music booking app LiveM using the code '%@' you'll receive 15%% off your first booking. Give it a try now and discover great doctors in your local area.\n\nDownload Link:- %@".localized
}


struct CHANGE_ME {
    
    static let emailTittle = "Change your email address".localized
    static let emaileText = "We will send a verification link to this email address. Post verification, it will be added to your account.".localized
    static let phoneTittle = "Change your mobile number".localized
    static let phoneText = "Enter the new mobile number and you will get a verification code to reset.".localized
}

struct MissingAlert {
    static let passwordMissing = "Enter password.".localized
//    static let password = "Minimum of 7 characters with a number,small letter & capital letter.".localized
    
    static let dobMissing = "Enter your date of birth.".localized
    static let dob = "you need to be 18 or older to use LiveM service.".localized
}

struct HELP {
    
//    static let discover = "Discover".localized
//    static let discoverDisc = "Get instant access to hundreds of artists, browse their profiles, watch their videos, read reviews of past gigs and begin your unique experience with live music.".localized
    
//    static let book = "Book".localized
//    static let bookDisc = "When you open the LiveM app, artists near your location ready to play will be shown. Check them out, request a booking and pay quickly and easily.".localized
    
//    static let enjoy = "Enjoy".localized
//    static let enjoyDisc = "The artist will come to your location on the chosen date and play. Simply relax and enjoy the live music!".localized
    
//    static let review = "Review".localized
//    static let reviewDisc = "When the event is finished, review the performance. Your feedback helps the LiveM community to build a better experience for everyone.".localized
}



let months      =   ["jan".localized,
                     "Feb".localized,
                     "Mar".localized,
                     "Apr".localized,
                     "May".localized,
                     "June".localized,
                     "July".localized,
                     "Aug".localized,
                     "Sep".localized,
                     "Oct".localized,
                     "Nov".localized,
                     "Dec".localized]

struct PROGRESS_MESSAGE {
    
    static let Loading                      = "Loading...".localized
    static let Saving                       = "Saving...".localized
    static let Adding                       = "Adding...".localized
    static let Deleting                     = "Deleting...".localized
    static let LogOut                       = "Logging out...".localized
    static let Accepting                    = "Accepting Your Booking...".localized
    static let SavingAddress                = "Saving your address"
    
    //LoginVC
    static let Login                        = "Logging in...".localized
    static let FBLogin                      = "Facebook Login...".localized
    static let GoogleLogin                  = "Google Login...".localized
    
    //Facebook
    static let FetchingDetailsFromFB        = "Fetching Details...".localized
    
    
    //Forgot Password VC
    static let ForgotPassword               = "Fetching Password...".localized
    
    //RegisterVC
    static let ValidatingReferralCode       = "Validating Referral Code...".localized
    static let ValidatingPhoneNumber        = "Validating Phone Number...".localized
    static let ValidatingEmail              = "Validating Email...".localized
    
    static let ChangingEmail                = "Changing Email...".localized
    static let ChangingPhoneNumber          = "Changing Phone Number...".localized
    static let ChangingPassword             = "Changing Password...".localized
    
    //OTP VC
    static let VerifyingOTP                 = "Verifying Phone Number...".localized
    static let SignUP                       = "Register...".localized
    static let SendingOTP                   = "Sending...".localized
    
    //Confirm Booking
    static let ConfirmBooking               = "Booking Vaid...".localized//"Confirm Booking..."
    
    //Review
    static let SubmittingReview             = "Submitting Review...".localized
    
    //CancelBooking
    static let CancelBooking                = "Cancel Booking...".localized
    
    //ZenDesk
    static let CreatingTicket               = "Creating ticket...".localized
    
    static let GettingTicket                = "getting tickets...".localized
    
    static let SendingTicket                = "sending...".localized
    
    static let AcceptingBooking             =  "Accepting the booking...".localized
    
    static let PharmacyDetails              = "Fetching Pharmacy Details..."
    
    static let UpdatingPharmacy             = "Updating Pharamcy deatils..."

}

struct MESSAGETEXT {

    static let PLACEHOLDER_TEXT = "Add comments here...".localizedWith()

}

struct vaiDRx_Home {
    
    static let vaidNow   = "VaidNow".localized
    static let vaidToday = "VaidToday".localized
    static let rxToday   = "RxToday".localized
    static let vaidNow_Description = "Vaid (Primary Care Doctor) will see you in his office within 1 hour of the request acceptance time".localized
    static let vaidToday_Description = "Vaid (Primary Care Doctor) will see you at any open time slot in his office today.The time slot will be made available for you to accept or reject.".localized
    static let rxToday_Description = "Your medicine will be delivered to your adress by VaidRx (or affiliated Pharmacies) no later than 24 hours from the reciept of the Rx Information.".localized
}


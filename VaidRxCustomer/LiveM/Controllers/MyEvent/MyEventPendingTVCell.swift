//
//  MyEventPendingTVCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 18/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import UICircularProgressRing
import Kingfisher

class MyEventPendingTVCell: UITableViewCell {

    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var providerNameLabel: UILabel!
    @IBOutlet weak var bookingDateLabel: UILabel!
    @IBOutlet weak var bookingTimeLabel: UILabel!
    @IBOutlet weak var providerDistanceLabel: UILabel!
    
    @IBOutlet weak var remaningTimeView: UICircularProgressRingView!
    
    @IBOutlet weak var remainingTimeLabel: UILabel!

    
    var timer = Timer()
    var intervalProgress: Double!
    var totalDuration: Double = 0.0
    var remainingTimeValue: Int = 0
    var bookingDetailModel:BookingDetailsModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func showBookingDetails(bookingDetail:BookingDetailsModel) {
        
        bookingDetailModel = bookingDetail
        
        if bookingDetail.providerImageURL.length > 0 {
            
            providerImage.kf.setImage(with: URL(string: bookingDetail.providerImageURL),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            providerImage.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }

        
        providerNameLabel.text = bookingDetail.providerName.capitalized
        
        providerDistanceLabel.text =  Helper.getDistanceDependingMileageMetricFromServer(distance: bookingDetail.distance, mileageMatric: bookingDetail.mileageMatric)//String(format:"%.2f miles away",bookingDetail.distance)
        
    
        let startDate = Date(timeIntervalSince1970: TimeInterval(bookingDetail.bookingStartTime))
    
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "d MMM yyyy"
        
        bookingDateLabel.text = dateFormat.string(from: startDate)
        
        dateFormat.dateFormat = "hh:mm a"
        bookingTimeLabel.text = dateFormat.string(from: startDate)
        
        let requestedAtDate = Date(timeIntervalSince1970: TimeInterval(bookingDetail.bookingRequestedAt))

        
        let endDate = Date(timeIntervalSince1970: TimeInterval(bookingDetail.bookingEndTime))
        
        totalDuration = endDate.timeIntervalSince(requestedAtDate)
        
        remainingTimeValue = Int(totalDuration)
        
        if endDate.timeIntervalSinceNow > 0 {
            
            if requestedAtDate.timeIntervalSinceNow < 0 {
                
                remainingTimeValue = Int(endDate.timeIntervalSinceNow)
                setRemainingTimeProgress()
                
            } else {
                
                setRemainingTimeProgress()
            }
            
            
            
        } else {
            
            remainingTimeValue = 0
            
            timer.invalidate()
            
            remaningTimeView.setProgress(value: 0, animationDuration: 0) {
                
                self.showRemainingTimeDetails()
                self.remaningTimeView.viewStyle = 3
                MyEventViewController.sharedInstance().passExpiredPendingEventToPastEventList(bookingDetail: bookingDetail)
            }

        }
        
    }
    
    func setRemainingTimeProgress() {
        
        intervalProgress = 100.0 / totalDuration
        
        scheduledTimerWithTimeInterval()
        
        self.showRemainingTimeDetails()
        
        remaningTimeView.setProgress(value: CGFloat(intervalProgress * Double(remainingTimeValue)), animationDuration: 0) {
            
        }
    }

    
    func scheduledTimerWithTimeInterval(){
        
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateRemainingTimeView), userInfo: nil, repeats: true)
    }
    
    @objc func updateRemainingTimeView(){
        
        remainingTimeValue -= 1
        
        if remainingTimeValue > 0 {
            
            remaningTimeView.setProgress(value:CGFloat(intervalProgress * Double(remainingTimeValue)), animationDuration: 0, completion: {
                
                self.showRemainingTimeDetails()
            })
            
            
        } else {
            
            timer.invalidate()
            
            remaningTimeView.setProgress(value: 0, animationDuration: 0) {
                
                self.showRemainingTimeDetails()
                
                MyEventViewController.sharedInstance().passExpiredPendingEventToPastEventList(bookingDetail: self.bookingDetailModel)
            }
            
        }
        
       
    }
    
    func showRemainingTimeDetails() {
        
        let minute:Int = remainingTimeValue/60
        let seconds:Int = remainingTimeValue - (minute * 60)
        
        var minuteString = ""
        var secondsString = ""
        
        if minute < 10 {
            
            minuteString = "0\(minute)"
            
        } else {
         
            minuteString = "\(minute)"
        }
        
        if seconds < 10 {
            
            secondsString = "0\(seconds)"
            
        } else {
            
            secondsString = "\(seconds)"
        }
        
        remainingTimeLabel.text = minuteString + ":" + secondsString

    }
   
}

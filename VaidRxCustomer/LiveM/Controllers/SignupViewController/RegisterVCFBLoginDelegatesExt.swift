//
//  RegisterVCFBLoginDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension RegisterViewController:facebookLoginDelegate {
    
    func didFacebookUserLogin(withDetails userInfo: [String : Any]) {

        DDLogVerbose("FBData = \(userInfo)")
        fbUserDataModel = UserDataModel.init(facebookDetails: userInfo)
        showFacebookDetails()
        
    }
    
    func showFacebookDetails() {
        
        if fbUserDataModel.mailId.length == 0 && fbUserDataModel.phoneNumber.length == 0 {
            
            emailTextField.text = ""
            registerViewModel.emailText.value = ""
            
            phoneNumerTextField.text = ""
            registerViewModel.phoneNumberText.value = ""
            
            firstNameTextField.text = ""
            registerViewModel.firstNameText.value = ""
            
            passwordTextField.text = ""
            registerViewModel.passwordText.value = ""
            
            yourBirthdayTextField.text = ""
            registerViewModel.dobText.value = ""
            
            referralCodeTextField.text = ""
            registerViewModel.referralCodeText.value = ""
            
            firstNameTextField.becomeFirstResponder()
            
        } else {
            
            isFBSignUp = true
            
            lastNameTextField.text = fbUserDataModel.lastName
            registerViewModel.lastNameText.value = lastNameTextField.text!
       
            firstNameTextField.text = fbUserDataModel.firstName
            registerViewModel.firstNameText.value = firstNameTextField.text!
            
            if fbUserDataModel.phoneNumber.length > 0 && fbUserDataModel.phoneNumber.length == 0 {
                
                emailFormFb = fbUserDataModel.phoneNumber
                emailTextField.text = fbUserDataModel.phoneNumber
                registerViewModel.emailText.value = emailTextField.text!
                
                sendServiceForFacebookLogin()
                
            } else {
                
                emailFormFb = fbUserDataModel.mailId
                emailTextField.text = fbUserDataModel.mailId
                registerViewModel.emailText.value = emailTextField.text!
                
                sendRequestToValidateEmailAddress()
            }
            
            profileAlternatePhoto = 1
            
            let fbProfileImageURL = String(format:Facebook.profileImageURL,fbUserDataModel.userId)
                
            ///Downloading the facebook profile image
            self.profilePhoto.kf.setImage(with: URL(string: fbProfileImageURL),
                                          placeholder:PROFILE_DEFAULT_IMAGE,
                                          options: nil,
                                          progressBlock: { receivedSize, totalSize in
                                            
            },
                                          completionHandler: nil)
            
            animateButtonLoading(checkFieldsAreValid(textField: firstNameTextField, textFieldText: firstNameTextField.text!))
        }
        
    }
    
    func didFailWithError(_ error: Error?) {
        
        
        Helper.alertVC(title: ALERTS.Message, message: (error?.localizedDescription)!)
    }
    
    func didUserCancelLogin() {
        
        DDLogDebug("User Cancelled Facebook Login")
    }
    
}

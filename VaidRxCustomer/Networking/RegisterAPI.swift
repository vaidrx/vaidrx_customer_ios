//
//  RegisterAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 22/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class RegisterAPI {
    
    let disposebag = DisposeBag()
    let register_Response = PublishSubject<APIResponseModel>()
    let validateEmail_Response = PublishSubject<APIResponseModel>()
    let validatePhoneNumber_Response = PublishSubject<APIResponseModel>()
    let validateReferralCode_Response = PublishSubject<APIResponseModel>()
    
    
    let location = LocationManager.sharedInstance()
    
    /// Method to call Register Service API
    ///
    /// - Parameter registerModel: Contains Register Request Details
    func registerServiceAPICall(registerModel: RegisterModel){
        
        let strURL = API.BASE_URL + API.METHOD.SIGNUP
        
        var requestParams:[String:Any] = [
            
            SERVICE_REQUEST.FirstName:registerModel.firstNameText,
            SERVICE_REQUEST.LastName:registerModel.lastNameText,
            SERVICE_REQUEST.Validation.Email:registerModel.emailText,
            SERVICE_REQUEST.Password:registerModel.passwordText,
            SERVICE_REQUEST.DOB:registerModel.dateOdBirth,
            SERVICE_REQUEST.CountryCode:registerModel.countryCode,
            SERVICE_REQUEST.Mobile:registerModel.phoneNumberText,
//            SERVICE_REQUEST.DOB:registerModel.dobText,
            SERVICE_REQUEST.ProfilePic:registerModel.profilePicURL,
            SERVICE_REQUEST.LoginType:registerModel.registerType.rawValue,
            SERVICE_REQUEST.Lat:location.latitute,
            SERVICE_REQUEST.Long:location.longitude,
            SERVICE_REQUEST.TermsAndCondition:registerModel.termsAndCond,
            SERVICE_REQUEST.DeviceID:Utility.deviceId,
//            SERVICE_REQUEST.PushToken:Utility.pushtoken,
            SERVICE_REQUEST.DeviceType:1,
            SERVICE_REQUEST.DeviceTime:TimeFormats.currentDateTime,
            SERVICE_REQUEST.DeviceModel:UIDevice.current.modelName,
            SERVICE_REQUEST.DeviceMake:Utility.deviceMake,
            SERVICE_REQUEST.AppVersion:Utility.appVersion,
            SERVICE_REQUEST.paymentType:registerModel.paymentType.rawValue,
            SERVICE_REQUEST.CardToken:registerModel.cardToken,
            SERVICE_REQUEST.insurance:registerModel.insurance,
            SERVICE_REQUEST.AddLine1:registerModel.addLine1,
            SERVICE_REQUEST.PharmacyId:registerModel.pharmacyId,
            SERVICE_REQUEST.TaggedAs:registerModel.taggedAs,
            SERVICE_REQUEST.ReferralCode:registerModel.referralCode,
            SERVICE_REQUEST.FaceBookID:registerModel.facebookId,
            SERVICE_REQUEST.GoogleId:registerModel.googleId,
//            SERVICE_REQUEST.CategoryId:"categoryId",
            SERVICE_REQUEST.DeviceOSVersion:Utility.deviceVersion,
            SERVICE_REQUEST.IPAddress:UserDefaults.standard.string(forKey: USER_DEFAULTS.IP_ADDRESS.iPAddress)!,
            SERVICE_REQUEST.PlaceName:"",
//            SERVICE_REQUEST.AddLine2:"addline2",
            SERVICE_REQUEST.City:registerModel.city,
            SERVICE_REQUEST.State:registerModel.state,
            SERVICE_REQUEST.Country:registerModel.country,
            SERVICE_REQUEST.PlaceId:"",
            SERVICE_REQUEST.Pincode:registerModel.pinCode,
        ]
        
        print("register model in registerAPI",requestParams)
        if registerModel.referralCode.count > 0 {
            
            requestParams[USER_DEFAULTS.USER.REFERRAL_CODE] = registerModel.referralCode
        }
        
        if registerModel.registerType == RegisterType.Facebook  {
            
            requestParams[SERVICE_REQUEST.FacebookId] = registerModel.facebookId
        } else if registerModel.registerType == RegisterType.Google {
            
            requestParams[SERVICE_REQUEST.GoogleId] = registerModel.facebookId
        }
        
        if let iPAddress = UserDefaults.standard.object(forKey: USER_DEFAULTS.IP_ADDRESS.iPAddress) as? String {
            
            if iPAddress.length > 0 {
                
                requestParams[SERVICE_REQUEST.IPAddress] = iPAddress
            }
            
        }
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.SignUP)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: nil)
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.signUp)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to call validate email service API
    ///
    /// - Parameter emailText: email to validate
    func validateEmailServiceAPICall(emailText:String) {
        
        let strURL = API.BASE_URL + API.METHOD.EMAIL_VALIDATION
        
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.Validation.Email:emailText,
        ]
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.ValidatingEmail)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: nil)
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.verifyEmail)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    

    
    /// Method to call validate Phone number service API
    ///
    /// - Parameters:
    ///   - countryCode: phonenumber country code
    ///   - phoneNumber: phone number to validate
    func validatePhoneNumberServiceAPICall(countryCode:String, phoneNumber:String, triggerValue: Int) {
        
        let strURL = API.BASE_URL + API.METHOD.PHONE_VALIDATION
        
        let requestParams:[String:Any] = [

            SERVICE_REQUEST.Validation.Country_Code : countryCode,
            SERVICE_REQUEST.Validation.Phone :phoneNumber,
            SERVICE_REQUEST.Validation.trigger: triggerValue
        ]
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.ValidatingPhoneNumber)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: nil)
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    if "\(dict["message"] ?? "")" == "This Phone number is not registered with us" {
                        Helper.hidePI()
//
                        
                        
//                        Helper.alertVC(title: ALERTS.Error , message: "\(dict["message"] ?? "")")
                        
                    }
//                    else {
//                        Helper.alertVC(title: ALERTS.Error , message: "\(dict["message"] ?? "")")
//                    }
                  let statuscode:Int = r.statusCode
                  self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.verifyMobileNumber)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to call validate Referral code service API
    ///
    /// - Parameter referralCode: referral code to vcalidate
    func validateReferralCodeServiceAPICall(referralCode:String) {
        
        let strURL = API.BASE_URL + API.METHOD.VALIDATE_REFERRAL
        
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.Referal.Code:referralCode,
            SERVICE_REQUEST.Referal.Lat:location.latitute,
            SERVICE_REQUEST.Referal.Long:location.longitude,
            SERVICE_REQUEST.Referal.UserType:1
            
        ]
        
        Helper.showPI(_message: PROGRESS_MESSAGE.ValidatingReferralCode)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: nil)
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.verifyReferralCode)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
            case HTTPSResponseCodes.BadRequest.rawValue:
                
                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                    
                }
                break
                
            case HTTPSResponseCodes.InternalServerError.rawValue:
                
                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                    
                }
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                    
                }
                break
                
                
            default:
                
                let responseModel:APIResponseModel!
                responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
                
                switch requestType {
                    
                    case .signUp:
                    
                        self.register_Response.onNext(responseModel)
                    
                    case .verifyEmail:
                    
                        self.validateEmail_Response.onNext(responseModel)
                    
                    case .verifyMobileNumber:
                    
                        self.validatePhoneNumber_Response.onNext(responseModel)
                    
                    case .verifyReferralCode:
                    
                        self.validateReferralCode_Response.onNext(responseModel)
                    
                    default:
                        break
                }
                
                break
        }
    }
    
}

//
//  FBLoginHandler.swift
//  FaceBookLoginSwift
//
//  Created by Raghavendra V on 21/01/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Accounts


protocol facebookLoginDelegate {
    /**
     *  Facebook login is success
     *
     *  @param userInfo Userdict
     */
    
    func didFacebookUserLogin(withDetails userInfo: [String:Any])
    /**
     *  Login failed with error
     *
     *  @param error error
     */
    
    func didFailWithError(_ error: Error?)
    
    /**
     *  User cancelled
     */
    
    func didUserCancelLogin()
}



class FBLoginHandler:NSObject {
    
    static var share:FBLoginHandler?
    
    var readPermission:[String] = []
    var parameters:[String:String] = ["fields": "picture, email, name, about, first_name, last_name"]
    var delegate: facebookLoginDelegate? = nil
    
    
    class func sharedInstance() -> FBLoginHandler {
        
        if (share == nil) {
            
            share = FBLoginHandler.self()
        }
        return share!
    }
    
    @objc func sendBackTheFacebookDetails(fbDetails: Any) {
        
        Helper.hidePI()
        
        if (self.delegate != nil)  {
            self.delegate?.didFacebookUserLogin(withDetails: fbDetails as! [String : Any])
        }
    }
    
    
    
    /// Method to login with facebook
    ///
    /// - Parameter viewController: current viewcontroller details
    func login(withFacebook viewController: UIViewController) {
        
//        AccessToken.setCurrent(nil)
        
        readPermission.append("public_profile")
        readPermission.append("email")
        readPermission.append("user_friends")
        readPermission.append("user_status")

        
        let login = LoginManager()
//        login.loginBehavior = LoginBehavior.native
        login.logOut()
        
        login.logIn(permissions: self.readPermission,
                    from: viewController) {  (result:LoginManagerLoginResult?,error:Error?) in
            
            if (error != nil) {
                
                print("Login error : \(String(describing: error?.localizedDescription))")
                
                if (self.delegate != nil) {
                    self.delegate?.didFailWithError(error)
                }
            }
            else if (result?.isCancelled)! {
                
                print("Cancelled")
                if (self.delegate != nil) {
                    self.delegate?.didUserCancelLogin()
                }
            }
            else {
                print("Logged in")
                self.getDetailsFromFacebook(loginManager: login)
            }
        }
    }
    
    
    
    /// Method to fetach facebook user details using Graph request
    func getDetailsFromFacebook(loginManager:LoginManager) {
        
        if (AccessToken.current != nil) {
            
            Helper.showPI(_message: PROGRESS_MESSAGE.FetchingDetailsFromFB)
            
            let graphRequest = GraphRequest(graphPath: "me", parameters: ["fields" : "picture, email, name, about, first_name, last_name"])
            let connection = GraphRequestConnection()
            connection.add(graphRequest, completionHandler: { (connection, result, error) in
                
                if error != nil {
                    
                    //do something with error
                    print("Getting details error : \(String(describing: error?.localizedDescription))")
                    
                    Helper.hidePI()
                    
                    if (self.delegate != nil) {
                        self.delegate?.didFailWithError(error)
                    }
                    
                } else {
                    
                    //do something with result
                    print("Fetched user:\(String(describing: result))")
                    self.perform(#selector(self.sendBackTheFacebookDetails(fbDetails:)), with: result, afterDelay: 0.6)
                }
                
                loginManager.logOut()
            })
            
            connection.start()
        }
    }
    
    
}


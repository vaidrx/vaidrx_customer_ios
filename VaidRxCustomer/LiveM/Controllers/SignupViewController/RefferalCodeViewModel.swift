//
//  RefferalCodeViewModel.swift
//  DayRunner
//
//  Created by NABEEL on 12/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
protocol RefferalDelegate {
    func RefferalDelegate(success:Bool,message:String)
}
class RefferalCodeViewModel: NSObject {
    
    var RefferalDelegate:RefferalDelegate! = nil
    func sendRequestForReferal(data : [String : Any]) {
        Helper.showPI()
        NetworkHelper.requestPOST(serviceName:API.METHOD.VALIDATE_REFERRAL,
                                  params: data,
                                  success: { (response : [String : Any]) in
                                    print("login response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        
                                        if let errFlag: Int = response["errFlag"] as? Int {
                                            
                                            if errFlag == 0 {
                                                if self.RefferalDelegate != nil {
                                                    self.RefferalDelegate.RefferalDelegate(success: true, message: response["errMsg"] as! String)
                                                }
                                            }
                                            else if errFlag == 1 {
                                                if self.RefferalDelegate != nil {
                                                    self.RefferalDelegate.RefferalDelegate(success: false, message: response["errMsg"] as! String)
                                                }
                                            }
                                        } else {
                                            
                                            if let statusCode: ERRORS = ERRORS(rawValue: (response["statusCode"] as? Int)!) {
                                                
                                                if statusCode == .MISSING {
                                                    if self.RefferalDelegate != nil {
                                                        self.RefferalDelegate.RefferalDelegate(success: false, message: response["message"] as! String)
                                                    }
                                                }
                                            }
                                        }
                                    }
        }) { (Error) in
            if self.RefferalDelegate != nil {
                self.RefferalDelegate.RefferalDelegate(success: false, message: Error.localizedDescription)
            }
        }
    }
}

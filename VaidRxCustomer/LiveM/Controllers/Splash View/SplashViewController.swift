//
//  SplasScreenViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 27/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import AVFoundation

class SplashViewController: UIViewController {
    
    //Image View
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var paused: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set image by screen Size
        let screenSize : CGSize = UIScreen.main.bounds.size
        Helper.editNavigationBar(navigationController!)

        
        //Show  splash screen image depending on screen size
        switch screenSize.height {
            case 480:
                self.backgroundImageView.image = UIImage(named: "iPhone4_Screen")
                break
            case 568:
                self.backgroundImageView.image = UIImage(named: "iPhone5_Screen")
                break
            case 667:
                self.backgroundImageView.image = UIImage(named: "iPhone6_Screen")
                break
            case 812:
                self.backgroundImageView.image = UIImage(named: "iPhoneX_Screen")
                break
            default:
                self.backgroundImageView.image = UIImage(named: "iPhone6Plus_Screen")
                break
        }
        
//        UserDefaults.standard.removeObject(forKey:USER_DEFAULTS.MUSICIAN_LIST.PREVIOUS)
        
        navigationController?.isNavigationBarHidden = true;
        
        let location = LocationManager.sharedInstance()
        location.start()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        
        if Utility.sessionToken.length > 0 {//Checking user session is available or not
            
            let location = LocationManager.sharedInstance()
            location.start()
            
            ConfigManager.sharedInstance.getConfigurationDetails()
            
            AppDelegate().connectToMQTT()
            MQTTOnDemandAppManager.sharedInstance().subScribeToInitialTopics()
            
            //Initialize Coouch DB
            CouchDBManager.sharedInstance.createCouchDB()
            
            //Goto HomeVC
            let menu = HelperLiveM.sharedInstance
            menu.createMenuView()
            
        } else {
            
            removeSplash()
        }
        
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
//    override var prefersStatusBarHidden : Bool {
//        return true
//    }
    
    
    
    /// Remove Splash Screen and go to Help Screen
    func removeSplash() -> Void {
        
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.helpVC)
            
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
            
            
            
        self.navigationController?.pushViewController(dstVC, animated: false)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}

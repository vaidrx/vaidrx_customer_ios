//
//  HelperLiveM.swift
//  DayRunner
//
//  Created by Rahul Sharma on 03/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class HelperLiveM: NSObject {
    
    static let sharedInstance = HelperLiveM()

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    
    /// Crate left Menu
    func createMenuView(){
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeScreenViewController = storyboard.instantiateViewController(withIdentifier: "VaidHomeVc") as! VrHomeViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: homeScreenViewController)
        nvc.navigationBar.isTranslucent = true
        
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuTableVC") as! LeftMenuTableViewController
        
        leftViewController.homeScreenViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        
        WINDOW_DELEGATE??.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        WINDOW_DELEGATE??.rootViewController = slideMenuController
        WINDOW_DELEGATE??.makeKeyAndVisible()
    }
    
    
    
    /// Method for Go to Landing view controller
    func goToSplashVC() {
        
        //        Helper.closeSplashLoading()
        
        if LocalNotificationView.share != nil {
            
            LocalNotificationView.share?.removeFromSuperview()
            LocalNotificationView.share = nil
        }
        
        
        let storyboard = UIStoryboard(name: "vaiDRx_Main", bundle: nil)
        
        
        let helpVC = storyboard.instantiateViewController(withIdentifier: VCIdentifier.helpVC) as! vaiDRxLandingVCViewController
        
        let rootNaviVC: UINavigationController = UINavigationController(rootViewController: helpVC)
        rootNaviVC.navigationBar.isTranslucent = false
        
        WINDOW_DELEGATE??.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        WINDOW_DELEGATE??.rootViewController = rootNaviVC
        WINDOW_DELEGATE??.makeKeyAndVisible()
        
    }
    
      
}

//
//  vaiDRx_SocialSignUpVC_CustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 27/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_SocialSignUpVC {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toVerifyPhone" {
            
            if let dstVC = segue.destination as? vaiDRx_VerifyOTPVC{
                
                dstVC.dataFromLandingVC = sender as! [String : Any]
                dstVC.registerViewModel = self.registerViewModel
                dstVC.isComingFrom = "SocialSignUp"
            }
        }

    }

}

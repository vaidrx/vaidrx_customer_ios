//
//  vaiDRx_PastProviderTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class vaiDRx_PastProviderTableViewCell: UITableViewCell {

    @IBOutlet weak var ProviderProfilePic: UIImageView!
    
    @IBOutlet weak var pastProviderName: UILabel!
    
    
    @IBOutlet weak var lastBookedOn: UILabel!
    
    @IBOutlet weak var totalBookingToProvider: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    

}

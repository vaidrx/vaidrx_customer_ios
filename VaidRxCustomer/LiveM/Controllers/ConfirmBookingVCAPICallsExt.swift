//
//  ConfirmBookingVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmBookingScreen {
    
    func liveBookinAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        confirmBookingViewModel.CBModel = CBModel
        
        confirmBookingViewModel.liveBookingAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.liveBooking)
        }

    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode {
            
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .liveBooking:
                            
                            progressMessage = PROGRESS_MESSAGE.ConfirmBooking
                            
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                break
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType {
                    
                    case RequestType.liveBooking:
                        
                        if let dataRes = dataResponse as? [String:Any] {
                            
                            if let bookingId = dataRes["bookingId"] as? Int64 {
                                
                                MixPanelManager.sharedInstance.LiveBookingEvent(bookingId: String(bookingId))
                                
                            }
                            
                        }
                        
                        AppoimtmentLocationModel.sharedInstance.bookingRequested = true
                        
                        UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.NewBookingisCreated)
                        UserDefaults.standard.synchronize()
                        
                        self.navigationController?.popToRootViewController(animated: false)
                        LeftMenuTableViewController.sharedInstance().changeViewController(LeftMenu.myEvent)
                        
                        break
                        
                    default:
                        
                        break
                }
                
                break
                
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }
                
                
                break
        }
        
    }
    
    
}

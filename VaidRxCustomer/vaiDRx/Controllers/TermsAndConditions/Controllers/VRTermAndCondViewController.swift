//
//  VRTermAndCondViewController.swift
//  GoTasker
//
//  Created by 3Embed on 15/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import WebKit

class VRTermAndCondViewController: UIViewController,WKNavigationDelegate {
    
   //var signUpViewModel = SignUpViewModel()
    

    @IBOutlet weak var webViewBack: UIView!
    var webView: WKWebView!
    @IBOutlet weak var acceptButton: UIButton!
    
    var registerViewModel : RegisterViewModel!
    var userRegisterType = RegisterType.Default
    var isAccepted = false
    var status = false
    
 
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView = WKWebView()
        webView.navigationDelegate = self
        let strurl = URL(string: "http://3.16.144.147/vaidRx/iserve-livem-php-central-admin/supportText/customer/en_termsAndConditions.php")!
        print(strurl)
        let requestObj = NSURLRequest(url: strurl)
        //                            self.liveChatWebView.loadRequest(requestObj as URLRequest)
        self.webView.load(requestObj as URLRequest)
        self.webView.allowsBackForwardNavigationGestures = true
        
        
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        
        Helper.getIPAddress()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }

    override func viewDidAppear(_ animated: Bool) {
//        webView.frame = CGRect(x: webViewBack.frame.origin.x, y: webViewBack.frame.origin.y, width: webViewBack.frame.size.width, height: webViewBack.frame.size.height)
        webView.frame = webViewBack.bounds
        webViewBack.insertSubview(webView, at: 1)
//        webViewBack.addSubview(webView)
    }
    
    func prepareUrl(_ url: String) -> String {
        
        let mutableString = NSMutableString.init(string: "https://\(url)")
    
        mutableString.replaceOccurrences(of: "{%license%}",
                                         with: LiveChat.LicenceNumber,
                                         options: .literal,
                                         range: NSRange(location: 0, length: (mutableString.length )))
        mutableString.replaceOccurrences(of: "{%group%}",
                                         with: APP_NAME,
                                         options: .literal,
                                         range: NSRange(location: 0, length: (mutableString.length)))
        return mutableString as String
        
    }

    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        
        if isAccepted == true{
            
           
        
            if status == false{
               // HelperVaidRx.showPI(_message: PROGRESS_MESSAGE_VAIDRX.Register)
                //sendSignUp()
                serviceRequestToMakeRegister()
                
            }
            
            
        }
        else{
            Helper.showAlert(head: ALERTS.Oops, message: "Accept term and conditions")
            //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Oops, message: "Accept term and conditions")
        }
        
        
    }
    
    @IBAction func acceptButtonAction(_ sender: UIButton) {
        if isAccepted == false{
            acceptButton.setImage(UIImage(named: "accept"), for: .normal)
            print("button tapped")
            isAccepted = true
            registerViewModel.termsAndCond = 1

        }
        else{
            acceptButton.setImage(UIImage(named: "reject"), for: .normal)
            status = false
            isAccepted = false
            registerViewModel.termsAndCond = 0
        }
        
    }
}

//
//  ConfirmBookingAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class ConfirmBookingAPI {
    
    let disposebag = DisposeBag()
    let liveBooking_Response = PublishSubject<APIResponseModel>()
    
    
    
    /// Method to call Live Booking Service API
    ///
    /// - Parameter confirmBookingModel: model contails livebooking request details
    func liveBookingServiceAPICall(confirmBookingModel:ConfirmBookingModel){
        
        let strURL = API.BASE_URL + API.METHOD.LIVE_BOOKING
        
        
        let requestParams = [
            
            SERVICE_REQUEST.LiveBooking.bookingModel: confirmBookingModel.bookingModel,
            SERVICE_REQUEST.LiveBooking.bookingType: confirmBookingModel.bookingType,
//                confirmBookingModel.appointmentLocationModel.bookingType.rawValue,
            SERVICE_REQUEST.LiveBooking.ipAddress: Utility.iPAddress, 
            SERVICE_REQUEST.LiveBooking.latitude:confirmBookingModel.appointmentLocationModel.pickupLatitude,
            SERVICE_REQUEST.LiveBooking.longitude:confirmBookingModel.appointmentLocationModel.pickupLongitude,
            SERVICE_REQUEST.LiveBooking.providerId:confirmBookingModel.providerId,
            SERVICE_REQUEST.LiveBooking.ageType:confirmBookingModel.ageType,
            SERVICE_REQUEST.LiveBooking.bookingMode:confirmBookingModel.bookingMode
//            SERVICE_REQUEST.LiveBooking.eventId:confirmBookingModel.selectedEventId,
//            SERVICE_REQUEST.LiveBooking.gigTimeId:confirmBookingModel.selectedGigTimeId,
            
        ] as [String : Any]
        
        if confirmBookingModel.paymentmethodTag == 0 {
//
//            requestParams[SERVICE_REQUEST.LiveBooking.paymentMethod] = 2
//
//            if confirmBookingModel.selectedCardModel != nil {
//
//                requestParams[SERVICE_REQUEST.LiveBooking.paymentCardId] = confirmBookingModel.selectedCardModel.id
//            }
            
        } else {
            
//            requestParams[SERVICE_REQUEST.LiveBooking.paymentMethod] = 1
        }
        
        if confirmBookingModel.appointmentLocationModel.bookingType == BookingType.Schedule {
//
//            let dateformat = DateFormatter()
//            dateformat.dateFormat = "yyyy-MM-dd HH:mm:ss"
//
//            requestParams["bookingDate"] = dateformat.string(from: confirmBookingModel.appointmentLocationModel.scheduleDate)
//            requestParams["scheduleTime"] = "\((confirmBookingModel.appointmentLocationModel.selectedEventStartTag + 1) * 30)"
//            requestParams["deviceTime"] = TimeFormats.currentDateTime
        } else {
            
//            requestParams["eventStartTime"] = (confirmBookingModel.selectedEventTag + 1) * 30
        }

        print(requestParams)
        Helper.showPI(_message: PROGRESS_MESSAGE.ConfirmBooking)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.liveBooking)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
            case .liveBooking:
                
                self.liveBooking_Response.onNext(responseModel)
                
            default:
                break
            }
            
            break
        }
    }
    
}

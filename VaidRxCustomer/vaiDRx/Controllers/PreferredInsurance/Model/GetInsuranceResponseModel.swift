//
//  GetInsuranceResponseModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 22/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//


import Foundation

class GetInsuranceResponseModel {
    
    var allInsurances: [[String : Any]] = []
    var insurance: [String:Any] = [:]

    
    
    /// Creating Insurances model from Insurance details
    ///

    init(insuranceDetails: Any) {
        
        if let providerDetail = insuranceDetails as? [[String:Any]] {
            
            
            for providers in providerDetail {
                self.insurance["name"] = GenericUtility.strForObj(object: providers["name"])
                
//                self.insurance[SERVICE_RESPONSE.Provider_LName] = GenericUtility.strForObj(object: providers[SERVICE_RESPONSE.Provider_LName])
                
                self.insurance["_id"] = GenericUtility.strForObj(object: providers["_id"])
                
                self.insurance["unselectImage"] = GenericUtility.strForObj(object: providers["unselectImage"])
                
                self.insurance["selectImage"] = GenericUtility.strForObj(object: providers["selectImage"])
                
                self.insurance["status"] = GenericUtility.intForObj(object: providers["status"])
                
//                self.insurance[SERVICE_RESPONSE.lastBooked] = GenericUtility.intForObj(object: providers[SERVICE_RESPONSE.lastBooked])
                
                allInsurances.append(self.insurance)
            }
        }
    }
}

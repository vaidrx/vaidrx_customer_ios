//
//  APIParameterConstants.swift
//  DayRunner
//
//  Created by Raghavendra V on 16/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

enum LoginType: Int {
    
    case Check = 0
    case Default = 1
    case Facebook = 2
    case Google = 3
}

enum BookingType: Int {
    
    case Default = 1
    case Schedule = 2
}

enum HTTPSResponseCodes: Int {
    
    case TokenExpired = 440
    case UserLoggedOut = 498
    case SuccessResponse = 200
    case BadRequest = 400
    case InternalServerError = 500
    
    enum Login:Int {
        
        case WrongPassword = 401
        case EmailNotRegistered = 404
        case SuccessResponse = 200
    }
    
    enum Register:Int {
        
        case WrongPhoneNumber = 406
        case MissingPasswordFBLogin = 410
        case WrongInputData = 412
        case SuccessResponse = 200
    }
    
    enum ChangeEmailMobile:Int {
        
        case EmailMobileAlreadyExist = 412
        case TokenExpired = 440
        case SuccessResponse = 200
    }
    
    enum ForgotPassword:Int {
        
        case WrongPassword = 401
        case EmailMobileNotExist = 404
        case WrongPhoneNumber = 406
        case WrongEmail = 409
        case ToManyRequest = 429
        case SuccessResponse = 200
        
    }
    
    enum SetPassword:Int {
        
        case UserNotExist = 404
        case SuccessResponse = 200
        case TokenExpired = 440
    }
    
    enum MusicianList:Int {
        
        case NoMusicians = 404
    }
}



enum RegisterType: Int {
    
    case Default = 1
    case Facebook = 2
    case Google = 3
}

enum PaymentType: Int {
    
    case SelfPay = 0
    case Insurance = 1
    case CustomInsurance = 2
}

enum DeviceType: Int {
    
    case IOS = 1
    case Android = 2
    case Web = 3
}


struct API
{
    static let BASE_IP              = "https://api.vaidrx.com"
    //"http://138.197.78.50:9999"
    //"http://138.197.78.50"
    
    static let BASE_URL             = "https://api.vaidrx.com/"  //"http://18.188.165.175:9999/"
    
    struct SOCKET {
        
        static let URL = BASE_IP
        static let PORT_NUMBER              = "9999"
        
        struct CHANNEL {
            static let CUSTOMER_STATUS      = "CustomerStatus"
            static let MESSAGE              = "Message"
        }
    }
    
    
    
    //API Method Names
    struct METHOD {
        
        static let Customer                    =   "customer/"
        
        //Master Login
        static let LOGIN                    =   Customer + "signIn"
        
        //Master signup
        static let SIGNUP                   =   Customer + "registerUser"
        
        static let PHARMACY                 =   Customer + "pharmacy"
        
        //Customer getProfile or UpdateProfile
        static let GET_PROFILE              =   PROFILE + "me"
        
        //Customer Music Genres
        static let GET_MUSIC_GENRES         =   Customer + "serviceCateogries"
        
        static let ACCEPT_BOOKING           =   Customer + "acceptBooking" 
        
        static let PHONE_VALIDATION         =   Customer + "phoneNumberValidation"
        
        static let EMAIL_VALIDATION         =   Customer + "emailValidation"
        
        static let GET_PAST_PROVIDERS       =   Customer + "pastProvider"
        
        static let GET_INSURANCE            =   Customer + "insurance"
        
        
        static let GET_ALL_MUSICIANS        =   Customer + "provider"
        
        static let ALL_PROVIDERS_POST       =   Customer + "location"
        
        static let ADDRESS                  =   Customer + "address"
        
        static let LIVE_BOOKING             =   Customer + "booking"
        
        static let PROFILE                  =   Customer + "profile/"
        
        static let CHECKPASSWORD            =   Customer + "checkpassword/"
        
        static let UPDATEPROFILE            =   Customer + "profile/me"
        
        static let CANCELBOOKING            =   Customer + "cancelBooking"
        
        static let GET_MUSICIAN_DETAILS     =   Customer + "providerDetails"
        
        
        static let SIGNUP_RESEND_OTP        =   Customer + "resendOtp"
        
        static let SIGNUP_VERIFYI_OTP       =   Customer + "verifyPhoneNumber"
        
        
        static let GET_BOOKINGS             =   Customer + "bookings"
        
        static let GET_BOOKING_DETAILS      =   Customer + "booking" //Customer/booking/{bookingId}/{num} :- 1:pending job 2:Upcoming job 3:Past job details
        
        static let GET_PENDING_REVIEWS      =   Customer + "reviewAndRatingPending"
        
        
        static let GET_BOOKING_DETAILS_INVOICE =   Customer + "booking/invoice"
        
        static let DisconnectCall   =   Customer + "disconnectBookingCall"
        
        
        
        static let paymentGateway           =   ""//"paymentGateway/"
        
        static let ADD_CARD                 =   paymentGateway + "card"
        
        static let DELETE_CARD              =   paymentGateway + "card"
        
        static let GET_CARDS                =   paymentGateway + "card"//"cards"
        
        static let CARD_DEFAULT             =   paymentGateway + "card"//"card/default"
        
        
        
        
        //        static let App                      =   "app/"
        
        static let CONFIG                   =   Customer + "config"
        
        static let SEND_OTP                 =   Customer + "signupOtp"
        
        
        static let CHANGE_EMAIL             =   Customer + "email"
        
        static let CHANGE_MOBILE            =   Customer + "phoneNumber"
        
        
        
        static let FORGET_PASSWORD          =   Customer + "forgotPassword"
        
        static let RESEND_OTP               =   Customer + "resendOtp"
        
        
        static let FORGET_PASSWORD_VERIFY_OTP   =   Customer + "verifyVerificationCode"
        
        
        static let CHANGE_PASSWORD          =   Customer + "password"
        
        static let UPDATE_PASSWORD          =   Customer + "password/me"
        
        static let ACESS_TOKEN              =   Customer + "accessToken"
        
        
        
        static let LOGOUT                   =   Customer + "logout"
        
        static let RATECARD                 =   Customer + "rateCard"
        
        static let CANCEL_REASONS           =   Customer + "cancelReasons/1"
        
        
        static let SUPPORT                  =   Customer + "support/1"
        
        
        static let thirdParty               =   "thirdParty/"
        
        static let GOOGLE                   =   thirdParty + "google"
        
        static let RIDEFAREESTIMATE         =   thirdParty + "rideFareEstimate"
        
        static let VALIDATE_PROMOCODE       =   Customer + "validatepromocode"
        
        static let VALIDATE_REFERRAL        =   Customer + "referralCodeValidation"
        
        
        static let ADD_ADDRESS              =   Customer + "address"
        
        static let DELETE_ADDRESS           =   Customer + "address"
        
        static let GET_ADDRESS              =   Customer + "address"
        
        static let UPDATE_ADDRESS           =   Customer + "address"
        
        
        static let REVIEWANDRATING          =   Customer + "reviewAndRating"
        
        
        static let REVIEW_AND_RATING_PROVIDER =   Customer + "providerReview"
        
        
        //ZenDesk
        static let ZenDesk                  =   "zendesk/"
        
        static let GetTicketDetails         =   ZenDesk + "user/ticket/"
        
        static let CreateTicket             =   ZenDesk + "ticket"
        
        static let GetTicketHistoryDetails  =   ZenDesk + "ticket/history/"
        
        static let CreateTicketComments     =   ZenDesk + "ticket/comments"
        
        static let GET_DISTANCE_ETA_GOOGLE  = "https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&key=%@"
        
    }
}




struct SERVICE_REQUEST {
    
    //Login & Signup
    static let Email_Phone     = "emailOrPhone" //email or phone number
    static let Password        = "password"
    static let DeviceID        = "deviceId"
    static let PushToken       = "pushToken"
    static let AppVersion      = "appVersion"
    static let DeviceMake      = "devMake"
    static let DeviceModel     = "devModel"
    static let DeviceType      = "devType" //1- IOS , 2- Android, 3- Web
    static let DeviceTime      = "deviceTime"
    static let LoginType       = "loginType" //1- normal login, 2- Fb , 3-google
    static let DeviceOSVersion = "deviceOsVersion"
    static let FaceBookID      = "facebookId"
    static let Change_Password = "password"
    static let New_Password    = "newPassword"
    static let Old_Password    = "oldPassword"
    static let IPAddress       = "ipAddress"
    static let CardToken       = "cardToken"
    static let CategoryId      = "categoryId"
    static let paymentType     = "paymentType"
    static let insurance       = "insurance"
    
    
    //Register
    static let RegisterType    = "loginType" //1- normal login, 2- Fb , 3-google
    static let FirstName       = "firstName"
    static let LastName        = "lastName"
    static let Mobile          = "phone"
    static let CountryCode     = "countryCode"
    static let ZipCode         = "ent_zipcode"
    static let Lat             = "latitude"
    static let Long            = "longitude"
    static let ProfilePic      = "profilePic"
    static let DOB             = "DOB" //YYYY-MM-D
    static let DOB_FORMAT      = "MM-dd-yyyy" //dddd
    static let FacebookId      = "facebookId"
    static let GoogleId        = "googleId"
    static let TermsAndCondition = "termsAndCond" //0 - false,1 - true
    static let ReferralCode    = "referralCode"
    static let AddLine1 = "addLine1"
    static let PharmacyId = "pharmacyId"
    static let TaggedAs = "taggedAs"
    static let PlaceName = "placeName"
    static let AddLine2 = "addLine2"
    static let City = "city"
    static let State = "state"
    static let Country = "country"
    static let PlaceId = "placeId"
    static let Pincode = "pincode"
    
    
    //Edit profile
    static let About           = "about"
    static let Genres          = "preferredGenres"
    static let PaymentType     = "paymentType"
    
    
    //RxToday
    static let OldPharmacyAddLine1 = "oldPharmacyAddLine1"
    static let OldPharmacyPhone = "oldPharmacyPhone"
    static let OldPharmacyCountryCode = "oldPharmacyCountryCode"
    static let OldPharmacyCheck = "oldPharmacyCheck"
    static let oldPharmacyTransferCheck = "oldPharmacyTransferCheck"
    static let OldPharmacyCity = "oldPharmacyCity"
    static let OldPharmacyState = "oldPharmacyState"
    static let OldPharmacyCountry = "oldPharmacyCountry"
    static let OldPharmacyPlaceId = "oldPharmacyPlaceId"
    static let OldPharmacyPincode = "oldPharmacyPincode"
    static let OldPharmacyLatitude = "oldPharmacyLatitude"
    static let OldPharmacyLongitude = "oldPharmacyLongitude"
    static let RefillCurrentMedications = "refillCurrentMedications"
    

    //Pharmacy
    static let Latitude        = "lat"
    static let Longitude       = "long"
    static let IpAddress       = "ipAddress"
    static let Authorization   = "authorization"
    
    
    //Validate Email & Phone
    static let validationType       = "validationType" //1- email, 2- mobile
    
    //Validate Email & Phone
    struct Validation {
        static let Country_Code       = "countryCode"
        static let Phone              = "phone"
        static let Email              = "email"
        static let trigger         = "trigger" //1 - signup,2 - change number
    }
    
    struct Profile {
        
        static let Password        = "password"
        static let Email           = "email"
        static let Mobile          = "mobile"
        static let Token           = "token"
        static let ProfilePic      = "profilePic"
        static let Name            = "fName"
    }
    struct UpdateProfile{
        static let authorization = "authorization"
        static let profilePic = "profilePic"
        static let paymentType = "paymentType"
        static let about = "about"
        
    }
    
    
    struct validatePhoneNumber {
        static let Email           = "email"
        static let mobile          = "mobile"
    }
    
    
    struct OTP {
        static let Email_Phone     = "emailOrPhone"
        static let mobile          = "mobile"
        static let userType        = "userType" //1- Customer , 2- Master
        static let processType     = "processType" //1- forgotpassword , 2- Normal Signup & change number
        static let code            = "code"
        static let CountryCode     = "countryCode"
        static let userId          = "userId"
        static let trigger         = "trigger" //1 - Register,2 - Forgot Password,3-change number
        
    }
    
    
    struct Referal {
        
        static let Code            = "code"
        static let UserType        = "userType"
        static let Lat             = "lat"
        static let Long            = "long"
    }
    
    
    struct ForgetPassword {
        
        static let Email_Phone     = "emailOrPhone" //email or phone number
        static let userType        = "userType" //1- Customer , 2- Master
        static let processType     = "type" //1- mobile , 2- email
    }
    
    
    struct Payment {
        
        static let cardToken       = "cardToken"
        static let cardId          = "cardId"
        static let email           = "email"
    }
    
    
    struct Address {
        
        static let addressId       = "id"
        static let address1        = "addLine1"
        static let address2        = "addLine2"
        static let city            = "city"
        static let state           = "state"
        static let country         = "country"
        static let pincode         = "pincode"
        static let latitude        = "latitude"
        static let longitude       = "longitude"
        static let taggedAs        = "taggedAs"
        static let userType        = "userType" //1- Customer , 2- Master
    }
    
    struct GetMusician {
        
        static let latitude       = "lat"
        static let longitude      = "long"
        static let bookingType    = "bookingType" //1- now , 2- scheduled
    }
    
    
    struct LiveBooking {
        
        static let bookingModel    = "bookingModel" //1- Market Place, 2- on-demand , 3-bid
        static let bookingType     = "bookingType" //1- now , 2- scheduled
        static let paymentMethod   = "paymentMethod" //1- cash, 2- card, 3-wallet
        static let address1        = "addLine1"
        static let address2        = "addLine2"
        static let city            = "city"
        static let state           = "state"
        static let country         = "country"
        static let pincode         = "pincode"
        static let latitude        = "latitude"
        static let longitude       = "longitude"
        static let providerId      = "providerId"
        static let gigTimeId       = "gigTimeId"
        static let eventId         = "eventId"
        static let promoCode       = "promoCode"
        static let paymentCardId   = "paymentCardId"
        static let ipAddress       = "ipAddress"
        static let ageType         = "ageType"
        static let bookingMode     = "bookingMode"
    }
    
    struct SUBMIT_REVIEW {
        
        static let bookingId       = "bookingId"
        static let ratingValue     = "rating"
        static let reviewText      = "review"
    }
    
    struct CANCEL_BOOKING {
        
        static let bookingId       = "bookingId"
        static let reasonId        = "resonId"
    }
    
    struct ACCEPT_TODAY_BOOKING {
        static let bookingId       = "bookingId"
    }
    
    struct ZENDESK {
        
        static let ticketId        = "id"
        static let ticketCommentBody = "body"
        static let ticketAuthorId  = "author_id"
        static let ticketSubject   = "subject"
        static let ticketStatus    = "status"
        static let ticketPriority  = "priority"
        static let ticketType      = "type"
        static let ticketGroupId   = "group_id"
        static let ticketRequesterId  = "requester_id"
        static let ticketAssignerId   = "assignee_id"
    }
    
}

struct SERVICE_RESPONSE {
    
    static let Error               = "error"
    static let ErrorMessage        = "message"
    
    static let FAQTileName         = "Name"
    static let FAQTileLink         = "link"
    static let FAQSubCat           = "subcat"
    
    static let DataResponse        = "data"
    
    //MyProfile Service Response
    static let About               = "about"
    static let Dob                 = "DOB"
    static let Genres              = "preferredGenres"
    static let Photo               = "profilePic"
    static let CountryCode         = "countryCode"
    static let PaymentType         = "paymentType"
    static let Insurance           = "insurance"
    static let insuranceName       = "insuranceName"
    static let insuranceId         = "insuranceId"
    static let AddLine1            = "addLine1"
    static let PharmacyName        = "pharmacyName"
    static let Latitude            = "latitude"
    static let Longitude           = "longitude"
    static let AddLine2            = "addLine2"
    
    
    //PastProviders
    static let lastBooked =  "lastBookTs"
    static let Provider_Email =  "providerEmail"
    static let Provider_FName =  "providerFirstName"
    static let Provider_Id =  "providerId"
    static let Provider_LName =  "providerLastName"
    static let Provider_Pic =  "providerProfilePic"
    static let Provider_BookingNum =  "totalNumberOfBooking"
    
    //Login & signup Service Response
    static let Sid                 = "sid"
    static let Email               = "email"
    static let PhoneNumber         = "phone"
    static let SessionToken        = "token"
    static let ReferralCode        = "referralCode"
    static let CurrencyCode        = "currencyCode"
    static let ProfilePic          = "profilePic"
    static let FirstName           = "firstName"
    static let LastName            = "lastName"
    static let StripeAPIKey        = "PublishableKey"
    static let FCMTopic            = "fcmTopic"
    static let ZenDeskRequesterID  = "requester_id"
    static let PharmacyId          = "pharmacyId"
    
    
    
    //Token
    static let Token               = "token"
    
    //Music Genres
    static let Genres_Name         = "catName"
    static let Genres_Id           = "id"
    
    //Review
    static let reviews             = "reviews"
    static let reviewCount         = "reviewCount"
    static let averageRating       = "averageRating"
}


//MQTT Constants
struct MQTT_TOPIC {
    
    static let ListAllProviders    = "provider/"
    static let GetJobStatus        = "jobStatus/"
    static let ProviderLiveTrack   = "liveTrack/"
    static let FCMTopic            = "/topics/"
    static let ChatMessageTopic    = "message/"
}

//WebService Request Types
enum RequestType : Int {
    case updateEmail
    case updateMobile
    
    case updateAppVersion
    case verifyMobileNumber
    case verifyReferralCode
    case verifyEmail
    case sendOTP
    case signUpOTP
    case verifyOTP
    case forgotPasswordVerifyOTP
    case signUp
    case signIn
    case getAllApptDetails
    case getAllOngoingApptDetails
    case getParticularApptDetails
    case getInvoiceDetails
    case getChatMessages
    case sendChatMessages
    case updateReview
    case getProfileData
    case updateProfileData
    case forgetPassword
    case forgotPasswordVerifyPhoneNumber
    case changePassword
    case signOut
    case liveBooking
    case getCancelReasons
    case cancelBooking
    case addCard
    case cardDefault
    case getCardDetails
    case deleteCard
    case checkCoupon
    case addAddress
    case deleteAddress
    case GetAddress
    case UpdateAddress
    case GetSupportDetails
    case ChangeBookingStatus
    case musicGenres
    case refressAccessToken
    case updatePassword
    case getAllMusicians
    case getProviderDetails
    case reviewAndRating
    case GetReviews
    case GetDistanceAndETA
    case GetPendingReviews
    case PastProviders
    case GetInsurances
    case AcceptingBooking
    case providerRating
    case ResendOTP
    case getPharmacy
    case confirmPharmacy
    case transferMedications
    case UpdateInsurance
    case RefillCurrentMedications
    case disconnectBookingCall
//    case UpdateAddress
}


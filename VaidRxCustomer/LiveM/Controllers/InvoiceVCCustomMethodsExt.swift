//
//  InvoiceVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension InvoiceViewController {
    
    func showBookingDetails() {
        
        ratingView.emptyImage = #imageLiteral(resourceName: "review_star_grey_icon")
        ratingView.fullImage = #imageLiteral(resourceName: "review_star_red_icon")
        ratingView.floatRatings = true
        
        
        let startDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.bookingStartTime))
        
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "d MMM yyyy"
        eventDateLabel.text = dateFormat.string(from: startDate)
        
        
        amountLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol ,Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))!)
        
        
        ratingView.rating = 5.0
        
        musicianNameLabel.text = bookingDetailModel.providerName
        
        
        if bookingDetailModel.providerImageURL.length > 0 {
            
            activityIndicator.startAnimating()
            
            musicianImageView.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        
    }
    
    
}

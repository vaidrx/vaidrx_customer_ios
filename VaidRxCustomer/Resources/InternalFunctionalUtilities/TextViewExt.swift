//
//  TextViewExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 28/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

class MyTextView : UITextView {
    override func caretRect(for position: UITextPosition) -> CGRect {
        var superRect = super.caretRect(for: position)
        guard let isFont = self.font else { return superRect }
        
        superRect.size.height = isFont.pointSize - isFont.descender
        // "descender" is expressed as a negative value,
        // so to add its height you must subtract its value
        
        return superRect
    }

}

//
//  BookingFlowTimerMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension BookingFlowViewController {
    
    func showBookingTimerDetails() {
        
        extendTimeButton.layer.borderColor = UIColor.white.cgColor
        extendTimeButton.layer.borderWidth = 1.2
        
        timerGigTimeLabel.text = "YOUR GIG TIME IS \(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["name"])) \(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["unit"]))"
        
        bookingTimer.invalidate()
        
        
        if bookingDetailModel.timerStatus == 1 {
            
            remainingSeconds =  GenericUtility.intForObj(object: bookingDetailModel.gigTimeDict["second"]) - GenericUtility.intForObj(object: bookingDetailModel.totalTimeElapsed)
            
            let startTime = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.timerStamp))
            
            var elapsedSeconds = Date().timeIntervalSince(startTime)
            
            if elapsedSeconds < 0 {
                
                elapsedSeconds = elapsedSeconds * -1
            }
            
            remainingSeconds = remainingSeconds - Int(elapsedSeconds)
            
            
            if remainingSeconds > 0 {
                
                showRemainingTimeDetails()
                scheduleBookingTimer()
                
            } else {
                
                remainingSeconds = 0
                bookingTimer.invalidate()
                showRemainingTimeDetails()
                
            }
            
            pausedLabel.isHidden = true
            
        } else {
            
            remainingSeconds =  GenericUtility.intForObj(object: bookingDetailModel.gigTimeDict["second"]) - GenericUtility.intForObj(object: bookingDetailModel.totalTimeElapsed)
            
            pausedLabel.isHidden = false
            
            
            if remainingSeconds > 0 {
                
                showRemainingTimeDetails()
                
            } else {
                
                remainingSeconds = 0
                showRemainingTimeDetails()
                
            }
            
        }
        
        showBookingTimerDetailsViewAnimation()
        
    }
    
    
    func scheduleBookingTimer(){
        
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        bookingTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                            target: self,
                                            selector: #selector(self.updateRemainingTimeDetails),
                                            userInfo: nil,
                                            repeats: true)
    }
    
    
    @objc func updateRemainingTimeDetails(){
        
        remainingSeconds -= 1
        
        if remainingSeconds <= 0 {
            
            bookingTimer.invalidate()
            
        }
        
        showRemainingTimeDetails()
        
    }
    
    
    func showRemainingTimeDetails() {
        
        let minute:Int = remainingSeconds/60
        let seconds:Int = remainingSeconds - (minute * 60)
        
        var minuteString = ""
        var secondsString = ""
        
        if minute < 10 {
            
            minuteString = "0\(minute)"
            
        } else {
            
            minuteString = "\(minute)"
        }
        
        if seconds < 10 {
            
            secondsString = "0\(seconds)"
            
        } else {
            
            secondsString = "\(seconds)"
        }
        
        timerLabel.text = minuteString + ":" + secondsString
        
    }
    
    
    func showBookingTimerDetailsViewAnimation() {
        
        timerBackViewTopConstraint.constant = 0
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 3,
                       options: [],
                       animations: {
                        
                        self.timerBackView.layoutIfNeeded()
                        
        }, completion: { (completed) in
            
            
        })
        
        
    }
    
    
    func hideBookingTimerDetailsViewAnimation() {
        
        timerBackViewTopConstraint.constant = -100
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 3,
                       options: [],
                       animations: {
                        
                        self.timerBackView.layoutIfNeeded()
                        
        }, completion: { (completed) in
            
            
        })
        
        
    }
    
}

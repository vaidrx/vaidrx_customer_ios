 //
//  PastPharmacyInfoViewController.swift
//  GoTasker
//
//  Created by 3Embed on 29/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
 import RxSwift
 import RxCocoa

 class PastPharmacyInfoViewController: UIViewController, AddMyAddressFromMapToModel {
    
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var transferMedicationBtn: UIButton!
    @IBOutlet weak var phoneNumberTF: UITextField!
//    @IBOutlet weak var oldPharmacyInfoTF: UITextField!
    @IBOutlet weak var oldPharmacyInfoTF: UILabel!
    
    @IBOutlet weak var acceptButton: UIButton!
    
    @IBOutlet weak var currentPharAddressLbl:UILabel!
    @IBOutlet weak var currentPharAddressView:UIView!
    
    @IBOutlet weak var orLbl:UILabel!
    
     @IBOutlet weak var currentPharMobileLbl:UILabel!
     @IBOutlet weak var currentPharMobileView:UIView!
    @IBOutlet weak var currentpharAddressBottomView:UIView!
    
    var activeTextField = UITextField()
    var pastPharmViewModel = PastPharmViewModel()
    var isAccepted = "0"
    var disposeBag = DisposeBag()
    var oldPharmacyTransferCheck = 0
    let profileViewModel = ProfileViewModel()
    var rxTodayModel:RxTodayModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideNavigation()
        self.navigationController?.navigationBar.isHidden = false
        initialsSetUp()
        addDoneButtonOnKeyboard()
        transferMedicationBtn.layer.cornerRadius = transferMedicationBtn.frame.height / 2
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    
   
    func hideNavigation() {
//
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.barTintColor = APP_COLOR
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = .clear //UIColor(hex: "05C1C9") //UIColor.clear
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        // Mark: - Customize toolbar color
        doneToolbar.isTranslucent = false
        doneToolbar.tintColor = .white
        doneToolbar.barTintColor = UIColor(hex: "05C1C9")
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        phoneNumberTF.autocorrectionType = .no
        phoneNumberTF.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        phoneNumberTF.resignFirstResponder()
    }
    func initialsSetUp(){
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            
            DDLogDebug(countryCode)
            let picker = CountryNameViewController()
            picker.initialSetUp()
            let contry: Country
            contry = picker.localCountry(countryCode)
            countryCodeLabel.text = contry.dial_code
            let imagestring = contry.country_code
            
            print("country code",countryCodeLabel.text!)
            let imagePath = "CountryPicker.bundle/\(imagestring).png"
            flagImage.image = UIImage(named: imagePath)
        }
    }
    @IBAction func tapGetureAction(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func addressButtonAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVc = storyboard.instantiateViewController(withIdentifier: "AddNewAddressVC") as! AddNewAddressViewController
        nextVc.addMyAddressDelegate = self
        //nextVc.registerViewModel = self.registerViewModel
        //        let storyboard = UIStoryboard(name: "vaiDRx_Main", bundle: nil)
        //        let nextVc = storyboard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddMyAddressViewController
        nextVc.isComingFrom = "oldPharmacyInfo"
        self.navigationController?.pushViewController(nextVc, animated: false)
//        self.present(nextVc, animated: true, completion: nil)
    }
    
    
    //delegate methods to get address
    func addMyAddress(address: String) {
        //
    }
    
    func addFullAddress(fulladdress: [String : Any]) {
        print("fulladdress",fulladdress)
        self.oldPharmacyInfoTF.text = fulladdress["address"] as? String
        
        pastPharmViewModel.longitude = fulladdress["logitude"] as! Double
        pastPharmViewModel.latitude = fulladdress["latitude"] as! Double
        pastPharmViewModel.addLine1 = fulladdress["address"] as! String
        pastPharmViewModel.taggedAs = fulladdress["taggedAs"] as! String
        pastPharmViewModel.city = fulladdress["city"] as! String
        pastPharmViewModel.state = fulladdress["state"] as! String
        pastPharmViewModel.country = fulladdress["country"] as! String
        pastPharmViewModel.pincode = fulladdress["pincode"] as! String
    }
    
    func addCorrectAddress(correctAddress: AddressModel, taggedAs: String) {
        print("address contailn model",correctAddress)
        
        pastPharmViewModel.latitude = Double(correctAddress.latitude)
        pastPharmViewModel.longitude = Double(correctAddress.longitude)
        pastPharmViewModel.addLine1 = correctAddress.addressLine1
//            + correctAddress.addressLine2
        pastPharmViewModel.city = correctAddress.city
        pastPharmViewModel.state = correctAddress.state
        pastPharmViewModel.pincode = correctAddress.zipcode
        pastPharmViewModel.country = correctAddress.country
        pastPharmViewModel.placeName = correctAddress.addressLine1
        pastPharmViewModel.taggedAs = taggedAs
        pastPharmViewModel.placeId = correctAddress.addressId
        pastPharmViewModel.country = correctAddress.country
        //        profileViewModel.placeId = correctAddress.
        
        self.oldPharmacyInfoTF.text = correctAddress.addressLine1
//            + correctAddress.addressLine2
//        serviceRequestToUpdateProfile()
        
        print("got full address")
    }
    

    @IBAction func countryCodeButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle:nil)
        let countryVc = storyBoard.instantiateViewController(withIdentifier:"countryNameVC") as! CountryNameViewController
        countryVc.countryDelegate = self
        self.navigationController?.pushViewController(countryVc, animated: true)
    }
    
    @IBAction func tickButtonAction(_ sender: UIButton) {
        
        if isAccepted == "0"{
            acceptButton.setImage(UIImage(named: "accept"), for: .normal)
            print("button tapped")
            isAccepted = "1"
            currentPharAddressLbl.alpha = 0.4
            currentPharAddressView.alpha = 0.4
            orLbl.alpha = 0.4
            currentPharMobileLbl.alpha = 0.4
            currentPharMobileView.alpha = 0.4
            currentpharAddressBottomView.alpha = 0.4
            phoneNumberTF.text = ""
//            pastPharmViewModel.phoneNumberText.value = ""
            oldPharmacyInfoTF.text = ""
            currentPharMobileView.isUserInteractionEnabled = false
            currentPharAddressView.isUserInteractionEnabled = false
            
        }
        else{
            acceptButton.setImage(UIImage(named: "reject"), for: .normal)
            isAccepted = "0"
            currentPharAddressLbl.alpha = 1.0
            currentPharAddressView.alpha = 1.0
            orLbl.alpha = 1.0
            currentPharMobileLbl.alpha = 1.0
            currentPharMobileView.alpha = 1.0
            currentpharAddressBottomView.alpha = 1.0
            currentPharMobileView.isUserInteractionEnabled = true
            currentPharAddressView.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func transferMedicationsButtonAction(_ sender: UIButton) {
        
        if isAccepted == "1" {
            updatePharmacyData()
        } else {
            if phoneNumberTF.text?.length == 0 {
                            Helper.showAlert(head: "Alert", message: "Enter your current pharmacy phone number")
                        }
                        else{
                            updatePharmacyData()
                        }
        }
        

    }
   
    @IBAction func tappedBack(_ sender: UIBarButtonItem) {
   self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    func addObserverVariables(){
//        oldPharmacyInfoTF.rx.text
//            .orEmpty
//            .bind(to: pastPharmViewModel.oldPharmacyAddText )
//            .disposed(by: disposeBag)

        phoneNumberTF.rx.text
            .orEmpty
            .bind(to: pastPharmViewModel.phoneNumberText )
            .disposed(by: disposeBag)
    }
    
    // MARK: - to move view up with keyboard
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeKeyboardObserveres()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addObserverVariables()
        addKeyBoardObserver()
    }
    
    private func removeKeyboardObserveres() {
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.init(UIResponder.keyboardWillShowNotification.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.init(UIResponder.keyboardWillHideNotification.rawValue), object: nil)
    }
    
    func addKeyBoardObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: Notification.Name.init(UIResponder.keyboardWillShowNotification.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: Notification.Name.init(UIResponder.keyboardWillHideNotification.rawValue), object: nil)
    }
    /*
     @objc func keyboardWillShow(notification: NSNotification) {
     
     //Need to calculate keyboard exact size due to Apple suggestions
     var info = notification.userInfo!
     let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
     let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize!.height + 60), 0.0)
     
     self.myScrollView.contentInset = contentInsets
     self.myScrollView.scrollIndicatorInsets = contentInsets
     
     }*/
    @objc func keyBoardWillShow(notification: NSNotification) {
        
        if let keyBoardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]  as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyBoardSize.height, right: 0)
            myScrollView.contentInset = contentInset
            myScrollView.scrollIndicatorInsets = contentInset
            //Scroll upto visible rect
            var visisbleRect = self.view.frame
            visisbleRect.size.height -= keyBoardSize.height
            if !visisbleRect.contains((activeTextField.center)) {
                myScrollView.scrollRectToVisible(visisbleRect, animated: true)
            }
        }
        
    }
    
    //Mark : - Called before Keyboard hides
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            self.myScrollView.contentInset = contentInsets
            self.myScrollView.scrollIndicatorInsets = contentInsets
        }
    }
 }

extension PastPharmacyInfoViewController : CountrySelectedDelegate {
    func countryNameSelected(countrySelected country: Country) {
        // update country code and flag here.
        //imgCountryImage.image = country
        countryCodeLabel.text = country.dial_code
        //self.selectedCountry = country
        //DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        let imagestring = country.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        flagImage.image = UIImage(named: imagePath)
    }
}

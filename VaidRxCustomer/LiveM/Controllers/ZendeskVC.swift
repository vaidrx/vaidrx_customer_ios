//
//  ZendeskVC.swift
//  Zendesk
//
//  Created by Vengababu Maparthi on 26/12/17.
//  Copyright © 2017 Vengababu Maparthi. All rights reserved.
//

import UIKit

class ZendeskVC: UIViewController {
    @IBOutlet weak var zendeskTableView: UITableView!
    
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var firstLetter: UILabel!
    @IBOutlet weak var currentTime: UILabel!
    @IBOutlet weak var myName: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var moreImage: UIImageView!
    @IBOutlet weak var priorityColorLabel: UILabel!
    @IBOutlet weak var addSubject: UITextField!
    @IBOutlet weak var priorityButton: UIButton!
    @IBOutlet weak var bottomContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var heightOfTheTextView: NSLayoutConstraint!
    
    @IBOutlet weak var navigationLeftButton: UIButton!
    
    var comments = [String]()
    var zendeskViewModel = ZendeskModel()
    var ticketHistoryData = historyModel()
    var ticketID:Int64 = 0
    var curserOnTextfield = false
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.zendeskTableView.rowHeight = UITableView.automaticDimension
        self.zendeskTableView.estimatedRowHeight = 80.0
        firstLetter.text =  String(Utility.firstName.first!)
        myName.text = Utility.firstName + " " + Utility.lastName
        
        headerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        headerView.layer.shadowColor = UIColor.lightGray.cgColor
        headerView.layer.shadowRadius = 1
        headerView.layer.shadowOpacity = 0.75
        let shadowFrame: CGRect = headerView.layer.bounds
        let shadowPath = (UIBezierPath(rect: shadowFrame).cgPath)
        headerView.layer.shadowPath = shadowPath
        
        priorityButton.setTitle("normal", for: .normal)
        priorityColorLabel.backgroundColor = UIColor.green
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupGestureRecognizer()
        
        self.currentTime.text = TimeFormats.currentDateWithoutTime
        
        if ticketID != 0 {
            arrowImage.isHidden = true
            zendeskViewModel.getTheTicketsHistory(userID: String(describing:ticketID)) { (success, ticketsData) in
                if success{
                    self.ticketHistoryData = ticketsData
                    self.addSubject.text = self.ticketHistoryData.subject
                    self.priorityButton.setTitle(self.ticketHistoryData.priority, for: .normal)
                    self.priorityButton.isEnabled = false
                    self.addSubject.isUserInteractionEnabled = false
                    switch self.ticketHistoryData.priority{
                    case "urgent":
                        self.priorityColorLabel.backgroundColor = UIColor.red
                        break
                    case "high":
                        self.priorityColorLabel.backgroundColor = UIColor.blue
                        break
                    case "normal":
                        self.priorityColorLabel.backgroundColor = UIColor.green
                        break
                    case "low":
                        self.priorityColorLabel.backgroundColor = UIColor.yellow
                        break
                    default:
                        self.priorityColorLabel.backgroundColor = UIColor.blue
                        break
                    }
                    
                    if self.ticketHistoryData.status == "solved"{
                        self.bottomContainerConstraint.constant =  -50
                    }
                    self.zendeskTableView.reloadData()
                }
            }
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ZendeskVC.showKeyboard),
                                               name:Notification.Name.init(UIResponder.keyboardDidShowNotification.rawValue),
                                               object: nil);
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ZendeskVC.keyboardDidShow),
                                               name:Notification.Name.init(UIResponder.keyboardDidShowNotification.rawValue),
                                               object: nil);
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ZendeskVC.keyboardWillHide),
                                               name:Notification.Name.init(UIResponder.keyboardWillHideNotification.rawValue),
                                               object: nil);
    }
    
    
    // MARK: Keyboard Dodging Logic
    //MARK: NotificationCenter handlers
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            if !curserOnTextfield{
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    self.bottomContainerConstraint.constant = height
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        self.scrollToBottomMessage()
    }
    
    // Scroll to bottom of table view for messages
    func scrollToBottomMessage() {
        if self.ticketHistoryData.comments.count == 0 {
            return
        }
        let bottomMessageIndex = IndexPath(row: self.ticketHistoryData.comments.count - 1, section: 0)
        zendeskTableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
    }
    
    @IBAction func gestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func backToVc(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomContainerConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    
    @IBAction func sendTheText(_ sender: Any) {
        // creating new ticket
        if self.ticketHistoryData.comments.count == 0{
            self.view.endEditing(true)
            
            if textView.text == "Type message here.."{
                
                Helper.alertVC(title: ALERTS.Message, message: "please add the comment to this ticket")
                
            }else if addSubject.text?.length == 0{
                
                Helper.alertVC(title: ALERTS.Message, message: "please add the subject to this ticket")

            }else if priorityButton.titleLabel?.text?.length == 0{
                
                Helper.alertVC(title: ALERTS.Message, message: "please add the priority to this ticket")

            }
            else{
                let ticketData = NewTicketRequest.init(sub: addSubject.text!, comments: textView.text!, status: "open", ticketType: "problem", priority: priorityButton.titleLabel!.text!, req_id: Utility.zendeskRequesterId)
                
                zendeskViewModel.postTheNewTicket(newTicketData: ticketData, completionHandler: { (success) in
                    self.backToVc(self.navigationLeftButton)
                })
            }
        }else{
            if textView.text == "Type message here.."{
                
                Helper.alertVC(title: ALERTS.Message, message: "please add the comment to this ticket")

            }else{
              
                // adding comment to the exisiting ticket
                let newTicket = ticketHistory()
                newTicket.comment = textView.text!
                newTicket.firstLetter = String(Utility.firstName.first!)
                newTicket.timeStamp = Int64(Date().timeIntervalSince1970)
                newTicket.name = Utility.firstName
                self.ticketHistoryData.comments.append(newTicket)
                self.zendeskTableView.reloadData()
                
                let params:[String:Any] = ["id":String(describing:ticketID),
                                           "body":textView.text! as Any,
                                           "author_id":Utility.zendeskRequesterId]
                self.textView.text = ""
                self.heightOfTheTextView.constant = 40
                zendeskViewModel.postTheNewTicketComment(params: params, completionHandler: { (success) in
                    if success{
                        self.scrollToBottomMessage()
                    }
                })
            }
        }
    }
    
    
    
    @IBAction func selectTheTicketPrority(_ sender: Any) {
        self.view.endEditing(true)
        let priorityView = PriorityView.instance
        priorityView.delegate = self
        priorityView.show()
        arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi ));
    }
    
    func adjustFrames() {
        heightOfTheTextView.constant = textView.contentSize.height
    }
}

extension ZendeskVC:selectProrityDelegate{
    func selectedPriority(priority: String,color:UIColor) {
        priorityButton.setTitle(priority, for: .normal)
        priorityColorLabel.backgroundColor = color
        arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi - 3.14159));
    }
    
    func  hideTheView() {
        arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi - 3.14159));
    }
}


///****** tableview datasource*************//
extension ZendeskVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ticketHistoryData.comments.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"chat") as! ZendeskTableCell?
        
        cell?.firstLetyter.text = self.ticketHistoryData.comments[indexPath.row].firstLetter
        cell?.name.text = self.ticketHistoryData.comments[indexPath.row].name
        cell?.comment.text = self.ticketHistoryData.comments[indexPath.row].comment
        cell?.time.text = Helper.getTheScheduleViewTimeFormat(timeStamp: self.ticketHistoryData.comments[indexPath.row].timeStamp)
        cell?.date.text = Helper.getTheOnlyDateFromTimeStamp(timeStamp: self.ticketHistoryData.comments[indexPath.row].timeStamp)
        
        return cell!
    }
}

///***********tableview delegate methods**************//
extension ZendeskVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ZendeskVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type message here.."{
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type message here.."
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            if let text = self.textView.text {
                if text.length > 0 {
                    self.sendTheText("")
                }
            }
            textView.resignFirstResponder()
            return false
        }
        adjustFrames()
        return true;
    }
}

extension ZendeskVC:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        curserOnTextfield = true
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        curserOnTextfield = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        curserOnTextfield = false
        self.view.endEditing(true)
        return true
    }
}

extension ZendeskVC: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension ZendeskVC {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}


//
//  GetBookingUpdatesMQTTModel.swift
//  LiveM
//
//  Created by Raghavendra V on 07/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

//Booking Statuses
enum Booking_Status: Int {
    
    case Pending = 1
    case Requested = 2
    case Accepted = 3
    case Declined = 4
    case IgnoreOrExpire = 5
    case Ontheway = 6
    case Arrived = 7
    case Started = 8
    case Completed = 9
    case Raiseinvoice = 10
    case CancelByProvider = 11
    case CancelByCustomer = 12
    case Cancel = 13
}

protocol BookingFlowDelegate {
    
    func didReceiveBookingAcception()
}


class BookingStatusResponseManager {
    
    var arrayOfOngoingBookings:[Any] = []
    var bookingId:Int64 = 0
    var bookingStatus = 0
    var navigationVC:UINavigationController!
    var bookingAlertController:UIAlertController!
    var bookingFlowVC:BookingFlowViewController!
    var arrayOfPendingInvoices:[Int64] = []
    var arrayOfPendingBookingStatus:[BookingStatusResponseModel] = []
    var delegate: BookingFlowDelegate? = nil
    
    static var obj:BookingStatusResponseManager? = nil
    
    class func sharedInstance() -> BookingStatusResponseManager {
        
        if obj == nil {
            
            obj = BookingStatusResponseManager()
        }
        
        return obj!
    }
    
    
    /// Method to maintain Booking status messages from push or mqtt
    ///
    /// - Parameters:
    ///   - isFromPush: Boolean value for message from push or mqtt
    ///   - bookingStatusModel: BookingStatus response model
    func bookingStatusMessageFromPushOrMQTT(isFromPush:Bool, bookingStatusModel:BookingStatusResponseModel) {
        
        bookingStatus = 0;
        bookingId = bookingStatusModel.bookingId
        
        if let savedDetails = UserDefaults.standard.object(forKey: USER_DEFAULTS.BOOKINGS.ON_GOING_BOOKINGS) {
            
            arrayOfOngoingBookings =  savedDetails as! [Any]
            
        } else {
            
            arrayOfOngoingBookings = []
        }
        
        
        let predicate = NSPredicate(format: "bookingId = %@", String(bookingId))
        let result = arrayOfOngoingBookings.filter { predicate.evaluate(with: $0) }
        
        if result.count == 0 {//Booking id is already available in arrayOfOngoingBookings or not
            
            print("Adding New Booking Staus")
            
            let dict = [
                "bookingId": String(bookingId),
                "status": bookingStatusModel.bookingStatus
                ] as [String : Any]
            
            //Adding New Booking Details To List
            arrayOfOngoingBookings.append(dict)
            UserDefaults.standard.set(arrayOfOngoingBookings, forKey: USER_DEFAULTS.BOOKINGS.ON_GOING_BOOKINGS)
            UserDefaults.standard.synchronize()
            
        } else {
            
            //Updating Booking Status
            for i in 0..<arrayOfOngoingBookings.count {
                
                if let bookingDict:[String:Any] = arrayOfOngoingBookings[i] as? [String:Any] {
                    
                    if bookingDict["bookingId"] as? String == String(bookingId) {
                        
                        bookingStatus = bookingDict["status"] as! Int
                        
                        if bookingStatus != bookingStatusModel.bookingStatus {
                            
                            print("Updated Booking Staus")
                            
                            arrayOfOngoingBookings.remove(at: i)
                            
                            let dict = [
                                "bookingId": String(bookingId),
                                "status": bookingStatusModel.bookingStatus
                                ] as [String : Any]
                            
                            //Booking Status has Changed
                            arrayOfOngoingBookings.append(dict)
                            UserDefaults.standard.set(arrayOfOngoingBookings, forKey: USER_DEFAULTS.BOOKINGS.ON_GOING_BOOKINGS)
                            UserDefaults.standard.synchronize()
                        }
                        break
                    }
                    
                }
                
            }
            
        }
        
        print(bookingStatus)
        print(bookingStatusModel.bookingStatus)
        print(bookingStatusModel.bookingStatus)
        print(Booking_Status.Started.rawValue)
        
//        if bookingStatus != bookingStatusModel.bookingStatus || bookingStatusModel.bookingStatus == Booking_Status.Started.rawValue {
            
            if SplashLoading.obj != nil {//Checking currently showing Splash loading or not
                
                var isNewBooking = true
                
                for index in 0..<arrayOfPendingBookingStatus.count {
                    
                    if arrayOfPendingBookingStatus[index].bookingId == bookingId {
                        
                        arrayOfPendingBookingStatus[index] = bookingStatusModel
                        isNewBooking = false
                        break
                    }
                }
                
                if isNewBooking {
                    
                    arrayOfPendingBookingStatus.append(bookingStatusModel)
                }
                
                
            } else {
                
                self.showBookingFlowMessages(bookingStatusModel: bookingStatusModel)
            }
            
//        }
        
    }
    
    
    
    /// Show Pending booking statuses after Splash loading
    func showPendingBookingStatusAfterSplashLoading() {
        
        for i in 0..<arrayOfPendingBookingStatus.count {
            
            bookingId = arrayOfPendingBookingStatus[i].bookingId
            self.showBookingFlowMessages(bookingStatusModel: arrayOfPendingBookingStatus[i])
        }
        
        arrayOfPendingBookingStatus = []
        
        showPendingReviewsAfterSplashLoading()
        
    }
    
    
    /// Method to show respective booking status messages
    ///
    /// - Parameter bookingStatusModel: booking status response model
    /// 1: requestl, 2: received, 3: accept, 4: reject, 5: ignore/Expire, 6- OntheWay , 7- Arrived, 8- started, 9- raise invoice, 10- completed, 11 - cancel by provider, 12 - cancel by customer
    
    func showBookingFlowMessages(bookingStatusModel:BookingStatusResponseModel) {
        
        
        
        bookingStatus = bookingStatusModel.bookingStatus
        
        
        print("Showing Updated Booking Staus")
        
        switch bookingStatus {
            
        case Booking_Status.CancelByProvider.rawValue://Declined by Musician
            
            Helper.showAlert(head: ALERTS.Message, message: bookingStatusModel.bookingStatusMessage)
            
            if Helper.getCurrentVC() is BookingFlowViewController {
                
                bookingFlowVC = BookingFlowViewController.sharedInstance()
                
                if bookingFlowVC.bookingId == bookingId {
                    
                    //If Showing Same Booking
                    bookingFlowVC.navigationLeftButtonAction(bookingFlowVC.navigationLeftButton)
                }
            }
//            else {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "canceledBooking"), object: self)
//                updateDatainBookingHistoryClass(bookingStatusModel: bookingStatusModel)
//            }
            
            
            break
        case Booking_Status.IgnoreOrExpire.rawValue:

            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "canceledBooking"), object: self)
            let cancelBookingView = BookingCancelledView.sharedInstance

            if bookingStatusModel.bookingType == 2 && bookingStatusModel.bookingModel == 1 {
                 BlockerAlert.instance.hide()
//                 cancelBookingView.changeTitle(message: "No Vaids are available today at the moment. Please try a little later or tomorrow")
                
                DispatchQueue.main.async {
                    self.moveTohome()
                }
                
                cancelBookingView.changeTitle(message: "Vaid does not have any appointments available, please try again later")
               
            } else if bookingStatusModel.bookingType == 1 && bookingStatusModel.bookingModel == 1 {
                
                cancelBookingView.changeTitle(message: "No Vaids are available within the hour. You can try again later or try Vaid Today to setup an appointment with a Vaid")
            } else if bookingStatusModel.bookingType == 2 && bookingStatusModel.bookingModel == 2 {
                BlockerAlert.instance.hide()
//                cancelBookingView.changeTitle(message: "\(bookingStatusModel.providerFName) is unavailable  today at the moment. Please try a little later or tomorrow.")
                DispatchQueue.main.async {
                    self.moveTohome()
                }
                cancelBookingView.changeTitle(message: "\(bookingStatusModel.providerFName) does not have any appointments available, please try again later")
            } else if bookingStatusModel.bookingType == 1 && bookingStatusModel.bookingModel == 2 {
                
                cancelBookingView.changeTitle(message: "\(bookingStatusModel.providerFName) is unavailable within the hour. You can try again later or Try Vaid Today to set up an appointment with this Vaid.")
            }
            
            WINDOW_DELEGATE??.addSubview(cancelBookingView)
            
            cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
            
            
            UIView.animate(withDuration: 0.5,
                           delay: 0.0,
                           options: UIView.AnimationOptions.beginFromCurrentState,
                           animations: {
                            
                            cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                            
            }) { (finished) in
                
                BlockerAlert.instance.hide()
               
            }
            break
        case Booking_Status.Declined.rawValue:
            
   
            NotificationCenter.default.post(name: Notification.Name(rawValue: "canceledBooking"), object: self)
            let cancelBookingView = BookingCancelledView.sharedInstance
            

            if bookingStatusModel.bookingModel == 1 {
                cancelBookingView.changeTitle(message: "\(bookingStatusModel.providerFName) is currently unavailable, please book a new vaid through our app.")
            } else {
                BlockerAlert.instance.hide()
                DispatchQueue.main.async {
                    self.moveTohome()
                }
                cancelBookingView.changeTitle(message: "\(bookingStatusModel.providerFName) does not have any appointments available, please try again later")
            }
        
            WINDOW_DELEGATE??.addSubview(cancelBookingView)
            
            cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
            
            
            UIView.animate(withDuration: 0.5,
                           delay: 0.0,
                           options: UIView.AnimationOptions.beginFromCurrentState,
                           animations: {
                            
                            cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                            
            }) { (finished) in
                
//                self.cancelBookingScreen.getCancelReasonsAPI()
                
            }
//            Helper.showAlert(head: ALERTS.Message, message: bookingStatusModel.bookingStatusMessage)
            
//            if Helper.getCurrentVC() is PendingBookingViewController {
//                
//                let pendingBookingVC = PendingBookingViewController.sharedInstance()
//                
//                if pendingBookingVC.bookingEventModel.bookingId == bookingId {
//                    
//                    //If Showing Same Booking
//                    if MyEventViewController.obj != nil {
//                        
//                        MyEventViewController.sharedInstance().gotoPastBooking = true
//                        MyEventViewController.sharedInstance().gotoPendingBooking = false
//                        MyEventViewController.sharedInstance().gotoUpcomingBooking = false
//                    }
//                    
//                    pendingBookingVC.navigationLeftButtonAction(pendingBookingVC.navigationLeftButton)
//                }
//            }
//            else {
            
//                updateDatainBookingHistoryClass(bookingStatusModel: bookingStatusModel)
//            }
            
            
            break
          case Booking_Status.Cancel.rawValue:
            let vaidTodayView = ConfirmVaidToday.sharedInstance
            vaidTodayView.hide()
            Helper.showAlert(head: "", message: "Appointment cancelled as you did not accept within 5 min")
//            Helper.showAlert(head: ALERTS.Message, message: bookingStatusModel.bookingStatusMessage)
            
            if Helper.getCurrentVC() is BookingFlowViewController {
                
                bookingFlowVC = BookingFlowViewController.sharedInstance()
                
                if bookingFlowVC.bookingId == bookingId {
                    
                    //If Showing Same Booking
                    bookingFlowVC.navigationLeftButtonAction(bookingFlowVC.navigationLeftButton)
                }
            }
            //            else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "canceledBooking"), object: self)
            //                updateDatainBookingHistoryClass(bookingStatusModel: bookingStatusModel)
            //            }
            
            
            break
            
        default:
            //For Remaining Status
            if !(Helper.getCurrentVC() is BookingFlowViewController) {
                
                if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                    
                    //Booking Completed
                    if Helper.getCurrentVC() is InvoiceViewController {
                        
                        if !arrayOfPendingInvoices.contains(bookingId) {
                            
                            arrayOfPendingInvoices.append(bookingId)
                        }
                        
                    } else {
                        
                        if !arrayOfPendingInvoices.contains(bookingId) {
                            
                            arrayOfPendingInvoices.append(bookingId)
                            showInvoiceScreen(bid: bookingStatusModel.bookingId)
                        }
                        
                    }
                    
                }
                else {
                    
                    if bookingStatusModel.bookingStatus == 3 {
                        print(bookingStatusModel.bookingType)
                        print(bookingStatusModel.vaidTodayStatus)
                        if bookingStatusModel.bookingType == 2 && bookingStatusModel.vaidTodayStatus == 0 {
                            let vaidTodayView = ConfirmVaidToday.sharedInstance
//                            vaidTodayView.changeTitle(message: "\(bookingStatusModel.providerFName) has an appointment available at \(Helper.getOnlyTimeFromTimeStamp(timeStamp: Int64(bookingStatusModel.clinicVisitTime))).")
                            vaidTodayView.changeTitle(message: "Please accept the appointment within 5 min.")
                            
                            if vaidTodayView.isVisible != true {
                                
                                vaidTodayView.isVisible = true
                                vaidTodayView.bookingId = bookingStatusModel.bookingId
                                WINDOW_DELEGATE??.addSubview(vaidTodayView)
                                
                                vaidTodayView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                                
                                UIView.animate(withDuration: 0.5,
                                               delay: 0.0,
                                               options: UIView.AnimationOptions.beginFromCurrentState,
                                               animations: {
                                                
                                                vaidTodayView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                                                
                                }) { (finished) in
                                }
                            }
                       
                        } else {
                       
                            if bookingStatusModel.bookingType == 2 {
                                let cancelBookingView = BookingCancelledView.sharedInstance
                                cancelBookingView.changeTitle(message: "\(bookingStatusModel.providerFName) has accepted your request, please visit the Vaid within one hour.")
                                WINDOW_DELEGATE??.addSubview(cancelBookingView)
                                
                                cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                                
                                
                                UIView.animate(withDuration: 0.5,
                                               delay: 0.0,
                                               options: UIView.AnimationOptions.beginFromCurrentState,
                                               animations: {
                                                
                                                cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                                                
                                }) { (finished) in
                                    
                                }
                            }

                            
                        }
                        
                    }
                    
//                    delegate?.didReceiveBookingAcception()
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "gotNewBooking"), object: self)
                    
                   
                    //This shows Local Notification for booking accepted
                    showBookingFlowMessageCustomNotification(bookingStatusModel: bookingStatusModel)
                }
                
                
            } else {
                
                bookingFlowVC = BookingFlowViewController.sharedInstance()
                
                if bookingFlowVC.bookingId != bookingId {
                    
                    //Booking Flow Controller with Different BID
                    
                    if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                        
                        //Booking Completed
                        if !arrayOfPendingInvoices.contains(bookingId) {
                            
                            arrayOfPendingInvoices.append(bookingId)
                            showInvoiceScreen(bid: bookingStatusModel.bookingId)
                        }
                    }
                    else {
                        
                        showBookingFlowMessageCustomNotification(bookingStatusModel: bookingStatusModel)
                    }
                }
                else {
                    
                    //Booking Flow Controller with same BID
                    bookingFlowVC.bookingStatus = bookingStatus
                    bookingFlowVC.bookingId = bookingId
                    
                    updateDatainBookingFlowClass(bookingStatusModel: bookingStatusModel)
                    
                    
                    if bookingStatus > Booking_Status.Ontheway.rawValue {
                        
                        bookingFlowVC.cancelButton.isHidden = true
                        
                    } else {
                        
                        bookingFlowVC.cancelButton.isHidden = false
                    }
                    
                    //For Booking Timer
                    if bookingStatus == Booking_Status.Started.rawValue {
                        
                        updateBookingTimerDetails(bookingStatusModel: bookingStatusModel)
                        bookingFlowVC.showBookingTimerDetails()
                        
                    } else {
                        
                        bookingFlowVC.hideBookingTimerDetailsViewAnimation()
                    }
                    
                    
                    bookingFlowVC.tableView.reloadData()
                    
                    bookingFlowVC.scrollTableViewToShowCusrrentBookingStatus()
                    
                    if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                        
                        //Show Invoice Controller
                        if !arrayOfPendingInvoices.contains(bookingId) {
                            
                            arrayOfPendingInvoices.append(bookingId)
                            showInvoiceScreen(bid: bookingStatusModel.bookingId)
                        }
                        
                    }
                }
            }
            
            updateDatainBookingHistoryClass(bookingStatusModel: bookingStatusModel)

            
            break
        }
        
    }
    
    func moveTohome() {

        let story = UIStoryboard(name: "Main", bundle:nil)
        let vc = story.instantiateViewController(withIdentifier: "VaidHomeVc") as! VrHomeViewController
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    /// Method to show
    ///
    /// - Parameter bookingStatusModel: booking status response model
    func showBookingFlowMessageCustomNotification(bookingStatusModel:BookingStatusResponseModel) {
        
        if LocalNotificationView.share != nil {
            
            LocalNotificationView.share?.removeFromSuperview()
            LocalNotificationView.share = nil
            
        }
        
        let localNotificationView = LocalNotificationView.sharedInstance
        localNotificationView.bookingStatusModel = bookingStatusModel
        
      //  WINDOW_DELEGATE??.addSubview(localNotificationView)
        
      // localNotificationView.showBookingNotificationDetails()
        
        
        Helper.playLocalNotificationSound()
        
      //  localNotificationView.springAnimationForNotificationView()
        
    }
    
    
    /// Method to Show booking status message in Booking status view controller
    func showBookingStatusinBookingFlowScreen() {
        
        if Helper.getCurrentVC() is BookingFlowViewController {//Cusrrent controller is Booking flow VC
            
            var bookingFlowVC = BookingFlowViewController.sharedInstance()
            
            bookingFlowVC = BookingFlowViewController.sharedInstance()
            
            bookingFlowVC.bookingId = bookingId
            bookingFlowVC.bookingStatus = bookingStatus
            
            bookingFlowVC.title = "\(ALERTS.BOOKING_FLOW.EventID)" + " : " + String(bookingId) //String(format:"%td", bookingId)
            
            //Call Service
            bookingFlowVC.getBookingDetailsAPI()
            
        }
        else {
            
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = storyboard.instantiateViewController(withIdentifier: VCIdentifier.bookingFlowVC) as? BookingFlowViewController
            
            vc?.bookingStatus = bookingStatus
            vc?.bookingId = bookingId
            
//            TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                      subType: kCATransitionFromTop,
//                                                                      for: (Helper.getCurrentVC().navigationController?.view)!,
//                                                                      timeDuration: 0.3)
            
            
            TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (Helper.getCurrentVC().navigationController?.view)!, timeDuration: 0.35)
            
            
            Helper.getCurrentVC().navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    
    
    /// Update booking status in my events controller without call service
    ///
    /// - Parameter bookingStatusModel: booking status response model
    func updateDatainBookingHistoryClass(bookingStatusModel: BookingStatusResponseModel) {
        
        if Helper.getCurrentVC() is MyEventViewController  {
            
            let myEventClass = MyEventViewController.sharedInstance()
            
            myEventClass.updateBookingDetailsAsynchronously(bookingStatusModel: bookingStatusModel)
            
        } else if MyEventViewController.obj != nil {
            
            let myEventClass = MyEventViewController.sharedInstance()
            
            myEventClass.updateBookingDetailsAsynchronously(bookingStatusModel: bookingStatusModel)
        }
    }
    
    
    
    /// Update each booking status dates in BookingFlow VC
    ///
    /// - Parameter bookingStatusModel: booking status response model
    func updateDatainBookingFlowClass(bookingStatusModel: BookingStatusResponseModel) {
        
        switch bookingStatus {
            
        case Booking_Status.Accepted.rawValue:
            
            bookingFlowVC.bookingDetailModel.acceptedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Ontheway.rawValue:
            
            bookingFlowVC.bookingDetailModel.onThewayDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Arrived.rawValue:
            
            bookingFlowVC.bookingDetailModel.arrivedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Started.rawValue:
            
            bookingFlowVC.bookingDetailModel.startedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Completed.rawValue:
            
            bookingFlowVC.bookingDetailModel.completedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Raiseinvoice.rawValue:
            
            bookingFlowVC.bookingDetailModel.invoiceRaisedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        default:
            break
        }
    }
    
    
    
    /// Update each booking Timer details in BookingFlow VC
    ///
    /// - Parameter bookingStatusModel: booking status response model
    func updateBookingTimerDetails(bookingStatusModel:BookingStatusResponseModel) {
        
        if !bookingStatusModel.bookingTimerDetails.isEmpty {
            
            if let timerStatusValue = bookingStatusModel.bookingTimerDetails["status"] as? Int {
                
                bookingFlowVC.bookingDetailModel.timerStatus = timerStatusValue
            }
            
            if let timerStampValue = bookingStatusModel.bookingTimerDetails["startTimeStamp"] as? Int {
                
                bookingFlowVC.bookingDetailModel.timerStamp = timerStampValue
            }
            
            if let totalTimeElapsedValue = bookingStatusModel.bookingTimerDetails["second"] as? Int {
                
                bookingFlowVC.bookingDetailModel.totalTimeElapsed = totalTimeElapsedValue
            }
            
        }
        
    }
    
    
    
    /// Show Invoice view controller
    ///
    /// - Parameter bid: booking id to show Invoice
//    func showInvoiceScreen(model: BookingStatusResponseModel) {
  
    func showInvoiceScreen(bid:Int64) {
        if LocalNotificationView.share != nil {
            
            if LocalNotificationView.share?.bookingStatusModel.bookingId == bid //model.bookingId
            {
                
                LocalNotificationView.share?.removeFromSuperview()
                LocalNotificationView.share = nil
                
            }
            
        }
        
        Helper.playLocalNotificationSound()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "RatingVc") as? VRRatingViewController
//        vc?.model = model
        vc?.bookingId = bid
//
//        vc?.bookingId = bid
//        
        if (Helper.getCurrentVC().navigationController != nil) {
            
//            TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                      subType: kCATransitionFromTop,
//                                                                      for: (Helper.getCurrentVC().navigationController?.view)!,
//                                                                      timeDuration: 0.3)
            
            
            
            TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (Helper.getCurrentVC().navigationController?.view)!, timeDuration: 0.35)

            
            Helper.getCurrentVC().navigationController?.pushViewController(vc!, animated: false)
        }
        
    }
    
    
    func updateNotReviewedBookingsArray(notReviewedBookings:[Any]) {
        arrayOfPendingInvoices.removeAll()
        for eachBookingId in notReviewedBookings {
            
            if let bookingId = eachBookingId as? Int64 {
                
                if !arrayOfPendingInvoices.contains(bookingId) {
                    
                    arrayOfPendingInvoices.append(bookingId)
                }
            }
        }
        
        if SplashLoading.obj == nil {//Checking currently showing Splash loading or not
            
            if arrayOfPendingInvoices.count > 0 {
                
                //Booking Completed
                if !(Helper.getCurrentVC() is InvoiceViewController) {
                    
                    showInvoiceScreen(bid: arrayOfPendingInvoices[0])
                }
            }
        }

    }
    
    func showPendingReviewsAfterSplashLoading() {
        
        if arrayOfPendingInvoices.count > 0 {
            
            //Booking Completed
            if !(Helper.getCurrentVC() is InvoiceViewController) {
                
                showInvoiceScreen(bid: arrayOfPendingInvoices[0])
            }
        }
    }
    
    
    
    /// Show remaining completed booking invoice
    ///
    /// - Parameter showedBookingId: already invoice showed booking id
    func showRemainingCompletedBookingInvoice(_ showedBookingId:Int64) {
        
        if arrayOfPendingInvoices.contains(showedBookingId) {
            
            arrayOfPendingInvoices.remove(at:arrayOfPendingInvoices.index(of: showedBookingId)!)
        }
        
        if arrayOfPendingInvoices.count > 0 {
            
            showInvoiceScreen(bid: arrayOfPendingInvoices[0])
        }
        
    }
    
}

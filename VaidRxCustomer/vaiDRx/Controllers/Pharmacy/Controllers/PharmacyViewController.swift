//
//  PharmacySelectionViewController.swift
//  GoTasker
//
//  Created by 3Embed on 05/12/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class PharmacyViewController: UIViewController,SelectedPharmacyDelegate {
    
    @IBOutlet weak var selectPharmacyLbl: UILabel!

    @IBOutlet weak var pharmacyLabel: UILabel!
    
    func didSelectPharmacy(selectedData: PharmacyItems) {
        self.pharmacyLabel.text = selectedData.pharmacyName
        registerViewModel?.pharmacyId = selectedData.pharmacyId
    }
    
    var registerViewModel: RegisterViewModel?
    
    var stripeKeyResponseModel :StripeKeyResponseModel?
    var publishableKey = ""
    var vaidRx_AddNewCardViewModel = VaidRx_AddNewCardViewModel()
    //    var stripeKeyResponseModel :StripeKeyResponseModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getStripeKeyData()
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigation()
        
        if self.pharmacyLabel.text?.count ?? 0 > 0 {
            selectPharmacyLbl.text = "Selected pharmacy"
        } else {
            selectPharmacyLbl.text = "Select pharmacy"
        }
        
    }
    
    func hideNavigation() {
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = APP_COLOR //UIColor(hex: "05C1C9") //UIColor.clear
        self.navigationController?.navigationBar.tintColor = UIColor.white
//        self.navigationController?.navigationBar.barTintColor = UIColor.clear //UIColor(hex: "05C1C9") 
        //        self.navigationController?.navigationBar.tintColor = APP_COLOR
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    

    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }

    @IBAction func nextButtonAction(_ sender: UIButton) {
        if pharmacyLabel.text != ""{
        
        performSegue(withIdentifier: "ToAddCardDeatilVC", sender: self)
            
        }
        else{
            Helper.showAlert(head: "Message", message: "Enter Pharmacy Details")
        }
    }
    @IBAction func selectPharmacyButtonAction(_ sender: UIButton) {
        
//        if pharmacyLabel.text == ""{
            let storyboard = UIStoryboard(name: "vaiDRx_Main", bundle: nil)
            let nextVc = storyboard.instantiateViewController(withIdentifier: "PharmacySelection") as? PharmacySelectionViewController
        nextVc?.selectedPharmacyDelegate = self
        nextVc?.registerViewModel = self.registerViewModel
        self.navigationController?.pushViewController(nextVc ?? UIViewController(), animated: true)
//        }
        
        
        
        //performSegue(withIdentifier: "toSelectPharmacy", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToAddCardDeatilVC" {
            
            if let dstVC = segue.destination as? vaiDRx_AddNewCardVC {
                if let x = self.registerViewModel {
                    dstVC.registerViewModel = x
                    dstVC.publishableKey = publishableKey
                }
                
            }
            
        }
    }
    
    func getStripeKeyData(){
        
        if !NetworkHelper.sharedInstance.networkReachable(){
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
            //Helper.showAlertMessage(title: ALERT_VAIDRX.Error, message: ALERT_VAIDRX.NoInternet)
            return
        }
        
        vaidRx_AddNewCardViewModel.getStripeKeyAPICall { (statusCode, errMsg, response) in
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response)
        }
    }
    
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResp:Any?){
        switch statusCode {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            stripeKeyResponseModel = StripeKeyResponseModel.init(stripeKEyResponseDetails:dataResp as Any )
            
            //            insuranceItems = insuranceData.insuranceItemsModel
            //            DispatchQueue.main.async {
            //                self.tableView.reloadData()
            //            }
            publishableKey = stripeKeyResponseModel?.publishableKey ?? ""
            print("insuranceItems",stripeKeyResponseModel)
            ConfigManager.sharedInstance.paymentGatewayKey = publishableKey
            
        //            print("data response",dataResp!)
        default:
            break
        }
    }
    
}

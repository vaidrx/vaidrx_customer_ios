//
//  vaiDRx_VerifyEmailVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class vaiDRx_VerifyEmailVC: UIViewController, KeyboardDelegate  {

    // MARK: - Outlets -
    
    //TextFeild Outlet
    @IBOutlet weak var emailTextFeild: UITextField!
    
    //Layout Constraint
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Variable Decleration -
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var registerViewModel: RegisterViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.hideNavBarShadow(vc: self)
        addObserveToVariables()
        OperationQueue.main.addOperation {
            self.emailTextFeild.becomeFirstResponder()
        }
        
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        appDelegate?.keyboardDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        appDelegate?.keyboardDelegate = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addObserveToVariables() {
        
        emailTextFeild.rx.text
            .orEmpty
            .bind(to: registerViewModel.emailText )
            .disposed(by: disposeBag)
    }

}

//
//  RateProviderViewModel.swift
//  LSP
//
//  Created by Rajan Singh on 29/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire

class RateProviderViewModel: NSObject {
    let disposeBag = DisposeBag()
    let rateProModel = RateProviderModel()
    let mainApi_response = PublishSubject<RateProviderModel>()
    
    func updateBookingID(_ bookingID: Int64) {
        rateProModel.updateBookingID(bookingID)
    }
    
    func updateRating(_ rating: Float) {
        rateProModel.updateRating(rating)
    }
    
    func updateReview(_ review: String) {
        rateProModel.updateReview(review)
    }
    
}

extension RateProviderViewModel {

    func getBookingID() -> Int64 {
        return rateProModel.bookingID
    }
    
    func getRating() -> Float {
        return rateProModel.rating
    }
    
    func getReview() -> String {
        return rateProModel.review
    }
    
}

extension RateProviderViewModel {
    

    func submitReviewAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxBookingAPI = BookingAPI()
        
        rxBookingAPI.submitReviewServiceAPICall(proRate: rateProModel)
        
        if !rxBookingAPI.submitReview_Response.hasObservers {
            
            rxBookingAPI.submitReview_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposeBag)
            
        }
    }
    

}









//
//  MusiciansListManager.swift
//  LiveM
//
//  Created by Raghavendra V on 07/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps
import RxCocoa
import RxSwift


protocol MusiciansListManagerDelegate {
    
    
    /// Show Initial Musician List
    ///
    /// - Parameter arrayOfUpdatedMusicians: array consisting of list of musicians model
    ///   - arrayOfMusiciansList: array consisting of list of Musicians
    func showInitialMusicianList(arrayOfUpdatedMusiciansModel: [MusicianDetailsModel],arrayOfMusiciansList:[Any])
    
    
    /// Method to Update the Musician List
    ///
    /// - Parameters:
    ///   - arrayOfUpdatedMusicians: array consisting of list of updated musicians model
    ///   - arrayOfRowsToAdd: array consisting of list of row numbers to add from musician list
    ///   - arrayOfRowsToRemove: array consisting of list of row numbers to remove from musician list
    ///   - arrayOfMusicianIdToRemove: array consisting of list of Musician id's to remove from musician list
    ///   - arrayOfMusiciansList: array consisting of list of Musicians
    func updateMusicianList(arrayOfUpdatedMusicians:[MusicianDetailsModel], arrayOfRowsToAdd:[Int], arrayOfRowsToRemove:[Int],arrayOfMusicianIdToRemove:[String], arrayOfMusiciansList:[Any])
    
    
    /// This method will invoke when user gets empty musician lists
    func emptyMusicianList(errMessage:String)
    
}


class MusiciansListManager {
    
    var publishTimer:Timer?
    let acessClass = AccessTokenRefresh.sharedInstance()
    var navigation:UINavigationController!
    var viewController:UIViewController!
    var delegate:MusiciansListManagerDelegate? = nil
    var arrayOfMusicians:[Any] = []
    let locationObj = LocationManager.sharedInstance()
    
    var errMessageString = ""
    
    let accessTokenrefreshClass = AccessTokenRefresh.sharedInstance
    
    static var obj:MusiciansListManager? = nil
    
    class func sharedInstance() -> MusiciansListManager {
        
        if obj == nil {
            
            obj = MusiciansListManager()
        }
        
        return obj!
    }
    
    let disposebag = DisposeBag()
    
    /// Time Interval
    var timeInterval: Double = 12 {
        
        didSet {
            
            startTimer()
        }
    }
    
    
    func startTimer() {
        
        if publishTimer == nil {
            
            publishTimer = Timer.scheduledTimer(timeInterval: timeInterval,
                                                target: self,
                                                selector: #selector(handleTimer),
                                                userInfo: nil,
                                                repeats: true)
            
        }
        
    }
    
    /// Handle Timer
    @objc func handleTimer() {
        
        if (publishTimer != nil) && (publishTimer?.isValid == true) {
            
            publish(isIinitial: false)
        }
    }
    
    
    /// Stop Publishing
    func stopPublish() {
        
        if (publishTimer != nil) && (publishTimer?.isValid == true) {
            
            publishTimer?.invalidate()
            publishTimer = nil
        }
    }
    
    
    
    /// Method to maintain Refresh Access Token
    func methodTorefreshAccessToken() {
        
        switch viewController {
            
            case is HomeScreenViewController:
            
                let homeVC = HomeScreenViewController.sharedInstance()
            
                homeVC.uninitializeMQTTMethods()
                homeVC.apiTag = RequestType.getAllMusicians.rawValue
                break
            
            case is HomeListViewController:
            
                let homeListVC = HomeListViewController.sharedInstance()
            
                homeListVC.uninitializeMQTTMethods()
                homeListVC.apiTag = RequestType.getAllMusicians.rawValue
                break
            
            default:
                break
        }
        
        self.acessClass.getAcessToken(progressMessage: "")
        
    }
    
    
    /// Publish
    func publish(isIinitial:Bool) {
                
        let appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
        
        if AppoimtmentLocationModel.sharedInstance.pickupLatitude == 0.0 || AppoimtmentLocationModel.sharedInstance.pickupLongitude == 0.0 {
            
            return
        }
        
        let rxMusiciansListAPICall = MusiciansListAPI()
        
        if !rxMusiciansListAPICall.musiciansList_Response.hasObservers {
            
            rxMusiciansListAPICall.musiciansList_Response
                .subscribe(onNext: {response in
                    
                    Helper.hidePI()
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        if response.httpStatusCode == 401 {
                            
                            if let viewController:UIViewController = UIApplication.shared.delegate?.window??.rootViewController {
                                
                                if let VC = viewController as? ExSlideMenuController
                                {
                                    print(VC)
                                    self.stopPublish()
                                    Helper.logOutMethod()
                                    
                                    if let errorMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                                        
                                        Helper.showAlert(head: ALERTS.Message, message: errorMessage)
                                    }
                                                                        
                                }
                            }
                            
                        }
                        
                        return

                    }
                    
                    self.WebServiceResponse(response: response, isInitial: isIinitial)
                    
                }, onError: {error in
                    
                    Helper.hidePI()
                    
                }).disposed(by: disposebag)
            
            
        }
        
        if isIinitial {
            
            
            rxMusiciansListAPICall.musiciansListGETServiceAPICall(pickupLat: appointmentLocationModel.pickupLatitude, pickupLong: appointmentLocationModel.pickupLongitude)
            
            
            
        } else {
            
            rxMusiciansListAPICall.musiciansListPOSTServiceAPICall(appointmentLocationModel:appointmentLocationModel)
            
        }
        
        
        
    }
    
    //MARK - Web Service Response -
    func WebServiceResponse(response:APIResponseModel, isInitial:Bool)
    {
        switch response.httpStatusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                AppDelegate().defaults.set(GenericUtility.strForObj(object: response.data[SERVICE_RESPONSE.DataResponse]), forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
                self.methodTorefreshAccessToken()
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                if let viewController:UIViewController = UIApplication.shared.delegate?.window??.rootViewController {
                    
                    if let VC = viewController as? ExSlideMenuController
                    {
                        print(VC)
                        self.stopPublish()
                        Helper.logOutMethod()
                        
                        if let errorMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                            
                            Helper.showAlert(head: ALERTS.Message, message: errorMessage)
                        }
                        
                    }
                }
                
                break
                
            
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                print("Get All Musician Response:\(response)")
                
                if isInitial {
                    
                    if let responseData = response.data[SERVICE_RESPONSE.DataResponse] as? [Any] {
                        
                        self.recievedListOfMusiciansFromMQTT(listOfNewMusicians: responseData, listOfCurrentMusicians: arrayOfMusicians)
                        
                    }
                }
                
                break
            
            case HTTPSResponseCodes.MusicianList.NoMusicians.rawValue:
            
                print("Get All Musician Error Response:\(response)")
                
                if let errMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                    self.noMusiciansResponse(message: errMessage)
                }
                
            
                break
                
            default:
                
                print("Get All Musician Response:\(response)")
                
                if isInitial {
                    
                    if let responseData = response.data[SERVICE_RESPONSE.DataResponse] as? [Any] {
                        
                        self.recievedListOfMusiciansFromMQTT(listOfNewMusicians: responseData, listOfCurrentMusicians: arrayOfMusicians)
                        
                    }
                }

                break
        }
        
    }

    
    func noMusiciansResponse(message:String) {
        
        errMessageString = message
        arrayOfMusicians = []
        
        if delegate != nil {
            
            delegate?.emptyMusicianList(errMessage: message)
        }
    }
    
    
    
    /// Method recieve listOfMusician Array from MQTT Model
    ///
    /// - Parameter listOfMusicians: consists array of Musicians who are online
    func recievedListOfMusiciansFromMQTT(listOfNewMusicians:[Any], listOfCurrentMusicians:[Any]) {
        
        let sortedListOfNewMusicians = listOfNewMusicians.removeDuplicates()
        
//        UserDefaults.standard.set(sortedListOfNewMusicians, forKey: USER_DEFAULTS.MUSICIAN_LIST.PREVIOUS)
//        UserDefaults.standard.synchronize()

        
        if sortedListOfNewMusicians.count == 0 {//checking musician list is empty or not
            
            arrayOfMusicians = sortedListOfNewMusicians
            
            if delegate != nil {
                
                delegate?.emptyMusicianList(errMessage: self.errMessageString)
            }

        }
        else {
            
            var arrayOfUpdatedMusicians:[MusicianDetailsModel] = []
        
            for Musician in sortedListOfNewMusicians {//Creating array of updated musician list
            
                let musicianDetailsModel = MusicianDetailsModel.init(musicianDetails: Musician)
                arrayOfUpdatedMusicians.append(musicianDetailsModel)
            }

            if listOfCurrentMusicians.isEmpty {//Checking previous musician list is empty or not
        
                arrayOfMusicians = sortedListOfNewMusicians
        
                if delegate != nil {
            
                    delegate?.showInitialMusicianList(arrayOfUpdatedMusiciansModel: arrayOfUpdatedMusicians,arrayOfMusiciansList:arrayOfMusicians)
                }
                
            } else if listOfCurrentMusicians.count == arrayOfUpdatedMusicians.count {
        
                let previous: NSArray = listOfCurrentMusicians as NSArray
                let updated: NSArray = sortedListOfNewMusicians as NSArray
        
                if previous.isEqual(to: updated as! [Any]) {
                    // Both are Equal
                    // Do not do anything
                    print("Musician List is Same")
            
                    arrayOfMusicians = sortedListOfNewMusicians
                    maintainUpdatedMusicianList(listOfNewMusicians: sortedListOfNewMusicians,arrayOfUpdatedMusicians:arrayOfUpdatedMusicians, listOfCurrentMusicians: listOfCurrentMusicians)
            
                }else {
            
                    // Both are not equal
                    print("Updated Musician List is not equal")
            
                    maintainUpdatedMusicianList(listOfNewMusicians: sortedListOfNewMusicians,arrayOfUpdatedMusicians:arrayOfUpdatedMusicians, listOfCurrentMusicians:listOfCurrentMusicians)
            
                }
            
            }else {
                
                maintainUpdatedMusicianList(listOfNewMusicians: sortedListOfNewMusicians,arrayOfUpdatedMusicians:arrayOfUpdatedMusicians, listOfCurrentMusicians:listOfCurrentMusicians)
            }
        }
    }
    
    
    /// Method to maintain updated musician list
    ///
    /// - Parameters:
    ///   - listOfNewMusicians: list of musican details
    ///   - arrayOfUpdatedMusicians: list of musican details model
    func maintainUpdatedMusicianList(listOfNewMusicians:[Any], arrayOfUpdatedMusicians:[MusicianDetailsModel], listOfCurrentMusicians:[Any]) {
        
        print("Musician List is Changed")
        
        var arrayOfRowsToRemove:[Int] = []
        var arrayOfRowsToAdd:[Int] = []
        var arrayOfMusicianIdToRemove:[String] = []
        
        //Finding Musicians to Add in the List
        for i in 0..<listOfNewMusicians.count {
            
            let musicianDetail = MusicianDetailsModel.init(musicianDetails: listOfNewMusicians[i])
            
            let predicate = NSPredicate(format: "id = %@", musicianDetail.providerId)
            let result = listOfCurrentMusicians.filter { predicate.evaluate(with: $0) }
            
            if result.count == 0 {
                
                arrayOfRowsToAdd.append(i)
            }
        }
        
        //Finding Musicians To Remove from the List
        for i in 0..<listOfCurrentMusicians.count {
            
            let musicianDetail = MusicianDetailsModel.init(musicianDetails: listOfCurrentMusicians[i])
            
            let predicate = NSPredicate(format: "id = %@", musicianDetail.providerId)
            let result = listOfNewMusicians.filter { predicate.evaluate(with: $0) }
            
            if result.count == 0 {
                
                arrayOfRowsToRemove.append(i)
                arrayOfMusicianIdToRemove.append(musicianDetail.providerId)
            }
        }
        
        
        arrayOfMusicians = listOfNewMusicians
        
        if delegate != nil {
            
            delegate?.updateMusicianList(arrayOfUpdatedMusicians: arrayOfUpdatedMusicians, arrayOfRowsToAdd: arrayOfRowsToAdd, arrayOfRowsToRemove: arrayOfRowsToRemove,arrayOfMusicianIdToRemove:arrayOfMusicianIdToRemove, arrayOfMusiciansList:arrayOfMusicians)
        }

    }
    
}

extension Array where Element:Any {
    func removeDuplicates() -> [Any] {
        
        var result = [Element]()
        for value in self {
            
            if !result.contains(where: {($0 as! AnyHashable  == value as! AnyHashable)}) {
                
                result.append(value)
            }
        }
        
        return (result as [Any])
    }
}

//
//  VerifyPhoneViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class VerifyPhoneViewModel {
    
    var codeText = ""
    var userId = ""
    var triggerValue = 0
    var phoneNumber = ""
    var countryCode = ""
    
    let disposebag = DisposeBag()
    
    let rxVerifyPhoneAPICall = VerifyPhoneAPI()
    
    func resendOTPAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxVerifyPhoneAPICall.resendOTPServiceAPICall(triggerValue: triggerValue,
                                                     userId: userId)
        
        if !rxVerifyPhoneAPICall.resendOTP_Response.hasObservers {
            
            rxVerifyPhoneAPICall.resendOTP_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    
    func registerVerifyOTPAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxVerifyPhoneAPICall.registerVerifyOTPServiceAPICall(verificationCode: codeText, userId: userId)
        
        if !rxVerifyPhoneAPICall.registerVerifyOTP_Response.hasObservers {
            
            rxVerifyPhoneAPICall.registerVerifyOTP_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    
    func forgotPasswordVerifyOTPAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxVerifyPhoneAPICall.forgotPasswordVerifyOTPServiceAPICall(verificationCode: codeText,
                                                                   triggerValue: triggerValue,
                                                                   userId: userId)
        
        if !rxVerifyPhoneAPICall.forgotPasswordVerifyOTP_Response.hasObservers {
            
            rxVerifyPhoneAPICall.forgotPasswordVerifyOTP_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }

    
}

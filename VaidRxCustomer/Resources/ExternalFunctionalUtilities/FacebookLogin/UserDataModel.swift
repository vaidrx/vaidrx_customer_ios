//
//  UserDataModel.swift
//  DayRunner
//
//  Created by STARK on 15/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import GoogleSignIn

class UserDataModel {
    
    var mailId              = ""
    var userId              = ""
    var firstName           = ""
    var lastName            = ""
    var imageUrl            = URL(string: "")
    var imageUrlString      = ""
    var phoneNumber         = ""
    
    
    init(facebookDetails:Any) {//Facebook details model
    
        if let fbDetails = facebookDetails as? [String:AnyObject] {
        
            if let titleTemp = fbDetails["email"] as? String{
                mailId = titleTemp
            }
            if let titleTemp = fbDetails["id"] as? String{
                userId = titleTemp
            }
            if let titleTemp = fbDetails["first_name"] as? String{
                firstName = titleTemp
            }
            if let titleTemp = fbDetails["last_name"] as? String{
                lastName = titleTemp
            }
//            if let titleTemp = fbDetails["last_name"] as? String{
//                lastName = titleTemp
//            }
            if let titleTemp = fbDetails["phoneNumber"] as? String{
                phoneNumber = titleTemp
            }
            
            if let titleTemp = fbDetails["picture"] as? [String:AnyObject]{
                if let titleTemp2 = titleTemp["data"] as? [String:AnyObject]{
                    if let titleTemp3 = titleTemp2["url"] as? String{
                        imageUrl = URL(string: titleTemp3)!
                        imageUrlString = titleTemp3
                    }
                }
            }

        }
    
    }
    
        init(googleDetails:GIDGoogleUser)  {
            if let titleTemp = googleDetails.profile?.email{
            mailId = titleTemp
        }
//        userId = userIdReached
            if let titleTemp = googleDetails.profile?.givenName{
            firstName = titleTemp
        }
            if let titleTemp = googleDetails.profile?.familyName{
            lastName = titleTemp
        }
        if let titleTemp = googleDetails.userID{
            userId = titleTemp
        }
            if let titleTemp = googleDetails.profile?.imageURL(withDimension: UInt(100)){
            imageUrl = titleTemp
            imageUrlString = titleTemp.absoluteString
        }
//        return self
    }
}

//
//  PaymentDetailsViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 13/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class PaymentDetailsViewController: UIViewController {
    
    
    @IBOutlet var navigationLeftButton: UIButton!
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var cardBackImageView: UIImageView!
    
    @IBOutlet var cardNumberLabel: UILabel!
    
    @IBOutlet var expiryLabel: UILabel!
    
    @IBOutlet var deleteCardButton: UIButtonCustom!
    
    @IBOutlet var makeDefaultButton: UIButton!
    
    @IBOutlet weak var cardTypeLabel: UILabel!
    
    
    var cardDetails:CardDetailsModel!

    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    let paymentDetailsViewModel = PaymentDetailsViewModel()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    //MARK: - Default Class Methods -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeDefaultButton.titleLabel?.numberOfLines = 0
        makeDefaultButton.titleLabel?.textAlignment = NSTextAlignment.center
        
        showCardDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Helper.hideNavBarShadow(vc: self)
        makeDefaultButton.titleLabel?.textAlignment = NSTextAlignment.center
        makeDefaultButton.setTitle("Make\nDefault", for: UIControl.State.normal)
        
        acessClass.acessDelegate = self
        
        setupGestureRecognizer()
        
        if cardDetails.defaultCard {
            
            makeDefaultButton.isHidden = true
            
        } else {
            
            makeDefaultButton.isHidden = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showCardDetails() {
        
        self.cardNumberLabel.text = "**** **** **** " + cardDetails.last4Digits
        self.expiryLabel.text = cardDetails.expiryMonth + "/" + cardDetails.expiryYear
        self.cardTypeLabel.text = cardDetails.brand.uppercased()
    }
    
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func makeDefaultButtonAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: ALERTS.Message, message: ALERTS.PAYMENT.MakeDefaultPaymentMessage, preferredStyle: UIAlertController.Style.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        let DestructiveAction = UIAlertAction(title: ALERTS.NO, style: UIAlertAction.Style.destructive) {
            (result : UIAlertAction) -> Void in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            
            
            DDLogDebug("Destructive")
        }
        
        // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        
        let okAction = UIAlertAction(title: ALERTS.YES, style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            
            self.makeCardDefaultAPI()
        }
        
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        //        self.present(alertController, animated: true, completion: nil)
        
//        if Helper.newAlertWindow == nil {
//
//            Helper.newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
//            Helper.newAlertWindow?.rootViewController = UIViewController()
//            Helper.newAlertWindow?.windowLevel = UIWindowLevelAlert + 1
//            Helper.newAlertWindow?.makeKeyAndVisible()
//            Helper.newAlertWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
//
//
//        } else {
        
            newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
//         }

        
    }
    
    
    @IBAction func deleteCardButtonAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: ALERTS.PAYMENT.RemovePayment,
                                                message:ALERTS.PAYMENT.RemovePaymentMessage,
                                                preferredStyle: .alert)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        alertController.addAction(UIAlertAction(title: ALERTS.NO,
                                                style: .default,
                                                handler: {(_ action: UIAlertAction) -> Void in
                                                    
                                                    newAlertWindow.resignKey()
                                                    newAlertWindow.removeFromSuperview()
        }))
        
        alertController.addAction(UIAlertAction(title: ALERTS.YES,
                                                style: .default,
                                                handler: {(_ action: UIAlertAction) -> Void in
                                                    
                                                    newAlertWindow.resignKey()
                                                    newAlertWindow.removeFromSuperview()
                                                    
                                                    //Delete Payment
                                                    self.deleteCardAPI(cardId: self.cardDetails.id)
                                                    
        }))
        
        newAlertWindow.rootViewController?.present(alertController, animated: true) 

    }
    
    
}

extension PaymentDetailsViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
            case RequestType.deleteCard.rawValue:
            
                deleteCardAPI(cardId: cardDetails.id)
                break
            
            case RequestType.cardDefault.rawValue:
            
                makeCardDefaultAPI()
                break
            
            default:
                break
        }
    }
    
}

extension PaymentDetailsViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension PaymentDetailsViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}

//
//  ForgotPasswordCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ForgetPasswordViewController{
    
    /// Initiall SetUp For Animation
    func initiallSetup() {
        forgotPasswordWithEmail = false
        phoneButton.setTitleColor(#colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1).withAlphaComponent(1), for: .normal)
        emailButton.setTitleColor(#colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1).withAlphaComponent(0.5), for: .normal)
        phoneButtonBottomView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)
        emailButtonBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
        emailViewHeight.constant = 0
        phoneNumView.transform = CGAffineTransform(scaleX: 0, y: 0)
        nextBottomView.transform = CGAffineTransform(translationX: -400, y: 0)
        phoneButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        emailButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            DDLogDebug(countryCode)
            let picker = CountryNameViewController()
            picker.initialSetUp()
            let contry: Country
            contry = picker.localCountry(countryCode)
            self.countryCode.text = contry.dial_code
            let imagestring = contry.country_code
            let imagePath = "CountryPicker.bundle/\(imagestring).png"
            countryImagee.image = UIImage(named: imagePath)
            
        }
    }
    
    func initiallAnimation() {
        UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
            self.nextBottomView.transform = .identity
        }) { (true) in
            UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
                self.phoneButton.transform = .identity
                self.emailButton.transform = .identity
            })
        }
    }
    
    /// animate the view at bottom of text Fields
    func animateBottomView() {
        if self.phoneNumView.transform != .identity {
            UIView.animate(withDuration: 1.4, animations: {
                self.phoneNumView.transform = .identity
            })
        }
    }
    
    /// Animate Button Loading for user Id password
    ///
    /// - Parameter value: value to decide its percent of loading
    func animateButtonLoading(_ value: Int) {
        self.view.layoutIfNeeded()
        if value == 1 {
            self.loadingViewHeight.constant = self.nextButtonOutlet.frame.size.width
        }else {
            self.loadingViewHeight.constant = 0
        }
        
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier) as! CountryNameViewController
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        dstVC.countryDelegate = self
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    
    func validateAllFields() {
        
        let email = GenericUtility.strForObj(object: emailTF.text)
        
        if UInt(email.length) == 0
        {
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailMissing)
            emailTF.becomeFirstResponder()
        }
        else if !Helper.isValidEmail(testStr: email)
        {
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailInvalid)
            emailTF.text = ""
            forgotPasswordViewModel.emailText.value = ""
            emailTF.becomeFirstResponder()
        }
        else
        {
            self.view.endEditing(true)
            sendRequestToForgotPasswordUsingEmail()
        }
    }
    
    // MARK: - Segue Method -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEgueIdetifiers.forgetPassToOtp {
            
            if let dstVC = segue.destination as? VerifyMobileViewController{
                
                dstVC.isCommingFrom = "ForgetPass"
                dstVC.dataFromForgetPasswordVC = sender as! [String : Any]
            }
        }
    }
    
    func backButtonMethod() {
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.popViewController(animated: false)
    }
    
}

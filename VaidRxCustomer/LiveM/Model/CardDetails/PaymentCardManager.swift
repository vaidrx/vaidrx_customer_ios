//
//  GetListOfCardsFromServer.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import RxCocoa
import RxSwift

class PaymentCardManager {
    
    static let sharedInstance = PaymentCardManager()
    let rxPaymentCardAPICall = PaymentCardAPI()
    let disposebag = DisposeBag()
    
    
    /// Method to get saved card list from server
    func getCards() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            return
        }
        
        if !rxPaymentCardAPICall.getPaymentCardList_Response.hasObservers {
            
            rxPaymentCardAPICall.getPaymentCardList_Response
                .subscribe(onNext: {response in
                    
                    self.WebServiceResponse(response: response, requestType: RequestType.getCardDetails)
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxPaymentCardAPICall.getListOfPaymentCardsServiceAPICall()
        
    }
    
    
    
    /// Update default card details in Couch DB document
    ///
    /// - Parameters:
    ///   - documentId: default card document id
    ///   - data: default card details
    func updateDefaultCardDocumentDetailsToCouchDB(documentId:String, data:[String:Any]) {
        
        var defaultCard:[Any] = []
        
        if !data.isEmpty {
            
            defaultCard.append(data)
        }
        
        PaymentCardCouchDBManager.sharedInstance.updateDefaultCardDetailsToCouchDBDocument(defaultCard: defaultCard)
    }
    
    
    
    /// Method to get default card details from Couch DB
    ///
    /// - Parameter documentId: default card document id
    /// - Returns: default card details
    func getDefaultCardDocumentDetailsFromCouchDB(documentId:String) -> [Any] {
        
        return PaymentCardCouchDBManager.sharedInstance.getDefaultCardDocumentDetailsFromCouchDB()
    }
    
    
    
    //MARK - WebService Response -
    func WebServiceResponse(response:APIResponseModel, requestType:RequestType)
    {
        if (response.data[SERVICE_RESPONSE.Error] != nil) {
            
            Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
            return
        }
        
        switch response.httpStatusCode
        {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.getCardDetails:
                
                if let dataResponse:[Any] = response.data[SERVICE_RESPONSE.DataResponse] as? [Any] {
                    
                    for cardDetail in dataResponse {
                        
                        let cardDetailModel = CardDetailsModel.init(cardDetail:cardDetail)
                        
                        print(cardDetailModel)
                    }
                    
                }
                
            default:
                break
            }
            
            break
            
        default:
            
            break
            
        }
        
    }
    
}

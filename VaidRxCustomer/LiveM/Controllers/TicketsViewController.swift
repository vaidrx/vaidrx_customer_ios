//
//  TicketsViewController.swift
//  Zendesk
//
//  Created by Vengababu Maparthi on 13/12/17.
//  Copyright © 2017 Vengababu Maparthi. All rights reserved.
//

import UIKit

class TicketsViewController: UIViewController {
    @IBOutlet weak var emptyImage: UIImageView!
    
    @IBOutlet weak var helpCenterTableview: UITableView!
    var zendeskModel = ZendeskModel()
    var zendeskData = ModelClass()
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    weak var delegate: LeftMenuProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupGestureRecognizer()
        
        zendeskModel.getTheTicketData(userID: String(describing:Utility.zendeskRequesterId)) { (success,ticketsData) in
            if success{
                self.zendeskData = ticketsData
                self.helpCenterTableview.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func bactToVc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addNewTicket(_ sender: Any) {
//        self.performSegue(withIdentifier: "toNewTicket", sender: 0)
        
        gotoAddNewTicketVC(ticketId: 0)
        
    }
    
    func gotoAddNewTicketVC(ticketId:Int64) {
        
        let newTicketVC:ZendeskVC = self.storyboard!.instantiateViewController(withIdentifier:
            "NewTicketVC") as! ZendeskVC
        
        newTicketVC.ticketID = ticketId
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController!.pushViewController(newTicketVC, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let nav = segue.destination as! UINavigationController
//        if let nextScene: ZendeskVC = nav.viewControllers.first as! ZendeskVC?{
//            nextScene.ticketID = sender as! Int
//        }
    }
}



///****** tableview datasource*************//
extension TicketsViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.zendeskData.open.count != 0 && self.zendeskData.close.count != 0{
            self.emptyImage.isHidden = true
            return 2
        }else if self.zendeskData.open.count != 0 || self.zendeskData.close.count != 0{
            self.emptyImage.isHidden = true
            return 1
            
        }else{
            self.emptyImage.isHidden = false
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if self.zendeskData.open.count != 0{
                return self.zendeskData.open.count + 1
            }else{
                return self.zendeskData.close.count + 1
            }
        case 1:
            return self.zendeskData.close.count + 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier:"header") as! HeaderTableViewCell?
                if self.zendeskData.open.count != 0{
                    cell?.titleOfStatus.text = "STATUS : OPEN"
                }else{
                    cell?.titleOfStatus.text = "STATUS : CLOSED"
                }
                return cell!
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier:"status") as! StatusTableCell?
                if self.zendeskData.open.count != 0{
                    cell?.subject.text = self.zendeskData.open[indexPath.row-1].subject
                    cell?.time.text = Helper.getTheScheduleViewTimeFormat(timeStamp: self.zendeskData.open[indexPath.row-1].time)
                    cell?.date.text = Helper.getTheOnlyDateFromTimeStamp(timeStamp: self.zendeskData.open[indexPath.row-1].time)
                }else{
                    cell?.subject.text = self.zendeskData.close[indexPath.row-1].subject
                    cell?.time.text = Helper.getTheScheduleViewTimeFormat(timeStamp: self.zendeskData.close[indexPath.row-1].time)
                    cell?.date.text = Helper.getTheOnlyDateFromTimeStamp(timeStamp: self.zendeskData.close[indexPath.row-1].time)
                }
                return cell!
            }
        }else{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier:"header") as! HeaderTableViewCell?
                cell?.titleOfStatus.text = "STATUS : CLOSED"
                return cell!
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier:"status") as! StatusTableCell?
                cell?.subject.text = self.zendeskData.close[indexPath.row-1].subject
                cell?.time.text = Helper.getTheScheduleViewTimeFormat(timeStamp: self.zendeskData.close[indexPath.row-1].time)
                cell?.date.text = Helper.getTheOnlyDateFromTimeStamp(timeStamp: self.zendeskData.close[indexPath.row-1].time)
                return cell!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0{
            return 40
        }else{
            return 70;
        }
    }
}

///***********tableview delegate methods**************//
extension TicketsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            return
        }
        
        if indexPath.section == 0{
            if self.zendeskData.open.count != 0{
                
                self.gotoAddNewTicketVC(ticketId: self.zendeskData.open[indexPath.row - 1].id)
//                self.performSegue(withIdentifier: "toNewTicket", sender: self.zendeskData.open[indexPath.row - 1].id)
            }else{
//                self.performSegue(withIdentifier: "toNewTicket", sender: self.zendeskData.close[indexPath.row - 1].id)
                
                self.gotoAddNewTicketVC(ticketId: self.zendeskData.close[indexPath.row - 1].id)

            }
        }else{
//            self.performSegue(withIdentifier: "toNewTicket", sender: self.zendeskData.close[indexPath.row - 1].id)
            
            self.gotoAddNewTicketVC(ticketId:self.zendeskData.close[indexPath.row - 1].id)

        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension TicketsViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension TicketsViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}




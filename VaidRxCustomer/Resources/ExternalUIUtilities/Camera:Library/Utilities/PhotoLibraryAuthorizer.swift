//
//  PhotoLibraryAuthorizer.swift
//  ALCameraViewController
//
//  Created by Alex Littlejohn on 2016/03/26.
//  Copyright © 2016 zero. All rights reserved.
//

import UIKit
import Photos

public typealias PhotoLibraryAuthorizerCompletion = (NSError?) -> Void

class PhotoLibraryAuthorizer {

    private let errorDomain = "com.zero.imageFetcher"

    private let completion: PhotoLibraryAuthorizerCompletion

    init(completion: @escaping PhotoLibraryAuthorizerCompletion) {
        self.completion = completion
        if #available(iOS 14, *) {
            handleAuthorization(status: PHPhotoLibrary.authorizationStatus())
        } else {
            // Fallback on earlier versions
        }
    }
    
    func onDeniedOrRestricted(completion: PhotoLibraryAuthorizerCompletion) {
        let error = errorWithKey("error.access-denied", domain: errorDomain)
        completion(error)
    }
    
    @available(iOS 14, *)
    func handleAuthorization(status: PHAuthorizationStatus) {
        switch PHPhotoLibrary.authorizationStatus(for: .readWrite) {
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization(handleAuthorization)
            break
        case .authorized:
            DispatchQueue.main.async {
                self.completion(nil)
            }
            break
        case .denied, .restricted:
            DispatchQueue.main.async {
                self.onDeniedOrRestricted(completion: self.completion)
            }
            break
        case .limited:
            break
        }
    }
}

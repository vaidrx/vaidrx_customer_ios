//
//  SetPasswordCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


extension SetPasswordController  {
    
    /// Initiall SetUp For Animation
    func initiallSetup() {
        
        if forgetPass {
            oldPassYConstrain.constant = 0
            oldPasswordTF.transform = CGAffineTransform(scaleX: 0, y: 0)
            oldPassView.isHidden = true
            newPassView.transform = CGAffineTransform(scaleX: 0, y: 0)
            reenterPassView.transform = CGAffineTransform(scaleX: 0, y: 0)
        }else {
            oldPassYConstrain.constant = 25
            oldPasswordTF.transform = .identity
            oldPassView.isHidden = false
            newPassView.transform = .identity
            reenterPassView.transform = .identity
        }
        
        if loadingViewHeight.constant != self.continueButtonOutlet.frame.size.width {
            continueButtonOutlet.isEnabled = true
        }
        
        
        
    }
    
    /// animate the view at bottom of text Fields
    func animateBottomView() {
        if self.newPassView.transform != .identity {
            UIView.animate(withDuration: 1.4, animations: {
                self.newPassView.transform = .identity
                self.reenterPassView.transform = .identity
            })
        }
    }
    
    /// Animate Button Loading for user Id password
    ///
    /// - Parameter value: value to decide its percent of loading
    func animateButtonLoading(_ value: Int) {
        self.view.layoutIfNeeded()
        switch value {
        case 0:
            self.loadingViewHeight.constant = 0
            UIView.animate(withDuration: 0.6, animations: {
                self.view.layoutIfNeeded()
            })
            
        case 1:
            self.loadingViewHeight.constant = self.continueButtonOutlet.frame.size.width / 2
            UIView.animate(withDuration: 0.6, animations: {
                self.view.layoutIfNeeded()
            })
            
        case 2:
            self.loadingViewHeight.constant = self.continueButtonOutlet.frame.size.width
            continueButtonOutlet.isEnabled = true
            UIView.animate(withDuration: 0.6, animations: {
                self.view.layoutIfNeeded()
            })
            
        default:
            break
        }
    }
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard?.instantiateViewController(withIdentifier: idntifier)
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.pushViewController(dstVC ?? UIViewController(), animated: false)
    }
    
    
    func validatePassword() {
        if newPassTF.text == reEnterPassTF.text {
            changePassword()
        }else {
            Helper.alertVC(title: "Alert", message: ALERTS.ChangePassword)
        }
    }
    
    
}

//
//  LandingModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

struct Landing {
    
    var phoneNumberText = ""
    var countryCode     = ""
    var triggerValue = 0
}

//
//  LocationManager.swift
//  Trustpals
//
//  Created by Vasant Hugar on 05/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

@objc protocol LocationManagerDelegate {
    
    /// When Update Location
    @objc optional func didUpdateLocation(location: LocationManager)
    
    /// When Failed to Update Location
    @objc optional func didFailToUpdateLocation()
    
    /// Did Change Authorized
    ///
    /// - Parameter authorized: YES or NO
    @objc optional func didChangeAuthorization(authorized: Bool)
}

class LocationManager: NSObject {
    
    var latitute : Double = 0.0
    var longitude : Double = 0.0
    var address = ""
    var name = ""
    var city = ""
    var state = ""
    var country = ""
    var zipcode = ""
    var street = ""
    var countryCode = ""
    
    var locationObj: CLLocationManager? = nil
    var delegate: LocationManagerDelegate? = nil
    
    let geoCoder = CLGeocoder()
    
    var isInitial = true
    
    static var obj:LocationManager? = nil
    
    class func sharedInstance() -> LocationManager {
        
        if obj == nil {
            
            obj = LocationManager()
        }
        
        return obj!
    }

    
    override init() {
        
        super.init()
        locationObj = CLLocationManager()
        locationObj?.delegate = self
        locationObj?.requestWhenInUseAuthorization()
        
    }
    /// Start Location Update
    func start() {
        locationObj?.startUpdatingLocation()
    }
    /// Stop Location Update
    func stop() {
        locationObj?.stopUpdatingLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch(status) {
            
            case .notDetermined:
                
                delegate?.didChangeAuthorization!(authorized: false)
                
                break
                
            case .restricted, .denied:
                
                print("No access")
                
                let view = LocationEnableView.shared
                view.show()
                
                delegate?.didChangeAuthorization!(authorized: false)
                
                break
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("Access")
                
                locationObj?.startUpdatingLocation()
                
                let view = LocationEnableView.shared
                view.hide()
                
                delegate?.didChangeAuthorization!(authorized: true)
                
                break
        }
    }
    
    
    
    /// CLLocation manager delegate method for successfuly fetched location details
    ///
    /// - Parameters:
    ///   - manager: CLLocationManager object
    ///   - locations: location details
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.first != nil {
            
            stop()
            
            let location = locations.first
            
            latitute = (location?.coordinate.latitude)!
            longitude = (location?.coordinate.longitude)!
            
            
            GMSGeocoder().reverseGeocodeCoordinate((location?.coordinate)!, completionHandler: { (gmsAddressResponse, error) in
                
                if let addressDetails = gmsAddressResponse?.firstResult() {
                    
                    DDLogVerbose("Address Details:\(addressDetails)")
                    
                    // Current Address
                    if let currentAddress = addressDetails.lines {
                        
                        print("Current Address: \(currentAddress)")
                        self.address = (currentAddress.joined(separator: ", "))
                        
                        if self.address.hasPrefix(", ") {
                            
                            self.address = self.address.substring(2)
                        }
                        
                        if self.address.hasSuffix(", ") {
                            
                            let endIndex = self.address.index(self.address.endIndex, offsetBy: -2)
                            
                            self.address = self.address.substring(to: endIndex)
                        }
                    }
                    
                    // Location name
                    if let name = addressDetails.thoroughfare {
                        self.name = name
                    }
                    
                    // Street address
                    if let street = addressDetails.subLocality {
                        self.street = street
                    }
                    
                    // City
                    if let city = addressDetails.locality {
                        self.city = city
                    }
                    
                    // Zip code
                    if let zip = addressDetails.postalCode {
                        self.zipcode = zip
                    }
                    
                    // Country
                    if let country = addressDetails.country {
                        self.country = country
                    }
                    
                    
                    if self.isInitial {
                    
                        self.isInitial = false
                        self.delegate?.didUpdateLocation!(location: self)
                    }

                    
                } else {
                    
                    DDLogError("Address Fetch Error: \(String(describing: error?.localizedDescription))")
                }
                
            })
            
            /*geoCoder.reverseGeocodeLocation(location!) {
                
                (placemarks, error) -> Void in
                
                if let arrayOfPlaces: [CLPlacemark] = placemarks as [CLPlacemark]! {
                    
                    // Place details
                    if let placemark: CLPlacemark = arrayOfPlaces.first {
                        
                        // Address dictionary
                        print("Address Dict :\(placemark.addressDictionary!)")
                        
                        // Current Address
                        if let currentAddress = placemark.addressDictionary?["FormattedAddressLines"] {
                            
                            print("Current Address: \(currentAddress)")
                            self.address = ((currentAddress as? Array)?.joined(separator: ", "))!
                        }
                        
                        // Location name
                        if let name = placemark.addressDictionary?["Name"] as? String {
                            self.name = name
                        }
                        
                        // Street address
                        if let street = placemark.addressDictionary?["Thoroughfare"] as? String {
                            self.street = street
                        }
                        
                        // City
                        if let city = placemark.addressDictionary?["City"] as? String {
                            self.city = city
                        }
                        
                        // Zip code
                        if let zip = placemark.addressDictionary?["ZIP"] as? String {
                            self.zipcode = zip
                        }
                        
                        // Country
                        if let country = placemark.addressDictionary?["Country"] as? String {
                            self.country = country
                        }
                        
                        if Google.myCOuntry.characters.count == 0 {
                            Google.myCOuntry = self.country
                        }
                        
                        if isInitial {
                            
                            self.delegate?.didUpdateLocation!(location: self)
                        }
                        
                    }
                }
            }*/
        }
    }
    
    
    
    /// CLLocation manager delegate method for error to fetch location details from CLLocation
    ///
    /// - Parameters:
    ///   - manager: CLLocationManager object
    ///   - error: error details
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("\nDid fail location : %@\n",error.localizedDescription)
        delegate?.didFailToUpdateLocation!()
    }
}

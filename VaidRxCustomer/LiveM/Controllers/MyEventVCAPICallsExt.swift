//
//  MyEventVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension MyEventViewController {
    
    func getBookingsAPI() {
        
        self.topView.isHidden = true
        self.bottomView.isHidden = true
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        myEventViewModel.getMyEventsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.getAllApptDetails)
        }

    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .getAllApptDetails:
                            
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)


                }
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                break
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType
                {
                    case RequestType.getAllApptDetails:
                        
                        arrayOfPendingBookings = []
                        arrayOfUpcomingBookings = []
                        arrayOfPastBookings = []
                        
                        if let dataRes = dataResponse as? [String:Any] {
                            
                            if let pendingBookings = dataRes["pending"] as? [Any] {
                                
                                for eachBooking in pendingBookings {
                                    
                                    let bookingEventModel = BookingDetailsModel.init(bookingEventDetails: eachBooking)
                                    arrayOfPendingBookings.append(bookingEventModel)
                                }
                                
                            }
                            
                            if let upcomingBookings = dataRes["upcoming"] as? [Any] {
                                
                                for eachBooking in upcomingBookings {
                                    
                                    let bookingEventModel = BookingDetailsModel.init(bookingEventDetails: eachBooking)
                                    arrayOfUpcomingBookings.append(bookingEventModel)
                                }
                                
                            }
                            
                            if let pastBookings = dataRes["past"] as? [Any] {
                                
                                for eachBooking in pastBookings {
                                    
                                    let bookingEventModel = BookingDetailsModel.init(bookingEventDetails: eachBooking)
                                    arrayOfPastBookings.append(bookingEventModel)
                                }
                                
                            }
                            
                            buttonEnable()
                            
                            self.pendingTableView.reloadData()
                            self.upcomingTableView.reloadData()
                            self.pastTableView.reloadData()
                            
                            self.topView.isHidden = false
                            self.bottomView.isHidden = false
                            
                        }
                        
                        break
                        
                    default:
                        
                        break
                }
                
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                    
                }
                
                
                break
        }
        
    }
    
}

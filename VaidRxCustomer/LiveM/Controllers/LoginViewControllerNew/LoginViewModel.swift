//
//  LoginViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 17/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


/// Login View Model Class to maintain Login View Data
class LoginViewModel{
    
    var loginModel:LoginModel!//Login Model used to bind login data(We can use this model in any class)
    
    var emailText = Variable<String>("")
    var passwordText = Variable<String>("")
    var loginType = LoginType.Default
    var facebookId = ""
    var googleId = ""
    var socialEmail = "iserve@3embed@gmail.com"
    var loginText = ""
    
    let disposebag = DisposeBag()

    let rxLoginAPICall = LoginAPI()
    
    func createLoginModel() {
        
//        if loginType.rawValue != 1{
//           loginText = socialEmail
//        }
//        else{
//            loginText = emailText.value
//        }
        
        loginModel = LoginModel()
        loginModel.emailText =  emailText.value
        loginModel.passwordText = passwordText.value
        loginModel.loginType = loginType
        loginModel.facebookId = facebookId
        loginModel.googleId = googleId
    }
    
    
    func loginAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        createLoginModel()        
        
        rxLoginAPICall.loginServiceAPICall(loginModel: loginModel)
        
        if !rxLoginAPICall.login_Response.hasObservers {
            
            rxLoginAPICall.login_Response
            .subscribe(onNext: {response in
                
                if (response.data[SERVICE_RESPONSE.Error] != nil) {
                    
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    return
                }
                
                completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                
                
            }, onError: {error in
                
            }).disposed(by: disposebag)
        }
        

    }
    
    func saveCurrentUserDetails(dataResponse:[String:Any]) {//Current user details
        
        let userDefaults:UserDefaults = UserDefaults.standard
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.Sid]), forKey: USER_DEFAULTS.USER.CUSTOMERID)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.FirstName]), forKey: USER_DEFAULTS.USER.FIRSTNAME)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.LastName]), forKey: USER_DEFAULTS.USER.LASTNAME)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.SessionToken]), forKey: USER_DEFAULTS.TOKEN.SESSION)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.Email]), forKey: USER_DEFAULTS.USER.EMAIL)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.CurrencyCode]), forKey: USER_DEFAULTS.USER.CURRENCYSYMBOL)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.ProfilePic]), forKey: USER_DEFAULTS.USER.PIC)
        
        AppDelegate().defaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.ReferralCode]), forKey: USER_DEFAULTS.USER.REFERRAL_CODE)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.StripeAPIKey]), forKey: USER_DEFAULTS.PAYMENT_GATEWAY.API_KEY)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.FCMTopic]), forKey: USER_DEFAULTS.USER.FCMTopic)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.ZenDeskRequesterID]), forKey: USER_DEFAULTS.USER.ZenDeskRequesterID)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.PharmacyName]), forKey: USER_DEFAULTS.USER.PHARMACY_NAME)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.PharmacyId]), forKey: USER_DEFAULTS.USER.PHARMACY_ID)
        
        
        userDefaults.synchronize()
        
        
        //Connect to MQTT
        AppDelegate().connectToMQTT()

    }
    
}

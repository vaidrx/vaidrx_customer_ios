//
//  ChatViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 18/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Photos
import Kingfisher

import CoreLocation

class ChatViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var inputText: UITextView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var widthMore: NSLayoutConstraint!
    @IBOutlet weak var heightOFContainer: NSLayoutConstraint!
    @IBOutlet weak var bottomContainerConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var navigationTopView: UIView!
    
    @IBOutlet weak var musicianNameLabel: UILabel!
    
    @IBOutlet weak var eventIdLabel: UILabel!
    
    @IBOutlet weak var bottomInputView: UIView!
    
    @IBOutlet weak var sendButton: UIButton!
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    // Shared instance object for gettting the singleton object
    static var obj:ChatViewController? = nil
    
    class func sharedInstance() -> ChatViewController {
        
        return obj!
    }

    
    var items = [Message]()
    let barHeight: CGFloat = 50
    var canSendLocation = true
    var chatModel = Chat()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var bookingId:String!
    var musicianId:String!
    var musicianName:String = ""
    var musicianImageURL:String = ""
    var shadowView: UIView!

    var value = 0

    let mqttChatManager = MQTTSimpleSingleChatManager.sharedInstance()
    
    let imagePickerObj = UIImagePickerController()

    
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    //MARK: ViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ChatViewController.obj = self
        
        self.customization()
        
        if ChatCouchDBManager.sharedInstance.getChatDocumentDetailsFromCouchDB(bookingId: bookingId).count == 0 {
            
            Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
            
        } else {
            
            maintainPreviousChatDFetailsFromCouchDB()
        }
        
        self.tableView.addPullRefresh { [weak self] in
            // some code
            sleep(1)
            self?.value += 1
            self?.fetchData(val: String(describing:self!.value))
        }
        fetchData(val:"0")
    }
    
    

    
    override func viewWillAppear(_ animated: Bool) {
        
        mqttChatManager.delegate = self
        appDelegate?.keyboardDelegate = self
        
        self.eventIdLabel.text = "\(ALERTS.BOOKING_FLOW.EventID)" + " : " + bookingId
        self.musicianNameLabel.text = musicianName
        
        setupGestureRecognizer()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatViewController.keyboardDidShow),
                                               name:Notification.Name.init(UIResponder.keyboardDidShowNotification.rawValue),
                                               object: nil);
        
        
        
        
        
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutIfNeeded()
        
        adjustFrames()
        updateBottomInputViewShadow()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        appDelegate?.keyboardDelegate = nil
        mqttChatManager.delegate = nil
//        imagePickerObj.delegate = nil
    }
    
    
    
    
    
    //MARK: Methods
    
    func customization() {
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 66.0
        self.navigationItem.setHidesBackButton(true, animated: false)
        
    }
    
    func updateBottomInputViewShadow() {
        
        self.bottomInputView.layer.shadowColor = UIColor.darkGray.cgColor
        self.bottomInputView.layer.shadowPath = UIBezierPath(roundedRect: self.bottomInputView.bounds, cornerRadius: self.bottomInputView.layer.cornerRadius).cgPath
        self.bottomInputView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        self.bottomInputView.layer.shadowOpacity = 0.3
        self.bottomInputView.layer.shadowRadius = 5.0
        self.bottomInputView.layer.masksToBounds = true
        
        self.sendButton.layer.shadowColor = UIColor.darkGray.cgColor
        self.sendButton.layer.shadowPath = UIBezierPath(roundedRect: self.bottomInputView.bounds, cornerRadius: self.bottomInputView.layer.cornerRadius).cgPath
        self.sendButton.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        self.sendButton.layer.shadowOpacity = 0.3
        self.sendButton.layer.shadowRadius = 5.0
        self.sendButton.layer.masksToBounds = true

    }
    
    func maintainPreviousChatDFetailsFromCouchDB() {
        
        let prevMessages = chatModel.updateTheChatData(chatDict: ChatCouchDBManager.sharedInstance.getChatDocumentDetailsFromCouchDB(bookingId: bookingId) as! [[String : Any]])
        
        self.items = prevMessages
        self.items.sort{ $0.timestamp < $1.timestamp }
        
        DispatchQueue.main.async {
            
            let state = self.items.isEmpty
            
            if state == false {
                
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                //                self.tableView.scrollRectToVisible(CGRect.init(x: self.tableView.contentSize.width - 1, y: self.tableView.contentSize.height - 1, width: 1, height: 1), animated: false)
            }
        }
    }
    
    //Downloads messages
    func fetchData(val:String) {
        chatModel.getTheChatHistory(bookingID: self.bookingId ,indexVal: val)  {[weak weakSelf = self]  (success, messagedDict) in
            if success{
                if val == "0"{
                    weakSelf?.items = messagedDict
                }else{
                    weakSelf?.items += messagedDict
                }
                weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
                DispatchQueue.main.async {
                    if let state = weakSelf?.items.isEmpty, state == false {
                        weakSelf?.tableView.reloadData()
                        if val == "0"{
                            weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                        }
                        self.tableView.stopPullRefreshEver()
                    }
                }
            }else{
                self.tableView.stopPullRefreshEver()
            }
        }
    }
    
    func updateTheMessageSendToServer(type:MessageType,
                                      timeStamp:Int64,
                                      content:String,
                                      bookingId:String,
                                      musicianId:String,completion:@escaping(Bool)->()) {
        
        
        
        self.chatModel.updateToServerchat(type: type,
                                          timeStamp: timeStamp,
                                          content: content,
                                          bookingId: bookingId,
                                          musicianId: musicianId,
                                          completionHandler: { (succeeded) in
            if succeeded{
                completion(true)
            }else{
                completion(false)
            }
        })
    }
    
    func composeMessage(type: MessageType, content: Any)  {
        
        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int64(Date().timeIntervalSince1970), isRead: false)
        
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.heightOFContainer.constant =   40
        })
        
        let messageType = message.type
        let content =   message.content
        let timestamp = message.timestamp
        
        switch messageType {
        case .photo:
            Helper.showPI(_message: "uploading...")
            if let image = message.content as? UIImage{
                let data = image.jpegData(compressionQuality: 1.0)
                let imageDict:[String:Any] = ["bookingId":bookingId]
                ImageUploadAPI().requestWith(imageData: data, parameters: imageDict, onCompletion: { (success,imageUrl) in
                    if success{
                        
                        self.updateTheMessageSendToServer(type: .photo,
                                                          timeStamp: timestamp,
                                                          content: imageUrl,
                                                          bookingId: self.bookingId,
                                                          musicianId: self.musicianId,
                                                          completion: { (success) in
                            if success{
                                let message = Message.init(type: .photo, content: imageUrl, owner: .sender, timestamp: timestamp, isRead: false)
                                self.items.append(message)
                                Helper.hidePI()
                                DispatchQueue.main.async {
                                    self.tableView.reloadData()
                                    
//                                    self.tableView.scrollRectToVisible(CGRect.init(x: self.tableView.contentSize.width - 1, y: self.tableView.contentSize.height - 1, width: 1, height: 1), animated: false)

                                    
                                    self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                                }
                            }
                        })
                    }
                }, onError: { (error) in
                    Helper.alertVC(title: ALERTS.Message, message: (error?.localizedDescription)!)
                })
            }
            break
        case .location:
            break
        default:
            items.append(message)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
//                self.tableView.scrollRectToVisible(CGRect.init(x: self.tableView.contentSize.width - 1, y: self.tableView.contentSize.height - 1, width: 1, height: 1), animated: false)
            }
            
           
            Helper.showPI(_message: "sending...")
            chatModel.updateToServerchat(type: .text,
                                         timeStamp: timestamp,
                                         content: content as! String,
                                         bookingId: bookingId,
                                         musicianId: musicianId,
                                         completionHandler: { (succeeded) in
                if succeeded{
                    Helper.hidePI()
                }else{
                    Helper.hidePI()
                }
            })
            break
        }
    }

    
    /// Current time stamp
    static var currentTimeStamp: String {
        return String(format: "%0.0f", Date().timeIntervalSince1970 * 1000)
    }
    
    
    // Scroll to bottom of table view for messages
    func scrollToBottomMessage() {
        if self.items.count == 0 {
            return
        }
        let bottomMessageIndex = IndexPath(row: items.count - 1, section: 0)
        tableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
    }
    
}





extension ChatViewController:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - Keyboard Delegate -
extension ChatViewController:KeyboardDelegate {
    
    //MARK: NotificationCenter handlers
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.bottomContainerConstraint.constant = height + 5
                self.view.layoutIfNeeded()
            })
        }

    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        
        self.scrollToBottomMessage()
    }
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomContainerConstraint.constant = 5
            self.view.layoutIfNeeded()
        })
    }

}




extension ChatViewController {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
//        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension ChatViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
}


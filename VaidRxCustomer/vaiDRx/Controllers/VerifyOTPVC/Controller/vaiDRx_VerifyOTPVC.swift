//
//  vaiDRx_VerifyOTPVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class vaiDRx_VerifyOTPVC: UIViewController, KeyboardDelegate {
    
    // MARK: - Outlets
    
    // Layout Constraint
    @IBOutlet weak var scrollContentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var resendCodeButtonBottomConstraint: NSLayoutConstraint!
    
    //ScrollView
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollBackView: UIView!

    // text Field
    @IBOutlet weak var code4: UITextField!
    @IBOutlet weak var code3: UITextField!
    @IBOutlet weak var code2: UITextField!
    @IBOutlet weak var code1: UITextField!
    
    // UIView
    @IBOutlet weak var code1View: UIView!
    @IBOutlet weak var code2View: UIView!
    @IBOutlet weak var code3View: UIView!
    @IBOutlet weak var code4View: UIView!
    
    //Button
    @IBOutlet weak var resendCodeButton: UIButton!
    
    //Image View
    @IBOutlet weak var otpImage4: UIImageView!
    @IBOutlet weak var otpImage3: UIImageView!
    @IBOutlet weak var otpImage2: UIImageView!
    @IBOutlet weak var otpImage1: UIImageView!

    @IBOutlet weak var dispTextLbl: UILabel!
    // MARK: - Variable Decleration -
    var activeTextField = UITextField()
    
     var countOtp = 0

    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var verifyPhoneViewModel = VerifyPhoneViewModel()
    
    var dataFromLandingVC:[String:Any] = [:]
    var userID: [String:Any] = [:]
    
    var isComingFrom = ""
    var countTimer: Int = 120
    
    var manualEntry = false
    var phoneNumber:String!
    var countryCode:String!

    
    var registerViewModel: RegisterViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    Helper.hideNavBarShadow(vc: self)
//    initiallSetup()
    self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
    setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        
        let phoneNum = dataFromLandingVC["phone"] as! String
        let firstThree = phoneNum.dropLast(7)
        let firstSix = phoneNum.dropLast(4)
        let midNum = firstSix.dropFirst(3)
        let lastFour = phoneNum.dropFirst(6)
        dispTextLbl.text = "Enter the 4-digit code sent to you at " + "(\(firstThree))" + " " + midNum + "-" + lastFour
    }
    
    override func viewDidAppear(_ animated: Bool) {

        initiallAnimation()
        countryCode = dataFromLandingVC[USER_DEFAULTS.USER.COUNTRY_CODE] as! String
        phoneNumber = dataFromLandingVC[USER_DEFAULTS.USER.MOBILE] as! String
        userID = [ SERVICE_RESPONSE.Sid : dataFromLandingVC[SERVICE_RESPONSE.Sid] as! String]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initiallSetup()
//        self.hideNavigation()
         self.navigationController?.setNavigationBarHidden(false, animated: true)
        clearTextField()
        addTimer()
        appDelegate?.keyboardDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        appDelegate?.keyboardDelegate = nil
        
//        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = nil
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = .clear //UIColor(hex: "05C1C9") //UIColor.clear
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func hideNavigation() {
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = APP_COLOR //.clear //UIColor(hex: "05C1C9") //UIColor.clear
    }

    
}



//
//  ConfirmBookingVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmBookingScreen:UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arrayOfHeaderTitles.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
            case 0,1,3,5,6:
                
                return 1
        
            case 2:
            
                if CBModel.appointmentLocationModel.bookingType == BookingType.Default  {
                    
                    return 0
                }
                return 1
            
            case 4:
                
                if CBModel.appointmentLocationModel.bookingType == BookingType.Default  {
                    
                    return 1
                }
                return 0
                
            case 7:
                
                return 3
                
            default:
                
                return 0
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if CBModel.appointmentLocationModel.bookingType == BookingType.Default && section == 2  {
            
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width:0, height: 0))
            return view
        }
        
        if CBModel.appointmentLocationModel.bookingType == BookingType.Schedule && section == 4  {
            
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width:0, height: 0))
            return view
        }

        
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width, height: 35))

        
        let label = UILabel.init(frame: CGRect.init(x: 15, y: 0, width: tableView.bounds.size.width-30, height: 35))
        label.font = UIFont.init(name: FONTS.HindMedium, size: 15)
        label.text = arrayOfHeaderTitles[section]
        
        label.textColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
        
        view.addSubview(label)
        
        
        return view
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
        return view
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if CBModel.appointmentLocationModel.bookingType == BookingType.Default && section == 2  {

            return 0.01
        }
        
        if CBModel.appointmentLocationModel.bookingType == BookingType.Schedule && section == 4  {

            return 0.01
        }
        
        return 35.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            
            gigTimeCell = tableView.dequeueReusableCell(withIdentifier: "gigTimeCell", for: indexPath) as! MDGigTimeTableViewCell
            
            
            gigTimeCell.CBModel = CBModel
            gigTimeCell.isConfirmBooking = true
            
            gigTimeCell.collectionView.reloadData()
            
            
            return gigTimeCell
            
        case 1:
            
            addressCell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as! CBAddressTableViewCell
            
            addressCell.changeButton.addTarget(self, action: #selector(changeAddressButtonAction), for: .touchUpInside)
            addressCell.addressLabel.text = CBModel.appointmentLocationModel.pickupAddress
            
            return addressCell
        
        case 2:
            
//            if selectedDateCell != nil {
            
                selectedDateCell = tableView.dequeueReusableCell(withIdentifier: "selectedDateCell", for: indexPath) as! CBSelectedDateTableViewCell
                
                let dateformat = DateFormatter()
                dateformat.dateFormat = "dd MMM yyyy hh:mm a"
                
                selectedDateCell.dateLabel.text = dateformat.string(from: CBModel.appointmentLocationModel.scheduleDate)
//            }
            
            return selectedDateCell
        
        case 3:
            
//            if eventsCell != nil {
            
                eventsCell = tableView.dequeueReusableCell(withIdentifier: "eventsCell", for: indexPath) as! MDEventsTableViewCell
            
                eventsCell.CBModel = CBModel
                eventsCell.isConfirmBooking = true
            
                eventsCell.collectionView.reloadData()
//            }
            
            return eventsCell
            
        case 4:
            
//            if eventStartCell != nil {
            
                eventStartCell = tableView.dequeueReusableCell(withIdentifier: "eventStartCell", for: indexPath) as! CBEventStartTableViewCell
                
                
                eventStartCell.CBModel = CBModel
                eventStartCell.noEventStartTimeBackView.isHidden = true
                eventStartCell.collectionView.isHidden = false
                
                eventStartCell.collectionView.reloadData()
//            }
            
            return eventStartCell
            
        case 5:
            
            let paymentCell:CBPaymentMethodCell = tableView.dequeueReusableCell(withIdentifier: "paymentMethodCell", for: indexPath) as! CBPaymentMethodCell
            
            paymentCell.addCardButton.addTarget(self, action: #selector(addPaymentButtonAction), for: .touchUpInside)
            
            paymentCell.cardButton.addTarget(self, action: #selector(paymentSelectedButtonAction), for: .touchUpInside)
            paymentCell.cashButton.addTarget(self, action: #selector(paymentSelectedButtonAction), for: .touchUpInside)
            
            paymentCell.showPaymentMethodDetails(CBModel: CBModel)
            
            return paymentCell
            
        case 6:
            
            promocodeCell = tableView.dequeueReusableCell(withIdentifier: "promocodeCell", for: indexPath) as! CBPromocodeTableViewCell
            
            promocodeCell.applyButton.addTarget(self, action: #selector(applyPromocodeButtonAction), for: .touchUpInside)
            promocodeCell.showPromocodeDetails(CBModel: CBModel)
            
            return promocodeCell
            
        default:
            
            if indexPath.row == 2 {
                
                let paymentTotalCell:CBTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "paymentTotalCell", for: indexPath) as! CBTotalTableViewCell
                
                let totalValue =  (CBModel.selectedGigTimeAmount + CBModel.serviceFeeAmount) - CBModel.discountValue
                
                paymentTotalCell.totalValueLabel.text = CBModel.providerFullDetalsModel.currencySymbol + String(format:" %.2f",totalValue)
                
                return paymentTotalCell
                
            } else {
                
                let paymentBreakdownCell:CBPaymentBreakDownCell = tableView.dequeueReusableCell(withIdentifier: "paymentBreakdownCell", for: indexPath) as! CBPaymentBreakDownCell
                
                switch indexPath.row {
                    
                case 0:
                    
                    let gigTimeDict = CBModel.providerFullDetalsModel.gigTimeArray[CBModel.selectedGigTimeTag] as! [String:Any]
                    
                    
//                    paymentBreakdownCell.titleLabel.text = String(format:"%@ %@ / Gig", GenericUtility.strForObj(object:gigTimeDict["name"]), GenericUtility.strForObj(object:gigTimeDict["unit"]))
                    
                    paymentBreakdownCell.titleLabel.text = String(format:"%@ %@", GenericUtility.strForObj(object:gigTimeDict["name"]), GenericUtility.strForObj(object:gigTimeDict["unit"]))
                    
                    paymentBreakdownCell.amountLabel.text = String(format:"%@ %.2f",CBModel.providerFullDetalsModel.currencySymbol ,Double(GenericUtility.strForObj(object:gigTimeDict["price"]))!)
                    
                    
                    
                case 1:
                    
                    if CBModel.discountType.length > 0 {
                        
                        if CBModel.discountType == "Percent" {
                            
                            paymentBreakdownCell.titleLabel.text = "Discount(\(CBModel.percentDiscountValue)%)"
                            
                        } else {
                            
                            paymentBreakdownCell.titleLabel.text = "Discount"
                        }
                        
                        paymentBreakdownCell.amountLabel.text = CBModel.providerFullDetalsModel.currencySymbol + String(format:" %.2f",CBModel.discountValue)
                        
                    } else {
                        
                        paymentBreakdownCell.titleLabel.text = "Discount"
                        paymentBreakdownCell.amountLabel.text = CBModel.providerFullDetalsModel.currencySymbol + " 0.00"
                        
                    }
                    
                default:
                    break
                    
                }
                
                return paymentBreakdownCell
                
            }
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height:CGFloat
        
        switch indexPath.section {
            
        case 0:
            return 90
            
            
        case 1:
            
            let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width - 103, height: 0))
            
            label.font = UIFont.init(name: FONTS.HindRegular, size: 12)
            label.text = CBModel.appointmentLocationModel.pickupAddress
            height = Helper.measureHeightLabel(label, width: SCREEN_WIDTH! - 103)
            
            return height + 51 + 4
            
        case 2:
            return 70
            
        case 3:
            return 130
            
            
        case 4:
            return 100
            
            
        case 5:
            
            if CBModel.paymentmethodTag == 0 {
                
                return 120
                
            } else {
                
                return 72
            }
            
            
        case 6:
            return 75
            
            
        case 7:
            return 30
            
            
        default:
            return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //        cell.contentView.transform = CGAffineTransform(translationX: -500, y: 0)
        //
        //        UIView.animate(withDuration: 0.8,
        //                       delay: 0.2,
        //                       usingSpringWithDamping: 0.5,
        //                       initialSpringVelocity: 4,
        //                       options: [],
        //                       animations: {
        //
        //            cell.contentView.transform = .identity
        //
        //        }) { (success) in
        //
        //        }
        
//        cell.contentView.transform = CGAffineTransform(scaleX: 1.4, y:1.4)
//
//        UIView.animate(withDuration: 0.8,
//                       delay: 0,
//                       usingSpringWithDamping: 0.5,
//                       initialSpringVelocity: 3,
//                       options: [],
//                       animations: {
//
//                        cell.contentView.transform = .identity
//
//        }, completion: { (completed) in
//
//
//        })
        
        
    }
    
    
}

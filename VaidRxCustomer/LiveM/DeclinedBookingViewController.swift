//
//  DeclinedBookingViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class DeclinedBookingViewController: UIViewController {
    
    
    @IBOutlet var navigationLeftButton: UIButton!
    
    @IBOutlet var eventIdLabel: UILabel!
    
    @IBOutlet var eventDateLabel: UILabel!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var scrollContentView: UIView!
    
    @IBOutlet var musicianImageView: UIImageView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var musicianNameLabel: UILabel!
    
    @IBOutlet var evenNameLabel: UILabel!
    
    @IBOutlet var eventLocationLabel: UILabel!
    
    @IBOutlet var gigNameLabel: UILabel!
    
    @IBOutlet var gigValueLabel: UILabel!
    
    @IBOutlet var serviceFeeLabel: UILabel!
    
    @IBOutlet var serviceFeeValueLabel: UILabel!
    
    @IBOutlet var discountLabel: UILabel!
    
    @IBOutlet var discountValueLabel: UILabel!
    
    @IBOutlet var totalLabel: UILabel!
    
    @IBOutlet var totalValueLabel: UILabel!
    
    @IBOutlet var eventStatusLabel: UILabel!
    
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet weak var topStatusView: UIView!
    
    @IBOutlet weak var topStatusLabel: UILabel!
    
    @IBOutlet var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var cancellationFeeLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var cancellationFeeLabelValueHeightConstraint: NSLayoutConstraint!

    @IBOutlet var detailsBackViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cancellationReasonbackView: UIView!
    
    @IBOutlet weak var cancellationReasonLabel: UILabel!
    
    
    var bookingDetailModel:BookingDetailsModel!
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!
    let declinedBookingViewModel = DeclinedBookingViewModel()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
   
    
    // Shared instance object for gettting the singleton object
    static var obj:DeclinedBookingViewController? = nil
    
    class func sharedInstance() -> DeclinedBookingViewController {
        
        return obj!
    }
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DeclinedBookingViewController.obj = self
        
        getBookingDetailsAPI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isTranslucent = false
        setupGestureRecognizer()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        updateScrollContent()
    }
    
    func updateScrollContentWithCancellationReason() {
        
        if (cancellationReasonbackView.frame.size.height + cancellationReasonbackView.frame.origin.y) + 25 > scrollView.frame.size.height {
            
            scrollContentViewHeightConstraint.constant = (cancellationReasonbackView.frame.size.height + cancellationReasonbackView.frame.origin.y) + 25 - scrollView.frame.size.height;
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0
        }
        
        scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
        
        self.view.layoutIfNeeded()
    }
    
    func updateScrollContentWithOutCancellationReason() {
        
        if (bottomView.frame.size.height + bottomView.frame.origin.y) + 25 > scrollView.frame.size.height {
            
            scrollContentViewHeightConstraint.constant = (bottomView.frame.size.height + bottomView.frame.origin.y) + 25 - scrollView.frame.size.height;
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0
        }
        
        scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
        
        self.view.layoutIfNeeded()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }

    
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension DeclinedBookingViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getParticularApptDetails.rawValue:
            
            getBookingDetailsAPI()
            
            break
            
        default:
            break
        }
    }
    
}

extension DeclinedBookingViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension DeclinedBookingViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            self.navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}


//
//  VRDateOfBirthViewController.swift
//  GoTasker
//
//  Created by 3Embed on 16/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VRDateOfBirthViewController: UIViewController {

    @IBOutlet weak var dateView: UIView!
    
    @IBOutlet weak var datePickerOutlet: UIDatePicker!
    @IBOutlet weak var datePickerTextField: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnNextBottomConstraint: NSLayoutConstraint!
    var registerViewModel: RegisterViewModel!
    var disposeBag = DisposeBag()
    let toolbar = UIToolbar()
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    let datePicker = UIDatePicker()
    var eighteenYearsBack: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .year, value: -18, to: Date(), options: [])!
    }
    var hundredAndTenYearsBack: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .year, value: -110, to: Date(), options: [])!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateView.isHidden = true
        addObserverVariable()
        createFromDatePicker()
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        datePickerOutlet.maximumDate = Date()
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func doneActionOfCalender(_ sender: UIButton) {
        dateView.isHidden = true
    }
    
    @IBAction func ChangeValueAction(_ sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM-dd-yyyy"
        datePickerTextField.text = dateFormatter.string(from: sender.date)
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let storeDate = dateFormatter1.string(from: sender.date)
        registerViewModel.dateOfBirth.value = storeDate
        
    }
    
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func addObserverVariable(){
        datePickerTextField.rx.text
            .orEmpty
            .bind(to:registerViewModel.dateOfBirth)
            .disposed(by: disposeBag)
    }
    

    func createFromDatePicker() {
        
        //Mark :- toolbar
//        let toolbar = UIToolbar()
//        toolbar.sizeToFit()
//
//        // Mark: - Customize toolbar
//        toolbar.isTranslucent = false
//        toolbar.tintColor = .white
//        toolbar.barTintColor = UIColor(hex: "05C1C9") //UIColor(hex: "469CAE")
//
//        // Mark :- Set title to toolbar
//        let toolbarLabel = UILabel(frame: .zero)
//        toolbarLabel.text = "Select Date"
//        toolbarLabel.sizeToFit()
//        toolbarLabel.backgroundColor = UIColor.clear
//        toolbarLabel.textColor = UIColor.white
//        toolbarLabel.textAlignment = .center
//        let labelItem = UIBarButtonItem.init(customView: toolbarLabel)
       
        
        
//        // done button for toolbar
//        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneFromPressed))
//        //toolbar.setItems([done], animated: false)
//
//        let cancelToolBar = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelPressed))
//
//        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//
//        toolbar.setItems([cancelToolBar,flexible,labelItem,flexible,done], animated: false)
//        //469CAE
//
//
//        datePickerTextField.inputAccessoryView = toolbar
//        datePickerTextField.inputView = datePicker
//
//
//        // format picker for date
//        datePicker.datePickerMode = .date
//        datePicker.minimumDate = hundredAndTenYearsBack
//        datePicker.maximumDate = Date()
        
    }
    
    @objc func doneFromPressed() {
        
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.dateFormat = "MM/dd/yyyy"
        let dateString = formatter.string(from: datePicker.date)
        //fromDate = pickerFrom.date
        datePickerTextField.text = "\(dateString)"
        registerViewModel.dateOfBirth.value = dateString
        //postData.from = dateString
        self.view.endEditing(true)
        
        
    }
    
    @objc func cancelPressed(){
        self.view.endEditing(true)
    }

    @IBAction func actionNextBtn(_ sender: UIButton) {
        
        if datePickerTextField.text == ""  {
            Helper.showAlert(head: "Select your date of birth", message: "")
            //showAlertMessage(title: "Fill the dOB", message: "Select your date of birth", action: nil)
        }
        else{
            //let userDefault = UserDefaults.standard
            //userDefault.set(self.tfDatePicket.text!,forKey:USER_DEFAULT_VAIDRX.USER.DATEOFBIRTH)
            performSegue(withIdentifier: "toAddress", sender: self)
            
            //performSegue(withIdentifier: "toAddressVC", sender: self)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddress" {
            
            if let dstVC = segue.destination as? AddMyAddressViewController {
                
                dstVC.registerViewModel = self.registerViewModel
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addKeyBoardObserver()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        removeKeyboardObserveres()
    }
    private func removeKeyboardObserveres() {
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.init(UIResponder.keyboardWillShowNotification.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.init(UIResponder.keyboardWillHideNotification.rawValue), object: nil)
    }
    
    func addKeyBoardObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: Notification.Name.init(UIResponder.keyboardWillShowNotification.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: Notification.Name.init(UIResponder.keyboardWillHideNotification.rawValue), object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            //if view.frame.origin.y == 0{
//            let height = keyboardSize.height
//            btnNextBottomConstraint.constant = datePicker.frame.height + toolbar.frame.height + btnNext.frame.height + 40
//            //self.btnNext.frame.origin.y -= height //+ self.btnNext.frame.height//.frame.size.height
//            //self.btnNext.frame.size.height -= height + 150
//            //}
//
//        }
       // dateView.isHidden = false
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            //if view.frame.origin.y != 0 {
//            let height = keyboardSize.height
//            btnNextBottomConstraint.constant = 140 //height
//            //self.btnNext.frame.origin.y += height //+ self.btnNext.frame.size.height
//            //self.btnNext.frame.size.height += height - 150
//            //}
//            
//        }
       // dateView.isHidden = true
    }
    
    @IBAction func tappedCalanderBtn(_ sender: UIButton) {
        dateView.isHidden=false
    }
}

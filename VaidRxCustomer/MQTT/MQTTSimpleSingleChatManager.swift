//
//  MQTTSimpleSingleChatManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


protocol MQTTSimpleSingleChatManagerDelegate {
    
    
    /// Delegate to show musician livetrack details
    ///
    /// - Parameter liveTrackDetails: liveTrack details model
    func chatMessageRecievedFromMQTT(chatMessageDetails:ChatMessageRecievedModel)
    
}


class MQTTSimpleSingleChatManager {
    
    static var obj:MQTTSimpleSingleChatManager? = nil
    
    class func sharedInstance() -> MQTTSimpleSingleChatManager {
        
        if obj == nil {
            
            obj = MQTTSimpleSingleChatManager()
        }
        
        return obj!
    }
    
    var delegate:MQTTSimpleSingleChatManagerDelegate? = nil
    
    var chatDetailsToShow:ChatMessageRecievedModel!
    
    var alertController:UIAlertController!
    
//    /// Method to maintain LiveTrack details getting from MQTT
//    ///
//    /// - Parameters:
//    ///   - liveTrackMessage: liveTrack message recieved from MQTT
//    ///   - topicName: name of topic the live track message as recieved
//    func chatMessageFromMQTT(chatMessage:[String:Any], topicName:String) {
//
//        let chatMessageDetail = ChatMessageRecievedModel.init(chatMessageRecieved: chatMessage)
//        chatDetailsToShow = nil
//
//        if SplashLoading.obj != nil {//Checking currently showing Splash loading or not
//
//            chatDetailsToShow = chatMessageDetail
//
//        } else {
//
//            showChatMessages(chatMessageDetail: chatMessageDetail)
//        }
//    }
    
    
    
    /// Method to maintain Chat messages from Push and MQTT
    ///
    /// - Parameter chatMessage: chat message recieved from Push and MQTT
    func chatMessageFromPushOrMQTT(chatMessage:[String:Any]) {
        
        let chatMessageDetail = ChatMessageRecievedModel.init(chatMessageRecieved: chatMessage)
        
        if !methodToCheckChatMessageAlreadyExist(chatMessageDetail: chatMessageDetail) {
            
            //Add New chat message to couch DB
            ChatCouchDBManager.sharedInstance.updateEachChatDetailsToCouchDBDocument(message: chatMessage, bookingId: chatMessageDetail.bookingId)
            
            chatDetailsToShow = nil

            if SplashLoading.obj != nil {//Checking currently showing Splash loading or not
                
                chatDetailsToShow = chatMessageDetail
                
            } else {
                
                showChatMessages(chatMessageDetail: chatMessageDetail)
            }

        }
        
    }

    
    func methodToCheckChatMessageAlreadyExist(chatMessageDetail:ChatMessageRecievedModel) -> Bool {
        
        if let prevChatDetails = ChatCouchDBManager.sharedInstance.getChatDocumentDetailsFromCouchDB(bookingId: chatMessageDetail.bookingId) as? [[String:Any]] {
            
            if prevChatDetails.count > 0 {
                
                if let index = prevChatDetails.index(where: {$0["content"] as? String == chatMessageDetail.messageContent && $0["timestamp"] as? Int == chatMessageDetail.messageTimestamp && $0["fromID"] as? String == chatMessageDetail.musicianId }) {
                    
                    DDLogVerbose("Previous Chat Details:- \(prevChatDetails[index])")
                    return true
                }
            }
        }
        
        return false
    }
   
    
    func showChatMessages(chatMessageDetail:ChatMessageRecievedModel) {
        
        Helper.playLocalNotificationSound()
        
        chatDetailsToShow = nil
        
        if delegate != nil && Helper.getCurrentVC() is ChatViewController {
            
            if ChatViewController.sharedInstance().bookingId != chatMessageDetail.bookingId && chatMessageDetail.bookingId.length > 0 {
                
                ChatViewController.sharedInstance().bookingId = chatMessageDetail.bookingId
                ChatViewController.sharedInstance().musicianId = chatMessageDetail.musicianId
                ChatViewController.sharedInstance().musicianImageURL = chatMessageDetail.musicianProfilePicURL

                
                ChatViewController.sharedInstance().title = "\(ALERTS.BOOKING_FLOW.EventID)" + " : " + chatMessageDetail.bookingId
                
                ChatViewController.sharedInstance().fetchData(val:"0")
                
            } else {
                
                if delegate != nil {
                    
                    delegate?.chatMessageRecievedFromMQTT(chatMessageDetails: chatMessageDetail)
                }

            }
            
        } else if Utility.sessionToken.length > 0 {
            
                // create the alert
                if alertController != nil {
                    
                    alertController.title = ALERTS.Message
                    alertController.message = ALERTS.ChatMessage + " " + chatMessageDetail.bookingId
//                    alertController.dismiss(animated: true, completion: nil)
                } else {
                    
                    alertController = UIAlertController(title: ALERTS.Message,
                                                        message: ALERTS.ChatMessage + " " + chatMessageDetail.bookingId,
                                                        preferredStyle: UIAlertController.Style.alert)
                    
                    let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
                    newAlertWindow.rootViewController = UIViewController()
                    newAlertWindow.windowLevel = UIWindow.Level.alert + 1
                    newAlertWindow.makeKeyAndVisible()
                    
                    let DestructiveAction = UIAlertAction(title: ALERTS.Ok, style: UIAlertAction.Style.destructive) {
                        (result : UIAlertAction) -> Void in
                        
                        newAlertWindow.resignKey()
                        newAlertWindow.removeFromSuperview()
                        self.alertController = nil
                        
                        print("Destructive")
                    }
                    
                    // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
                    
                    let okAction = UIAlertAction(title: ALERTS.VIEW, style: UIAlertAction.Style.default) {
                        (result : UIAlertAction) -> Void in
                        
                        newAlertWindow.resignKey()
                        newAlertWindow.removeFromSuperview()
                        
                        self.alertController = nil
                        
                        self.goToChatVC(chatMessageDetail: chatMessageDetail)
                    }
                    
                    alertController.addAction(DestructiveAction)
                    alertController.addAction(okAction)
                    
                    //                if Helper.newAlertWindow == nil {
                    //
                    //                    Helper.newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
                    //                    Helper.newAlertWindow?.rootViewController = UIViewController()
                    //                    Helper.newAlertWindow?.windowLevel = UIWindowLevelAlert + 1
                    //                    Helper.newAlertWindow?.makeKeyAndVisible()
                    //                    Helper.newAlertWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
                    //
                    //
                    //                } else {
                    
                    newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
                    //                }
                    
                }
            
            
        }

    }
    
    func goToChatVC(chatMessageDetail:ChatMessageRecievedModel) {
        
        let chatVC:ChatViewController = Helper.getCurrentVC().storyboard!.instantiateViewController(withIdentifier: VCIdentifier.chatVC) as! ChatViewController
        
        chatVC.bookingId = chatMessageDetail.bookingId
        chatVC.musicianId = chatMessageDetail.musicianId
        chatVC.musicianName = chatMessageDetail.musicianName
        chatVC.musicianImageURL = chatMessageDetail.musicianProfilePicURL

        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (Helper.getCurrentVC().navigationController?.view)!, timeDuration: 0.35)
        
        Helper.getCurrentVC().navigationController?.pushViewController(chatVC, animated: false)
    }

}


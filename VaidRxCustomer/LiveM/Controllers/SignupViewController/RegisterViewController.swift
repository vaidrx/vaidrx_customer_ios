//
//  RegisterViewController.swift
//  Iserve
//
//  Created by Rahul Sharma on 11/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import Kingfisher
import RxSwift
import RxCocoa

class RegisterViewController: UIViewController {
    
    // MARK: - Outlets
    
    // Layout Constraint
    @IBOutlet weak var pickerButtomConstrain: NSLayoutConstraint! //250
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var loadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollContentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var firstNameMissingHeight: NSLayoutConstraint!
    @IBOutlet weak var lastNameMissingHeight: NSLayoutConstraint!
    @IBOutlet weak var emailMissingHeight: NSLayoutConstraint!
    @IBOutlet weak var phoneMissingHeight: NSLayoutConstraint!
    
    @IBOutlet weak var detailMainViewHeight: NSLayoutConstraint!
    
    
    //UI View
    @IBOutlet weak var refferalCodeView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var yourBirthdayView: UIView!
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var facbookBottomView: UIView!
    @IBOutlet weak var facebookBackView: UIView!
    @IBOutlet weak var googleBackView: UIView!
    @IBOutlet weak var signUpBackView: UIViewCustom!
    @IBOutlet weak var textFieldsBackView: UIView!
    @IBOutlet weak var loadingView: UIViewCustom!
    
    // Scroll View
    @IBOutlet weak var scrollView: UIScrollView!
    
    // Image View
    @IBOutlet weak var countryFlagImage: UIImageView!
    @IBOutlet weak var profilePhoto: UIImageView!
    
    //Text Field
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: FloatLabelTextField!
    @IBOutlet weak var yourBirthdayTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumerTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var referralCodeTextField: UITextField!
    
    // Label
    @IBOutlet weak var datePickerLabel: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var passwordmissingLabel: UILabel!
    @IBOutlet weak var birthdaymissingLabel: UILabel!
    
    // Button
    @IBOutlet weak var termsAndCondButton: UIButton!
    @IBOutlet weak var sigUpButtonOutlet: UIButtonCustom!
    
    @IBOutlet weak var birthdayDatePicker: UIDatePicker!
    
    
    // MARK: - Variable Decleration -
    var selectedCountry: Country!
    var activeField: UITextField?
    
    var isEmailValid: Bool = false
    var isPhoneNumberValid: Bool = false
    var isReferralCodeValid: Bool = false
    var isFromLogin: Bool = false
    var isFBSignUp: Bool = false
    var profilePicURL:String!
    var fbUserDataModel:UserDataModel!
    
    let locationObj = LocationManager.sharedInstance()
    var fbLogin: Bool = false
    
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var profileAlternatePhoto = 0
    var backButtonActive: Bool = false
    var heightChange = 0
    var emailFormFb = ""
    
    var selectedDOB:Date!
    
    var userLoginType = LoginType.Default
    var userRegisterType = RegisterType.Default

    let registerViewModel = RegisterViewModel()
    let disposeBag = DisposeBag()
    let imagePicker = UIImagePickerController()
    
    
    // MARK: - Class Initial Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        Helper.editNavigationBar(navigationController!)
            locationObj.start()
        
        self.profilePhoto.image = PROFILE_DEFAULT_IMAGE
        self.profilePhoto.layer.borderWidth = 1
        self.profilePhoto.layer.borderColor = RED_COLOR.cgColor

        birthdayDatePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
        selectedDOB = birthdayDatePicker.date
        
        dateChanged(birthdayDatePicker)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        firstNameMissingHeight.constant = 0
        lastNameMissingHeight.constant = 0
        emailMissingHeight.constant = 0
        phoneMissingHeight.constant = 0
        detailMainViewHeight.constant = 490
        
        imagePicker.delegate = self
        
        appDelegate?.keyboardDelegate = self
        
        if isFromLogin {
            
            showFacebookDetails()
        }
        addObserveToVariables()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        profilePhoto.layer.borderWidth = 1
        profilePhoto.layer.borderColor = RED_COLOR.cgColor
        
        sigUpButtonOutlet.isEnabled = true
        animateBottomView()
        springAnimation()
        if((textFieldsBackView.frame.size.height + textFieldsBackView.frame.origin.y) + 30 > scrollView.frame.size.height) {
            
            scrollContentViewHeightConstraint.constant = (textFieldsBackView.frame.size.height + textFieldsBackView.frame.origin.y) + 30 - scrollView.frame.size.height;
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0
        }
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        appDelegate?.keyboardDelegate = nil
    }
    
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        datePickerLabel.text = dateFormat.string(from: sender.date)
    }
    
    func addObserveToVariables() {
        
        firstNameTextField.rx.text
            .orEmpty
            .bind(to: registerViewModel.firstNameText)
            .disposed(by: disposeBag)
        
        lastNameTextField.rx.text
            .orEmpty
            .bind(to: registerViewModel.lastNameText)
            .disposed(by: disposeBag)
        
        emailTextField.rx.text
            .orEmpty
            .bind(to: registerViewModel.emailText)
            .disposed(by: disposeBag)
        
        passwordTextField.rx.text
            .orEmpty
            .bind(to: registerViewModel.passwordText)
            .disposed(by: disposeBag)

        phoneNumerTextField.rx.text
            .orEmpty
            .bind(to: registerViewModel.phoneNumberText)
            .disposed(by: disposeBag)
        
        yourBirthdayTextField.rx.text
            .orEmpty
            .bind(to: registerViewModel.dobText)
            .disposed(by: disposeBag)
        
        referralCodeTextField.rx.text
            .orEmpty
            .bind(to: registerViewModel.referralCodeText)
            .disposed(by: disposeBag)
        
    }

}

extension RegisterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var  chosenImage = UIImage()
        chosenImage = info[UIImagePickerController.InfoKey.editedImage.rawValue] as! UIImage //2
        
        chosenImage = Helper.changeImageFrame(with: chosenImage, scaledTo: CGSize(width: 100, height: 100))
        
        profilePhoto.image = chosenImage
        profilePhoto.layer.borderWidth = 1
        profilePhoto.layer.borderColor = RED_COLOR.cgColor
        profileAlternatePhoto = 1
            dismiss(animated:true, completion: nil) //5
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
           dismiss(animated: true, completion: nil)
    }
}


// MARK: - Keyboard Delegate Methods -
extension RegisterViewController:KeyboardDelegate {
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: (keyboardSize!.height + 60), right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
        }
    }
}


// MARK: - CountrySelectedDelegate -
extension RegisterViewController: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryFlagImage.image = UIImage(named: imagePath)
        
        countryCodeLabel.text =  self.selectedCountry.dial_code
    }
    
}


// MARK: - Upload Image To Amazon -
extension RegisterViewController {
    
    func uploadProfileImageToAmazon() {
    
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        Helper.showPI(_message: PROGRESS_MESSAGE.SignUP)
        
        if profileAlternatePhoto == 0 || profileAlternatePhoto == 2 {
            profilePhoto.image  = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        AmazonManager.sharedInstance().upload(withImage: profilePhoto.image! ,
                                              imgPath: "ProfileImages/") { (result, imageUrl) in
                                                
                                                self.profilePicURL = imageUrl
                                                self.serviceRequestToMakeRegister()
                                                
        }
    }
    
}

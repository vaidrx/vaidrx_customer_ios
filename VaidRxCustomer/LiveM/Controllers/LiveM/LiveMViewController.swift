//
//  LiveMViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class LiveMViewController: UIViewController {
    
    // MARK: - Outlets -
    // label

    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var tagLineLabel: UILabel!
    
    //View
    @IBOutlet weak var rateBottomView: UIView!
    @IBOutlet weak var mainBottomViews: UIView!
    @IBOutlet weak var likeBottomView: UIView!
    @IBOutlet weak var legalBottomView: UIView!
    
    //Button
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var legalButton: UIButton!
    
    
    // MARK: - Variable Decleratons -
    weak var delegate: LeftMenuProtocol?
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    
    
    // MARK: - Class Default Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.editNavigationBar(navigationController!)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        var versionString = "1.0"
        if let versionNumber = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versionString = versionNumber
        }
        
        if let buildnNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionString = versionString + "." + buildnNumber
        }
        
        versionLabel.text = versionString
        
        setupGestureRecognizer()
        initialSetUp()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initialAnimation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    // MARK: - Action methods -
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rateAction(_ sender: Any) {
        
        if let url = NSURL(string: Links.AppstoreLink) {
            
            UIApplication.shared.openURL(url as URL)
        }

    }
    
    @IBAction func likeAction(_ sender: Any) {
        
        if let url = NSURL(string: Links.FacebookPageLink) {
            
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func legalAction(_ sender: Any) {
        
        performSegue(withIdentifier: SEgueIdetifiers.legalToWeb , sender: nil)
    }
    
    @IBAction func companyLinkAction(_ sender: Any) {
        
        if let url = NSURL(string: Links.AppWebsiteLink){
            
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEgueIdetifiers.legalToWeb {
            
            if let dstVC = segue.destination as? TermsWebViewController {
                
                dstVC.isForTermsAndConditions = false
                dstVC.navTitle = "Subscribe to our Youtube Channel"
            }
        }
    }
    
}


extension LiveMViewController {
    
    /// initiall SetUp For Animations
    func initialSetUp() {
        rateBottomView.transform = CGAffineTransform(translationX: mainBottomViews.frame.size.width + 100, y: 0)
        likeBottomView.transform = CGAffineTransform(translationX: mainBottomViews.frame.size.width + 100, y: 0)
        legalBottomView.transform = CGAffineTransform(translationX: mainBottomViews.frame.size.width + 100, y: 0)
        rateButton.transform = CGAffineTransform(translationX: -mainBottomViews.frame.size.width , y: 0)
        likeButton.transform = CGAffineTransform(translationX: -mainBottomViews.frame.size.width , y: 0)
        legalButton.transform = CGAffineTransform(translationX: -mainBottomViews.frame.size.width , y: 0)
        mainBottomViews.transform = CGAffineTransform(translationX: 0, y: 1000)
    }
    
    /// initiall Animations when view will appear
    func initialAnimation() {
        UIView.animate(withDuration: 0.8, animations: {
            self.rateBottomView.transform = .identity
            self.likeBottomView.transform = .identity
            self.legalBottomView.transform = .identity
            self.rateButton.transform = .identity
            self.likeButton.transform = .identity
            self.legalButton.transform = .identity
        }) { (true) in
            UIView.animate(withDuration: 0.6, delay: 0.3, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                self.mainBottomViews.transform = .identity
            })
        }
    }
    
    
}

extension LiveMViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension LiveMViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}



//
//  vaiDRx_LandingVC_APICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRxLandingVCViewController {
    
    //MARK - WebService Call -
    func sendRequestToValidatePhoneNumber() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        landingViewModel.countryCode = countryCodeLabel.text!
        landingViewModel.triggerValue = 1
        landingViewModel.validatePhoneNumberAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.verifyMobileNumber, userRegisterType: nil)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType, userRegisterType:RegisterType?) {
        
        if HTTPSResponseCodes.Register(rawValue: statusCode) != nil {
            
            let responseCodes : HTTPSResponseCodes.Register = HTTPSResponseCodes.Register(rawValue: statusCode)!
            
            switch responseCodes {
                
            case .WrongInputData://wrong data
                
                isPhoneNumberValid = true
                
                
                if let dataRes = dataResponse as? [String:Any] {
                    
                    if let sid = dataRes["sid"] as? String {
                        
                        DDLogDebug("SID:\(sid)")
                        
                        let params:[String:Any] = [
                            USER_DEFAULTS.USER.COUNTRY_CODE:countryCodeLabel.text!,
                            USER_DEFAULTS.USER.MOBILE:phoneNumberTextFeild.text!,
                            SERVICE_RESPONSE.Sid :sid
                        ]
                        registerViewModel.registerType = RegisterType.Default
                        registerViewModel.countryCode = countryCodeLabel.text!
//                        performSegue(withIdentifier: "toVerifyOTP", sender: params)
                        self.goToVerifyOTPScreen(params: params)
                    }
                    
                }
                
                
            case .WrongPhoneNumber:
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                phoneNumberTextFeild.text = ""
                landingViewModel.phoneNumberText.value = ""
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.phoneNumberTextFeild.becomeFirstResponder()
                })
                
                //Do the Login in process here
                
            case .SuccessResponse:
                
                isPhoneNumberValid = true
                performSegue(withIdentifier: "toSignIn", sender: self)
                
                
                
                
            case .MissingPasswordFBLogin:
                 Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                break
            }
            
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
        }
        
    }
    
    func goToVerifyOTPScreen(params:[String:Any]){
        
         let storyboard = UIStoryboard(name: "vaiDRx_Main", bundle: nil)
         let nextVC = storyboard.instantiateViewController(withIdentifier: "VerifyMobileVC") as? vaiDRx_VerifyOTPVC
         nextVC?.dataFromLandingVC = params
         nextVC?.registerViewModel = self.registerViewModel
         nextVC?.isComingFrom = "signUp"
         self.navigationController?.pushViewController(nextVC!, animated: true)
        
    }
}

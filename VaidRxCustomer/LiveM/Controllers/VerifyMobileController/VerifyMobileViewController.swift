//
//  VerifyMobileViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 28/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


protocol VerifyMobileViewControllerDelegate  {
    func didUpdateMobileNumber()
}

class VerifyMobileViewController: UIViewController,KeyboardDelegate {
    
    // MARK: - Outlets -
    
    //ScrollView
    @IBOutlet weak var scrollBackView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    
    
    // text Field
    @IBOutlet weak var code1: UITextField!
    @IBOutlet weak var code2: UITextField!
    @IBOutlet weak var code3: UITextField!
    @IBOutlet weak var code4: UITextField!
    
    //Image View
    @IBOutlet weak var otpImage1: UIImageView!
    @IBOutlet weak var otpImage2: UIImageView!
    @IBOutlet weak var otpImage3: UIImageView!
    @IBOutlet weak var otpImage4: UIImageView!
    
    // UIView
    @IBOutlet weak var code1View: UIView!
    @IBOutlet weak var code2View: UIView!
    @IBOutlet weak var code3View: UIView!
    @IBOutlet weak var code4View: UIView!
    @IBOutlet weak var verifyBackView: UIViewCustom!
    
    //Button
    @IBOutlet weak var verifyButtonOutlet: UIButtonCustom!
    
    //Layout Constraint
    @IBOutlet weak var loadingViewWidth: NSLayoutConstraint!
    @IBOutlet weak var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var resendCodeButton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    
    // MARK: - Variable Decleration -
    var activeTextField = UITextField()
    var isCommingFrom:String!
    var userLoginType:LoginType = LoginType.Default
    var mobileNo : String = ""
    var delegate : VerifyMobileViewControllerDelegate?
    var dataFromRegisterVC:[String:Any] = [:]
    var dataFromForgetPasswordVC:[String:Any] = [:]
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var userID: [String:Any] = [:]
    var phoneNumberWithCountryCode:String!
    var profilePicURL:String!
    
    var phoneNumber:String!
    var countryCode:String!
    
    let locationObj = LocationManager.sharedInstance()
    var countTimer: Int = 120
    var manualEntry = false
    var countOtp1 = 0
    
    var verifyPhoneViewModel = VerifyPhoneViewModel()

    
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallSetup()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isCommingFrom == "ForgetPass" || isCommingFrom == "ChangeEmailPhone" {
            countryCode = dataFromForgetPasswordVC[USER_DEFAULTS.USER.COUNTRY_CODE] as! String
            phoneNumber = dataFromForgetPasswordVC[USER_DEFAULTS.USER.MOBILE] as! String
            userID = [ SERVICE_RESPONSE.Sid : dataFromForgetPasswordVC[SERVICE_RESPONSE.Sid] as? String]
            phoneNumberWithCountryCode = String(format:"%@%@",dataFromForgetPasswordVC[USER_DEFAULTS.USER.COUNTRY_CODE] as! CVarArg, dataFromForgetPasswordVC[USER_DEFAULTS.USER.MOBILE] as! CVarArg)
        }
        initiallAnimation()
        
        if((verifyBackView.frame.size.height + verifyBackView.frame.origin.y) + 30 > scrollView.frame.size.height) {
            
            scrollContentViewHeightConstraint.constant = (verifyBackView.frame.size.height + verifyBackView.frame.origin.y) + 30 - scrollView.frame.size.height;
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0;
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addTimer()
        appDelegate?.keyboardDelegate = self
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        appDelegate?.keyboardDelegate = nil
    }
    
    
    // MARK: - Action Methods -
    @IBAction func resendOtpAction(_ sender: Any) {
        code1.text = ""
        code2.text = ""
        code3.text = ""
        code4.text = ""
        code1.becomeFirstResponder()
        resendOTPToMobileNumber()
        
    }
    
    @IBAction func verifyCodeAction(_ sender: AnyObject) {
        
        if (code1.text?.length)! > 0 && (code2.text?.length)! > 0 && (code3.text?.length)! > 0 && (code4.text?.length)! > 0 {
            
            let verificationCode = code1.text! + code2.text! + code3.text! + code4.text!
            
            if isCommingFrom == "signUp" {
                
                signUpVerifyOTP(code: verificationCode)
                
            } else {
                
                forgotPasswordVerifyOTP(code: verificationCode)
            }
            
        } else {
            
            Helper.showAlert(head: ALERTS.Message, message:ALERTS.OPTMissing)
            
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapGesture(_ sender: AnyObject)
    {
        self.view.endEditing(true)
    }
    
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
        }
        
    }
    
}

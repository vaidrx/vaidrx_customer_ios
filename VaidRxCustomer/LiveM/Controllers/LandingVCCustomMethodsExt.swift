//
//  LandingVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import AVFoundation

extension LandingViewController{
    
    /// play video in background
    func setupVideo() {
        
        //Show Launch screen image and app logo image depending on screen size
        switch Int(SCREEN_HEIGHT!) {
            
        case 480:
            self.backgroundImage.image = UIImage(named: "iPhone4_Help_Screen")
            self.logoImageView.image = UIImage(named: "Iphone5_Help_icon")
            break
            
        case 568:
            self.backgroundImage.image = UIImage(named: "iPhone5_Help_Screen")
            self.logoImageView.image = UIImage(named: "Iphone5_Help_icon")
            break
            
        case 667:
            self.backgroundImage.image = UIImage(named: "iPhone6_Help_Screen")
            self.logoImageView.image = UIImage(named: "Iphone6_Help_Icon")
            break
            
        case 812:
            self.backgroundImage.image = UIImage(named: "iPhoneX_Help_Screen")
            self.logoImageView.image = UIImage(named: "Iphone6Plus_Help_Icon")
            break

            
        default:
            self.backgroundImage.image = UIImage(named: "iPhone6Plus_Help_Screen")
            self.logoImageView.image = UIImage(named: "Iphone6Plus_Help_Icon")
            break
        }
        
        
        //Load Splash screen video fro assets
        let videoURL = Bundle.main.url(forResource:"splashvideo", withExtension: "mp4")
        let avAsset = AVAsset(url: videoURL!)
        
        //Assets loading completion block method
        avAsset.loadValuesAsynchronously(forKeys: ["playable"]) {
            var error: NSError? = nil
            let status = avAsset.statusOfValue(forKey: "playable", error: &error)
            
            switch status {
                
            case .loaded:
                // Sucessfully loaded, continue processing
                // Build the player
                
                DispatchQueue.main.async(execute: {() -> Void in
                    
                    let avPlayerItem = AVPlayerItem(asset: avAsset)
                    
                    self.avPlayer = AVPlayer(playerItem: avPlayerItem)
                    self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
                    self.avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    self.avPlayer.volume = 0
                    self.avPlayer.actionAtItemEnd = .none
                    self.avPlayerLayer.backgroundColor = UIColor.clear.cgColor
                    
                    self.avPlayerLayer.frame = SCREEN_RECT
                    
                    self.videoView.layer.insertSublayer(self.avPlayerLayer, at: 0)
                    //                        self.videoView.layer.addSublayer(self.avPlayerLayer)
                    
                    NotificationCenter.default.addObserver(self,
                                                           selector: #selector(self.playerItemDidReachEnd(notification:)),
                                                           name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                           object: self.avPlayer.currentItem)
                    self.avPlayer.play()
                    self.paused = false
                    
                })
                
                
                
                
            case .failed:
                // Examine NSError pointer to determine failure
                break
                
            default:
                break
            }
        }
        
        
        
    }
    
    
    /// Video player reached end method
    ///
    /// - Parameter notification:Video player reached end notification details
    @objc func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: CMTime.zero)
    }
    
    ///Initial Animate Views
    func animations() {
        
        //Logo image down to top animation
        switch Int(SCREEN_HEIGHT!) {
            
        case 568:
            self.logoImageView.transform = CGAffineTransform(translationX: 0, y: 100)
            break
            
        case 667:
            self.logoImageView.transform = CGAffineTransform(translationX: 0, y: 120)
            break
            
        case 812:
            self.logoImageView.transform = CGAffineTransform(translationX: 0, y: 150)
            break
            
        default:
            self.logoImageView.transform = CGAffineTransform(translationX: 0, y: 130)
            break
        }
        
        
        self.logoImageView.transform = CGAffineTransform(translationX: 0, y: 120)
        self.loginButtonOutlet.transform = CGAffineTransform(translationX: 0, y: 150)
        self.registerButtonOutlet.transform = CGAffineTransform(translationX: 0, y: 150)
        
        
        UIView.animate(withDuration: 0.75, animations: {
            
            self.logoImageView.transform = .identity
            self.collectionView.isHidden = false
            self.pageController.isHidden = false
            self.loginButtonOutlet.transform = .identity
            self.registerButtonOutlet.transform = .identity
            
        }) { (true) in
            
        }
    }
    
    /// Change Button from sellected to unsellected and viceversa
    ///
    /// - Parameter sender: Action
    func changeButtonState(sender:AnyObject)  {
        let mBtn = (sender as! UIButton)
        for button: UIView in mBtn.superview!.subviews{
            if button is UIButton
            {
                (button as! UIButton).isSelected = false
                (button as! UIButton).backgroundColor = UIColor.clear
                (button as! UIButton).setTitleColor(UIColor.white, for: .normal)
            }
        }
        mBtn.isSelected = true
        mBtn.backgroundColor = UIColor.white
        mBtn.setTitleColor(UIColor.red, for: .normal)
    }
    
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    /// Initaill Setup For Login and Register buttons
    func initiallButtonSetUp(){
        if registerButtonOutlet.isSelected == false {
            loginButtonOutlet.isSelected = true
            loginButtonOutlet.backgroundColor = UIColor.white
            registerButtonOutlet.backgroundColor = UIColor.clear
            loginButtonOutlet.setTitleColor(UIColor.red, for: .normal)
            registerButtonOutlet.setTitleColor(UIColor.white, for: .normal)
        }
        else {
            loginButtonOutlet.isSelected = false
            loginButtonOutlet.backgroundColor = UIColor.clear
            registerButtonOutlet.backgroundColor = UIColor.white
            loginButtonOutlet.setTitleColor(UIColor.white, for: .normal)
            registerButtonOutlet.setTitleColor(UIColor.red, for: .normal)
        }
    }
}

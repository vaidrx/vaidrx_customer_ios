//
//  RateProviderViewController_APICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 17/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//


import Foundation

extension RateProviderViewController {
    
    func submitReviewAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
//        invoiceViewModel.bookingId = model.bookingId//bookingId
//        invoiceViewModel.ratingValue = Double(ratingView.rating)
        rateProVM.updateBookingID(bookingId)
        
        if expDiscriptionTextView.text == ALERTS.invoiceReviewPlaceholder || expDiscriptionTextView.text.length == 0 {
            
//            invoiceViewModel.reviewText = ""
            rateProVM.updateReview("")
            
        } else {
            rateProVM.updateReview(expDiscriptionTextView.text!)
//            invoiceViewModel.reviewText = expDiscriptionTextView.text!
        }
        
        
        
        rateProVM.submitReviewAPICall{ (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.reviewAndRating)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if let dataRes = dataResponse as? String {
                
                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
//                self.apiTag = requestType.rawValue
                
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
                case .getParticularApptDetails:
                    
                    progressMessage = PROGRESS_MESSAGE.Loading
                    
                case .reviewAndRating:
                    
                    progressMessage = PROGRESS_MESSAGE.SubmittingReview
                    
                    
                default:
                    
                    break
                }
                
//                self.acessClass.getAcessToken(progressMessage: progressMessage)
                
                
            }
            
            
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
            }
            
            break
            
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.getParticularApptDetails:
                
//                if dataResponse != nil {
//
//                    bookingDetailModel = BookingDetailsModel.init(bookingInvoiceDetails: dataResponse!)
//
//                    self.showBookingDetails()
//                    self.scrollView.isHidden = false
//                    self.submitButtonBackView.isHidden = false
//                    self.navigationTopView.isHidden = false
//
//                }
                
                break
                
            case RequestType.reviewAndRating:
                
//                MixPanelManager.sharedInstance.BookingCompletedEvent(bookingId: String(bookingId),
//                                                                     rating: ratingView.rating,
//                                                                     reviewComment:invoiceViewModel.reviewText)
//                ChatCouchDBManager.sharedInstance.deleteParticularBookingChatCouchDBDocument(bookingId: String(bookingId))
                
                self.navigationController?.popToRootViewController(animated: false)
                
                UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.BookingReviewed)
                UserDefaults.standard.synchronize()
//                LeftMenuTableViewController.sharedInstance().changeViewController(LeftMenu.searchArtist)
//                BookingStatusResponseManager.sharedInstance().showRemainingCompletedBookingInvoice(bookingId)
                
                break
                
                
            default:
                break
            }
            break
            
            
        default:
            
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
            }
            
            break
        }
        
    }
}

//
//  ChangeEmailViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

/// Change Email Mobile View Model Class to maintain Change Email Mobile View Data
class ChangeEmailMobileViewModel{
    
    var emailText = Variable<String>("")
    var phoneNumberText = Variable<String>("")
    var countryCode = ""
    var triggerValue = 0
    
    let disposebag = DisposeBag()
    
    let rxRegisterAPICall = RegisterAPI()
    
    let rxChangeEmailMobileAPICall = ChangeEmailMobileAPI()
    
    
    func changeEmailAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxChangeEmailMobileAPICall.changeEmailServiceAPICall(emailText: emailText.value)
        
        if !rxChangeEmailMobileAPICall.changeEmail_Response.hasObservers {
            
            rxChangeEmailMobileAPICall.changeEmail_Response
            .subscribe(onNext: {response in
                
                if (response.data[SERVICE_RESPONSE.Error] != nil) {
                    
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    return
                }
                
                completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                
                
            }, onError: {error in
                
            }).disposed(by: disposebag)

        }
            
        
    }
    
    func changeMobileAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxChangeEmailMobileAPICall.changePhoneNumberServiceAPICall(countryCode: countryCode, phoneNumberText: phoneNumberText.value)
        
        if !rxChangeEmailMobileAPICall.changePhoneNumber_Response.hasObservers {
            
            rxChangeEmailMobileAPICall.changePhoneNumber_Response
            .subscribe(onNext: {response in
                
                if (response.data[SERVICE_RESPONSE.Error] != nil) {
                    
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    return
                }
                
                completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                
                
            }, onError: {error in
                
            }).disposed(by: disposebag)

        }
        
    }
    
    
    func validateEmailAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxRegisterAPICall.validateEmailServiceAPICall(emailText: emailText.value)
        
        if !rxRegisterAPICall.validateEmail_Response.hasObservers {
            
            rxRegisterAPICall.validateEmail_Response
            .subscribe(onNext: {response in
                
                if (response.data[SERVICE_RESPONSE.Error] != nil) {
                    
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    return
                }
                                
                completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                
                
            }, onError: {error in
                
            }).disposed(by: disposebag)
            

        }
        
    }
    
    
    func validatePhoneNumberAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxRegisterAPICall.validatePhoneNumberServiceAPICall(countryCode: countryCode, phoneNumber: phoneNumberText.value, triggerValue: triggerValue)
        
        
        if !rxRegisterAPICall.validatePhoneNumber_Response.hasObservers {
            
            rxRegisterAPICall.validatePhoneNumber_Response
            .subscribe(onNext: {response in
                
                if (response.data[SERVICE_RESPONSE.Error] != nil) {
                    
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    return
                }
                
                completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                
                
            }, onError: {error in
                
            }).disposed(by: disposebag)
            
        }
        
    }
    
}

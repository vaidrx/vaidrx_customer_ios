//
//  InsuranceTypesAPI.swift
//  GoTasker
//
//  Created by 3Embed on 18/12/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class InsuranceTypeAPI {
    
    let disposeBag = DisposeBag()
    var insuranceTypesResponse = PublishSubject<APIResponseModel>()
    let updateInsurance_Response = PublishSubject<APIResponseModel>()
    
    func getInsuranceTypesServiceAPICall(){
        let strURL = API.BASE_URL + API.METHOD.GET_INSURANCE
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        RxAlamofire.requestJSON(.get, strURL, parameters: nil, encoding: JSONEncoding.default, headers: NetworkHelper.sharedInstance.getAOTHHeader()).subscribe(onNext: {(r,json) in
            
            print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
            
            if let dict = json as? [String:Any]{
                let statusCode = r.statusCode
                self.checkResponse(statusCode: statusCode, responseDict: dict,requestType: RequestType.GetInsurances)
            }
            Helper.hidePI()
        }, onError: {(error) in
            Helper.showAlert(head: ALERTS.Error, message: error.localizedDescription)
            //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: error.localizedDescription)
            Helper.hidePI()
        }).disposed(by: disposeBag)
    }
    
    /// Method to Call Update Profile Details service API
    ///
    /// - Parameter profileRequestModel: model contains current customer details
    func updateInsuranceDetailsServiceAPICall(profileRequestModel:ProfileRequestModel) {
        
        let strURL = API.BASE_URL + API.METHOD.UPDATEPROFILE
        
        let requestParams: [String: Any] = [
            
//            SERVICE_REQUEST.FirstName : profileRequestModel.firstName,
//            SERVICE_REQUEST.LastName  : profileRequestModel.lastName ,
//            SERVICE_REQUEST.DOB       : profileRequestModel.dateOfBirth,
//            SERVICE_REQUEST.ProfilePic: profileRequestModel.profilePicURL,
            SERVICE_REQUEST.paymentType: profileRequestModel.paymentType,
            SERVICE_REQUEST.insurance : profileRequestModel.insurance,
//            SERVICE_REQUEST.AddLine1 : profileRequestModel.addLine1,
//            SERVICE_REQUEST.AddLine2 : profileRequestModel.addLine2,
//            SERVICE_REQUEST.City : profileRequestModel.city,
//            SERVICE_REQUEST.Country : profileRequestModel.country,
//            SERVICE_REQUEST.State : profileRequestModel.state,
//            SERVICE_REQUEST.PlaceId : profileRequestModel.placeId,
//            SERVICE_REQUEST.PlaceName : profileRequestModel.placeName,
//            SERVICE_REQUEST.Pincode : profileRequestModel.pincode,
//            SERVICE_REQUEST.TaggedAs : profileRequestModel.taggedAs,
//            SERVICE_REQUEST.Long : profileRequestModel.longitude,
//            SERVICE_REQUEST.Lat : profileRequestModel.latitude,
            
            ]
        
//        if profileRequestModel.about.length > 0 {
//            
//            requestParams[SERVICE_REQUEST.About] = profileRequestModel.about
//        }
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Saving)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.updateProfileData)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposeBag)
        
        
    }
    
    func checkResponse(statusCode:Int,responseDict:[String:Any],requestType:RequestType){
        
        switch statusCode {
        case HTTPSResponseCodes.InternalServerError.rawValue:
            Helper.showAlert(head: ALERTS.Error, message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: responseDict[SERVICE_RESPONSE_VAIDRX.ErrorMessage] as! String)
        default:
            let responseModel:APIResponseModel
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            switch requestType {
                
            case .GetInsurances:
                self.insuranceTypesResponse.onNext(responseModel)
            case .updateProfileData:
                self.updateInsurance_Response.onNext(responseModel)
            default :
                break
            }
        }
    }
    
    
}

//
//  ConfirmBookingVCCustomMethods.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension ConfirmBookingScreen {
    
    func initialSetup() {
        
        arrayOfHeaderTitles = [
            "Choose Gig Time",
            "Event location",
            "Selected date and time",
            "Type of event",
            "Event start",
            "Payment method",
            "Promocode",
            "Payment Breakdown"
        ]
        
        Helper.setShadowFor(self.navigationTopView, andWidth: SCREEN_WIDTH!, andHeight: NavigationBarHeight)

        
        ratingView.emptyImage = #imageLiteral(resourceName: "review_star_grey_icon")
        ratingView.fullImage = #imageLiteral(resourceName: "review_star_red_icon")
        ratingView.floatRatings = true
        
        
        if SCREEN_HEIGHT == 812 {//iPhoneX
            
            self.navigationTopViewHeightConstraint.constant = NavigationBarHeight
            self.navigationTopView.layoutIfNeeded()
            
//            self.tableViewTopConstraint.constant = -64
//            tableView.layoutIfNeeded()
        }
        
        if Helper.SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(version: "11.0") {
            
            self.tableViewTopConstraint.constant = -NavigationBarHeight
            tableView.layoutIfNeeded()
        }
        
        musicianNameLabel.text = CBModel.providerFullDetalsModel.firstName + " " + CBModel.providerFullDetalsModel.lastName
        
        navigationTitleLabel.text = musicianNameLabel.text
        
        ratingView.rating = Float(CBModel.providerFullDetalsModel.overallRating)
        numberOfReviewsLabel.text = "\(CBModel.providerFullDetalsModel.noOfReviews) reviews"//"\(CBModel.providerFullDetalsModel.reviewsArray.count.description) reviews"
        
        milesLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: CBModel.providerModel.distance, mileageMatric: CBModel.providerFullDetalsModel.mileageMatric)
        
        bookButton.setTitle(ALERTS.CONFIRM_BOOKING.BookButton, for: UIControl.State.normal)
        
        
        if CBModel.providerModel.status == 0 {
            
            musicianStatusImageView.image = #imageLiteral(resourceName: "offline_image")
            
        }else {
            
            musicianStatusImageView.image = #imageLiteral(resourceName: "online_image")
            
        }
        
        if CBModel.providerFullDetalsModel.profilePic.length > 0 {
            
            musicianImageView.kf.setImage(with: URL(string: CBModel.providerFullDetalsModel.profilePic),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        self.bookButtonBackView.transform = CGAffineTransform(translationX: 0, y: 100)
        
        self.tableView.reloadData()
        
    }
    
    
    /// Initial spring animation for all views
    func showInitialAnimation(){
        
        self.tableHeaderView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 5,
                       options: [],animations: {
                        
                        
                        self.tableHeaderView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        
        }, completion: { (completed) in
            
            UIView.animate(withDuration: 0.8,
                           delay: 0.2,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 5, options: [], animations: {
                            
                            self.bookButtonBackView.transform = .identity
                            
            })
            
            
        })
        
        
    }
    
    func fetchDefaultCardDetails() {
        
        var defaultCardArray:[Any] = []
        
        if !Utility.defaultCardDocId.isEmpty {
            
            defaultCardArray = PaymentCardManager.sharedInstance.getDefaultCardDocumentDetailsFromCouchDB(documentId: Utility.defaultCardDocId)
        }
        
        if defaultCardArray.count > 0 {
            
            CBModel.selectedCardModel = CardDetailsModel.init(selectedCardDetails: defaultCardArray[0])
        }
        
    }
    
    
    func bookNowValidation() {
        
        if CBModel.selectedGigTimeId.length == 0 {
            
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.CONFIRM_BOOKING.MissingGigTime)
            
        } else if CBModel.appointmentLocationModel.pickupAddress.length == 0 {
            
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.CONFIRM_BOOKING.MissingAddress)
            
        } else if CBModel.selectedEventId.length == 0 {
            
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.CONFIRM_BOOKING.MissingEvent)
            
        }else if CBModel.paymentmethodTag == 0 && CBModel.selectedCardModel == nil {
            
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.CONFIRM_BOOKING.MissingPayment)
            
        } else {
            
            liveBookinAPI()
        }
        
        
        
    }
    
}

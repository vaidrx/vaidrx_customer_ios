//
//  vaiDRx_VerifyOTP_ButonActionExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_VerifyOTPVC{
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        
        
        self.verifyCodeAction()
    }
    
    @IBAction func rendOTOButtonAction(_ sender: UIButton) {
        resendOTPToMobileNumber()
    }
}

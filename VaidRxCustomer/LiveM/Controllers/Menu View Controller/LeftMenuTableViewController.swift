//
//  LeftMenuTableViewController.swift
//  Iserve
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher
import Social
import MessageUI

enum LeftMenu: Int {
//    case searchArtist = 0
    case myEvent = 0
    case payment
    case support
    case share
    case helpCenter
    case liveChat
    case reviews
    case address
    case liveMAbout
}

protocol LeftMenuProtocol: class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftMenuTableViewController: UITableViewController,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!

    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var actiVityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ProfileButtonOutlet: UIButton!
    
    var profilViewController: ProfileViewController!
    var homeScreenViewController: UIViewController!
    var paymentViewController: PaymentViewController!
    var reviewsViewController: ReviewsViewController!
    var yourAddressViewController: YourAddressViewController!
    var supportListViewController: SupportListViewController!
    var shareViewController: ShareViewController!
    var helpCenterViewController: TicketsViewController!
    var liveMViewController: LiveMViewController!
    var myEventViewController: MyEventViewController!
    var liveChatViewController: LiveChatViewController!
    
    let menuModal = LeftMenuViewModel()
    
    var storybrd:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    
    static var obj:LeftMenuTableViewController? = nil
    
    class func sharedInstance() -> LeftMenuTableViewController {
        
        return obj!
    }

    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        LeftMenuTableViewController.obj = self
        
        
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        self.setUpViewControllers()
        self.tableView.registerCellClass(BaseTableViewCell.self)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
//        self.navigationController?.view?.backgroundColor = UIColor(hex: "05C1C9") //UIColor.white //05C1C9
//        self.navigationController?.navigationBar.barTintColor = UIColor(hex: "05C1C9") //UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        showUserDetails()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    /// Method to show current user details
    func showUserDetails () {
        
        DispatchQueue.main.async { [self] in
            userNameLabel.text = "\(Utility.firstName) \(Utility.lastName)"
            
            self.profileImage.layer.borderWidth = 2
            self.profileImage.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x67AFF7).cgColor
            
        }
        
        
        if !Utility.profilePic.isEmpty {
            
           // actiVityIndicator.startAnimating()
            
            profileImage.kf.setImage(with: URL(string: Utility.profilePic),
                                     placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: nil,
                                     completionHandler: nil)
            
        } else {
            
            profileImage.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .myEvent, .payment, .reviews, .address, .support, .share, .helpCenter,  .liveMAbout:
                return BaseTableViewCell.height()
            case .liveChat:
                return 0
            }
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuModal.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .myEvent, .payment, .reviews, .address, .support, .share, .helpCenter, .liveChat, .liveMAbout:
                let cell:LeftMenuTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "LeftMenu") as! LeftMenuTableViewCell
                cell.setData(menuModal.menus[indexPath.row], image: menuModal.menusImages[indexPath.row])
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
    /// Custom Method to load all Left menu controllers
    func setUpViewControllers() -> Void {
        
        self.profilViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.profileVC) as? ProfileViewController
        self.profilViewController.delegate = self
        
        self.shareViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.shareVC) as? ShareViewController
        self.shareViewController.delegate = self
        
        self.paymentViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.paymentVC) as? PaymentViewController
        self.paymentViewController.delegate = self
        
        self.yourAddressViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.yourAddressVC) as? YourAddressViewController
        self.yourAddressViewController.delegate = self
        
        self.reviewsViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.reviewsVC) as? ReviewsViewController
        self.reviewsViewController.delegate = self
        
        self.supportListViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.supportVC) as? SupportListViewController
        self.supportListViewController.delegate = self
        
        self.helpCenterViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.zenDeskVC) as? TicketsViewController
        self.helpCenterViewController.delegate = self
        
        self.liveMViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.liveMVC) as? LiveMViewController
        self.liveMViewController.delegate = self
        
        
        self.myEventViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.myEventVC) as? MyEventViewController
        self.myEventViewController.delegate = self
        
        self.liveChatViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.liveChatVC) as? LiveChatViewController
        self.liveChatViewController.delegate = self
        
    
    }
    
    
    /// Show User Profile details button action
    ///
    /// - Parameter sender: Show User Profile details button object
    @IBAction func ProfileButtonAction(_ sender: Any) {
        
        self.profilViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.profileVC) as? ProfileViewController
        self.profilViewController.delegate = self
        
        Helper.getCurrentVC().navigationController?.pushViewController(self.profilViewController, animated: false)
        self.slideMenuController()?.closeLeft()
        self.slideMenuController()?.closeRight()

    }
    
    func configuredMailComposeViewController() {
        
        if MFMailComposeViewController.canSendMail() {
            
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setToRecipients(["helpdesk@vaidrx.com"])
//            mailComposerVC.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : APP_COLOR,NSBackgroundColorAttributeName : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
            
            mailComposerVC.navigationBar.isTranslucent = false
//            mailComposerVC.navigationBar.tintColor = UIColor.white
            
            
            mailComposerVC.setSubject("Help \(Utility.appName) app")
            mailComposerVC.setMessageBody("", isHTML: false)
            self.present(mailComposerVC, animated: true, completion: nil)
            
        } else {
            
            //            self.showSendMailErrorAlert()
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.EmailError)
            
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - LeftMenuProtocol
extension LeftMenuTableViewController : LeftMenuProtocol
{
    
    /// Method to maintain each left menu button action
    ///
    /// - Parameter menu: left menu class object
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
            
//        case .searchArtist:
//           // Helper.getCurrentVC().navigationController?.pushViewController(self.homeScreenViewController, animated: false)
//
//            break
            
        case .myEvent:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let pastViewController = storyboard.instantiateViewController(withIdentifier: "pastProvidersVC") as! vaiDRx_PastProvidersVC
            pastViewController.isComing = "Home"
            
            if let controller = Helper.getCurrentVC() as? vaiDRx_HomeVC{
                
                controller.confirmBookingViewModel.CBModel = controller.CBModel
                controller.confirmBookingViewModel.CBModel.providerId = ""
                controller.confirmBookingViewModel.CBModel.bookingType = 2
                controller.confirmBookingViewModel.CBModel.appointmentLocationModel = controller.appointmentLocationModel
                controller.confirmBookingViewModel.CBModel.appointmentLocationModel.bookingType = BookingType.Default
                pastViewController.confirmBookingViewModel = controller.confirmBookingViewModel
            }
            
            
            
            
            Helper.getCurrentVC().navigationController?.pushViewController(pastViewController, animated: false)
            
//            confirmBookingViewModel.CBModel = CBModel
//            confirmBookingViewModel.CBModel.providerId = ""
//            confirmBookingViewModel.CBModel.bookingType = 2
//            confirmBookingViewModel.CBModel.appointmentLocationModel = self.appointmentLocationModel
//            confirmBookingViewModel.CBModel.appointmentLocationModel.bookingType = BookingType.Default
//            confirmBookingViewModel.CBModel.appointmentLocationModel.pickupAddress = self.addressLabel.text!
//
//            pastViewController.confirmBookingViewModel = confirmBookingViewModel
            
//            let navController = UINavigationController(rootViewController: pastViewController) // Creating a navigation controller with pastViewController at the root of the navigation stack.
//            self.present(navController, animated:true, completion: nil)
            //                self.myEventViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.myEventVC) as! MyEventViewController
            //                self.myEventViewController.delegate = self
            
            //                Helper.getCurrentVC().navigationController?.pushViewController(self.myEventViewController, animated: false)
            break
        case .payment:
            
                            Helper.getCurrentVC().navigationController?.pushViewController(self.paymentViewController, animated: false)
            break
        case .reviews:
            
            //                self.reviewsViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.reviewsVC) as! ReviewsViewController
            //                self.reviewsViewController.delegate = self
            //
            //                Helper.getCurrentVC().navigationController?.pushViewController(self.reviewsViewController, animated: false)
            break
        case .address:
            
            //                Helper.getCurrentVC().navigationController?.pushViewController(self.yourAddressViewController, animated: false)
            break
        case .support:
            
            self.supportListViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.supportVC) as? SupportListViewController
            self.supportListViewController.delegate = self
            
            Helper.getCurrentVC().navigationController?.pushViewController(self.supportListViewController, animated: false)
            
        case .share:
            
            Helper.getCurrentVC().navigationController?.pushViewController(self.shareViewController, animated: false)
            
        case .helpCenter:
            configuredMailComposeViewController()
            
//            Helper.getCurrentVC().navigationController?.pushViewController(self.helpCenterViewController, animated: false)
            
        case .liveChat:
            
            self.liveChatViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.liveChatVC) as? LiveChatViewController
                            self.liveChatViewController.delegate = self
            
                            Helper.getCurrentVC().navigationController?.pushViewController(self.liveChatViewController, animated: false)
            break
            
        case .liveMAbout:
            
            //                Helper.getCurrentVC().navigationController?.pushViewController(self.liveMViewController, animated: false)
            break
        }
        
        self.slideMenuController()?.closeLeft()
        self.slideMenuController()?.closeRight()
    }
}

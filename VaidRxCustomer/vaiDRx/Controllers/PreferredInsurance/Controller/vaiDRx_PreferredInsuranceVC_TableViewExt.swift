//
//  vaiDRx_PreferredInsuranceVC_TableViewExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 01/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_PreferredInsuranceVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0:
            
            if insuranceResponseModel != nil {
                
                return (insuranceResponseModel?.allInsurances.count)!
            } else {
               return 7
            }
            
        case 1:
            return 1
        default:
            return 1
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_PreferredPaymentTVCell.self), for: indexPath) as! vaiDRx_PreferredPaymentTVCell
            
            if insuranceResponseModel != nil  {
                
                if isComing == "Profile" && profileResponseModel != nil && profileResponseModel?.preferredPayment == 1 &&  profileResponseModel?.insuranceName == ((insuranceResponseModel?.allInsurances[indexPath.row])! ["name"]) as! String {
                    
                    cell.paymentMethodLabelNew?.textColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
                    cell.selectedCheckImageNew.image = UIImage(named: "uncheck_unselected")
                } else {
                    
                    cell.paymentMethodLabelNew?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
                    cell.selectedCheckImageNew.image = UIImage(named: "check_unselected")
                }

                    cell.paymentMethodLabelNew.text = "\(((insuranceResponseModel?.allInsurances[indexPath.row])! ["name"]) as! String)"
            
            } else {
                
                cell.paymentMethodLabelNew.text = " "
            }
            

            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_PrefferedInsurance_TVCell.self), for: indexPath) as! vaiDRx_PrefferedInsurance_TVCell
            if isComing == "Profile" && profileResponseModel != nil && profileResponseModel?.preferredPayment == 2  {
                
                cell.insuranceName.text = profileResponseModel?.insuranceName
            } else {
                
                cell.insuranceName.text = ""
            }
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
}

extension vaiDRx_PreferredInsuranceVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 50
        case 1:
            if isComing == "Profile" && profileResponseModel != nil && profileResponseModel?.preferredPayment == 2 {
                return 75
            } else {
                return 50
            }
        default:
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.reloadData()
        switch indexPath.section {
            
        case 0:
            switch indexPath.row {
            case indexPath.row :
                let selectedCell = tableView.cellForRow(at: indexPath) as! vaiDRx_PreferredPaymentTVCell
                selectedCell.paymentMethodLabelNew?.textColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
                selectedCell.selectedCheckImageNew.image = UIImage(named: "uncheck_unselected")
                if isComing != "Profile" {
                    
                    registerViewModel.categoryId = (((insuranceResponseModel?.allInsurances[indexPath.row])! ["_id"]) as? String)!
                    self.performSegue(withIdentifier: "toCardDetails", sender: self)
//                    serviceRequestToMakeRegister()
                    
                } else {
                    
                    preferredInsuranceViewModel.paymentType = PaymentType.Insurance

                    if insuranceResponseModel != nil {
                        preferredInsuranceViewModel.insurance = (((insuranceResponseModel?.allInsurances[indexPath.row])! ["_id"]) as? String)!
                        delegate?.paymentSelected(PaymentType.Insurance, Insurance: preferredInsuranceViewModel.insurance)
                        self.serviceRequestToUpdateProfile()
                        
                    } else {
                        
                        delegate?.paymentSelected(PaymentType.Insurance, Insurance: "Empty")
                    }

                }
                
                
            default:
                let selectedCell = tableView.cellForRow(at: indexPath) as! vaiDRx_PreferredPaymentTVCell
                selectedCell.paymentMethodLabelNew?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
                selectedCell.selectedCheckImageNew.image = UIImage(named: "check_unselected")
            }
            break
            
        case 1:
            if isComing != "Profile" {
                
                registerViewModel.paymentType = PaymentType.CustomInsurance
            }
            preferredInsuranceViewModel.paymentType = PaymentType.CustomInsurance
            let cancelBookingView = AddInsuranceView.sharedInstance
            cancelBookingView.isComing = self.isComing
            cancelBookingView.registerViewModel = self.registerViewModel
            cancelBookingView.delegate = self
            WINDOW_DELEGATE??.addSubview(cancelBookingView)
            
            cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
            
            
            UIView.animate(withDuration: 0.5,
                           delay: 0.0,
                           options: UIView.AnimationOptions.beginFromCurrentState,
                           animations: {
                            
                            cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                            
            }) { (finished) in

            }

            break
        default:
            break
        }
    }
    
}

extension vaiDRx_PreferredInsuranceVC: customInsuranceDelegate {
    
    func addedNewInsurance(model: RegisterViewModel?,orString: String) {
        
        if isComing != "Profile" {
            
            self.registerViewModel = model!
            self.performSegue(withIdentifier: "toCardDetails", sender: self)
        } else {
            
            delegate?.paymentSelected(PaymentType.CustomInsurance, Insurance: orString)
            preferredInsuranceViewModel.insurance = orString
            self.serviceRequestToUpdateProfile()
        }
    }
}

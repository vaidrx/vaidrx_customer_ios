//
//  AcceptVaidTodayAPI.swift
//  vaiDRx
//
//  Created by Rahul Sharma on 12/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class AcceptVaidTodayAPI {
    
    let disposebag = DisposeBag()
    let acceptBooking_Response = PublishSubject<APIResponseModel>()
    
    /// Method to Call Update Vaid Today Booking service API
    ///
    /// - Parameter profileRequestModel: model contains current customer details
    func updateTodayBookingServiceAPICall(bookingId:Int64) {
        
        let strURL = API.BASE_URL + API.METHOD.ACCEPT_BOOKING
        
        let requestParams: [String: Any] = [
            
            SERVICE_REQUEST.ACCEPT_TODAY_BOOKING.bookingId : bookingId
        ]
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Accepting)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.AcceptingBooking)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {

                
            case .AcceptingBooking:
                
                self.acceptBooking_Response.onNext(responseModel)
                
            default:
                break
            }
            
            break
        }
    }


}

//
//  ProfileDetailModal.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


struct ProfileRequestModel {
    
    var firstName = ""
    var lastName = ""
    var about = ""
    var dateOfBirth = ""
    var generes = ""
    var profilePicURL = ""
    var paymentType = 0
    var insurance = ""
    var authorization = ""
    var DOB = ""
    var placeName = ""
    var addLine1 = ""
    var addLine2 = ""
    var city = ""
    var state = ""
    var country = ""
    var placeId = ""
    var pincode = ""
    var taggedAs = ""
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var pharmacyId = ""
    
}

struct RxTodayModel {
    
    var firstName = ""
    var lastName = ""
    var about = ""
    var dateOfBirth = ""
    var generes = ""
    var profilePicURL = ""
    var paymentType = 0
    var insurance = ""
    var authorization = ""
    var DOB = ""
    var placeName = ""
    var addLine1 = ""
    var addLine2 = ""
    var city = ""
    var state = ""
    var country = ""
    var placeId = ""
    var pincode = ""
    var taggedAs = ""
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var pharmacyId = ""
    
}

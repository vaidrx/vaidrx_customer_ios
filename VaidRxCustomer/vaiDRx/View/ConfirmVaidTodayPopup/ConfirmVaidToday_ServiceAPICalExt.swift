//
//  ConfirmVaidToday_ServiceAPICalExt.swift
//  vaiDRx
//
//  Created by Rahul Sharma on 12/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmVaidToday {
    
    // Method to call Cancel booking Service API
    func cancelBookingAPI() {
        
        confirmBookingViewModel.bookingId = self.bookingId
        
        confirmBookingViewModel.cancelBookingAPICall{ (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.cancelBooking)
        }
        
    }
    
    // Method to call Cancel booking Service API
    func acceptBookingAPI() {
        
        confirmBookingViewModel.bookingId = self.bookingId
        
        confirmBookingViewModel.acceptBookingAPICall{ (statCode, errMsg, dataResp) in
            self.delegate?.bookingAccepted()
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.AcceptingBooking)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if dataResponse != nil {
                
                AppDelegate().defaults.set(GenericUtility.strForObj(object: dataResponse!), forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
//                self.apiTag = requestType.rawValue
                
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
                case .getCancelReasons:
                    
                    progressMessage = PROGRESS_MESSAGE.Loading
                    
                case .cancelBooking:
                    
                    progressMessage = PROGRESS_MESSAGE.CancelBooking
                
                case .AcceptingBooking:
                    progressMessage = PROGRESS_MESSAGE.AcceptingBooking
                    
                default:
                    
                    break
                }
                
//                self.acessClass.getAcessToken(progressMessage: progressMessage)
                
                
            }
            
            
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
            }
            
            break
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.getCancelReasons:
                
//                if let arrayOfCancelReasons = dataResponse  as? [Any] {
//
//                    showCancelReasons(cancelReasonResponse: arrayOfCancelReasons)
//                }
                break
                
            case RequestType.cancelBooking:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                MixPanelManager.sharedInstance.BookingCancelledEvent(bookingId: String(self.bookingId))
                
//                self.closeButtonAction(self.closeButton)
                
                        topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                
                        UIView.animate(withDuration: 0.6,
                                       delay: 0.2,
                                       options: UIView.AnimationOptions.beginFromCurrentState,
                                       animations: {
                
                                        self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                
                        }) { (finished) in
                
                            self.removeFromSuperview()
                            ConfirmVaidToday.share = nil
                
                        }
                
                Helper.getCurrentVC().navigationController?.popToRootViewController(animated: false)
                
                UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.BookingCancelled)
                UserDefaults.standard.synchronize()
                
                
                break
                
            case RequestType.AcceptingBooking :
                self.delegate?.bookingAccepted()
                topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                
                UIView.animate(withDuration: 0.6,
                               delay: 0.2,
                               options: UIView.AnimationOptions.beginFromCurrentState,
                               animations: {
                                
                                self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                                
                }) { (finished) in
                    
                    self.removeFromSuperview()
                    ConfirmVaidToday.share = nil
                    
                }
                break
                
                
                
            default:
                break
            }
            
            break
            
            
        default:
            
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
            }
            
            break
        }
        
    }
}

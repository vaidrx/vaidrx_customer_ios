//
//  GetPharmacyViewModel.swift
//  vaiDRx
//
//  Created by 3Embed on 08/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class GetPharmacyViewModel {
    let rxGetPharmacyAPI = GetPharmacyAPI()
    let disposeBag = DisposeBag()
    var getPharmacyModel : GetPharmacyModel!
    var selectedPharmacyId = ""
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var iPAddress = ""
    
    func createPharmacyModel(){
//        Helper.getIPAddress()
        getPharmacyModel = GetPharmacyModel()
        getPharmacyModel.latitude = latitude //13.028632
        getPharmacyModel.longitude = longitude //77.589519
        getPharmacyModel.ipAddress = UserDefaults.standard.value(forKey: USER_DEFAULTS.IP_ADDRESS.iPAddress) as? String ?? "112.196.35.202"
        
    }
    
    
    
    func getPharmacyAPICall(completion:@escaping (Int,String?,Any?) -> ()){
        createPharmacyModel()
        rxGetPharmacyAPI.getPharmacyServiceAPICall(getPharmacyModel: getPharmacyModel)
        if !rxGetPharmacyAPI.pharmacy_Response.hasObservers{
            rxGetPharmacyAPI.pharmacy_Response.subscribe(onNext: {(response) in
                if response.data[SERVICE_RESPONSE.Error] != nil {
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: response.data[SERVICE_RESPONSE_VAIDRX.Error] as! String)
                    return
                }
                completion(response.httpStatusCode,response.data[SERVICE_RESPONSE.ErrorMessage] as? String,response.data[SERVICE_RESPONSE.DataResponse])
            }, onError: {(error) in
                
            }).disposed(by: disposeBag)
        }
    }
    
    func updatePharmacyAPICall(completion:@escaping (Int,String?,Any?) -> ()){
        
        if pharName.count > 0 {
            rxGetPharmacyAPI.updatePharmacyServiceAPICall(selectedPharmacyId: pharId)
        } else {
            rxGetPharmacyAPI.updatePharmacyServiceAPICall(selectedPharmacyId: selectedPharmacyId)
        }
        
        
        if !rxGetPharmacyAPI.pharmacy_Update_Response.hasObservers{
            rxGetPharmacyAPI.pharmacy_Update_Response.subscribe(onNext: {(response) in
                if response.data[SERVICE_RESPONSE.Error] != nil {
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: response.data[SERVICE_RESPONSE_VAIDRX.Error] as! String)
                    return
                }
                completion(response.httpStatusCode,response.data[SERVICE_RESPONSE.ErrorMessage] as? String,response.data[SERVICE_RESPONSE.DataResponse])
            }, onError: {(error) in
                
            }).disposed(by: disposeBag)
        }
    }
}

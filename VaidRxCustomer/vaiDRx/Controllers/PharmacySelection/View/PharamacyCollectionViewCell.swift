//
//  PharamacyCollectionViewCell.swift
//  vaiDRx
//
//  Created by 3Embed on 07/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class PharamacyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var pharmacyName: UILabel!
    @IBOutlet weak var pharmacyLogoImage: UIImageView!
    
}

//
//  vaiDRx_CreatePasswordVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class vaiDRx_CreatePasswordVC: UIViewController, KeyboardDelegate  {
    
    // MARK: - Outlets -
    
    //Labels
    
    @IBOutlet weak var titleLabel: UILabel!
    
    //TextFeilds
    @IBOutlet weak var passwordTextFeild: UITextField!
    
    //Layout Constraint
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Variable Decleration -
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var registerViewModel: RegisterViewModel!
    var isComingFrom = ""
    var setPasswordViewModel = SetPasswordViewModel()
    var userID: [String:Any] = [:]
    let disposeBag = DisposeBag()
    var stripeKeyResponseModel :StripeKeyResponseModel!
    var vaidRx_AddNewCardViewModel = VaidRx_AddNewCardViewModel()
    var showPass = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // print("registerViewModel",registerViewModel.emailText.value)
        addObserveToVariables()
        getStripeKeyData()
        Helper.hideNavBarShadow(vc: self)
        OperationQueue.main.addOperation {
            self.passwordTextFeild.becomeFirstResponder()
        }
        
        // Do any additional setup after loading the view.
        
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isComingFrom == "signIn" {
            titleLabel.text = "Reset password"
        } else {
            titleLabel.text = "Create your account password"
        }
        appDelegate?.keyboardDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        appDelegate?.keyboardDelegate = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addObserveToVariables() {
        
        if (isComingFrom != "signIn" || isComingFrom == "SocialSignUp" ) {
            
            passwordTextFeild.rx.text
                .orEmpty
                .bind(to: registerViewModel.passwordText )
                .disposed(by: disposeBag)
        } else {
            
            passwordTextFeild.rx.text
                .orEmpty
                .bind(to: setPasswordViewModel.reEnterPasswordText )
                .disposed(by: disposeBag)
        }

    }

    @IBAction func tappedShowPass(_ sender: UIButton) {
        
        if showPass {
            passwordTextFeild.isSecureTextEntry = true
            sender.isSelected = false
            showPass = false
        } else {
            passwordTextFeild.isSecureTextEntry = false
            sender.isSelected = true
            showPass = true
        }
        
    }
    
}

//
//  RxToadyViewModel.swift
//  vaiDRx
//
//  Created by 3Embed on 11/03/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift

class RxToadyViwModel {
    
    var transferMyCurrentMedicins = 0
    var refillMyCurrentMedicins = 0
    var rxTodayAPI = RxTodayAPI()
    var disposeBag = DisposeBag()
    
    
    func rxTodayTransferMedicationsAPICall(completion:@escaping (Int,String?,Any?) -> ()){
        rxTodayAPI.rxTodayTransferMedicationsServiceAPICall(transferMedicins: transferMyCurrentMedicins)
        if !rxTodayAPI.rxToday_Response.hasObservers{
            rxTodayAPI.rxToday_Response.subscribe(onNext: {(response) in
                if response.data[SERVICE_RESPONSE.Error] != nil {
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: response.data[SERVICE_RESPONSE_VAIDRX.Error] as! String)
                    return
                }
                completion(response.httpStatusCode,response.data[SERVICE_RESPONSE.ErrorMessage] as? String,response.data[SERVICE_RESPONSE.DataResponse])
            }, onError: {(error) in
                
            }).disposed(by: disposeBag)
        }
    }
    
    
    func rxTodayRefillMedicationsAPICall(completion:@escaping (Int,String?,Any?) -> ()){
        rxTodayAPI.rxTodayRefillMedicationsServiceAPICall(refillMedicins: refillMyCurrentMedicins)
        if !rxTodayAPI.rxTOday_RefillMedications.hasObservers{
            rxTodayAPI.rxTOday_RefillMedications.subscribe(onNext: {(response) in
                if response.data[SERVICE_RESPONSE.Error] != nil {
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: response.data[SERVICE_RESPONSE_VAIDRX.Error] as! String)
                    return
                }
                completion(response.httpStatusCode,response.data[SERVICE_RESPONSE.ErrorMessage] as? String,response.data[SERVICE_RESPONSE.DataResponse])
            }, onError: {(error) in
                
            }).disposed(by: disposeBag)
        }
    }
    
}

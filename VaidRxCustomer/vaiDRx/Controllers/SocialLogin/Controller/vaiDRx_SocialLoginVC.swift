//
//  vaiDRx_SocialLoginVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore

class vaiDRx_SocialLoginVC: UIViewController {
    
    //variable Declarations
    var fbUserDataModel:UserDataModel!
    var gmailUserDataModel:UserDataModel!
    var isFBLogin : Bool! = false
    var isGoogleLogin : Bool! = false
    var loginViewModel = LoginViewModel()
    var registerViewModel = RegisterViewModel()
    var registerType = RegisterType.Google
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

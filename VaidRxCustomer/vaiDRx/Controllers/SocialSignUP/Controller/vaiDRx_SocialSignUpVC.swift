//
//  vaiDRx_SocialSignUpVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 26/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS
import RxSwift
import RxCocoa

class vaiDRx_SocialSignUpVC: UIViewController, KeyboardDelegate {
    
    
    @IBOutlet weak var firstNameTF: UITextField!
    
    @IBOutlet weak var lastNameTF: UITextField!
    

    
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    @IBOutlet weak var countryFlagImage: UIImageView!
    
    @IBOutlet weak var emailTF: UITextField!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var phoneNumberTextFeild: UITextField!
    
    // MARK: - Variable Decleration -
    var countryRegion: String!
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var registerViewModel: RegisterViewModel!
    let landingViewModel = LandingViewModel()
    let phoneUtil = NBPhoneNumberUtil()
    var isPhoneNumberValid: Bool = false
    var selectedCountry: Country!
    let disposeBag = DisposeBag()
    var UserDataModel:UserDataModel!
    var registerType = RegisterType.Google
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDataModel.firstName.isEmpty == false {
            firstNameTF.text = UserDataModel.firstName
        }
        if UserDataModel.lastName.isEmpty == false {
            lastNameTF.text = UserDataModel.lastName
        }
        
        if UserDataModel.phoneNumber.isEmpty == false {
            phoneNumberTextFeild.text = UserDataModel.phoneNumber
        }
        
        if UserDataModel.mailId.isEmpty == false {
            emailTF.text = UserDataModel.mailId
        }
        
//        registerViewModel.
        
        Helper.hideNavBarShadow(vc: self)
        setCountryCode()
        // Do any additional setup after loading the view.
        
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        addObserveToVariables()
        appDelegate?.keyboardDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        appDelegate?.keyboardDelegate = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCountryCode(){
        
        let countrie = NSLocale.current.regionCode
        let coutryModel = ContryNameModelClass()
        let countries = coutryModel.getCoutryDetailsFormjsonSerial()
        
        for country in countries  {
            if country["code"] == countrie {
                countryCodeLabel.text = country["dial_code"]
                countryRegion = country["code"]
                let imagestring = countryRegion
                let imagePath = "CountryPicker.bundle/\(imagestring!).png"
                countryFlagImage.image = UIImage(named: imagePath)
            }
        }
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
    
        checkFortheField()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func changeCountryAction(_ sender: UIButton) {
    
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier) as! CountryNameViewController
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)

        
        dstVC.countryDelegate = self
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    func addObserveToVariables() {
        
        phoneNumberTextFeild.rx.text
            .orEmpty
            .bind(to: landingViewModel.phoneNumberText)
            .disposed(by: disposeBag)
        
        phoneNumberTextFeild.rx.text
            .orEmpty
            .bind(to: registerViewModel.phoneNumberText )
            .disposed(by: disposeBag)
        
        emailTF.rx.text
            .orEmpty
            .bind(to: registerViewModel.emailText )
            .disposed(by: disposeBag)
        
        firstNameTF.rx.text
            .orEmpty
            .bind(to: registerViewModel.firstNameText )
            .disposed(by: disposeBag)
        
        lastNameTF.rx.text
            .orEmpty
            .bind(to: registerViewModel.lastNameText )
            .disposed(by: disposeBag)
        
    }
    
    func checkFortheField() {
        

        isPhoneNumberValid = true
        if (phoneNumberTextFeild.text?.count)! > 5 && isPhoneNumberValid {
            
            
            do {
                ///mobileNumber Validation
                let phoneNumber = try phoneUtil.parse(phoneNumberTextFeild.text!, defaultRegion: countryRegion)
                
                let validPhoneNumber: Bool = phoneUtil.isValidNumber(phoneNumber)
                if validPhoneNumber == true
                {
                    sendRequestToValidatePhoneNumber()
                }
                else
                {
                    Helper.alertVC(title: "Message", message: "Please Enter a Valid Mobile Number")
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }

            
        }else {
            Helper.alertVC(title: "Message", message: "This phone number is invalid")

        }
    }
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        

        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
//                        self.nextButtonBottomConstraint.constant = ((keyboardSize?.height)! + 30.0)
                        self.scrollView.contentInset.bottom = (keyboardSize?.height)!
                        self.view.layoutIfNeeded()
        })
    }
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.4) {
            
            self.scrollView.contentInset.bottom = 0.0
//            self.nextButtonBottomConstraint.constant = 30
            self.view.layoutIfNeeded()
        }
        
    }
    
    
    @IBAction func tapAction(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}

// MARK: - CountrySelectedDelegate -
extension vaiDRx_SocialSignUpVC: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryFlagImage.image = UIImage(named: imagePath)
        countryRegion = selectedCountry.country_code
        countryCodeLabel.text =  self.selectedCountry.dial_code
    }
    
}

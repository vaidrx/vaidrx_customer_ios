//
//  Utility.swift
//  DayRunner
//
//  Created by Rahul Sharma on 28/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

let SESSION_TOKEN = Utility.sessionToken
let DEVICE_TOKEN  = Utility.deviceId
let PUSH_TOKEN    = Utility.pushtoken

class Utility: NSObject {
    
    //App Name
    static var appName: String {
        return (Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String)!
    }
    
    static var appVersion: String {
        return (Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String)!
    }
    
    static var deviceName: String {
        return UIDevice.current.systemName as String
    }
    
    static var deviceVersion: String {
        return UIDevice.current.systemVersion as String
    }
    
    static var deviceMake: String {
        return "APPLE"
    }
    
    static var deviceId : String {
        if let id = UserDefaults.standard.string(forKey:USER_DEFAULTS.USER.DEVICEID){
            return id
        }
        return "Simulator"
    }
    
    static var pushtoken : String {
        if let token = UserDefaults.standard.string(forKey:USER_DEFAULTS.TOKEN.PUSH){
            return token
        }
        return ""
    }
    
    static var userType : Int
    {
        return 1
    }


    static var sessionToken : String {
        
        if let token = UserDefaults.standard.string(forKey:USER_DEFAULTS.TOKEN.SESSION){
            return token
        }
        return ""
    }
    
    static var accessToken: String {
        
        if let accessToken = UserDefaults.standard.string(forKey: USER_DEFAULTS.TOKEN.ACCESS) {
            return accessToken
        }
        return ""
    }
    
    static var customerId : String {
        
        if let customerId = UserDefaults.standard.string(forKey:USER_DEFAULTS.USER.CUSTOMERID){
            return customerId
        }
        return ""
        
    }
    
    static var zendeskRequesterId : String {
        
        if let zendeskRequesterId = UserDefaults.standard.string(forKey:USER_DEFAULTS.USER.ZenDeskRequesterID){
            return zendeskRequesterId
        }
        return ""
        
    }
    
    static var fcmTopic: String {
        
        if let fcmTopic = UserDefaults.standard.string(forKey:USER_DEFAULTS.USER.FCMTopic){
            return fcmTopic
        }
        return ""
        
    }


    
    static var currencySymbol : String {
//        if let currency = UserDefaults.standard.string(forKey:USER_DEFAULTS.USER.CURRENCYSYMBOL){
//            return currency
//        }
        return ConfigManager.sharedInstance.currencySymbol
    }
    
    
    static var firstName : String {
        if let name = UserDefaults.standard.string(forKey:USER_DEFAULTS.USER.FIRSTNAME){
            return name.capitalized
        }
        return ""
    }
    
    static var lastName : String {
        if let name = UserDefaults.standard.string(forKey:USER_DEFAULTS.USER.LASTNAME){
            return name.capitalized
        }
        return ""
    }
    
    static var mobileNumber : String {
        if let mobileNumber = UserDefaults.standard.string(forKey:USER_DEFAULTS.USER.MOBILE){
            return mobileNumber
        }
        return ""
    }
    
    static var profilePic : String {
        if let ppic = UserDefaults.standard.string(forKey:USER_DEFAULTS.USER.PIC){
            return ppic
        }
        return ""
    }
    

    static var emailId : String
    {
        if let proEmail = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.EMAIL){
            return proEmail
        }
        return ""
    }
    
    static var referralCode : String {
        if let referralCode = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.REFERRAL_CODE){
            return referralCode
        }
        return ""
    }
    
    static var ratingCategories : [[String]] {
        return ConfigManager.sharedInstance.categoriesArray
    }
    
    static var paymentGatewayAPIKey : String {
        
        if let paymentGatewayAPIKey = UserDefaults.standard.string(forKey: USER_DEFAULTS.PAYMENT_GATEWAY.API_KEY){
            return paymentGatewayAPIKey
        }
        return ConfigManager.sharedInstance.paymentGatewayKey
    }
    
    static var searchAddressDocId : String {
        
        if let searchAddressDocId = UserDefaults.standard.string(forKey:COUCH_DB.SEARCH_ADDRESS_DOCUMENT_ID){
            return searchAddressDocId
        }
        return ""
    }
    
    static var manageAddressDocId : String {
        
        if let manageAddressDocId = UserDefaults.standard.string(forKey:COUCH_DB.MANAGE_ADDRESS_DOCUMENT_ID){
            return manageAddressDocId
        }
        return ""
    }
    
    static var defaultCardDocId : String {
        
        if let defaultCardDocId = UserDefaults.standard.string(forKey:COUCH_DB.DEFAULT_CARD_DOCUMENT_ID){
            return defaultCardDocId
        }
        return ""
    }

    static var chatDocId : String {
        
        if let chatDocId = UserDefaults.standard.string(forKey:COUCH_DB.CHAT_DOCUMENT_ID){
            return chatDocId
        }
        return ""
    }
    
    static var iPAddress : String {
        
        if let iPAddress = UserDefaults.standard.string(forKey:USER_DEFAULTS.IP_ADDRESS.iPAddress){
            return iPAddress
        }
        return ""
    }

    
    static var goToUpcomingBooking : Bool {
        
        return UserDefaults.standard.bool(forKey:USER_DEFAULTS.USER.GoToUpcomingBookings)
    }
    
    static var newBookingisCreated : Bool {
        
        return UserDefaults.standard.bool(forKey:USER_DEFAULTS.USER.NewBookingisCreated)
    }
    
    static var bookingCancelled : Bool {
        
        return UserDefaults.standard.bool(forKey:USER_DEFAULTS.USER.BookingCancelled)
    }
    
    static var bookingReviewed : Bool {
        
        return UserDefaults.standard.bool(forKey:USER_DEFAULTS.USER.BookingReviewed)
    }

    
    // Get Mileage Matric Detail
    static var mileageMatric: Int {
        
        if let mileageMatric = UserDefaults.standard.value(forKey: USER_DEFAULTS.USER.MILEAGE_MATRIC) as? Int {
            
            return mileageMatric
        }
        return 0
    }
}

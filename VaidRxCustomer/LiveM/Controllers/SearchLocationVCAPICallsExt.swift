//
//  SearchLocationVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension SearchLocationViewController {
    
    func deleteAddressAPI(addressId:String) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        searchLocationViewModel.addressIdToDelete = addressId
        
        searchLocationViewModel.deleteAddressAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.deleteAddress)
        }
        
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if let dataRes = dataResponse as? String {
                
                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
                self.apiTag = requestType.rawValue
                
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
                case .deleteAddress:
                    
                    progressMessage = PROGRESS_MESSAGE.Deleting
                    
                default:
                    
                    break
                }
                
                self.acessClass.getAcessToken(progressMessage: progressMessage)
                
            }
            
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
            }
            
            break
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.deleteAddress:
                
                managedAddress.remove(at: selectedIndexPathToDeleteAddress.row)
                
                AddressCouchDBManager.sharedInstance.updateManageAddressDetailsToCouchDBDocument(manageAddressArray: managedAddress)
                
                self.tableView.reloadData()
                
            default:
                break
            }
            
        default:
            
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
            }
            
            break
        }
        
    }
    
}

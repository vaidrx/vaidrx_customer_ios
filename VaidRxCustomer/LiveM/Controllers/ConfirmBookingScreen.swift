//
//  ConfirmBookingScreen.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Foundation

class ConfirmBookingScreen:UIViewController {
    
    @IBOutlet weak var navigationBackButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableHeaderView: UIView!
    
    @IBOutlet weak var nameDetailsBackView: UIView!
    
    @IBOutlet weak var musicianNameLabel: UILabel!
    
    @IBOutlet weak var numberOfReviewsLabel: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet weak var milesLabel: UILabel!
    
    @IBOutlet weak var musicianImageView: UIImageView!
    
    @IBOutlet weak var musicianStatusImageView: UIImageView!
    
    @IBOutlet weak var bookButtonBackView: UIView!
    
    @IBOutlet weak var bookButton: UIButtonCustom!
    
    @IBOutlet var blurrView: UIView!
    
    @IBOutlet var navigationTopView: UIView!
    
    @IBOutlet var navigationTitleView: UIView!
    
    @IBOutlet var navigationTitleLabel: UILabel!
    
    @IBOutlet var blurrViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var bookButtonBackViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!

    @IBOutlet weak var navigationTopViewHeightConstraint: NSLayoutConstraint!
    
    
    
    
    
    var arrayOfHeaderTitles:[String] = []
    
    var apiTag:Int!
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var selectedDateCell: CBSelectedDateTableViewCell!
    var gigTimeCell: MDGigTimeTableViewCell!
    var eventStartCell: CBEventStartTableViewCell!
    var eventsCell: MDEventsTableViewCell!
    var addressCell:CBAddressTableViewCell!
    var promocodeCell:CBPromocodeTableViewCell!
    
    var CBModel:ConfirmBookingModel = ConfirmBookingModel.sharedInstance()
    
    let confirmBookingViewModel = ConfirmBookingViewModel()
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!

    
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        fetchDefaultCardDetails()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        acessClass.acessDelegate = self
        
//        tableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
        
        self.tableView.scrollRectToVisible(CGRect.init(x: 0, y: 0, width: 1, height: 1), animated: false)

        
        tableView.reloadData()
        
        setupGestureRecognizer()
        hideNavigation()
        showInitialAnimation()
        
//        self.view.bringSubview(toFront: self.navigationTopView)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
        showNavigation()
    }
    
    
    
    
    //MARK: - UIButton Actions -
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)

        
        self.navigationController?.popViewController(animated:false)
    }
    
    @IBAction func bookButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        bookNowValidation()
    }
    
    @IBAction func changeAddressButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let addressVC:YourAddressViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.yourAddressVC) as! YourAddressViewController
        
        addressVC.isFromConfirmBookingVC = true
        
        self.navigationController!.pushViewController(addressVC, animated: true)
        
    }
    
    @IBAction func addPaymentButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let paymentVC:PaymentViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.paymentVC) as! PaymentViewController
        
        paymentVC.isFromConfirmBookingVC = true
        paymentVC.CBModel = CBModel
        
        self.navigationController!.pushViewController(paymentVC, animated: true)
        
    }
    
    @IBAction func applyPromocodeButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func paymentSelectedButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let button = sender as? UIButton
        
        switch (button?.tag)! {
            
            case 1:
                
                CBModel.paymentmethodTag = 0
//                tableView.reloadSections(IndexSet(integer: 5), with: .none)
                self.tableView.reloadData()

                break
                
                
            case 2:
                
                CBModel.paymentmethodTag = 1
//                tableView.reloadSections(IndexSet(integer: 5), with: .none)
                self.tableView.reloadData()
                
                break
                
            default:
                break
        }
        
        
    }
    
    
}

extension ConfirmBookingScreen:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
            case RequestType.liveBooking.rawValue:
                
                liveBookinAPI()
                
                break
                
            default:
                break
        }
    }
    
}


extension ConfirmBookingScreen {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
}


extension ConfirmBookingScreen:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let yOffset = scrollView.contentOffset.y
        
//        if SCREEN_HEIGHT == 812 {//iPhoneX
//
//            yOffset = yOffset + 64
//        }
        
        if yOffset > 0 {
            
            if yOffset < tableHeaderView.frame.size.height - NavigationBarHeight {
                
                //Scroll to Top
                if blurrView.frame.size.height <= NavigationBarHeight {
                    
                    //show Navigation
                    self.navigationTopView.isHidden = false
                    self.navigationTitleView.isHidden = false
                    
                    blurrViewTopConstraint.constant = tableHeaderView.frame.size.height - NavigationBarHeight - 1
                    blurrView.alpha = 0
                    
                    DDLogDebug("\nNo - 1, y = \(yOffset)")
                    
                } else {
                    
                    hideNavigation()
                    blurrViewTopConstraint.constant = yOffset
                    blurrView.alpha = yOffset/(tableHeaderView.frame.size.height - NavigationBarHeight)
                    
                    DDLogDebug("\nNo - 2, y = \(yOffset)")
                    
                }
                
            } else {
                
                self.navigationTopView.isHidden = false
                self.navigationTitleView.isHidden = false
                
                blurrViewTopConstraint.constant = tableHeaderView.frame.size.height - NavigationBarHeight - 1
                blurrView.alpha = 0
                DDLogDebug("\nNo - 3, y = \(yOffset)")
                
            }
            
        } else {
            
            hideNavigation()
            blurrViewTopConstraint.constant = 0
            blurrView.alpha = 0
            
            DDLogDebug("\nNo - 4, y = \(yOffset)")
        }
        
        
    }

    
    func showNavigation() {
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.view?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    
    func hideNavigation() {
        
        self.navigationTopView.isHidden = true
        self.navigationTitleView.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.view?.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
    }

}


extension ConfirmBookingScreen: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

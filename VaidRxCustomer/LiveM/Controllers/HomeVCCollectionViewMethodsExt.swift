//
//  HomeVCCollectionViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
//import youtube_ios_player_helper
import CoreLocation
import GoogleMaps

extension HomeScreenViewController:iCarouselDataSource {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        
        if arrayOfProvidersModel.count == 0 {
            
            
//            if noArtistMessageToShow {
//
//                noArtistMessageToShow = false
            
                if SplashLoading.obj == nil {
                    
                    self.showNoArtistMessage(message: MusiciansListManager.sharedInstance().errMessageString)
                }
                
//            }
            
            return 0
            
        } else {
            
//            noArtistMessageToShow = true
            
//            if noAeristAlertController != nil {
//
//                noAeristAlertController.dismiss(animated: true, completion: nil)
//                self.noAeristAlertController = nil
//            }
            
            showMusiciansViewConstraints()

            return arrayOfProvidersModel.count
        }
        
    }
    
    func showMusiciansViewConstraints() {
        
        collectionBackViewHeightConstraint.constant = self.view.bounds.size.height * 0.28
        self.collectionView.layoutIfNeeded()
//        self.collectionView.isHidden = false
        
        self.collectionView.isHidden = true
        self.noMusiciansMessageBackView.isHidden = true
        self.showListBackViewBottomConstraint.constant = collectionBackViewHeightConstraint.constant + 10
//        self.currentLocationButtonBottomConstraint.constant = collectionBackViewHeightConstraint.constant + 10
        
    }
    
    func hideMusiciansViewConstraints(message:String) {
        
        collectionBackViewHeightConstraint.constant = 0
        self.collectionView.layoutIfNeeded()
        self.collectionView.isHidden = true
        
        if message.length > 0 {
            
            self.noMusiciansMessageBackView.isHidden = false
            Helper.showAlert(head: "Message", message: "No Vaid avalable")
            
        } else {
            
            self.noMusiciansMessageBackView.isHidden = true
        }
        
        
        self.showListBackViewBottomConstraint.constant = self.noMusiciansMessageBackView.bounds.size.height + 10
//        self.currentLocationButtonBottomConstraint.constant = self.noMusiciansMessageBackView.bounds.size.height + 10

    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let topView:ProviderCollectionView!
        
        let musicianDetails = arrayOfProvidersModel[index]
        
        //reuse view if available, otherwise create a new view
        if  view is ProviderCollectionView {
            
            topView = view as! ProviderCollectionView
            
        } else {
            
            topView = ProviderCollectionView.init(isForVertical: false, viewController: self)
            
            if topView.nameLabel != nil {
                
                topView.nameLabel.text = musicianDetails.firstName.capitalized + " " + musicianDetails.lastName.capitalized
                
            }
            
            if topView.milesLabel != nil {
                
                topView.milesLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: musicianDetails.distance, mileageMatric: musicianDetails.mileageMatric)//String(format:"%.2f miles away",musicianDetails.distance)
            }
            
            
            topView.defaultImageView.isHidden = false
            topView.actvityIndicator.startAnimating()
//            topView.youtubePlayerView.isHidden = true
            topView.blurrView.isHidden = true
            
//            topView.youtubePlayerView.tag = index
            
            topView.musicianId = musicianDetails.providerId
            
            let youtubeVideoId = Helper.extractYoutubeIdFromLink(link:musicianDetails.youtubeVideoLink)
            
            if !(youtubeVideoId?.isEmpty)! {
                
                let dict = [
                    "playsinline" : 1,
                    "showinfo" : 0,
                    "controls" : 1,
                    "modestbranding": 0,
                    "enablejsapi":1,
                    "autohide":2,
                    "rel":0,
                    "origin" : "http://www.youtube.com",
                    ] as [String : Any]
                
                //liwCttfeJ7E
                //C-7yMcSi6_8
                
                topView.youtubeVideoURL = youtubeVideoId!
//                let result = topView.youtubePlayerView.load(withVideoId: youtubeVideoId!, playerVars: dict)
//
//                topView.youtubePlayerView.webView?.allowsInlineMediaPlayback = true
                
//                DDLogVerbose("Result = \(result)")
                
//                topView.youtubePlayerView.delegate = self
                
                
            } else {
                
                topView.actvityIndicator.stopAnimating()
                topView.youtubeVideoURL = ""
            }
            
            
            if index == selectedProviderTag {
                
                selectedView = topView
                selectedProviderId = selectedView.musicianId
                
                UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 6, options: [],animations: {
                    
                    self.selectedView.topView.transform = CGAffineTransform(scaleX: 1.105, y: 1.105)
                    
                }, completion: { (completed) in
                    
                    
                })
                
            }
            
        }
        
        return topView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.08
        }
        return value
    }
    
    
    func focusMarker() {
        
        if selectedProviderTag >= 0 && prevSelectedMarkerTag >= 0 && arrayOfProvidersModel.count > 0 {
            
            let musicianDetail = arrayOfProvidersModel[selectedProviderTag]
            
            self.selectMarkerInMapView(markerTag: selectedProviderTag, prevMarkerTag:self.prevSelectedMarkerTag)
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(2.0)
            
            let coordinates = CLLocationCoordinate2DMake(CLLocationDegrees(musicianDetail.latitude), CLLocationDegrees(musicianDetail.longitude))
            
            let point = self.mapView.projection.point(for: coordinates)
            let updatedCamera =  GMSCameraUpdate.setTarget(self.mapView.projection.coordinate(for: point))
            
            mapView.animate(with: updatedCamera)
            
            CATransaction.commit()
            
            isFocusedMarkerPicked = true
            
        }
        
    }
    
    func showCurrentMusicianCollectionViewAnimation() {
        
        if selectedProviderTag >= 0 && self.arrayOfProvidersModel.count > 0 {
            
            if let providerView = self.collectionView.itemView(at: selectedProviderTag) as? ProviderCollectionView {
                
                UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 6, options: [],animations: {
                    
                    providerView.topView.transform = CGAffineTransform(scaleX: 1.105, y: 1.15)
                    
                }, completion: { (completed) in
                    
                    
                })

                
            }

        }
        
    }
    
    func pauseAllYoutubeVideo(){
        
        for i in 0..<arrayOfProvidersModel.count {
            
            if let providerView = self.collectionView.itemView(at: i) as? ProviderCollectionView {
                
//                providerView.youtubePlayerView.pauseVideo()
            }
        }
        
    }

    
}


extension HomeScreenViewController:iCarouselDelegate {
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {//End Scroll
        
        if selectedProviderTag == carousel.currentItemIndex {
            
            if  selectedView != nil {
                
                selectedView.nameLabel.textColor = UIColor.white
                selectedView.milesLabel.textColor = UIColor.white
                
                selectedProviderId = selectedView.musicianId
                
//                if selectedView.youtubePlayerView.playerState() == YTPlayerState.playing {
//
//                    selectedView.youtubePlayerView.pauseVideo()
//
//                } else {
//
//                    selectedView.youtubePlayerView.playVideo()
//                }
                
            }
            
        } else {
            
            
            if selectedView != nil {
                
                UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 6, options: [],animations: {
                    
                    self.selectedView.topView.transform = .identity
                    
                }, completion: { (completed) in
                    
                    
                })
                
                selectedView.nameLabel.textColor = UIColor.lightGray
                selectedView.milesLabel.textColor = UIColor.lightGray
//                selectedView.youtubePlayerView.pauseVideo()
                
            }
            
            if carousel.currentItemIndex >= 0{
                
                selectedProviderTag = carousel.currentItemIndex
                
            } else {
                
                selectedProviderTag = 0
            }
            
            
            focusMarker()
            
            UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 6, options: [],animations: {
                
                if let musicianView = carousel.currentItemView as? ProviderCollectionView {
                    
                    musicianView.topView.transform = CGAffineTransform(scaleX: 1.105, y: 1.105)
                }
                
            }, completion: { (completed) in
                
                
            })
            
            
            if let topView = carousel.currentItemView as? ProviderCollectionView {
                
                selectedView = topView
                
                selectedProviderId = selectedView.musicianId
                
                selectedView.nameLabel.textColor = UIColor.white
                selectedView.milesLabel.textColor = UIColor.white
                
//                selectedView.youtubePlayerView.playVideo()
            }
            
        }
        
        
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
    
    func carouselDidEndDragging(_ carousel: iCarousel, willDecelerate decelerate: Bool) {
        
    }
    
    func carousel(_ carousel: iCarousel, shouldSelectItemAt index: Int) -> Bool {
        
        return true
    }
    
}

//extension HomeScreenViewController:YTPlayerViewDelegate {
//
//    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
//
//        if isVCisDispappeared {
//
//            if state == YTPlayerState.buffering || state == YTPlayerState.playing {
//
//                playerView.pauseVideo()
//            }
//        }
//    }
//
//    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
//
//        if isVCisDispappeared {
//
//            if playerView.playerState() == YTPlayerState.buffering || playerView.playerState() == YTPlayerState.playing {
//
//                playerView.pauseVideo()
//            }
//        }
//    }
//
//    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
//
//    }
//
//    func playerView(_ playerView: YTPlayerView, didChangeTo quality: YTPlaybackQuality) {
//
//        if isVCisDispappeared {
//
//            if playerView.playerState() == YTPlayerState.buffering || playerView.playerState() == YTPlayerState.playing {
//
//                playerView.pauseVideo()
//            }
//        }
//    }
//
//    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
//
//        if let providerView = self.collectionView.itemView(at: playerView.tag) as? ProviderCollectionView {
//
//            providerView.defaultImageView.isHidden = true
//            providerView.blurrView.isHidden = false
//            providerView.actvityIndicator.stopAnimating()
//            providerView.youtubePlayerView.isHidden = false
//
//        }
//
//    }
//
//}

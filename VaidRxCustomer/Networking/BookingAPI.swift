//
//  BookingAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class BookingAPI {
    
    let disposebag = DisposeBag()
    let getBookingDetails_Response = PublishSubject<APIResponseModel>()
    let submitReview_Response = PublishSubject<APIResponseModel>()
    let disconnectCallResponse = PublishSubject<APIResponseModel>()
    
    
    /// Method to call get particular booking details Service API
    ///
    /// - Parameter methodName: name of the method
    func getBookingDetailsServiceAPICall(methodName:String) {
        
        let strURL = API.BASE_URL + methodName
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.getParticularApptDetails)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to call get particular booking details Service API
    ///
    /// - Parameter methodName: name of the method
    func disconnectBookingCall(methodName:String,bookingID:Int64) {
        
        let strURL = API.BASE_URL + methodName
        
        print(bookingID)
        let requestParams:[String:Any] = [
            "bookingId": bookingID
        ]
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.disconnectBookingCall)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to call submit review service API
    ///
    /// - Parameters:
    ///   - bookingId: booking id to submit review
    ///   - ratingValue: rating value given by customer
    ///   - reviewText: review comment given by customer
//    func submitReviewServiceAPICall(bookingId:Int64,
//                                    ratingValue:Double,
//                                    reviewText:String)
    func submitReviewServiceAPICall(proRate: RateProviderModel) {
        
        let strURL = API.BASE_URL + API.METHOD.REVIEWANDRATING
        
        let requestParams:[String:Any] = [
//            SERVICE_REQUEST.SUBMIT_REVIEW.bookingId: bookingId,
            SERVICE_REQUEST.SUBMIT_REVIEW.bookingId: proRate.bookingID!,
            SERVICE_REQUEST.SUBMIT_REVIEW.ratingValue: Float(proRate.rating),
//            SERVICE_REQUEST.SUBMIT_REVIEW.reviewText: proRate.review
        ]
        
        Helper.showPI(_message: PROGRESS_MESSAGE.SubmittingReview)

        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.reviewAndRating)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
                case .getParticularApptDetails:
                    
                    self.getBookingDetails_Response.onNext(responseModel)
                
                case .reviewAndRating:
                
                    self.submitReview_Response.onNext(responseModel)
                
            case .disconnectBookingCall:
                   
                self.disconnectCallResponse.onNext(responseModel)
                
                default:
                    break
            }
            
            break
        }
    }
    
}

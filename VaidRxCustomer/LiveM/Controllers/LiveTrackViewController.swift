//
//  LiveTrackViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps
import MessageUI
import Kingfisher

class LiveTrackViewController:UIViewController {
    
    @IBOutlet var navigationLeftButton: UIButton!
    
    @IBOutlet var cancelButton: UIButton!
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var mapView: GMSMapView!
    
    @IBOutlet var onlineStatusImageView: UIImageView!
    
    @IBOutlet var musicianImageView: UIImageView!
    
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var musicianNameLabel: UILabel!
    
    @IBOutlet var ratingView: FloatRatingView!
    
    @IBOutlet var reviewsLabel: UILabel!
    
    @IBOutlet var viewProfileButton: UIButton!
    
    @IBOutlet var callButton: UIButton!
    
    @IBOutlet var messageButton: UIButton!
    
    
    @IBOutlet weak var distanceValueLabel: UILabel!
    
    @IBOutlet weak var etaValueLabel: UILabel!
    
    
    var bookingDetailModel:BookingDetailsModel!
    
    var appointmentLocMarker:GMSMarker!
    var providerMarker:GMSMarker!
    
    var cancelBookingScreen:CancelBookingScreen!
    
    let liveTrackResponseManager:LiveTrackResponseManager = LiveTrackResponseManager.sharedInstance()
    
    let distanceAndETAManager = DistanceAndETAManager.sharedInstance
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!

    
    
    // Shared instance object for gettting the singleton object
    static var obj:LiveTrackViewController? = nil
    
    class func sharedInstance() -> LiveTrackViewController {
        
        return obj!
    }
    
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LiveTrackViewController.obj = self
        showInitialData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        Helper.statusBarView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        liveTrackResponseManager.delegate = self
        subscribeToProviderTopic()
        distanceAndETAManager.delegate = self
        
        distanceAndETAManager.mileageMatric = bookingDetailModel.mileageMatric
        
        setupGestureRecognizer()
        
        if bookingDetailModel.bookingStatus > 6 {
            
            cancelButton.isHidden = true
            
        } else {
            
            cancelButton.isHidden = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.markersToFitInMapView()
        
        self.focusOnProviderMarkerOnMapView(mapZoomLevel: MAP_ZOOM_LEVEL)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        liveTrackResponseManager.delegate = nil
        distanceAndETAManager.delegate = nil
        liveTrackResponseManager.unsubscribeToParticularProvider(providerId: bookingDetailModel.providerId)
    }
    
    func subscribeToProviderTopic() {
        
        liveTrackResponseManager.subscribeToParticularProvider(providerId: bookingDetailModel.providerId)
    }
    
    
    @IBAction func cancelButtonAction(_ sender: Any) {
                
        cancelBookingScreen = CancelBookingScreen.sharedInstance
        
        cancelBookingScreen.bookingID = bookingDetailModel.bookingId
        
        WINDOW_DELEGATE??.addSubview(cancelBookingScreen)
        
        cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { (finished) in
            
            self.cancelBookingScreen.getCancelReasonsAPI()
            
        }
        


    }
    
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionReveal,
//                                                                  subType: kCATransitionFromBottom,
//                                                                  for: (self.navigationController?.view)!,
//                                                                  timeDuration: 0.3)
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func viewProfileButtonAction(_ sender: Any) {
        
        let musicianDetailsVC:MusicianDetailsViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.musicianDetailsVC) as! MusicianDetailsViewController
        
        let providerDetailsModel = MusicianDetailsModel.init(bookingFlowMusicianDetails: bookingDetailModel.providerDict)
        
        musicianDetailsVC.providerDetailFromPrevController = providerDetailsModel
        musicianDetailsVC.isFromLiveTrackVC = true
        
        self.navigationController!.pushViewController(musicianDetailsVC, animated: true)
        
    }
    
    @IBAction func callButtonAction(_ sender: Any) {
        
        if let phoneCallURL = URL(string: "tel://\(bookingDetailModel.providerPhoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            
            if (application.canOpenURL(phoneCallURL)) {
                
                application.openURL(phoneCallURL)
                
            } else {
                
                Helper.showAlert(head: ALERTS.Missing, message:ALERTS.MissingCallFeature)
            }
        } else {
            
            Helper.showAlert(head: ALERTS.Missing, message:ALERTS.MissingCallFeature)
        }

    }
    
    @IBAction func messageButtonAction(_ sender: Any) {
        
        /*let textMessageRecipients = [bookingDetailModel.providerPhoneNumber]
        
        if (MFMessageComposeViewController.canSendText()) {
            
            // Present the configured MFMessageComposeViewController instance
            // Note that the dismissal of the VC will be handled by the messageComposer instance,
            // since it implements the appropriate delegate call-back
            
            let messageComposeVC = MFMessageComposeViewController()
            
            messageComposeVC.messageComposeDelegate = self  //  Make sure to set this property to self, so that the controller can be dismissed!
            messageComposeVC.recipients = textMessageRecipients
            messageComposeVC.body = ""
            
            present(messageComposeVC, animated: true, completion: nil)
            
        } else {
            
            // Let the user know if his/her device isn't able to send text messages
            Helper.showAlert(head: ALERTS.Missing, message:ALERTS.MissingMessageFeature)
        }*/
        
        let chatVC:ChatViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.chatVC) as! ChatViewController
        
        
        chatVC.bookingId = String(bookingDetailModel.bookingId)
        chatVC.musicianId = bookingDetailModel.providerId
        chatVC.musicianName = bookingDetailModel.providerName
        chatVC.musicianImageURL = bookingDetailModel.providerImageURL

        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                  subType: kCATransitionFromTop,
//                                                                  for: (self.navigationController?.view)!,
//                                                                  timeDuration: 0.3)
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.pushViewController(chatVC, animated: false)
        
    }
    
    
}

extension LiveTrackViewController:MFMessageComposeViewControllerDelegate {
    
    // MFMessageComposeViewControllerDelegate callback - dismisses the view controller when the user is finished with it
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        controller.dismiss(animated: true, completion: nil)
    }
    
}





extension LiveTrackViewController {
    
    func showInitialData() {
        
        showInitialMapViewProperties()
        
        musicianNameLabel.text = bookingDetailModel.providerName
        
        
        if bookingDetailModel.providerImageURL.length > 0 {
            
            musicianImageView.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        ratingView.emptyImage = #imageLiteral(resourceName: "review_star_grey_icon")
        ratingView.fullImage = #imageLiteral(resourceName: "review_star_red_icon")
        ratingView.floatRatings = true
        
        if bookingDetailModel.providerStatus == 0 {
            
            onlineStatusImageView.image = #imageLiteral(resourceName: "offline_image")
            
        } else {
            
            onlineStatusImageView.image = #imageLiteral(resourceName: "online_image")
        }
        
        ratingView.rating = Float(bookingDetailModel.providerRating)
        
        if bookingDetailModel.numberOfReviews == 0 {
            
            reviewsLabel.text = "No review"
            
        } else if bookingDetailModel.numberOfReviews == 1 {
            
            reviewsLabel.text = "1 review"
            
        } else {
            
            reviewsLabel.text = "\(bookingDetailModel.numberOfReviews) reviews"
        }
        
    }
}

extension LiveTrackViewController:DistanceAndETAManagerDelegate {
    
    func distanceAndETARsponse(timeInMinute: String, distance: String) {
        
        if providerMarker != nil {
            
            providerMarker.snippet = "Distance: \(distance)\nETA: \(timeInMinute)"
            distanceValueLabel.text = distance
            etaValueLabel.text = timeInMinute
        }
    }
}

extension LiveTrackViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension LiveTrackViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}

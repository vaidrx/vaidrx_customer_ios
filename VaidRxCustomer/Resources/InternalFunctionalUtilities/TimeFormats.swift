//
//  TimeFormats.swift
//  DayRunner
//
//  Created by Rahul Sharma on 05/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class TimeFormats: NSObject {
    
    /********************************************************/
    //MARK: - TimeFormats
    /********************************************************/
    
    /// Current Date and Time
    static var currentDateTime: String {
        
        let now = Date()
        let dateFormatter = DateFormatter()
        let indianLocale = NSLocale(localeIdentifier: "en_US")
        dateFormatter.locale = indianLocale as Locale?
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: now)
    }
    
    /// Current Date and Time
    static var currentDateWithoutTime: String {
        
        let now = Date()
        let dateFormatter = DateFormatter()
        let indianLocale = NSLocale(localeIdentifier: "en_US")
        dateFormatter.locale = indianLocale as Locale?
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.string(from: now)
    }
    
    class func timeInterval(withStartedTime jobStartedAt: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fromDate: Date? = dateFormatter.date(from: jobStartedAt)
        let toDate = Date()
        let gregorianCalendar = Calendar(identifier: .gregorian)
        let components : DateComponents = gregorianCalendar.dateComponents([.hour,.minute,.second], from: fromDate!
            , to: toDate)
        let hour = Int((components.hour)!)
        let min = Int((components.minute)!)
        let sec = Int((components.second)!)
        return String(format: "%02d:%02d:%02d", hour, min, sec)
    }
    
    //currentdateandtime
    
    class func changeDateFormate(_ originalDate: String) -> String {
        
        guard originalDate.isEmpty == false else {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date: Date? = dateFormatter.date(from: originalDate)
        dateFormatter.dateFormat = "MMM dd, HH:mm a" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date!)
        
        return formattedDateString
    }
    
    
    /// Current time stamp
    static var currentTimeStamp: String {
        return String(format: "%0.0f", Date().timeIntervalSince1970 * 1000)
    }
    
    /// Current time Stamp in Int64 format
    static var currentTimeStampInt64: Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    class func roundDate(_ mydate: Date) -> Date {
        
        var newDate = mydate
        
        // Get the nearest 5 minute block
        let time: DateComponents? = Calendar.current.dateComponents([.hour, .minute], from: mydate)
        let minutes: Int? = time?.minute
        let remain: Int = minutes! % 15
        
        // Add the remainder of time to the date to round it up evenly
        newDate = newDate.addingTimeInterval(TimeInterval(60 * (15 - remain)))
        return newDate
    }

    
}


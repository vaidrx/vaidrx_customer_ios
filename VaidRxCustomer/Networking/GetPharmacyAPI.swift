//
//  GetPharmacyAPI.swift
//  vaiDRx
//
//  Created by 3Embed on 08/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class GetPharmacyAPI{
    let disposeBag = DisposeBag()
    var pharmacy_Response = PublishSubject<APIResponseModel>()
    var pharmacy_Update_Response = PublishSubject<APIResponseModel>()
    
    func getPharmacyServiceAPICall(getPharmacyModel:GetPharmacyModel){
         let strURL = API.BASE_URL + API.METHOD.PHARMACY + "/\(getPharmacyModel.latitude)" + "/\(getPharmacyModel.longitude)" + "/\(getPharmacyModel.ipAddress)"
        
        let requestParams : [String :Any] = [
        
            SERVICE_REQUEST.Latitude : getPharmacyModel.latitude,
            SERVICE_REQUEST.Longitude : getPharmacyModel.longitude,
            SERVICE_REQUEST.IpAddress : getPharmacyModel.ipAddress
//            SERVICE_REQUEST.Authorization : getPharmacyModel.authorization
        ]
        
        print("pharmacy requestParams",requestParams)
        
        Helper.showPI(_message: PROGRESS_MESSAGE.PharmacyDetails)
        
        RxAlamofire.requestJSON(.get, strURL, parameters: nil, encoding: JSONEncoding.default, headers: NetworkHelper.sharedInstance.getAOTHHeader()).subscribe(onNext: { (r,json) in
            
            print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
            if let dict = json as? [String:Any]{
                let statusCode = r.statusCode
                self.checkResponse(statusCode: statusCode, responseDict: dict,requestType: RequestType.getPharmacy)
            }
            Helper.hidePI()
        }, onError: { (error) in
            print("API Response \(strURL)\nError:\(error.localizedDescription)")
            
            Helper.hidePI()
            Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
        }).disposed(by: disposeBag)
    }
    
    func updatePharmacyServiceAPICall(selectedPharmacyId:String){
        
        let strURL = API.BASE_URL + API.METHOD.UPDATEPROFILE
        
        let requestParams : [String:Any] = [
//            SERVICE_REQUEST.Authorization : Utility.sessionToken,
            SERVICE_REQUEST.PharmacyId : selectedPharmacyId
        ]
        
        Helper.showPI(_message: PROGRESS_MESSAGE.UpdatingPharmacy)
        RxAlamofire.requestJSON(.patch, strURL, parameters: requestParams, encoding: JSONEncoding.default, headers: NetworkHelper.sharedInstance.getAOTHHeader()).subscribe(onNext: { (r,json) in
            
            print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
            if let dict = json as? [String:Any]{
                let statusCode = r.statusCode
               self.checkResponse(statusCode: statusCode, responseDict: dict,requestType: RequestType.updateProfileData)
            }
            Helper.hidePI()
        }, onError: { (error) in
            
            print("API Response \(strURL)\nError:\(error.localizedDescription)")
            Helper.hidePI()
            Helper.showAlert(head: ALERTS.Error, message: error.localizedDescription)
        }).disposed(by: disposeBag)
    }
    
    func checkResponse(statusCode:Int,responseDict:[String:Any],requestType:RequestType){
        switch statusCode {
        case HTTPSResponseCodes.BadRequest.rawValue:
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String{
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
            }
            break
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
        case HTTPSResponseCodes.MusicianList.NoMusicians.rawValue:
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
        default:
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            
            switch requestType{
            case .updateProfileData:
                pharmacy_Update_Response.onNext(responseModel)
                break
            case .getPharmacy:
                self.pharmacy_Response.onNext(responseModel)
                break
            default :
                break
            }
            
            
            break
        }
    }
    
    
}

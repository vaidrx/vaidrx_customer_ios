//
//  PendingBookingCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension PendingBookingViewController {
    
    func setInitialData() {
        
        arrayOfData = [
            "Date",
            "Type of Event",
            "Time",
            "Gig Time",
            "Price",
            "Payment"
        ]
        
        showBookingDetails()
    }
    
    func showBookingDetails() {
        
        showInitialMapViewProperties()
        
        addressLabel.text = bookingEventModel.address1
        milesLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: bookingEventModel.distance, mileageMatric: bookingEventModel.mileageMatric)//String(format:"%.2f miles away",bookingEventModel.distance)
        
        providerNameLabel.text = bookingEventModel.providerName
        
        
        if bookingEventModel.providerImageURL.length > 0 {
            
            providerImageView.kf.setImage(with: URL(string: bookingEventModel.providerImageURL),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            providerImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        ratingView.emptyImage = #imageLiteral(resourceName: "review_star_grey_icon")
        ratingView.fullImage = #imageLiteral(resourceName: "review_star_red_icon")
        ratingView.floatRatings = true
        
        ratingView.rating = Float(bookingEventModel.providerRating)
        
        
        let startDate = Date(timeIntervalSince1970: TimeInterval(bookingEventModel.bookingRequestedAt))
        
        let endDate = Date(timeIntervalSince1970: TimeInterval(bookingEventModel.bookingEndTime))
        
        totalDuration = endDate.timeIntervalSince(startDate)
        
        remainingTimeValue = Int(totalDuration)
        
        if endDate.timeIntervalSinceNow > 0 {
            
            if startDate.timeIntervalSinceNow < 0 {
                
                remainingTimeValue = Int(endDate.timeIntervalSinceNow)
                setRemainingTimeProgress()
                
            } else {
                
                setRemainingTimeProgress()
            }
            
            
            
        } else {
            
            remainingTimeValue = 0
            
            timer.invalidate()
            
            remainingTimeView.setProgress(value: 0, animationDuration: 0) {
                
                self.showRemainingTimeDetails()
            }
            
        }
        
        self.tableView.reloadData()
        
    }
    
    
    
    
}

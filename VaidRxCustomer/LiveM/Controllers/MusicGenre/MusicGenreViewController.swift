//
//  MusicGenreViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

protocol MusicGenreViewDelegate {
    
    func setGenresData(data: [MusicGenreModel])
}


class MusicGenreViewController: UIViewController, AccessTokeDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var doneButton: UIButton!
    
    var genresData: [MusicGenreModel] = []
    var sellectedGenres: [Any] = []
    var delegate: MusicGenreViewDelegate?
    let acessClass = AccessTokenRefresh()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var apiTag: Int!
    var selectedMusicGenres: [MusicGenreModel] = []
    var prevSelectedMusicGenres: [MusicGenreModel] = []
    let musicGenreViewModel = MusicGenreViewModel()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.doneButton.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDelegate?.accessTokenDelegate = self
        makeApiCall()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate?.accessTokenDelegate = nil
    }
    
    @IBAction func backButonAction(_ sender: Any) {
        backButton()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        
        delegate?.setGenresData(data: selectedMusicGenres)
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    /// to move back to last controller
    func backButton() {
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    func setGenresData(data: [Any]){
        
        delegate?.setGenresData(data: selectedMusicGenres)
    }
    
    /// to recall the api
    @objc func recallApi() {
        
        makeApiCall()
    }
    
    
}


extension MusicGenreViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if genresData.count >= 1 {
            
            return genresData.count
            
        }else {
            
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let musicGenreCell:MusicGenereTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "musicGenresCell") as? MusicGenereTableViewCell)!
        
        
        let genresDataModel = genresData[indexPath.row]
        
        musicGenreCell.titleLabel.text = genresDataModel.musicGenreName
       
        
        if selectedMusicGenres.count > 0 {
            
            if let index = selectedMusicGenres.index(where: {$0.musicGenreId == genresDataModel.musicGenreId}) {
               
                DDLogDebug("\(index)")
                musicGenreCell.selectedImageView.image = #imageLiteral(resourceName: "Ellipse3")
                musicGenreCell.titleLabel.textColor = RED_COLOR

            } else {
                
                // item could not be found
                musicGenreCell.selectedImageView.image = #imageLiteral(resourceName: "Ellipse2")
                musicGenreCell.titleLabel.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            }
            
        } else {
            
            musicGenreCell.selectedImageView.image = #imageLiteral(resourceName: "Ellipse2")
            musicGenreCell.titleLabel.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        return musicGenreCell
        
    }
    
}


extension MusicGenreViewController: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH! - 50, height: 0))
        label.font = UIFont.init(name: FONTS.HindRegular, size: 14)
        
        let genresDataModel = genresData[indexPath.row]
        
        label.text = genresDataModel.musicGenreName
        
        return Helper.measureHeightLabel(label, width: SCREEN_WIDTH! - 50) + 30
        
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let genresDataModel = genresData[indexPath.row]
        
        if selectedMusicGenres.count > 0 {
            
            if let index = selectedMusicGenres.index(where: {$0.musicGenreId == genresDataModel.musicGenreId}) {
                
                 selectedMusicGenres.remove(at: index)
                
            } else {
                
                // item could not be found
                selectedMusicGenres.append(genresDataModel)
            }
            
        } else {
            
           selectedMusicGenres.append(genresDataModel)
        }
        
        if selectedMusicGenres.count > 0{
            
            self.doneButton.isHidden = false
            
        } else {
            
            self.doneButton.isHidden = true
        }
        
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)

    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}


//
//  vaiDRx_AddNewCard_APICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_AddNewCardVC {
    
    
    func getStripeKeyData(){
        
        if !NetworkHelper.sharedInstance.networkReachable(){
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
            //Helper.showAlertMessage(title: ALERT_VAIDRX.Error, message: ALERT_VAIDRX.NoInternet)
            return
        }
        
        vaidRx_AddNewCardViewModel.getStripeKeyAPICall { (statusCode, errMsg, response) in
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response)
        }
    }
    
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResp:Any?){
        switch statusCode {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            stripeKeyResponseModel = StripeKeyResponseModel.init(stripeKEyResponseDetails:dataResp as Any )
            
//            insuranceItems = insuranceData.insuranceItemsModel
//            DispatchQueue.main.async {
//                self.tableView.reloadData()
//            }
            publishableKey = stripeKeyResponseModel.publishableKey
            print("insuranceItems",stripeKeyResponseModel)
            
            
//            print("data response",dataResp!)
        default:
            break
        }
    }
    
    func serviceRequestToMakeRegister() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        
//        registerViewModel.registerType = userRegisterType
        
        registerViewModel.registerAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.signUp, userRegisterType: self.userRegisterType)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType, userRegisterType:RegisterType?)
    {
        if HTTPSResponseCodes.Register(rawValue: statusCode) != nil {
            
            let responseCodes : HTTPSResponseCodes.Register = HTTPSResponseCodes.Register(rawValue: statusCode)!
            
            switch responseCodes {
                
            case .WrongInputData://wrong data
                
                break
                
                
            case .WrongPhoneNumber:
                
                break
                
                
                
            case .MissingPasswordFBLogin://User Already Registered using normal password, so user as to make login using normal password
                
                
                
                break
                
                
                
            case .SuccessResponse:
                

                
                if let userDataResponse = dataResponse as? [String:Any] {
                    
                    registerViewModel.saveCurrentUserDetails(dataResponse: userDataResponse)
                    
                    let cancelBookingView = BookingCancelledView.sharedInstance
                    cancelBookingView.changeTitle(message: "In case of life threatening emergency please dial 911.")
                    
                    
                    WINDOW_DELEGATE??.addSubview(cancelBookingView)
                    
                    cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                    
                    
                    UIView.animate(withDuration: 0.5,
                                   delay: 0.0,
                                   options: UIView.AnimationOptions.beginFromCurrentState,
                                   animations: {
                                    
                                    cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                                    
                    }) { (finished) in
                        
                    }
                    
                    ConfigManager.sharedInstance.getConfigurationDetails()
                    self.performSegue(withIdentifier: "toTermsandCondition", sender: self)
                }
                
                
                break
                
            }
            
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
            
            switch requestType {
                
            case RequestType.verifyReferralCode:
                
                //                referralCodeTextField.text = ""
                registerViewModel.referralCodeText.value = ""
                
            case RequestType.signIn:
                
                //                if isFBSignUp {
                //
                //                    emailTextField.text = ""
                //                    registerViewModel.emailText.value = ""
                //                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                //                        self.emailTextField.becomeFirstResponder()
                //                    })
                //
                //                }
                break
                
                
            default:
                break
            }
            
        }
        
        
    }
}

//
//  vaiDRx_createPassword_CustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_CreatePasswordVC {
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.nextButtonBottomConstraint.constant = ((keyboardSize?.height)! + 30.0)
                        self.view.layoutIfNeeded()
        })
    }
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.4) {
            
            self.nextButtonBottomConstraint.constant = 30
            self.view.layoutIfNeeded()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toGetName" {
            
            if let dstVC = segue.destination as? vaiDRx_GetNameVC {
                
                dstVC.registerViewModel = self.registerViewModel
            }
        } else if segue.identifier == "toDateOfBirthVC" {
            
            if let dstVC = segue.destination as? VRDateOfBirthViewController {
                
                dstVC.registerViewModel = self.registerViewModel
                print("******",registerViewModel.passwordText.value)
            }
        }
    }

    
}

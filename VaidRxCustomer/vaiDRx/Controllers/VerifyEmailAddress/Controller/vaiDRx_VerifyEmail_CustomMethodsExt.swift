//
//  vaiDRx_VerifyEmail_CustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_VerifyEmailVC {
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.nextButtonBottomConstraint.constant = ((keyboardSize?.height)! + 30.0)
                        self.view.layoutIfNeeded()
        })
    }
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.4) {
            
            self.nextButtonBottomConstraint.constant = 30
            self.view.layoutIfNeeded()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toCreatePassword" {
            
            if let dstVC = segue.destination as? vaiDRx_CreatePasswordVC{
                
                print("registerViewModel",registerViewModel.emailText.value)
                dstVC.registerViewModel = self.registerViewModel
                
            }
        }
    }
}

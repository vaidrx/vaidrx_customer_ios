//
//  CouchDBEvents.swift
//  Rogi
//
//  Created by NABEEL on 14/06/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CouchDBDocument: NSObject {

    static var obj:CouchDBDocument? = nil
    
    class func sharedInstance() -> CouchDBDocument {
        
        if obj == nil {
            
            obj = CouchDBDocument()
        }
        
        return obj!
    }

    
    /// Create couch DB Document
    ///
    /// - Parameters:
    ///   - database: database details
    ///   - withArray: initial data to add in document
    /// - Returns: created documnet object
    func createDocument(database:CBLDatabase, withArray:[String : AnyObject])->CBLDocument{
        
        let document: CBLDocument? = database.createDocument()
        let docID: String? = document?.documentID
        do{
            try document?.putProperties(withArray)
            print("Document created, ID = " + docID! + "\n")
        }
        catch{
            print("Document not created\n")
        }
        return document!
    }
    
    
    
    /// Get Document details
    ///
    /// - Parameters:
    ///   - database: database details
    ///   - documentId: Document Identification
    /// - Returns: document object details
    func getDocument(database:CBLDatabase, documentId: String)->CBLDocument{
        
        let document: CBLDocument? = database.document(withID: documentId)
        let docContent: [AnyHashable: Any]? = document?.properties
        print("Got document = " , docContent! , "\n")
        return document!
    }
    
    
    
    /// Update the document details
    ///
    /// - Parameters:
    ///   - database: database details
    ///   - documentId: Document Identification value
    ///   - documentArray: data to add in document
    func updateDocument(database: CBLDatabase, documentId: String, documentArray: [AnyObject]){
        
        if let getDocument: CBLDocument = database.document(withID: documentId) {
            
            var docContent:[String: Any] = getDocument.properties!
            docContent["Value"] = documentArray as [AnyObject]
            let newRev: CBLSavedRevision? = try! getDocument.putProperties(docContent as [String : Any])
            if newRev == nil {
                print("Document not updated\n")
            }
            print("Updated document = ",newRev?.properties! as Any,"\n")
        }
        
    }
    
    
    
    
    
    /// Delete the document details
    ///
    /// - Parameters:
    ///   - database: database details
    ///   - id: Document Identification value
    func deleteDocument(database:CBLDatabase, id:String){
        
        let document: CBLDocument? = database.document(withID: id)
        do{
            try document?.delete()
            print("Document deleted, status = ",document?.isDeleted as Any,"\n")
            return
        }
        catch{
            print("Document not deleted\n")
            return
        }
    }
    
}

//
//  LeftMenuModelClass.swift
//  Iserve
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class LeftMenuViewModel: NSObject {
    
    //Left menu titles
    var menus = [
                 "Past bookings",
                 "Payment method",
                 "FAQ",
                 "Share",
                 "Help center","Live Chat"]
    //        ["Search artists",
    //                 "My events",
    //                 "Payment method",
    //                 "Reviews",
    //                 "Your addresses",
    //                 "Support",
    //                 "Share",
    //                 "Help center",
    //                 "Live chat",
    //                 "LiveM"]
    
    //Left menu each title image names
    var menusImages = [
                       "menu_review_icon_off",
                       "menu_card_icon_off",
                       "menu_faq_icon_off",
                       "menu_share_icon_off",
                       "menu_help_center_icon_off","menu_review_icon_off"]
    //        ["menu_search_icon_off",
    //                       "menu_my_event_icon_off",
    //                       "menu_card_icon_off",
    //                       "menu_review_icon_off",
    //                       "menu_address_icon_off",
    //                       "menu_faq_icon_off",
    //                       "menu_share_icon_off",
    //                       "menu_help_center_icon_off",
    //                       "live_chat_icon_off",
    //                       "menu_m_logo_icon_off"]
    
    var count: Int {
        return menus.count
    }
}

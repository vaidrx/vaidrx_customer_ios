//
//  vaiDRx_PreferredPayment_CustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation


extension vaiDRx_PreferredPaymentVC {
    
    
    // MARK: - Segue Method -
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        if segue.identifier == "toPreferredInsurance" {
            //toYesOrNoSCreen
           if segue.identifier == "toPreferredInsurance" {
            if let dstVC = segue.destination as? vaiDRx_PreferredInsuranceVC {
                
                dstVC.registerViewModel = self.registerViewModel
                dstVC.isComing = self.isComing
                dstVC.delegate = self.delegate
                dstVC.profileResponseModel = self.profileResponseModel
                dstVC.profileViewModel = self.profileViewModel
            }
            
        }
        if segue.identifier == "toYesOrNoSCreen" {
            
            if let dstVC = segue.destination as? PharmacyServiceYesOrNo {
                
                dstVC.registerViewModel = self.registerViewModel
            }
            
        }
        if segue.identifier == "toAddCard" {
            
            if let dstVC = segue.destination as? vaiDRx_AddNewCardVC {
                
                dstVC.registerViewModel = self.registerViewModel
            }
            
        }
        
            if segue.identifier == "toInsuranceVC" {
                
                if let dstVC = segue.destination as? TypesInsuranceViewController {
                    dstVC.isComminfFrom = isComing
                    dstVC.registerViewModel = self.registerViewModel
                }
                
            }
        
        
        
 
    }
    
    
    func serviceRequestToUpdateProfile() {
        
        paymentViewModel.firstName = profileViewModel.firstNameText.value
        paymentViewModel.lastNameText = profileViewModel.lastNameText.value
        paymentViewModel.aboutMeText = profileViewModel.aboutMeText.value
        paymentViewModel.profilePicURL = profileViewModel.profilePicURL
        paymentViewModel.dobText = profileViewModel.dobText
        paymentViewModel.insurance = "Self Pay"
        paymentViewModel.paymentType = PaymentType(rawValue: 0)!
        paymentViewModel.updateProfileDetailsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.updateProfileData)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        
        switch statusCode
        {
            
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if let dataRes = dataResponse as? String {
                
                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                

                
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
                case .signOut:
                    
                    progressMessage = PROGRESS_MESSAGE.LogOut
                    
                case .getProfileData:
                    
                    progressMessage = PROGRESS_MESSAGE.Loading
                    
                case .updateProfileData:
                    
                    progressMessage = PROGRESS_MESSAGE.Saving
                    
                    
                default:
                    
                    break
                }

            }
            
            break
            
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.getProfileData:

                break
                
            case RequestType.updateProfileData :
                delegate?.paymentSelected(PaymentType.SelfPay, Insurance: nil)
                self.navigationController?.popViewController(animated: true)
                DDLogDebug("Sucess")
                
                
            case RequestType.signOut :
                Helper.logOutMethod()
                DDLogDebug("Sucess")
                
            default:
                
                break
            }
            
        default:
            
            if  errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                
            }
            
            
            break
        }
        
    }
    
}

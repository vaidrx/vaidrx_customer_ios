//
//  AddMyAddressViewController.swift
//  GoTasker
//
//  Created by 3Embed on 21/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class AddMyAddressViewController: UIViewController,AddMyAddressFromMapToModel {
    func addCorrectAddress(correctAddress: AddressModel,taggedAs:String) {
        print("address contailn model",correctAddress)
        
        registerViewModel.latitude = Double(correctAddress.latitude)
        registerViewModel.longitude = Double(correctAddress.longitude)
//        registerViewModel.address = correctAddress.addressLine1 + correctAddress.addressLine2
        
//        if correctAddress.addressLine2.contains(correctAddress.addressLine1){
//            registerViewModel.address = correctAddress.addressLine2
//        }
//        else{
//            registerViewModel.address = correctAddress.addressLine1 + correctAddress.addressLine2
//        }
        registerViewModel.address = correctAddress.addressLine1
        registerViewModel.city = correctAddress.city
        registerViewModel.state = correctAddress.state
        registerViewModel.country = correctAddress.zipcode
        registerViewModel.country = correctAddress.country
        
        print(correctAddress.addressLine1)
        print(correctAddress.addressLine2)
        self.lbAddress.text = correctAddress.addressLine1
        //correctAddress.addressLine1 + correctAddress.addressLine2
        print("got full address")
    }
    
    
    //Mark:- Delegate method to get address from map
    func addMyAddress(address: String) {
        addressString = address
        self.lbAddress.text = address
        //registerViewModel.address = address
        
        print("address in addmyaddress",address)
    }
    
    func addFullAddress(fulladdress: [String : Any]) {
        
        if fulladdress.count > 0 {
        registerViewModel.longitude = fulladdress["logitude"] as! Double
        registerViewModel.latitude = fulladdress["latitude"] as! Double
        registerViewModel.address = fulladdress["address"] as! String
        registerViewModel.taggedAs = fulladdress["taggedAs"] as! String
        registerViewModel.city = fulladdress["city"] as! String
        registerViewModel.state = fulladdress["state"] as! String
        registerViewModel.country = fulladdress["country"] as! String
        registerViewModel.pincode = fulladdress["pincode"] as! String
        registerViewModel.taggedAs = fulladdress["taggedAs"] as! String
        print("Full adress",fulladdress)
        } else {
            print("nil dictionary")
        }
    }
    

    @IBOutlet weak var lbAddress: UILabel!
    
    var addressString = ""
    var isComingFrom = ""
    var registerViewModel: RegisterViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkForAddress()
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
//let addMyAddressDelegate = AddMyAddressFromMapToModel?.self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }

    
    @IBAction func actionNextButton(_ sender: UIButton) {
        if lbAddress.text == ""{
            Helper.showAlert(head: "Enter your address", message: "")
            
        }
        else{
            if isComingFrom != "Profile"{
               performSegue(withIdentifier: "toVaidRxPreferedPayment", sender: self)
            }
            else{
                
            }
            
            
            //performSegue(withIdentifier: "toPaymentMethods", sender: self)
            
        }
    }
    func checkForAddress(){
        lbAddress.text = addressString
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toVaidRxPreferedPayment" {
            
            if let dstVC = segue.destination as? vaiDRx_PreferredPaymentVC {
                
                dstVC.registerViewModel = self.registerViewModel
            }
        }
    }
    
    
    
    
    @IBAction func actionAddressButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVc = storyboard.instantiateViewController(withIdentifier: "AddNewAddressVC") as! AddNewAddressViewController
        nextVc.addMyAddressDelegate = self
        nextVc.registerViewModel = self.registerViewModel
        self.navigationController?.pushViewController(nextVc, animated: false)
    }
    
}

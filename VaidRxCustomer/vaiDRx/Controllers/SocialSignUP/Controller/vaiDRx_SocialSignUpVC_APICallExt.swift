//
//  vaiDRx_SocialSignUpVC_APICallExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 26/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_SocialSignUpVC {
    
    //MARK - WebService Call -
    func sendRequestToValidatePhoneNumber() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        landingViewModel.countryCode = countryCodeLabel.text!
        landingViewModel.triggerValue = 1
        landingViewModel.validatePhoneNumberAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.verifyMobileNumber, userRegisterType: nil)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType, userRegisterType:RegisterType?) {
        
        if HTTPSResponseCodes.Register(rawValue: statusCode) != nil {
            
            let responseCodes : HTTPSResponseCodes.Register = HTTPSResponseCodes.Register(rawValue: statusCode)!
            
            switch responseCodes {
                
            case .WrongInputData://wrong data
                
                isPhoneNumberValid = true
                registerViewModel.firstNameText.value = UserDataModel.firstName
                registerViewModel.lastNameText.value = UserDataModel.lastName
                registerViewModel.emailText.value = UserDataModel.mailId
                registerViewModel.phoneNumberText.value = phoneNumberTextFeild.text!
                registerViewModel.countryCode = countryCodeLabel.text!
//                registerViewModel.registerType = self.registerType
                registerViewModel.facebookId = UserDataModel.userId
                
                
                
                if let dataRes = dataResponse as? [String:Any] {
                    
                    if let sid = dataRes["sid"] as? String {
                        
                        DDLogDebug("SID:\(sid)")
                        
                        let params:[String:Any] = [
                            USER_DEFAULTS.USER.COUNTRY_CODE:countryCodeLabel.text!,
                            USER_DEFAULTS.USER.MOBILE:phoneNumberTextFeild.text!,
                            SERVICE_RESPONSE.Sid :sid
                        ]
                        performSegue(withIdentifier: "toVerifyPhone", sender: params)
                        
                    }
                    
                }
                
                
            case .WrongPhoneNumber:
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                phoneNumberTextFeild.text = ""
                landingViewModel.phoneNumberText.value = ""
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.phoneNumberTextFeild.becomeFirstResponder()
                })
                
                //Do the Login in process here
                
            case .SuccessResponse:
                
                isPhoneNumberValid = true
//                performSegue(withIdentifier: "toSignIn", sender: self)
                
                Helper.showAlert(head: ALERTS.Message, message: "This phone number is already registered with us, please try signing up with a different number or try signing in with this number!")
                
                
            case .MissingPasswordFBLogin:
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                break
            }
            
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
        }
        
    }
    
}

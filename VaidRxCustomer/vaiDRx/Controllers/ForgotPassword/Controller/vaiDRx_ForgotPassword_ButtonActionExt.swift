//
//  vaiDRx_ForgotPassword_ButtonActionExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_ForgotPasswordVC {
    
    // MARK: - Custom Action Methods -
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
             self.view.endEditing(true)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
        backButtonMethod()
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
    
        if (phonenumberTF.text?.length)! > 0 || (emailTF.text?.length)! > 0 {
            
            if forgotPasswordWithEmail {
                
                validateAllFields()
                
            }else {
                sendRequestToForgotPasswordUsingEmail()
            }
        }
    }
    
    @IBAction func CountryCodeSelected(_ sender: UIButton) {
      
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    @IBAction func emailAction(_ sender: UIButton) {
        
        self.view.layoutIfNeeded()
        emailViewHeight.constant = nextBottomView.frame.size.width
        UIView.animate(withDuration: 1.4, animations: {
            self.view.layoutIfNeeded()
        })
        phonenumberTF.text = ""
        forgotPasswordViewModel.phoneNumberText.value = ""
        self.view.endEditing(true)
        forgotPasswordWithEmail = true
        emailButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).withAlphaComponent(1), for: .normal)
        phoneButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).withAlphaComponent(0.5), for: .normal)
        emailButtonBottomView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        phoneButtonBottomView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        nextButtonOutlet.setTitle("CONFIRM", for: .normal)
    }
    
    @IBAction func phoneAction(_ sender: UIButton) {
        self.view.layoutIfNeeded()
        emailViewHeight.constant = 0
        UIView.animate(withDuration: 1.4, animations: {
            self.view.layoutIfNeeded()
        })
        emailTF.text = ""
        forgotPasswordViewModel.emailText.value = ""
        self.view.endEditing(true)
        forgotPasswordWithEmail = false
        phoneButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).withAlphaComponent(1), for: .normal)
        emailButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).withAlphaComponent(0.5), for: .normal)
        phoneButtonBottomView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        emailButtonBottomView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        nextButtonOutlet.setTitle("NEXT", for: .normal)
    
    }
    
    func phoneAction()  {
        self.view.layoutIfNeeded()
        emailViewHeight.constant = 0
//        UIView.animate(withDuration: 1.4, animations: {
//            self.view.layoutIfNeeded()
//        })
        emailTF.text = ""
        forgotPasswordViewModel.emailText.value = ""
        self.view.endEditing(true)
        forgotPasswordWithEmail = false
//        phoneButton.setTitleColor(#colorLiteral(red: 0.4039215686, green: 0.6862745098, blue: 0.968627451, alpha: 1).withAlphaComponent(1), for: .normal)
//        emailButton.setTitleColor(#colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1).withAlphaComponent(0.5), for: .normal)
//        phoneButtonBottomView.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0.6862745098, blue: 0.968627451, alpha: 1)
//        emailButtonBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
        nextButtonOutlet.setTitle("NEXT", for: .normal)
    }
}

//
//  ProfileVCTextFieldDelegateExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 08/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

extension ProfileViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        activeField = textField
        
        switch textField {
            
            case nameTextField:
                firstNameBottomView.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0.6862745098, blue: 0.968627451, alpha: 1)//UIColor(red:0.12, green:0.48, blue:0.74, alpha:1.0)
                
            case lastNameTextField:
                lastNameBottomView.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0.6862745098, blue: 0.968627451, alpha: 1)
            
            case musicGenreTextField:
                musicGenreBottomView.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0.6862745098, blue: 0.968627451, alpha: 1)//UIColor(red:0.12, green:0.48, blue:0.74, alpha:1.0)
       
            default:
                break
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
            
            case nameTextField:
                firstNameBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)//UIColor(red:0.12, green:0.48, blue:0.74, alpha:1.0)
                
            case lastNameTextField:
                lastNameBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
            
            case musicGenreTextField:
                musicGenreBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)

                
            default:
                break
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            
            case nameTextField:
                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.lastNameTextField.becomeFirstResponder()
//                })
                return false
            
            case lastNameTextField:
            
                textField.resignFirstResponder()
            
            case musicGenreTextField:
            
                textField.resignFirstResponder()
            
            default:
                break
        }
        
        return true
    }
 
    
}

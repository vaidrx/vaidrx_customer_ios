//
//  vaiDRxLandingTextFeildDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

// MARK: - TextFeildDelegate -
extension vaiDRxLandingVCViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        mainView.backgroundColor = UIColor.white
        phoneView.backgroundColor = UIColor.white
        phoneNumberTextFeild.textColor = UIColor(hex: "05C1C9") //UIColor.black
        countryCodeLabel.textColor = UIColor(hex: "05C1C9") //.black
        titleLabel.textColor = UIColor(hex: "05C1C9") //.black
    
        //enterYourNameLabel.isHidden = false
        enterYourNameLabel.textColor = UIColor(hex: "05C1C9")
        if phoneNumberTextFeild.text?.count != 0{
            enterYourNameLabel.isHidden = true
        }
        else{
            enterYourNameLabel.isHidden = false
        }
        
        if (phoneNumberTextFeild.text?.length)! > 0 && isPhoneNumberValid == false {
            
            isPhoneNumberValid = true
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField.text?.count ?? 0 > 0 {
            checkFortheField()
        }
        return false
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        isPhoneNumberValid = false
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        var textFieldText = String()
        
        if range.length == 0 {//Entered New Character
            
            textFieldText = textField.text! + string
        }
        else {//Entered BackSpace
            
            textFieldText = String(textField.text!.dropLast())
        }
        
        isPhoneNumberValid = false
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        backButton.isHidden = false
        logoButton.isHidden = false
        mainView.backgroundColor = UIColor.clear
        phoneView.backgroundColor = UIColor.white
        phoneNumberTextFeild.textColor = UIColor.white
        countryCodeLabel.textColor = .white
        titleLabel.textColor = .white
        enterYourNameLabel.textColor = .white
        enterYourNameLabel.isHidden = true
        return true
        
    }
    
    
    func checkFieldsAreValid(textField:UITextField, textFieldText:String) -> Int {
        
        var phoneNumberText = phoneNumberTextFeild.text
        var validFieldCount:Int = 0

        phoneNumberText = textFieldText

        if (phoneNumberText?.length)! > 0 && isPhoneNumberValid {
            
            validFieldCount = validFieldCount + 1
        }

        return validFieldCount
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if phoneNumberTextFeild.text?.count != 0{
            enterYourNameLabel.isHidden = true
        }
        else{
            enterYourNameLabel.isHidden = false
        }
    }
    
}

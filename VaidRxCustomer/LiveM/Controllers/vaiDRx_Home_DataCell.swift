//
//  vaiDRx_Home_DataCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class vaiDRx_Home_DataCell: UITableViewCell {

    @IBOutlet weak var key: UILabel!
    
    @IBOutlet weak var value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

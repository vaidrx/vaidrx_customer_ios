//
//  CancelBookingViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class CancelBookingViewModel {
    
    var methodName = ""
    let disposebag = DisposeBag()
    var bookingId:Int64 = 0
    var reasonId = ""
    
    let rxCancelBookingAPI = CancelBookingAPI()
    
    
    
    /// Method to call get cancellation reasons service API
    ///
    /// - Parameter completion:Service API response completion block
    func getCancelBookingReasonsAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxCancelBookingAPI.getCancelBookingReasonsServiceAPICall()
        
        if !rxCancelBookingAPI.getCancelBookingReasons_Response.hasObservers {
            
            rxCancelBookingAPI.getCancelBookingReasons_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
    
    
    
    /// Nethod to call Cancel Booking service API
    ///
    /// - Parameter completion: Service API response completion block
    func cancelBookingAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxCancelBookingAPI.cancelBookingServiceAPICall(bookingId: bookingId, reasonId: reasonId)
        
        if !rxCancelBookingAPI.cancelBooking_Response.hasObservers {
            
            rxCancelBookingAPI.cancelBooking_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }

}


//
//  HomeVCButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension HomeScreenViewController {
    
    @IBAction func currentLocationAction(_ sender: Any) {
        
        showCurrentLocation()
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        let leftMenu = slideMenuController()
        leftMenu?.openLeft()
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func listAction(_ sender: Any) {
        
        
        if arrayOfProvidersModel.count > 0 {
            
            savePickupLocationDetails()
            
            let listVC:HomeListViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.homeListVC) as! HomeListViewController
            
            listVC.arrayOfProvidersModel = arrayOfProvidersModel
            
            TransitionAnimationWrapperClass.flipFromLeftAnimation(view: self.navigationController!.view)
            self.navigationController!.pushViewController(listVC, animated: false)
            
        } else {
            
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.HOME_SCREEN.MusicianMissing)
        }
        
    }
    
    @IBAction func datePickerAction(_ sender: Any) {
        
        datePickerView.date = selectedDate
        
        self.datePickerBackBottomConstraint.constant = 0
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
        
    }
    
    
    @IBAction func datePickerSaveButtonAction(_ sender: Any) {
        
        selectedDate = datePickerView.date
        
        self.selectedProviderId = ""
        
        appointmentLocationModel.bookingType = BookingType.Schedule
        appointmentLocationModel.scheduleDate = selectedDate
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        musiciansListManager.publish(isIinitial: false)
        
        updateSelectedBookLaterDetails()
        
        if self.selectedBookLaterBackView.isHidden == true {
            
            showSelectedBookLaterDetails()
        }
        
        self.datePickerBackBottomConstraint.constant = -600
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
        })
        
    }
    
    @IBAction func datePickerCancelButtonAction(_ sender: Any) {
        
        self.datePickerBackBottomConstraint.constant = -600
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
        })
        
    }
    
    @IBAction func selectedBookLaterCloseButtonAction(_ sender: Any) {
        
        selectedEventStartTag = 0
        selectedDate = scheduleDate
        
        self.selectedProviderId = ""
        
        appointmentLocationModel.bookingType = BookingType.Default
        appointmentLocationModel.selectedEventStartTag = selectedEventStartTag
        
        self.eventStartCollectionView.reloadData()
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        musiciansListManager.publish(isIinitial: false)
        
        hideSelectedBookLaterDetails()
    }

    

    
    @IBAction func searchLocationButton(_ sender: Any) {
        
        let searchLocationVC:SearchLocationViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.searchLocationVC) as! SearchLocationViewController
        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                  subType: kCATransitionFromTop,
//                                                                  for: (self.navigationController?.view)!,
//                                                                  timeDuration: 0.3)
//
        searchLocationVC.delegate = self
        
        self.navigationController?.pushViewController(searchLocationVC, animated: false)
    }
    
}

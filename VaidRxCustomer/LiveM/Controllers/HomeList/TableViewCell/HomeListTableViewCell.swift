//
//  HomeListTableViewCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 05/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
//import youtube_ios_player_helper

class HomeListTableViewCell: UITableViewCell {

    // MARK: - Outlets -
    // Image View

    @IBOutlet weak var topView: UIViewCustom!
    
    @IBOutlet weak var youtubePlayerBackView: UIView!
    
//    @IBOutlet var youtubePlayerView: YTPlayerView!
    
    @IBOutlet var defaultImageView: UIImageView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var bottomView: UIView!
    
    
    @IBOutlet weak var musicianImageView: UIImageView!
    @IBOutlet weak var statusImageView: UIImageView!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    // Array Of star Images
   
    
    // label
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var youtubeVideoURL:String!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  vaiDRx_PreferredInsuranceTBVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 08/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class vaiDRx_PreferredInsuranceTBVC: UITableViewController {
    
//    var model = PreferredInsurance()
    var registerViewModel: RegisterViewModel!
    var userRegisterType = RegisterType.Default
    var isComing = ""
    var delegate: PreferredPaymentDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0:
            return 7
        case 1:
            return 1
        default:
            return 1
            
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_PreferredPaymentTVCell.self), for: indexPath) as! vaiDRx_PreferredPaymentTVCell
            
            cell.paymentMethodLabelNew.text = " "//model.insurances[indexPath.row]
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 50
        default:
            return 70
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
            
        case 0:
            switch indexPath.row {
            case indexPath.row :
                let selectedCell = tableView.cellForRow(at: indexPath) as! vaiDRx_PreferredPaymentTVCell
                selectedCell.paymentMethodLabelNew?.textColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
                selectedCell.selectedCheckImageNew.image = UIImage(named: "uncheck_unselected")
                if isComing != "Profile" {
                    
                    registerViewModel.categoryId = " "//model.insurances[indexPath.row]
                    serviceRequestToMakeRegister()
                    
                } else {
                    
//                   delegate?.paymentSelected(PaymentType.Insurance, Insurance: model.insurances[indexPath.row])
                    
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
                    
                    for aViewController in viewControllers {
                        
                        if(aViewController is ProfileViewController){
                            self.navigationController!.popToViewController(aViewController, animated: true);
                        }
                    }
                }


            default:
                let selectedCell = tableView.cellForRow(at: indexPath) as! vaiDRx_PreferredPaymentTVCell
                selectedCell.paymentMethodLabelNew?.textColor = Helper.UIColorFromRGB(rgbValue: 0xDBDBDB)
                selectedCell.selectedCheckImageNew.image = UIImage(named: "check_unselected")
            }
            break
            
        case 1:
            self.navigationController?.popViewController(animated: true)
            break
        default:
            break
        }
        
        
    }

    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension vaiDRx_PreferredInsuranceTBVC {
    
    func serviceRequestToMakeRegister() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        
        registerViewModel.registerType = userRegisterType
        
        registerViewModel.registerAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.signUp, userRegisterType: self.userRegisterType)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType, userRegisterType:RegisterType?)
    {
        if HTTPSResponseCodes.Register(rawValue: statusCode) != nil {
            
            let responseCodes : HTTPSResponseCodes.Register = HTTPSResponseCodes.Register(rawValue: statusCode)!
            
            switch responseCodes {
                
            case .WrongInputData://wrong data
                
                break
                
                
            case .WrongPhoneNumber:
                
                break
                
                
                
            case .MissingPasswordFBLogin://User Already Registered using normal password, so user as to make login using normal password
                
                
                
                break
                
                
                
            case .SuccessResponse:
                
                if let userDataResponse = dataResponse as? [String:Any] {
                    
                    registerViewModel.saveCurrentUserDetails(dataResponse: userDataResponse)
                    
                    self.performSegue(withIdentifier: "toTermsandConditionNew", sender: self)
                }

                break
                
            }
            
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
            
            switch requestType {
                
            case RequestType.verifyReferralCode:
                
                registerViewModel.referralCodeText.value = ""
                
            case RequestType.signIn:

                break
                
                
            default:
                break
            }
            
        }
        
        
    }
    
    

}

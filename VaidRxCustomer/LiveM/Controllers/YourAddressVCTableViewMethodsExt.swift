//
//  YourAddressVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension YourAddressViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrayOfAddress.count > 0 {
            
            messageLabel.isHidden = true
            return arrayOfAddress.count
        }
        
        messageLabel.isHidden = false
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let addresscell:YourAddressTableViewCell = tableView.dequeueReusableCell(withIdentifier: "YourAddressCell", for: indexPath) as! YourAddressTableViewCell
        
        let addressModel = arrayOfAddress[indexPath.row]
        
        addresscell.deleteButton.tag = indexPath.row
        addresscell.deleteButton.addTarget(self, action: #selector(deleteAddressButtonAction), for: .touchUpInside)
        
        switch addressModel.tagAddress {
            
        case "home":
            
            addresscell.addressImage.image = #imageLiteral(resourceName: "your_address_home_icon")
            addresscell.addressTypeLabel.text = "Home"
            
        case "office":
            
            addresscell.addressImage.image = #imageLiteral(resourceName: "your_address_briefcase_icon")
            addresscell.addressTypeLabel.text = "Office"
            
        default:
            
            addresscell.addressImage.image = #imageLiteral(resourceName: "your_address_heart_icon")
            addresscell.addressTypeLabel.text = addressModel.tagAddress
            
        }
        
        addresscell.fullAddressLabel.text = addressModel.addressLine1
        
        return addresscell
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH! - 90, height: 0))
        label.font = UIFont.init(name: FONTS.HindRegular, size: 12)
        
        let addressModel = arrayOfAddress[indexPath.row]
        
        label.text = addressModel.addressLine1
        
        
        return Helper.measureHeightLabel(label, width: SCREEN_WIDTH! - 145) + 55
        
    }
    
    
}

extension YourAddressViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if isFromConfirmBookingVC {
            
            //Save Selected Address details in Confirm Booking Model
            let CBModel = ConfirmBookingModel.sharedInstance()
            
            let appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
            
            let addressModel = arrayOfAddress[indexPath.row]
            
            appointmentLocationModel.pickupAddress = addressModel.addressLine1
            appointmentLocationModel.pickupLatitude = Double(addressModel.latitude)
            appointmentLocationModel.pickupLongitude = Double(addressModel.longitude)
            
            
            CBModel.appointmentLocationModel = appointmentLocationModel
            
            self.navigationController?.popViewController(animated: true)
            
        } else {
            
            //Go to Edit Address VC
            let editAddressVC:AddNewAddressViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.addNewAddressVC) as! AddNewAddressViewController
            
            editAddressVC.isForEditing = true
            editAddressVC.isAddressForEditing  = true
            editAddressVC.isFromConfirmBookingVC = isFromConfirmBookingVC
            
            editAddressVC.addressDetails = arrayOfAddress[indexPath.row]
            
            self.navigationController!.pushViewController(editAddressVC, animated: false)
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            print("Delete button tapped")
            self.deleteAddressButtonAction(index:index.row)
            
            //            addresscell.deleteButton.addTarget(self, action: #selector(deleteAddressButtonAction), for: .touchUpInside)
            
        }
        
        delete.backgroundColor = RED_COLOR
        
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        
    }
    
    
    
}

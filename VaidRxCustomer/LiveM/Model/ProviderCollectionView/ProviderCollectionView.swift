//
//  ProviderCollectionView.swift
//  LiveM
//
//  Created by Rahul Sharma on 27/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
//import youtube_ios_player_helper

class ProviderCollectionView:UIView {
    
    var topView:UIView!//blurr view
    var nameLabel:UILabel!//musician name label
    var milesLabel:UILabel!//musician miles label
    var blurrView:UIView!//blurr view
//    var youtubePlayerView:YTPlayerView!//youtube player view
    var defaultImageView:UIImageView!//default imageview will show when there is no youtube link
    var actvityIndicator:UIActivityIndicatorView!//activity indicator view will show while initial loading of youtube video
    var musicianId:String!
    var youtubeVideoURL:String!
    
    init(isForVertical:Bool, viewController:UIViewController) {
        
        if isForVertical {
            
            super.init(frame: CGRect(x: 10, y: 0, width: SCREEN_WIDTH! - 20 , height: SCREEN_HEIGHT! * 0.25))
            
        } else {
            
            super.init(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH! * 0.65, height: SCREEN_HEIGHT! * 0.25))
        }
        
        topView = UIView(frame: CGRect(x: 5, y: 5, width: self.frame.size.width - 10, height: self.frame.size.height - 10))
        
        topView.layer.cornerRadius = 5
        topView.clipsToBounds = true

        self.addSubview(topView)
        
        Helper.setShadowFor(topView, andWidth: self.frame.size.width - 10, andHeight: self.frame.size.height - 10)
        
        defaultImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: topView.frame.size.width, height: topView.frame.size.height))
        
        defaultImageView.image = #imageLiteral(resourceName: "default_youtube_player_screen")
        
        topView.addSubview(defaultImageView)
        
        
        actvityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: topView.frame.size.width, height: topView.frame.size.height))
        
        actvityIndicator.style = UIActivityIndicatorView.Style.gray
        actvityIndicator.hidesWhenStopped = true
        
        
        topView.addSubview(actvityIndicator)
        
        
//        youtubePlayerView = YTPlayerView.init(frame: topView.frame)
//        
//        youtubePlayerView.layer.cornerRadius = 5
//        youtubePlayerView.clipsToBounds = true
//        
//        youtubePlayerView.layer.cornerRadius = 5
//        youtubePlayerView.clipsToBounds = true
//
//        youtubePlayerView.webView?.layer.cornerRadius = 5
//        youtubePlayerView.webView?.clipsToBounds = true
//        
//        topView.addSubview(youtubePlayerView)
        
        
        blurrView = UIView.init(frame: topView.frame)
        blurrView.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        blurrView.alpha = 0.4
        
        blurrView.isUserInteractionEnabled = false
        
        topView.addSubview(blurrView)
        
        
        nameLabel = UILabel(frame: CGRect(x: 10, y: topView.center.y - 25, width: topView.frame.size.width - 10, height:20))
        nameLabel.backgroundColor = .clear
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont.init(name: FONTS.HindMedium, size: 14)
        nameLabel.textColor = UIColor.lightGray
        
        
        topView.addSubview(nameLabel)
        
        
        milesLabel = UILabel(frame: CGRect(x: 10, y: topView.center.y, width: topView.frame.size.width - 10, height:20))
        milesLabel.backgroundColor = .clear
        milesLabel.textAlignment = .center
        milesLabel.font = UIFont.init(name: FONTS.HindRegular, size: 12)
        milesLabel.textColor = UIColor.lightGray
        
        topView.addSubview(milesLabel)
        
        topView.bringSubviewToFront(nameLabel)
        topView.bringSubviewToFront(milesLabel)
        
        
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}


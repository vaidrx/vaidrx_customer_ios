//
//  vaiDRx_SocialLogin_TableViewExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import FacebookLogin
import GoogleSignIn

extension vaiDRx_SocialLoginVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_SocialLoginTableViewCell.self), for: indexPath) as! vaiDRx_SocialLoginTableViewCell
        
        switch indexPath.row {
        case 0:
            
            cell.socialLoginIcon.image = #imageLiteral(resourceName: "social_media_facebook_icon")
            cell.socialLoginTitle.text = "FACEBOOK"
            break
            
        case 1:
            
            cell.socialLoginTitle.text = "GOOGLE"
            cell.socialLoginIcon.image = #imageLiteral(resourceName: "social_media_google_icon")
            cell.bottomView.isHidden = true
            break
            
        default:
            break
        }
        return cell
    }
}

extension vaiDRx_SocialLoginVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            //facebook login action
            registerViewModel.registerType = RegisterType.Facebook
            let fbLoginManager:LoginManager = LoginManager.init()
            fbLoginManager.logOut()
//            isFBLogin = true
            let fbLoginHandler = FBLoginHandler.sharedInstance()
            fbLoginHandler.delegate = self
            fbLoginHandler.login(withFacebook: self)
            break
            
        case 1:
            //Google Login
            registerViewModel.registerType = RegisterType.Google
         //   GIDSignIn.sharedInstance().delegate = self
            GmailLoginHandler.sharedInstance().login(withGmail: self)
            break
            
        default:
            break
        }
    }
}


//
//  vaiDRx_Home_DoctorDetail.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class vaiDRx_Home_DoctorDetail: UITableViewCell {

    @IBOutlet weak var docImage: UIImageView!
    
    @IBOutlet weak var docName: UILabel!
    
    @IBOutlet weak var clinicAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

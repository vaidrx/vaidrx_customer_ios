//
//  vaiDRx_BookingVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 08/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import UICircularProgressRing
import Kingfisher


class vaiDRx_BookingVC: UIViewController {

    @IBOutlet weak var ring: UICircularProgressRingView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var connectingLbl: UILabel!
    @IBOutlet weak var timer: UILabel!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var doctorPic: UIImageView!
    @IBOutlet weak var timerView: UIView!
    
    var timerProgress = Timer()
    var countDown:Int = 60
    let nf = NumberFormatter()
    var pastProviderResponseModel: PastProviderResponseModel? = nil
    var selectedIndex = 0
    var isComingFrom = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        
        timerView.layer.cornerRadius = timerView.frame.height / 2
        
        
        if pastProviderResponseModel != nil {
            
            messageLabel.text = "Please wait for \(countDown) seconds while we assign \(((pastProviderResponseModel?.allProviders[selectedIndex])! [SERVICE_RESPONSE.Provider_FName]) as! String) to you."

            doctorPic.layer.borderWidth = 1.0
            doctorPic.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x83D788).cgColor
            
            if !((((pastProviderResponseModel?.allProviders[selectedIndex])! [SERVICE_RESPONSE.Provider_Pic]) as! String).isEmpty){
                
                doctorPic.kf.setImage(with: URL(string: ((self.pastProviderResponseModel?.allProviders[selectedIndex])! [SERVICE_RESPONSE.Provider_Pic]) as! String),
                                                    placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                                    options: [.transition(ImageTransition.fade(1))],
                                                    progressBlock: { receivedSize, totalSize in
                },
                                                    completionHandler: nil)
                
                
            }else {
                
                doctorPic.image = #imageLiteral(resourceName: "myevent_profile_default_image")
            }
            
            connectingLbl.text = "Connecting to \(((pastProviderResponseModel?.allProviders[selectedIndex])! [SERVICE_RESPONSE.Provider_FName]) ?? "")" + " \(((pastProviderResponseModel?.allProviders[selectedIndex])! [SERVICE_RESPONSE.Provider_LName]) ?? "")"
            doctorPic.isHidden = false
            
        } else {
            
            messageLabel.text = "Please wait for \(countDown) seconds while we assign a Qualified Vaid to you."
            connectingLbl.text = "Connecting to a Vaid"
            doctorPic.isHidden = true
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveBookingAcception),
                                               name: NSNotification.Name(rawValue: "gotNewBooking"),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveBookingAcception), name: NSNotification.Name(rawValue: "canceledBooking"), object: nil)
        
        
        if isComingFrom == "VaidNow"{
            titleLabel.text = "Vaid Now"
            titleImage.image = UIImage(named: "vaid_now")
        }
        if isComingFrom == "VaidToday"{
            titleLabel.text = "Vaid Today"
            titleImage.image = UIImage(named: "vaid_today")
        }
        
//        let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
//        statusBar?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.ring.viewStyle = 3
//
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        timerProgress =  Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
        nf.numberStyle = NumberFormatter.Style.none
        nf.maximumFractionDigits = 2
        
        ring.setProgress(value: 0, animationDuration: 5) { [unowned self] in
            // Increase it more, and customize some properties
            self.ring.viewStyle = 3
            self.ring.setProgress(value: 100, animationDuration: TimeInterval(self.countDown)) {
                self.ring.font = UIFont.systemFont(ofSize: 50)
                print("Ring finished")
            }
        }
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "dismissPast"), object: self)
        NotificationCenter.default.removeObserver(self)
        
//        let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
//        statusBar?.backgroundColor = .clear
    }
    
    @objc func didReceiveBookingAcception() {
        
        self.dismiss(animated: true, completion: nil)
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionReveal, subType: kCATransitionFromBottom, for: (self.navigationController?.view)!, timeDuration: 0.35)
//        self.navigationController?.popViewController(animated: false)
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "gotNewBooking"), object: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func timerTick(){
//        if let count = UserDefaults.standard.value(forKey: "RUNNINGTIMER") {
//                   countDown = count as! Int
//               }
        var minutes: Int = 00
        var seconds: Int = 00
        if countDown > 60 {
            minutes = countDown / 60
            seconds = countDown - (minutes * 60)
        } else {
            seconds = countDown - (minutes * 60)
        }
        countDown = countDown - 1;
        UserDefaults.standard.set(countDown, forKey: "RUNNINGTIMER")
       
        if seconds <= 0 {
            seconds = 60
        }
        seconds = seconds - 1;
        if seconds < 10 {
            self.timer.text = "0\(minutes) : 0\(seconds)"
        } else {
            self.timer.text = "0\(minutes) : \(seconds)"
        }
        
        if countDown <= 0 {
            timerProgress.invalidate()
            UserDefaults.standard.removeObject(forKey: "RUNNINGTIMER")
            appointmentAvail = ""
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    

}

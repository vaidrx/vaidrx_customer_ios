//
//  PastProviderViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class PastProviderViewModel {
    
    let disposebag = DisposeBag()
    
    let rxPastProviderAPI = PastAPI()
    
    var CBModel:ConfirmBookingModel!

    let rxConfirmBookingAPI = ConfirmBookingAPI()
    
    func getPastProviderAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxPastProviderAPI.getPastProvidersServiceAPICall()
        
        if !rxPastProviderAPI.getPastProvider_Response.hasObservers {
            
            rxPastProviderAPI.getPastProvider_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    
    func liveBookingAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxConfirmBookingAPI.liveBookingServiceAPICall(confirmBookingModel: CBModel)
        
        if !rxConfirmBookingAPI.liveBooking_Response.hasObservers {
            
            rxConfirmBookingAPI.liveBooking_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
        }
    }
    
    
}

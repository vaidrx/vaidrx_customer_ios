//
//  CancelBookingAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class CancelBookingAPI {
    
    let disposebag = DisposeBag()
    let getCancelBookingReasons_Response = PublishSubject<APIResponseModel>()
    let cancelBooking_Response = PublishSubject<APIResponseModel>()
    
    
    
    /// Method to call get cancel booking reasons Service API
    func getCancelBookingReasonsServiceAPICall() {
        
        let strURL = API.BASE_URL + API.METHOD.CANCEL_REASONS
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.getCancelReasons)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to call cancel booking service API
    ///
    /// - Parameters:
    ///   - bookingId: booking id to cancel
    ///   - reasonId: customer selected reason id
    func cancelBookingServiceAPICall(bookingId:Int64,
                                    reasonId:String) {
        
        let strURL = API.BASE_URL + API.METHOD.CANCELBOOKING
        
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.CANCEL_BOOKING.bookingId: bookingId,
            SERVICE_REQUEST.CANCEL_BOOKING.reasonId: reasonId,
        ]
        
        Helper.showPI(_message: PROGRESS_MESSAGE.CancelBooking)
        
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.cancelBooking)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
            case .getCancelReasons:
                
                self.getCancelBookingReasons_Response.onNext(responseModel)
                
            case .cancelBooking:
                
                self.cancelBooking_Response.onNext(responseModel)
                
            default:
                break
            }
            
            break
        }
    }
    
}

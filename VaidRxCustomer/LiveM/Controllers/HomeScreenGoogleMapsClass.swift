//
//  HomeScreenGoogleMapsClass.swift
//  LiveM
//
//  Created by Apple on 24/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

extension HomeScreenViewController:GMSMapViewDelegate {
    
    /// Set Intial MapView Properties
//    func setInitialMapViewProperties() {
//        
//        currentLatitude = locationObj.latitute
//        currentLatitude = locationObj.longitude
//        
//        pickupLatitude = locationObj.latitute
//        pickupLongitude = locationObj.longitude
//        
//        addressLabel.text = locationObj.address
//        
//        self.mapView.isMyLocationEnabled = true
//        
//        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(currentLatitude),
//                                              longitude: CLLocationDegrees(currentLongitude),
//                                              zoom: MAP_ZOOM_LEVEL)
//        self.mapView.animate(to: camera)
//        
//    }
    
    
    /// Method to show current location in MapView
    @objc func showCurrentLocation() {
        
        let location = self.mapView.myLocation
        
        currentLatitude = (location?.coordinate.latitude)!
        currentLongitude = (location?.coordinate.longitude)!
        
        if (location != nil) {
            
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(currentLatitude),
                                                  longitude: CLLocationDegrees(currentLongitude),
                                                  zoom: MAP_ZOOM_LEVEL)
            self.mapView.animate(to: camera)

        }

    }
    
    
    /// Get Location Details From Lat & Long
    ///
    /// - Parameter coordinate: current map position lat & Long
    func getLocationDetails(_ coordinate: CLLocationCoordinate2D) {
        
        pickupLatitude = coordinate.latitude
        pickupLongitude =  coordinate.longitude
        
        let geoCoder = CLGeocoder()
        let location = CLLocation.init(latitude: pickupLatitude, longitude: pickupLongitude)
        
        geoCoder.reverseGeocodeLocation(location) {
            
            (placemarks, error) -> Void in
            
            if let arrayOfPlaces: [CLPlacemark] = placemarks as [CLPlacemark]! {
                
                // Place details
                if let placemark: CLPlacemark = arrayOfPlaces.first {
                    
                    // Address dictionary
                    print("Address Dict :\(placemark.addressDictionary!)")
                    
                    // Current Address
                    if let currentAddress = placemark.addressDictionary?["FormattedAddressLines"] as? String {
                        
                        print("Current Address: \(currentAddress)")
                        self.addressLabel.text = currentAddress 
                    }
                    
                }
            }
        }

    }
    
    
    // MARK: - GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        if isAddressManuallyPicked {//Checking Address Picked Manually (Get Address From Search Address Controller)
            
            isAddressManuallyPicked = false
        } else {
            
            getLocationDetails(position.target)
        }
    }

}

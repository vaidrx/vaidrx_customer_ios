//
//  MusiciansListAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class MusiciansListAPI {
    
    let disposebag = DisposeBag()
    let musiciansList_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call Get Initial musician's List service API (Home & List screen)
    ///
    /// - Parameters:
    ///   - pickupLat: user selected address's latitude
    ///   - pickupLong: user selected address's longitude
    func musiciansListGETServiceAPICall(pickupLat:Double,
                                         pickupLong:Double) {
        
        let strURL = API.BASE_URL + "\(API.METHOD.GET_ALL_MUSICIANS)/\(pickupLat)/\(pickupLong)/\(Utility.iPAddress)"
        
        DDLogVerbose("Get Location URL:- \(strURL)")

        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
                
                
            }, onError: {  (error) in
                
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
//                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    
    /// Method to call Get musician's List service API (Home & List screen)
    ///
    /// - Parameter appointmentLocationModel: model contains user selected appointment location details
    func musiciansListPOSTServiceAPICall(appointmentLocationModel:AppoimtmentLocationModel) {
        
        let strURL = API.BASE_URL + API.METHOD.ALL_PROVIDERS_POST
        
        var requestParams = [
            
            SERVICE_REQUEST.GetMusician.latitude:appointmentLocationModel.pickupLatitude,
            SERVICE_REQUEST.GetMusician.longitude:appointmentLocationModel.pickupLongitude,
            SERVICE_REQUEST.GetMusician.bookingType:appointmentLocationModel.bookingType.rawValue
            
        ] as [String : Any]
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            let dateformat = DateFormatter()
            dateformat.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            requestParams["scheduleDate"] = dateformat.string(from: appointmentLocationModel.scheduleDate)
            requestParams["scheduleTime"] = "\((appointmentLocationModel.selectedEventStartTag + 1) * 30)"
            requestParams["deviceTime"] = TimeFormats.currentDateTime
        }
        
        DDLogVerbose("Post Location Params:- \(requestParams)")
        
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
                
                
            }, onError: {  (error) in
                
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    func checkResponse(statusCode:Int, responseDict: [String:Any]){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.hidePI()
//            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.hidePI()
//            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.musiciansList_Response.onNext(responseModel)
            break
        }
    }
    
}

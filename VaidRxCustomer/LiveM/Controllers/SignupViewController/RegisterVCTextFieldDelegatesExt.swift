//
//  RegisterVCTextFieldDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension RegisterViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        pickerButtomConstrain.constant = -500
        activeField = textField
        
        switch textField {
            
        case firstNameTextField:
            firstNameView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)//UIColor(red:0.12, green:0.48, blue:0.74, alpha:1.0)
            
        case lastNameTextField:
            firstNameView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            
        case emailTextField:
            emailView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)// BABABA - disable 3B5998 - Enabled
            
        case passwordTextField:
            passwordView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            
        case phoneNumerTextField:
            phoneNumberView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            
        case yourBirthdayTextField:
            
            showDobDatePickerView()
            yourBirthdayView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
            return false
            
        case referralCodeTextField:
            refferalCodeView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            
        default:
            break
        }
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if backButtonActive {
            
            return
        }
        
        switch textField {
            
            case firstNameTextField:
                firstNameView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)//UIColor(red:0.73, green:0.73, blue:0.73, alpha:1.0)
                
            case lastNameTextField:
                firstNameView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
                
            case phoneNumerTextField:
                
                if !backButtonActive {
                    
                    phoneNumberView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
                    if (phoneNumerTextField.text?.length)! > 0 && isPhoneNumberValid == false {
                    
                        sendRequestToValidatePhoneNumber()
                    }
                }
                
            case emailTextField:
                emailView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
                if !Helper.isValidEmail(testStr: emailTextField.text!) {
                    
                    if !backButtonActive {
                        
                        if (emailTextField.text?.length)! > 0{
                            
                            Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailInvalid)
//                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                                self.emailTextField.becomeFirstResponder()
//                            })

                        }
                    }
                    
                }else {
                    if !fbLogin && !backButtonActive && isEmailValid == false {
                        
                        sendRequestToValidateEmailAddress()
                    }
                }
                
            case passwordTextField:
                passwordView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
                if !Helper.isValidPassword(password: passwordTextField.text!) {
                    if !backButtonActive {
                        
                        if (passwordTextField.text?.length)! > 0{
                            
                            passwordTextField.text = ""
                            registerViewModel.passwordText.value = ""
                            
                            Helper.alertVC(title: ALERTS.Message, message: ALERTS.PasswordInvalid)
//                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                                self.passwordTextField.becomeFirstResponder()
//                            })
                            
                        }
                        
                    }
                }
                
                
            case yourBirthdayTextField:
                yourBirthdayView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
                
            case referralCodeTextField:
                refferalCodeView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
                
                if (referralCodeTextField.text?.length)! > 0 && isReferralCodeValid == false {
                    
                    sendRequestToValidateReferralCode()
                }
                
                
            default:
                break
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            
            case firstNameTextField:
                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.lastNameTextField.becomeFirstResponder()
                    return false
//                })
            

            case lastNameTextField:

//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.emailTextField.becomeFirstResponder()
                    return false
//                })
            
            case emailTextField:
                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.passwordTextField.becomeFirstResponder()
                    return false
//                })
            
                
            case passwordTextField:
                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.phoneNumerTextField.becomeFirstResponder()
                    return false
//                })
            
                
            case phoneNumerTextField:
                yourBirthdayTextField.becomeFirstResponder()
                return false
                

            case referralCodeTextField:
                self.view.endEditing(true)
                return false
            
            default:
                break
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        switch textField {
            
            case emailTextField:
                isEmailValid = false
                
            case phoneNumerTextField:
                isPhoneNumberValid = false
                
            case referralCodeTextField:
                isReferralCodeValid = false
                
            default:
                break
            
        }
        
        animateButtonLoading(checkFieldsAreValid(textField: textField, textFieldText: ""))
        
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var textFieldText = String()
        
        if range.length == 0 {//Entered New Character
            
            textFieldText = textField.text! + string
        }
        else {//Entered BackSpace
            
            textFieldText = String(textField.text!.dropLast())
        }
        
        switch textField {
            
            case emailTextField:
                isEmailValid = false
            
            case phoneNumerTextField:
                isPhoneNumberValid = false
                
            case referralCodeTextField:
                isReferralCodeValid = false
                
            default:
                break
        }
        
        animateButtonLoading(checkFieldsAreValid(textField: textField, textFieldText: textFieldText))
        
        return true
    }
    
    
    func checkFieldsAreValid(textField:UITextField, textFieldText:String) -> Int {
        
        var firstNameText = firstNameTextField.text
        var lastNameText = lastNameTextField.text
        var emailText = emailTextField.text
        var phoneNumberText = phoneNumerTextField.text
        var passwordText = passwordTextField.text
        var validFieldCount:Int = 0
        
        switch textField {
            
            case firstNameTextField:
                firstNameText = textFieldText
                
            case lastNameTextField:
                lastNameText = textFieldText
                
            case emailTextField:
                emailText = textFieldText
                
            case phoneNumerTextField:
                phoneNumberText = textFieldText
                
            case passwordTextField:
                passwordText = textFieldText
                
            default:
                break
        }
        
        if (firstNameText?.length)! > 0 {
            
            validFieldCount = validFieldCount + 1
        }
        
        if (lastNameText?.length)! > 0 {
            
            validFieldCount = validFieldCount + 1
        }
        
        if Helper.isValidEmail(testStr: emailText!) && isEmailValid {
            
            validFieldCount = validFieldCount + 1
        }
        
        if (phoneNumberText?.length)! > 0 && isPhoneNumberValid {
            
            validFieldCount = validFieldCount + 1
        }
        
        if Helper.isValidPassword(password: passwordText!) {
            
            validFieldCount = validFieldCount + 1
        }
        
        if (yourBirthdayTextField.text?.length)! > 0 {
            
            validFieldCount = validFieldCount + 1
        }
        
        return validFieldCount
        
    }
    
    
    
}

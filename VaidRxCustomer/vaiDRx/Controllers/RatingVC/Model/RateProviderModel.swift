//
//  RateProviderModel.swift
//  LSP
//
//  Created by Rajan Singh on 29/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class RateProviderModel: NSObject {

    var bookingID    : Int64!
    var rating        : Float!
    var review        : String!
    
    var requestType: RequestType? = nil
    var httpStatusCode:Int = 0
    
    func initializeData(bookingID: Int64, rating: Float, review: String) {
        self.bookingID = bookingID
        self.rating = rating
        self.review = review
        self.requestType = RequestType.providerRating
    }
    
    func updateBookingID(_ bookingID: Int64) {
        self.bookingID = bookingID
    }
    
    func updateRating(_ rating: Float) {
        self.rating = rating
        self.review = ""
    }
    
    func updateReview(_ review: String) {
        self.review = review
    }
    
    func updateHttpStatusCode(_ httpStatusCode: Int) {
        requestType = RequestType.providerRating
        self.httpStatusCode = httpStatusCode
    }
    
    
}

//
//  vaiDRx_ForgotPassword_TextFeildDelegateExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_ForgotPasswordVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        phoneNumView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)//UIColor.init(hex: "#3B5998")
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField.text?.length)! > 5 {
            animateButtonLoading(1)
        }else {
            animateButtonLoading(0)
        }
        phoneNumView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1) //UIColor.init(hex: "#BABABA")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        phoneNumView.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0.6862745098, blue: 0.968627451, alpha: 1)
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.length)! > 5 {
            animateButtonLoading(1)
        }else {
            animateButtonLoading(0)
        }
        return true
    }
}

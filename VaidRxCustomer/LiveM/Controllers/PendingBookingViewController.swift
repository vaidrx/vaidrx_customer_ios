//
//  PendingBookingViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import UICircularProgressRing


class PendingBookingViewController:UIViewController {
    
    
    @IBOutlet var navigationLeftButton: UIButton!
    
    @IBOutlet var cancelButton: UIButton!
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var headerView: UIView!
    
    @IBOutlet var mapView: GMSMapView!
    
    @IBOutlet var providerDetailsBackView: UIView!
    
    @IBOutlet var addressBackView: UIView!
    
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var milesLabel: UILabel!
    
    @IBOutlet var providerImageView: UIImageView!
    
    @IBOutlet var providerNameLabel: UILabel!
    
    @IBOutlet var remainingTimeView: UICircularProgressRingView!
    
    @IBOutlet var remainingTimeLabel: UILabel!
    
    @IBOutlet var ratingView: FloatRatingView!
    
    @IBOutlet weak var topViewTopConstraint: NSLayoutConstraint!
    
    
    var bookingEventModel:BookingDetailsModel!
        
    var arrayOfData:[String] = []
    

    var appointmentLocMarker:GMSMarker!
    var providerMarker:GMSMarker!

    var timer = Timer()
    var intervalProgress: Double!
    var totalDuration: Double = 0.0
    var remainingTimeValue: Int = 0
    
    var cancelBookingScreen:CancelBookingScreen!
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    
    // Shared instance object for gettting the singleton object
    static var obj:PendingBookingViewController? = nil
    
    class func sharedInstance() -> PendingBookingViewController {
        
        return obj!
    }

    
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PendingBookingViewController.obj = self
        
        if Helper.SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(version: "11.0") {
            
            self.topViewTopConstraint.constant = -NavigationBarHeight
            self.topView.layoutIfNeeded()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setInitialData()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        setupGestureRecognizer()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if timer.isValid {
            
            timer.invalidate()
        }
        
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.view?.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        if MyEventViewController.obj != nil {
            
            MyEventViewController.sharedInstance().gotoPastBooking = false
            MyEventViewController.sharedInstance().gotoPendingBooking = true
            MyEventViewController.sharedInstance().gotoUpcomingBooking = false
        }
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        cancelBookingScreen = CancelBookingScreen.sharedInstance
        
        cancelBookingScreen.bookingID = bookingEventModel.bookingId
        
        WINDOW_DELEGATE??.addSubview(cancelBookingScreen)
        
        cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { (finished) in
            
            self.cancelBookingScreen.getCancelReasonsAPI()
            
        }
        
    }
    
}

extension PendingBookingViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension PendingBookingViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            
            if MyEventViewController.obj != nil {
                
                MyEventViewController.sharedInstance().gotoPastBooking = false
                MyEventViewController.sharedInstance().gotoPendingBooking = true
                MyEventViewController.sharedInstance().gotoUpcomingBooking = false
            }
            
            self.navigationController?.popViewController(animated: true)
//            self.navigationLeftButtonAction(self.navigationLeftButton)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
}
    



extension PendingBookingViewController {
    
    func setRemainingTimeProgress() {
        
        intervalProgress = 100.0 / totalDuration
        
        scheduledTimerWithTimeInterval()
        
        self.showRemainingTimeDetails()
        
        remainingTimeView.setProgress(value: CGFloat(intervalProgress * Double(remainingTimeValue)), animationDuration: 0) {
            
        }
    }
    
    
    
    
    func scheduledTimerWithTimeInterval(){
        
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateRemainingTimeView), userInfo: nil, repeats: true)
    }
    
    @objc func updateRemainingTimeView(){
        
        remainingTimeValue -= 1
        
        if remainingTimeValue > 0 {
            
            remainingTimeView.setProgress(value:CGFloat(intervalProgress * Double(remainingTimeValue)), animationDuration: 0, completion: {
                self.remainingTimeView.viewStyle = 3
                self.showRemainingTimeDetails()
            })
            
            
        } else {
            
            timer.invalidate()
            
            remainingTimeView.setProgress(value: 0, animationDuration: 0) {
                
                self.showRemainingTimeDetails()
            }
            
            if MyEventViewController.obj != nil {
                
                MyEventViewController.sharedInstance().gotoPastBooking = true
                MyEventViewController.sharedInstance().gotoPendingBooking = false
                MyEventViewController.sharedInstance().gotoUpcomingBooking = false
            }
            
            self.navigationLeftButtonAction(self.navigationLeftButton)
            
        }
        
        
    }
    
    func showRemainingTimeDetails() {
        
        let minute:Int = remainingTimeValue/60
        let seconds:Int = remainingTimeValue - (minute * 60)
        
        var minuteString = ""
        var secondsString = ""
        
        if minute < 10 {
            
            minuteString = "0\(minute)"
            
        } else {
            
            minuteString = "\(minute)"
        }
        
        if seconds < 10 {
            
            secondsString = "0\(seconds)"
            
        } else {
            
            secondsString = "\(seconds)"
        }
        
        remainingTimeLabel.text = minuteString + ":" + secondsString
        
    }
    
    
}

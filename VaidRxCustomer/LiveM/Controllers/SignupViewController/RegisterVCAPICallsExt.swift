//
//  RegisterVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension RegisterViewController {
    
    func serviceRequestToMakeRegister() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        var facebookId:String = ""
        
        if isFBSignUp {
            
            facebookId = fbUserDataModel.userId
        }
        
        if facebookId.length > 0 {
            
            userRegisterType = RegisterType.Facebook
            registerViewModel.facebookId = facebookId
        }
        
        if !isReferralCodeValid {
            
            registerViewModel.referralCodeText.value = ""
        }
        
        let dateFormat = DateFormatter()
        //dddd
        dateFormat.dateFormat = "MM-dd-yyyy"
        registerViewModel.dobText.value = dateFormat.string(from: selectedDOB)
        
        registerViewModel.registerType = userRegisterType
        registerViewModel.profilePicURL = profilePicURL
        registerViewModel.countryCode = countryCodeLabel.text!
        
        registerViewModel.registerAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.signUp, userRegisterType: self.userRegisterType)
        }
        
    }
    
    
    
    func sendRequestToValidateEmailAddress() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        registerViewModel.validateEmailAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.verifyEmail, userRegisterType: nil)
        }
        
    }
    
    func sendRequestToValidatePhoneNumber() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        registerViewModel.countryCode = countryCodeLabel.text!
        
        registerViewModel.validatePhoneNumberAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.verifyMobileNumber, userRegisterType: nil)
        }
        
    }
    
    func sendRequestToValidateReferralCode() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        registerViewModel.validateReferralCodeAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.verifyReferralCode, userRegisterType: nil)
        }
        
    }
    
    @objc func sendServiceForFacebookLogin() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        userLoginType = LoginType.Facebook
        
        var facebookId:String = ""
        
        if isFBSignUp {
            
            facebookId = fbUserDataModel.userId
        }
        
        if facebookId.length > 0 {
            
            userLoginType = LoginType.Facebook
            registerViewModel.facebookId = facebookId
        }
        
        
        registerViewModel.facebookLoginAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.signIn, userRegisterType: RegisterType.Facebook)
        }
   
    }
  
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType, userRegisterType:RegisterType?)
    {
        if HTTPSResponseCodes.Register(rawValue: statusCode) != nil {
        
            let responseCodes : HTTPSResponseCodes.Register = HTTPSResponseCodes.Register(rawValue: statusCode)!
            
            switch responseCodes {
                
                case .WrongInputData://wrong data
                    
                    switch requestType {
                        
                    case RequestType.verifyEmail://Verify Email Error Response
                        
                        if !isFBSignUp {
                            
                            Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                            emailTextField.text = ""
                            registerViewModel.emailText.value = ""
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                                self.emailTextField.becomeFirstResponder()
                            })
                            
                        } else {
                            
                            isFBSignUp = true
                            self.perform(#selector(sendServiceForFacebookLogin), with:nil, afterDelay: 0.6)
                        }
                        
                    case RequestType.verifyMobileNumber:
                        
                        Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                        phoneNumerTextField.text = ""
                        registerViewModel.phoneNumberText.value = ""
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                            self.phoneNumerTextField.becomeFirstResponder()
                        })
                        
                        
                    case RequestType.verifyReferralCode:
                        
                        Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                        referralCodeTextField.text = ""
                        registerViewModel.referralCodeText.value = ""
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                            self.referralCodeTextField.becomeFirstResponder()
                        })
                        
                        
                    default:
                        
                        break
                        
                    }
                    
                case .WrongPhoneNumber:
                    
                    switch requestType {
                        
                    case RequestType.verifyMobileNumber:
                        
                        Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                        phoneNumerTextField.text = ""
                        registerViewModel.phoneNumberText.value = ""
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                            self.phoneNumerTextField.becomeFirstResponder()
                        })
                        
                        
                    default:
                        
                        break
                        
                    }
                    
                    
                case .MissingPasswordFBLogin://User Already Registered using normal password, so user as to make login using normal password
                    
                    switch requestType {
                        
                        case RequestType.signIn:
                            
                            if errorMessage != nil {
                                
                                Helper.alertVC(title: ALERTS.Message , message: errorMessage!)
                            }
                            
                            self.performSegue(withIdentifier: SEgueIdetifiers.registerToLogin , sender: nil)
                        
                        default:
                            
                            break
                        
                    }
                
                    
                case .SuccessResponse:
                    
                    switch requestType {
                        
                        case RequestType.verifyEmail://Verify Email Success Response (Email available)
                            
                            isEmailValid = true
                            animateButtonLoading(checkFieldsAreValid(textField: emailTextField, textFieldText: emailTextField.text!))
                            
                        case RequestType.verifyMobileNumber:
                            
                            isPhoneNumberValid = true
                            animateButtonLoading(checkFieldsAreValid(textField: phoneNumerTextField, textFieldText: phoneNumerTextField.text!))
                            
                        case RequestType.verifyReferralCode:
                            
                            isReferralCodeValid = true
                        
                        case RequestType.signIn: //Sign In Success Response
                            
                            if let userDataResponse = dataResponse as? [String:Any] {
                                
                                registerViewModel.saveCurrentUserDetails(dataResponse: userDataResponse)
                                
                                self.showSplashLoading()
                                
                                //Initialize Coouch DB
                                CouchDBManager.sharedInstance.createCouchDB()
                                
                                //Connect to MQTT
//                                AppDelegate().connectToMQTT()
                                
                                ConfigManager.sharedInstance.getConfigurationDetails()
                                
                                MQTTOnDemandAppManager.sharedInstance().subScribeToInitialTopics()
                                
                                MixPanelManager.sharedInstance.initializeMixPanel()
                                
                                MixPanelManager.sharedInstance.userRegisteredEvent(registerType: userLoginType)
                                
                                //Goto HomeVC
                                let menu = HelperLiveM.sharedInstance
                                menu.createMenuView()

                            }
                            
                            
                            
                        case .sendOTP:
                            
                            break
                            
                        case .signUp:
                            
                            if let dataRes = dataResponse as? [String:Any] {
                                
                                let params = [ SERVICE_RESPONSE.Sid : GenericUtility.strForObj(object: dataRes[SERVICE_RESPONSE.Sid])]
                                self.performSegue(withIdentifier: SEgueIdetifiers.signinToOtp, sender: params)

                            }
                            
                            
                        default:
                            
                            break
                    }
                        
                    break
                        
            }
        
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
            
            switch requestType {
                
                case RequestType.verifyReferralCode:
                    
                    referralCodeTextField.text = ""
                    registerViewModel.referralCodeText.value = ""
                
                case RequestType.signIn:
                
                    if isFBSignUp {
                        
                        emailTextField.text = ""
                        registerViewModel.emailText.value = ""
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                            self.emailTextField.becomeFirstResponder()
                        })

                    }
                
                
                default:
                    break
            }
            
        }

        
    }
    
    
    //MARK: - Splash Loading -
    func showSplashLoading() {
        
        Helper.showSplashLoading()
        
        self.perform(#selector(self.closeSplashLoading), with: nil, afterDelay: 45)
        
    }
    
    @objc func closeSplashLoading(){
        
        Helper.closeSplashLoading()
    }
    
}

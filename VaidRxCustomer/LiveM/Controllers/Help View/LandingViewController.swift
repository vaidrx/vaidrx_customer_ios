//
//  LandingViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 27/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import AVFoundation

class LandingViewController: UIViewController {
    
    // MARK: - Outlets -
    //ImageView
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    //Page Controller
    @IBOutlet weak var pageController: UIPageControl!
    
    //Collection View
    @IBOutlet weak var collectionView: UICollectionView!
    
    //Button
    @IBOutlet weak var loginButtonOutlet: UIButton!
    @IBOutlet weak var registerButtonOutlet: UIButton!
    @IBOutlet var videoView: UIView!
    
    
    // MARK: - Variable Declerations -
    var loginView : LoginViewController?
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var paused: Bool = true
    
    
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupVideo()//Set up and show splash screen video
        
        let location = LocationManager.sharedInstance()
        location.start()
        
        animations()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true

        initiallButtonSetUp()//Show previous selected button in Help screen
        
        if self.avPlayer != nil {
            
            self.avPlayerLayer.player?.play()
            self.paused = false
        }

    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.avPlayer != nil {
            
            avPlayer.pause()
            paused = true

        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override var prefersStatusBarHidden : Bool {
//        return true
//    }
    
    // MARK: - Action Methods -
    
    /// Action to login Controller
    ///
    /// - Parameter sender: Action
    @IBAction func loginAction(_ sender: Any) {
        
        self.changeButtonState(sender: sender as AnyObject)
//        catransitionAnimation(idntifier: VCIdentifier.loginVC)
        
        let loginVC = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.loginVC) as! LoginViewController
        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                  subType: kCATransitionFromTop,
//                                                                  for: (self.navigationController?.view)!,
//                                                                  timeDuration: 0.3)
        
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        self.navigationController?.pushViewController(loginVC, animated: false)

        
    }
    
    /// Action To Register Controller
    ///
    /// - Parameter sender: Action
    @IBAction func registerAction(_ sender: Any) {
        
        self.changeButtonState(sender: sender as AnyObject)
//        catransitionAnimation(idntifier: VCIdentifier.registerVC)
        
        let registerVC = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.registerVC) as! RegisterViewController
        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                  subType: kCATransitionFromTop,
//                                                                  for: (self.navigationController?.view)!,
//                                                                  timeDuration: 0.3)
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.pushViewController(registerVC, animated: false)

    }
    
}





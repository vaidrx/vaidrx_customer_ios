//
//  BookingCancelledView.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class BookingCancelledView: UIView {

    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    var message: String = "Booking declined by doctor!"
    
    private static var share: BookingCancelledView? = nil
    
    static var sharedInstance: BookingCancelledView {
        
        if share == nil {
            
            share = Bundle(for: self).loadNibNamed("BookingCancelled",
                                                   owner: nil,
                                                   options: nil)?.first as? BookingCancelledView
            
            share?.frame = (WINDOW_DELEGATE??.frame)!
            
//            share?.tableView.register(UINib(nibName: "CancelBookingTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cancelBookingCell")
            
        }
        
        
        
//        share?.acessClass.acessDelegate = share!
        
        return share!
    }


    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //custom logic goes here
    }
    
    func changeTitle(message: String)  {
        
        messageLabel.text = message
    }

    @IBAction func closeAction(_ sender: UIButton) {
        
        topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        
        UIView.animate(withDuration: 0.6,
                       delay: 0.2,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                        
        }) { (finished) in
            
//            BookingCancelledView.share?.acessClass.acessDelegate = nil
            
            self.removeFromSuperview()
            BookingCancelledView.share = nil
            let story = UIStoryboard(name: "Main", bundle:nil)
            let vc = story.instantiateViewController(withIdentifier: "VaidHomeVc") as! VrHomeViewController
            UIApplication.shared.windows.first?.rootViewController = vc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
}

//
//  UserDefaultsModel.swift
//  LiveM
//
//  Created by Raghavendra V on 16/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


struct USER_DEFAULTS {
    
    struct TOKEN {
        static let ACCESS  = "token"
        static let SESSION = "session_token"
        static let PUSH    = "push_token"
    }
    
    struct LOCATION {
        
        static let Latitude  = "latitude"
        static let Longitude = "longitude"
        static let Address    = "address"
        static let City    = "city"
        static let Country    = "country"
        static let State    = "state"
    }
    
    struct USER {
        
        static let CUSTOMERID = "customer_id"
        static let FIRSTNAME = "firstName"
        static let LASTNAME = "lastName"
        static let MOBILE = "phone"
        static let COUNTRY_CODE = "countryCode"
        static let EMAIL = "email"
        static let USER_PASSWORD = "password"
        static let FACEBOOK_USER_ID = "fbUserId"
        static let USER_DOB = "dateOfBirth"
        static let PIC = "profilePic"
        static let LoginType = "loginType"
        static let DEVICEID = "deviceId"
        static let DEVICETYPE = "deviceType"
        static let BID = "bid"
        static let CURRENCYSYMBOL = "currencySymbol"
        static let DISTANCE = "distance"
        static let TIME_INTERVAL = "TimeInterval"
        static let REFERRAL_CODE = "referralCode"
        static let COUPON = "coupon_code"
        static let FCMTopic = "fcmTopic"
        static let ZenDeskRequesterID = "zenDeskRequesterID"
        static let PAYMENT_TYPE = "paymentType"
        
        static let PHARMACY_NAME = "pharmacyName"
        static let PHARMACY_ID = "pharmacyId"

        
        static let CANCENLATION_FEE = "cancellation_fee"
        static let CANCENLATION_MIN_AFTER = "cancenlation_min_After"
    
        static let GoToUpcomingBookings = "goToUpcomingBookings"
        static let NewBookingisCreated = "newBookingisCreated"
        static let BookingCancelled = "bookingCancelled"
        static let BookingReviewed = "bookingReviewed"
        static let MILEAGE_MATRIC = "mileageMatric"
        
        static let INSURANCEID = "insuranceId"
        static let PAYMENTTYPE = "paymentType"
        static let PAYMENT_TYPE_CHANGED = "paymentTypeChanged"
        static let INSURANCE = "insurance"
        
    }
    
    struct CONFIG {
        
        static let CURRENCYSYMBOL = "currencySymbol"
        static let CURRENCYCODE = "currencyCode"
        static let APP_VERSION = "appVersion"
        static let PAYMENT_GATEWAY_KEY = "paymentGatewayKey"
        static let GOOGLE_MAP_SERVER_KEY = "googleMapServerKey"
        static let DISTANCE_TYPE = "distanceType"
        static let CUSTOMER_PUBLISH_LOC_TIME_INTERVAL = "customerPubLocTimeInterval"
        
    }

    
    
    struct  KEY {
        static let GOOGLE_MAP_KEY = "mapKey"
        static let GOOGLE_PLACES_KEY = "PlacesKey"
    }
    
    struct  PAYMENT_GATEWAY {
        static let API_KEY = "apiKey"
    }
    
    struct CHANNEL {
        static let PUBLISH_CHANNEL = "pubnub_publish"
        static let SUBSCRIBE_CHANNEL = "pubnub_subscribe"
        static let SERVER_CHANNEL = "server_channel"
        static let PRESENCE_CHANNEL = "abc"
        static let MY_CHANNEL = "my_channel"
        static let DRIVER_CHANNEL = "driver_channel"
    }
    
    struct API_CALL {
        static var profileScreen = true
        static var faqScreen = true
        
    }
    
    struct RATING {
        static let CATEGORIES = "categories"
    }

    
    struct MUSICIAN_LIST {
        
        static let PREVIOUS = "previousMusicianList"
    }
    
    struct BOOKINGS {
        
        static let ON_GOING_BOOKINGS = "onGoingBookings"
    }
    
    struct IP_ADDRESS {
        
        static let iPAddress = "ipAddress"
    }
}

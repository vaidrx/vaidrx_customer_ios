//
//  vaiDRx_EnterPassword_ButonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_EnterPasswordVC {
    
    
    @IBAction func nextAction(_ sender: UIButton) {
    
        if Helper.isValidPassword(password: passwordTextFeild.text!) {
        
            sendServiceToMakeLogin(loginType: LoginType.Default)
            
        }
        else {
        
            Helper.alertVC(title: "Message", message: "Entered a strong password")
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
    
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        
//        sendRequestToForgotPasswordUsingEmail()
        performSegue(withIdentifier: "toForgotPassword", sender: self)
    }
}

//
//  BookingStatusResponseModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 10/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Booking Flow Status Response Model
class BookingStatusResponseModel {
    
    var bookingId:Int64 = 0
    var bookingStatus = 0
    var bookingStatusMessage = ""
    var statusUpdatedTime = 0
    var providerFName = ""
    var providerLName = ""
    var bookingType = 0//1-vaidNow//2-vaidToday
    var clinicVisitTime = 0
    var vaidTodayStatus = 2 //0-not accepted//1-Accepted
    var bookingModel = 0//1-for all//2-particular
//    var lastBooked = 0
    
    var musicianImageURL = ""
    
    var bookingTimerDetails:[String:Any] = [:]

    
    init(bookingStatusUpdateMessage:Any) {//MQTT booking stastus message
        
        if let bookingStatusResponse = bookingStatusUpdateMessage as? [String:Any] {
            providerFName = GenericUtility.strForObj(object:bookingStatusResponse["firstName"])
            
            providerLName = GenericUtility.strForObj(object:bookingStatusResponse["lastName"])
            
            bookingId = Int64(GenericUtility.strForObj(object:bookingStatusResponse["bookingId"]))!
            bookingStatus = GenericUtility.intForObj(object:bookingStatusResponse["status"])
            bookingType = GenericUtility.intForObj(object: bookingStatusResponse["bookingType"])
            clinicVisitTime = GenericUtility.intForObj(object: bookingStatusResponse["bookingDate"])
            statusUpdatedTime = GenericUtility.intForObj(object:bookingStatusResponse["statusUpdateTime"])
            bookingStatusMessage = GenericUtility.strForObj(object:bookingStatusResponse["statusMsg"])
            vaidTodayStatus = GenericUtility.intForObj(object: bookingStatusResponse["vaidTodayStatus"])
            musicianImageURL = GenericUtility.strForObj(object:bookingStatusResponse["proProfilePic"])
            bookingModel = GenericUtility.intForObj(object: bookingStatusResponse["bookingModel"])
            
            if let bookingTimer = bookingStatusResponse["bookingTimer"] as? [String:Any] {
                
                bookingTimerDetails = bookingTimer
            }
            
        }
    }
    
    init(pushbookingStatusMessage:Any) {//Push booking stastus message
        
        if let pushbookingStatusResponse = pushbookingStatusMessage as? [String:Any] {
            
            bookingId = Int64(GenericUtility.strForObj(object:pushbookingStatusResponse["bookingId"])) ?? 0
            bookingStatus = Int(GenericUtility.strForObj(object:pushbookingStatusResponse["status"])) ?? 0
            
            statusUpdatedTime = Int(GenericUtility.strForObj(object:pushbookingStatusResponse["statusUpdateTime"])) ?? 0
            bookingStatusMessage = GenericUtility.strForObj(object:pushbookingStatusResponse["statusMsg"])
            
            musicianImageURL = GenericUtility.strForObj(object:pushbookingStatusResponse["proProfilePic"])
            
            if let bookingTimer = pushbookingStatusResponse["bookingTimer"] as? [String:Any] {
                
                bookingTimerDetails = bookingTimer
            }
            
        }
    }


    
    
}

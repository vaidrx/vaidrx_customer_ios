//
//  ChangeEmailMobileViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 18/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol changeEmailMobileDelegate {
    func recallApi(sucess: Bool)
}

class ChangeEmailMobileViewController: UIViewController, AccessTokeDelegate {
    
    
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var mobileEmailTF: UITextField!
    
    @IBOutlet weak var eraseTFButton: UIButton!
    
    @IBOutlet weak var discriptionLabel: UILabel!
    
    @IBOutlet weak var loadingWidth: NSLayoutConstraint!
    
    @IBOutlet weak var phoneNumberView: UIView!
    
    @IBOutlet weak var continueBottomConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var continueButton: UIButtonCustom!
    
    var delegate: changeEmailMobileDelegate?
    
    var cImage: String = ""
    var isEmail: Bool = false
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var selectedCountry: Country!
    var apiTag: Int!
    let acessClass = AccessTokenRefresh()
    
    var currentEmail = ""
    var currentPhoneNumber = ""
    var currentCountryCode = ""
    var currentCountryImage:UIImage!
    
    var changeEmailMobileViewModel = ChangeEmailMobileViewModel()
    let disposeBag = DisposeBag()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallSetUp()
        Helper.hideNavBarShadow(vc: self)
//        mobileEmailTF.text = currentEmail
//        phoneNumberTF.text = currentPhoneNumber
        countryCodeLabel.text = currentCountryCode
        
        if currentCountryImage != nil {
            
            countryImage.image = currentCountryImage
        }
        
        changeEmailMobileViewModel.emailText.value = mobileEmailTF.text!
        changeEmailMobileViewModel.phoneNumberText.value = phoneNumberTF.text!
        
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        appDelegate?.keyboardDelegate = self
        appDelegate?.accessTokenDelegate = self
        setNavigationBar()
        addObserveToVariables()
        
        if isEmail {
            
            discriptionLabel.text = CHANGE_ME.emaileText
            phoneNumberView.isHidden = true
            mobileEmailTF.becomeFirstResponder()
            titleLbl.text = CHANGE_ME.emailTittle
            
            validateFields(textFieldText: mobileEmailTF.text!)
            
        }else {
            
            discriptionLabel.text = CHANGE_ME.phoneText
            phoneNumberView.isHidden = false
            phoneNumberTF.becomeFirstResponder()
            titleLbl.text = CHANGE_ME.phoneTittle
            validateFields(textFieldText: phoneNumberTF.text!)
        }
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        //        self.navigationController?.view?.backgroundColor = UIColor.clear
//        self.navigationController?.navigationBar.barTintColor = APP_COLOR
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func eraseAction(_ sender: Any) {
        phoneNumberTF.text = ""
        changeEmailMobileViewModel.emailText.value = ""
        mobileEmailTF.text = ""
        changeEmailMobileViewModel.phoneNumberText.value = ""
    }
    
    @IBAction func continueAction(_ sender: Any) {
        
        if isEmail {
            
            if (mobileEmailTF.text?.length)! > 0 {
                
                if Helper.isValidEmail(testStr: mobileEmailTF.text!) {
                    
                    sendRequestToValidateEmailAddress()
                    
                } else {
                    
                     Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailMissing)
                }
                
            } else {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailMissing)
            }
            
        }else {
            
            if (phoneNumberTF.text?.length)! > 5 {
                
                sendRequestToValidatePhoneNumber()
                
            } else {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.PhoneNumberMissing)
            }
            
        }
    }
    
    @IBAction func countryPickerAction(_ sender: Any) {
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    func recallApi() {
        switch apiTag {
        case RequestType.verifyEmail.rawValue:
            sendRequestToValidateEmailAddress()
            
        case RequestType.updateMobile.rawValue:
            sendRequestToChangeMobile()
            
        case RequestType.updateEmail.rawValue:
            sendRequestToChangeEmail()
            
        case RequestType.verifyMobileNumber.rawValue:
            sendRequestToValidatePhoneNumber()
            
        default:
            break
        }
    }
    
    func initiallSetUp()  {
        
        if countryCodeLabel.text?.length == 0 {
            
            if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
                DDLogDebug(countryCode)
                let picker = CountryNameViewController()
                picker.initialSetUp()
                let contry: Country
                contry = picker.localCountry(countryCode)
                countryCodeLabel.text = contry.dial_code
                let imagestring = contry.country_code
                let imagePath = "CountryPicker.bundle/\(imagestring).png"
                countryImage.image = UIImage(named: imagePath)
            }

        }
        
    }
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier) as! CountryNameViewController
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        dstVC.countryDelegate = self
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEgueIdetifiers.changeEMToVerify {
            if let dstVC = segue.destination as? VerifyMobileViewController {
                dstVC.isCommingFrom = "ChangeEmailPhone"
                dstVC.dataFromForgetPasswordVC = sender as! [String : Any]
                dstVC.delegate = self
            }
        }
    }
    
    func addObserveToVariables() {
        
        mobileEmailTF.rx.text
            .orEmpty
            .bind(to: changeEmailMobileViewModel.emailText)
            .disposed(by: disposeBag)
        
        phoneNumberTF.rx.text
            .orEmpty
            .bind(to: changeEmailMobileViewModel.phoneNumberText)
            .disposed(by: disposeBag)
        
    }
    
    
    @IBAction func nextAction(_ sender: UIButton) {
        
        if isEmail {
            
            if (mobileEmailTF.text?.length)! > 0 {
                
                if Helper.isValidEmail(testStr: mobileEmailTF.text!) {
                    
                    sendRequestToValidateEmailAddress()
                    
                } else {
                    
                    Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailMissing)
                }
                
            } else {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailMissing)
            }
            
        }else {
            
            if (phoneNumberTF.text?.length)! > 5 {
                
                sendRequestToValidatePhoneNumber()
                
            } else {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.PhoneNumberMissing)
            }
            
        }
    }
    
}


extension ChangeEmailMobileViewController: KeyboardDelegate {
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
//        let inputViewFrame: CGRect? = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect
//        continueBottomConstrain.constant = (inputViewFrame?.size.height)!

        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.nextButtonBottomConstraint.constant = ((keyboardSize?.height)! + 30.0)
                        self.view.layoutIfNeeded()
        })
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        //Once keyboard disappears, restore original positions
        
        UIView.animate(withDuration: 0.4) {
            
            self.nextButtonBottomConstraint.constant = 30
            self.view.layoutIfNeeded()
        }
        
    }
    
}

// MARK: - CountrySelectedDelegate -
extension ChangeEmailMobileViewController: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImage.image = UIImage(named: imagePath)
        
        countryCodeLabel.text =  self.selectedCountry.dial_code
    }
    
    /// Filter Country Name
    ///
    /// - Parameter searchText: To search text in country name
    func filtercountry(_ searchText: String) {
        let picker = CountryNameViewController()
        picker.initialSetUp()
        let contry: Country
        contry = picker.localCountryName(searchText)
        let imagestring = contry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImage.image = UIImage(named: imagePath)
    }
    
}

extension ChangeEmailMobileViewController: VerifyMobileViewControllerDelegate {
    func didUpdateMobileNumber() {
        delegate?.recallApi(sucess: true)
    }
}


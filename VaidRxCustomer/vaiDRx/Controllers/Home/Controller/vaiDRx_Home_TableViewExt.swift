//
//  vaiDRx_Home_TableViewExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 13/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_HomeVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_Home_TableViewCell.self), for: indexPath) as! vaiDRx_Home_TableViewCell
        
        switch indexPath.row {
            
        case 0:

            cell.descriptionLabel.text = vaiDRx_Home.vaidNow_Description
            cell.vaidTitle.text = vaiDRx_Home.vaidNow
            cell.icon.image = #imageLiteral(resourceName: "home_briefcase_icon_on")

            
        case 1:
            cell.descriptionLabel.textColor = UIColor.white
            cell.descriptionLabel.text = vaiDRx_Home.vaidToday_Description
            cell.vaidTitle.textColor = UIColor.white
            cell.vaidTitle.text = vaiDRx_Home.vaidToday
            cell.icon.image = #imageLiteral(resourceName: "home_note_icon_off")

            
//        case 2:
//
//            cell.descriptionLabel.text = vaiDRx_Home.rxToday_Description
//            cell.vaidTitle.text = vaiDRx_Home.rxToday
//            cell.icon.image = #imageLiteral(resourceName: "home_tablet_icon_on")
            
            
        default:
            break
        }
        
        return cell
    }
}

extension vaiDRx_HomeVC: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch indexPath.row {
        case 0:
            self.vaidType = 1
            performSegue(withIdentifier: "toDataSource", sender: self)
        case 1:
            self.vaidType = 2
            performSegue(withIdentifier: "toDataSource", sender: self)
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            cell.backgroundColor = .clear
        default:
            cell.backgroundColor = UIColor(hex: "67AFF7", alpha: 0.7)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0,1 :
            return 300
        default:
            return 300
        }
    }
}

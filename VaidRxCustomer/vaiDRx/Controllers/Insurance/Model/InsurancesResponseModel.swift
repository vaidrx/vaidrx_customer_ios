//
//  TypesOfInsuranceModel.swift
//  GoTasker
//
//  Created by 3Embed on 18/12/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

struct InsuranceItems {
    var customerId = ""
    var insuranceName = ""
    var insuranceNameLang : [String:Any] = [:]
    var insuranceDescriptionLange = ""
    var num = 0
}

struct InsurancesResponseModel{
    
    var insuranceItemsModel = Array<InsuranceItems>()
    init(insuranceResponseDetails:Any) {
        
        print("insuranceResponseDetails",insuranceResponseDetails)
        if let insuranceResponse = insuranceResponseDetails as? Array<Dictionary<String,Any>>{
            print("insuranceResponse",insuranceResponse)
                for item in insuranceResponse{
                    
                    var dataItems = InsuranceItems()
                        dataItems.customerId = GenericUtility.strForObj(object: item["_id"])
                        dataItems.insuranceName = GenericUtility.strForObj(object: item["insuranceName"])
                        //dataItems.insuranceNameLang = item["insuranceNameLang"] as! [String:Any]
                        //dataItems.insuranceDescriptionLange = GenericUtility.strForObj(object: dataItems.insuranceNameLang["en"])
                       // dataItems.num = item["num"] as! Int
                    
                    
                    insuranceItemsModel.append(dataItems)
                    
                }
        
        }
        
        
    }
}

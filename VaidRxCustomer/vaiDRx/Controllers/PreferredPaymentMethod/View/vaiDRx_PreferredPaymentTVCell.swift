//
//  vaiDRx_PreferredPaymentTVCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 08/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class vaiDRx_PreferredPaymentTVCell: UITableViewCell {
    
    //UI ImageViews
    @IBOutlet weak var selectedCheckImage: UIImageView!
    @IBOutlet weak var selectedCheckImageNew: UIImageView!
    
    //Labels
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var paymentMethodLabelNew: UILabel!


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  vaiDRx_SocialLoginVC_CustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 26/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_SocialLoginVC {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSocialSignUp" {
            if let dstVC = segue.destination as? vaiDRx_SocialSignUpVC{
                
                dstVC.registerViewModel = self.registerViewModel
                
                if isFBLogin! {

                    dstVC.UserDataModel = self.fbUserDataModel
                    

                } else if isGoogleLogin! {
                    
                    dstVC.UserDataModel = self.gmailUserDataModel
                    
                }
                
            }
        }
        
        if segue.identifier == "ToPasswordVc" {
            if let dstVC = segue.destination as? vaiDRx_EnterPasswordVC{
                if isFBLogin! {

                    dstVC.socialMediaEmail = self.fbUserDataModel.mailId


                } else if isGoogleLogin! {

//                    dstVC.loginViewModel = self.gmailUserDataModel

                }

            }
        }
    }
    
//
    
}

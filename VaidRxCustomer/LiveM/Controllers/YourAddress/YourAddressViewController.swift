//
//  YourAddressViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 10/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class YourAddressViewController: UIViewController {
    
    // MARK: - Outlets -
    // Button
    @IBOutlet weak var addNewAddressButton: UIButtonCustom!
    
    @IBOutlet var addNewAddressButtonBackView: UIView!
    
    //TableView
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var messageLabel: UILabel!
    
    
    
    // MARK: - Variable Decleration -
    var selectedDirection: UITableView.Direction!
    
    weak var delegate: LeftMenuProtocol?
    
    var arrayOfAddress:[AddressModel]! = []
    
    var isFromConfirmBookingVC:Bool = false
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    var selectedAddressTag:Int!
    
    var addressArrayFromCouchDB:[Any] = []
    
    static var obj:YourAddressViewController? = nil
    
    var isAddressListChanged:Bool = false
    
    var yourAddressViewModel = YourAddressViewModel()
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!

    
    class func sharedInstance() -> YourAddressViewController {
        
        return obj!
    }
    
    
    
    
    // MARK: - Default calss Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        YourAddressViewController.obj = self
        
        isAddressListChanged = true
        
        tableView.backgroundView = messageLabel
        
        getAddressAPI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Helper.editNavigationBar(navigationController!)
        
        setupGestureRecognizer()
        acessClass.acessDelegate = self
        
        if isAddressListChanged {
            
            getAddressAPI()
            
        } else {
            
            loadAddressFromCouchDB()
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Action methods -
    
    @IBAction func addNewAddressAction(_ sender: Any) {
        
        //Go to Add New Address VC
        let addNewAddressVC:AddNewAddressViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.addNewAddressVC) as! AddNewAddressViewController
        
        addNewAddressVC.isAddressForEditing = false
        addNewAddressVC.isFromConfirmBookingVC = isFromConfirmBookingVC
        
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController!.pushViewController(addNewAddressVC, animated: false)
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        if isFromConfirmBookingVC {
            
            self.navigationController?.popViewController(animated: true)
            
        } else {
            
//            delegate?.changeViewController(LeftMenu.searchArtist)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func deleteAddressButtonAction(index:Int) {
//
//        selectedAddressTag = index
//
//        let alertController = UIAlertController(title: ALERTS.RemoveAddress,
//                                                message:ALERTS.RemoveAddressMessage,
//                                                preferredStyle: .alert)
//
//        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
//        newAlertWindow.rootViewController = UIViewController()
//        newAlertWindow.windowLevel = UIWindowLevelAlert + 1
//        newAlertWindow.makeKeyAndVisible()
//
//        alertController.addAction(UIAlertAction(title: ALERTS.NO,
//                                                style: .default,
//                                                handler: {(_ action: UIAlertAction) -> Void in
//
//                                                    newAlertWindow.resignKey()
//                                                    newAlertWindow.removeFromSuperview()
//        }))
//
//        alertController.addAction(UIAlertAction(title: ALERTS.YES,
//                                                style: .default,
//                                                handler: {(_ action: UIAlertAction) -> Void in
//
//                                                    newAlertWindow.resignKey()
//                                                    newAlertWindow.removeFromSuperview()
//                                                    //Delete Address
//                                                    self.deleteAddressAPI()
//
//        }))
//
//        newAlertWindow.rootViewController?.present(alertController, animated: true) { _ in }
//
    }

}


extension YourAddressViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension YourAddressViewController {
    
    func setupGestureRecognizer() {
        
        guard (self.navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}


extension YourAddressViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.GetAddress.rawValue:
            
            getAddressAPI()
            
            break
            
        case RequestType.deleteAddress.rawValue:
            
            deleteAddressAPI()
            
            break
            
            
        default:
            break
        }
    }
    
}

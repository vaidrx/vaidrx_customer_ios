//
//  DistanceAndETAManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import RxCocoa
import RxSwift

protocol DistanceAndETAManagerDelegate {
    
    
    /// Delegate to show musician Distance and ETA details
    func distanceAndETARsponse(timeInMinute:String, distance:String)
}

class DistanceAndETAManager {
    
    static let sharedInstance = DistanceAndETAManager()
    var delegate:DistanceAndETAManagerDelegate? = nil
    
    var mileageMatric = 0

    let rxDistanceAndETAAPICall = DistanceAndETAAPI()
    let disposebag = DisposeBag()
    
    /// Method to call get list of saved address service API
    func getDistanceAndETA(appointmentLat:Double,
                           appointmentLong:Double,
                           musicianLat:Double,
                           musicianLong:Double) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            return
        }
        
        if !rxDistanceAndETAAPICall.getDistanceAndETA_Response.hasObservers {
            
            rxDistanceAndETAAPICall.getDistanceAndETA_Response
                .subscribe(onNext: {response in
                    
                    self.WebServiceResponse(response: response, requestType: RequestType.GetDistanceAndETA)
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxDistanceAndETAAPICall.getDistanceAndETAServiceAPICall(appointmentLat: appointmentLat,
                                                                appointmentLong: appointmentLong,
                                                                musicianLat: musicianLat,
                                                                musicianLong: musicianLong)
        
    }
    
    
    //MARK - WebService Response -
    func WebServiceResponse(response:APIResponseModel, requestType:RequestType)
    {
        if (response.data[SERVICE_RESPONSE.Error] != nil) {
            
//            Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
            
            sendEmptyResponse()
            return
        }
        
        switch response.httpStatusCode
        {
            case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
                case RequestType.GetDistanceAndETA:
                
                    parseDistanceAndETAResponse(response: response)
                
                default:
                    break
            }
            break
            
        default:
            
            break
        }
        
    }
    
    func parseDistanceAndETAResponse(response:APIResponseModel) {
        
        if let routesesponse:[Any] = response.data["routes"] as? [Any] {
            
            if routesesponse.count > 0 {
                
                if let innerDict = routesesponse[0] as? [String:Any] {
                    
                    if let legsArray = innerDict["legs"] as? [Any] {
                        
                        if let legsDict = legsArray[0] as? [String:Any] {
                            
                            var timeInMinute = 1
                            
                            if let duration = legsDict["duration"] as? [String:Any] {
                                
                                if let timeInSeconds = duration["value"] as? Int {
                                    
                                    timeInMinute = self.convertTimeFromSecToMinute(timeinSeconds: timeInSeconds)
                                }
                                
                            }
                            
                            var distanceInMeter = 0
                            
                            if let distance = legsDict["distance"] as? [String:Any] {
                                
                                if let distInMeter = distance["value"] as? Int {
                                    
                                    distanceInMeter = distInMeter
                                }
                                
                            }
                            
                            if delegate != nil {
                                
                                delegate?.distanceAndETARsponse(timeInMinute: "\(timeInMinute) MIN", distance: Helper.getDistanceDependingMileageMetricFromServer(distance: Double(distanceInMeter), mileageMatric: mileageMatric))
                            }
                            
                        }
                        
                    }
                    
                } else {
                    
                    sendEmptyResponse()
                }

            } else {
                
                sendEmptyResponse()
            }
            
            
        } else {
            
            sendEmptyResponse()
        }
        
    }
    
    func sendEmptyResponse() {
        
        if delegate != nil {
            
            delegate?.distanceAndETARsponse(timeInMinute: "1 MIN", distance: Helper.getDistanceDependingMileageMetricFromServer(distance: 0, mileageMatric: mileageMatric))
        }
    }
    
    func convertTimeFromSecToMinute(timeinSeconds: Int) -> Int {
        
        var min: Int
        
        if timeinSeconds < 60 && timeinSeconds <= 0 {
            min = 1
        }
        else if timeinSeconds > 60 {
            
            min = timeinSeconds / 60
            let remainingTime: Int = timeinSeconds % 60
            if remainingTime < 60 && remainingTime >= 30 {
                min += 1
            }
        }
        else {
            
            min = 1
        }
        
        return min
    }
    
    
    
}

//
//  InsuranceTypesAPICallExt.swift
//  GoTasker
//
//  Created by 3Embed on 18/12/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension TypesInsuranceViewController{
    
    
    func getInsuranceData(){
        
        if !NetworkHelper.sharedInstance.networkReachable(){
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
            //Helper.showAlertMessage(title: ALERT_VAIDRX.Error, message: ALERT_VAIDRX.NoInternet)
            return
        }
        
        insuranceTypesViewModel.getInsuranceTypesAPICall { (statusCode, errMsg, response) in
            print(response)
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response,requestType: RequestType.GetInsurances)
        }
    }
    
    func updateInsuranceData(){
        
        if !NetworkHelper.sharedInstance.networkReachable(){
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
            //Helper.showAlertMessage(title: ALERT_VAIDRX.Error, message: ALERT_VAIDRX.NoInternet)
            return
        }
        
        insuranceTypesViewModel.updateInsuranceTypesAPICall { (statusCode, errMsg, response) in
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response,requestType: RequestType.UpdateInsurance)
        }
    }
    
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResp:Any?,requestType: RequestType){
        switch statusCode {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            switch requestType {
                
            case .GetInsurances:
            
                insuranceData = InsurancesResponseModel.init(insuranceResponseDetails:dataResp as Any )
                
                insuranceItems = insuranceData.insuranceItemsModel
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
                print("insuranceItems",insuranceItems)
                
                
                print("data response",dataResp!)
              
            case .UpdateInsurance :
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                for vc in viewControllers
                {
                    if vc.isKind(of:ProfileViewController.self)
                    {
                        
                        self.navigationController!.popToViewController(vc, animated: true)
                        break;
                    }
                    
                }
                break
            default :
                break
            }
        default:
            break
        }
    }
}

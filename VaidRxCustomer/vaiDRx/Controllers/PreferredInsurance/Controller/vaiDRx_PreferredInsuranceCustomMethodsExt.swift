//
//  vaiDRx_PreferredInsuranceCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 28/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_PreferredInsuranceVC {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toCardDetails" {
            
            if let dstVC = segue.destination as? vaiDRx_AddNewCardVC {
                
                dstVC.registerViewModel = self.registerViewModel
            }
        }
    }
    
    
}

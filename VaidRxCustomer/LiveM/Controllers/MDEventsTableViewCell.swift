//
//  MDEventsTableViewCell.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

class MDEventsTableViewCell:UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var noEventsBackView: UIView!
    @IBOutlet var noEventsLabel: UILabel!
    
    @IBOutlet var selectedView: UIView!
    
    var CBModel:ConfirmBookingModel!
    
    var isConfirmBooking:Bool = false
    
}

extension MDEventsTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if CBModel.providerFullDetalsModel != nil {
            
            if CBModel.providerFullDetalsModel.eventsArray.count > 0 {
                
                self.noEventsBackView.isHidden = true
                self.collectionView.isHidden = false
                
                return 1
                
            } else {
                
                self.noEventsBackView.isHidden = false
                self.collectionView.isHidden = true
                
                return 0
            }

        }
        
        self.noEventsBackView.isHidden = false
        self.collectionView.isHidden = true

        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return CBModel.providerFullDetalsModel.eventsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let eventsCell:MDEventsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventsCell", for: indexPath) as! MDEventsCollectionViewCell
        
        let eventDict = CBModel.providerFullDetalsModel.eventsArray[indexPath.row] as! [String:Any]
        
        
        eventsCell.eventNameLabel.text = GenericUtility.strForObj(object:eventDict["name"])
        
        
        
        eventsCell.eventNameLabel.textColor = UIColor.black
        eventsCell.selectedDivider.isHidden = true
        eventsCell.eventImageView.isSelected = false
        
        //selectImage   //unselectImage
        
        let selectedImageView = UIImageView()
        
        let unSelectedImageView = UIImageView()
        
//        if let unselectedImageURL = eventDict["unselectImage"] as? String {
//            
//            
//        } else {
//            
//            
//        }
        
        
        if GenericUtility.strForObj(object: eventDict["unselectImage"]).length > 0 {
            
            unSelectedImageView.kf.setImage(with: URL(string:GenericUtility.strForObj(object: eventDict["unselectImage"])),
                                          placeholder:nil,
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            eventsCell.eventImageView.setImage(#imageLiteral(resourceName: "filter_birthday_party_icon_on"), for: UIControl.State.normal)
        }
        
        
        if GenericUtility.strForObj(object: eventDict["selectImage"]).length > 0 {
            
            selectedImageView.kf.setImage(with: URL(string: GenericUtility.strForObj(object: eventDict["selectImage"])),
                                            placeholder:nil,
                                            options: [.transition(ImageTransition.fade(1))],
                                            progressBlock: { receivedSize, totalSize in
            },
                                            completionHandler: nil)
            
        } else {
            
            eventsCell.eventImageView.setImage(#imageLiteral(resourceName: "filter_birthday_party_icon_on"), for: UIControl.State.selected)
        }


        
        if isConfirmBooking {
            
            if CBModel.selectedEventTag == indexPath.row {
                
                eventsCell.eventNameLabel.textColor = UIColor.red
                eventsCell.selectedDivider.isHidden = false
                eventsCell.eventImageView.isSelected = true
                
                eventsCell.eventImageView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                
                CBModel.selectedEventId = GenericUtility.strForObj(object:eventDict["_id"])
                
                UIView.animate(withDuration: 0.8, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options: [], animations: {
                    
                    eventsCell.eventImageView.transform = .identity
                    
                }) { (success) in
                    
                    
                }

            }
            
        }
        
        return eventsCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sizeOfEachCell:CGSize = CGSize.init(width: (collectionView.bounds.size.width - 30)/3.5, height: 95)
        
        return sizeOfEachCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isConfirmBooking {
            
            if self.CBModel.selectedEventTag != indexPath.row {
                    
                self.CBModel.selectedEventTag = indexPath.row
                self.collectionView.reloadData()
            }

        }
    }

}


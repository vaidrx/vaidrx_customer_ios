//
//  LandingScreenCollectionViewDelegateExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

// MARK: - Collection View Data Source -
extension LandingViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HelpCollectionCell", for: indexPath) as! HelpCollectionViewCell
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = ""//HELP.discover
            cell.discriptionLabel.text = ""// HELP.discoverDisc
            
        case 1:
            cell.titleLabel.text = ""//HELP.book
            cell.discriptionLabel.text = ""//HELP.bookDisc
            
        case 2:
            cell.titleLabel.text = ""//HELP.enjoy
            cell.discriptionLabel.text = ""//HELP.enjoyDisc
            
        case 3:
            cell.titleLabel.text = ""//HELP.review
            cell.discriptionLabel.text = ""//HELP.reviewDisc
            
        default:
            break
        }
        
        return cell
    }
    
}

// MARK: - Collection View Delegate -
extension LandingViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageController.currentPage = indexPath.row
    }
}

// MARK: - Collection View Flow Layout Delegate -
extension LandingViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.size.height - CGFloat(20)
        return CGSize(width: self.view.bounds.size.width , height:  height)
    }
    
}

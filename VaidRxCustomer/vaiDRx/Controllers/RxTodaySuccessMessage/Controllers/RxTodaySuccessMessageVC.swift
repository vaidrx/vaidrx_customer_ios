//
//  RxTodaySuccessMessageVC.swift
//  GoTasker
//
//  Created by 3Embed on 30/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class RxTodaySuccessMessageVC: UIViewController {

    
    @IBOutlet weak var backToHomeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backToHomeButton.layer.cornerRadius = backToHomeButton.frame.height / 2
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        hideNavigation()
    }
    
    func hideNavigation() {
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = .clear //UIColor(hex: "05C1C9") //UIColor.clear
    }
    
    @IBAction func backToHomeButAction(_ sender: UIButton) {
        
        // Mark :- Pop to Home screen
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        
        
        /*
        // Mark: - Pop two view controllers
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for vc in viewControllers
        {
            if vc.isKind(of:RxTodayViewController.self)
            {
                self.navigationController!.popToViewController(vc, animated: true)
                break;
            }
        }
        */
    }
    
    

}

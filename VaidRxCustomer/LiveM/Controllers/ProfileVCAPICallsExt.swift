//
//  ProfileVCAPICalssExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ProfileViewController {
    
    func logOut()  {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        profileViewModel.logoutAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.signOut)
        }
        
    }
    
    func sendServiceToGetProfile() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        hideViews()
        
        profileViewModel.getProfileDetailsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.getProfileData)
        }
        
    }
    
    
    func serviceRequestToUpdateProfile() {
        
//        if aboutMeTextView.text! != ALERTS.aboutMe && (aboutMeTextView.text?.length)! > 0 {
//            
//            profileViewModel.aboutMeText.value = aboutMeTextView.text!
//            
//        } else {
//            
//            profileViewModel.aboutMeText.value = ""
//        }
//        if musicGenreTextField.text == "insurance"{
//            
//        }
        let dateFormat = DateFormatter()
        //dddd
        dateFormat.dateFormat = "MM/dd/yyyy"
        profileViewModel.dobText = dateFormat.string(from: selectedDOB)
        
        profileViewModel.firstNameText.value = nameTextField.text!
        profileViewModel.lastNameText.value = lastNameTextField.text!
        profileViewModel.musicGenereText.value = musicGenreTextField.text!//musicGenreApi
       // profileViewModel.insurance = ""
        profileViewModel.profilePicURL = profilePicURL
       
        
        
//        profileViewModel.profileRequestModel = self.profileRequestModel
        
        profileViewModel.updateProfileDetailsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.updateProfileData)
        }
        
    }
    
    func serviceRequestToUpdateAddress(){
        profileViewModel.updateAddressDetailsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.UpdateAddress)
        }
    }
    
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        
        switch statusCode
        {
            
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if let dataRes = dataResponse as? String {
                
                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
                self.apiTag = requestType.rawValue
                
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
                case .signOut:
                    
                    progressMessage = PROGRESS_MESSAGE.LogOut
                    
                case .getProfileData:
                    
                    progressMessage = PROGRESS_MESSAGE.Loading
                    
                case .updateProfileData:
                    
                    progressMessage = PROGRESS_MESSAGE.Saving
                    sendServiceToGetProfile()
                  
                case .UpdateAddress:
                    
                    progressMessage = PROGRESS_MESSAGE.SavingAddress
                    sendServiceToGetProfile()
                default:
                    
                    break
                }
                
                self.acessClass.getAcessToken(progressMessage: progressMessage)
            }
            
            break
            
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.getProfileData:
                
                self.showViews()
                
                if dataResponse != nil {
                    
                    self.profileResponseModel = ProfileResponseModel.init(profileDetails: dataResponse!)
                    setAllData(data: profileResponseModel!)
                    
                }
                
                
            case RequestType.updateProfileData :
                DDLogDebug("Sucess")
                
                
            case RequestType.signOut :
                Helper.logOutMethod()
                DDLogDebug("Sucess")
                
            default:
                
                break
            }
            
        default:
            
            if  errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                
            }
            
            
            break
        }
        
    }
    
}

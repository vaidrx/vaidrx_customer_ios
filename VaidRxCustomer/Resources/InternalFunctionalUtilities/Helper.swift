//
//  Helper.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 19/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import Stripe
import AVFoundation

class Helper: NSObject,NVActivityIndicatorViewable {
    
    static var splashNewWindow:UIWindow?
    static var newAlertWindow:UIWindow? = nil
//    static let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
    
    static var player:AVAudioPlayer!
    
    //MARK: - Progress Ideicator
    
    // Show with Default Message
    class func showPI() {
        
        let activityData = ActivityData(size: CGSize(width: 30,height: 30),
                                        message: PROGRESS_MESSAGE.Loading,
                                        type: NVActivityIndicatorType(rawValue: 29),
                                        color: UIColor.white,
                                        padding: nil,
                                        displayTimeThreshold: nil,
                                        minimumDisplayTime: nil)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    /// Show loading
    ///
    /// - Parameter view: main View
    /// - Returns: return an image
    class func showLoading(view: UIView)-> UIImageView{
        let jeremyGif = UIImage.gifImageWithName("loadingImageAnimate")
        let imageView = UIImageView(image: jeremyGif)
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        imageView.frame = CGRect(x:  (view.frame.size.width / 2) - 35, y:  (view.frame.size.height / 2) - 35 , width: 70.0, height: 70.0)
        view.addSubview(imageView)
        view.bringSubviewToFront(imageView)
        return imageView
    }
    
    /// hides the image
    ///
    /// - Parameter imageView: image
    class func hideLoading(imageView: UIImageView){
        imageView.removeFromSuperview()
    }
    
    ///hide navigation bar shadow
    ///
    /// - Parameter navigationController: navigation Controller
    class func hideNavBarShadow(vc: UIViewController) {
        vc.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        vc.navigationController?.navigationBar.shadowImage = UIImage()
//        vc.navigationController?.navigationBar.tintColor = UIColor.clear
    }
    
    /// eddit navigation bar
    ///
    /// - Parameter navigationController: navigation Controller
    class func editNavigationBar(_ navigationController: UINavigationController)
    {
        navigationController.navigationBar.backgroundColor = UIColor.clear //UIColor(hex: "05C1C9") //UIColor.white
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
        //navigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)]
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController.navigationBar.shadowImage = UIImage()
//        navigationController.navigationBar.layer.shadowColor = UIColor.black.cgColor
        navigationController.navigationBar.layer.shadowOffset = CGSize(width: 3, height: 3)
        navigationController.navigationBar.layer.shadowRadius = 2.0
        navigationController.navigationBar.layer.shadowOpacity = 0.08
    }
    // Show with Your Message
    class func showPI(_message:  String) {
        
        let activityData = ActivityData(size: CGSize(width: 30,height: 30),
                                        message: _message,
     
                                        type: NVActivityIndicatorType(rawValue: 29),
                                        color: UIColor.white,
                                        padding: nil,
                                        displayTimeThreshold: nil,
                                        minimumDisplayTime: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        let when = DispatchTime.now() + 180
        DispatchQueue.main.asyncAfter(deadline: when){
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    // Hide
    class func hidePI() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    //AlertView
    class func alertVC(title:String, message:String) {
       
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        // Create the actions
        let okAction = UIAlertAction(title: ALERTS.Ok, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
        }
        // Add the actions
        alertController.addAction(okAction)
        
        newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    
    class func showAlert(head:String, message: String){
        let alertController = UIAlertController(title: head, message: message, preferredStyle: .alert)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        // Create the actions
        let okAction = UIAlertAction(title: ALERTS.Ok, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            if message == "Booking cancellation update failed" {
                ConfirmVaidToday.sharedInstance.hide()
            }
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
        }
        // Add the actions
        alertController.addAction(okAction)
        newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    class func clearAlertWindow() {
        if newAlertWindow != nil {
            newAlertWindow?.resignKey()
            newAlertWindow?.removeFromSuperview()
            newAlertWindow = nil
        }
    }

    class func addBorderShadow(view : UIView) -> UIView {
        
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
        view.layer.shouldRasterize = true
        return view
    }
    
    class func shadowView(sender: UIView, width:CGFloat, height:CGFloat ) {
        
        sender.layer.cornerRadius = 2
        // Drop Shadow
        let squarePath = UIBezierPath(rect:CGRect.init(x: 0, y:0 , width: width, height: height)).cgPath
        
        sender.layer.shadowPath = squarePath;
        //sender.layer.shadowRadius = 4
        sender.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(0.2))
        sender.layer.shadowColor = UIColor.lightGray.cgColor
        sender.layer.shadowOpacity = 0.3
        sender.layer.masksToBounds = false
    }
    
    
    class func addBorderShadow1(view : UIView) {
        
        view.backgroundColor = WHITE_COLOR;
        view.layer.masksToBounds = false;
        view.layer.shadowColor = Helper.UIColorFromRGB(rgbValue: 0x646464).cgColor;
        view.layer.shadowOffset = CGSize.zero;
        view.layer.shadowOpacity = 0.4;
        view.layer.shadowRadius = 5.0;
        
    }
    
    
    
    /// Method to add corner Radius to UIView
    ///
    /// - Parameters:
    ///   - view: input view
    ///   - radius: corner radius value
    ///   - color: border color value
    ///   - width: border width value
    class func addCornerRadius(view : UIView, radius:CGFloat , color: UIColor ,width:CGFloat) {
        view.layer.cornerRadius = radius;
        view.layer.borderColor = color.cgColor;
        view.layer.borderWidth = width;
        view.layer.masksToBounds = true
    }
    
    
    
    /// Method to get color from RGB value
    ///
    /// - Parameter rgbValue: input RGB value
    /// - Returns: Result color from input RGB Value
    class func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    
    /// Method to Resize the image
    ///
    /// - Parameters:
    ///   - image: input image
    ///   - targetSize: output size value
    ///   - scale: result image scale value
    /// - Returns: Resized output image
    class func resizeImage(_ image: UIImage,_ targetSize: CGSize,_ scale: Float) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, CGFloat(scale))
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    
    /// Get Payment Card Image
    ///
    /// - Parameter type: Type of Card
    /// - Returns: Return Value is Card Image
    class func cardImage(with type: String) -> UIImage {
        
        switch type {
            
            case "Visa":
                return STPImageLibrary.cardBrandImage(for: STPCardBrand.visa)
                
            case "MasterCard":
                return STPImageLibrary.cardBrandImage(for: STPCardBrand.mastercard)
                
            case "Discover":
                return STPImageLibrary.cardBrandImage(for: STPCardBrand.discover)
                
            case "American Express":
                return STPImageLibrary.cardBrandImage(for: STPCardBrand.amex)
                
            case "JCB":
                return STPImageLibrary.cardBrandImage(for: STPCardBrand.JCB)
                
            case "Diners Club":
                return STPImageLibrary.cardBrandImage(for: STPCardBrand.dinersClub)
                
            default:
                return STPImageLibrary.cardBrandImage(for: STPCardBrand.unknown)
        }
    }
    
    
    
    /// Method to Get User Date Of Birth Depends on date format
    ///
    /// - Parameter dobInString: input DOB string
    /// - Returns: converted DOB string
    class func getUserDOBDependsonDateFormat(dobInString:String) -> String {
        
        //Convert stringDate to Date Format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d-MM-yyyy"
//         let englishLocale = NSLocale(localeIdentifier: "en_US")
//         dateFormatter.locale = englishLocale as Locale!
        
        
        
        if let dob = dateFormatter.date(from: dobInString) {
            
            //Get String Date in required Format
            
            dateFormatter.dateFormat = "YYYY-MM-dd"
            return dateFormatter.string(from: dob)
        }
        
        return ""
        
    }
    
    
    
    //MARK: - Local Validation -
    
    /// Validate Method for email And Phone Number
    ///
    /// - Parameter testStr: input email or Phone Number
    /// - Returns: True or False
    class func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = EMAIL_REG_EXP
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    
    /// Method to check user entered valid password (used in Register screen)
    ///
    /// - Parameter password: input password
    /// - Returns: Boolean vaue TRUE or FALSE
    class func isValidPassword(password:String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", PASSWORD_REG_EXP)
        return passwordTest.evaluate(with: password)
    }
    
    
    
    /// Method to show shadow on UIView
    ///
    /// - Parameters:
    ///   - view: input view to show shadow
    ///   - width: width of the input view
    ///   - height: height of the input view
    class func setShadowFor(_ view: UIView, andWidth width: CGFloat, andHeight height: CGFloat) {
        
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: width, height: height))
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        //Here you control x and y
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 2.5
        //Here your control your blur
        view.layer.masksToBounds = false
        view.layer.shadowPath = shadowPath.cgPath
    }
    
    
    
    /// Method to check system version is greater than or not with the input version
    ///
    /// - Parameter version: input version value
    /// - Returns: Boolean vaue TRUE or FALSE
    class func SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(version: String) -> Bool {
        
        return UIDevice.current.systemVersion.compare(version, options: .numeric) != .orderedAscending
    }
    
    
    
    /// Method to measure height of the label depending on content
    ///
    /// - Parameters:
    ///   - label: input Label
    ///   - width: width of the input label
    /// - Returns: estimated height of the label
    class func measureHeightLabel(_ label:UILabel, width:CGFloat) -> CGFloat {
        
        let constrainedSize = CGSize(width: width, height: 9999)
        let attributesDictionary: [AnyHashable: Any] = [
            NSAttributedString.Key.font : UIFont(name: label.font.fontName, size: label.font.pointSize)!
        ]
        
//        let string = NSMutableAttributedString(string: label.text!, attributes: attributesDictionary as? [String : Any] ?? [String : Any]())
//        let requiredHeight: CGRect = string.boundingRect(with: constrainedSize, options: .usesLineFragmentOrigin, context: nil)
//        var newFrame: CGRect = label.frame
//        newFrame.size.height = requiredHeight.size.height
        return 30.0
    }
    
    
    
    /// Method to resize the image
    ///
    /// - Parameters:
    ///   - image: input image
    ///   - newSize: custom size user want to change
    /// - Returns: resized output image
    class func image(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.5)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
    
    
    
    
    /// Method to get current view controller showing on screen
    ///
    /// - Returns: current view controller that showing on screen
    class func getCurrentVC() -> UIViewController {
        
        if let viewController:UIViewController = UIApplication.shared.delegate?.window??.rootViewController {
            
            if let VC = viewController as? ExSlideMenuController
            {
                for navVC in VC.children {
                    
                    if let currentNavVC = navVC as? UINavigationController {
                        
                        return currentNavVC.topViewController!
                    }
                }
            } else {
                
                return viewController
            }
            
        }
        
        return UIViewController.init()
        
    }
    
    
    
    /// Method to parse youtube Id from youtube link address
    ///
    /// - Parameter link: Youtube link address
    /// - Returns: Parsed youtube Id
    class func extractYoutubeIdFromLink(link: String) -> String? {
        
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        guard let regExp = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive) else {
            return ""
        }
        let nsLink = link as NSString
        let options = NSRegularExpression.MatchingOptions(rawValue: 0)
        let range = NSRange(location: 0, length: nsLink.length)
        let matches = regExp.matches(in: link as String, options:options, range:range)
        if let firstMatch = matches.first {
            return nsLink.substring(with: firstMatch.range)
        }
        return ""
    }
    
    
    
    /// Play Custom Notification Sound
    class func playLocalNotificationSound() {
        
        let audioPath = Bundle.main.path(forResource:"sms-received", ofType: "wav")
        
        if audioPath != nil {
            
            do {
                
                let audioData = try Data.init(contentsOf: URL.init(fileURLWithPath: audioPath!))
                
                do {
                    
                    player = try AVAudioPlayer(data: audioData)
                    
                    player.prepareToPlay()
                    player.play()
                }
                catch {
                    
                    print("Something bad happened. Try catching specific errors to narrow things down",error)
                }
                
                
                
            }catch {
                
                print("Something bad happened. Try catching specific errors to narrow things down",error)
            }
            
        }
        
    }
    
    
    /// Method to get user selected language code to send in API request
    ///
    /// - Returns: user selected language code
    class func getLanguageCode() -> String {
        
        return "1"
    }

    
    
    /// Method to convert string dictionary to swift dictionary
    ///
    /// - Parameter text: string dictionary response from server
    /// - Returns: parsed swift dictionary [String:Any]
    class func convertToDictionary(text: String) -> [String: Any]? {
        
        if let data = text.data(using: .utf8) {
            
            do {
                
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                
            } catch {
                
                print(error.localizedDescription)
            }
            
        }
        
        return nil
    }
    
    
    
    /// Method to close Splash loading screen
    class func closeSplashLoading() {
        
        if SplashLoading.obj != nil {
            
            SplashLoading.obj?.closeView()
            
        }
        
    }
    
    
    
    class func showStatusBarBackView() {
        
//        statusBarView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        WINDOW_DELEGATE??.addSubview(statusBarView)
        
    }
    
    
    
    //MARK: - Splash Loading -
    class func showSplashLoading() {
        
        let splashLoading = SplashLoading.shared
        
        if splashNewWindow != nil {
            
            splashNewWindow?.resignKey()
            splashNewWindow?.removeFromSuperview()
            splashNewWindow = nil
        }
        
        splashNewWindow = UIWindow(frame: UIScreen.main.bounds)
        splashNewWindow?.rootViewController = UIViewController()
        splashNewWindow?.windowLevel = UIWindow.Level.alert + 1
        splashNewWindow?.makeKeyAndVisible()
        
        splashNewWindow?.addSubview(splashLoading)
        
        
        splashLoading.showGifAnimation()
        
        splashLoading.transform = CGAffineTransform.identity.scaledBy(x: 1.2, y: 1.2)
        
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0,
                       options: [],
                       animations: {
                        
                        splashLoading.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { _ in
            
            
        }
        
    }
    
    class func attributedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {
        
        let attrs = [
            NSAttributedString.Key.font: UIFont(name: "CircularAirPro-Bold", size: 13.0 ),
            // UIFont.boldSystemFont(ofSize: fontSize),
            NSAttributedString.Key.foregroundColor: Helper.UIColorFromRGB(rgbValue: 0x3e3e3e)
        ]
        let nonBoldAttribute = [
            NSAttributedString.Key.font: UIFont(name: "CircularAirPro-Book", size: 13.0 ),
            NSAttributedString.Key.foregroundColor: Helper.UIColorFromRGB(rgbValue: 0x3e3e3e)
        ]
        let attrStr = NSMutableAttributedString(string: string, attributes: attrs )
        if let range = nonBoldRange {
            attrStr.setAttributes(nonBoldAttribute, range: range)
        }
        return attrStr
    }
    
    
    
    /// Method to reload controller when network reachable
    class func reloadControllerWhenNetworkReachable() {
        
        switch Helper.getCurrentVC() {
            
            case is BookingFlowViewController:
            
                BookingFlowViewController.sharedInstance().getBookingDetailsAPI()
            
            case is MyEventViewController:
            
                MyEventViewController.sharedInstance().getBookingsAPI()
            
            default:
            
                break
            
        }
        
    }
    
    /// Method to logout
    class func logOutMethod() {
        
        if let viewController:UIViewController = UIApplication.shared.delegate?.window??.rootViewController {
            
            if let VC = viewController as? ExSlideMenuController
            {
                
                print(VC)
                
                Helper.goToSplashVC()
            }
            
        }
        
    }
    
    
    
    /// Method to move to splash viewcontroller
    class func goToSplashVC() {
        
        let deviceId: String = Utility.deviceId
        let pushToken: String = Utility.pushtoken
        
        
        //Remove previous search addresses document from couchDB
        AddressCouchDBManager.sharedInstance.deletePreviousSearchAddressCouchDBDocument()
        
        //Remove previous manage addresses document from couchDB
        AddressCouchDBManager.sharedInstance.deleteManageAddressCouchDBDocument()
        
        //Remove default card details document from couchDB
        PaymentCardCouchDBManager.sharedInstance.deletePaymentCardCouchDBDocument()
        
        //Remove Chat details document from couchDB
        ChatCouchDBManager.sharedInstance.deleteChatCouchDBDocument()
        
        MQTTOnDemandAppManager.sharedInstance().unSubScribeToInitialTopics()
        
        for key in UserDefaults.standard.dictionaryRepresentation().keys {
            UserDefaults.standard.removeObject(forKey: key.description)
        }
        
        let defaults = UserDefaults.standard
        defaults.set(deviceId, forKey: USER_DEFAULTS.USER.DEVICEID)
        defaults.set(pushToken, forKey: USER_DEFAULTS.TOKEN.PUSH)
        
        defaults.synchronize()
        
        MQTT.sharedInstance().closeMQTTConnection()
        Helper.clearAlertWindow()
        HelperLiveM.sharedInstance.goToSplashVC()
    }
    
    class func showBannedCustomerAlert(title:String, message:String) {
        
        
    }

    
    // Return IP address of WiFi interface (en0) as a String, or `nil`
    class func getIPAddress(){
        
        let url = URL(string: "https://icanhazip.com/")
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if data != nil {
                
                if var ipAddress = String(data: data!, encoding: String.Encoding.utf8) {
                    
                    ipAddress = ipAddress.trimmingCharacters(in: CharacterSet.newlines)
                    UserDefaults.standard.set(ipAddress, forKey: USER_DEFAULTS.IP_ADDRESS.iPAddress)
                    print(ipAddress)
                }

            }
            
        }
        
        task.resume()
        
    }
    
    
    
    /// Method to change Image frame
    ///
    /// - Parameters:
    ///   - image: input image to change frame
    ///   - newSize: new size of image
    /// - Returns: new size updated image
    class func changeImageFrame(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.5)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }

    
    /// Method to get DOB with date suffix
    ///
    /// - Parameter date: input date
    /// - Returns: result date with the suffix in date
    class func getDobStringDate(date:Date) -> String {
        
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year {
            
            print("\(day) \(month) \(year)")
            
            var days: String = String(day)
            switch (days) {
            case "1" , "21" , "31":
                days.append("st")
            case "2" , "22":
                days.append("nd")
            case "3" ,"23":
                days.append("rd")
            default:
                days.append("th")
            }
            
            return "\(days) \(months[month - 1]) \(year)"
            
        } else {
            
            return ""
        }
        
        
    }
    
    
    
    /// Method to get distance string depending on config mileage matric
    ///
    /// - Parameter distance: input distance in meter
    /// - Returns: output distamce string
    class func getDistanceDependingMileageMetricFromServer(distance:Double, mileageMatric:Int) -> String {
        
        if mileageMatric == 0 {
            

            return String(format:"%.2f kms away", Helper.convertMeter(toKiloMeter: distance))
            
        } else {
            
            return String(format:"%.2f miles away", Helper.convertMeter(toMiles: distance))
        }
        
    }

    
    /// Method to convert meter to miles
    ///
    /// - Parameter meter: input meter value
    /// - Returns: output miles value
    class func convertMeter(toMiles meter: Double) -> Double {
        
        return meter / 1600.0
    }
    
    
    /// Method to convert meter to kiloMeter
    ///
    /// - Parameter meter: input meter value
    /// - Returns: output kiloMeter value
    class func convertMeter(toKiloMeter meter: Double) -> Double {
        
        return meter / 1000.0
    }

    class func getTheScheduleViewTimeFormat(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        let dateString = df.string(from: date as Date)
        return dateString
    }

    class func getTheOnlyDateFromTimeStamp(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter()
        df.dateFormat = "dd MMMM yyyy"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    class func getDayAndDateFromTimeStamp(timeStamp:Int64) -> String{
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp))
        let df = DateFormatter()
        df.dateFormat = "EEEE, MMM d, yyyy"
        let dateString = df.string(from: date as Date)
        return dateString
    }
    
    class func getOnlyTimeFromTimeStamp(timeStamp:Int64) -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        let df = DateFormatter()
        df.dateFormat = "h:mm a"
        let formattedDateString: String = df.string(from: date)
        
        return formattedDateString
    }
    
    class func getTimePlusOneHr(timeStamp:Int64) -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp)).addingTimeInterval(3600)
        let df = DateFormatter()
        df.dateFormat = "h:mm a"
        let formattedDateString: String = df.string(from: date)
        print(formattedDateString)
        return formattedDateString
    }
    
    class func changeDateFormate(timeStamp: String) -> String {
        
        guard timeStamp.isEmpty == false else {
            return ""
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp)!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a" //22-Nov-2012
        let formattedDateString: String = dateFormatter.string(from: date)
        
        return formattedDateString
    }
    
//    class func getCreationTimeInt(expiryTimeStamp : Int64) -> Int64{
//        let distanceTime = Date().timeIntervalSince1970
//        return expiryTimeStamp - Int64(distanceTime)
//
//    }
    
    class func clearLaterBookingRequestDetails() {
        
        let appointmentLocaModel = AppoimtmentLocationModel.sharedInstance
        appointmentLocaModel.bookingType = .Default
        appointmentLocaModel.pickupAddress = ""
        appointmentLocaModel.pickupLatitude = 0.0
        appointmentLocaModel.pickupLongitude = 0.0

        
    }
    
    class func convertDicToString(sender:[String:AnyObject]) -> String {
             var returnString = ""
             for item in sender {
              returnString += item.key + "=" + (item.value as? String ?? "\(item.value)") + "&"
             }
             returnString.removeLast()
             print(returnString)
             return returnString
         }
    
}

//
//  SignUpAPI.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 16/11/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa

class ChatAPI:NSObject {
    
    let disposebag = DisposeBag()
    let getchat_Response = PublishSubject<APIResponseModel>()
    let chatSend_Response = PublishSubject<APIResponseModel>()
    

    func getTheChatMessages(bookingID: String){
        
        let strURL = "http://138.197.78.50:5011/chatHistory/" + bookingID
        
        
        RxAlamofire
            .requestJSON(.get, strURL,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.getChatMessages)
                }
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
    }
    
    

    func sendTheChatMessage(chatSendMessageModel: ChatMessageSendModel){
        
        var typeID = 0
        
        switch chatSendMessageModel.messageType {
            
            case .photo:
                
                typeID = 2
                break
                
            case .text:
                
                typeID = 1
                break
                
            default:
                
                typeID = 3
                break
        }
        
        
        
        let requestParams : [String : Any] = [
            
            "type": typeID,
            "timestamp":chatSendMessageModel.messageTimestamp,
            "content" :chatSendMessageModel.messageContent,
            "fromID":Utility.customerId,
            "bid":chatSendMessageModel.bookingId,
            "targetId":chatSendMessageModel.musicianId
        ]

        let strURL = "http://138.197.78.50:5011/message"
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.sendChatMessages)
                }
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
            case HTTPSResponseCodes.BadRequest.rawValue:
                
                Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
                break
                
            case HTTPSResponseCodes.InternalServerError.rawValue:
                
                Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                    
                }
                break
                
                
            default:
                
                let responseModel:APIResponseModel!
                responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
                
                switch requestType {
                    
                    case .getChatMessages:
                        
                        self.getchat_Response.onNext(responseModel)
                        
                    case .sendChatMessages:
                        
                        self.chatSend_Response.onNext(responseModel)
                    
                    default:
                        break
                }
                
                break
        }
    }

    
}


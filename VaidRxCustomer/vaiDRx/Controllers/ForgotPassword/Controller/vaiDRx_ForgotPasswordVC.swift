//
//  vaiDRx_ForgotPasswordVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class vaiDRx_ForgotPasswordVC: UIViewController {

    // MARK: - Outlets -
    //Layout Constrains
    @IBOutlet weak var loadingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var codeWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailViewHeight: NSLayoutConstraint!
    
    //Image View
    @IBOutlet weak var countryImagee: UIImageView!
    
    // Label
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    
    // text Field
    @IBOutlet weak var phonenumberTF: UITextField!
    @IBOutlet weak var emailTF: FloatLabelTextField!
    
    // View
    @IBOutlet weak var phoneNumView: UIView!
    @IBOutlet weak var emailBottomView: UIView!
    @IBOutlet weak var nextBottomView: UIViewCustom!
    @IBOutlet weak var phoneButtonBottomView: UIView!
    @IBOutlet weak var emailButtonBottomView: UIView!
    
    //Button
    @IBOutlet weak var nextButtonOutlet: UIButtonCustom!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    // MARK: - Variable Decleration -
    let catransitionAnimationClass = CatransitionAnimationClass()
    var selectedCountry: Country!
    
    var forgotPasswordViewModel = ForgotPasswordViewModel()
    let disposeBag = DisposeBag()
    var forgotPasswordWithEmail:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initiallSetup()
        phoneAction()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animateBottomView()
        initiallAnimation()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        addObserveToVariables()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addObserveToVariables() {
        
        emailTF.rx.text
            .orEmpty
            .bind(to: forgotPasswordViewModel.emailText)
            .disposed(by: disposeBag)
        
        phonenumberTF.rx.text
            .orEmpty
            .bind(to: forgotPasswordViewModel.phoneNumberText)
            .disposed(by: disposeBag)
        
    }
    
}

// MARK: - UINavigationControllerDelegate
extension vaiDRx_ForgotPasswordVC: UINavigationControllerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - CountrySelectedDelegate -
extension vaiDRx_ForgotPasswordVC: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImagee.image = UIImage(named: imagePath)
        
        countryCode.text =  self.selectedCountry.dial_code
    }
    
}

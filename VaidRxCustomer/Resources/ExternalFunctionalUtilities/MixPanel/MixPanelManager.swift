//
//  MixPanelManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 30/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Mixpanel

class MixPanelManager {

    /// Shared instance object
    static var sharedInstance = MixPanelManager()
    var eventName = ""
    
    
    /// Method to initialize Mixpanel object
    func initializeMixPanel() {
        
        Mixpanel.initialize(token: "f85f4ed5518a9980140c8ab30f034b6c")//RV-Demo:- 2a8cbc5c37aae3596c979a235f0753a3, RV-LiveM:- 7f281ac739d8b511145cbf97df66814b
        Mixpanel.mainInstance().loggingEnabled = true
        Mixpanel.mainInstance().flushInterval = 5
        
        Mixpanel.mainInstance().loggingEnabled = true
       
        Mixpanel.mainInstance().identify(distinctId: Utility.emailId)
        Mixpanel.mainInstance().people.set(properties: [
                                                        "$name": "\(Utility.firstName) \(Utility.lastName)",
                                                        "$first_name": Utility.firstName,
                                                        "$last_name": Utility.lastName,
                                                        "$email": Utility.emailId
                                                       ])

        
    }
    
    
    
    /// Method to update User Logged in event in Mixpanel
    ///
    /// - Parameter loginType: user login type
    func userLoggedInEvent(loginType:LoginType) {
        
        eventName = "Login Event!"
        
        var property = [
                        "CustomerID":Utility.customerId,
                        "Email": Utility.emailId,
                        "Date and time": Date()
                       ] as Properties
        
        if loginType == LoginType.Google {
            
            property["LoginType"] = "Google"
            
        } else if loginType == LoginType.Facebook {
            
            property["LoginType"] = "Facebook"
            
        } else {
            
            property["LoginType"] = "Normal"
        }
        
        property["DeviceId"] = Utility.deviceId
        
        Mixpanel.mainInstance().people.set(property: "DeviceType", to: "IOS")
        Mixpanel.mainInstance().track(event: eventName, properties: property)
        print("Event: \"\(eventName)\"\n Properties: \(property)")
        
    }
    
    
    /// Method to update User Register event in Mixpanel
    ///
    /// - Parameter registerType: user register type
    func userRegisteredEvent(registerType:LoginType) {
        
        eventName = "Register Event!"
        
        var property = [
            "CustomerID":Utility.customerId,
            "Email": Utility.emailId,
            "Date and time": Date()
        ] as Properties
        
        if registerType == LoginType.Google {
            
            property["RegisterType"] = "Google"
            
        } else if registerType == LoginType.Facebook {
            
            property["RegisterType"] = "Facebook"
            
        } else {
            
            property["RegisterType"] = "Normal"
        }
        
        property["DeviceId"] = Utility.deviceId
        
        Mixpanel.mainInstance().people.set(property: "DeviceType", to: "IOS")
        
        Mixpanel.mainInstance().track(event: eventName, properties: property)
        print("Event: \"\(eventName)\"\n Properties: \(property)")
        
    }
    
    
    
    /// Update Change password event in MixPanel
    func changePasswordEvent() {
        
        eventName = "Change Password!"
        
        var property = [
            "CustomerID": Utility.customerId,
            "Email": Utility.emailId,
            "Date and time": Date()
        ] as Properties
        
        property["DeviceId"] = Utility.deviceId
        
        Mixpanel.mainInstance().people.set(property: "DeviceType", to: "IOS")
        
        Mixpanel.mainInstance().track(event: eventName, properties: property)
        print("Event: \"\(eventName)\"\n Properties: \(property)")

    }
    
    
    
    /// Method to update live booking event in Mixpanel
    ///
    /// - Parameter bookingId: New booking id
    func LiveBookingEvent(bookingId:String) {
        
        eventName = "Live Booking Event!"
        
        var property = [
            "CustomerID": Utility.customerId,
            "Email": Utility.emailId,
            "Date and time": Date(),
            "BookingId":bookingId
        ] as! Properties
        
        property["DeviceId"] = Utility.deviceId
        
        Mixpanel.mainInstance().people.set(property: "DeviceType", to: "IOS")
        
        Mixpanel.mainInstance().track(event: eventName, properties: property)
        print("Event: \"\(eventName)\"\n Properties: \(property)")

    }
    
    
    
    /// Method to update booking completed event in Mixpanel
    ///
    /// - Parameter bookingId: completed booking Id
    func BookingCompletedEvent(bookingId:String,
                               rating:Float,
                               reviewComment:String) {
        
        eventName = "Booking Completed!"
        
        var property = [
            "CustomerID": Utility.customerId,
            "Email": Utility.emailId,
            "Date and time": Date(),
            "BookingId":bookingId,
            "rating":rating,
            "reviewComment":reviewComment
        ] as! Properties
        
        property["DeviceId"] = Utility.deviceId
        
        Mixpanel.mainInstance().people.set(property: "DeviceType", to: "IOS")
        
        Mixpanel.mainInstance().track(event: eventName, properties: property)
        print("Event: \"\(eventName)\"\n Properties: \(property)")

    }

    
    
    /// Method to update cancel booking event in Mixpanel
    ///
    /// - Parameter bookingId: cancelled booking booking id
    func BookingCancelledEvent(bookingId:String) {
        
        eventName = "Booking Cancelled Event!"
        
        var  property = [
            "CustomerID": Utility.customerId,
            "Email": Utility.emailId,
            "Date and time": Date(),
            "BookingId":bookingId
        ] as! Properties
        
        property["DeviceId"] = Utility.deviceId
        
        Mixpanel.mainInstance().people.set(property: "DeviceType", to: "IOS")
        
        Mixpanel.mainInstance().track(event: eventName, properties: property)
        print("Event: \"\(eventName)\"\n Properties: \(property)")
        
    }


}

//
//  PastPharmacyInfoViewModel.swift
//  vaiDRx
//
//  Created by 3Embed on 28/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class PastPharmViewModel{
    
    var rxTodayAPI = RxTodayAPI()
    var disposeBag = DisposeBag()
    var rxTodayModel:RxTodayModel!
//    var oldPharmacyAddText = Variable<String>("")
    var phoneNumberText = Variable<String>("")
    var countryCode = ""
    var check = "0"
    var oldPharmacyTransferCheck = 0
    
    var addLine1 = ""
    var city = ""
    var state = ""
    var addLine2 = ""
    var longitude : Double = 0.0
    var latitude : Double = 0.0
    var placeId = ""
    var pincode = ""
    var taggedAs = ""
    var country = ""
    var placeName = ""
    
    func rxTodayPharmacyAPICall(completion:@escaping (Int,String?,Any?) -> ()){
        createProfileRequestModel()
        rxTodayAPI.rxTodayServiceAPICall(oldPharmAdd: addLine1, phoneNumber: phoneNumberText.value,oldPharmacyCountryCode:countryCode,oldPharmacyCheck : check,oldPharmacyTransferCheck: oldPharmacyTransferCheck,rxTodayModel: rxTodayModel)
        if !rxTodayAPI.rxToday_Response.hasObservers{
            rxTodayAPI.rxToday_Response.subscribe(onNext: {(response) in
                if response.data[SERVICE_RESPONSE.Error] != nil {
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: response.data[SERVICE_RESPONSE_VAIDRX.Error] as! String)
                    return
                }
                completion(response.httpStatusCode,response.data[SERVICE_RESPONSE.ErrorMessage] as? String,response.data[SERVICE_RESPONSE.DataResponse])
            }, onError: {(error) in
                
            }).disposed(by: disposeBag)
        }
    }
    
    func createProfileRequestModel() {
        
        rxTodayModel = RxTodayModel()
        
        rxTodayModel.addLine1 = addLine1
        rxTodayModel.country = country
        rxTodayModel.city = city
        rxTodayModel.state = state
        rxTodayModel.addLine2 = addLine2
        rxTodayModel.pincode = pincode
        rxTodayModel.placeId = placeId
        rxTodayModel.longitude = longitude
        rxTodayModel.latitude = latitude
        rxTodayModel.taggedAs = taggedAs
    }
}

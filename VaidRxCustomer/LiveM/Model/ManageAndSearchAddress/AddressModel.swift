//
//  SelectedAddressModel.swift
//  LiveM
//
//  Created by Apple on 01/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GooglePlaces


/// Address Details Model
class AddressModel {
    
    var addressLine1 = ""
    var addressLine2 = ""
    var addressId = ""
    var latitude = 0.0
    var longitude = 0.0
    var zipcode = ""
    var flatNumber = ""
    var tagAddress = ""
    var city = ""
    var state = ""
    var country = ""
    var fullAddress = ""
    
    init(addressDetails:Any?) {//For Saved and Previous search addresses
        print(addressDetails)
        if let address = addressDetails as?  [String : Any]{
            
            //            addressLine1 = GenericUtility.strForObj(object:address[ADDRESS.AddressLine1])
            addressLine1 = address[ADDRESS.AddressLine1] as! String
            addressLine2 = address[ADDRESS.AddressLine2] as! String
            fullAddress = addressLine1 + ", " + addressLine2
            addressId = address[ADDRESS.AddressId] as! String
            latitude = address[ADDRESS.Latitude] as! Double
            longitude = address[ADDRESS.Longitude] as! Double
            //            flatNumber = address[ADDRESS.FlatNumber] as! String
            //            tagAddress = address[ADDRESS.TagAddress] as! String
            //            zipcode = address[ADDRESS.ZipCode] as! String
            
        }
        
    }
    
    init(manageAddressDetails:Any?) {//For manage Addresses
         print(manageAddressDetails)
        if let address = manageAddressDetails as?  [String : Any]{
            
            addressLine1 = address["addLine1"] as! String
            fullAddress = address["addLine1"] as! String
            addressLine2 = address["addLine2"] as! String
            addressId = address["_id"] as! String
            latitude = address["latitude"] as! Double
            longitude = address["longitude"] as! Double
            
            if latitude == 0 {
                
                latitude = 0.0
            }
            if longitude == 0 {
                
                longitude = 0.0
            }
            tagAddress = GenericUtility.strForObj(object: address["taggedAs"])
            zipcode = GenericUtility.strForObj(object: address["pincode"])
            
        }
        
    }
    
    class func forSearchAddress(addressDetails:NSDictionary,addressid:String,descript:String) -> [String:Any] {//For current searched addresses
        print(addressDetails)
        let addressRes = addressDetails.value(forKey: "result") as! NSDictionary
        let addressComp = addressRes.value(forKey: "address_components") as! NSArray
        let geo = addressRes.value(forKey: "geometry") as! NSDictionary
        let location = geo.value(forKey: "location") as! NSDictionary
        var addressLine1 = ""
        var addressLine2 = ""
        let latitude = location.value(forKey: "lat") as! Double
        let longitude = location.value(forKey: "lng") as! Double
        addressLine1 = descript
        for i in addressComp {
            let item = i as! NSDictionary
            if item.value(forKey: "long_name") != nil {
//                  addressLine1 = addressLine1 + "\(longName)"
                if let shortName = item.value(forKey: "short_name") {
                  addressLine2 = addressLine2 + "\(shortName)"
                }
            }
        }
        
        let addressDict = [
            ADDRESS.AddressLine1: addressLine1,
            ADDRESS.AddressLine2: addressLine2,
            ADDRESS.AddressId:addressid,
            ADDRESS.Latitude:latitude,
            ADDRESS.Longitude:longitude,
            ] as [String : Any]
        print(addressDict)
        return addressDict
    }
}

//
//  SetPasswordController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 14/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


protocol setPasswordDelegate {
    func passwordUpdated(sucess: Bool)
}

class SetPasswordController: UIViewController, AccessTokeDelegate {
    
    
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Outlets -
    
    //Views
    @IBOutlet weak var oldPassYConstrain: NSLayoutConstraint!
    @IBOutlet weak var newPassView: UIView!
    @IBOutlet weak var reenterPassView: UIView!
    @IBOutlet weak var continueBackView: UIViewCustom!
    @IBOutlet weak var oldPassView: UIView!
    
    // Label
    @IBOutlet weak var passwordText: UILabel!
    
    // Text Field
    @IBOutlet weak var newPassTF: UITextField!
    @IBOutlet weak var reEnterPassTF: UITextField!
    @IBOutlet weak var oldPasswordTF: FloatLabelTextField!
    
    // Button
    @IBOutlet weak var continueButtonOutlet: UIButtonCustom!
    
    // Layout Contstrain
    @IBOutlet weak var loadingViewHeight: NSLayoutConstraint!
    
    // MARK: - Variable Decleration -
    var forgetPass: Bool = false
    var phoneNumber: String = ""
    let catransitionAnimationClass = CatransitionAnimationClass()
    var countryCode: String = ""
    let acessClass = AccessTokenRefresh()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var apiName: Int?
    var userID: [String:Any] = [:]
    var delegate: setPasswordDelegate?
    
    var setPasswordViewModel = SetPasswordViewModel()
    let disposeBag = DisposeBag()
    
    var showCurrentPass = false
    var showNewPass = false
    var showCnfPass = false
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallSetup()
        
//        Helper.hideNavBarShadow(vc: self)
        self.passwordText.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animateBottomView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDelegate?.accessTokenDelegate = self
         appDelegate?.keyboardDelegate = self
        continueButtonOutlet.isEnabled = true
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.view?.backgroundColor = UIColor(red: 38, green: 170, blue: 167)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 38, green: 170, blue: 167)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        addObserveToVariables()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate?.accessTokenDelegate = nil
    }
    
    // MARK: - Action Methods -
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func continueButtonAction(_ sender: AnyObject) {
        if forgetPass {
            changePassword()
        }
        else{
            updatePassword()
        }
        continueButtonOutlet.isEnabled = false
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapGestureAction(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    @objc func recallApi() {
        updatePassword()
    }
    
    func addObserveToVariables() {
        
        newPassTF.rx.text
            .orEmpty
            .bind(to: setPasswordViewModel.newPasswordText)
            .disposed(by: disposeBag)
        
        reEnterPassTF.rx.text
            .orEmpty
            .bind(to: setPasswordViewModel.reEnterPasswordText)
            .disposed(by: disposeBag)
        
        oldPasswordTF.rx.text
            .orEmpty
            .bind(to: setPasswordViewModel.oldPasswordText)
            .disposed(by: disposeBag)

        
    }

    
    @IBAction func nextAction(_ sender: UIButton) {
    
        if forgetPass {
            changePassword()
        }
        else{
            updatePassword()
        }
        continueButtonOutlet.isEnabled = false
    }
    
    @IBAction func tappedShowCurrentPass(_ sender: UIButton) {
        if showCurrentPass {
            oldPasswordTF.isSecureTextEntry = true
            sender.isSelected = false
            showCurrentPass = false
        } else {
            oldPasswordTF.isSecureTextEntry = false
            sender.isSelected = true
            showCurrentPass = true
        }
    }
    
    @IBAction func tappedShowNewPass(_ sender: UIButton) {
        if showNewPass {
            newPassTF.isSecureTextEntry = true
            sender.isSelected = false
            showNewPass = false
        } else {
            newPassTF.isSecureTextEntry = false
            sender.isSelected = true
            showNewPass = true
        }
    }
    
    @IBAction func tappedShowCnfPass(_ sender: UIButton) {
        
        if showCnfPass {
            reEnterPassTF.isSecureTextEntry = true
            sender.isSelected = false
            showCnfPass = false
        } else {
            reEnterPassTF.isSecureTextEntry = false
            sender.isSelected = true
            showCnfPass = true
        }
        
    }
}


// MARK: - UINavigationControllerDelegate
extension SetPasswordController: UINavigationControllerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}



extension SetPasswordController: KeyboardDelegate {
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        //        let inputViewFrame: CGRect? = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect
        //        continueBottomConstrain.constant = (inputViewFrame?.size.height)!
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo
        let keyboardSize = (info?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.nextButtonBottomConstraint.constant = ((keyboardSize?.height) ?? 0.0 + 30.0)
                        self.view.layoutIfNeeded()
        })
    }
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        //Once keyboard disappears, restore original positions
        
        UIView.animate(withDuration: 0.4) {
            
            self.nextButtonBottomConstraint.constant = 30
            self.view.layoutIfNeeded()
        }
        
    }
    
}





//
//  RegisterModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 22/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

struct RegisterModel {
    
    var firstNameText = ""
    var lastNameText = ""
    var dobText = ""
    var emailText = ""
    var passwordText = ""
    var phoneNumberText = ""
    var dateOdBirth = ""
    var registerType = RegisterType.Default
    var facebookId = ""
    var countryCode = ""
    var profilePicURL = ""
    var cardToken     = ""
    var categoryId    = ""
    var paymentType   = PaymentType.SelfPay
    var addLine1 = ""
    var insurance = ""
    var pharmacyId = ""
    var googleId = ""
    var appVersion = ""
    var deviceOsVersion = ""
    var referralCode = ""
    var taggedAs = ""
    var state = ""
    var city = ""
    var ipAddress = ""
    var placeName = ""
    var addLine2 = ""
    var country = ""
    var placeId = ""
    var pinCode = ""
    var termsAndCond = 0
}

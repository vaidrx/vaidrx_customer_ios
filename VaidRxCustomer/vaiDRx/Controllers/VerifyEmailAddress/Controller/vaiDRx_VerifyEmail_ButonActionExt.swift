//
//  vaiDRx_VerifyEmail_ButonActionExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_VerifyEmailVC{
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        if emailTextFeild.text!.count > 5 {
            
            if Helper.isValidEmail(testStr: emailTextFeild.text!) {
                
                performSegue(withIdentifier: "toCreatePassword", sender: self)
            } else {
                
                Helper.alertVC(title: "Message", message: "Please Enter Valid Email Address")
                emailTextFeild.becomeFirstResponder()
            }
            
        } else {
            
            Helper.alertVC(title: "Message", message: "You have not entered correct email address, please check!")
            emailTextFeild.becomeFirstResponder()
        }
    }
}

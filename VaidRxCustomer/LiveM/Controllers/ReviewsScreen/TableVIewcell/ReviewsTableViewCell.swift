//
//  ReviewsTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 10/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class ReviewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet var userStarImage: [UIImageView]!
    
    @IBOutlet weak var starRating: FloatRatingView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func showReviewDetails(reviewModel: ReviewsModel) {
        
        if !reviewModel.userPicURL.isEmpty {
            
            activityIndicator.startAnimating()
            
            userImage.kf.setImage(with: URL(string: reviewModel.userPicURL),
                                     placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: { receivedSize, totalSize in
            },
                                     completionHandler: nil)
            
        } else {
            
            userImage.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }

        userNameLabel.text = reviewModel.userName
        
        commentLabel.text = reviewModel.comment
        starRating.emptyImage = #imageLiteral(resourceName: "review_star_grey_icon")
        starRating.fullImage = #imageLiteral(resourceName: "review_star_red_icon")
        starRating.floatRatings = false
        starRating.editable = false
        starRating.rating = Float(reviewModel.ratings)
        
        if reviewModel.reviewAt > 0  {
            
            let reviewDate = Date(timeIntervalSince1970: TimeInterval(reviewModel.reviewAt))
            
            let gregorianCalendar = Calendar.init(identifier: .gregorian)
            
            let components: DateComponents? = gregorianCalendar.dateComponents([.day, .month, .year], from: reviewDate, to: Date())
            
            print("components:\(String(describing: components))")
            
            if (components?.year)! > 0 {
                
                if components?.year == 1 {
                    
                    hoursLabel.text = "\((components?.year)!) year ago"
                }
                else {
                    
                    hoursLabel.text = "\((components?.year)!) years ago"
                }
            }
            else if (components?.month)! > 0 {
                
                if components?.month == 1 {
                    
                    hoursLabel.text = "\((components?.month)!) month ago"
                }
                else {
                    
                    hoursLabel.text = "\((components?.month)!) months ago"
                }
                
            }
            else {
                
                if components?.day == 0 {
                    
                    hoursLabel.text = "Today"
                }
                else if components?.day == 1 {
                    
                    hoursLabel.text = "Yesterday"
                }
                else {
                    
                    hoursLabel.text = "\((components?.day)!) days ago"
                }
            }
            
            
        } else {
            
            hoursLabel.text = "Today"
        }

        
    }
    
    
}

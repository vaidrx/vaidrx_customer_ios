//
//  CountryNameViewController.swift
//  Iserve
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit



protocol CountrySelectedDelegate {
    func countryNameSelected(countrySelected country: Country) -> Void
}


class CountryNameViewController: UIViewController {

    // MARK: - Variable declaration -
    let catransitionAnimationClass = CatransitionAnimationClass()
    var countries = [[String: String]]()                    // all country data from jason
    var countryDelegate: CountrySelectedDelegate!           // Country delegate to pass Data
    var countriesFiltered = [Country]()                     // Filterd Country data
    var countriesModel = [Country]()                        // managed data for contries
    let coutryModel = ContryNameModelClass()
    
    // MARK: - Outlets -
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    var selectedDirection: UITableView.Direction!
    
    @IBOutlet var navigationLeftButton: UIButton!
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        self.title = "Country Code"
        tableView.register(CountryTableViewCell.self, forCellReuseIdentifier: "cell")
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        selectedDirection = UITableView.Direction.rotation3D(type: .deadpool)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData(with: .simple(duration: 0.8, direction: selectedDirection, constantDelay: 0), reversed: false)
        
        
    }
    
    /// To move controller To Back Button
    ///
    /// - Parameter sender: Action
    @IBAction func backButtonAction(_ sender: Any) {
        
//        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        self.navigationController?.popViewController(animated: false)
    }

    
}
// MARK: - Custom Methods -
extension CountryNameViewController{
    /// make array of data of countries
    func collectCountries() {
        for country in countries  {
            let code = country["code"] ?? ""
            let name = country["name"] ?? ""
            let dailcode = country["dial_code"] ?? ""
            countriesModel.append(Country(country_code:code,dial_code:dailcode, country_name:name))
        }
    }
    
    /// Check If Status Bar is Active
    ///
    /// - Returns: Bool
    func checkSearchBarActive() -> Bool {
        if searchBar.isFirstResponder && searchBar.text != "" {
            return true
        }else {
            return false
        }
    }
    
    
    /// Filter Country Name
    ///
    /// - Parameter searchText: To search text in country name
    func filtercountry(_ searchText: String) {
        countriesFiltered = countriesModel.filter({(country ) -> Bool in
            let value = country.country_name.lowercased().contains(searchText.lowercased()) || country.country_code.lowercased().contains(searchText.lowercased())
            return value
        })
//         tableView.reloadData(with: .simple(duration: 0.8, direction: selectedDirection, constantDelay: 0), reversed: true)
         tableView.reloadData(with: .spring(duration: 0.8, damping: 0.75, velocity: 1, direction: selectedDirection, constantDelay: 0), reversed: false)
    }
    
    func localCountry(_ searchText: String) -> Country{
        let county = countriesModel[0]
        for contry in countriesModel {
            let nameString = contry.country_code
            if nameString == searchText {
                return contry
            }
        }
        return county
    }
    
    func localCountryName(_ searchText: String) -> Country{
        let county = countriesModel[0]
        for contry in countriesModel {
            let nameString = contry.dial_code
            if nameString == searchText {
                return contry
            }
        }
        return county
    }
    
    func initialSetUp() {
        countries = coutryModel.getCoutryDetailsFormjsonSerial()
        collectCountries()
    }

}
// MARK: - Search Bar Delegate
extension CountryNameViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filtercountry(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}

// MARK: - Table View DataSource
extension CountryNameViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if checkSearchBarActive() {
            return countriesFiltered.count
        }
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if checkSearchBarActive() {
            countryDelegate.countryNameSelected(countrySelected: countriesFiltered[indexPath.row])
        }else {
            countryDelegate.countryNameSelected(countrySelected: countriesModel[indexPath.row])
        }
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)

        self.navigationController?.popViewController(animated: false)
    }
    
}

// MARK: - Table View Delegate
extension CountryNameViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CountryTableViewCell
        let contry: Country
        if checkSearchBarActive() {
            contry = countriesFiltered[indexPath.row]
        }else{
            contry = countriesModel[indexPath.row]
        }
        cell.textLabel?.text = contry.country_name
        cell.detailTextLabel?.text = contry.dial_code
        let imagestring = contry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        cell.imageView?.image = UIImage(named: imagePath)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
   
    
}

//
//  VaidAcceptedViewController.swift
//  GoTasker
//
//  Created by 3Embed on 27/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class VaidAcceptedViewController: UIViewController {

    @IBOutlet weak var btnCancelAppointment: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnCancelAppointment.layer.cornerRadius = btnCancelAppointment.frame.height / 2

        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    

    @IBAction func cancelAppointmentAction(_ sender: UIButton) {
        /*
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "CancelAppontmentPopoverVC") as! CancelAppointmentPopOverVC
        
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

        present(popOverVC, animated: true)        //self.addChildViewController(popOverVC)
        //popOverVC.view.frame = self.view.frame
        //popOverVC.didMove(toParentViewController: self)
        */
        /*
        let popOverVC = UIStoryboard(name: "VaidRxMain", bundle: nil).instantiateViewController(withIdentifier: "CancelAppontmentPopoverVC") as! CancelAppointmentPopOverVC
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
        */
        
        
//        let popOverVC = UIStoryboard(name: "VaidRxMain", bundle: nil).instantiateViewController(withIdentifier: "CancelAppontmentPopoverVC") as! CancelAppointmentPopOverVC
//        popOverVC.modalPresentationStyle = .overCurrentContext
//        popOverVC.modalTransitionStyle = .crossDissolve
//        
//        
//        popOverVC.providesPresentationContextTransitionStyle = true
//        popOverVC.definesPresentationContext = true
//        
//        popOverVC.view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
//        
//        present(popOverVC, animated: true, completion: nil)
    }
    
}

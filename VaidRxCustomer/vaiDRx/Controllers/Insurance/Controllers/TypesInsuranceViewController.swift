//
//  TypesInsuranceViewController.swift
//  GoTasker
//
//  Created by 3Embed on 16/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

//protocol InsuranceSelectionDelegate{
//    func insuranceSeleceted(selectedInsurance : [String:Any])
//}

class TypesInsuranceViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    
    //@IBOutlet weak var textFieldBottomConstraint: NSLayoutConstraint!
    var activeTextField = UITextField()
    var numberOfRows = 4
    var heightAtIndexPath = NSMutableDictionary()
    
    var insuranceData : InsurancesResponseModel!
    var insuranceItems = Array<InsuranceItems>()
    var registerViewModel: RegisterViewModel!
    
    let insuranceTypesViewModel = InsuranceTypesViewModel()
    var selectedCellNumber = -1
    var isComminfFrom = ""
    var keyboardHeight:CGFloat!
    
//    var insuranceDelegate : InsuranceSelectionDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
//        //HelperVaidRx.showPI(_message: PROGRESS_MESSAGE_VAIDRX.Loading)
//        getInsuranceData()
//
//       self.tableView.reloadData()
//        Helper.hidePI()
        
        
         //print("insuranceData.insuranceItemsModel.count",insuranceData.insuranceItemsModel.count)
         title = "Insurance companies"
        initViews()
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    private func initViews() {
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInsuranceData()
        
        self.tableView.reloadData()
//        tableView.reloadData()
    }
    
    // MARK : - Tableview delegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = heightAtIndexPath.object(forKey: indexPath) as? NSNumber {
            
            return CGFloat(height.floatValue)
            
        } else {
            
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return insuranceItems.count+1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        let cellNo = indexPath.row
//        if cellNo == 0 {
//
//            let descCell = tableView.dequeueReusableCell(withIdentifier: "descCell") as! VRInsuranceDescCell
//            //descCell.isUserInteractionEnabled = false
//            cell = descCell
//        }
        
        //else{
            if cellNo == insuranceItems.count{
            let otherCell = tableView.dequeueReusableCell(withIdentifier: "otherCell") as! VROtherInsuranceCell
                //otherCell.tfOtherInsurance.text = "myothercell"
                //otherCell.isUserInteractionEnabled = true
                cell = otherCell
            }
    
        else {
                
                let typeCell = tableView.dequeueReusableCell(withIdentifier: "typeCell") as! VRInsuranceTypeCell
                typeCell.lbInsuranceTypeName.text = insuranceItems[indexPath.row].insuranceName //"Insurance"
                //typeCell.isUserInteractionEnabled = true
            typeCell.checkButton.setImage(UIImage(named: "check_unselected"), for: UIControl.State.normal)
                typeCell.lbInsuranceTypeName?.textColor = .white
                cell = typeCell
                
                if selectedCellNumber != -1 && indexPath.row == selectedCellNumber {
                    typeCell.lbInsuranceTypeName?.textColor = .black //Helper.UIColorFromRGB(rgbValue: 0x484848)
                    //        selectedCell.selectedCheckImage.image = UIImage(named: "uncheck_unselected")
                    typeCell.checkButton.setImage(UIImage(named: "uncheck_unselected"), for: UIControl.State.normal)
                }
        }
        //}
    
        cell.selectionStyle = .none
         return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let rowNo = indexPath.row
       if rowNo != insuranceItems.count+1{
        selectedCellNumber = indexPath.row
        tableView.reloadData()
        if let selectedCell = tableView.cellForRow(at: indexPath) as? VRInsuranceTypeCell {
            selectedCell.lbInsuranceTypeName?.textColor = .black //Helper.UIColorFromRGB(rgbValue: 0x484848)
            //        selectedCell.selectedCheckImage.image = UIImage(named: "uncheck_unselected")
            selectedCell.checkButton.setImage(UIImage(named: "uncheck_unselected"), for: UIControl.State.normal)
        }
       
         if isComminfFrom == "Profile"{
            insuranceTypesViewModel.insurance = insuranceItems[indexPath.row].customerId
            insuranceTypesViewModel.paymentType = PaymentType.Insurance
//            let selectedInsurance = ["insuranceId" : insuranceItems[indexPath.row].customerId, "paymentType" : "Insurance"]
//            self.insuranceDelegate?.insuranceSeleceted(selectedInsurance : selectedInsurance)
            UserDefaults.standard.set(insuranceItems[indexPath.row].customerId, forKey: USER_DEFAULTS.USER.INSURANCEID)
            UserDefaults.standard.set(insuranceItems[indexPath.row].insuranceName, forKey: USER_DEFAULTS.USER.INSURANCE)
            UserDefaults.standard.set("Insurance", forKey: USER_DEFAULTS.USER.PAYMENTTYPE)
            UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.PAYMENT_TYPE_CHANGED)
        //self.navigationController?.popViewController(animated: true)
       
            updateInsuranceData()
//            // Mark: - Pop two view controllers
//                                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
//                                for vc in viewControllers
//                                {
//                                    if vc.isKind(of:ProfileViewController.self)
//                                    {
//
//                                        self.navigationController!.popToViewController(vc, animated: true)
//                                        break;
//                                    }
//
//                                }
        }
        else{
            registerViewModel.insurance = insuranceItems[indexPath.row].customerId
            registerViewModel.paymentType = PaymentType.Insurance
            performSegue(withIdentifier: "toPharmacyVC", sender: self)
        }
        
        
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text! != ""{
            if isComminfFrom != "Profile" {
            registerViewModel.paymentType = PaymentType.CustomInsurance
            registerViewModel.insurance = textField.text!
            //self.navigationController?.popViewController(animated: true)
            
            performSegue(withIdentifier: "toPharmacyVC", sender: self)
            }
            else if isComminfFrom == "Profile" {
                UserDefaults.standard.set(textField.text!, forKey: USER_DEFAULTS.USER.INSURANCE)
                UserDefaults.standard.set("", forKey: USER_DEFAULTS.USER.INSURANCEID)
                UserDefaults.standard.set("Insurance", forKey: USER_DEFAULTS.USER.PAYMENTTYPE)
                UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.PAYMENT_TYPE_CHANGED)
                insuranceTypesViewModel.insurance = textField.text!
                insuranceTypesViewModel.paymentType = PaymentType.CustomInsurance
                updateInsuranceData()
                // Mark: - Pop two view controllers
//                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
//                for vc in viewControllers
//                {
//                    if vc.isKind(of:ProfileViewController.self)
//                    {
//                        
//                        self.navigationController!.popToViewController(vc, animated: true)
//                        break;
//                    }
//                    
//                }
            }
        }
        
        else{
            Helper.showAlert(head: "Select insurance or write you insurance", message: "")
        }
       
        
        return false
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        activeTextField = textField
        return true
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if segue.identifier == "toPharmacyVC" {
            
            if let dstVC = segue.destination as? PharmacyServiceYesOrNo {
                
                dstVC.registerViewModel = self.registerViewModel
            }
            
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeKeyboardObserveres()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        addKeyBoardObserver()
    }
    
    private func removeKeyboardObserveres() {
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.init(UIResponder.keyboardWillShowNotification.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.init(UIResponder.keyboardWillHideNotification.rawValue), object: nil)
    }
    
    func addKeyBoardObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: Notification.Name.init(UIResponder.keyboardWillShowNotification.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: Notification.Name.init(UIResponder.keyboardWillHideNotification.rawValue), object: nil)
    }
    /*
     @objc func keyboardWillShow(notification: NSNotification) {
     
     //Need to calculate keyboard exact size due to Apple suggestions
     var info = notification.userInfo!
     let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
     let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize!.height + 60), 0.0)
     
     self.myScrollView.contentInset = contentInsets
     self.myScrollView.scrollIndicatorInsets = contentInsets
     
     }*/
    @objc func keyBoardWillShow(notification: NSNotification) {
        
        
        if let keyBoardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]  as? NSValue)?.cgRectValue {
            keyboardHeight = keyBoardSize.height
            let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyBoardSize.height, right: 0)
            myScrollView.contentInset = contentInset
            myScrollView.scrollIndicatorInsets = contentInset
            //Scroll upto visible rect
            var visisbleRect = self.view.frame
            visisbleRect.size.height -= keyBoardSize.height
            if !visisbleRect.contains((activeTextField.center)) {
                myScrollView.scrollRectToVisible(visisbleRect, animated: true)
            }
            
            UIView.animate(withDuration: 1, animations: {
                            self.myScrollView.contentOffset = CGPoint(x: self.myScrollView.frame.origin.x, y: self.view.frame.size.height - self.keyboardHeight)
                        }, completion: nil)
            
        }
        
    }
    
    //Mark : - Called before Keyboard hides
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            self.myScrollView.contentInset = contentInsets
            self.myScrollView.scrollIndicatorInsets = contentInsets
        }
    }
    
    
    
}




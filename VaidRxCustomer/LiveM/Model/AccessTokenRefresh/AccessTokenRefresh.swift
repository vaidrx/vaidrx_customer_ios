//
//  AccessTokenRefresh.swift
//  LiveM
//
//  Created by Rahul Sharma on 31/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import RxCocoa
import RxSwift


@objc protocol AccessTokeDelegate{
    @objc func recallApi()
}

class AccessTokenRefresh {
    
    static var obj:AccessTokenRefresh? = nil

    var acessDelegate: AccessTokeDelegate? = nil
    
    let rxAccessTokenRefreshAPICall = AccessTokenRefreshAPI()
    let disposebag = DisposeBag()
    
    var isAlreadyCalled:Bool = false
    
    class func sharedInstance() -> AccessTokenRefresh {
        
        if obj == nil {
            
            obj = AccessTokenRefresh()
        }
        
        return obj!
    }

    
    
    /// Method to call access token service API
    func getAcessToken(progressMessage:String) {
        
        if !isAlreadyCalled {
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 60){
                self.checkGotResponseOrNot()
            }
            
            isAlreadyCalled = true
            
            if !rxAccessTokenRefreshAPICall.accessTokenRefresh_Response.hasObservers {
                
                rxAccessTokenRefreshAPICall.accessTokenRefresh_Response
                    .subscribe(onNext: {response in
                        
                        self.WebServiceResponse(response: response, requestType: RequestType.refressAccessToken)
                        
                    }, onError: {error in
                        
                    }).disposed(by: disposebag)
                
            }
            
            rxAccessTokenRefreshAPICall.accessTokenRefreshServiceAPICall(progressMessage:progressMessage)

        }
        
        
    }
    
    
    
    /// Method to update access token service already called or not
    func checkGotResponseOrNot() {
        
        if isAlreadyCalled {
            
            isAlreadyCalled = false
        }
    }
    
    
    //MARK - WebService Response -
    func WebServiceResponse(response:APIResponseModel, requestType:RequestType)
    {
        if (response.data[SERVICE_RESPONSE.Error] != nil) {
            
            Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
            return
        }
        
        isAlreadyCalled = false
        
        switch requestType
        {
            case RequestType.refressAccessToken:
                
                switch response.httpStatusCode {
                    
                    case HTTPSResponseCodes.SuccessResponse.rawValue:
                    
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: response.data[SERVICE_RESPONSE.DataResponse]), forKey: USER_DEFAULTS.TOKEN.SESSION)
                        
                        
                        let when = DispatchTime.now() + 0.1
                        DispatchQueue.main.asyncAfter(deadline: when){
                            self.recalApi()
                        }

                    
                        break
                    
                    case HTTPSResponseCodes.UserLoggedOut.rawValue, HTTPSResponseCodes.TokenExpired.rawValue:
                    
                        Helper.logOutMethod()
                        if let errorMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                            
                            Helper.showAlert(head: ALERTS.Message, message: errorMessage)
                            
                        }
                        break
                    
                    default:
                        
                        if let errorMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                            
                            Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                            
                        }

                        
                        break
                }
                
            default:
            
                break
        }
        
    }
    
    
    
    /// Method to recall particular API that was got session token expiry
    func recalApi(){
        
        if (self.acessDelegate != nil) {
            
            self.acessDelegate?.recallApi()
        }
    }
    
    
    
        
    
}

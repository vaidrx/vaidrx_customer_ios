//
//  NextFlashViewController.swift
//  GoTasker
//
//  Created by Rahul Sharma on 14/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class NextFlashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
//            self.performSegue(withIdentifier: "toRegisterVC", sender: self)
//        })
        // Do any additional setup after loading the view.
    }
    
   

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)

    }
    
    @IBAction func tappedNext(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toRegisterVC", sender: self)
    }
    

}

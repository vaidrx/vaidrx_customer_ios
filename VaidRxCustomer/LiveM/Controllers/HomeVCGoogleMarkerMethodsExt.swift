//
//  HomeVCGoogleMarkerMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps
import Kingfisher

extension HomeScreenViewController {
    
    //MARK: - GMSMarker Methods -
    
    /// Method to show musicians markers on mapview
    func showMarkersInMapView() {
        
        var isNewMarkerCreated = false
        
        for i in 0..<arrayOfProvidersModel.count {
            
            let providerDetail:MusicianDetailsModel = arrayOfProvidersModel[i]
            
            if dictOfMarkers[providerDetail.providerId] == nil {
                
                var view: UIView!
                
                if i == selectedProviderTag {
                    
                    view  = createCustomMarker(isSelectedMarker: true, providerDetail:providerDetail)
                    
                    addMarkerInMapView(markerView: view, markerTag:i, providerDetail: providerDetail, isSelectedMarker: true)
                    
                } else {
                    
                    view  = createCustomMarker(isSelectedMarker: false, providerDetail:providerDetail)
                    addMarkerInMapView(markerView: view, markerTag:i, providerDetail: providerDetail, isSelectedMarker: false)
                }
                
                isNewMarkerCreated = true
                
                
            } else {
                
                if i == selectedProviderTag {
                    
                    updatedProviderMarkerDetails(providerDetail: providerDetail, isSelected:true)
                    
                } else {
                    
                    updatedProviderMarkerDetails(providerDetail: providerDetail, isSelected:false)
                }
                
            }
            
        }
        
        if isNewMarkerCreated {
            
            isNewMarkerCreated = false
            showCurrentSelectedMusicianMarkerOnTop()
        }
        
    }
    
    
    
    /// Method to show selected musician marker on top when any new marker created
    func showCurrentSelectedMusicianMarkerOnTop() {
        
        for i in 0..<arrayOfProvidersModel.count {
            
            let providerDetail:MusicianDetailsModel = arrayOfProvidersModel[i]
            
            if let providerMarker = dictOfMarkers[providerDetail.providerId] as? GMSMarker {
                
                if i == selectedProviderTag {
                    
                    providerMarker.zIndex = 1
                    
                } else {
                    
                    providerMarker.zIndex = 0
                }
            }
        }
    }
    
    
    
    /// Method to create new musician marker and add to mapview
    ///
    /// - Parameters:
    ///   - markerView: musician marker view
    ///   - markerTag: unique identification to marker
    ///   - providerDetail: providerDetail model
    ///   - isSelectedMarker: currently selected musician or not
    func addMarkerInMapView(markerView:UIView, markerTag:Int, providerDetail:MusicianDetailsModel,isSelectedMarker:Bool) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            let providerMarker:GMSMarker = GMSMarker()
            providerMarker.position = CLLocationCoordinate2DMake(providerDetail.latitude,providerDetail.longitude)
            providerMarker.isTappable = true
            providerMarker.isFlat = true
            providerMarker.appearAnimation = GMSMarkerAnimation.pop
            providerMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            providerMarker.iconView = markerView
            providerMarker.map = self.mapView
            providerMarker.userData = providerDetail
            
            if isSelectedMarker {
                
                providerMarker.zIndex = 0
                
            } else {
                
                providerMarker.zIndex = 1
            }
            
            self.dictOfMarkers[providerDetail.providerId] = providerMarker
        })
        
    }
    
    
    func removeMarker(marker:GMSMarker) {
        
        //DispatchQueue.main.async(execute: {() -> Void in
        
         DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()){
            
            marker.map = self.mapView;
            marker.map = nil;
        }
        
    }
    
    
    /// Create Musician Marker view
    ///
    /// - Parameters:
    ///   - isSelectedMarker: currently selected musician or not
    ///   - providerDetail: providerDetail model
    /// - Returns: created musicina marker view
    func createCustomMarker(isSelectedMarker:Bool, providerDetail: MusicianDetailsModel) -> UIView {
        
        var markerView:UIView!
        var markerImageViewInner:UIImageView!
        var markerOnlineImageView:UIImageView!
//        var statusImageView:UIImageView!
        
        
        markerView = UIView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        markerOnlineImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        markerImageViewInner = UIImageView(frame: CGRect(x: 2, y: 2, width: 48, height: 48))
//        statusImageView = UIImageView(frame: CGRect(x: 7, y: 30, width: 13, height: 14))
        
        markerOnlineImageView.layer.cornerRadius = 52 / 2
        markerView.layer.cornerRadius = 52 / 2
        markerImageViewInner.layer.cornerRadius = 48 / 2

        markerView.backgroundColor = UIColor.clear
        
        markerView.clipsToBounds = true
        markerOnlineImageView.clipsToBounds = true
        markerImageViewInner.clipsToBounds = true

        
        if isSelectedMarker {
            
//            markerView = UIView(frame: CGRect(x: 0, y: 0, width: 65, height: 65))
//            markerOnlineImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 65, height: 65))
//            markerImageViewInner = UIImageView(frame: CGRect(x: 2, y: 2, width: 61, height: 61))
//            statusImageView = UIImageView(frame: CGRect(x: 7, y: 36, width: 19, height: 20))
            
//            markerView.layer.cornerRadius = 65 / 2
//            markerOnlineImageView.layer.cornerRadius = 65 / 2
//            markerImageViewInner.layer.cornerRadius = 61 / 2
            
            markerView.transform = CGAffineTransform(scaleX: 1.25, y:1.25)
            
//
//            if providerDetail.status == 0 {
//
//                statusImageView.image = #imageLiteral(resourceName: "offline_big")
//
//            } else {
//
//                statusImageView.image = #imageLiteral(resourceName: "online_big")
//            }
            
            
            
        } else {
            
//            markerView = UIView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
//            markerOnlineImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
//            markerImageViewInner = UIImageView(frame: CGRect(x: 2, y: 2, width: 48, height: 48))
//            statusImageView = UIImageView(frame: CGRect(x: 7, y: 30, width: 13, height: 14))
            
//            markerOnlineImageView.layer.cornerRadius = 52 / 2
//            markerView.layer.cornerRadius = 52 / 2
//            markerImageViewInner.layer.cornerRadius = 48 / 2

            
            markerView.transform = .identity
            
//            if providerDetail.status == 0 {
//
//                statusImageView.image = #imageLiteral(resourceName: "offline_small")
//
//            } else {
//
//                statusImageView.image = #imageLiteral(resourceName: "online_small")
//            }
            
        }
        
        markerImageViewInner.tag = ProviderMarkerSubViews.providerImageView
        markerOnlineImageView.tag = ProviderMarkerSubViews.providerOnlineImageView
//        statusImageView.tag = ProviderMarkerSubViews.statusImageView
        
        
        if providerDetail.status == 0 {
            
            markerOnlineImageView.image = #imageLiteral(resourceName: "offline_image")
            
        } else {
            
            markerOnlineImageView.image = #imageLiteral(resourceName: "online_image")
        }
        
        
        if providerDetail.profilePic.length > 0 {
            
            markerImageViewInner.kf.setImage(with: URL(string: providerDetail.profilePic),
                                             placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                             options: [.transition(ImageTransition.fade(1))],
                                             progressBlock: { receivedSize, totalSize in
            },
                                             completionHandler: nil)
            
        } else {
            
            markerImageViewInner.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        
        markerView.addSubview(markerOnlineImageView)
        markerView.addSubview(markerImageViewInner)
//        markerView.addSubview(statusImageView)
        
        return markerView
        
    }
    
    
    /// Update each musician details on Mapview
    ///
    /// - Parameters:
    ///   - providerDetail: providerDetail model
    ///   - isSelected: currently selected musician or not
    func updatedProviderMarkerDetails(providerDetail:MusicianDetailsModel , isSelected:Bool) {
        
        let providerMarker:GMSMarker = dictOfMarkers[providerDetail.providerId] as! GMSMarker
        var isProfileImageChanged:Bool = false
        
        if let userPrevData = providerMarker.userData as? MusicianDetailsModel {
            
            if userPrevData.profilePic.length > 0 && userPrevData.profilePic != providerDetail.profilePic {
                
                isProfileImageChanged = true
            }
        }
        
        providerMarker.userData = providerDetail
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        providerMarker.position = CLLocationCoordinate2DMake(providerDetail.latitude,providerDetail.longitude)
        CATransaction.commit()
        
        if isSelected {
            
            providerMarker.iconView?.transform = CGAffineTransform(scaleX: 1.25, y:1.25)
            providerMarker.zIndex = 1
            
        } else {
            
            providerMarker.iconView?.transform = .identity
            providerMarker.zIndex = 0
        }
        
        
        if let markerImageViewInner:UIImageView = providerMarker.iconView?.viewWithTag(ProviderMarkerSubViews.providerImageView) as? UIImageView {
            
            if isProfileImageChanged {
                
                markerImageViewInner.kf.setImage(with: URL(string: providerDetail.profilePic),
                                                 placeholder:nil,
                                                 options: [.transition(ImageTransition.fade(1))],
                                                 progressBlock: { receivedSize, totalSize in
                },
                                                 completionHandler: nil)
                
            }
            
            
        }
        
        if let markerOnlineImageView:UIImageView = providerMarker.iconView?.viewWithTag(ProviderMarkerSubViews.providerOnlineImageView) as? UIImageView {
            
            if providerDetail.status == 0 {
                
                markerOnlineImageView.image = #imageLiteral(resourceName: "offline_image")
                
            } else {
                
                markerOnlineImageView.image = #imageLiteral(resourceName: "online_image")
            }
            
        }
        
        
        if let statusImageView:UIImageView = providerMarker.iconView?.viewWithTag(ProviderMarkerSubViews.statusImageView) as? UIImageView {
            
            if isSelected {
                
//                if providerDetail.status == 0 {
//
//                    statusImageView.image = #imageLiteral(resourceName: "offline_big")
//
//                } else {
//
//                    statusImageView.image = #imageLiteral(resourceName: "online_big")
//                }
                
            } else {
                
//                if providerDetail.status == 0 {
//
//                    statusImageView.image = #imageLiteral(resourceName: "offline_small")
//
//                } else {
//
//                    statusImageView.image = #imageLiteral(resourceName: "online_small")
//                }
                
            }
            
        }
        
    }
    
    
    /// select Marker in MapView
    ///
    /// - Parameter markerTag: Selected Marker Tag Number
    /// - prevMarkerTag: Previous Selected Marker Tag Number
    
    func selectMarkerInMapView(markerTag:Int, prevMarkerTag:Int) {
        
        if markerTag >= 0 && prevMarkerTag >= 0 && arrayOfProvidersModel.count > 0 {
            
            let prevMarkerProviderDetail:MusicianDetailsModel = arrayOfProvidersModel[prevMarkerTag]
            let providerDetail:MusicianDetailsModel = arrayOfProvidersModel[markerTag]
            
            DispatchQueue.main.async(execute: {() -> Void in
                
                if let marker:GMSMarker = self.dictOfMarkers[prevMarkerProviderDetail.providerId] as? GMSMarker {
                    
                    marker.iconView?.transform = .identity
                    marker.zIndex = 0
                    
                    
                    if let markerOnlineImageView:UIImageView = marker.iconView?.viewWithTag(ProviderMarkerSubViews.providerOnlineImageView) as? UIImageView {
                        
                        
                        if prevMarkerProviderDetail.status == 0 {
                            
                            markerOnlineImageView.image = #imageLiteral(resourceName: "offline_image")
                            
                        } else {
                            
                            markerOnlineImageView.image = #imageLiteral(resourceName: "online_image")
                        }
                        
                    }
                    
                    if let statusImageView:UIImageView = marker.iconView?.viewWithTag(ProviderMarkerSubViews.statusImageView) as? UIImageView {
                        
//                        if prevMarkerProviderDetail.status == 0 {
//
//                            statusImageView.image = #imageLiteral(resourceName: "offline_small")
//
//                        } else {
//
//                            statusImageView.image = #imageLiteral(resourceName: "online_small")
//                        }
                        
                    }
                    
                }
                
                if let marker:GMSMarker = self.dictOfMarkers[providerDetail.providerId] as? GMSMarker {
                    
                    marker.iconView?.transform = CGAffineTransform(scaleX: 1.25, y:1.25)
                    
                    marker.zIndex = 1
                    
                    if let markerOnlineImageView:UIImageView = marker.iconView?.viewWithTag(ProviderMarkerSubViews.providerOnlineImageView) as? UIImageView {
                        
                        if providerDetail.status == 0 {
                            
                            markerOnlineImageView.image = #imageLiteral(resourceName: "offline_image")
                            
                        } else {
                            
                            markerOnlineImageView.image = #imageLiteral(resourceName: "online_image")
                        }
                        
                    }
                    
                    
//                    if let statusImageView:UIImageView = marker.iconView?.viewWithTag(ProviderMarkerSubViews.statusImageView) as? UIImageView {
//
//                        if providerDetail.status == 0 {
//
//                            statusImageView.image = #imageLiteral(resourceName: "offline_big")
//
//                        } else {
//
//                            statusImageView.image = #imageLiteral(resourceName: "online_big")
//                        }
//
//                    }
                    
                }
                
                self.prevSelectedMarkerTag = self.selectedProviderTag
            })
            
        }

            
    }
        
        
}

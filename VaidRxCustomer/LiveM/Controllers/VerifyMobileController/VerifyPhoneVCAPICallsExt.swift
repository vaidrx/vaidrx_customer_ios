//
//  VerifyPhoneAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension VerifyMobileViewController {
    
    //MARK: - WebService Call -
    func resendOTPToMobileNumber() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        verifyPhoneViewModel.userId = userID[SERVICE_RESPONSE.Sid] as! String
        
        if isCommingFrom == "Register" {
            
            verifyPhoneViewModel.triggerValue = 1
            
        }else if isCommingFrom == "ChangeEmailPhone" {
            
            verifyPhoneViewModel.triggerValue = 3
            
        }else {
            
            verifyPhoneViewModel.triggerValue = 2
        }
        
        verifyPhoneViewModel.resendOTPAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.signUpOTP)
        }
        
    }
    

    func signUpVerifyOTP(code:String) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        verifyPhoneViewModel.codeText = code
        verifyPhoneViewModel.userId = userID[SERVICE_RESPONSE.Sid] as! String
        
        verifyPhoneViewModel.registerVerifyOTPAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.verifyOTP)
        }

    }
    
    func forgotPasswordVerifyOTP(code:String) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        verifyPhoneViewModel.codeText = code
        verifyPhoneViewModel.userId = userID[SERVICE_RESPONSE.Sid] as? String ?? ""
        
        if isCommingFrom == "ChangeEmailPhone" {
            
            verifyPhoneViewModel.triggerValue = 3
            
        }else {
            
            verifyPhoneViewModel.triggerValue = 2
            
        }
        
        verifyPhoneViewModel.forgotPasswordVerifyOTPAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.forgotPasswordVerifyOTP)
        }

    }
    
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.signUpOTP:
                addTimer()
                
            case RequestType.verifyOTP, RequestType.forgotPasswordVerifyOTP://Verify OTP Success Response
                
                if isCommingFrom == "Register" {
                    
                    if let userDataResponse = dataResponse as? [String:Any] {
                        
                        self.showSplashLoading()
                        
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.Sid]), forKey: USER_DEFAULTS.USER.CUSTOMERID)
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.FirstName]), forKey: USER_DEFAULTS.USER.FIRSTNAME)
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.LastName]), forKey: USER_DEFAULTS.USER.LASTNAME)
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.SessionToken]), forKey: USER_DEFAULTS.TOKEN.SESSION)
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.ReferralCode]), forKey: USER_DEFAULTS.USER.REFERRAL_CODE)
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.Email]), forKey: USER_DEFAULTS.USER.EMAIL)
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.CurrencyCode]), forKey: USER_DEFAULTS.USER.CURRENCYSYMBOL)
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.ProfilePic]), forKey: USER_DEFAULTS.USER.PIC)
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.Token]), forKey: USER_DEFAULTS.TOKEN.ACCESS)
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.ZenDeskRequesterID]), forKey: USER_DEFAULTS.USER.ZenDeskRequesterID)

                        
                        
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.StripeAPIKey]), forKey: USER_DEFAULTS.PAYMENT_GATEWAY.API_KEY)
                        
                        AppDelegate().defaults.synchronize()
                        
                        //Connect to MQTT
                        AppDelegate().connectToMQTT()
                        
                        ConfigManager.sharedInstance.getConfigurationDetails()
                        MQTTOnDemandAppManager.sharedInstance().subScribeToInitialTopics()
                        
                        //Initialize Coouch DB
                        CouchDBManager.sharedInstance.createCouchDB()
                        
                        MixPanelManager.sharedInstance.initializeMixPanel()
                        
                        MixPanelManager.sharedInstance.userRegisteredEvent(registerType: userLoginType)
                        
                        //Goto HomeVC
                        let menu = HelperLiveM.sharedInstance
                        menu.createMenuView()

                    }
                    
                }else if isCommingFrom == "ChangeEmailPhone" {
                    
                    delegate?.didUpdateMobileNumber()
                    var viewControllers = navigationController?.viewControllers
                    viewControllers?.removeLast(2) // views to pop
                    self.navigationController?.setViewControllers(viewControllers!, animated: true)
                    
                }
                else{
                    
                    let params = [ SERVICE_RESPONSE.Sid : GenericUtility.strForObj(object: userID[SERVICE_RESPONSE.Sid])]
                    self.performSegue(withIdentifier: SEgueIdetifiers.verMobileToSetPass, sender: params)
                }
                
                
            case RequestType.signUp:
                
                if let userDataResponse = dataResponse as? [String:Any] {
                    
                    self.showSplashLoading()
                    
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.Sid]), forKey: USER_DEFAULTS.USER.CUSTOMERID)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.FirstName]), forKey: USER_DEFAULTS.USER.FIRSTNAME)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.LastName]), forKey: USER_DEFAULTS.USER.LASTNAME)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.SessionToken]), forKey: USER_DEFAULTS.TOKEN.SESSION)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.ReferralCode]), forKey: USER_DEFAULTS.USER.REFERRAL_CODE)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.Email]), forKey: USER_DEFAULTS.USER.EMAIL)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.CurrencyCode]), forKey: USER_DEFAULTS.USER.CURRENCYSYMBOL)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.ProfilePic]), forKey: USER_DEFAULTS.USER.PIC)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.Token]), forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.StripeAPIKey]), forKey: USER_DEFAULTS.PAYMENT_GATEWAY.API_KEY)
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: userDataResponse[SERVICE_RESPONSE.ZenDeskRequesterID]), forKey: USER_DEFAULTS.USER.ZenDeskRequesterID)

                    
                    AppDelegate().defaults.synchronize()
                    
                    
                    //Initialize Coouch DB
                    CouchDBManager.sharedInstance.createCouchDB()
                    
                    //Connect to MQTT
                    AppDelegate().connectToMQTT()
                    
                    ConfigManager.sharedInstance.getConfigurationDetails()
                    MQTTOnDemandAppManager.sharedInstance().subScribeToInitialTopics()
                    
                    MixPanelManager.sharedInstance.initializeMixPanel()
                    
                    MixPanelManager.sharedInstance.userRegisteredEvent(registerType: userLoginType)
                    
                    //Goto HomeVC
                    let menu = HelperLiveM.sharedInstance
                    menu.createMenuView()
                    
                }
                
            case RequestType.forgetPassword:
                
                if let dataRes = dataResponse as? [String:Any] {
                    
                    let params = [ SERVICE_RESPONSE.Sid : GenericUtility.strForObj(object: dataRes[SERVICE_RESPONSE.Sid])]
                    userID = params

                }
                
                
                break
                
                
                default:
                    break
            }
            
            break
            
        default:
            
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
            }
            
            switch requestType {
                
                case RequestType.verifyOTP, RequestType.forgotPasswordVerifyOTP:
                    code4.becomeFirstResponder()
                
                
                default:
                    break
            }
            
            break
            
        }
        
    }
    
    //MARK: - Splash Loading -
    func showSplashLoading() {
        
        Helper.showSplashLoading()
        
        self.perform(#selector(self.closeSplashLoading), with: nil, afterDelay: 45)
        
    }
    
    @objc func closeSplashLoading(){
        
        Helper.closeSplashLoading()
    }
    
    
}

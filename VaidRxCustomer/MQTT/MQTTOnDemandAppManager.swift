//
//  MQTTOnDemandAppManager.swift
//  LiveM
//
//  Created by Raghavendra V on 07/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import MQTTClient
import Foundation
import FirebaseMessaging

class MQTTOnDemandAppManager {
    
    
    static var obj:MQTTOnDemandAppManager? = nil
    
    var window: UIWindow?
    
    class func sharedInstance() -> MQTTOnDemandAppManager  {
        
        if obj == nil {
            
            obj  = MQTTOnDemandAppManager()
        }
        
        return obj!
    }
    
    
    
    /// Method to subscribe all topics
    func subScribeToInitialTopics() {
        
        Messaging.messaging().subscribe(toTopic: "\(MQTT_TOPIC.FCMTopic)" + "\(Utility.fcmTopic)")
        subScribeToFCMTopics()
        
        if MQTT.sharedInstance().isConnected && Utility.customerId.length > 0 {
            
            
             MQTT.sharedInstance().subscribeChannel(withChannelName: "customer/patchAcceptBooking/" + "\(Utility.customerId)")
            
            MQTT.sharedInstance().subscribeChannel(withChannelName: "\(MQTT_TOPIC.ListAllProviders)" + "\(Utility.customerId)")
            
            MQTT.sharedInstance().subscribeChannel(withChannelName: "\(MQTT_TOPIC.ChatMessageTopic)" + "\(Utility.customerId)")
            
            MQTT.sharedInstance().subscribeChannel(withChannelName: "\(MQTT_TOPIC.GetJobStatus)" + "\(Utility.customerId)")
            
            
            if Helper.getCurrentVC() is LiveTrackViewController {
                
                LiveTrackViewController.sharedInstance().subscribeToProviderTopic()
            }
            
        }
        
    }
    
    
    /// Method to subscribe all FCM Topics
    func subScribeToFCMTopics() {
        
//        if MQTT.sharedInstance().isConnected && Utility.customerId.length > 0 {
        
            let fcmTopics = ConfigManager.sharedInstance.fcmTopicsToSubscribe
            
            if let allCitiesCustomers = fcmTopics["allCitiesCustomers"] as? String {
                
                Messaging.messaging().subscribe(toTopic: allCitiesCustomers)
            }
            
            if let allCustomers = fcmTopics["allCustomers"] as? String {
                
                Messaging.messaging().subscribe(toTopic: allCustomers)
            }
            
            if let outZoneCustomers = fcmTopics["outZoneCustomers"] as? String {
                
                Messaging.messaging().subscribe(toTopic: outZoneCustomers)
            }
            
            if let city = fcmTopics["city"] as? String {
                
                Messaging.messaging().subscribe(toTopic: city)
            }
//        }
        
    }
    
    /// Method to unsubscribe all FCM Topics
    func unSubScribeToFCMTopics() {
        
//        if MQTT.sharedInstance().isConnected {
        
            let fcmTopics = ConfigManager.sharedInstance.fcmTopicsToSubscribe
            
            if let allCitiesCustomers = fcmTopics["allCitiesCustomers"] as? String {
                
                Messaging.messaging().unsubscribe(fromTopic: allCitiesCustomers)
            }
            
            if let allCustomers = fcmTopics["allCustomers"] as? String {
                
                Messaging.messaging().unsubscribe(fromTopic: allCustomers)
            }
            
            if let outZoneCustomers = fcmTopics["outZoneCustomers"] as? String {
                
                Messaging.messaging().unsubscribe(fromTopic: outZoneCustomers)
            }
            
            if let city = fcmTopics["city"] as? String {
                
                Messaging.messaging().unsubscribe(fromTopic: city)
            }
//        }
        
    }
    

    /// Method to unsubscribe all Initial Topics
    func unSubScribeToInitialTopics() {
        
        Messaging.messaging().unsubscribe(fromTopic: "\(MQTT_TOPIC.FCMTopic)" + "\(Utility.fcmTopic)")

        unSubScribeToFCMTopics()
        
        if MQTT.sharedInstance().isConnected {
            MQTT.sharedInstance().unsubscribeTopic(topic: "customer/patchAcceptBooking/" + "\(Utility.customerId)")
            MQTT.sharedInstance().unsubscribeTopic(topic: "\(MQTT_TOPIC.ListAllProviders)" + "\(Utility.customerId)")
            
            MQTT.sharedInstance().unsubscribeTopic(topic: "\(MQTT_TOPIC.ChatMessageTopic)" + "\(Utility.customerId)")
            
            MQTT.sharedInstance().unsubscribeTopic(topic: "\(MQTT_TOPIC.GetJobStatus)" + "\(Utility.customerId)")
            
        }
        
    }
    
    
    /// Got New Message From MQTT Server
    ///
    /// - Parameters:
    ///   - data: consists of message Information
    ///   - topic: name of the topic
    func getNewMessage(withData data: [String: Any],in topic: String) {
        
        if Utility.sessionToken.length > 0 {
        
            if let errFlag = data["errFlag"] as? Int {
                
                if errFlag == 1 {//Checking got any error or not
                    
                    if let errNum = data["errNum"] as? Int {
                        
                        if errNum == 404 {
                            
                            if let errMsg = data["errMsg"] as? String {
                                
                                if topic.range(of:MQTT_TOPIC.ListAllProviders) != nil { //Parsing messages by topic nam
                                    MusiciansListManager.sharedInstance().noMusiciansResponse(message: errMsg)
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                    return
                    
                } else {
                    
                    if  data["data"] != nil { //Checking got data response
                        
                        if let responseDetails =  data["data"]  as? [String:Any] {
                            print(responseDetails)
                            print(responseDetails["msg"])
                            if responseDetails["msg"] as? String == "please wait for few minutes." {
                                BlockerAlert.instance.show()
                                    self.createMenuView()
                                
                               
                                
                                
//                                Helper.showAlert(head: "Vaid Accepted", message: "\(responseDetails["msg"] ?? "")")
                                                                             
                                                   }
                        }
                       
                        if topic.range(of:MQTT_TOPIC.ListAllProviders) != nil { //Parsing messages by topic nam
                            
                            if let providersArray:[Any] = data["data"] as? [Any] {
                                
                                MusiciansListManager.sharedInstance().recievedListOfMusiciansFromMQTT(listOfNewMusicians: providersArray, listOfCurrentMusicians: MusiciansListManager.sharedInstance().arrayOfMusicians)
                                
                            }
                            
                        } else if topic.range(of:MQTT_TOPIC.GetJobStatus) != nil {
                            
                            if let bookingStatusResponse = data["data"] as? [String:Any] {
                                print(data["data"])
                                
                                if data["statusMsg"] as? String == "Cancel" {
                                    
                                    return
                                }
                                
                                let bookingStatusModel = BookingStatusResponseModel.init(bookingStatusUpdateMessage: bookingStatusResponse)
                                
                                
                                BookingStatusResponseManager.sharedInstance().bookingStatusMessageFromPushOrMQTT(isFromPush: false, bookingStatusModel: bookingStatusModel)
                                
                            }
                            
                        } else if topic.range(of:MQTT_TOPIC.ProviderLiveTrack) != nil {
                            
                            LiveTrackResponseManager.sharedInstance().liveTrackMessageFromMQTT(liveTrackMessage: data, topicName:topic)
                            
                        } else if topic.range(of:MQTT_TOPIC.ChatMessageTopic) != nil {
                            
                            MQTTSimpleSingleChatManager.sharedInstance().chatMessageFromPushOrMQTT(chatMessage: data)
                        }
                        
                    }
                }
                
            } else if topic.range(of:MQTT_TOPIC.ProviderLiveTrack) != nil {
            
                if let latitude:Double = data["latitude"] as? Double {
                    
                    print(latitude)
                    LiveTrackResponseManager.sharedInstance().liveTrackMessageFromMQTT(liveTrackMessage: data, topicName:topic)
                }
                
            } else if topic.range(of:MQTT_TOPIC.FCMTopic) != nil  {
                
                
            } else if topic.range(of:MQTT_TOPIC.ChatMessageTopic) != nil {
                
                if let chatMessageResponse = data["data"] as? [String:Any] { //Checking got data response
                    
                    MQTTSimpleSingleChatManager.sharedInstance().chatMessageFromPushOrMQTT(chatMessage: chatMessageResponse)
                }
            }
        }

    }
    
    func MQTTServerisConnected() {
        
//        ListAllProvidersMQTTModel.sharedInstance().subscribeAllProviders()
    }
    
    func createMenuView(){
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeScreenViewController = storyboard.instantiateViewController(withIdentifier: "VaidHomeVc") as! VrHomeViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: homeScreenViewController)
        nvc.navigationBar.isTranslucent = true
        
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuTableVC") as! LeftMenuTableViewController
        
        leftViewController.homeScreenViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        
        WINDOW_DELEGATE??.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        WINDOW_DELEGATE??.rootViewController = slideMenuController
        WINDOW_DELEGATE??.makeKeyAndVisible()
    }

    
}

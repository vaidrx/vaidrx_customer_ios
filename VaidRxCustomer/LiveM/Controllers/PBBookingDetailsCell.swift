//
//  PBBookingDetailsCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class PBBookingDetailsCell:UITableViewCell {
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var valueLabel: UILabel!
    
    @IBOutlet var divider: UIView!
    
}

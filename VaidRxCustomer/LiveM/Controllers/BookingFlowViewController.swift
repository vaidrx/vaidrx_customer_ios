//
//  BookingFlowViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import MessageUI

class BookingFlowViewController:UIViewController {
    
    @IBOutlet var navigationLeftButton: UIButton!
    
    @IBOutlet var cancelButton: UIButton!
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var gigNameLabel: UILabel!
    
    @IBOutlet var gigValueLabel: UILabel!
    
    @IBOutlet var serviceFeeLabel: UILabel!
    
    @IBOutlet var serviceFeeValueLabel: UILabel!
    
    @IBOutlet var discountLabel: UILabel!
    
    @IBOutlet var discountValueLabel: UILabel!
    
    @IBOutlet var totalLabel: UILabel!
    
    @IBOutlet var totalValueLabel: UILabel!
    
    @IBOutlet var timerBackView: UIView!
    
    @IBOutlet var timerGigTimeLabel: UILabel!
    
    @IBOutlet var timerLabel: UILabel!
    
    @IBOutlet var pausedLabel: UILabel!
    
    @IBOutlet var extendTimeButton: UIButton!
    
    @IBOutlet var timerBackViewTopConstraint: NSLayoutConstraint!
    
    
    
    
    
    var bookingDetailModel:BookingDetailsModel!
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!
    
    var bookingId:Int64!
    var bookingStatus:Int!
    
    var cancelBookingScreen:CancelBookingScreen!
    
    var remainingSeconds = 0
    var bookingTimer = Timer()
    var isComeFromVirtualCall = ""
    
    let bookingFlowViewModel = BookingFlowViewModel()
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!

    // Shared instance object for gettting the singleton object
    static var obj:BookingFlowViewController? = nil
    
    class func sharedInstance() -> BookingFlowViewController {
        
        return obj!
    }
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BookingFlowViewController.obj = self
        
        getBookingDetailsAPI()
        
        self.title = "\(ALERTS.BOOKING_FLOW.EventID)" + " : " + String(bookingId) //String(format:"%td", bookingId)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        acessClass.acessDelegate = self
       
        setupGestureRecognizer()
        
        checkAndClearBookingFlowLocalNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        bookingTimer.invalidate()
        acessClass.acessDelegate = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkAndClearBookingFlowLocalNotifications() {
        
        if LocalNotificationView.share != nil {
            
            if let localNotificationView = LocalNotificationView.share {
                
                if localNotificationView.bookingStatusModel.bookingId == bookingId {
                    
                    localNotificationView.closeButtonAction(localNotificationView.closeButton)
                }
            }
            
        }
    }
    
    
    //MARK: - UIButton Actions -

    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
       
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.popToRootViewController(animated: false)
        
        UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.GoToUpcomingBookings)
        UserDefaults.standard.synchronize()

        LeftMenuTableViewController.sharedInstance().changeViewController(LeftMenu.myEvent)
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        cancelBookingScreen = CancelBookingScreen.sharedInstance
        
        cancelBookingScreen.bookingID = bookingDetailModel.bookingId
        
        WINDOW_DELEGATE??.addSubview(cancelBookingScreen)
        
        cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { (finished) in
            
            self.cancelBookingScreen.getCancelReasonsAPI()
            
        }

    
    }
    
    @IBAction func callButtonAction(_ sender: Any) {
        
        if let phoneCallURL = URL(string: "tel://\(bookingDetailModel.providerPhoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            
            if (application.canOpenURL(phoneCallURL)) {
                
                application.openURL(phoneCallURL)
                
            } else {
                
                Helper.showAlert(head: ALERTS.Missing, message:ALERTS.MissingCallFeature)
            }
        } else {
            
            Helper.showAlert(head: ALERTS.Missing, message:ALERTS.MissingCallFeature)
        }

    }
    
    @IBAction func messageButtonAction(_ sender: Any) {
        
        /*let textMessageRecipients = [bookingDetailModel.providerPhoneNumber]
       
        if (MFMessageComposeViewController.canSendText()) {
           
            // Present the configured MFMessageComposeViewController instance
            // Note that the dismissal of the VC will be handled by the messageComposer instance,
            // since it implements the appropriate delegate call-back
            
            let messageComposeVC = MFMessageComposeViewController()
            
            
            messageComposeVC.messageComposeDelegate = self  //  Make sure to set this property to self, so that the controller can be dismissed!
            messageComposeVC.recipients = textMessageRecipients
            messageComposeVC.body = ""
            
            present(messageComposeVC, animated: true, completion: nil)
            
        } else {
            
            // Let the user know if his/her device isn't able to send text messages
            Helper.showAlert(head: ALERTS.Missing, message:ALERTS.MissingMessageFeature)
        }*/
        
        let chatVC:ChatViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.chatVC) as! ChatViewController
        
        
        chatVC.bookingId = String(bookingDetailModel.bookingId)
        chatVC.musicianId = bookingDetailModel.providerId
        chatVC.musicianName = bookingDetailModel.providerName
        chatVC.musicianImageURL = bookingDetailModel.providerImageURL
        
        
        
        self.navigationController?.pushViewController(chatVC, animated: false)
        
    }
    
    @IBAction func liveTrackButtonAction(_ sender: Any) {
        
        let liveTrackVC:LiveTrackViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.liveTrackVC) as! LiveTrackViewController
        
        
        liveTrackVC.bookingDetailModel = bookingDetailModel
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)

        
        self.navigationController!.pushViewController(liveTrackVC, animated: false)
    }
    
    
    @IBAction func extendTimeButtonAction(_ sender: Any) {
        
        
        
    }
    
}

extension BookingFlowViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getParticularApptDetails.rawValue:
            
            getBookingDetailsAPI()
            
            break
            
        default:
            break
        }
    }
    
}

extension BookingFlowViewController:MFMessageComposeViewControllerDelegate {

    // MFMessageComposeViewControllerDelegate callback - dismisses the view controller when the user is finished with it
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        controller.dismiss(animated: true, completion: nil)
    }
}

extension BookingFlowViewController {
    
    func showBookingDetails() {
        
        gigNameLabel.text = String(format:"%@ %@", GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["name"]), GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["unit"]))
        
        gigValueLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol ,Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))!)
        
        
        discountValueLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol, bookingDetailModel.discountValue)
        
        bookingStatus = bookingDetailModel.bookingStatus
        
        let totalValue =  (Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))! + bookingDetailModel.serviceFee) - bookingDetailModel.discountValue
        
        totalValueLabel.text = bookingDetailModel.currencySymbol + String(format:" %.2f",totalValue)
        
        if bookingDetailModel.bookingStatus > 6 {
            
            cancelButton.isHidden = true
            
        } else {
            
            cancelButton.isHidden = false
        }
        
        if bookingDetailModel.bookingStatus == Booking_Status.Started.rawValue {
            
            showBookingTimerDetails()
            
        } else {
            
            hideBookingTimerDetailsViewAnimation()
        }
        
        self.tableView.reloadData()
        
        self.scrollTableViewToShowCusrrentBookingStatus()
    }
}


extension BookingFlowViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension BookingFlowViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.GoToUpcomingBookings)
            UserDefaults.standard.synchronize()
            self.navigationController?.popViewController(animated: true)
            
//            self.navigationLeftButtonAction(self.navigationLeftButton)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
}

//
//  LeftMenuTableViewCell.swift
//  Iserve
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {

    // MARK: - Outlets -
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var manageAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    open class func height() -> CGFloat {
        return 50
    }
    
    open func setData(_ data: Any? , image: Any?) {
        self.backgroundColor = UIColor.clear
        if let menuText = data as? String {
            self.manageAddressLabel?.text = menuText
            self.leftImage.image = UIImage(named: image as! String)
        }
    }
    
    override open func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            self.alpha = 0.4
        } else {
            self.alpha = 1.0
        }
    }
    

}

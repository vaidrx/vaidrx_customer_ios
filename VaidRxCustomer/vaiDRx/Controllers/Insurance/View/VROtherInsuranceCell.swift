//
//  VROtherInsuranceCell.swift
//  GoTasker
//
//  Created by 3Embed on 16/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class VROtherInsuranceCell: UITableViewCell {

    @IBOutlet weak var otherInsuranceTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        otherInsuranceTextField.attributedPlaceholder = NSAttributedString(string: "Other",
                                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}

//
//  HomeListVCButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension HomeListViewController {
    
    // MARK: - Action Meyhods -
    @IBAction func mapAction(_ sender: Any) {
        
        TransitionAnimationWrapperClass.flipFromRightAnimation(view: (self.navigationController?.view)!)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    
    @IBAction func ProfileImageButtonAction(_ sender: Any) {
        let leftMenu = slideMenuController()
        leftMenu?.openLeft()
    }
    
    
    @IBAction func searchAction(_ sender: Any) {
        
        let searchLocationVC:SearchLocationViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.searchLocationVC) as! SearchLocationViewController
        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                  subType: kCATransitionFromTop,
//                                                                  for: (self.navigationController?.view)!,
//                                                                  timeDuration: 0.3)
//        
        searchLocationVC.delegate = self
        
        self.navigationController?.pushViewController(searchLocationVC, animated: false)
        
    }
    
    @IBAction func datePickerAction(_ sender: Any) {
        
        datePickerView.date = selectedDate
        
        self.datePickerBackBottomConstraint.constant = 0
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
        
    }
    
    
   
    @IBAction func datePickerSaveButtonAction(_ sender: Any) {
        
        selectedDate = datePickerView.date
        
        appointmentLocationModel.bookingType = BookingType.Schedule
        appointmentLocationModel.scheduleDate = selectedDate
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        musiciansListManager.publish(isIinitial: false)
        
        if self.selectedBookLaterBackView.isHidden == true {
            
            showSelectedBookLaterDetails()
        }
        
        updateSelectedBookLaterDetails()
        
        self.datePickerBackBottomConstraint.constant = -600
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
        
    }
    
    @IBAction func datePickerCancelButtonAction(_ sender: Any) {
        
        self.datePickerBackBottomConstraint.constant = -600
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
        
        
    }
    
    @IBAction func selectedBookLaterCloseButtonAction(_ sender: Any) {
        
        selectedEventStartTag = 0
        selectedDate = scheduleDate
        
        appointmentLocationModel.bookingType = BookingType.Default
        appointmentLocationModel.selectedEventStartTag = selectedEventStartTag
        
        self.eventStartCollectionView.reloadData()
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        musiciansListManager.publish(isIinitial: false)
        
        hideSelectedBookLaterDetails()
    }


}

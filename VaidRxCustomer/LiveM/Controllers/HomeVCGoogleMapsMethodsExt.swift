//
//  HomeVCGoogleMapsMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps

struct ProviderMarkerSubViews {
    
    static let providerImageView = 100
    static let providerOnlineImageView = 200
    static let statusImageView = 300
    
}


extension HomeScreenViewController:GMSMapViewDelegate {
    
    
    /// Set Intial MapView Properties
    func setInitialMapViewProperties() {
        
        self.mapView.isMyLocationEnabled = true
        
        if locationObj.latitute != 0.0 {
            
            currentLat = locationObj.latitute
            currentLong = locationObj.longitude
            loadedLocationLat = currentLat
            loadedLocationLong = currentLong
            appointmentLocationModel.pickupLatitude = locationObj.latitute
            appointmentLocationModel.pickupLongitude = locationObj.longitude
            
            addressLabel.text = locationObj.address
            
            let camera = GMSCameraUpdate.setTarget(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(locationObj.latitute), longitude: CLLocationDegrees(locationObj.longitude)), zoom: MAP_ZOOM_LEVEL)
            
            self.mapView.animate(with: camera)

        }
        
        mapPinBackView.layoutIfNeeded()
        
        
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        
    }
    
    
    // MARK: - GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
//        hideHomeScreenMapDraggingAnimation()
        
        if isAddressManuallyPicked {//Checking Address Picked Manually (Get Address From Search Address Controller)
            
            isAddressManuallyPicked = false
            
        } else if isFocusedMarkerPicked {
            
            if position.target.latitude != 0.0 {
                
                getLocationDetails(position.target)
            }
            
        } else  {
            
            if position.target.latitude != 0.0 {
                
                getLocationDetails(position.target)
            }
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
//        let musicianDetailsVC:MusicianDetailsViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.musicianDetailsVC) as! MusicianDetailsViewController
//
//        musicianDetailsVC.providerDetailFromPrevController = marker.userData as! MusicianDetailsModel
//        savePickupLocationDetails()
//
//
//        self.navigationController!.pushViewController(musicianDetailsVC, animated: true)
        
        return true
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
        if gesture {
            
//            showHomeScreenMapDraggingAnimation()
        }
        
    }
    
    func showHomeScreenMapDraggingAnimation() {
        
        self.topAddressBackViewTopConstraint.constant = -45
        self.providerListCollectionViewBottomConstraint.constant = -130
        
        UIView.animate(withDuration: 1.2,
                       delay: 0.0,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0, options: [], animations: {
                        
                        self.topAddressBackView.layoutIfNeeded()
                        self.collectionView.layoutIfNeeded()
                        
                        self.navigationController?.setNavigationBarHidden(true, animated: true)
                        DDLogDebug("Hide")
                        
        })
        
    }
    
    func hideHomeScreenMapDraggingAnimation() {
        
        self.topAddressBackViewTopConstraint.constant = 20
        self.providerListCollectionViewBottomConstraint.constant = 0
        
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.0,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0, options: [], animations: {
                        
                        self.topAddressBackView.layoutIfNeeded()
                        self.collectionView.layoutIfNeeded()
                        
                        self.navigationController?.setNavigationBarHidden(false, animated: true)
                        
                        DDLogDebug("Unhide")
                        
                        
        })
        
        
    }
    
    
    
    
    
}

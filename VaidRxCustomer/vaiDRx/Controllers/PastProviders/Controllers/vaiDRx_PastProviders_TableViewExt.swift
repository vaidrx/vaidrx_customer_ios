//
//  vaiDRx_PastProviders_TableViewExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension vaiDRx_PastProvidersVC: UITableViewDataSource {
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pastProviderResponseModel != nil {
            
            return pastProviderResponseModel!.allProviders.count
        } else {
            
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_PastProviderTableViewCell.self)) as! vaiDRx_PastProviderTableViewCell
        
        if pastProviderResponseModel != nil {
            
            cell.pastProviderName.text = "\(((pastProviderResponseModel?.allProviders[indexPath.row])! [SERVICE_RESPONSE.Provider_FName]) as! String) \(((pastProviderResponseModel?.allProviders[indexPath.row])! [SERVICE_RESPONSE.Provider_LName]) as! String)"
            
            if !((((pastProviderResponseModel?.allProviders[indexPath.row])! [SERVICE_RESPONSE.Provider_Pic]) as! String).isEmpty){
                
                cell.ProviderProfilePic.kf.setImage(with: URL(string: ((self.pastProviderResponseModel?.allProviders[indexPath.row])! [SERVICE_RESPONSE.Provider_Pic]) as! String),
                                                    placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                                    options: [.transition(ImageTransition.fade(1))],
                                                    progressBlock: { receivedSize, totalSize in
                },
                                                    completionHandler: nil)
            }else {
                cell.ProviderProfilePic.image = #imageLiteral(resourceName: "myevent_profile_default_image")
            }
            
            cell.totalBookingToProvider.text = "Total no of bookings: \(((pastProviderResponseModel?.allProviders[indexPath.row])![SERVICE_RESPONSE.Provider_BookingNum]) as! Int)"
            
            cell.lastBookedOn.text = "Last booked on: " +  Helper.getDayAndDateFromTimeStamp(timeStamp: Int64((pastProviderResponseModel?.allProviders[indexPath.row])![SERVICE_RESPONSE.lastBooked] as! Int))
            
        } else {
            
            cell.pastProviderName.text = " "
            cell.ProviderProfilePic.image = #imageLiteral(resourceName: "myevent_profile_default_image")
            cell.totalBookingToProvider.text = "Total no of bookings: 0"
            cell.lastBookedOn.text = "Last booked on: 17th Mar 2018"
        }
        return cell
    }
    
}

extension vaiDRx_PastProvidersVC: UITableViewDelegate {
    
    func call911()  {
//        let cancelBookingView = EmergencyMessageView.sharedInstance
//        //        cancelBookingView.changeTitle(message: "In case of a life threatening emergency please contact 911 for immediate action")
//
//        cancelBookingView.delegate = self
//        WINDOW_DELEGATE??.addSubview(cancelBookingView)
//
//        cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
//
//
//        UIView.animate(withDuration: 0.5,
//                       delay: 0.0,
//                       options: UIViewAnimationOptions.beginFromCurrentState,
//                       animations: {
//
//                        cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
//
//        }) { (finished) in
//
//        }
        
        //vani
        self.showAlert(head: "Message", message: "In case of life threatening emergency please dial 911.")
        
    }
    
    //vani
    func showAlert(head:String, message: String){
        let alertController = UIAlertController(title: head, message: message, preferredStyle: .alert)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        // Create the actions
        let okAction = UIAlertAction(title: ALERTS.Ok, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            OperationQueue.main.addOperation {
                self.okPressed()
            }
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
        }
        // Add the actions
        alertController.addAction(okAction)
        
        newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isComing != "Home"{
            selectedIndex = indexPath.row
            if pastProviderResponseModel != nil {
                pastProviderViewModel.CBModel.providerId = ((pastProviderResponseModel?.allProviders[indexPath.row])![SERVICE_RESPONSE.Provider_Id]) as! String
                call911()
                
//                pastBookinAPI()
            } else {
                Helper.alertVC(title: "Error", message: "No Past Providers Available")
            }
            
        }
    }
}

extension vaiDRx_PastProvidersVC: VaidTodayProtocol,BookingApiDelegate {
    func bookingAccepted() {
        return
    }
    
    func isPatientMinor(value: Int) {
        CBModel.ageType = value
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let bookingViewController = storyboard.instantiateViewController(withIdentifier: "BookingTypeVC") as! vaiDRx_BookingTypeVC
        bookingViewController.delegateForApi = self
        self.present(bookingViewController, animated: true, completion: nil)
   }
    

    func RefreshBookingAPI(type: String) {
        pastBookinAPI(mode: type)
    }
    
    
    
}

extension vaiDRx_PastProvidersVC: EmergencyMessageDelegate {
    
    func okPressed() {
        
        let vaidTodayView = ConfirmVaidToday.sharedInstance
        vaidTodayView.show()
        vaidTodayView.change(source: "Booking")
        vaidTodayView.changeTitle(message: "Is the patient  above 18 years?")
        vaidTodayView.delegate = self
        
        //                vaidTodayView.isComingFor = "Booking"
        
        WINDOW_DELEGATE??.addSubview(vaidTodayView)
        
        vaidTodayView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        vaidTodayView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { (finished) in
            
        }
    }
}

//
//  LiveChatViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 22/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire
import WebKit

class LiveChatViewController:UIViewController, WKNavigationDelegate {
    
    
    
    var liveChatWebView: WKWebView!
    @IBOutlet weak var navigationLeftButton: UIButton!
        
    // MARK: - Variable Decleratons -
    weak var delegate: LeftMenuProtocol?
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    let disposebag = DisposeBag()

    
    override func loadView() {
        liveChatWebView = WKWebView()
        liveChatWebView.navigationDelegate = self
        view = liveChatWebView
    }
    // MARK: - Class Default Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.editNavigationBar(navigationController!)
        
        requestURLs()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.navigationBar.barTintColor = APP_COLOR
        
        setupGestureRecognizer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    // MARK: - Action methods -
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension LiveChatViewController {
    
    func requestURLs() {
        
        let strURL = LiveChat.URLLink
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any] {
                    
                    let statuscode:Int = r.statusCode
                    
                    if statuscode == HTTPSResponseCodes.SuccessResponse.rawValue {
                        
                        if let chatURLResponse  = dict["chat_url"] as? String {
                            
                            let liveChatUrl = URL.init(string: self.prepareUrl(chatURLResponse))
//
                            let requestObj = NSURLRequest(url: liveChatUrl!)
//                            self.liveChatWebView.loadRequest(requestObj as URLRequest)
                            self.liveChatWebView.load(requestObj as URLRequest)
                            self.liveChatWebView.allowsBackForwardNavigationGestures = true
                        } else {
                            
                            Helper.hidePI()
                        }
                        
                    } else {
                        
                        Helper.hidePI()
                    }
                   
                } else {
                    
                    Helper.hidePI()
                }
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
    }

    func prepareUrl(_ url: String) -> String {
        
        let mutableString = NSMutableString.init(string: "https://\(url)")
    
        mutableString.replaceOccurrences(of: "{%license%}",
                                         with: LiveChat.LicenceNumber,
                                         options: .literal,
                                         range: NSRange(location: 0, length: (mutableString.length )))
        mutableString.replaceOccurrences(of: "{%group%}",
                                         with: APP_NAME,
                                         options: .literal,
                                         range: NSRange(location: 0, length: (mutableString.length)))
        return mutableString as String
        
    }
}


// MARK: - Web View Delegate Method -
//extension LiveChatViewController : UIWebViewDelegate {
//
//    func webViewDidStartLoad(_ webView: UIWebView) {
//
////        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//
//        self.perform(#selector(self.hideProgressIndicator), with: nil, afterDelay: 0.2)
//    }
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//
//        self.perform(#selector(self.hideProgressIndicator), with: nil, afterDelay: 0.2)
//
//    }
//
//    @objc func hideProgressIndicator() {
//
//        Helper.hidePI()
//    }
//}

extension LiveChatViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
    

    
}

extension LiveChatViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}

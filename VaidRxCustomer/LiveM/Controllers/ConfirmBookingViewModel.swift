//
//  ConfirmBookingViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class ConfirmBookingViewModel {
    
    var CBModel:ConfirmBookingModel!
    let disposebag = DisposeBag()
    let rxConfirmBookingAPI = ConfirmBookingAPI()
    var bookingId:Int64 = 0
    var reasonId = "1"
    var bookingMode = "0"
    
    let rxCancelBookingAPI = CancelBookingAPI()
    let rxAcceptBookingAPI = AcceptVaidTodayAPI()


    
    func liveBookingAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxConfirmBookingAPI.liveBookingServiceAPICall(confirmBookingModel: CBModel)
        
        if !rxConfirmBookingAPI.liveBooking_Response.hasObservers {
            
            rxConfirmBookingAPI.liveBooking_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
    func acceptBookingAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxAcceptBookingAPI.updateTodayBookingServiceAPICall(bookingId:bookingId)
        
        if !rxAcceptBookingAPI.acceptBooking_Response.hasObservers {
            
            rxAcceptBookingAPI.acceptBooking_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
    /// Method to call Cancel Booking service API
    ///
    /// - Parameter completion: Service API response completion block
    func cancelBookingAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxCancelBookingAPI.cancelBookingServiceAPICall(bookingId: bookingId, reasonId: reasonId)
        
        if !rxCancelBookingAPI.cancelBooking_Response.hasObservers {
            
            rxCancelBookingAPI.cancelBooking_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
        }
    }
}


//
//  vaiDRx_AddNewCardVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class vaiDRx_AddNewCardVC: UIViewController, KeyboardDelegate {

    // MARK: - Variable Decleration -
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var paymentTextField:StripePaymentTextField!
    var cardToken:String!
    let stripeManager = StripeManager.sharedInstance()
    let screenSize = UIScreen.main.bounds
    var registerViewModel = RegisterViewModel()
    var countryRegion: String!
    var selectedCountry: Country!
    var userRegisterType = RegisterType.Default
    var vaidRx_AddNewCardViewModel = VaidRx_AddNewCardViewModel()
    var stripeKeyResponseModel :StripeKeyResponseModel!
    var publishableKey = ""
    //Image Viewa
    @IBOutlet weak var countryFlag: UIImageView!
    
    //Label
    @IBOutlet weak var countryName: UILabel!
    
    //Layout Constraints
    @IBOutlet weak var saveButtonBottomConstraint: NSLayoutConstraint!

    //UIView
    @IBOutlet weak var paymentBackgroundView: UIView!
    @IBOutlet weak var zipCodeTextField: UITextField!
    
    //Button Outlets
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var scanCardButton: UIButton!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.hideNavBarShadow(vc: self)
        setCountryName()
//        getStripeKeyData()
        setPaymentTextFieldProperties()
//        let publishableKey = stripeKeyResponseModel.publishableKey
        publishableKey = UserDefaults.standard.string(forKey: USER_DEFAULTS.CONFIG.PAYMENT_GATEWAY_KEY) ?? ""
        stripeManager.setStripeDefaultPublishableKey(publishableKey:publishableKey)
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
//        stripeManager.setStripeDefaultPublishableKey(publishableKey:"pk_test_hb1isCw9xIEbjjg9qornsdqd")
//        "pk_test_L130fWgicI73UClINTF8YvIF")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        appDelegate?.keyboardDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        appDelegate?.keyboardDelegate = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - setCountrycode
    func setCountryName(){
        
        let countrie = NSLocale.current.regionCode
        let coutryModel = ContryNameModelClass()
        let countries = coutryModel.getCoutryDetailsFormjsonSerial()
        
        for country in countries  {
            if country["code"] == countrie {
                countryName.text = country["name"]
                countryRegion = country["code"]
                let imagestring = countryRegion
                let imagePath = "CountryPicker.bundle/\(imagestring!).png"
                countryFlag.image = UIImage(named: imagePath)
            }
        }
    }
    
    @IBAction func sentToFaq(_ sender: UIButton) {
        let story:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:SupportListViewController = story.instantiateViewController(withIdentifier: VCIdentifier.supportVC) as! SupportListViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}

extension vaiDRx_AddNewCardVC: CardIOPaymentViewControllerDelegate {
    //MARK: - Card IO Deleagte -
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        self.dismiss(animated: true, completion: nil)
        
        stripeManager.validateCardDetails(cardNumber: cardInfo.cardNumber,
                                          expiryMonth: cardInfo.expiryMonth,
                                          expiryYear: cardInfo.expiryYear,
                                          cvv: cardInfo.cvv,
                                          completion: { (isValid) in
                                            
                                            if isValid {
                                                
                                                self.saveAction(self.saveButton)
                                                
                                            } else {
                                                
                                                Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.MissingCardDetails)
                                            }
        })
        
        
    }
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension vaiDRx_AddNewCardVC {
    
    func setPaymentTextFieldProperties() {
        
        paymentTextField = StripeManager.sharedInstance().getPaymentTextField(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenSize.size.width - 100), height: CGFloat(40)))
        
        paymentBackgroundView.addSubview(paymentTextField)
    }
}

// MARK: - CountrySelectedDelegate -
extension vaiDRx_AddNewCardVC: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryFlag.image = UIImage(named: imagePath)
        countryRegion = selectedCountry.country_code
        countryName.text =  self.selectedCountry.country_name
    }
    
}


 //
//  ReachabilityShowView.swift
//  Sales Paddock
//
//  Created by Vasant Hugar on 06/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ReachabilityView: UIView {
    
    private static var share: ReachabilityView? = nil
    var isShown:Bool = false
    
    static var instance: ReachabilityView {
        
        if (share == nil) {
            if let views = Bundle(for: self).loadNibNamed("ReachabilityView", owner: nil, options: nil) {
                share = views.first as? ReachabilityView
            }
        }
        return share!
    }
    
    /// Show Network
    func show()
    {
        if isShown == false {
            
            if let window = UIApplication.shared.keyWindow {
                isShown = true
                self.frame = (window.frame)
                window.addSubview(self)
                
                self.alpha = 0.0
                UIView.animate(withDuration: 0.5,
                               animations: {
                                self.alpha = 1.0
                })
            }
        }
    }
    
    /// Hide
    func hide() {
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            
                            
//                            Helper.reloadControllerWhenNetworkReachable()
                            self.removeFromSuperview()
                            
            })
        }
    }
}

 class BlockerAlert: UIView {
     
     private static var share: BlockerAlert? = nil
     var isShown:Bool = false
     
     static var instance: BlockerAlert {
         
         if (share == nil) {
             if let views = Bundle(for: self).loadNibNamed("BlockerAlert", owner: nil, options: nil) {
                 share = views.first as? BlockerAlert
             }
         }
         return share!
     }
     
     /// Show Network
     func show()
     {
//         if isShown == false {
             
             if let window = UIApplication.shared.keyWindow {
                 isShown = true
                 self.frame = (window.frame)
                //self.frame = CGRectMake( 100, self.frame.height-150, self.frame.size.width, self.frame.size.height);
               // self.frame = CGRect.init(x: 100, y: self.frame.height-150, width: self.frame.size.width, height: self.frame.size.height)
                 window.addSubview(self)
                 
                 self.alpha = 0.0
                 UIView.animate(withDuration: 0.5,
                                animations: {
                                 self.alpha = 1.0
                 })
             }
//         }
     }
     
     /// Hide
     func hide() {
//         if isShown == true {
//             isShown = false
             
             UIView.animate(withDuration: 0.5,
                            animations: {
                             self.alpha = 0.0
             },
                            completion: { (completion) in
                             
                             
 //                            Helper.reloadControllerWhenNetworkReachable()
                             self.removeFromSuperview()
                             
             })
//         }
     }
    
    @IBAction func tappedClose(Sender: UIButton) {
        self.hide()
    }
    
 }


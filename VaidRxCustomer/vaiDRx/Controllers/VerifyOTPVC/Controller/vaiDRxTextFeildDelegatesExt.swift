//
//  vaiDRxTextFeildDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_VerifyOTPVC: UITextFieldDelegate {
    
   
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    
        
        switch textField {
            
        case code1:
            otpImage1.isHidden = true
            
        case code2:
            manualEntry = true
            otpImage2.isHidden = true
            
        case code3:
            otpImage3.isHidden = true
            
        default:
            otpImage4.isHidden = true
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        //        animatingLoadingView(calculateTheLoadingHeight())
        switch textField {
        case code1:
            code1View.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)//UIColor(red:0.12, green:0.48, blue:0.74, alpha:1.0)
            
        case code2:
            code2View.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)// BABABA - disable 3B5998 - Enabled
            
        case code3:
            code3View.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case code4:
            code4View.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        default:
            break
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        let string : String = textField.text!
        if string.length == 0 {
            switch textField {
            case code1:
                otpImage1.isHidden = false
                
            case code2:
                otpImage2.isHidden = false
                
            case code3:
                otpImage3.isHidden = false
                
            default:
                otpImage4.isHidden = false
            }
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        
        // for manual otp entry
        if manualEntry {
            
            if textField == code2 {
                
                otpImage2.isHidden = true
                textField.text = "\(string)"
                code3.becomeFirstResponder()
            } else if textField == code3 {
                
                otpImage3.isHidden = true
                textField.text = "\(string)"
                code4.becomeFirstResponder()
            } else if textField == code4 {
                
                otpImage4.isHidden = true
                textField.text = "\(string)"
                self.view.endEditing(true)
                self.verifyCodeAction()
            }
            return true
        }
        
        // for auto refill in multiple textfields
        countOtp = countOtp + 1
        print(string + "and..." + "\(countOtp)")
        if countOtp == 3 {
            code1.text = ""
            otpImage1.isHidden = true
            code1.text = "\(string)"
        } else if countOtp == 4 {
            code2.text = ""
            otpImage2.isHidden = true
            code2.text = "\(string)"
            
        } else if countOtp == 5 {
            code3.text = ""
            otpImage3.isHidden = true
            code3.text = "\(string)"
            
        } else if countOtp == 6 {
             code4.text = ""
             otpImage4.isHidden = true
             code4.text = "\(string)"
             self.view.endEditing(true)
             self.verifyCodeAction()
        }


        
        
        
    
//        if textField.isEqual(code4) && !string.isEmpty {
//            textField.text = string
//            textField.resignFirstResponder()
//            self.verifyCodeAction()
//        }
        let oldLength = textField.text!.length
        let replacementLength = string.length
        let rangeLength = range.length
        let newLength = oldLength - rangeLength + replacementLength
        
        
        if (newLength == 1) {
            
            if textField.isEqual(code1) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: code2, afterDelay: 0.05)
            }
            else if textField.isEqual(code2) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: code3, afterDelay: 0.05)
            }
            else if textField.isEqual(code3) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: code4, afterDelay: 0.05)
            }
            

            
                
            
            
        }
        else if (oldLength > 0 && newLength == 0) {
            
            if textField.isEqual(code4) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: code3, afterDelay: 0.05)
            }
            else if textField.isEqual(code3) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: code2, afterDelay: 0.05)
            }
            else if textField.isEqual(code2) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: code1, afterDelay: 0.05)
            } else if textField.isEqual(code1) {
                //                animatingLoadingView(0)
            }
        }
        
        return newLength <= 1
        
    }
    
    
    func calculateTheLoadingHeight() -> Int {
        var length = 0
        if code1.text?.length != 0 {
            length = length + 1
        }
        if code2.text?.length != 0 {
            length = length + 1
        }
        if code3.text?.length != 0 {
            length = length + 1
        }
        if code4.text?.length != 0 {
            length = length + 1
        }
        
        return length
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //        animatingLoadingView(calculateTheLoadingHeight())
        switch textField {
        case code1:
            code1View.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)//UIColor(red:0.73, green:0.73, blue:0.73, alpha:1.0)
            
        case code2:
            code2View.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case code3:
            code3View.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case code4:
            code4View.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    @objc func setNextResponder(nextResponder: UITextField) {
        nextResponder.becomeFirstResponder()
    }
}

//
//  HomeViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 25/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import Kingfisher

protocol HomeViewControllerDelegate {
    
    func userDidLogout()
}

enum  HomeSectionType : Int {
    
    case VehicleType = 1
}

enum HomeRowType : Int {
    
    case RowType = 1
}

class HomeViewController: UIViewController, AddressDelegate {
    var tempAddressText = ""
    var addressList = [ModalForAddress]()
    var delegate : HomeViewControllerDelegate? = nil
    var isFavouriteAddSet = Bool()
    var timer = Timer()
    var pickLat : Float = 0.00
    var pickLong : Float = 0.00
    var typesArray: [[String: AnyObject]] = []
    var indexSelected : Int = 0
    let pub: VNHPubNubWrapper = VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapCenterPinImage: NSLayoutConstraint!
    @IBOutlet weak var dayRunnerImage: UIImageView!
    @IBOutlet weak var currentLocationView: UIButton!
    @IBOutlet weak var vechilTypeView: UIView!
    @IBOutlet weak var addressPickView: UIView!
    @IBOutlet weak var bookNowAndLaterView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var laterBookingTimeLabel: UILabel!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var datePickBottom: NSLayoutConstraint!
    @IBOutlet weak var pickAddressOutlet: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var vehicleDetailsView: UIView!
    @IBOutlet weak var vehicleDetailsBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var currentMarker: UIImageView!
    @IBOutlet weak var currentLine: UIImageView!
    @IBOutlet weak var saveFavourite: UIButton!
    @IBOutlet weak var saveAddressHeight: NSLayoutConstraint!
    @IBOutlet weak var saveCancelView: NSLayoutConstraint!
    @IBOutlet weak var addFavTF: NSLayoutConstraint!
    @IBOutlet weak var currentlineView: UIView!
    @IBOutlet weak var vehicleDetailsHeight: NSLayoutConstraint!
    @IBOutlet weak var bookLaterHeight: NSLayoutConstraint!
    @IBOutlet weak var enterFavLocation: UITextField!
    var isClicked = false
    let defaults = UserDefaults.standard
    @IBOutlet weak var menuLeading: NSLayoutConstraint!
    @IBOutlet weak var menuImageHelight: NSLayoutConstraint!
    @IBOutlet weak var shadowImage: UIImageView!
    let locationObj = LocationManager.shared
    
    
    //Vehicle Details Outlets
    @IBOutlet weak var vehCapacity: UILabel!
    @IBOutlet weak var vehSize: UILabel!
    @IBOutlet weak var vehPricePerMile: UILabel!
    @IBOutlet weak var vehDur: UILabel!
    @IBOutlet weak var vehMinFare: UILabel!
    @IBOutlet weak var vehPrice: UILabel!
    @IBOutlet weak var vehPricePerMin: UILabel!
    
    
    //MARK: - Vasant
    var arrayOfMaster: [AnyObject] = []
    var isPickedFromSearch: Bool = false
    
    //MARK: -
    
    var DefaultImage: [UIImage] = [
        UIImage(named: "1_on")!,
        UIImage(named: "2_on")!,
        UIImage(named: "3_on")!,
        UIImage(named: "4_on")!,
        UIImage(named: "5_on")!,
        UIImage(named: "1_on")!,
        UIImage(named: "2_on")!,
        UIImage(named: "3_on")!,
        UIImage(named: "4_on")!,
        ]
    var VehicleImage: [UIImage] = [
        UIImage(named: "1_off")!,
        UIImage(named: "2_off")!,
        UIImage(named: "3_off")!,
        UIImage(named: "4_off")!,
        UIImage(named: "5_off")!,
        UIImage(named: "1_off")!,
        UIImage(named: "2_off")!,
        UIImage(named: "3_off")!,
        UIImage(named: "4_off")!,
        ]
    
    var vechileDetails = [VehicleDetails]()
    var tids = [VehicleTID]()
    var vehMapImg = [VehicleImageModel]()
    let address = ModalForAddress()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        saveAddressHeight.constant = 50
        addFavTF.constant = 0
        saveCancelView.constant = 0
        initiateThePubnub()
        publishToServer()
        address.sendRequestForGetAddress()
        
        initiateMap()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.view.endEditing(true)
        self.navigationController?.isNavigationBarHidden = true
        if defaults.object(forKey: "vechTypeData") != nil {
            
            self.vehicleTypeDetails(message : defaults.object(forKey: "vechTypeData") as! [AnyHashable : Any])
            collectionView.reloadData()
        } else {
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.timer.invalidate()
        pub.delegate = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initiateThePubnub() {
        pub.subscribeToMyChannel()
        pub.delegate = self
        pub.initiatePubNub()
    }
    
    func publishToServer()
    {
        self.timer = Timer.scheduledTimer(timeInterval:5,
                                          target: self,
                                          selector: #selector(handleTimer),
                                          userInfo: nil,
                                          repeats: true)
    }
    
    func handleTimer()
    {
        let dict =  ["a"     :1 as AnyObject,
                     "chn"   :Utility.myChannel ,
                     "lt"    :pickLat.description,
                     "lg"    :pickLong.description] as [String : Any]
        print("\n\nEmit Message : \(dict)\n\n")
        pub.publish(withParameter: dict, toChannel:ConfigKeys.SERVER_KEY)
    }
    
    
    
    func initiateMap() {
        
        pickLat = locationObj.latitute
        pickLong = locationObj.longitude
        
        address.AddressDelegate = self
        
        mapView.delegate = self
        
        mapView.bringSubview(toFront: vehicleDetailsView)
        mapView.bringSubview(toFront: dayRunnerImage)
        mapView.bringSubview(toFront: bookNowAndLaterView)
        mapView.bringSubview(toFront: currentLocationView)
        mapView.bringSubview(toFront: addressPickView)
        mapView.bringSubview(toFront: vechilTypeView)
        mapView.bringSubview(toFront: menuButton)
        mapView.bringSubview(toFront: datePickerView)
        mapView.bringSubview(toFront: pickAddressOutlet)
        mapView.bringSubview(toFront: currentMarker)
        mapView.bringSubview(toFront: currentLine)
        mapView.bringSubview(toFront: currentlineView)
        mapView.bringSubview(toFront: shadowImage)
        mapView.isMyLocationEnabled = true
        
        Helper.shadowView(sender: addressPickView, width:UIScreen.main.bounds.width - 60, height: 60)
        self.enterFavLocation.isUserInteractionEnabled = true
        
        pickLat = locationObj.latitute
        pickLong = locationObj.longitude
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(locationObj.latitute),
                                              longitude: CLLocationDegrees(locationObj.longitude),
                                              zoom: 16)
        self.mapView.animate(to: camera)
        
    }
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    /// DatePicker
    ///
    /// - parameter datePicker: Date
    
    func datePickerChanged(_ datePicker:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: datePicker.date)
        laterBookingTimeLabel.text = strDate
    }
    
    @IBAction func setPickUpTimeAction(_ sender: AnyObject) {
        
        UIView.animate(withDuration: 1.0,
                       animations: {
                        self.datePickBottom.constant = -315
                        self.view.superview?.layoutIfNeeded()
        })
    }
    
    // MARK: - UIButtons
    
    @IBAction func saveAddressButtonAction(_ sender: Any) {
        if (enterFavLocation.text?.isEmpty)! {
            self.present(Helper.alertVC(title: ALERTS.Message, message: ALERTS.AddressName as NSString), animated: true, completion: nil)
            return
        }
        let params : [String : Any] = ["token"          :Utility.sessionToken,
                                       "address"        :self.addressLabel.text!,
                                       "Name"           :self.enterFavLocation.text!,
                                       "ent_lat"        :String(pickLat),
                                       "ent_long"       :String(pickLong)
        ]
        address.sendRequestForSaveAddress(address: params)
        
        
    }
    
    
    @IBAction func bookNowClicked(_ sender: AnyObject) {
        performSegue(withIdentifier: "selectAddress", sender: self)
    }
    
    @IBAction func bookLaterClicked(_ sender: AnyObject) {
        
        UIView.animate(withDuration: 1.0,
                       animations: {
                        self.datePickBottom.constant = 0
                        self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func selectPickAddress(_ sender: AnyObject) {
        if !isFavouriteAddSet {
            self.enterFavLocation.becomeFirstResponder()
        }
        pickAddressOutlet.tag = 20
        performSegue(withIdentifier: "selectAddress", sender: sender)
    }
    
    @IBAction func saveFavouriteAction(_ sender: AnyObject) {
        
        if sender as! NSObject == self.saveFavourite && saveFavourite.isSelected{
            var addressTemp = ModalForAddress()
            for data in addressList {
                if data.Address == self.addressLabel.text! || data.Name == self.addressLabel.text!{
                    addressTemp = data
                    break
                }
            }
            address.sendRequestForDeleteAddress(data: addressTemp.AddressID)
            return
        }
        if isFavouriteAddSet
        {
            //            saveFavourite.isSelected = false
            
            isFavouriteAddSet = false
            
            UIView.animate(withDuration: 0.6,
                           animations: {
                            
                            self.saveAddressHeight.constant = 50
                            self.addFavTF.constant = 0
                            self.vehicleDetailsHeight.constant = 100
                            self.bookLaterHeight.constant = 0
                            self.menuLeading.constant = 0
                            self.menuImageHelight.constant = 0
                            self.saveCancelView.constant = 0
                            self.view.layoutIfNeeded()
                            
                            Helper.shadowView(sender: self.addressPickView, width:UIScreen.main.bounds.width - 60, height: 60)
                            self.view.endEditing(true)
                            
            },completion:nil)
        }else {
            
            //            saveFavourite.isSelected = true
            isFavouriteAddSet = true
            
            UIView.animate(withDuration: 0.6,
                           animations: {
                            self.saveAddressHeight.constant = 140
                            self.addFavTF.constant = 30
                            self.saveCancelView.constant = 30
                            self.vehicleDetailsHeight.constant = -60
                            self.bookLaterHeight.constant = -130
                            self.menuLeading.constant = -150
                            self.menuImageHelight.constant = -150
                            self.view.layoutIfNeeded()
                            Helper.shadowView(sender: self.addressPickView, width:UIScreen.main.bounds.width - 60, height: 140)
            })
        }
    }
    
    @IBAction func getCurrentLocation(_ sender: AnyObject) {
        pickLat = locationObj.latitute
        pickLong = locationObj.longitude
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
            let lng = self.mapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 16)
        self.mapView.animate(to: camera)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        switch segue.identifier! as String {
            
        case "selectAddress":
            if let searchAddress =  segue.destination as? SearchAddressController
            {
                if pickAddressOutlet.tag == 20 {
                    
                    searchAddress.isComming = .PickUp
                    searchAddress.delegate = self
                    pickAddressOutlet.tag = 30
                    break
                } else {
                    
                    searchAddress.isComming = .DropOff
                    var bookDetail = BookingDetails()
                    
                    bookDetail.pickAddress = self.addressLabel.text!
                    bookDetail.pickLat = locationObj.latitute.description
                    bookDetail.pickLong = locationObj.longitude.description
                    bookDetail.workType = vechileDetails[indexSelected].Vehtype_id!
                    bookDetail.vechicleName = vechileDetails[indexSelected].Vehtype_name!
                    searchAddress.passBookingDetail = bookDetail
                    searchAddress.delegate = self
                }
            }
            
        case "":
            
            break
            
        default:
            break
        }
    }
    
    
    func vehicleTypeDetails(message : [AnyHashable : Any]!) {
        
        
        
        let vehData = message[AnyHashable("types")]! as! [[String : AnyObject]]
        if vehData == nil {
            return
        } else {
            
            for details in vehData {
                
                let mapIcon = details["MapIcon"] as? String
                let distance_price_unit = details["distance_price_unit"] as? String
                let mileage_price = details["mileage_price_unit"] as? String
                let min_fare = details["min_fare"] as? String
                let type_id = details["type_id"] as? String
                let type_name = details["type_name"] as? String
                let vehicle_capacity = details["vehicle_capacity"] as? String
                let vehicleDimantion = details["vehicleDimantion"] as? String
                let vehicle_img = details["vehicle_img"] as? String
                let x_mileage = details["x_mileage"] as? String
                let x_min = details["x_min"] as? String
                let vech_img_off = details["vehicle_img_off"] as? String
                vechileDetails.append(VehicleDetails.init(MapIcon: mapIcon!,
                                                          distance_price_unit: distance_price_unit!,
                                                          mileage_price_unit: mileage_price!,
                                                          min_fare: min_fare!,
                                                          type_id: type_id!,
                                                          type_name: type_name!,
                                                          vehicleDimantion: vehicleDimantion!,
                                                          vehicle_capacity: vehicle_capacity!,
                                                          vehicle_img: vehicle_img!,
                                                          vehicle_offImg: vech_img_off!,
                                                          x_mileage: x_mileage!,
                                                          x_min: x_min!))
                typesArray = message[AnyHashable("types")]! as! [[String : AnyObject]]
            }
        }
        
        //let masData = message[AnyHashable("masArr")]! as! [[String : AnyObject]]
    }
    
    func setAddress() {
        self.saveFavourite.isSelected = false
        if addressList.count != 0 {
            for data in addressList {
                if data.Address == self.addressLabel.text! {
                    self.addressLabel.text = data.Name
                    self.saveFavourite.isSelected = true
                    break
                }else{
                    self.addressLabel.text = tempAddressText
                }
            }
        }
    }
}


extension HomeViewController {
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let labelHeight = self.addressLabel.intrinsicContentSize.height
        self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0,
                                            bottom: labelHeight, right: 0)
        
        UIView.animate(withDuration: 0.25, animations:
            {
                self.view.layoutIfNeeded()
        })
        
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate) {
            
            response, error in
            if let address = response?.firstResult()
            {
                let lines = address.lines! as [String]
                self.addressLabel.text = lines.joined(separator: "\n")
                self.tempAddressText = lines.joined(separator: "\n")
                self.setAddress()
                UIView.animate(withDuration: 0.25, animations:
                    {
                        self.view.layoutIfNeeded()
                })
            }
        }
    }
}


extension HomeViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.typesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell", for: indexPath) as! HomeCollectionCell
        
        if indexSelected == indexPath.row {
            if let imageURl = vechileDetails[indexPath.row].Vehvehicle_img {
                
                cell.vehicleType.kf.setImage(with: URL(string: imageURl),
                                             placeholder:DefaultImage[(indexPath as NSIndexPath).row],
                                             options: [.transition(ImageTransition.fade(1))],
                                             progressBlock: { receivedSize, totalSize in
                },
                                             completionHandler: { image, error, cacheType, imageURL in
                })
            }
            cell.vehicleName.textColor = UIColor.white
            cell.vehicleDuration.textColor = UIColor.white
        }else{
            if let imageURl = vechileDetails[indexPath.row].Vehvvehicle_offImg {
                
                cell.vehicleType.kf.setImage(with: URL(string: imageURl),
                                             placeholder:VehicleImage[(indexPath as NSIndexPath).row],
                                             options: [.transition(ImageTransition.fade(1))],
                                             progressBlock: { receivedSize, totalSize in
                },
                                             completionHandler: { image, error, cacheType, imageURL in
                })
            }
            cell.vehicleName.textColor = UIColor.gray
            cell.vehicleDuration.textColor = UIColor.gray
        }
        //        cell.vehicleType.image = VehicleImage[(indexPath as NSIndexPath).row]
        cell.vehicleName.text = vechileDetails[indexPath.row].Vehtype_name
        //        cell.vehicleDuration.text = self.masArray[indexPath.row]["tid"]?.description
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.indexPathsForSelectedItems?.first {
        case .some(indexPath):
            return CGSize()
        default:
            let height = (view.frame.width) * 9 / 16
            return CGSize(width: view.frame.width, height: height + 50 + 50)
        }
    }
}

extension HomeViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell : HomeCollectionCell = collectionView.cellForItem(at:indexPath) as! HomeCollectionCell
        cell.isSelected = true
        cell.vehicleType.isHighlighted = true
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        cell.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        if !isClicked {
            if indexPath.row == indexSelected {
                UIView.animate(withDuration: 1.0,
                               animations: {
                                self.vehicleDetailsBottomSpace.constant = 140
                                self.view.superview?.layoutIfNeeded()
                                self.isClicked = true
                })
            }
        }else {
            if indexPath.row == indexSelected {
                UIView.animate(withDuration: 1.0,
                               animations: {
                                self.vehicleDetailsBottomSpace.constant = 20
                                self.view.superview?.layoutIfNeeded()
                                self.isClicked = false
                })
            } else {
                UIView.animate(withDuration: 1.0,
                               animations: {
                                self.vehicleDetailsBottomSpace.constant = 20
                                self.view.superview?.layoutIfNeeded()
                                self.isClicked = false
                })
            }
        }
        indexSelected = indexPath.row
        self.vehMinFare.text = vechileDetails[indexPath.row].Vehmin_fare
        self.vehCapacity.text = vechileDetails[indexPath.row].Vehvehicle_capacity
        self.vehSize.text = vechileDetails[indexPath.row].VehvehicleDimantion
        self.vehPricePerMile.text = vechileDetails[indexPath.row].Vehx_mileage
        self.vehDur.text = vechileDetails[indexPath.row].Vehx_min
        self.vehPrice.text = vechileDetails[indexPath.row].Vehmileage_price_unit
        self.vehPricePerMin.text = vechileDetails[indexPath.row].Vehdistance_price_unit
        self.collectionView.reloadData()
        
        // Plot Provider on Map when you select an Type
        if arrayOfMaster.isEmpty == false && indexPath.row < arrayOfMaster.count - 1 {
            plotProviderOnMap(dictionary: arrayOfMaster[indexPath.row] as! [String : AnyObject])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let cell : HomeCollectionCell = collectionView.cellForItem(at:indexPath) as? HomeCollectionCell {
            cell.isSelected = false
            cell.vehicleType.isHighlighted = false
            cell.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        
    }
    // MARK: - AddressModalDelegate
    func addressDelegate(data: [ModalForAddress]) {
        saveFavourite.isSelected = true
        if isFavouriteAddSet {
            self.saveFavouriteAction(self)
        }
        addressList.removeAll()
        addressList = data
        self.setAddress()
    }
}


// MARK: - GMSMapViewDelegate
extension HomeViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        guard isPickedFromSearch else {
            reverseGeocodeCoordinate(position.target)
            return
        }
        isPickedFromSearch = false
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool)
    {
        print("willMove gesture")
    }
}


extension HomeViewController: SearchAddressControllerDelegate {
    
    func didSelectAdress(lat: Float, Long: Float, Address1: String, Address2: String, ZipCode: String) {
        performSegue(withIdentifier: "shippmentSegue", sender:self)
    }
    
    func didSelectPickAdd(lat: Float, Long: Float, Address1: String, Address2: String) {
        pickLat = lat
        pickLong = Long
        publishToServer()
        
        isPickedFromSearch = true
        
        self.addressLabel.text = Address1 + Address2
        let mapLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(lat), CLLocationDegrees(Long))
        mapView.camera = GMSCameraPosition(target: mapLocation, zoom: 17, bearing: 0, viewingAngle: 0)
    }
}


extension HomeViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

extension HomeViewController: VNHPubNubWrapperDelegate {
    
    func receivedMessage(_ message:[AnyHashable : Any]!, andChannel channel: String!) {
        let dict = message
        let status = dict?["a"] as? NSNumber
        print("PubNub Response :",status!)
        
        switch status! {
        case 2:
            
            if (message["flag"] as! Int) == 1 {
                // Types and MasArr are empty
                return
            }
            
            let newTypeArray = message[AnyHashable("types")]! as! [[String : AnyObject]]
            
            // Vasant
            arrayOfMaster = message[AnyHashable("masArr")]! as! [AnyObject]
            if arrayOfMaster.isEmpty == false {
                plotProviderOnMap(dictionary: arrayOfMaster[indexSelected] as! [String : AnyObject])
            }
            
            // -----------------
            
            if newTypeArray.count > 0 {
                
                self.collectionView.isHidden = false
                if typesArray.count == 0 {
                    
                    typesArray = newTypeArray
                    self.vehicleTypeDetails(message: message)
                    collectionView.reloadData()
                }else {
                    
                    
                    if  newTypeArray.count != typesArray.count {
                        
                        defaults.set(message, forKey:"vechTypeData")
                        defaults.synchronize()
                        
                        typesArray = newTypeArray
                        self.vehicleTypeDetails(message: message)
                        collectionView.reloadData()
                        
                    } else if newTypeArray.count == typesArray.count {
                        
                        if (newTypeArray as NSArray).isEqual(to: typesArray) {
                            
                            //Providers Types Remains Same then dont do anything
                            defaults.set(message, forKey:"vechTypeData")
                            defaults.synchronize()
                            
                        } else {
                            defaults.set(message, forKey:"vechTypeData")
                            defaults.synchronize()
                            
                            typesArray = newTypeArray
                            self.vehicleTypeDetails(message: message)
                            collectionView.reloadData()
                        }
                    }
                }
            }else {
                self.collectionView.isHidden = true
                self.present(Helper.alertVC(title:"", message: "There are no wehicle types in your area"), animated: true, completion: nil)
                //alertMessages of no types are available
            }
            break
            
        case 3:
            let bookingStatus = dict?["st"] as? NSNumber
            switch  bookingStatus! {
                
            case 2:
                self.present(Helper.alertVC(title:"", message: dict?["msg"] as! NSString), animated: true, completion: nil)
                break
            case 6:
                self.present(Helper.alertVC(title:"", message: dict?["msg"] as! NSString), animated: true, completion: nil)
                break
            case 7:
                self.present(Helper.alertVC(title:"", message: dict?["msg"] as! NSString), animated: true, completion: nil)
                break
            case 8:
                self.present(Helper.alertVC(title:"", message: dict?["msg"] as! NSString), animated: true, completion: nil)
                break
            case 9:
                self.present(Helper.alertVC(title:"", message: dict?["msg"] as! NSString), animated: true, completion: nil)
                break
                
            default:
                self.present(Helper.alertVC(title:"", message: dict?["msg"] as! NSString), animated: true, completion: nil)
                break
            }
            break
        default:
            
            break
        }
    }
}


extension HomeViewController { // Vasant
    
    
    /// Plot Provider on Map based on Type selected
    /// and check if Provider Location has changed
    /// Also Update the map marker when received on Pubnub
    ///
    /// - Parameter dictionary: Details of selected Type Details
    func plotProviderOnMap(dictionary: [String: AnyObject]) {
        print("Select array: \(dictionary)")
        
        // Clear the Map
        mapView.clear()
        
        // Get type Icon/ Download
        let mapIConImageView = UIImageView()
        if typesArray.count != 0 {
            let mapIconURL = typesArray[indexSelected]["MapIcon"]
            mapIConImageView.kf.setImage(with: URL(string: mapIconURL as! String),
                                         placeholder:#imageLiteral(resourceName: "car"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
            },
                                         completionHandler: { image, error, cacheType, imageURL in
            })
        }
        
        
        // Create Marker on map
        if let array: [AnyObject] = (dictionary["mas"] as? [AnyObject]?)! {
            
            if array.isEmpty == false {
                for index in 0...array.count - 1 {
                    
                    let dict: [String: AnyObject] = array[index] as! [String : AnyObject]
                    
                    let coordinates = CLLocationCoordinate2D(latitude: dict["lt"] as! CLLocationDegrees,
                                                             longitude: dict["lg"] as! CLLocationDegrees)
                    // Creates a marker in the center of the map.
                    let marker = GMSMarker()
                    marker.position = coordinates
                    marker.icon = mapIConImageView.image
                    marker.map = mapView
                    
                    // Get ETA between Selected Location and First nearest Provider
                    if index == 0 {
                        
                        let param: ETAParams = ETAParams()
                        
                        param.source?.latitude = Double(pickLat)
                        param.source?.longitude = Double(pickLong)
                        param.destination?.latitude = dict["lt"] as! Double
                        param.destination?.longitude = dict["lt"] as! Double
                        
                        ETAModel.getETA(params: param, completion: { (success, response) in
                            
                            if success {
                                print("\n\nDuration :\(response.durationText)\nDistance :\(response.distanceText)")
                            }
                        })
                    }
                }
            }
        }
    }
    
}


//
//  LoginViewController.swift
//  Iserve
//
//  Created by Rahul Sharma on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import RxSwift
import RxCocoa

class LoginViewController: UIViewController,KeyboardDelegate {
    
    // MARK: - Outlets -
    
    //ScrollView
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var scrollContentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var loadingConstrain: NSLayoutConstraint!
    @IBOutlet weak var PhoneMissingConst: NSLayoutConstraint!
    @IBOutlet weak var passMissingCons: NSLayoutConstraint!
    
    //UIView
    @IBOutlet weak var passwordTFBottomLineView: UIView!
    @IBOutlet weak var emailTFBottomLineView: UIView!
    @IBOutlet weak var loginBottomView: UIViewCustom!
    @IBOutlet weak var facebookBackView: UIView!
    @IBOutlet weak var googleBackView: UIView!
    @IBOutlet weak var loadingView: UIViewCustom!
    
    
    //Button
    @IBOutlet weak var facebookLoginButton: UIButton!
    @IBOutlet weak var gmailLoginButton: UIButtonCustom!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    //Text Field
    
    @IBOutlet var passwordTextfield: FloatLabelTextField!
    
    @IBOutlet var emailTextField: FloatLabelTextField!
    
    // MARK: - Variable Declerations -
    let catransitionAnimationClass = CatransitionAnimationClass()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var isFBLogin : Bool! = false
    
    var fbUserDataModel:UserDataModel!
    
    let locationObj = LocationManager.sharedInstance()
    var loadingImage: UIImageView!
    var emailFormFb = ""
    var registerFormFb = false
        
    var loginViewModel = LoginViewModel()
    
    let disposeBag = DisposeBag()
    
    
    // MARK: - Class Initial Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false;
        initiallSetUp()
        Helper.editNavigationBar(navigationController!)
        locationObj.start()
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        if registerFormFb {
            emailTextField.text = emailFormFb
            loginViewModel.emailText.value = emailTextField.text!
        }
        
        //Set Scrollview total scroll frame
        if((loginBottomView.frame.size.height + loginBottomView.frame.origin.y) + 30 > scrollView.frame.size.height) {
            
            scrollContentViewHeightConstraint.constant = (loginBottomView.frame.size.height + loginBottomView.frame.origin.y) + 30 - scrollView.frame.size.height;
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0;
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDelegate?.keyboardDelegate = self
        animateBottomView()
        springAnimation()
        PhoneMissingConst.constant = 0
        passMissingCons.constant = 0
        
        addObserveToVariables()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        appDelegate?.keyboardDelegate = nil
    }
    
    func addObserveToVariables() {
        
        emailTextField.rx.text
            .orEmpty
            .bind(to: loginViewModel.emailText)
            .disposed(by: disposeBag)
        
        passwordTextfield.rx.text
            .orEmpty
            .bind(to: loginViewModel.passwordText)
            .disposed(by: disposeBag)
        
    }

    
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
        }
        
    }
    
}


//
//  PaymentViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    
    // MARK: - Outlets -
    //TableView
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: LeftMenuProtocol?
    var arrayOfCards = [CardDetailsModel]()
    var selectedIndexPath:IndexPath!
    
    var isFromConfirmBookingVC:Bool = false
    var CBModel:ConfirmBookingModel!
    
    var isPaymentListChanged:Bool = false
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    let paymentListViewModel = PaymentListViewModel()
    
    static var obj:PaymentViewController? = nil
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    

    
    class func sharedInstance() -> PaymentViewController {
        
        return obj!
    }

    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        PaymentViewController.obj = self
        isPaymentListChanged = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        Helper.editNavigationBar(navigationController!)
        Helper.hideNavBarShadow(vc: self)
        
        setupGestureRecognizer()
        
        if isPaymentListChanged {
            
            getCards()
        }
        
        
        acessClass.acessDelegate = self
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
//        self.navigationController?.view?.backgroundColor = #colorLiteral(red: 0.01960784314, green: 0.7568627451, blue: 0.7882352941, alpha: 1) //UIColor.white //05C1C9
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //UIColor(hex: "05C1C9") //UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Methods -
    
    @IBAction func addNewCardAction(_ sender: Any) {
        
        performSegue(withIdentifier: SEgueIdetifiers.paymentToAddCard, sender: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
//        if isFromConfirmBookingVC {
        
            self.navigationController?.popViewController(animated: true)
            
//        } else {
//
////            delegate?.changeViewController(LeftMenu.searchArtist)
//            self.navigationController?.popViewController(animated: true)
//        }
        
    }
    
}

extension PaymentViewController: UINavigationControllerDelegate {

    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        return SlideAnimatedTransitioning()
    }

    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {

        navigationController.delegate = nil

        if panGestureRecognizer.state == .began {

            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }

        return percentDrivenInteractiveTransition
    }
}

extension PaymentViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}


extension PaymentViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getCardDetails.rawValue:
            
            getCards()
            
            break
            
        default:
            break
        }
    }
    
}

//
//  vaiDRx_PreferredInsurance_APICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 01/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_PreferredInsuranceVC {

    func serviceRequestToUpdateProfile() {
        
        preferredInsuranceViewModel.firstName = profileViewModel.firstNameText.value
        preferredInsuranceViewModel.lastNameText = profileViewModel.lastNameText.value
        preferredInsuranceViewModel.aboutMeText = profileViewModel.aboutMeText.value
        preferredInsuranceViewModel.profilePicURL = profileViewModel.profilePicURL
        preferredInsuranceViewModel.dobText = profileViewModel.dobText
        
        preferredInsuranceViewModel.updateProfileDetailsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.updateProfileData, userRegisterType: self.userRegisterType)
        }
        
    }
    
    
    func serviceRequestToMakeRegister() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        
//        registerViewModel.registerType = userRegisterType
        
        registerViewModel.registerAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.signUp, userRegisterType: self.userRegisterType)
        }
        
    }
    
    func sendServiceToGetInsurances() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        preferredInsuranceViewModel.getInsurancesAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.GetInsurances, userRegisterType: self.userRegisterType)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType, userRegisterType:RegisterType?)
    {
        if HTTPSResponseCodes.Register(rawValue: statusCode) != nil {
            
            let responseCodes : HTTPSResponseCodes.Register = HTTPSResponseCodes.Register(rawValue: statusCode)!
            
            switch responseCodes {
                
            case .WrongInputData://wrong data
                
                break
                
                
            case .WrongPhoneNumber:
                
                break
                
                
                
            case .MissingPasswordFBLogin://User Already Registered using normal password, so user as to make login using normal password
                
                
                
                break
                
                
                
            case .SuccessResponse:
                
                switch requestType
                {
                case RequestType.signUp:
                    if let userDataResponse = dataResponse as? [String:Any] {
                        
                        registerViewModel.saveCurrentUserDetails(dataResponse: userDataResponse)
                        
                        self.performSegue(withIdentifier: "toTermsandConditionNew", sender: self)
                    }
                    
                    break
                    
                case RequestType.GetInsurances:
                    
                    if dataResponse != nil {
                        
                        self.insuranceResponseModel = GetInsuranceResponseModel.init(insuranceDetails: dataResponse!)
                        tableView.reloadData()

                        
                    }
                    
                case RequestType.updateProfileData:
                    
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
                    
                    for aViewController in viewControllers {
                        
                        if(aViewController is ProfileViewController){
                            self.navigationController!.popToViewController(aViewController, animated: true);
                            //                            self.profileViewModel = ProfileViewModel()
                            //                            self.profileResponseModel = nil
                        }
                    }
                    DDLogDebug("Sucess")
                default:
                    break
//                case .updateEmail:
//                    break
//                case .updateMobile:
//                    break
//                case .updateAppVersion:
//                    break
//                case .verifyMobileNumber:
//                    break
//                case .verifyReferralCode:
//                    break
//                case .verifyEmail:
//                    break
//                case .sendOTP:
//                    break
//                case .signUpOTP:
//                    break
//                case .verifyOTP:
//                    break
//                case .forgotPasswordVerifyOTP:
//                    break
//                case .signIn:
//                    break
//                case .getAllApptDetails:
//                    break
//                case .getAllOngoingApptDetails:
//                    break
//                case .getParticularApptDetails:
//                    break
//                case .getInvoiceDetails:
//                    break
//                case .getChatMessages:
//                    break
//                case .sendChatMessages:
//                    break
//                case .updateReview:
//                    break
                    
//                case .getProfileData:
//                    break


//                case .forgetPassword:
//                    break
//                case .forgotPasswordVerifyPhoneNumber:
//                    break
//                case .changePassword:
//                    break
//                case .signOut:
//                    break
//                case .liveBooking:
//                    break
//                case .getCancelReasons:
//                    break
//                case .cancelBooking:
//                    break
//                case .addCard:
//                    break
//                case .cardDefault:
//                    break
//                case .getCardDetails:
//                    break
//                case .deleteCard:
//                    break
//                case .checkCoupon:
//                    break
//                case .addAddress:
//                    break
//                case .deleteAddress:
//                    break
//                case .GetAddress:
//                    break
//                case .UpdateAddress:
//                    break
//                case .GetSupportDetails:
//                    break
//                case .ChangeBookingStatus:
//                    break
//                case .musicGenres:
//                    break
//                case .refressAccessToken:
//                    break
//                case .updatePassword:
//                    break
//                case .getAllMusicians:
//                    break
//                case .getProviderDetails:
//                    break
//                case .reviewAndRating:
//                    break
//                case .GetReviews:
//                    break
//                case .GetDistanceAndETA:
//                    break
//                case .GetPendingReviews:
//                    break
//                case .PastProviders:
//                    break
//                case .AcceptingBooking:
//                    break
                }
                

                
            }
            
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
            
            switch requestType {
                
            case RequestType.verifyReferralCode:
                
                registerViewModel.referralCodeText.value = ""
                
            case RequestType.signIn:
                
                break
                
                
            default:
                break
            }
            
        }
        
        
    }
}

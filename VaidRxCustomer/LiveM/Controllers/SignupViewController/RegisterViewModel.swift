//
//  RegisterViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

/// Register View Model Class to maintain Login View Data
class RegisterViewModel{
    
    var registerModel:RegisterModel!//Register Model used to bind login data(We can use this model in any class)
    
    var loginModel:LoginModel!
    
    var firstNameText = Variable<String>("")
    var lastNameText = Variable<String>("")
    var dobText = Variable<String>("")
    var emailText = Variable<String>("")
    var passwordText = Variable<String>("")
    var phoneNumberText = Variable<String>("")
    var referralCodeText = Variable<String>("")
    var dateOfBirth = Variable<String>("")
    var registerType = RegisterType.Default
    var paymentType = PaymentType.SelfPay
    var address = ""
    var insurance = ""
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var pharmacyId = ""
    var city = ""
    var state = ""
    var pincode = ""
    var country = ""
    var termsAndCond = 0
    
    var facebookId = ""
    var googleId = ""
    var taggedAs = ""
    
    var cardToken = ""
    var categoryId = ""
    var countryCode = ""
    var profilePicURL = ""
    
    let disposebag = DisposeBag()
    
    let rxRegisterAPICall = RegisterAPI()
    
    
    func createRegisterModel() {
        
        registerModel = RegisterModel()
        
        registerModel.firstNameText = firstNameText.value
        registerModel.lastNameText = lastNameText.value
        registerModel.emailText = emailText.value
        registerModel.passwordText = passwordText.value
        registerModel.dobText = dobText.value
        registerModel.countryCode = countryCode
        registerModel.phoneNumberText = phoneNumberText.value
        registerModel.profilePicURL = profilePicURL
        registerModel.registerType = registerType
        registerModel.referralCode = referralCodeText.value
        registerModel.registerType = registerType
        registerModel.facebookId = facebookId
        
        
        registerModel.paymentType = paymentType
        registerModel.categoryId  = categoryId
        registerModel.cardToken   = cardToken
        registerModel.insurance = insurance
        registerModel.addLine1 = address
        registerModel.pharmacyId = pharmacyId
        registerModel.googleId = googleId
        registerModel.state = state
        registerModel.city = city
        registerModel.country = country
        registerModel.pinCode = pincode
        registerModel.taggedAs = taggedAs
        registerModel.dateOdBirth = dateOfBirth.value
        registerModel.termsAndCond = termsAndCond
    }
    
    
    func registerAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        createRegisterModel()
        
        rxRegisterAPICall.registerServiceAPICall(registerModel: registerModel)
        
        if !rxRegisterAPICall.register_Response.hasObservers {
            
            rxRegisterAPICall.register_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    
    func validateEmailAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxRegisterAPICall.validateEmailServiceAPICall(emailText: emailText.value)
        
        if !rxRegisterAPICall.validateEmail_Response.hasObservers {
            
            rxRegisterAPICall.validateEmail_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        
    }
    
    
    func validatePhoneNumberAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxRegisterAPICall.validatePhoneNumberServiceAPICall(countryCode: countryCode, phoneNumber: phoneNumberText.value,triggerValue: 0)
        
        if !rxRegisterAPICall.validatePhoneNumber_Response.hasObservers {
            
            rxRegisterAPICall.validatePhoneNumber_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    
    func validateReferralCodeAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxRegisterAPICall.validateReferralCodeServiceAPICall(referralCode: referralCodeText.value)
        
        if !rxRegisterAPICall.validateReferralCode_Response.hasObservers {
            
            rxRegisterAPICall.validateReferralCode_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        
    }
    
    func facebookLoginAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        createLoginModel()
        
        let rxLoginAPICall = LoginAPI()
        
        rxLoginAPICall.loginServiceAPICall(loginModel: loginModel)
        
        if !rxLoginAPICall.login_Response.hasObservers {
            
            rxLoginAPICall.login_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    func createLoginModel() {
        
        loginModel = LoginModel()
        
        loginModel.emailText = emailText.value
        loginModel.passwordText = passwordText.value
        loginModel.loginType = LoginType.Facebook
        loginModel.facebookId = facebookId
    }
    
    func saveCurrentUserDetails(dataResponse:[String:Any]) {//Current user details
        
        let userDefaults:UserDefaults = UserDefaults.standard
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.Sid]), forKey: USER_DEFAULTS.USER.CUSTOMERID)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.FirstName]), forKey: USER_DEFAULTS.USER.FIRSTNAME)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.LastName]), forKey: USER_DEFAULTS.USER.LASTNAME)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.SessionToken]), forKey: USER_DEFAULTS.TOKEN.SESSION)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.Email]), forKey: USER_DEFAULTS.USER.EMAIL)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.CurrencyCode]), forKey: USER_DEFAULTS.USER.CURRENCYSYMBOL)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.ProfilePic]), forKey: USER_DEFAULTS.USER.PIC)
        
        AppDelegate().defaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.ReferralCode]), forKey: USER_DEFAULTS.USER.REFERRAL_CODE)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.StripeAPIKey]), forKey: USER_DEFAULTS.PAYMENT_GATEWAY.API_KEY)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.FCMTopic]), forKey: USER_DEFAULTS.USER.FCMTopic)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.ZenDeskRequesterID]), forKey: USER_DEFAULTS.USER.ZenDeskRequesterID)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.PharmacyName]), forKey: USER_DEFAULTS.USER.PHARMACY_NAME)
        
        userDefaults.set(GenericUtility.strForObj(object: dataResponse[SERVICE_RESPONSE.PharmacyId]), forKey: USER_DEFAULTS.USER.PHARMACY_ID)
        
        
        userDefaults.synchronize()
        
        //Initialize Coouch DB
        CouchDBManager.sharedInstance.createCouchDB()
        
        //Connect to MQTT
        AppDelegate().connectToMQTT()
        
    }
    
    
}

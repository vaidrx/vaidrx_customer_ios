//
//  vaiDRx_PreferredPayment_ButtonActionExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 08/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_PreferredPaymentVC {
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

}

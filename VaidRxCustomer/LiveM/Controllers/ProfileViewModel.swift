//
//  ProfileViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class ProfileViewModel {
    
    var firstNameText = Variable<String>("")
    var lastNameText = Variable<String>("")
    var musicGenereText = Variable<String>("")
    var emailText = Variable<String>("")
    var phoneNumberText = Variable<String>("")
    var aboutMeText = Variable<String>("")
    var profilePicURL = ""
    var dobText = ""
    var paymentType = PaymentType.SelfPay
    var insurance = ""
    var addLine1 = ""
    var city = ""
    var state = ""
    var addLine2 = ""
    var longitude : Double = 0.0
    var latitude : Double = 0.0
    var placeId = ""
    var pincode = ""
    var taggedAs = ""
    var country = ""
    var placeName = ""
    
    var profileRequestModel:ProfileRequestModel!
    
    let disposebag = DisposeBag()
    
    let rxProfileAPI = ProfileAPI()
    
    func logoutAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxProfileAPI.logoutServiceAPICall()
        
        if !rxProfileAPI.logout_Response.hasObservers {
            
            rxProfileAPI.logout_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    
    func getProfileDetailsAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxProfileAPI.getProfileDetailsServiceAPICall()
        
        if !rxProfileAPI.getProfile_Response.hasObservers {
            
            rxProfileAPI.getProfile_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    
    func updateProfileDetailsAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        createProfileRequestModel()
        
        rxProfileAPI.updateProfileDetailsServiceAPICall(profileRequestModel: profileRequestModel)
        
        if !rxProfileAPI.updateProfile_Response.hasObservers {
            
            rxProfileAPI.updateProfile_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    
    //updateAddressServiceAPICall
    
    func updateAddressDetailsAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        createAddressRequestModel()
        
        rxProfileAPI.updateAddressServiceAPICall(profileRequestModel: profileRequestModel)
        
        if !rxProfileAPI.updateAddress_Response.hasObservers {
            
            rxProfileAPI.updateAddress_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    func createProfileRequestModel() {
        
        profileRequestModel = ProfileRequestModel()
        
        profileRequestModel.firstName = firstNameText.value
        profileRequestModel.lastName = lastNameText.value
        profileRequestModel.generes = musicGenereText.value
        profileRequestModel.about = aboutMeText.value
        profileRequestModel.dateOfBirth = dobText
        profileRequestModel.profilePicURL = profilePicURL
        profileRequestModel.paymentType  = paymentType.rawValue
        profileRequestModel.insurance  = insurance
        profileRequestModel.addLine1 = addLine1
        profileRequestModel.country = country
        profileRequestModel.city = city
        profileRequestModel.state = state
        profileRequestModel.addLine2 = addLine2
        profileRequestModel.pincode = pincode
        profileRequestModel.placeId = placeId
        profileRequestModel.longitude = longitude
        profileRequestModel.latitude = latitude
        profileRequestModel.taggedAs = taggedAs
    }
    
    func createAddressRequestModel() {
        
        profileRequestModel = ProfileRequestModel()
        
//        profileRequestModel.firstName = firstNameText.value
//        profileRequestModel.lastName = lastNameText.value
//        profileRequestModel.generes = musicGenereText.value
//        profileRequestModel.about = aboutMeText.value
//        profileRequestModel.dateOfBirth = dobText
//        profileRequestModel.profilePicURL = profilePicURL
//        profileRequestModel.paymentType  = paymentType.rawValue
//        profileRequestModel.insurance  = insurance
        profileRequestModel.addLine1 = addLine1
        profileRequestModel.country = country
        profileRequestModel.city = city
        profileRequestModel.state = state
        profileRequestModel.addLine2 = addLine2
        profileRequestModel.pincode = pincode
        profileRequestModel.placeId = placeId
        profileRequestModel.longitude = longitude
        profileRequestModel.latitude = latitude
        profileRequestModel.taggedAs = taggedAs
    }
}

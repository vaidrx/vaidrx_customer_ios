//
//  ForgotPasswordUITextFieldDelegateExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ForgetPasswordViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        phoneNumView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)//UIColor.init(hex: "#3B5998")
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField.text?.length)! > 5 {
            animateButtonLoading(1)
        }else {
            animateButtonLoading(0)
        }
        phoneNumView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1) //UIColor.init(hex: "#BABABA")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        phoneNumView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.length)! > 5 {
            animateButtonLoading(1)
        }else {
            animateButtonLoading(0)
        }
        return true
    }
    
}

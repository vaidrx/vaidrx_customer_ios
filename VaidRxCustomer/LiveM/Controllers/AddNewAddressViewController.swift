//
//  AddressPickupViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps

protocol AddMyAddressFromMapToModel{
    func addMyAddress(address:String)
    func addFullAddress(fulladdress:[String:Any])
    func addCorrectAddress(correctAddress: AddressModel,taggedAs:String)
}

class AddNewAddressViewController:UIViewController {

    
    @IBOutlet var navigationLeftButton: UIButton!
    @IBOutlet var saveButton: UIButton!
    
    @IBOutlet var mapView: GMSMapView!
    
    @IBOutlet var topAddressView: UIViewCustom!
    
    @IBOutlet var homeButton: UIButton!
    
    @IBOutlet var workButton: UIButton!
    
    @IBOutlet var otherButton: UIButton!
    
    @IBOutlet var otherTextFieldBackView: UIView!

    @IBOutlet var otherTextField: UITextField!
    
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var mapPinBackView: UIView!
    
    @IBOutlet var mapPin: UIImageView!
    
    var addMyAddressDelegate: AddMyAddressFromMapToModel!
    
    @IBOutlet var topAddressViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var otherTextfieldBackViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var mapPinBackViewCenterYConstraint: NSLayoutConstraint!
    
    var registerViewModel: RegisterViewModel!
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    
    var pickupLatitude : Double = 0.0
    var pickupLongitude : Double = 0.0
    
    var taggedAs = "home"
    var isComingFrom = ""
    var fullAddressParams : [String:Any] = [:]
    var isFromSearch = false
    
    var selectedAddresstype:Int! = 1
    
    var isForEditing:Bool! = false
    
    var isAddressForEditing = false

    let locationObj = LocationManager.sharedInstance()
    
    var addressDetails:AddressModel! = nil
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    var isFromConfirmBookingVC:Bool = false
    
    var addNewAddressViewModel = AddNewAddressViewModel()
    
    var isAddressPickedManually = false
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!

    
    // MARK: - Default calss Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setInitialMapViewProperties()
        setInitialAddressviewProperties()
        
        showInitialSpringAnimation()
        
        if SCREEN_HEIGHT == 812 { //iPhoneX
            
            self.mapPinBackViewCenterYConstraint.constant = 24
        }
        
        
        let searchLocationVC:SearchLocationViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.searchLocationVC) as! SearchLocationViewController
        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                  subType: kCATransitionFromTop,
//                                                                  for: (self.navigationController?.view)!,
//                                                                  timeDuration: 0.3)
        
        searchLocationVC.delegate = self
        
        self.navigationController?.pushViewController(searchLocationVC, animated: false)
        
//        self.navigationController?.navigationBar.titl
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        self.navigationController?.navigationBar.barTintColor = UIColor(hex: "05C1C9") //UIColor.orange
//        self.navigationController?.navigationBar.tintColor = UIColor.white
//        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        if !isFromSearch{
            addMyAddressDelegate?.addMyAddress(address: addressLabel.text!)
            self.addMyAddressDelegate.addFullAddress(fulladdress: fullAddressParams)
        }
        
//             self.addMyAddressDelegate.addFullAddress(fulladdress: fullAddressParams)
//            addMyAddressDelegate?.addMyAddress(address: addressLabel.text!)
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
        
        
        hideNavigation()
        acessClass.acessDelegate = self
        setupGestureRecognizer()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
    }
    func hideNavigation() {
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = APP_COLOR //.clear //UIColor(hex: "05C1C9") //UIColor.clear
        self.navigationController?.navigationBar.tintColor = UIColor.white
//        self.navigationController?.navigationBar.barTintColor = UIColor.clear //UIColor(hex: "05C1C9") //UIColor.orange
//        self.navigationController?.navigationBar.tintColor = APP_COLOR
    
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
    }

    
    // MARK: - Button Actions -
    
    @IBAction func addressTypeButtonAction(_ sender: Any) {
        
        let button = sender as? UIButton
        
        switch (button?.tag)! {
            
            case 1:
                
                changeButtonState(homeButton)
                selectedAddresstype = homeButton.tag
                showNormalAddressTagFrame()

                taggedAs = "home"
                break
            
            case 2:
            
                changeButtonState(workButton)
                selectedAddresstype = workButton.tag
                showNormalAddressTagFrame()
                
                taggedAs = "office"
                break
            
            case 3:
            
                changeButtonState(otherButton)
                selectedAddresstype = otherButton.tag
                otherTextField.text? = ""
                showOtherAddressTagFrame()
            
                taggedAs = "other"
            break

            
            default:
                break
        }
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
        if selectedAddresstype == 3 {
            
            if (otherTextField.text?.length)!  > 0 {
                
                print("address in AddnewAddress")
                if !isFromSearch{
                    addMyAddressDelegate?.addMyAddress(address: addressLabel.text!)
                    self.addMyAddressDelegate.addFullAddress(fulladdress: fullAddressParams)
                }
                
//                addMyAddressDelegate?.addMyAddress(address: addressLabel.text!)
//                 self.addMyAddressDelegate.addFullAddress(fulladdress: fullAddressParams)
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
                
                 //self.saveAddressAPI()
                
            } else {
                
                Helper.showAlert(head: ALERTS.Message, message: ALERTS.SelectAddressTag)
            }
            
        } else {
            print("address in AddnewAddress next")
            if !isFromSearch{
                addMyAddressDelegate?.addMyAddress(address: addressLabel.text!)
                self.addMyAddressDelegate.addFullAddress(fulladdress: fullAddressParams)
            }
            
//             self.addMyAddressDelegate.addFullAddress(fulladdress: fullAddressParams)
//            addMyAddressDelegate?.addMyAddress(address: addressLabel.text!)
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
            
            //saveAddressAPI()
        }
        
    }
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionReveal,subType: kCATransitionFromBottom,for: (self.navigationController?.view)!,timeDuration: 0.3)
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func searchAddressButtonAction(_ sender: Any) {
        
        let searchLocationVC:SearchLocationViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.searchLocationVC) as! SearchLocationViewController
        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                  subType: kCATransitionFromTop,
//                                                                  for: (self.navigationController?.view)!,
//                                                                  timeDuration: 0.3)
        
        searchLocationVC.delegate = self
        
        self.navigationController?.pushViewController(searchLocationVC, animated: false)
        
    }
    
    
}

extension AddNewAddressViewController:SearchAddressDelegate {
    
    func searchAddressDelegateMethod(_ addressModel:AddressModel,descript:String) {
        
        isAddressPickedManually = true
        isFromSearch = true
        
        //newAddressModel = addressModel
        self.addMyAddressDelegate.addCorrectAddress(correctAddress: addressModel,taggedAs:taggedAs)
        print(descript)
        self.addressLabel.text = descript
        
        pickupLatitude = Double(addressModel.latitude)
        pickupLongitude = Double(addressModel.longitude)
        if isComingFrom != "Profile" && isComingFrom != "oldPharmacyInfo"{
            registerViewModel.longitude = pickupLongitude
            registerViewModel.latitude = pickupLatitude
        }
        
//        let fullAddressParams : [String:Any] = ["address":addressModel.fullAddress,"latitude":pickupLatitude,"logitude":pickupLongitude,"taggedAs":taggedAs]
//        addMyAddressDelegate.addFullAddress(fulladdress: fullAddressParams)
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(pickupLatitude),
                                              longitude: CLLocationDegrees(pickupLongitude),
                                              zoom: MAP_ZOOM_LEVEL)
        self.mapView.animate(to: camera)
        
    }
    
    func searchAddressCurrentLocationButtonClicked() {
        
        showCurrentLocation()
    }
    
    /// Method to show current location in MapView
    func showCurrentLocation() {
        
        let location = self.mapView.myLocation
        
        if (location != nil) {
            
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees((location?.coordinate.latitude)!),
                                                  longitude: CLLocationDegrees((location?.coordinate.longitude)!),
                                                  zoom: MAP_ZOOM_LEVEL)
            
            if isComingFrom != "Profile" && isComingFrom != "oldPharmacyInfo"{
            
                registerViewModel.longitude = (location?.coordinate.longitude)!
            
                registerViewModel.latitude = (location?.coordinate.latitude)!
            }
            self.mapView.animate(to: camera)
            
        }
        
    }
    
}

extension AddNewAddressViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
            case RequestType.addAddress.rawValue,RequestType.UpdateAddress.rawValue:
                
                saveAddressAPI()
                
                break
                
            default:
                break
        }
    }
    
}

extension AddNewAddressViewController:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}

extension AddNewAddressViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension AddNewAddressViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}

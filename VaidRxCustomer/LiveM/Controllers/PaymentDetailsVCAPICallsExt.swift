//
//  PaymentDetailsVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


extension PaymentDetailsViewController {
    
    func deleteCardAPI(cardId:String)  {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        paymentDetailsViewModel.cardId = cardId
        
        paymentDetailsViewModel.deletePaymentCardAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.deleteCard)
        }

    }
    
    func makeCardDefaultAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        paymentDetailsViewModel.cardId = cardDetails.id
        
        paymentDetailsViewModel.makePaymentCardDefaultAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.cardDefault)
        }

    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if let dataRes = dataResponse as? String {
                
                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                self.apiTag = requestType.rawValue
                
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
                    case .deleteCard:
                    
                        progressMessage = PROGRESS_MESSAGE.Deleting
                    
                    case .cardDefault:
                    
                        progressMessage = PROGRESS_MESSAGE.Saving
                    
                    default:
                    
                        break
                }
                
                self.acessClass.getAcessToken(progressMessage: progressMessage)
            }
            
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
            }
            
            break
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
                case RequestType.deleteCard:
                
                    PaymentViewController.sharedInstance().isPaymentListChanged = true
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    break
                
                case RequestType.cardDefault:
                
                    PaymentViewController.sharedInstance().isPaymentListChanged = true
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    break
                
                
                default:
                    break
            }
            
        default:
            
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                
            }
            
            
            break
        }
        
    }
    
}

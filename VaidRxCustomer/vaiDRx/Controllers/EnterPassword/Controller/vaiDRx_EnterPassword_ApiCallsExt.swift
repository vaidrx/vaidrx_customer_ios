//
//  vaiDRx_EnterPassword_ApiCallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 13/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_EnterPasswordVC {
    
    //MARK: - WebService Call -
    
    func sendRequestToForgotPasswordUsingEmail() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        forgotPasswordViewModel.countryCode = countryCode
        forgotPasswordViewModel.forgotPasswordWithEmail = forgotPasswordWithEmail
//        forgotPasswordViewModel.phoneNumberTxt = phoneNumber
        
        
        forgotPasswordViewModel.forgotPasswordAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.forgetPassword )
        }
        
    }
    
    //MARK - WebService Call -
    func sendServiceToMakeLogin(loginType:LoginType) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        loginViewModel.loginType = loginType
        
        //        if loginType == LoginType.Facebook {
        //
        //            loginViewModel.facebookId = fbUserDataModel.userId
        //        }
        
        
        loginViewModel.loginAPICall { (statusCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.signIn)
        }
        
    }
    
    //MARK: - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {

        
            if HTTPSResponseCodes.ForgotPassword(rawValue: statusCode) != nil {
                
                let responseCodes : HTTPSResponseCodes.ForgotPassword = HTTPSResponseCodes.ForgotPassword(rawValue: statusCode)!
                
                switch responseCodes
                {
                    
                case .WrongPassword://Wrong Password
                    
                    switch requestType {
                        
                    case RequestType.forgetPassword:
                        
                        break
                        
                    case RequestType.signIn:
                        
                        passwordTextFeild.text = ""
                        loginViewModel.passwordText.value = ""
                        passwordTextFeild.becomeFirstResponder()
                        
                        if errorMessage != nil {
                            
                            Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                        }
                        break
                        
                    default:
                        break
                    }

                    
                case .EmailMobileNotExist, .WrongEmail, .WrongPhoneNumber, .ToManyRequest:
                    
                    switch requestType {
                        
                    case RequestType.forgetPassword:
                        
                        Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                        forgotPasswordViewModel.emailText.value = ""
                        forgotPasswordViewModel.phoneNumberText.value = ""
//                        forgotPasswordViewModel.phoneNumberTxt = ""
                        passwordTextFeild.text = ""
                        passwordTextFeild.becomeFirstResponder()
                        break
                        
                    case RequestType.signIn:
                        Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                        passwordTextFeild.text = ""
                        passwordTextFeild.becomeFirstResponder()
                        break
                        
                    default:
                        break
                        
                    }

                    
                    
                    
                case .SuccessResponse:
                    
                    switch requestType {
                        
                    case RequestType.forgetPassword:
                        
                        
                        if let dataRes = dataResponse as? [String:Any] {
                            
                            if let sid = dataRes[SERVICE_RESPONSE.Sid] as? String {
                                
                                DDLogDebug("SID:\(sid)")
                                
                                let params:[String:Any] = [
                                    USER_DEFAULTS.USER.COUNTRY_CODE:countryCode,
                                    USER_DEFAULTS.USER.MOBILE:phoneNumber,
                                    SERVICE_RESPONSE.Sid :sid
                                ]
                                
                                performSegue(withIdentifier: SEgueIdetifiers.forgetPassToOtp, sender: params)
                            }
                            
                        }
                        
                        break
                        
                    case RequestType.signIn: //Sign In Success Response
                        
                        if let userDataResponse = dataResponse as? [String:Any] {
                            
                            loginViewModel.saveCurrentUserDetails(dataResponse: userDataResponse)
                            
                            //                            self.showSplashLoading()
                            
                            //Initialize Coouch DB
                            CouchDBManager.sharedInstance.createCouchDB()
                            
                            PaymentCardManager.sharedInstance.getCards()
                            AddressManager.sharedInstance.getAddress()
                            
                            //Connect to MQTT
                            //                                AppDelegate().connectToMQTT()
                            
                            
                            ConfigManager.sharedInstance.getConfigurationDetails()
                            
                            MQTTOnDemandAppManager.sharedInstance().subScribeToInitialTopics()
                            
                            MixPanelManager.sharedInstance.initializeMixPanel()
                            
//                            MixPanelManager.sharedInstance.userLoggedInEvent(loginType: userLoginType)
                            
                            let cancelBookingView = BookingCancelledView.sharedInstance
                            cancelBookingView.changeTitle(message: "In case of life threatening emergency please dial 911.")

                            
                            WINDOW_DELEGATE??.addSubview(cancelBookingView)
                            
                            cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                            
                            
                            UIView.animate(withDuration: 0.5,
                                           delay: 0.0,
                                           options: UIView.AnimationOptions.beginFromCurrentState,
                                           animations: {
                                            
                                            cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                                            
                            }) { (finished) in
                                
                            }
                            //Goto HomeVC
                            let menu = HelperLiveM.sharedInstance
                            menu.createMenuView()
                            
                        }
                        
                        
                        break
                        
                    default:
                        break
                    }
                    
                    break
                    
                }
                
            } else {
                
                if errorMessage != nil {
                    
                    Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
                }
            }
    }
    
}

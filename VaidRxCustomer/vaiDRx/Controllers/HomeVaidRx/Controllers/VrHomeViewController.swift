//
//  VrHomeViewController.swift
//  GoTasker
//
//  Created by 3Embed on 22/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher


var appointmentAvail = ""
class VrHomeViewController: UIViewController {
    
    @IBOutlet weak var viewTopView: UIView!
    @IBOutlet weak var profileImageBackView: UIView!
    @IBOutlet weak var imgPrifileImage: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var viadNowView: UIView!
    @IBOutlet weak var imgVaidNow: UIImageView!
    @IBOutlet weak var btnVaidNow: UIButton!
    @IBOutlet weak var vaidTodayView: UIView!
    @IBOutlet weak var rxTodayView: UIView!
    
    
    var vaidType = 0
    var goingTo = ""
    weak var delegate: LeftMenuProtocol?
  
    
    let bookingFlowViewModel = BookingFlowViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getBookings()
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        imgPrifileImage.layer.cornerRadius = imgPrifileImage.frame.height / 2
        showProfileImageView()
        
    }
    
    
//    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
//        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
//        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
//        gradientLayer.locations = [0, 1]
//        gradientLayer.frame = self.view.frame
//        
//        self.view.layer.insertSublayer(gradientLayer, at: 0)
//    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ToVaidNow" {
            
            if let dstVC = segue.destination as? HomeScreenViewController {
                
                dstVC.vaidType = self.vaidType
                dstVC.isComimgFrom = goingTo
                dstVC.appointmentAvail1 = appointmentAvail
               // dstVC.vaidType = self.vaidType
            }
            
        }
        
    }
    
    func showProfileImageView() {
        
        imgPrifileImage.layer.borderColor = RED_COLOR.cgColor
        self.imgPrifileImage.layer.borderWidth = 1
        
        imgPrifileImage.layer.borderColor = APP_COLOR.cgColor
        self.imgPrifileImage.layer.masksToBounds = true
        self.imgPrifileImage.clipsToBounds = true
        
        if !Utility.profilePic.isEmpty {
            
            imgPrifileImage.kf.setImage(with: URL(string: Utility.profilePic),
                                         placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
            },
                                         completionHandler: nil)
            
        } else {
            
            imgPrifileImage.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        applyViewChanges()
        showProfileImageView()
        self.navigationController?.navigationBar.isHidden = true
        
    }

    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func getBookings() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        Helper.showPI(_message: "Loading....")
        bookingFlowViewModel.methodName = API.METHOD.GET_BOOKINGS
        bookingFlowViewModel.getBookingDetailsAPICall { (statCode, errMsg, dataResp) in
            Helper.hidePI()
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.getParticularApptDetails)
        }
    }

    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode {
            
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
            }
            break
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType {
                
            case RequestType.getParticularApptDetails:
                
                if dataResponse != nil {
                    if let bookings = dataResponse as? [String:Any]{
                        if bookings.count > 0 {
                            
                           if let vaidDetailsDict = UserDefaults.standard.value(forKey: "VaidRequestType") as? NSDictionary {
                                self.vaidType = vaidDetailsDict.value(forKey: "vaidType") as! Int
                                goingTo = vaidDetailsDict.value(forKey: "vaidGoingTo") as! String
                                appointmentAvail = "1"
                                self.performSegue(withIdentifier: "ToVaidNow", sender: self)
                            }
                        } else {
                            appointmentAvail = ""
                        }
                    }
                }
                break
            default:
                break
            }
            break
        default:
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
            }
            break
        }
}
    
    
    func applyViewChanges(){
        viadNowView.vaidRxCustomView()
        vaidTodayView.vaidRxCustomView()
        rxTodayView.vaidRxCustomView()
    }

    @IBAction func leftMenuAction(_ sender: UIButton) {
        slideMenuController()?.openLeft()
       /*
        let storyboard = UIStoryboard(name: "VaidRxMain", bundle: nil)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "leftmenuVc") as! LeftMenuTableVC
        self.navigationController?.pushViewController(nextVC, animated: true)
        */
    }
    

    
    @IBAction func vaidNowButtonAction(_ sender: UIButton) {
        self.vaidType = 1
        goingTo = "VaidNow"
        let vaidDetails = ["vaidGoingTo":goingTo,"vaidType":self.vaidType] as NSDictionary
        UserDefaults.standard.set(vaidDetails, forKey: "VaidRequestType")
        self.performSegue(withIdentifier: "ToVaidNow", sender: self)

    }
    
    @IBAction func vaidTodayButtonAction(_ sender: UIButton) {
        self.vaidType = 2
        goingTo = "VaidToday"
        let vaidDetails = ["vaidGoingTo":goingTo,"vaidType":self.vaidType] as NSDictionary
        UserDefaults.standard.set(vaidDetails, forKey: "VaidRequestType")
        self.performSegue(withIdentifier: "ToVaidNow", sender: self)
    }
    
    @IBAction func rxTodatButtonAction(_ sender: UIButton) {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "rxTodayVieController") as! RxTodayViewController
        
        popOverVC.modalPresentationStyle = .overCurrentContext
        popOverVC.modalTransitionStyle = .crossDissolve
        
        
        popOverVC.providesPresentationContextTransitionStyle = true
        popOverVC.definesPresentationContext = true
        
        popOverVC.view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
        
        present(popOverVC, animated: true, completion: nil)
        //self.addChildViewController(popOverVC)
        //popOverVC.view.frame = self.view.frame
        //self.view.addSubview(popOverVC.view)
        //popOverVC.didMove(toParentViewController: self)
    }
    
}
extension UIView {
    
    func vaidRxCustomView() {
        layer.borderWidth = 1
        layer.borderColor = UIColor(hex: "05C1C9").cgColor
        layer.masksToBounds = false
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.18).cgColor
        layer.shadowOpacity = 0.7 //1
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowRadius = 12 //2
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        layer.shouldRasterize = true
        //layer.rasterizationScale = scale ? UIScreen.main.scale : 1

    }
}
/*
extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
 */

//
//  InsuranceTypesViewModel.swift
//  GoTasker
//
//  Created by 3Embed on 18/12/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class InsuranceTypesViewModel {
    let rxInsuranceTypeAPI = InsuranceTypeAPI()
    let disposeBag = DisposeBag()
    var profileRequestModel:ProfileRequestModel!
    
    var paymentType = PaymentType.SelfPay
    var insurance = ""
    
    func getInsuranceTypesAPICall(completion:@escaping (Int,String?,Any?) -> ()){
        rxInsuranceTypeAPI.getInsuranceTypesServiceAPICall()
        if !rxInsuranceTypeAPI.insuranceTypesResponse.hasObservers{
            rxInsuranceTypeAPI.insuranceTypesResponse.subscribe(onNext: {(response) in
                if response.data[SERVICE_RESPONSE.Error] != nil {
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: response.data[SERVICE_RESPONSE_VAIDRX.Error] as! String)
                    return
                }
                completion(response.httpStatusCode,response.data[SERVICE_RESPONSE.ErrorMessage] as? String,response.data[SERVICE_RESPONSE.DataResponse])
            }, onError: {(error) in
                
            }).disposed(by: disposeBag)
        }
    }
    
    //updateInsuranceDetailsServiceAPICall
    func updateInsuranceTypesAPICall(completion:@escaping (Int,String?,Any?) -> ()){
        
        createProfileRequestModel()
        rxInsuranceTypeAPI.updateInsuranceDetailsServiceAPICall(profileRequestModel: profileRequestModel)
        if !rxInsuranceTypeAPI.updateInsurance_Response.hasObservers{
            rxInsuranceTypeAPI.updateInsurance_Response.subscribe(onNext: {(response) in
                if response.data[SERVICE_RESPONSE.Error] != nil {
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: response.data[SERVICE_RESPONSE_VAIDRX.Error] as! String)
                    return
                }
                completion(response.httpStatusCode,response.data[SERVICE_RESPONSE.ErrorMessage] as? String,response.data[SERVICE_RESPONSE.DataResponse])
            }, onError: {(error) in
                
            }).disposed(by: disposeBag)
        }
    }
    
    func createProfileRequestModel() {
        
        profileRequestModel = ProfileRequestModel()
        
//        profileRequestModel.firstName = firstNameText.value
//        profileRequestModel.lastName = lastNameText.value
//        profileRequestModel.generes = musicGenereText.value
//        profileRequestModel.about = aboutMeText.value
//        profileRequestModel.dateOfBirth = dobText
//        profileRequestModel.profilePicURL = profilePicURL
        profileRequestModel.paymentType  = paymentType.rawValue
        profileRequestModel.insurance  = insurance
//        profileRequestModel.addLine1 = addLine1
//        profileRequestModel.country = country
//        profileRequestModel.city = city
//        profileRequestModel.state = state
//        profileRequestModel.addLine2 = addLine2
//        profileRequestModel.pincode = pincode
//        profileRequestModel.placeId = placeId
//        profileRequestModel.longitude = longitude
//        profileRequestModel.latitude = latitude
//        profileRequestModel.taggedAs = taggedAs
    }
    
}




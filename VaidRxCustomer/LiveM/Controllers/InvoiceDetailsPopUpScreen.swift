//
//  InvoiceDetailsPopUpScreen.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit


class InvoiceDetailsPopUpScreen:UIView {
    
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var eventIdLabel: UILabel!
    
    @IBOutlet var closeButton: UIButton!
    
    @IBOutlet var gigNameLabel: UILabel!
    
    @IBOutlet var gigValueLabel: UILabel!
    
    @IBOutlet var serviceFeeValueLabel: UILabel!
    
    @IBOutlet var discountValueLabel: UILabel!
    
    @IBOutlet var totalValueLabel: UILabel!
    
    @IBOutlet var signatureImageView: UIImageView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    
    var bookingDetailModel:BookingDetailsModel!
    
    
    
    //MARK: - Initial Methods -
    
    private static var share: InvoiceDetailsPopUpScreen? = nil
    
    static var sharedInstance: InvoiceDetailsPopUpScreen {
        
        if share == nil {
            
            share = Bundle(for: self).loadNibNamed("InvoiceDetailsPopUpScreen",
                                                   owner: nil,
                                                   options: nil)?.first as? InvoiceDetailsPopUpScreen
            
            share?.frame = (WINDOW_DELEGATE??.frame)!
            
        }
        
        return share!
    }

    
    
    
    @IBAction func closeButtonAction(_ sender: Any) {
        
        topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.2,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                        
        }) { (finished) in
            
            self.removeFromSuperview()
            InvoiceDetailsPopUpScreen.share = nil
            
        }

        
    }
    
}

//
//  TransferCurrtMedViewController.swift
//  vaiDRx
//
//  Created by 3Embed on 25/03/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit

class TransferCurrtMedViewController: UIViewController {
    
    
    @IBOutlet weak var popoverView: UIView!
    @IBOutlet weak var pharmacySelectedLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        confirmButton.layer.cornerRadius = confirmButton.frame.height / 2
        popoverView.layer.cornerRadius = 10
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

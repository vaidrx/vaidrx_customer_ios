//
//  PastBookingVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension PastBookingViewController {
    
    func showBookingDetails() {
        
        eventIdLabel.text = "Event id : \(bookingDetailModel.bookingId)"
        
        let startDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.bookingStartTime))
        
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "d MMM yyyy"
        eventDateLabel.text = dateFormat.string(from: startDate)
        
        
        amountLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol ,Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))!)
        
        if bookingDetailModel.providerImageURL.length > 0 {
            
            activityIndicator.startAnimating()
            
            musicianImageView.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        musicianNameLabel.text = bookingDetailModel.providerName
        
        evenNameLabel.text = GenericUtility.strForObj(object:bookingDetailModel.eventDict["name"])
        
        reviewLabel.text = String(format:"%.2f",bookingDetailModel.givenRating)
        
        eventLocationLabel.text = bookingDetailModel.address1
        
        gigNameLabel.text = String(format:"%@ %@", GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["name"]), GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["unit"]))
        
        gigValueLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol ,Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))!)
        
        
        discountValueLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol, bookingDetailModel.discountValue)
        
        
        
        let totalValue =  (Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))! + bookingDetailModel.serviceFee) - bookingDetailModel.discountValue
        
        totalValueLabel.text = bookingDetailModel.currencySymbol + String(format:" %.2f",totalValue)
        
        
//        if bookingDetailModel.paymentType.capitalized == "Card" {
//            
//            paymentTypeLabel.text = "Card ending " + bookingDetailModel.cardNumber//"Card"//Show Card Number
//            paymentTypeImageView.image = Helper.cardImage(with: "Visa")
//            paymentImageViewWidthConstraint.constant = 35
//            paymentImageViewHeightConstraint.constant = 25
//            
//        } else {
//            
//            paymentTypeLabel.text = "Cash"
//            paymentTypeImageView.image = #imageLiteral(resourceName: "cash")
//            paymentImageViewWidthConstraint.constant = 40
//            paymentImageViewHeightConstraint.constant = 25
//        }
        
        
        eventStatusLabel.text = bookingDetailModel.bookingStatusMessage
        topStatusLabel.text = bookingDetailModel.bookingStatusMessage
        
        if bookingDetailModel.bookingStatusMessage.length > 0 {
            
            topStatusView.isHidden = false
        } else {
            
            topStatusView.isHidden = true
        }
        
        updateScrollContent()
    }
    
}

//
//  LoginVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


extension LoginViewController {
    
    //MARK - WebService Call -
    func sendServiceToMakeLogin(loginType:LoginType) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        loginViewModel.loginType = loginType
        
        if loginType == LoginType.Facebook {
            
            loginViewModel.facebookId = fbUserDataModel.userId
        }
        
        
        loginViewModel.loginAPICall { (statusCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.signIn, userLoginType: loginType)
        }
        
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType, userLoginType:LoginType)
    {
        if HTTPSResponseCodes.Login(rawValue: statusCode) != nil {
        
            let responseCodes : HTTPSResponseCodes.Login = HTTPSResponseCodes.Login(rawValue: statusCode)!
                   
            switch responseCodes
            {
                case .WrongPassword://Wrong Password
                    
                    passwordTextfield.text = ""
                    loginViewModel.passwordText.value = ""
                    passwordTextfield.becomeFirstResponder()
                    
                    if errorMessage != nil {
                        
                        Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                    }
                    
                    break
                    
                    
                case .EmailNotRegistered:
                    
                    if isFBLogin! {//Email Not Registered
                        
                        //Go to SignUpVC with user details for Facebook Sign up
                        self.performSegue(withIdentifier: SEgueIdetifiers.siginToRegister , sender: nil)
                    }
                    else {
                        
                        //Wrong Email
                        emailTextField.text = ""
                        loginViewModel.emailText.value = ""
                        passwordTextfield.text = ""
                        loginViewModel.passwordText.value = ""
                        emailTextField.becomeFirstResponder()
                        
                        if errorMessage != nil {
                            
                            Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                        }

                    }
                    
                    break
                    
                    
                case .SuccessResponse:
                    
                    switch requestType
                    {
                        case RequestType.signIn: //Sign In Success Response
                            
                            if let userDataResponse = dataResponse as? [String:Any] {
                                
                                loginViewModel.saveCurrentUserDetails(dataResponse: userDataResponse)
                                
                                self.showSplashLoading()
                                
                                //Initialize Coouch DB
                                CouchDBManager.sharedInstance.createCouchDB()
                                
                                PaymentCardManager.sharedInstance.getCards()
                                AddressManager.sharedInstance.getAddress()
                                
                                //Connect to MQTT
//                                AppDelegate().connectToMQTT()
                                
                                
                                ConfigManager.sharedInstance.getConfigurationDetails()
                                
                                MQTTOnDemandAppManager.sharedInstance().subScribeToInitialTopics()
                                
                                MixPanelManager.sharedInstance.initializeMixPanel()
                                
                                MixPanelManager.sharedInstance.userLoggedInEvent(loginType: userLoginType)
                                
                                
                                //Goto HomeVC
                                let menu = HelperLiveM.sharedInstance
                                menu.createMenuView()

                            }
                            
                            
                            break
                            
                        default:
                            
                            break
                    }
                    
                    break
                
            }
            
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
        }
        
    }
    
    
    //MARK: - Splash Loading -
    func showSplashLoading() {
        
        Helper.showSplashLoading()
        
        self.perform(#selector(self.closeSplashLoading), with: nil, afterDelay: 45)
        
    }
    
    @objc func closeSplashLoading(){
        
        Helper.closeSplashLoading()
    }
    
    
}

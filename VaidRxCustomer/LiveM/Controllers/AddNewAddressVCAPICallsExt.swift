//
//  AddNewAddressVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension AddNewAddressViewController {
    
    func saveAddressAPI()  {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        var addressType:String!
        
        switch selectedAddresstype {
            
        case 1:
            
            addressType = "home"
            break
            
        case 2:
            
            addressType = "office"
            break
            
        case 3:
            
            addressType = otherTextField.text
            break
            
        default:
            
            addressType = ""
            break
        }
        
               
        addNewAddressViewModel.addressLine1 = addressLabel.text!
        addNewAddressViewModel.addressLine2 = "address2"
        addNewAddressViewModel.city = "Bangalore"
        addNewAddressViewModel.state = "Karnataka"
        addNewAddressViewModel.country = "India"
        addNewAddressViewModel.pincode = "560032"
        addNewAddressViewModel.latitude = pickupLatitude
        addNewAddressViewModel.longitude = pickupLongitude
        addNewAddressViewModel.addressType = addressType
        
        
        if isForEditing {
            
            addNewAddressViewModel.addressId = addressDetails.addressId
            
            addNewAddressViewModel.updateAddressAPICall { (statCode, errMsg, dataResp) in
                
                self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.UpdateAddress)
            }
            
        } else {
            
            addNewAddressViewModel.addressId = ""
            
            addNewAddressViewModel.addAddressAPICall { (statCode, errMsg, dataResp) in
                
                self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.addAddress)
            }

        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
                    self.apiTag = requestType.rawValue
                    
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .addAddress,.UpdateAddress:
                        
                            progressMessage = PROGRESS_MESSAGE.Saving
                        
                        default:
                        
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                }
                
                break
                
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                break
                
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType
                {
                    case RequestType.addAddress,RequestType.UpdateAddress:
                        
//                        if requestType == RequestType.UpdateAddress {
//                            
//                            if errorMessage != nil {
//                                
//                                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
//                            }
//                        }
//                        
//                        YourAddressViewController.sharedInstance().isAddressListChanged = true
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    default:
                        break
                }
                
                break
                
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }
                
                
                break
            
        }
        
    }
    
}

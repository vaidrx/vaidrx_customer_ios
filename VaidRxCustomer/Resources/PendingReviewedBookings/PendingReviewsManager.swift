//
//  PendingReviewsManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import RxCocoa
import RxSwift

class PendingReviewsManager {
    
    static let sharedInstance = PendingReviewsManager()
    
    let rxPendingReviewsAPICall = PendingReviewsAPI()
    let disposebag = DisposeBag()
    
    /// Method to call get list of saved address service API
    func getPendingReviews() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            return
        }
        
        if !rxPendingReviewsAPICall.pendingReviews_Response.hasObservers {
            
            rxPendingReviewsAPICall.pendingReviews_Response
                .subscribe(onNext: {response in
                    
                    self.WebServiceResponse(response: response, requestType: RequestType.GetPendingReviews)
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxPendingReviewsAPICall.pendingReviewsServiceAPICall()
        
    }
    
    func updatePendingReviewsinBokkingStatusManager(pendingReviewsArray:[Any]) {
        
    BookingStatusResponseManager.sharedInstance().updateNotReviewedBookingsArray(notReviewedBookings: pendingReviewsArray)
    }
    
    
    
    //MARK - WebService Response -
    func WebServiceResponse(response:APIResponseModel, requestType:RequestType)
    {
        if (response.data[SERVICE_RESPONSE.Error] != nil) {
            
            Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
            return
        }
        
        switch response.httpStatusCode
        {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.GetPendingReviews:
                
                if let dataResponse:[Any] = response.data[SERVICE_RESPONSE.DataResponse] as? [Any] {
                    
                    if dataResponse.count > 0 {
                        
                        self.updatePendingReviewsinBokkingStatusManager(pendingReviewsArray: dataResponse)
                    }
                    
                }
                
            default:
                break
            }
            break
            
        default:
            
            break
        }
        
    }
    
}

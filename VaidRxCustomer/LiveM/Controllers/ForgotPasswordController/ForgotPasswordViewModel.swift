//
//  ForgotPasswordViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


/// Forgot Password View Model Class to maintain Forgot Password View Data
class ForgotPasswordViewModel {
    
    var emailText = Variable<String>("")
    var phoneNumberText = Variable<String>("")
    var forgotPasswordWithEmail:Bool = false
    var countryCode = ""
//    var phoneNumberTxt = ""
    
    let disposebag = DisposeBag()
    
    let rxForgotPasswordAPICall = ForgotPasswordAPI()
    
    func forgotPasswordAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxForgotPasswordAPICall.forgotPasswordServiceAPICall(forgotPasswordWithEmail: forgotPasswordWithEmail,
                                                             emailText: emailText.value,
                                                             phoneNumberText: phoneNumberText.value,
                                                             countryCode: countryCode)
        
        if !rxForgotPasswordAPICall.forgotPassword_Response.hasObservers {
            
            rxForgotPasswordAPICall.forgotPassword_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        
    }
    
}


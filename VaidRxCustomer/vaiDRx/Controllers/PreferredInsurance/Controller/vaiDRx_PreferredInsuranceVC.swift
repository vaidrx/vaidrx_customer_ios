//
//  vaiDRx_PreferredInsuranceVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 01/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class vaiDRx_PreferredInsuranceVC: UIViewController {

//    var model = PreferredInsurance()
    var registerViewModel: RegisterViewModel!
    var userRegisterType = RegisterType.Default
    var isComing = ""
    var delegate: PreferredPaymentDelegate?
    let preferredInsuranceViewModel = PreferredInsuranceViewModel()
    var insuranceResponseModel: GetInsuranceResponseModel? = nil
    var profileResponseModel: ProfileResponseModel? = nil
    var profileViewModel = ProfileViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        Helper.hideNavBarShadow(vc: self)
        sendServiceToGetInsurances()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        hideNavigation()
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
    
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
    
    
    }
    
    func hideNavigation() {
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = .clear //UIColor(hex: "05C1C9") //UIColor.clear
    }
    
}

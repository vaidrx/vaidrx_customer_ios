//
//  ViewAnimation.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 19/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func animateFromBottom() {
        
        self.reloadData()
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        
        for cellObj in cells {
            let cell: UITableViewCell = cellObj as UITableViewCell
            
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
                }, completion: nil)
            index += 1
        }
    }
}

extension UIView {
    
    func dismissAlert() {
        
        let bounds = self.bounds
        let smallFrame = self.frame.insetBy(dx: self.frame.size.width / 4, dy: self.frame.size.height / 4)
        let finalFrame = smallFrame.offsetBy(dx: 0, dy: bounds.size.height)
        
        UIView.animateKeyframes(withDuration: 4, delay: 0, options: .calculationModeCubic, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5) {
                self.frame = smallFrame
            }
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                self.frame = finalFrame
            }
            }, completion: nil)
        
    }
}



extension UINavigationController {
    
    func showNavigation() {
       self.setNavigationBarHidden(false, animated: false);
    }
    
    func hideNavigation() {
        self.setNavigationBarHidden(true, animated: false);
    }

    func white() {
        
        self.navigationBar.barTintColor = UIColor.clear //UIColor(hex: "05C1C9") //UIColor.white
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor ( red: 0.1333, green: 0.1333, blue: 0.1333, alpha: 1.0 )]
    }
    
    func green() {
        
        self.navigationBar.barTintColor = UIColor ( red: 0.1569, green: 0.651, blue: 0.5412, alpha: 1.0 )
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func hideNavigationLine() {
        
        for parent in self.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView && childView.frame.size.height == 0.5) {
                    
                    let img = childView as UIView?
                    
                    img?.isHidden = true
                    
                }
            }
        }
    }
    
    func showNavigationLine() {
        
        for parent in self.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView ) {
    
                    let img = childView as UIView?
                    
                    img?.isHidden = false
                    
                }
            }
        }
    }
    
}

extension UINavigationItem {
    
    func addLeftSpace() {
        
        let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
        if (floatVersion >= 7.0) {
            let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
            negativeSpace.width = -16.0
            self.leftBarButtonItems = [negativeSpace, self.leftBarButtonItem!]
            
        }
        else {
            self.leftBarButtonItems = [self.leftBarButtonItem!]
        }
    }
    
    func addRightSpace() {
        
        let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
        if (floatVersion >= 7.0) {
            let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
            negativeSpace.width = -16.0
            self.rightBarButtonItems = [negativeSpace, self.rightBarButtonItem!]
            
        }
        else {
            self.rightBarButtonItems = [self.rightBarButtonItem!]
        }
    }
}

extension String {
    
    func localizedWith() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment:"")
    }
}





//
//  BookingEventsModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 25/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Booking Details Model
class BookingDetailsModel {
    
    var address1 = ""
    var address2 = ""
    var bookingId:Int64 = 0
    var bookingStatus = 0
    var bookingStatusMessage = ""
    var bookingMode = 0
    var bookingStartTime = 0
    var bookingEndTime = 0
    
    var providerDict:[String:Any] = [:]
    
    var providerId = ""
    var providerImageURL = ""
    var providerName = ""
    var providerPhoneNumber = ""
    var provider = ""
    var numberOfReviews = 0
    var providerRating = 4.5
    var distance = 0.0
    var providerStatus = 0
    
    var appointmentLat = 0.0
    var appointmentLong = 0.0
    
    var providerLat = 0.0
    var providerLong = 0.0
    
    var gigTimeDict:[String:Any] = [:]
    var eventDict:[String:Any] = [:]
    
    var paymentType = 0
    
    var acceptedDate = 0
    var onThewayDate = 0
    var arrivedDate = 0
    var startedDate = 0
    var invoiceRaisedDate = 0
    var completedDate = 0
    
    var serviceFee = 0.0
    
    var discountValue = 0.0
    
    var timerStatus = 1
    var timerStamp = 0
    var totalTimeElapsed = 0
    
    var signatureURL = ""
    
    var totalAmount = 0.0
    
    var cardNumber = ""
    
    var givenRating = 0.0
    
    var musicianGivenRating = 0.0
    
    var cancellationFee = 0.0
    var cancellationReason = ""
    
    var currencySymbol = ""
    
    var bookingRequestedAt = 0
    
    var mileageMatric = 0
    var insuranceId = ""
    var insuranceName = ""
    var countDown = 0
    //variables for vaidToday
    var bookingType = 0 //1-VaidNow//2-VaidToday
    var vaidTodayStatus:Int!//0-Booking Pending//1-Booking Accepted
    var cancelTime = 0
    var callDisconnected = ""
    
    init(bookingEventDetails:Any) {//My Event list booking details model
        print(Utility.sessionToken)
        if let bookingDetail = bookingEventDetails as? [String:Any] {
            
            address1 = GenericUtility.strForObj(object:bookingDetail["addLine1"])
            address2 = GenericUtility.strForObj(object:bookingDetail["addLine2"])
            bookingId = Int64(GenericUtility.strForObj(object:bookingDetail["bookingId"]))!
            bookingStatus = Int(GenericUtility.strForObj(object:bookingDetail["status"]))!
            bookingStatusMessage = GenericUtility.strForObj(object: bookingDetail["statusMsg"])
            bookingStartTime = GenericUtility.intForObj(object:bookingDetail["bookingRequestedFor"])
            bookingEndTime = GenericUtility.intForObj(object:bookingDetail["bookingExpireTime"])
            bookingRequestedAt = GenericUtility.intForObj(object:bookingDetail["bookingRequestedAt"])
            countDown = GenericUtility.intForObj(object: bookingDetail["ExpiryTimer"])
            
            providerId = GenericUtility.strForObj(object:bookingDetail["providerId"])
            providerImageURL = GenericUtility.strForObj(object:bookingDetail["profilePic"])
            providerName = GenericUtility.strForObj(object:bookingDetail["firstName"]) + " " + GenericUtility.strForObj(object:bookingDetail["lastName"])
            providerPhoneNumber = GenericUtility.strForObj(object:bookingDetail["phone"])
            
            paymentType = GenericUtility.intForObj(object:bookingDetail["paymentType"])
            callDisconnected = GenericUtility.strForObj(object:bookingDetail["callDisconnected"])
            
            
            
            if let apptLat = bookingDetail["latitude"] as? Double {
                
                appointmentLat = apptLat
            }
            
            if let mode = bookingDetail["bookingMode"] as? NSNumber {
                
                bookingMode = Int(mode)
            }
            
            if let apptLong = bookingDetail["longitude"] as? Double {
                
                appointmentLong = apptLong
            }
            
            if let coordinate = bookingDetail["proLocation"] as? [String:Any] {
                
                providerLat = Double(GenericUtility.strForObj(object: coordinate["latitude"]))!
                providerLong = Double(GenericUtility.strForObj(object: coordinate["longitude"]))!
            }
            
            if let event = bookingDetail["typeofEvent"] as? [String:Any] {
                
                eventDict = event
            }
            
            if let gigTime = bookingDetail["gigTime"] as? [String:Any] {
                
                gigTimeDict = gigTime
            }
            
            if let rating = bookingDetail["averageRating"] as? Double {
                
                providerRating = rating
            }
            
            if let distancevalue = bookingDetail["distance"] as? Double {
                
                distance = distancevalue
            }
            
            if let serviceFeeValue = bookingDetail["serviceFee"] as? Double {
                
                serviceFee = serviceFeeValue
            }
            
            if let discount = bookingDetail["discount"] as? Double {
                
                discountValue = discount
            }
            
            if let currencySym = bookingDetail["currencySymbol"] as? String {
                
                currencySymbol = currencySym
            }
            
            
            if let accountingDetails = bookingDetail["accounting"] as? [String:Any] {
                
                if let cancellFee = accountingDetails["cancellationFee"] as? Double {
                    
                    cancellationFee = cancellFee
                }

            }
            
            if let canResason = bookingDetail["cancellationReason"] as? String {
                
                cancellationReason = canResason
            }
            
            if let mileageMatricValue = bookingDetail["mileageMatric"] as? Int {
                
                mileageMatric = mileageMatricValue
            }
        }
    }
    
    init(bookingDetails:Any) {//Booking flow booking details model
        
        if let bookingDetail = bookingDetails as? [String:Any] {
            
            address1 = GenericUtility.strForObj(object:bookingDetail["addLine1"])
            address2 = GenericUtility.strForObj(object:bookingDetail["addLine2"])
            bookingId = Int64(GenericUtility.strForObj(object:bookingDetail["bookingId"]))!
            bookingStatus = GenericUtility.intForObj(object:bookingDetail["status"])
            bookingStatusMessage = GenericUtility.strForObj(object: bookingDetail["statusMsg"])
            bookingStartTime = GenericUtility.intForObj(object:bookingDetail["bookingRequestedFor"])
            bookingEndTime = GenericUtility.intForObj(object:bookingDetail["waitingTime1ForCustomer"])
            countDown = GenericUtility.intForObj(object: bookingDetail["ExpiryTimer"])
            bookingType = GenericUtility.intForObj(object: bookingDetail["bookingType"])
            bookingMode = GenericUtility.intForObj(object: bookingDetail["bookingMode"])
            vaidTodayStatus = GenericUtility.intForObj(object: bookingDetail["vaidTodayStatus"])
            if let insuranceData = bookingDetail["insurance"] as? [String: Any] {
                insuranceName = GenericUtility.strForObj(object:insuranceData["insuranceName"])
                insuranceId = GenericUtility.strForObj(object:insuranceData["insuranceId"])
            }
            paymentType = GenericUtility.intForObj(object:bookingDetail["paymentType"])
            bookingRequestedAt = GenericUtility.intForObj(object:bookingDetail["bookingRequestedAt"])

            signatureURL = GenericUtility.strForObj(object:bookingDetail["signURL"])
            callDisconnected = GenericUtility.strForObj(object:bookingDetail["callDisconnected"])
            
            if let currencySym = bookingDetail["currencySymbol"] as? String {
                
                currencySymbol = currencySym
            }
            
            if let proDetail = bookingDetail["providerDetail"] as? [String:Any] {
                
                providerDict = proDetail
                
                if let coordinate = proDetail["proLocation"] as? [String:Any] {
                    
                    providerLat = Double(GenericUtility.strForObj(object: coordinate["latitude"]))!
                    providerLong = Double(GenericUtility.strForObj(object: coordinate["longitude"]))!
                }
                
                if let rating = proDetail["averageRating"] as? Double {
                    
                    providerRating = rating
                }
                
                if let distancevalue = proDetail["distance"] as? Double {
                    
                    distance = distancevalue
                }
                
                if let reviewCount = proDetail["reviewCount"] as? Int {
                    
                    numberOfReviews = reviewCount
                }
                
                if let statusValue = proDetail["proStatus"] as? Int {
                    
                    providerStatus = statusValue
                }
                
                providerId = GenericUtility.strForObj(object:proDetail["providerId"])
                providerImageURL = GenericUtility.strForObj(object:proDetail["profilePic"])
                providerName = GenericUtility.strForObj(object:proDetail["firstName"]) + " " + GenericUtility.strForObj(object:proDetail["lastName"])
                providerPhoneNumber = GenericUtility.strForObj(object:proDetail["phone"])
                
            }
            
            if let apptLat = bookingDetail["latitude"] as? Double {
                
                appointmentLat = apptLat
            }
            
            if let apptLong = bookingDetail["longitude"] as? Double {
                
                appointmentLong = apptLong
            }
            
            if let event = bookingDetail["typeofEvent"] as? [String:Any] {
                
                eventDict = event
            }
            
            if let gigTime = bookingDetail["gigTime"] as? [String:Any] {
                
                gigTimeDict = gigTime
            }
            
//            if let signatureURLValue = bookingDetail["signURL"] as? String {
//
//                signatureURL = signatureURLValue
//            }
//
            
            
            if let statusTimeDict = bookingDetail["jobStatusLogs"] as? [String:Any] {
                
                if let acceptedDateValue = statusTimeDict["acceptedTime"] as? Int {
                    
                    acceptedDate = acceptedDateValue
                }
                
                if let onthewayDateValue = statusTimeDict["onthewayTime"] as? Int {
                    
                    onThewayDate = onthewayDateValue
                }
                
                if let arrivedDateValue = statusTimeDict["arrivedTime"] as? Int {
                    
                    arrivedDate = arrivedDateValue
                }
                
                if let startedDateValue = statusTimeDict["startedTime"] as? Int {
                    
                    startedDate = startedDateValue
                }
                
                if let completedDateValue = statusTimeDict["completedTime"] as? Int {
                    
                    completedDate = completedDateValue
                }
                
                if let invoiceRaisedDateValue = statusTimeDict["raiseInvoiceTime"] as? Int {
                    
                    invoiceRaisedDate = invoiceRaisedDateValue
                }
                
                
            }
            
            if let bookingTimerDetails = bookingDetail["bookingTimer"] as? [String:Any] {
                
                if let timerStatusValue = bookingTimerDetails["status"] as? Int {
                    
                    timerStatus = timerStatusValue
                }
                
                if let timerStampValue = bookingTimerDetails["startTimeStamp"] as? Int {
                    
                    timerStamp = timerStampValue
                }
                
                if let totalTimeElapsedValue = bookingTimerDetails["second"] as? Int {
                    
                    totalTimeElapsed = totalTimeElapsedValue
                }
                
                
            }
            
            if let cancelTimeValue = bookingDetail["cancelTime"] as? Int {
                
                cancelTime = cancelTimeValue
            }
            
            
            if let completedBookingDetails = bookingDetail["completedDetails"] as? [String:Any] {
                
                if let signatureURLValue = completedBookingDetails["signURL"] as? String {
                    
                    signatureURL = signatureURLValue
                }
                
                if let totalAmountValue = completedBookingDetails["totalAmount"] as? Double {
                    
                    totalAmount = totalAmountValue
                }
                
                
                if let givenRatingValue = completedBookingDetails["givenRating"] as? Double {
                    
                    givenRating = givenRatingValue
                }
                
                
            }
            
            if let proDetail = bookingDetail["reviewByCustomer"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    givenRating = rating
                }
            }
            
            if let proDetail = bookingDetail["reviewByProvider"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    musicianGivenRating = rating
                }
            }
            
            if let accountingDetails = bookingDetail["accounting"] as? [String:Any] {
                
                if let cardNumberValue = accountingDetails["last4"] as? String {
                    
                    cardNumber = cardNumberValue
                }
                
                if let cancellFee = accountingDetails["cancellationFee"] as? Double {
                    
                    cancellationFee = cancellFee
                }
                    
            }
            
            
            
            if let serviceFeeValue = bookingDetail["serviceFee"] as? Double {
                
                serviceFee = serviceFeeValue
            }
            
            if let discount = bookingDetail["discount"] as? Double {
                
                discountValue = discount
            }
            
            if let canResason = bookingDetail["cancellationReason"] as? String {
                
                cancellationReason = canResason
            }
            
            if let mileageMatricValue = bookingDetail["mileageMatric"] as? Int {
                
                mileageMatric = mileageMatricValue
            }
        }
    }
    

//    "bookingId": 1515399974834,
//    "bookingRequestedFor": 1515399974,
//    "bookingRequestedAt": 1515399975,
//    "status": 10,
//    "statusMsg": "Invoice Raised",
//    "providerData": {
//    "firstName": "Jessi",
//    "lastName": "Anaol",
//    "profilePic": "https://s3.amazonaws.com/livemapplication/Provider/ProfilePics/1515132543476_0_01.png",
//    "phone": "+918553112345",
//    "averageRating": 2.4,
//    "location": {
//    "longitude": 77.5894203,
//    "latitude": 13.0286075
//    }
//    },
//    "accounting": {
//    "amount": 110,
//    "cancellationFee": 70,
//    "discount": 0,
//    "total": 110,
//    "appEarning": 18.51,
//    "providerEarning": 88,
//    "pgCommissionApp": 3.4899999999999998,
//    "pgCommissionProvider": 0,
//    "totalPgCommission": 3.4899999999999998,
//    "appEarningPgComm": 22,
//    "paymentMethod": 2,
//    "chargeId": "ch_1BhvxTHH011Rur9WBLqrzmAP",
//    "last4": "4242",
//    "appCommission": 20
//    },
//    "service": {
//    "id": "5a423df4b0554924c34d5296",
//    "name": "15",
//    "unit": "MIN.",
//    "price": 110,
//    "second": 900
//    },
//    "signURL": "https://s3.amazonaws.com/livemapplication/Provider/Signature/1515399974834.jpg",
//    "bookingType": 1,
//    "currencySymbol": "₹",
//    "currency": "INR"

    
    init(bookingInvoiceDetails:Any) {//Invoice booking details model
        
        if let bookingDetail = bookingInvoiceDetails as? [String:Any] {
            
//            address1 = GenericUtility.strForObj(object:bookingDetail["addLine1"])
            bookingId = Int64(GenericUtility.strForObj(object:bookingDetail["bookingId"]))!
            bookingStatus = Int(GenericUtility.strForObj(object:bookingDetail["status"]))!
            bookingStatusMessage = GenericUtility.strForObj(object: bookingDetail["statusMsg"])
            bookingStartTime = GenericUtility.intForObj(object:bookingDetail["bookingRequestedFor"])
//            bookingEndTime = GenericUtility.intForObj(object:bookingDetail["bookingExpireTime"])
            paymentType = GenericUtility.intForObj(object:bookingDetail["paymentType"])
            bookingRequestedAt = GenericUtility.intForObj(object:bookingDetail["bookingRequestedAt"])

            signatureURL = GenericUtility.strForObj(object:bookingDetail["signURL"])
            bookingMode = GenericUtility.intForObj(object:bookingDetail["bookingMode"])
            
            if let currencySym = bookingDetail["currencySymbol"] as? String {
                
                currencySymbol = currencySym
            }
            
            if let proDetail = bookingDetail["providerData"] as? [String:Any] {
                
                providerDict = proDetail
                
                if let coordinate = proDetail["location"] as? [String:Any] {
                    
                    providerLat = Double(GenericUtility.strForObj(object: coordinate["latitude"]))!
                    providerLong = Double(GenericUtility.strForObj(object: coordinate["longitude"]))!
                }
                
                if let rating = proDetail["averageRating"] as? Double {
                    
                    providerRating = rating
                }
                
                if let ppic = proDetail["profilePic"] as? String {
                    
                    providerImageURL = ppic
                }
            
                
//                if let distancevalue = proDetail["distance"] as? Double {
//
//                    distance = distancevalue
//                }
                
//                if let reviewCount = proDetail["reviewCount"] as? Int {
//
//                    numberOfReviews = reviewCount
//                }
                
//                if let statusValue = proDetail["proStatus"] as? Int {
//
//                    providerStatus = statusValue
//                }
                
//                providerId = GenericUtility.strForObj(object:proDetail["providerId"])
                providerName = GenericUtility.strForObj(object:proDetail["firstName"]) + " " + GenericUtility.strForObj(object:proDetail["lastName"])
                providerPhoneNumber = GenericUtility.strForObj(object:proDetail["phone"])
                
            }
            
//            if let apptLat = bookingDetail["latitude"] as? Double {
//
//                appointmentLat = apptLat
//            }
//
//            if let apptLong = bookingDetail["longitude"] as? Double {
//
//                appointmentLong = apptLong
//            }
            
            if let event = bookingDetail["typeofEvent"] as? [String:Any] {
                
                eventDict = event
            }
            
            if let gigTime = bookingDetail["service"] as? [String:Any] {
                
                gigTimeDict = gigTime
            }
            
            if let totalAmountValue = bookingDetail["total"] as? Double {
                
                totalAmount = totalAmountValue
            }
            
            if let accountingDetails = bookingDetail["accounting"] as? [String:Any] {
                
                if let cardNumberValue = accountingDetails["last4"] as? String {
                    
                    cardNumber = cardNumberValue
                }
            }
            
            if let cardNumberValue = bookingDetail["last4"] as? String {
                
                cardNumber = cardNumberValue
            }
            
            
            if let proDetail = bookingDetail["reviewByCustomer"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    givenRating = rating
                }
            }
            
            if let proDetail = bookingDetail["reviewByProvider"] as? [String:Any] {
                
                if let rating = proDetail["rating"] as? Double {
                    
                    musicianGivenRating = rating
                }
            }
            
            if let serviceFeeValue = bookingDetail["serviceFee"] as? Double {
                
                serviceFee = serviceFeeValue
            }
            
            if let discount = bookingDetail["discount"] as? Double {
                
                discountValue = discount
            }
            
            if let mileageMatricValue = bookingDetail["mileageMatric"] as? Int {
                
                mileageMatric = mileageMatricValue
            }
        }
    }
}

//
//  RegisterVCButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import FacebookLogin

extension RegisterViewController {
    
    // MARK: - Action Methods -
    @IBAction func backButtonAction(_ sender: Any) {
        backButton()
    }
    
    /// sellect Country
    ///
    /// - Parameter sender: Action
    @IBAction func countryNameAction(_ sender: Any) {
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }

    
    @IBAction func facebookLoginButtonAction(_ sender: Any) {
        fbLogin = true
        let fbLoginManager:LoginManager = LoginManager.init()
        fbLoginManager.logOut()
        let fbLoginHandler = FBLoginHandler.sharedInstance()
        fbLoginHandler.delegate = self
        fbLoginHandler.login(withFacebook: self)
    }
    
    @IBAction func gmailLoginButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
        pickerButtomConstrain.constant = -500
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    /// Show Term and Condition
    ///
    /// - Parameter sender: Action
    @IBAction func termsAndConditionAction(_ sender: Any) {
        
//        self.performSegue(withIdentifier: SEgueIdetifiers.registerToTermsOfService , sender: nil)
        
        let termsAndConditionVC:TermsAndConditionViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.termsAndCondVC) as! TermsAndConditionViewController
        
        
        self.navigationController!.pushViewController(termsAndConditionVC, animated: true)

    }
    
    @IBAction func cancelPickerAction(_ sender: Any) {
        pickerButtomConstrain.constant = -500
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    
    /// Login Action Button
    ///
    /// - Parameter sender: Action
    @IBAction func signUpButtonAction(_ sender: Any) {
        
        checkFortheField()
        
    }
    
    
    @IBAction func profileImageButtonAction(_ sender: Any) {
        
        alertView()
    }
    
    @IBAction func calenderAction(_ sender: Any) {
     
        showDobDatePickerView()
    }
    
    func showDobDatePickerView() {
        
        birthdayDatePicker.setDate(selectedDOB, animated: true)
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        datePickerLabel.text = dateFormat.string(from: selectedDOB)
        
        self.view.endEditing(true)
        pickerButtomConstrain.constant = 0
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })

    }
    
    @IBAction func DonePickerAction(_ sender: Any) {
        
        selectedDOB = birthdayDatePicker.date
        yourBirthdayTextField.text = Helper.getDobStringDate(date: selectedDOB)
        
        pickerButtomConstrain.constant = -500
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
        })
        
        animateButtonLoading(checkFieldsAreValid(textField: yourBirthdayTextField, textFieldText: yourBirthdayTextField.text!))
        
    }

    
    
}

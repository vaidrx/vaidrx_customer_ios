//
//  MusicianDetailsVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension MusicianDetailsViewController {
    
    func sendRequestTogetProviderDetails() {
        
        self.tableView.isHidden = true
        self.bookButtonBackView.isHidden = true
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        musicianDetailsViewModel.providerId = providerDetailFromPrevController.providerId
        
        musicianDetailsViewModel.getMusicianDetailsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.getProviderDetails)
        }

    }
    
    func getReviewsAndRating() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            if pageNo > 0 {
                
                self.tableView.es.stopLoadingMore()
            }
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        musicianDetailsViewModel.getReviewsListAPICall(methodName: API.METHOD.REVIEW_AND_RATING_PROVIDER + "/\(providerDetailFromPrevController.providerId)" + "/\(pageNo)", pageNo: pageNo) { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.GetReviews)
        }
        
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .getProviderDetails:
                            
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)

                }
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                break
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType
                {
                    case RequestType.getProviderDetails:
                        
                        if dataResponse != nil {
                            
                            self.musicianFullDetailsModel = MusicianDetailsModel.init(musicianFullDetail: dataResponse!)
                            
//                            if musicianFullDetailsModel.reviewsArray.count == 0 {
//
//                                self.tableView.es.removeRefreshFooter()
//                            }
                            
                            showProviderDetails()

                        } else if errorMessage != nil {
                            
                            Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                        }
                        
                        
                        break
                    
                    case RequestType.GetReviews:
                        
                        if let dataRes = dataResponse as? [String : Any] {
                            
                            self.tableView.es.stopLoadingMore()

                            if let revArray = dataRes[SERVICE_RESPONSE.reviews] as? [Any] {
                                
                                if revArray.count == 0 {
                                    
                                    self.tableView.es.removeRefreshFooter()
                                    
                                } else {
                                    
                                    for eachReview in revArray {
                                        
                                        musicianFullDetailsModel.reviewsArray.append(eachReview)
                                    }
                                    
                                }
                                
                                self.tableView.reloadSections(IndexSet(integer: 6), with: .none)
                                
                                self.tableView.scrollToRow(at: IndexPath.init(row: musicianFullDetailsModel.reviewsArray.count - 1, section: 6), at: UITableView.ScrollPosition.none, animated: false)
                                
                            }
                            
                        }
                        
                    default:
                        break
                }
                
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }
                
                break
        }
        
    }
    
}

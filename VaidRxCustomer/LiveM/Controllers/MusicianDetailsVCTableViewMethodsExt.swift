//
//  MusicianDetailsVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


extension MusicianDetailsViewController:UITableViewDelegate,UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if musicianFullDetailsModel != nil {
            
            return arrayOfHeaderTitles.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0,1,2,3,4,5:
            
            return 1
            
        case 6:
            
            return musicianFullDetailsModel.reviewsArray.count
            
        default:
            
            return 0
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width, height: 35))
        
        let label = UILabel.init(frame: CGRect.init(x: 15, y: 0, width: tableView.bounds.size.width-30, height: 35))
        label.font = UIFont.init(name: FONTS.HindMedium, size: 15)
        
        if section == 6 {
            
            if musicianFullDetailsModel.reviewsArray.count == 0 {
                
                let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                return view
                
            } else {
                
                label.text = "\(arrayOfHeaderTitles[section]) (\(musicianFullDetailsModel.noOfReviews))"
            }
            
            
        } else {
            
            label.text = arrayOfHeaderTitles[section]
        }
        
        
        label.textColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
        
        view.addSubview(label)
        
        
        return view
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        var view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
        
        switch section {
            
        case 6:
            
            if musicianFullDetailsModel.reviewsArray.count == 0 {
                
//                view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width, height: 50))
//
//                let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width, height: 50))
//
//                label.textAlignment = NSTextAlignment.center
//
//                label.font = UIFont.init(name: FONTS.HindMedium, size: 15)
//
//                label.textColor = #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)
//
//                label.text = "No Reviews"
//
//                view.addSubview(label)
                
                 view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                
                return view
                
            }
            
            return view
            
        default:
            
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 6 && musicianFullDetailsModel.reviewsArray.count == 0 {
            
            return 0.01
        }
        return 35.0

    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        switch section {
            
        case 6:
            
            if musicianFullDetailsModel.reviewsArray.count == 0 {
                
                return 0.01
                
            } else {
                
                return 0.01
            }
            
        default:
            
            return 0.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            
            gigTimeCell = tableView.dequeueReusableCell(withIdentifier: "gigTimeCell", for: indexPath) as! MDGigTimeTableViewCell
            
            
            gigTimeCell.CBModel = CBModel
            
            gigTimeCell.collectionView.reloadData()
            
            
            return gigTimeCell
            
        case 1,2,4,5:
            
            let normalCell:MDNormalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! MDNormalTableViewCell
            
           
            if indexPath.section == 1 {
                
                normalCell.readMoreLabel.text = musicianFullDetailsModel.about
                
            } else if indexPath.section == 2 {
                
                normalCell.readMoreLabel.text = musicianFullDetailsModel.musicGeneres
                
            } else if indexPath.section == 4 {
                
                normalCell.readMoreLabel.text = musicianFullDetailsModel.rules
                
            } else {
                
                normalCell.readMoreLabel.text = musicianFullDetailsModel.instrument
            }
            
            normalCell.readMoreButton.tag = indexPath.section
            normalCell.readMoreLabel.numberOfLines = 0
            
            if labelNumberOflines(label: normalCell.readMoreLabel) > 3 {
                
                normalCell.readMoreButton.isHidden = false
                normalCell.readMoreButtonHeightConstraint.constant = 20
                
                if states[indexPath.section] {
                    
                    normalCell.readMoreLabel.numberOfLines = 3
                    normalCell.readMoreButton.setTitle("Read more", for: .normal)
                    
                } else {
                    
                    normalCell.readMoreLabel.numberOfLines = 0
                    normalCell.readMoreButton.setTitle("Read less", for: .normal)
                }
                
                normalCell.readMoreButton.addTarget(self, action: #selector(readMoreButtonAction(button:)), for: .touchUpInside)
                
            } else {
                
                normalCell.readMoreButton.isHidden = true
                normalCell.readMoreButtonHeightConstraint.constant = 0
                normalCell.readMoreButton.setTitle("", for: .normal)
            }
            
            return normalCell
            
        case 3:
            
            eventsCell = tableView.dequeueReusableCell(withIdentifier: "eventsCell", for: indexPath) as! MDEventsTableViewCell
            
            eventsCell.CBModel = CBModel
            
            eventsCell.collectionView.reloadData()
            
            return eventsCell
            
        default:
            
            let reviewCell:MDReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! MDReviewTableViewCell
        
            
            reviewCell.noReviewsBackView.isHidden = true
            reviewCell.reviewView.isHidden = false
            
            reviewCell.showReviewDetails(reviewModel: ReviewsModel.init(musicianReviewDetails: musicianFullDetailsModel.reviewsArray[indexPath.row]))
            //            }
            
            return reviewCell
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height:CGFloat
        switch indexPath.section {
            
        case 0:
            return 90
            
        case 1,2,4,5:
            
            /*let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width - 30, height: 0))
            
            label.font = UIFont.init(name: FONTS.HindRegular, size: 12)
            
            
            if indexPath.section == 1 {
                
                label.text = musicianFullDetailsModel.about
                
            } else if indexPath.section == 2 {
                
                label.text = musicianFullDetailsModel.musicGeneres
                
            } else if indexPath.section == 4 {
                
                label.text = musicianFullDetailsModel.rules
                
            } else {
                
                label.text = musicianFullDetailsModel.instrument
                
            }
            
            height = Helper.measureHeightLabel(label, width: SCREEN_WIDTH! - 30)
            
            return height + 35*/
            
            return UITableView.automaticDimension
            
            
        case 3:
            return 130
            
        case 6:
            
            let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width - 83, height: 0))
            
            label.font = UIFont.init(name: FONTS.HindRegular, size: 12)
            
            let reviewModel = ReviewsModel.init(musicianReviewDetails: musicianFullDetailsModel.reviewsArray[indexPath.row])
            
            label.text = reviewModel.comment
            
            height = Helper.measureHeightLabel(label, width: SCREEN_WIDTH! - 83)
            
            return height + 75
            
            
        default:
            return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
//        cell.contentView.transform = CGAffineTransform(scaleX: 1.4, y:1.4)
//
//        UIView.animate(withDuration: 0.8,
//                       delay: 0,
//                       usingSpringWithDamping: 0.5,
//                       initialSpringVelocity: 3,
//                       options: [],
//                       animations: {
//
//                        cell.contentView.transform = .identity
//
//        }, completion: { (completed) in
//
//
//        })
//
        
        
    }
    
    func labelNumberOflines(label: UILabel) -> Int {
        let textSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: CGFloat(Float.infinity))
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float(label.font.lineHeight))
        let lineCount = rHeight/charSize
        return lineCount
    }
    
    @objc func readMoreButtonAction(button:UIButton) {
        
        if states[button.tag] {
            
            states[button.tag] = false
            
        } else {
            
            states[button.tag] = true
        }
        
//        self.tableView.reloadSections(IndexSet(integer: button.tag), with: .none)
        
        self.tableView.reloadData()
        
        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: button.tag), at: UITableView.ScrollPosition.middle, animated: true)
        
    }
    
}



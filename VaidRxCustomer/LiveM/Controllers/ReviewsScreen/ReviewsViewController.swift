//
//  ReviewsViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 10/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import ESPullToRefresh


class ReviewsViewController: UIViewController {
    
    // MARK: - Outlets -
    
    // label
    @IBOutlet weak var averageStarLabel: UILabel!
    @IBOutlet weak var reviewsLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var ratingView: FloatRatingView!
    
    @IBOutlet var noReviewsMessageView: UIView!
    
    @IBOutlet weak var tableHeaderView: UIView!
    
    
    
    
    weak var delegate: LeftMenuProtocol?
    
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    var isServiceAPICalling = false
    
    var reviewsViewModel = ReviewsViewModel()
    
    var pageNo: Int = 0
    var reviewsArray:[ReviewsModel] = []
    
    var footer: ESRefreshProtocol & ESRefreshAnimatorProtocol = ESRefreshFooterAnimator.init(frame: CGRect.zero)
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = noReviewsMessageView
        
        ratingView.emptyImage = #imageLiteral(resourceName: "review_star_grey_icon")
        ratingView.fullImage = #imageLiteral(resourceName: "review_star_red_icon")
        ratingView.floatRatings = true
        
        Helper.editNavigationBar(navigationController!)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 140
        
//        self.tableView.addSubview(self.refreshControl)
        
        
        addTableViewBottomRefresh()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupGestureRecognizer()
        acessClass.acessDelegate = self
        getReviewsAndRating()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    // MARK: - Action methods -
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addTableViewBottomRefresh() {
        
        self.tableView.es.addInfiniteScrolling(animator: footer) { [weak self] in
            
            self?.handleRefresh()
        }

    }
    
    func handleRefresh() {
        
        pageNo = pageNo + 1
        getReviewsAndRating()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 180){
            
            self.tableView.es.stopLoadingMore()
//            self.footer.refreshAnimationEnd(view: self.footer as! ESRefreshComponent)
        }

    }
    
}

extension ReviewsViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension ReviewsViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}



extension ReviewsViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getAllApptDetails.rawValue:
            
            getReviewsAndRating()
            
            break
            
        default:
            break
        }
    }
    
}

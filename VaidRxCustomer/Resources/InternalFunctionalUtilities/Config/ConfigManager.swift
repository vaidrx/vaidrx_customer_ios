//
//  Config.swift
//  DayRunner
//
//  Created by 3Embed on 20/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import RxCocoa
import RxSwift

class ConfigManager: NSObject {
    
    static let sharedInstance = ConfigManager()
    
    private let defaults = UserDefaults.standard
    
    let rxConfigAPICall = ConfigAPI()
    
    let disposebag = DisposeBag()
    
    var fcmTopicsToSubscribe:[String:Any] = [:]
    
    var currencySymbol = "" {
        
        didSet {
            defaults.set(currencySymbol,
                         forKey: USER_DEFAULTS.CONFIG.CURRENCYSYMBOL)
        }
    }
    
    var currencyCode = "" {
        
        didSet {
            defaults.set(currencyCode,
                         forKey: USER_DEFAULTS.CONFIG.CURRENCYCODE)
        }
    }

    var mileage_metric = 0 {
        
        didSet {
            defaults.set(mileage_metric,
                         forKey: USER_DEFAULTS.CONFIG.DISTANCE_TYPE)
        }
    }
    
    var appVersion = 1.0 {
        
        didSet {
            
            defaults.set(appVersion,
                         forKey: USER_DEFAULTS.CONFIG.APP_VERSION)
        }
    }

    
    var customerPublishLocationInterval: Int = 12 {
        
        didSet {
            
            defaults.set(customerPublishLocationInterval,
                         forKey: USER_DEFAULTS.CONFIG.CUSTOMER_PUBLISH_LOC_TIME_INTERVAL)
        }
    }
    
    var custGoogleMapServerKey = "" {
        
        didSet {
            defaults.set(custGoogleMapServerKey,
                         forKey: USER_DEFAULTS.CONFIG.GOOGLE_MAP_SERVER_KEY)
        }
    }
    
    var paymentGatewayKey = "" {
        
        didSet {
            defaults.set(paymentGatewayKey,
                         forKey: USER_DEFAULTS.CONFIG.PAYMENT_GATEWAY_KEY)
        }
    }

    
    var categoriesArray = [[String]]() {
        
        didSet {
            defaults.set(categoriesArray,
                         forKey: USER_DEFAULTS.RATING.CATEGORIES)
        }
    }
    
    
        
    //MARK: - Get Config Details Service API Call -
    
    
    /// Method to call get Confguration details setvice API
    func getConfigurationDetails() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            return
        }
        
        if !rxConfigAPICall.config_Response.hasObservers {
            
            rxConfigAPICall.config_Response
                .subscribe(onNext: {response in
                    
                    self.webserviceResponse(response: response)
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
            
        }
        
        rxConfigAPICall.configServiceAPICall()
        
    }
    
    
    
    /// Method to parse get configuration service API Response
    ///
    /// - Parameter response: get configuration service API Response
    func webserviceResponse(response:APIResponseModel) {
        
        if (response.data[SERVICE_RESPONSE.Error] != nil) {
            
            return
        }
        
        switch response.httpStatusCode
        {
            case HTTPSResponseCodes.SuccessResponse.rawValue:
            
                if let dataResponse = response.data[SERVICE_RESPONSE.DataResponse] as? [String:Any] {
                    
                    self.updateConfigDetails(configDetails: dataResponse)
                    
                }
                
                break
            
            default:
                    
                break
        }
    
    }
    
    
    
    /// Method to update all configuration details
    ///
    /// - Parameter configDetails: configuration details from server
    func updateConfigDetails(configDetails:[String:Any]) {
        
        if let driverRating = configDetails["DriverRating"] as? [String:Any] {
            if let ratings = driverRating["en"] as? [String:Any] {
                self.categoriesArray = []
                if let oneStar = ratings["1"] as? [String] {
                    print("\(oneStar)")
                    self.categoriesArray.append(oneStar)
                }
                
                if let twoStar = ratings["2"] as? [String] {
                    print("\(twoStar)")
                    self.categoriesArray.append(twoStar)
                }
                
                if let threeStar = ratings["3"] as? [String] {
                    print("\(threeStar)")
                    self.categoriesArray.append(threeStar)
                }
                
                if let fourStar = ratings["4"] as? [String] {
                    print("\(fourStar)")
                    self.categoriesArray.append(fourStar)
                }
                
                if let fiveStar = ratings["5"] as? [String] {
                    print("\(fiveStar)")
                    self.categoriesArray.append(fiveStar)
                }
                
            }
        }
        
        if let curSymbol = configDetails["currencySymbol"] as? String {
            
            self.currencySymbol = curSymbol
        }
        
        if let currCode = configDetails["currency"] as? String {
            
            self.currencyCode = currCode
        }

//        if Int((configDetails["mileage_metric"] as? String)!) != nil {
//
//            self.mileage_metric = Int((configDetails["mileage_metric"] as? String)!)!
//        }
        
        if let appVersionDict = configDetails["appVersions"] as? [String:Any] {
            
            if let appVer = appVersionDict["ios_customer"] as? Double {
                
                self.appVersion = appVer
            }

        }
        
        if let paymentGatewayDict = configDetails["stripeTestKeys"] as? [String:Any] {//stripeLiveKeys
            
            if let paymentgateKey = paymentGatewayDict["PublishableKey"] as? String {
                
                self.paymentGatewayKey = paymentgateKey
            }
            
        }
        
        if let googleMapKeysArray = configDetails["custGoogleMapKeys"] as? [String] {
            
            if googleMapKeysArray.count > 0 {
                
                self.custGoogleMapServerKey = googleMapKeysArray.first!
            }
            
        }
        
        if let customerApiIntervalDict = configDetails["customerFrequency"] as? [String:Any] {
            
            if let customerApiInterval = customerApiIntervalDict["customerHomePageInterval"] as? Int {
                
                self.customerPublishLocationInterval = customerApiInterval
            }

        }
        
        if let pushTopics = configDetails["pushTopics"] as? [String:Any] {
            
            fcmTopicsToSubscribe = pushTopics
            MQTTOnDemandAppManager.sharedInstance().subScribeToFCMTopics()
        }
        
        self.defaults.synchronize()
        
        
        switch  Helper.getCurrentVC() {
            
            case is AddCardViewController:
                
                AddCardViewController.sharedInstance().stripeManager.stripePublishableKey = Utility.paymentGatewayAPIKey
            
            case is HomeScreenViewController, is HomeListViewController:
            
                MusiciansListManager.sharedInstance().timeInterval = Double(self.customerPublishLocationInterval)
            
            default:
                break
            
        }
        
    }

}


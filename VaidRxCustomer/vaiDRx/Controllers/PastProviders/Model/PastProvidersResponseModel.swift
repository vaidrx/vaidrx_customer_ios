//
//  PastProvidersResponseModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class PastProviderResponseModel {

    var allProviders: [[String : Any]] = []
    var provider: [String:Any] = [:]
//    var lastBookTs: Float = 1520931294;
//    var providerEmail = ""
//    var providerFirstName = ""
//    var providerId = ""
//    var providerLastName = ""
//    var providerProfilePic = "";
//    var totalNumberOfBooking = 0
    
    
    /// Creating Past Provider model from Past provider details
    ///
    /// - Parameter profileDetails: user details
    init(providerDetails: Any) {
        
        if let providerDetail = providerDetails as? [[String:Any]] {
            

            for providers in providerDetail {
                self.provider[SERVICE_RESPONSE.Provider_FName] = GenericUtility.strForObj(object: providers[SERVICE_RESPONSE.Provider_FName])
                
                self.provider[SERVICE_RESPONSE.Provider_LName] = GenericUtility.strForObj(object: providers[SERVICE_RESPONSE.Provider_LName])
                
                self.provider[SERVICE_RESPONSE.Provider_Email] = GenericUtility.strForObj(object: providers[SERVICE_RESPONSE.Provider_Email])
                
                self.provider[SERVICE_RESPONSE.Provider_Id] = GenericUtility.strForObj(object: providers[SERVICE_RESPONSE.Provider_Id])
                
                self.provider[SERVICE_RESPONSE.Provider_Pic] = GenericUtility.strForObj(object: providers[SERVICE_RESPONSE.Provider_Pic])
                
                self.provider[SERVICE_RESPONSE.Provider_BookingNum] = GenericUtility.intForObj(object: providers[SERVICE_RESPONSE.Provider_BookingNum])
                
                self.provider[SERVICE_RESPONSE.lastBooked] = GenericUtility.intForObj(object: providers[SERVICE_RESPONSE.lastBooked])
                
                allProviders.append(self.provider)
            }
        }
    }
}

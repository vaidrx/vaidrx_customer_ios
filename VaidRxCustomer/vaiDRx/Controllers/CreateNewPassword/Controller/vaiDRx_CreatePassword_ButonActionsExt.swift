//
//  vaiDRx_CreatePassword_ButonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_CreatePasswordVC {
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        
        if Helper.isValidPassword(password: passwordTextFeild.text!) {
        
            if isComingFrom == "signIn" {
                
                changePassword()
        } else if isComingFrom == "SocialSignUp" {
                performSegue(withIdentifier: "toDateOfBirthVC", sender: self)
            
            //performSegue(withIdentifier: "toSelectPayment", sender: self)
        }
            else {
                
                performSegue(withIdentifier: "toGetName", sender: self)
            }
            
        } else {
            
            Helper.alertVC(title: "Message", message: "Invalid Password, Password should consist of a capital letter, a small letter, a number & minimum of 7 characters.")
        }
    }
}

//
//  ConfirmVaidToday.swift
//  vaiDRx
//
//  Created by Rahul Sharma on 11/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

protocol VaidTodayProtocol {
    func bookingAccepted()
    func isPatientMinor(value: Int)
}



class ConfirmVaidToday: UIView {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!

    let confirmBookingViewModel = ConfirmBookingViewModel()
    var message: String = "I Dr.() has a slot  available at (Time) , would you like to confirm the same ?"
    var bookingId:Int64 = 0
    var delegate: VaidTodayProtocol? = nil
    
    var isComingFor = ""
    
    static var share: ConfirmVaidToday? = nil
    var isVisible: Bool = false
    
 
    static var sharedInstance: ConfirmVaidToday {
        
        if share == nil {
            
            share = Bundle(for: self).loadNibNamed("VaidTodayView",
                                                   owner: nil,
                                                   options: nil)?.first as? ConfirmVaidToday
            
            share?.frame = (WINDOW_DELEGATE??.frame)!
            
            
        }
        
        return share!
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        if isComingFor == "Booking" {
//
//        } else {
//            acceptButton.setTitle("Accept Booking", for: .normal)
//            rejectButton.setTitle("Reject", for: .normal)
//        }
        topView.layer.cornerRadius = 10
        rejectButton.layer.cornerRadius = 10
        acceptButton.layer.cornerRadius = 10
        BlockerAlert.instance.hide()
    }
    
    func changeTitle(message: String)  {
        
        messageLabel.text = message
    }
    
    func change(source: String)  {
        isComingFor = source
        acceptButton.setTitle("Yes", for: .normal)
        rejectButton.setTitle("No", for: .normal)
    }

    
    @IBAction func acceptBookingAction(_ sender: UIButton) {
        
        if isComingFor == "Booking"{
            self.delegate?.isPatientMinor(value: 1)
            
            topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
            
            UIView.animate(withDuration: 0.6,
                           delay: 0.2,
                           options: UIView.AnimationOptions.beginFromCurrentState,
                           animations: {
                            
                            self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                            
            }) { (finished) in
                
                self.removeFromSuperview()
                ConfirmVaidToday.share = nil
                
            }
        } else {
            self.acceptBookingAPI()
        }
    }
    
    @IBAction func rejectBookingAction(_ sender: UIButton) {
        
        if isComingFor == "Booking"{
            self.delegate?.isPatientMinor(value: 0)
            topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
            
            UIView.animate(withDuration: 0.6,
                           delay: 0.2,
                           options:UIView.AnimationOptions(rawValue: UIView.AnimationOptions.beginFromCurrentState.rawValue),
                           animations: {
                            
                            self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                            
            }) { (finished) in
                
                self.removeFromSuperview()
                ConfirmVaidToday.share = nil
                
            }
        } else {
            self.cancelBookingAPI()
        }
    }
    
    func hide() {
        
        if isVisible == true {
            isVisible = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
    
    func show() {
        isVisible == true
        UIView.animate(withDuration: 0.5,
        animations: {
        self.alpha = 1.0
        },
        completion: { (completion) in
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
        })
    }
    
}

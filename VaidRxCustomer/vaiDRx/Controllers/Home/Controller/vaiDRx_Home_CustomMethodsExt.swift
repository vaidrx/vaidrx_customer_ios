//
//  vaiDRx_Home_CustomMethodsExt.swift
//  vaiDRx
//
//  Created by Rahul Sharma on 10/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_HomeVC {
    
    // MARK: - Segue Method -
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toDataSource" {
            
            if let dstVC = segue.destination as? HomeScreenViewController {
                
                dstVC.vaidType = self.vaidType
                
            }
            
        }
        
    }

    
}

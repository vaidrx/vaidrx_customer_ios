//
//  PreferredPaymentViewModel.swift
//  vaiDRx
//
//  Created by Rahul Sharma on 05/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class PreferredPaymentViewModel {
    
    var profileRequestModel:ProfileRequestModel!
    
    var paymentType = PaymentType.SelfPay
    var insurance = ""
    var firstName = ""
    var lastNameText = ""
    var aboutMeText = ""
    var profilePicURL = ""
    var dobText = ""
    
    
    let disposebag = DisposeBag()
    let rxProfileAPI = ProfileAPI()
    
    func updateProfileDetailsAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        createProfileRequestModel()
        
        rxProfileAPI.updateProfileDetailsServiceAPICall(profileRequestModel: profileRequestModel)
        
        if !rxProfileAPI.updateProfile_Response.hasObservers {
            
            rxProfileAPI.updateProfile_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
    func createProfileRequestModel() {
        
        profileRequestModel = ProfileRequestModel()
        
        profileRequestModel.firstName = firstName
        profileRequestModel.lastName = lastNameText
        profileRequestModel.about = aboutMeText
        profileRequestModel.dateOfBirth = dobText
        profileRequestModel.profilePicURL = profilePicURL
        profileRequestModel.paymentType  = paymentType.rawValue
        profileRequestModel.insurance  = insurance
    }

}

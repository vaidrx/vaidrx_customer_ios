//
//  vaiDRx_PreferredPaymentVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 08/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol PreferredPaymentDelegate {
    
    func paymentSelected(_ selectedPayment: PaymentType, Insurance: String?)
}

class vaiDRx_PreferredPaymentVC: UIViewController {
    
    //Variable Declarations
    var registerViewModel: RegisterViewModel!
    var delegate: PreferredPaymentDelegate?
    var isComing = ""
    var profileResponseModel: ProfileResponseModel? = nil
    var profileViewModel = ProfileViewModel()
    var paymentViewModel = PreferredPaymentViewModel()
    //@IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //nextButton.isHidden = true
        Helper.hideNavBarShadow(vc: self)
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigation()
    }
    func hideNavigation() {
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = .clear //UIColor(hex: "05C1C9") //UIColor.clear
    }
    
//    @IBAction func nextButtonAction(_ sender: UIButton) {
//        print("button tapped")
//        performSegue(withIdentifier: "toPharmacyVC", sender: self)
//
//    }
    
}

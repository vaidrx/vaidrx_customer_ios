//
//  HomeVCLocationMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

extension HomeScreenViewController:LocationManagerDelegate {
    
    func didUpdateLocation(location: LocationManager) {
        
        currentLat = location.latitute
        currentLong = location.longitude
    
        appointmentLocationModel.pickupLatitude = locationObj.latitute
        appointmentLocationModel.pickupLongitude = locationObj.longitude
        
        addressLabel.text = locationObj.address
        
        appointmentLocationModel.pickupAddress = addressLabel.text!
    
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            musiciansListManager.publish(isIinitial: false)

        } else {
            
            musiciansListManager.publish(isIinitial: true)
        }
        
    
    
//        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(location.latitute),
//                                              longitude: CLLocationDegrees(location.longitude),
//                                              zoom: MAP_ZOOM_LEVEL)
//        self.mapView.animate(to: camera)
        
        let camera = GMSCameraUpdate.setTarget(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(location.latitute), longitude: CLLocationDegrees(location.longitude)), zoom: MAP_ZOOM_LEVEL)
        
        
        self.mapView.animate(with: camera)
        
        self.perform(#selector(self.showCurrentLocation), with: nil, afterDelay: 0.6)
        
    }
    
    func didFailToUpdateLocation() {
        
    }
    
    func didChangeAuthorization(authorized: Bool) {
        
    }
    
    
    /// Method to show current location in MapView
    @objc func showCurrentLocation() {
        
        let location = self.mapView.myLocation
        if (location != nil) {
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees((location?.coordinate.latitude) ?? 0.0) ,
                                                  longitude: CLLocationDegrees((location?.coordinate.longitude) ?? 0.0) ,
                                                  zoom: MAP_ZOOM_LEVEL)
            self.mapView.animate(to: camera)
        }
        
    }
    
    
    /// Get Location Details From Lat & Long
    ///
    /// - Parameter coordinate: current map position lat & Long
    func getLocationDetails(_ coordinate: CLLocationCoordinate2D) {
        
        var positionChanged = false
        
        if appointmentLocationModel.pickupLatitude != coordinate.latitude || appointmentLocationModel.pickupLongitude != coordinate.longitude {
            
            positionChanged = true
        }
        
        appointmentLocationModel.pickupLatitude = coordinate.latitude
        appointmentLocationModel.pickupLongitude =  coordinate.longitude
        
        if positionChanged {
            
            if appointmentLocationModel.bookingType == BookingType.Schedule {
                
                musiciansListManager.publish(isIinitial: false)
                
            } else {
                
                musiciansListManager.publish(isIinitial: true)
            }
        }
       
        
        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: { (gmsAddressResponse, error) in
            
            if let addressDetails = gmsAddressResponse?.firstResult() {
                
                // Address Details
                DDLogVerbose("Add New Address Address Details:\(addressDetails)")
                
                
                // Current Address
                if let currentAddress = addressDetails.lines {
                    
        
                    DDLogVerbose("Current Address: \(currentAddress)")
                    
                    var address = (currentAddress.joined(separator: ", "))
                    
                    if address.hasPrefix(", ") {
                        
                        address = address.substring(2)
                    }
                    
                    if address.hasSuffix(", ") {
                        
                        let endIndex = address.index(address.endIndex, offsetBy: -2)
                        
                        address = address.substring(to: endIndex)
                    }
                    
                    self.addressLabel.text = address

                }
                
            } else {
                
                DDLogError("Home Class Address Fetch Error: \(String(describing: error?.localizedDescription))")
            }
            
        })

        
        
        /*let geoCoder = CLGeocoder()
        let location = CLLocation.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location) {
            
            (placemarks, error) -> Void in
            
            if let arrayOfPlaces: [CLPlacemark] = placemarks as [CLPlacemark]! {
                
                // Place details
                if let placemark: CLPlacemark = arrayOfPlaces.first {
                    
                    // Address dictionary
                    print("Address Dict :\(placemark.addressDictionary!)")
                    
                    // Current Address
                    if let currentAddress = placemark.addressDictionary?["FormattedAddressLines"]  {
                        
                        print("Current Address: \(currentAddress)")
                        self.addressLabel.text = (currentAddress as? Array)?.joined(separator: ", ")
                    }
                    
                }
            }
        }*/
        
    }
    
}



extension HomeScreenViewController:SearchAddressDelegate {
    
    func searchAddressDelegateMethod(_ addressModel:AddressModel,descript:String) {
        
        addressLabel.text = GenericUtility.strForObj(object:addressModel.fullAddress)
        appointmentLocationModel.pickupAddress = addressLabel.text!
        
        if appointmentLocationModel.pickupLatitude != Double(addressModel.latitude) {
            
            needToShowProgress = true
//            noArtistMessageToShow = true
        }
        
        appointmentLocationModel.pickupLatitude = Double(addressModel.latitude)
        appointmentLocationModel.pickupLongitude = Double(addressModel.longitude)
        
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(appointmentLocationModel.pickupLatitude),
                                              longitude: CLLocationDegrees(appointmentLocationModel.pickupLongitude),
                                              zoom: MAP_ZOOM_LEVEL)
        self.mapView.animate(to: camera)
        
        CATransaction.commit()
        
        isAddressManuallyPicked = true
    }
    
    func searchAddressCurrentLocationButtonClicked() {
        
        if appointmentLocationModel.pickupLatitude != self.mapView.myLocation?.coordinate.latitude {
            
            needToShowProgress = true
//            noArtistMessageToShow = true
        }
        
        currentLocationAction(locationButtonOutlet)
    }
    
}

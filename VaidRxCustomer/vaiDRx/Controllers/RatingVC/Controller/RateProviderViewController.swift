//
//  RateProviderViewController.swift
//  LSP
//
//  Created by Rajan Singh on 13/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class RateProviderViewController: UIViewController {
    
    // MARK: - Outlets -
    // label
//    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var providerNameLabel: UILabel!
    @IBOutlet weak var providerDateTimeLabel: UILabel!
    
    @IBOutlet weak var providerRating: FloatRatingView!
//    @IBOutlet weak var workTitleLabel: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    // Image
    @IBOutlet weak var providerImage: UIImageViewX!
    @IBOutlet var starImages: [UIImageView]!
    
    // Collection View
    @IBOutlet weak var collectionView: UICollectionView!

    
    // Text View
    @IBOutlet weak var expDiscriptionTextView: UITextView!
    
    // MARK: - Variables / Constant Decleration -
    let collectionCellID = "RateProviderCollectionCell"
    
    let rateProVM = RateProviderViewModel()
    var model: BookingStatusResponseModel!
    var numOfStar = 2
    let invoiceViewModel = InvoiceViewModel()
    var bookingId: Int64 = 0
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        bookingId = model.bookingId
        providerRating.delegate = self
        rateProVM.updateRating(3)
        
        providerNameLabel.text = "\(model.providerFName) \(model.providerLName)"
        providerDateTimeLabel.text = Helper.getTheOnlyDateFromTimeStamp(timeStamp: Int64(model.statusUpdatedTime))
        titleLbl.text = "Rate your vaid"
        if !(model.musicianImageURL.isEmpty){
        providerImage.layer.borderWidth = 1.0
        providerImage.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x83D788).cgColor
        providerImage.kf.setImage(with: URL(string: model.musicianImageURL),
            placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
            options: [.transition(ImageTransition.fade(1))],
            progressBlock: { receivedSize, totalSize in
            },
            completionHandler: nil)
        } else {
            
            providerImage.layer.borderWidth = 1.0
            providerImage.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x83D788).cgColor
            providerImage.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    
    @IBAction func closeAction(_ sender: Any) {
        self.submitReviewAPI()
//        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func reviewButton(sender: UIButton!) {
        let tag = sender.tag
    }
    
}

// MARK: - Collection View Delegates -

extension RateProviderViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

// MARK: - Collection View DataSource -

extension RateProviderViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if Utility.ratingCategories.count > 0 {
            
            let numberOfRating = Utility.ratingCategories[(self.numOfStar - 1)]
            if numberOfRating.count > 0 {
                return numberOfRating.count
            }
        }
        return 5
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellID, for: indexPath) as! RateProviderCollectionViewCell
        
        var numberOfRating = [String]()
        if Utility.ratingCategories.count > 0 && self.numOfStar < Utility.ratingCategories.count {
            
            numberOfRating = Utility.ratingCategories[(self.numOfStar - 1)]
            
            cell.rateButton.setTitle(numberOfRating[indexPath.row], for: .normal)
            cell.rateButton.addTarget(self, action: #selector(reviewButton), for: .touchUpInside)
            cell.rateButton.tag = indexPath.row
        } else {
            let services = ["SERVICE", "TIMING", "COMFORT", "QUALITY", "OTHER"]

            cell.rateButton.setTitle(services[indexPath.row], for: .normal)
            cell.rateButton.addTarget(self, action: #selector(reviewButton), for: .touchUpInside)
            cell.rateButton.tag = indexPath.row
        }
        
        return cell
        
    }
    
}

extension RateProviderViewController: FloatRatingViewDelegate {
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        rateProVM.updateRating(rating)
        numOfStar = Int(rating)
        collectionView.reloadData()
    }
    
}





//
//  AddCardViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Stripe

class AddCardViewController: UIViewController, KeyboardDelegate {

    // MARK: - Outlets -
    @IBOutlet weak var paymentBackgroundView: UIView!
    
    @IBOutlet weak var countryFlag: UIImageView!
    
    @IBOutlet weak var countryName: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var doneButtonBottomConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var doneButton: UIButton!
    
//    @IBOutlet weak var scanButton: UIButtonCustom!

    // MARK: - Variable Declearation -
    
    let screenSize = UIScreen.main.bounds
    var paymentTextField:StripePaymentTextField!
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    let stripeManager = StripeManager.sharedInstance()
    
    var apiTag:Int!
    
    var selectedCountry: Country!
    
    var cardToken:String!
    
    var countryRegion: String!
    
    var addPaymentViewModel = AddPaymentViewModel()
    
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!

    
    
    static var obj:AddCardViewController? = nil
    
    class func sharedInstance() -> AddCardViewController {
    
        return obj!
    }
    
    
    // MARK: - Default Class Method -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        Helper.hideNavBarShadow(vc: self)
        setCountryName()
        
        AddCardViewController.obj = self
        
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        stripeManager.setStripeDefaultPublishableKey(publishableKey: Utility.paymentGatewayAPIKey)
        
        setPaymentTextFieldProperties()
        
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    // MARK: - setCountrycode
    func setCountryName(){
        
        let countrie = NSLocale.current.regionCode
        let coutryModel = ContryNameModelClass()
        let countries = coutryModel.getCoutryDetailsFormjsonSerial()
        
        for country in countries  {
            if country["code"] == countrie {
                countryName.text = country["name"]
                countryRegion = country["code"]
                let imagestring = countryRegion
                let imagePath = "CountryPicker.bundle/\(imagestring!).png"
                countryFlag.image = UIImage(named: imagePath)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        acessClass.acessDelegate = self
        appDelegate?.keyboardDelegate = self
        setupGestureRecognizer()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
//        self.navigationController?.view?.backgroundColor = UIColor(hex: "05C1C9") //UIColor.white //05C1C9
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 38, green: 170, blue: 167) //UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
        appDelegate?.keyboardDelegate = nil
    }
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.scrollView.contentInset.bottom = (keyboardSize?.height)!
                        self.doneButtonBottomConstrain.constant = -(keyboardSize?.height)!
                        self.scrollView.layoutIfNeeded()
                        self.view.layoutIfNeeded()
        })
    }
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.4) {
            
            self.scrollView.contentInset.bottom = 0.0
            self.doneButtonBottomConstrain.constant = 0
            self.scrollView.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }
        
    }


    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        paymentTextField.becomeFirstResponder()
    }
    
    
    // MARK: - Action methods -
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func doneAction(_ sender: Any) {
            
            if !paymentTextField.isValid {
                
                Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.MissingCardDetails)
            }
                
            else if stripeManager.defaultPublishableKey().length == 0 {
                
                Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.MissingStripeKey)
            }
            else {
                
                Helper.showPI(_message: PROGRESS_MESSAGE.Adding)
                guard let cardParams = paymentTextField?.cardParams else { return }
    //            stripeManager.createStripeCardToken(cardParams: paymentTextField.cardParams, completion: { (tokenId, error) in
                let cardParameters = STPCardParams()
                
                cardParameters.number =  cardParams.number
                cardParameters.expMonth = cardParams.expMonth as! UInt
                cardParameters.expYear = cardParams.expYear as! UInt
                cardParameters.cvc = cardParams.cvc
                    stripeManager.createStripeCardToken(cardParams: cardParameters, completion: { (tokenId, error) in
                    if (error != nil) {
                        
                        Helper.hidePI()
                        Helper.showAlert(head: ALERTS.Error, message:(error!.localizedDescription))
                    }
                    else {
                        
                        DDLogDebug("Card Token: \(String(describing: tokenId))")
                        self.cardToken = tokenId
                        self.addCardAPI(tokenId: tokenId!)
                        
                    }

                })
                
            }

        }
    
    
    
    @IBAction func scanCardAction(_ sender: Any) {
        
        let scanCardViewController = CardIOPaymentViewController.init(paymentDelegate: self as CardIOPaymentViewControllerDelegate)
        scanCardViewController?.hideCardIOLogo = true
        scanCardViewController?.disableManualEntryButtons = true
        scanCardViewController?.modalPresentationStyle = UIModalPresentationStyle.formSheet
        scanCardViewController?.title = "Scan Card"
        scanCardViewController?.navigationController?.isNavigationBarHidden = false
        
        self.present(scanCardViewController!, animated: true, completion: nil)
    }
    
    
    @IBAction func getCountryAction(_ sender: UIButton) {
        
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
}

// MARK: - CountrySelectedDelegate -
extension AddCardViewController: CountrySelectedDelegate {
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier) as! CountryNameViewController
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        dstVC.countryDelegate = self
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryFlag.image = UIImage(named: imagePath)
        
        countryName.text =  self.selectedCountry.dial_code
    }
    
}

extension AddCardViewController:CardIOPaymentViewControllerDelegate {
    //MARK: - Card IO Deleagte -
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        self.dismiss(animated: true, completion: nil)
        
        stripeManager.validateCardDetails(cardNumber: cardInfo.cardNumber,
                                        expiryMonth: cardInfo.expiryMonth,
                                        expiryYear: cardInfo.expiryYear,
                                        cvv: cardInfo.cvv,
                                        completion: { (isValid) in
                        
                                            if isValid {
                                                
                                                self.doneAction(self.doneButton)
                                                
                                            } else {
                                                
                                                Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.MissingCardDetails)
                                            }
        })
        
        
    }
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AddCardViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.addCard.rawValue:
            
            if cardToken != nil {
                
                 addCardAPI(tokenId:cardToken)
            }
            
            break
            
        default:
            break
        }
    }
    
}


extension AddCardViewController {
    
    func setPaymentTextFieldProperties() {
        
        paymentTextField = StripeManager.sharedInstance().getPaymentTextField(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenSize.size.width - 100), height: CGFloat(40)))
        
        paymentBackgroundView.addSubview(paymentTextField)
    }
    
}

extension AddCardViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension AddCardViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}


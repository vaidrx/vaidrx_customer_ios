//
//  CancelBookingAPIMethods.swift
//  LiveM
//
//  Created by Rahul Sharma on 04/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension CancelBookingScreen {
    
    /// Method to call get cancellation reasons Service API
    func getCancelReasonsAPI() {
        
        cancelBookingViewModel.getCancelBookingReasonsAPICall{ (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.getCancelReasons)
        }
        
    }
    
    
    // Method to call Cancel booking Service API
    func cancelBookingAPI() {
        
        cancelBookingViewModel.bookingId = bookingID
        cancelBookingViewModel.reasonId = arrayOfCancelReasons[selectedReasonRowTag].reasonId
        
        cancelBookingViewModel.cancelBookingAPICall{ (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.cancelBooking)
        }

    }

    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if dataResponse != nil {
                    
                    AppDelegate().defaults.set(GenericUtility.strForObj(object: dataResponse!), forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .getCancelReasons:
                            
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        case .cancelBooking:
                        
                            progressMessage = PROGRESS_MESSAGE.CancelBooking
                        
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)


                }
                
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                break

            case HTTPSResponseCodes.SuccessResponse.rawValue:
            
                switch requestType
                {
                    case RequestType.getCancelReasons:
                        
                        if let arrayOfCancelReasons = dataResponse  as? [Any] {
                            
                            showCancelReasons(cancelReasonResponse: arrayOfCancelReasons)
                        }
                        break
                        
                    case RequestType.cancelBooking:
                        
                        if errorMessage != nil {
                            
                            Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                        }
                        
                        MixPanelManager.sharedInstance.BookingCancelledEvent(bookingId: String(bookingID))
                        
                        self.closeButtonAction(self.closeButton)
                        appointmentAvail = ""
                        Helper.getCurrentVC().navigationController?.popToRootViewController(animated: false)
                        
                        UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.BookingCancelled)
                        UserDefaults.standard.synchronize()
//                        LeftMenuTableViewController.sharedInstance().changeViewController(LeftMenu.myEvent)
                        
                        break
                        
                        
                        
                    default:
                        break
                }
                
                break

            
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }

                break
        }
        
    }

}

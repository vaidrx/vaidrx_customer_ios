//
//  SetPasswordTextFieldDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension SetPasswordController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == oldPasswordTF{
            if oldPasswordTF.text?.length != 0 {
                animateButtonLoading(1)
            }
            oldPassView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }else if textField == newPassTF && textField.text?.length != 0{
            if forgetPass{
                animateButtonLoading(0)
            }else if oldPasswordTF.text?.length != 0 {
                animateButtonLoading(1)
            }
            newPassView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)//UIColor.init(hex: "#3B5998")
        }else if textField == reEnterPassTF{
            if reEnterPassTF.text?.length != 0 {
                animateButtonLoading(1)
            }
            reenterPassView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == newPassTF{
            
            if textField == oldPasswordTF{
                animateButtonLoading(1)
                reEnterPassTF.becomeFirstResponder()
                oldPassView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            
            newPassView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //UIColor.init(hex: "#BABABA")
            
        }else if textField == reEnterPassTF{
            if reEnterPassTF.text?.length != 0 && oldPasswordTF.text?.length != 0 && newPassTF.text?.length != 0 {
                animateButtonLoading(2)
            }else if reEnterPassTF.text?.length != 0 && newPassTF.text?.length != 0 {
                animateButtonLoading(2)
            }
            reenterPassView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
        
        if textField == oldPasswordTF{
            
            oldPassView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
//        if !Helper.isValidPassword(password: textField.text!)
//        {
//            Helper.alertVC(title: ALERTS.Message, message: ALERTS.PasswordInvalid)
//            textField.becomeFirstResponder()
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if reEnterPassTF.text?.length != 0 && oldPasswordTF.text?.length != 0 && newPassTF.text?.length != 0 {
            animateButtonLoading(2)
        }else if reEnterPassTF.text?.length != 0 && newPassTF.text?.length != 0 {
            animateButtonLoading(2)
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if Helper.isValidPassword(password: textField.text!)
//        {
            if textField == oldPasswordTF {
                
                newPassTF.becomeFirstResponder()
                return false
                
            } else if textField == newPassTF {
                
                reEnterPassTF.becomeFirstResponder()
                return false
                
            }else if textField == reEnterPassTF {
                
                self.view.endEditing(true)
                return false
            }
            
//        }else {
//
//            Helper.alertVC(title: ALERTS.Message, message: ALERTS.PasswordInvalid)
//            textField.becomeFirstResponder()
//            return false
//        }
        
        return true
    }
    
    
}

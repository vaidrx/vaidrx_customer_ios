//
//  PharmacySelectionViewController.swift
//  vaiDRx
//
//  Created by 3Embed on 07/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps

protocol SelectedPharmacyDelegate{
    func didSelectPharmacy(selectedData:PharmacyItems)
}

class PharmacySelectionViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var collectioViewBackView: UIView!
    @IBOutlet weak var pharmacyNearYou: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
   
    @IBOutlet weak var leftSwipeBtn: UIButton!
    
    @IBOutlet weak var rightSwipeBtn: UIButton!
    
    var scrollafter2 = 3
    
    var pharmacyData : PharmacyResponseModel!
    var pharmacyItems = Array<PharmacyItems>()
    var profileResponseModel: ProfileResponseModel!
    
    var selectedPharmacyDelegate : SelectedPharmacyDelegate?
    var registerViewModel: RegisterViewModel!
    
    var getPharmacyViewModel = GetPharmacyViewModel()
    
    let locationObj = LocationManager.sharedInstance()
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    var pickupLatitude : Double = 0.0
    var pickupLongitude : Double = 0.0
    
    var currentLat = 0.0
    var currentLong = 0.0
    var isComingFrom = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialMapViewProperties()

        self.mapView.bringSubviewToFront(topView)
        self.navigationItem.setHidesBackButton(true, animated:false)
        Helper.getIPAddress()
//        print("Stored location",registerViewModel.latitude,registerViewModel.longitude)
        getPharmacyData()
        // Do any additional setup after loading the view.
        self.rightSwipeBtn.isHidden = true
        self.leftSwipeBtn.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
       

//        self.navigationController?.navigationBar.barTintColor = APP_COLOR //UIColor(hex: "05C1C9") //UIColor.clear
        
//        getPharmacyData()
    }
    
    
    
//    func setInitialMapViewProperties() {
//
//        currentLatitude = locationObj.latitute
//        currentLongitude = locationObj.longitude
//
//        print("current locaion",currentLatitude,currentLongitude)
//        print("Stored location",registerViewModel.latitude,registerViewModel.longitude)
//    }
    
    @IBAction func currectLocationButtonAction(_ sender: UIButton) {
        showCurrentLocation()
    }
    
    // MARK: - CollectionView Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("pharmacy items count",pharmacyItems)
        
        if pharmacyItems.count >= 3 {
            self.rightSwipeBtn.isHidden = false
            self.leftSwipeBtn.isHidden = false
        } else {
            self.rightSwipeBtn.isHidden = true
            self.leftSwipeBtn.isHidden = true
        }
        
        return pharmacyItems.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PharamacyCollectionViewCell
        cell.pharmacyName.text = pharmacyItems[indexPath.row].pharmacyName
        cell.distanceLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: pharmacyItems[indexPath.row].distance, mileageMatric: 1) // mileageMatric: 0 = km, 1 = miles
        cell.pharmacyLogoImage.layer.cornerRadius = cell.pharmacyLogoImage.frame.size.height / 2
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedPharmacyDelegate?.didSelectPharmacy(selectedData: pharmacyItems[indexPath.row])
        if isComingFrom == "Profile"{
            getPharmacyViewModel.selectedPharmacyId = pharmacyItems[indexPath.row].pharmacyId
            ispharEdited = true
            pharName = pharmacyItems[indexPath.row].pharmacyName
            pharId = pharmacyItems[indexPath.row].pharmacyId
            self.navigationController?.popViewController(animated: true)
//            updatePharmacyData()
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
//        self.navigationController?.popViewController(animated: true)
    }
    
    func scrollToNextItem() {
        
        let index = IndexPath.init(row: pharmacyItems.count - 1, section: 0)
        self.collectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }
    
    func scrollToPreviousItem() {
        let index = IndexPath.init(row: 0, section: 0)
        self.collectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    func showCurrentLocation() {
        
        let location = self.mapView.myLocation
        
        if (location != nil) {
            
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees((location?.coordinate.latitude)!),
                                                  longitude: CLLocationDegrees((location?.coordinate.longitude)!),
                                                  zoom: MAP_ZOOM_LEVEL)
            self.mapView.animate(to: camera)
            
        }
    }

    @IBAction func tappedLeftBtn(_ sender: UIButton) {
        
        scrollToPreviousItem()
    }
    
    @IBAction func tappedRightBtn(_ sender: UIButton) {
        scrollToNextItem()
    }
    
}

extension PharmacySelectionViewController:GMSMapViewDelegate {
    
    /// Set Intial MapView Properties
    func setInitialMapViewProperties() {
        
        self.mapView.isMyLocationEnabled = true
        
        if locationObj.latitute != 0.0 {
            if isComingFrom != "Profile"{
            
                currentLat = registerViewModel.latitude //locationObj.latitute
                currentLong = registerViewModel.longitude //locationObj.longitude
            
//            appointmentLocationModel.pickupLatitude = locationObj.latitute
//            appointmentLocationModel.pickupLongitude = locationObj.longitude
//
            addressLabel.text = locationObj.address //locationObj.address
            print(locationObj.address)
            }
            else {
                currentLat = profileResponseModel.latitude
                currentLong = profileResponseModel.longitude
                addressLabel.text = profileResponseModel.addLine1
                print(profileResponseModel.addLine1)
            }
//            let camera = GMSCameraUpdate.setTarget(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(locationObj.latitute), longitude: CLLocationDegrees(locationObj.longitude)), zoom: MAP_ZOOM_LEVEL)
            let camera = GMSCameraUpdate.setTarget(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(currentLat), longitude: CLLocationDegrees(currentLong)), zoom: MAP_ZOOM_LEVEL)
            self.mapView.animate(with: camera)
            
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
//        if isAddressForEditing {
//
//            isAddressForEditing = false
//
//        } else if isAddressPickedManually {
//
//            isAddressPickedManually = false
//
//        } else {
        
            getLocationDetails(position.target)
//        }
        
        
    }
    
    
    // MARK: - GMSMapViewDelegate
    
//    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
//    {
        //        hideHomeScreenMapDraggingAnimation()
        
//        if isAddressManuallyPicked {//Checking Address Picked Manually (Get Address From Search Address Controller)
//
//            isAddressManuallyPicked = false
//
//        } else if isFocusedMarkerPicked {
//
//            if position.target.latitude != 0.0 {
//
//                getLocationDetails(position.target)
//            }
//
//        } else  {
//
//            if position.target.latitude != 0.0 {
//
//                getLocationDetails(position.target)
//            }
//        }
        
//    }
    
//    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    
        //        let musicianDetailsVC:MusicianDetailsViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.musicianDetailsVC) as! MusicianDetailsViewController
        //
        //        musicianDetailsVC.providerDetailFromPrevController = marker.userData as! MusicianDetailsModel
        //        savePickupLocationDetails()
        //
        //
        //        self.navigationController!.pushViewController(musicianDetailsVC, animated: true)
        
//        return true
//    }
    
    
//    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
//
//        if gesture {
//
//            //            showHomeScreenMapDraggingAnimation()
//        }
//
//    }
    
//    func showHomeScreenMapDraggingAnimation() {
    
//        self.topAddressBackViewTopConstraint.constant = -45
//        self.providerListCollectionViewBottomConstraint.constant = -130
//
//        UIView.animate(withDuration: 1.2,
//                       delay: 0.0,
//                       usingSpringWithDamping: 0.8,
//                       initialSpringVelocity: 0, options: [], animations: {
//
//                        self.topAddressBackView.layoutIfNeeded()
//                        self.collectionView.layoutIfNeeded()
//
//                        self.navigationController?.setNavigationBarHidden(true, animated: true)
//                        DDLogDebug("Hide")
//
//        })
        
//    }
    
//    func hideHomeScreenMapDraggingAnimation() {
//        
//        self.topAddressBackViewTopConstraint.constant = 20
//        self.providerListCollectionViewBottomConstraint.constant = 0
//        
//        
//        UIView.animate(withDuration: 0.8,
//                       delay: 0.0,
//                       usingSpringWithDamping: 0.8,
//                       initialSpringVelocity: 0, options: [], animations: {
//                        
//                        self.topAddressBackView.layoutIfNeeded()
//                        self.collectionView.layoutIfNeeded()
//                        
//                        self.navigationController?.setNavigationBarHidden(false, animated: true)
//                        
//                        DDLogDebug("Unhide")
//                        
//                        
//        })
//        
//        
//    }
//    
    
    
    /// Get Location Details From Lat & Long
    ///
    /// - Parameter coordinate: current map position lat & Long
    func getLocationDetails(_ coordinate: CLLocationCoordinate2D) {
        
        pickupLatitude = coordinate.latitude
        pickupLongitude =  coordinate.longitude
        
        //        let geoCoder = CLGeocoder()
        //        let location = CLLocation.init(latitude: pickupLatitude, longitude: pickupLongitude)
        
        
        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: { (gmsAddressResponse, error) in
            
            if let addressDetails = gmsAddressResponse?.firstResult() {
                
                // Address Details
                DDLogVerbose("Add New Address Address Details:\(addressDetails)")
                
                
                // Current Address
                if let currentAddress = addressDetails.lines {
                    
                    print("Current Address: \(currentAddress)")
                    
                    var address = (currentAddress.joined(separator: ", "))
                    
                    if address.hasPrefix(", ") {
                        
                        address = address.substring(2)
                    }
                    
                    if address.hasSuffix(", ") {
                        
                        let endIndex = address.index(address.endIndex, offsetBy: -2)
                        
                        address = address.substring(to: endIndex)
                    }
                    
                    
                    
//                    self.fullAddressParams  = ["address":address,"latitude":self.pickupLatitude,"logitude":self.pickupLongitude,"taggedAs":self.taggedAs,"city":addressDetails.locality ?? "","state":addressDetails.administrativeArea ?? "","country":addressDetails.country ?? "","pincode":addressDetails.postalCode ?? ""]
//                    print("fullAddressParams",self.fullAddressParams)
                    
                    //                    self.addMyAddressDelegate.addFullAddress(fulladdress: fullAddressParams)
                    self.addressLabel.text = address
                    print(address)
                    
//                    self.updatedAddressFromMap()
                }
                
                
            } else {
                
                DDLogError("Add New Address Class Address Fetch Error: \(String(describing: error?.localizedDescription))")
            }
            
        })
        
        
        
        /*geoCoder.reverseGeocodeLocation(location) {
         
         (placemarks, error) -> Void in
         
         if let arrayOfPlaces: [CLPlacemark] = placemarks as [CLPlacemark]! {
         
         // Place details
         if let placemark: CLPlacemark = arrayOfPlaces.first {
         
         // Address dictionary
         print("Address Dict :\(placemark.addressDictionary!)")
         
         // Current Address
         if let currentAddress = placemark.addressDictionary?["FormattedAddressLines"]  {
         
         print("Current Address: \(currentAddress)")
         self.addressLabel.text = (currentAddress as? Array)?.joined(separator: ", ")
         self.updatedAddressFromMap()
         }
         
         }
         }
         }*/
        
    }
    
    
}

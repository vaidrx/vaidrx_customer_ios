//
//  AddPaymentViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import Stripe


class AddPaymentViewModel {
    
    let disposebag = DisposeBag()
    var cardTokenId = ""
    
    
    func addPaymentCardAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxPaymentCardAPICall = PaymentCardAPI()
        
        if !rxPaymentCardAPICall.addPaymentCard_Response.hasObservers {
            
            rxPaymentCardAPICall.addPaymentCard_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxPaymentCardAPICall.addPaymentCardServiceAPICall(cardTokenId: cardTokenId)
    }
}

//
//  LoginVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension LoginViewController {
    
    /// initial setup View
    func initiallSetUp()  {
        
        passwordTFBottomLineView.transform = CGAffineTransform(scaleX: 0, y: 0)
        emailTFBottomLineView.transform = CGAffineTransform(scaleX: 0, y: 0)
        facebookBackView.transform = CGAffineTransform(translationX: -loginButton.frame.size.width + 50, y: 0)
        googleBackView.transform = CGAffineTransform(translationX: -loginButton.frame.size.width + 50, y: 0)
        loginBottomView.transform = CGAffineTransform(translationX: 1000, y: 0)
        
        self.loadingConstrain.constant = 0
    }
    
    /// to move back to last controller
    func backButton() {
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)

        
        self.navigationController?.popViewController(animated: false)
    }
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        navigationController?.pushViewController(dstVC, animated: false)
    }
    
    /// animate the view at bottom of text Fields
    func animateBottomView() {
        if self.passwordTFBottomLineView.transform != .identity {
            UIView.animate(withDuration: 1.4, animations: {
                self.passwordTFBottomLineView.transform = .identity
                self.emailTFBottomLineView.transform = .identity
                //                self.facbookGoogleView.transform = .identity
            })
        }
    }
    
    /// Animate Login Button Loading
    ///
    /// - Parameter value: value to decide its percent of loading
    func animateButtonLoading(_ value: Int) {
        
        self.view.layoutIfNeeded()
        
        
        switch value {
            
        case 0:
            self.loadingConstrain.constant = 0
            
        case 1:
            self.loadingConstrain.constant = self.loginButton.frame.size.width / 2
            
        case 2:
            self.loadingConstrain.constant = self.loginButton.frame.size.width
            
            
        default:
            break
        }
        
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
        
    }
    
    
    /// Validating All Input Fields
    func validateAllFields() {
        
        let email = GenericUtility.strForObj(object: emailTextField.text)
        let password = GenericUtility.strForObj(object: passwordTextfield.text)
        
        if UInt(email.length) == 0
        {
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailOrPhoneNumberMissing)
            emailTextField.becomeFirstResponder()
        }
        else if !Helper.isValidEmail(testStr: email)
        {
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailOrPhoneNumberInvalid)
            emailTextField.text = ""
            loginViewModel.emailText.value = ""
            emailTextField.becomeFirstResponder()
            
        }
        else if UInt(password.length) == 0
        {
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.PaswordMissing)
            passwordTextfield.becomeFirstResponder()
        }
        else
        {
            self.view.endEditing(true)
            
            if isFBLogin == true {
                
                animateButtonLoading(2)
                sendServiceToMakeLogin(loginType: LoginType.Facebook)
                
            } else {
                
                sendServiceToMakeLogin(loginType: LoginType.Default)
            }
            
        }
    }
    
    
    
    /// spring animation for bottomViews
    func springAnimation(){
        UIView.animate(withDuration: 0.8,
                       delay: 0.3,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0, options: [], animations: {
                        
                        self.loginBottomView.transform = .identity
                        self.facebookBackView.transform = .identity
                        
                        UIView.animate(withDuration: 1.2, delay: 0.2, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                            self.googleBackView.transform = .identity
                        })
                        
        })
    }
    
    
    func showForgotPasswordAlertActionSheet() {
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: ALERTS.ForgotPassword, message: ALERTS.ForgotPasswordMessage, preferredStyle: .actionSheet)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        let cancelActionButton = UIAlertAction(title: ALERTS.Cancel, style: .cancel) { _ in
            DDLogDebug("\(ALERTS.Cancel)")
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: ALERTS.Email, style: .default) { _ in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            
            self.performSegue(withIdentifier: SEgueIdetifiers.LoginToForgetPassordVC, sender: "Email")
        }
        
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: ALERTS.PhoneNumber, style: .default) { _ in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            
            self.performSegue(withIdentifier: SEgueIdetifiers.LoginToForgetPassordVC, sender: "PhoneNumber")
        }
        
        actionSheetControllerIOS8.addAction(deleteActionButton)
        
        newAlertWindow.rootViewController?.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    
    // MARK: - Segue Method -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier! {
            
            case SEgueIdetifiers.siginToRegister:
                
                if let registerVC = segue.destination as? RegisterViewController {
                    
                    registerVC.isFromLogin = true
                    registerVC.fbUserDataModel = fbUserDataModel
                }
                
            case SEgueIdetifiers.LoginToForgetPassordVC:
                
                TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
                
            default:
                break
        }
        
    }
    
    func shakeFields(view: [UIView]){
        for views in view {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.1
            animation.repeatCount = 5
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: views.center.x - 10, y: views.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: views.center.x + 10, y: views.center.y))
            views.layer.add(animation, forKey: "position")
        }
        
        
    }
    
    func checkFortheField() {
        var shakeView: [UIView] = []
        if Helper.isValidEmail(testStr: emailTextField.text!){
            PhoneMissingConst.constant = 0
        }else {
            shakeView.append(emailTextField)
            emailTFBottomLineView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            alertsAtTheBottom(alertText: emailTextField)
        }
        
        if (passwordTextfield.text?.count)! > 5 {
            passMissingCons.constant = 0
        }else {
            shakeView.append(passwordTextfield)
            passwordTFBottomLineView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            alertsAtTheBottom(alertText: passwordTextfield)
        }
        
        shakeFields(view: shakeView)
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
        
        self.view.resignFirstResponder()
    }
    
    func alertsAtTheBottom(alertText: UITextField) {
        switch alertText {
        case emailTextField:
            PhoneMissingConst.constant = 14
        case passwordTextfield:
            passMissingCons.constant = 14
        default:
            break
        }
        
        
        
    }
    
    
    
}

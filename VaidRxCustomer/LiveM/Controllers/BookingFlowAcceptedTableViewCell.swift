//
//  BookingFlowAcceptedTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class BookingFlowAcceptedTableViewCell:UITableViewCell {
    
    @IBOutlet var timeLabel: UILabel!
    
    @IBOutlet var statusImageView: UIImageView!
    
    @IBOutlet var statusNameLabel: UILabel!
    
    @IBOutlet var musicianImageView: UIImageView!
    
    @IBOutlet var acceptedMessagelabel: UILabel!
    
    
    @IBOutlet var callButton: UIButton!
    
    @IBOutlet var messageButton: UIButton!
    
    
    
}

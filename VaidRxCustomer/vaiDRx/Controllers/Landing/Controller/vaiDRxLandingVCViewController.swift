//
//  vaiDRxLandingVCViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 05/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS
import Kingfisher
import RxSwift
import RxCocoa

class vaiDRxLandingVCViewController: UIViewController, KeyboardDelegate {
    
//    UIGestureRecognizerDelegate

    // MARK: - Outlets
    
    //UI View
    @IBOutlet weak var buttomSignupView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var mainView: UIView!

    // Button
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var logoButton: UIButton!
    
    // Layout Constraint
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationTextBottomConstraint: NSLayoutConstraint!
    
    //UI TextFields Declaration
    @IBOutlet weak var phoneNumberTextFeild: UITextField!
   
    
    
    // UI ImageView Declaration
    @IBOutlet weak var countryFlagImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    
    // UI Label Declarations
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var enterYourNameLabel: UILabel!
    
    // MARK: - Variable Decleration -
    var selectedCountry: Country!
    var isPhoneNumberValid: Bool = false
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    let phoneUtil = NBPhoneNumberUtil()
    var countryRegion: String!
    let landingViewModel = LandingViewModel()
    let registerViewModel = RegisterViewModel()
    var loginViewModel = LoginViewModel()
    let disposeBag = DisposeBag()
    
    var countryBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

      // Helper.hideNavBarShadow(vc: self)
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        
        setCountryCode()
        Helper.shadowView(sender: buttomSignupView, width:UIScreen.main.bounds.size.width, height:buttomSignupView.frame.size.height)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        hideNavigation()
        backButton.isHidden = true
        logoButton.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        addObserveToVariables()
        appDelegate?.keyboardDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        appDelegate?.keyboardDelegate = nil
        
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
//        self.navigationController?.view?.backgroundColor = APP_COLOR
//        self.navigationController?.navigationBar.backgroundColor = APP_COLOR

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideNavigation() {
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = .clear //UIColor(hex: "05C1C9") //UIColor.clear
    }
    
    // MARK: - setCountrycode
    func setCountryCode(){
        
        let countrie = NSLocale.current.regionCode
        let coutryModel = ContryNameModelClass()
        let countries = coutryModel.getCoutryDetailsFormjsonSerial()
        
        for country in countries  {
            if country["code"] == countrie {
                countryCodeLabel.text = country["dial_code"]
                countryRegion = country["code"]
                let imagestring = countryRegion
                let imagePath = "CountryPicker.bundle/\(imagestring!).png"
                countryFlagImage.image = UIImage(named: imagePath)
            }
        }
    }
    
    func addObserveToVariables() {
        
        phoneNumberTextFeild.rx.text
            .orEmpty
            .bind(to: landingViewModel.phoneNumberText)
            .disposed(by: disposeBag)
    
        phoneNumberTextFeild.rx.text
            .orEmpty
            .bind(to: registerViewModel.phoneNumberText )
            .disposed(by: disposeBag)
        
        phoneNumberTextFeild.rx.text
            .orEmpty
            .bind(to: loginViewModel.emailText)
            .disposed(by: disposeBag)
    }
    
}

// MARK: - CountrySelectedDelegate -
extension vaiDRxLandingVCViewController: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryFlagImage.image = UIImage(named: imagePath)
        countryRegion = selectedCountry.country_code
        countryCodeLabel.text =  self.selectedCountry.dial_code
    }
    
}

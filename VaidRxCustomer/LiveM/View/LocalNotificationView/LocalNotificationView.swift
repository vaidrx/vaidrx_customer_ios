//
//  LocalNotificationView.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher


class LocalNotificationView:UIView {
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var notificationView: UIView!
    
    @IBOutlet var musicianImageView: UIImageView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var messageLabel: UILabel!
    
    @IBOutlet var closeButton: UIButton!
    
    @IBOutlet var showButton: UIButton!
    
    @IBOutlet weak var swipeUpGesture: UISwipeGestureRecognizer!
    
    @IBOutlet weak var swipeLeftGesture: UISwipeGestureRecognizer!
    
    @IBOutlet weak var swipeRightGesture: UISwipeGestureRecognizer!
    
    @IBOutlet var notificationViewTopConstraint: NSLayoutConstraint!
    
    
    
    var bookingStatusModel:BookingStatusResponseModel!
    
    
    //MARK: - Initial Methods -
    
    static var share: LocalNotificationView? = nil
    
    static var sharedInstance: LocalNotificationView {
        
        if share == nil {
            
            share = Bundle(for: self).loadNibNamed("LocalNotificationView",
                                                   owner: nil,
                                                   options: nil)?.first as? LocalNotificationView
            
            share?.frame = CGRect.init(x:0, y:0, width:(WINDOW_DELEGATE??.frame.size.width)!, height:84)
            
        }
        
        return share!
    }
    
    
    
    /// Method to close custom local notification view
    ///
    /// - Parameter sender: close button object
    @IBAction func closeButtonAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                self.notificationView.transform = CGAffineTransform(translationX: 0, y: -600)
                        
        }) { (finished) in
            
            self.removeFromSuperview()
            LocalNotificationView.share = nil
        }

        
        
    }
    
    
    
    /// show booking details button action
    ///
    /// - Parameter sender: show button object
    @IBAction func showButtonAction(_ sender: Any) {
        
        //Go to Booking Flow View Controller
//        showBookingStatusinBookingFlowScreen()
        closeButtonAction(closeButton)
        
    }
    
    
    
    /// Method to show booking status details
    func showBookingNotificationDetails(){
        
        messageLabel.text = bookingStatusModel.bookingStatusMessage
        
        if bookingStatusModel.musicianImageURL.length > 0 {
            
            musicianImageView.kf.setImage(with: URL(string: bookingStatusModel.musicianImageURL),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        notificationView.transform = CGAffineTransform(translationX: 0, y: -600)
        
        Helper.shadowView(sender: notificationView, width: SCREEN_WIDTH! - 30, height: 80)
        
    }
    
    
    
    /// Animation for notificationView
    func springAnimationForNotificationView(){
                
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.notificationView.transform = .identity
                        
        }) { (finished) in
            
            
        }

        
    }
    
    
    
    /// Method to show booking status in booking flow View controller
    func showBookingStatusinBookingFlowScreen() {
        
        if Helper.getCurrentVC() is BookingFlowViewController {
            
            var bookingFlowVC = BookingFlowViewController.sharedInstance()
            
            bookingFlowVC = BookingFlowViewController.sharedInstance()
            
            bookingFlowVC.bookingId = bookingStatusModel.bookingId
            bookingFlowVC.bookingStatus = bookingStatusModel.bookingStatus
            
            bookingFlowVC.title = "\(ALERTS.BOOKING_FLOW.EventID)" + " : " + String(format:"%d", bookingStatusModel.bookingId)
            
            //Call Service
            bookingFlowVC.getBookingDetailsAPI()
            
        }
        else {
            
//            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//            let vc = storyboard.instantiateViewController(withIdentifier: VCIdentifier.bookingFlowVC) as? BookingFlowViewController
//
//            vc?.bookingStatus = bookingStatusModel.bookingStatus
//            vc?.bookingId = bookingStatusModel.bookingId
//
//            TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                      subType: kCATransitionFromTop,
//                                                                      for: (Helper.getCurrentVC().navigationController?.view)!,
//                                                                      timeDuration: 0.3)
//
//
//            Helper.getCurrentVC().navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    
    
    /// Swipe up gesture action
    ///
    /// - Parameter sender: swipe up gesture object
    @IBAction func swipeUpGestureAction(_ sender: Any) {
        
        self.closeButtonAction(self.closeButton)
        
    }
    
    
    
    /// Swipe left gesture action
    ///
    /// - Parameter sender: swipe left gesture object
    @IBAction func swipeLeftGestureAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.notificationView.transform = CGAffineTransform(translationX: -SCREEN_WIDTH!, y: 0)
                        
        }) { (finished) in
            
            self.removeFromSuperview()
            LocalNotificationView.share = nil
            
        }

        
    }
    
    
    
    /// Swipe Right gesture action
    ///
    /// - Parameter sender: swipe right gesture object
    @IBAction func swipeRightGestureAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.notificationView.transform = CGAffineTransform(translationX: SCREEN_WIDTH!, y: 0)
                        
        }) { (finished) in
            
            self.removeFromSuperview()
            LocalNotificationView.share = nil
            
        }

        
    }


    
}


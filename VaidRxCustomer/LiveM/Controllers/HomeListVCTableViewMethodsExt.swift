//
//  HomeListVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
//import youtube_ios_player_helper

extension HomeListViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return SCREEN_HEIGHT! * 0.45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let musicianDetailsVC:MusicianDetailsViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.musicianDetailsVC) as! MusicianDetailsViewController
        
        musicianDetailsVC.providerDetailFromPrevController = arrayOfProvidersModel[indexPath.row]
        
        self.navigationController!.pushViewController(musicianDetailsVC, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        cell.contentView.transform = CGAffineTransform(scaleX: 1.4, y:1.4)
//        
//        UIView.animate(withDuration: 0.8,
//                       delay: 0,
//                       usingSpringWithDamping: 0.5,
//                       initialSpringVelocity: 3,
//                       options: [],
//                       animations: {
//                        
//                        cell.contentView.transform = .identity
//                        
//        }, completion: { (completed) in
//            
//            
//        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Dequeue the cell
        DDLogDebug("IndexPath:\(indexPath)")
        
        if indexPath.row == 2 {
            
            DDLogDebug("Row:\(indexPath.row)")
        }

        
        if let cell = self.tableView.cellForRow(at: indexPath) as? HomeListTableViewCell {
            
//            if cell.youtubePlayerView.playerState() == YTPlayerState.playing {
//
//                cell.youtubePlayerView.pauseVideo()
//            }
        }

    }
    
}

extension HomeListViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrayOfProvidersModel.count > 0 {
            
            self.scrollContentView.isHidden = false
            self.messageView.isHidden = true

        } else {
            
            self.messageLabel.text = musiciansListManager.errMessageString
            self.scrollContentView.isHidden = true
            self.messageView.isHidden = false
        }
        
        return arrayOfProvidersModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: HomeListTableViewCell!
        let musicianDetails = arrayOfProvidersModel[indexPath.row]
        
        
        cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! HomeListTableViewCell
        
        cell.nameLabel.text = musicianDetails.firstName.capitalized + " " + musicianDetails.lastName.capitalized
        
        cell.ratingView.emptyImage = #imageLiteral(resourceName: "review_star_grey_icon")
        cell.ratingView.fullImage = #imageLiteral(resourceName: "review_star_red_icon")
        cell.ratingView.floatRatings = true
        
        if musicianDetails.status == 0 {
            
            cell.statusImageView.image = #imageLiteral(resourceName: "offline_big")
            
        } else {
            
            cell.statusImageView.image = #imageLiteral(resourceName: "online_big")
        }
        
        
        Helper.setShadowFor(cell.topView, andWidth: self.view.bounds.size.width - 20, andHeight: (SCREEN_HEIGHT! * 0.45) - 10)
        
        
        cell.ratingView.rating = Float(musicianDetails.overallRating)
        
        cell.defaultImageView.isHidden = false
        cell.activityIndicator.startAnimating()
//        cell.youtubePlayerView.isHidden = true
        
        cell.isHidden = false
        
        
        cell.priceLabel.text = String(format:"%@ %.2f", musicianDetails.currencySymbol, musicianDetails.amount)
        
        cell.distanceLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: musicianDetails.distance, mileageMatric: musicianDetails.mileageMatric)
        
//        cell.youtubePlayerView.tag = indexPath.row
        
        let youtubeVideoId = Helper.extractYoutubeIdFromLink(link:musicianDetails.youtubeVideoLink)
        
        if !(youtubeVideoId?.isEmpty)! {
            
            let dict = [
                "playsinline" : 1,
                "showinfo" : 0,
                "controls" : 1,
                "modestbranding": 0,
                "enablejsapi":1,
                "autohide":2,
                "rel":0,
                "origin" : "http://www.youtube.com",
                ] as [String : Any]
            
            //liwCttfeJ7E
            //C-7yMcSi6_8
            
            cell.youtubeVideoURL = youtubeVideoId!
            
//            let result = cell.youtubePlayerView.load(withVideoId: youtubeVideoId!, playerVars: dict)
//
//            cell.youtubePlayerView.webView?.allowsInlineMediaPlayback = true
            
//            DDLogVerbose("Result = \(result)")
            
//            cell.youtubePlayerView.delegate = self
            
            
        } else {
            
            cell.activityIndicator.stopAnimating()
            cell.youtubeVideoURL = ""
        }
        
        return cell
        
        
        
    }
    
}

//extension HomeListViewController:YTPlayerViewDelegate {
//    
//    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
//        
//        if isVCisDispappeared {
//            
//            if state == YTPlayerState.buffering || state == YTPlayerState.playing {
//                
//                playerView.pauseVideo()
//            }
//        }
//    }
//    
//    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
//        
//        if isVCisDispappeared {
//            
//            if playerView.playerState() == YTPlayerState.buffering || playerView.playerState() == YTPlayerState.playing {
//                
//                playerView.pauseVideo()
//            }
//        }
//    }
//    
//    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
//        
//    }
//    
//    func playerView(_ playerView: YTPlayerView, didChangeTo quality: YTPlaybackQuality) {
//        
//        if isVCisDispappeared {
//            
//            if playerView.playerState() == YTPlayerState.buffering || playerView.playerState() == YTPlayerState.playing {
//                
//                playerView.pauseVideo()
//            }
//        }
//    }
//    
//    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
//        
//        if let cell = self.tableView.cellForRow(at: IndexPath.init(row: playerView.tag, section: 0)) as? HomeListTableViewCell {
//            
//            cell.defaultImageView.isHidden = true
//            cell.activityIndicator.stopAnimating()
//            cell.youtubePlayerView.isHidden = false
//            
//        }
//        
//    }
//}

//
//  vaiDRxLandingCustomMethodsVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRxLandingVCViewController {
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier) as! CountryNameViewController
        
//        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        dstVC.countryDelegate = self
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    func checkFortheField() {
        
        var shakeView: [UIView] = []
        isPhoneNumberValid = true
        if (phoneNumberTextFeild.text?.count)! > 5 && isPhoneNumberValid {
            
            
            do {
                ///mobileNumber Validation
                let phoneNumber = try phoneUtil.parse(phoneNumberTextFeild.text!, defaultRegion: countryRegion)
                
                let validPhoneNumber: Bool = phoneUtil.isValidNumber(phoneNumber)
                if validPhoneNumber == true
                {
                    sendRequestToValidatePhoneNumber()
                }
                else
                {
                    mainViewBottomConstraint.constant = 0
                    //        self.navigationController?.setNavigationBarHidden(true, animated: true)
                    nextButton.isHidden     = true
                    nextButton.isEnabled    = false
                    phoneView.isHidden      = true
                    titleLabel.text         = "Login/Register to vaiDRx"
                    logoImage.isHidden      = false
                    phoneNumberTextFeild.resignFirstResponder()
                    mainView.backgroundColor = UIColor.white
                    phoneView.backgroundColor = UIColor.white
                    phoneNumberTextFeild.textColor = APP_COLOR
                    countryCodeLabel.textColor = APP_COLOR
                    titleLabel.textColor = APP_COLOR
                    enterYourNameLabel.textColor = APP_COLOR
                    if phoneNumberTextFeild.text?.count != 0 {
                        enterYourNameLabel.isHidden = true
                    }
                    else{
                        enterYourNameLabel.isHidden = false
                    }
                    self.view.layoutIfNeeded()
                    Helper.alertVC(title: "Message", message: "Please Enter a Valid Mobile Number")
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            
            //            phoneMissingHeight.constant = 0
            //            updateHeight(isIncrese: true)
            
        }else {
            
            mainViewBottomConstraint.constant = 0
            //        self.navigationController?.setNavigationBarHidden(true, animated: true)
            nextButton.isHidden     = true
            nextButton.isEnabled    = false
            phoneView.isHidden      = true
            titleLabel.text         = "Login/Register to vaiDRx"
            logoImage.isHidden      = false
            phoneNumberTextFeild.resignFirstResponder()
            mainView.backgroundColor = UIColor.white
            phoneView.backgroundColor = UIColor.white
            phoneNumberTextFeild.textColor = APP_COLOR
            countryCodeLabel.textColor = APP_COLOR
            titleLabel.textColor = APP_COLOR
            enterYourNameLabel.textColor = APP_COLOR
            if phoneNumberTextFeild.text?.count != 0 {
                enterYourNameLabel.isHidden = true
            }
            else{
                enterYourNameLabel.isHidden = false
            }
            self.view.layoutIfNeeded()
            
            Helper.alertVC(title: "Message", message: "Entered Phone Number is not valid.")
            shakeView.append(phoneNumberTextFeild)
            //            phoneNumberView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
//                        alertsAtTheBottom(alertText: phoneNumberTextFeild)
        }
    }
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        countryBtn.frame = CGRect(x: 15, y: 165, width: 63, height: 30)
        countryBtn.backgroundColor = UIColor.clear
        countryBtn.addTarget(self, action: #selector(self.countryButtonClicked), for: .touchUpInside)
        self.view.addSubview(countryBtn)
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        mainViewBottomConstraint.constant = (self.view.frame.size.height - 244)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        nextButton.isHidden     = false
        nextButton.isEnabled    = true
        phoneView.isHidden      = false
        //titleLabelUIResponder.keyboardFrameEndUserInfoKeyobile number"
        logoImage.isHidden      = true
        backButton.isHidden     = false
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.nextButtonBottomConstraint.constant = ((keyboardSize?.height)! + 0.0)
                        self.notificationTextBottomConstraint.constant = ((keyboardSize?.height)! - 15.0)
                        self.view.layoutIfNeeded()
        })
    }
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        self.countryBtn.removeFromSuperview()
        UIView.animate(withDuration: 0.4) {
            self.mainViewBottomConstraint.constant = 0
            self.nextButtonBottomConstraint.constant = 30
            self.notificationTextBottomConstraint.constant = 40
            self.logoImage.isHidden = false
            self.backButton.isHidden = true
            self.view.layoutIfNeeded()
        }
        
    }
    
    @objc func countryButtonClicked() {
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    // MARK: - Segue Method -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toVerifyOTP" {
            
            if let dstVC = segue.destination as? vaiDRx_VerifyOTPVC{
            
                dstVC.dataFromLandingVC = sender as! [String : Any]
                dstVC.registerViewModel = self.registerViewModel
                dstVC.isComingFrom = "signUp"
            }
        } else if segue.identifier == "toSignIn" {
            
            if let dstVC = segue.destination as? vaiDRx_EnterPasswordVC{
                
                dstVC.countryCode = countryCodeLabel.text!
                dstVC.phoneNumber = phoneNumberTextFeild.text!
                dstVC.loginViewModel = loginViewModel
            }
        } 
    }
}

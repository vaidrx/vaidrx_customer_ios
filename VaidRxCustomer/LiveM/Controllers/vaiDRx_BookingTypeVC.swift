//
//  vaiDRx_BookingTypeVC.swift
//  VaidRxCustomer
//
//  Created by Apple on 14/03/22.
//

import UIKit



protocol BookingApiDelegate{
    func RefreshBookingAPI(type:String)
}
class vaiDRx_BookingTypeVC: UIViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var connectingLbl: UILabel!
    
    var countDown:Int = 60
    var isComingFrom = ""
    var delegateForApi: BookingApiDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        
        if isComingFrom == "VaidNow"{
            titleLabel.text = "Vaid Now"
            titleImage.image = UIImage(named: "vaid_now")
        }
        if isComingFrom == "VaidToday"{
            titleLabel.text = "Vaid Today"
            titleImage.image = UIImage(named: "vaid_today")
        }
        // Do any additional setup after loading the view.
    }

    
    @IBAction func RealVisitAction(_ sender: UIButton) {
        
        
        delegateForApi?.RefreshBookingAPI(type: "0")
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func VirtualVisitAction(_ sender: UIButton) {
        delegateForApi?.RefreshBookingAPI(type: "1")
        self.dismiss(animated: false, completion: nil)
    }
    
    

}

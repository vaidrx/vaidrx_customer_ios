//
//  LoginModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

/// Login Details model
struct LoginModel {
    
    var emailText = ""
    var passwordText = ""
    var loginType = LoginType.Default
    var facebookId = ""
    var googleId = ""
}

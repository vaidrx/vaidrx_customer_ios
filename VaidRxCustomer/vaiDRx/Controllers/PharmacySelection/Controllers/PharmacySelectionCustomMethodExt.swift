//
//  PharmacySelectionCustomMethodExt.swift
//  vaiDRx
//
//  Created by 3Embed on 10/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
import Foundation
import GoogleMaps
import Kingfisher

extension PharmacySelectionViewController {
    
    //MARK: - GMSMarker Methods -
    
    /// Method to show musicians markers on mapview
    func showMarkersInMapView() {
        
//        if arrayOfCategoriesModel[selectedCategoryTag].arrayOfProviders.count > 0 {
//
//            for i in 0..<arrayOfCategoriesModel[selectedCategoryTag].arrayOfProviders.count {
//
//                let providerDetail:ProviderDetailsModel = arrayOfCategoriesModel[selectedCategoryTag].arrayOfProviders[i]
        for item in pharmacyItems
        {
                if item.latitude != nil {
                    
                    var view: UIView!
                    
                    view  = createCustomMarker(isSelectedMarker: false, item:item)
                    addMarkerInMapView(markerView: view, item:item, isSelectedMarker: false)
                    
//                } else {
//
//                    updatedProviderMarkerDetails(providerDetail: providerDetail, isSelected:false)
//
//                }
                
//            }
        }
    else {
            
            // Clear the Map
            DispatchQueue.main.async(execute: {() -> Void in
                
                self.mapView.clear()
            })
        }
        
        }
        
    }
    
    
    
    /// Method to create new musician marker and add to mapview
    ///
    /// - Parameters:
    ///   - markerView: musician marker view
    ///   - providerDetail: providerDetail model
    ///   - isSelectedMarker: currently selected musician or not
    func addMarkerInMapView(markerView:UIView, item:PharmacyItems,isSelectedMarker:Bool) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            let providerMarker:GMSMarker = GMSMarker()
            providerMarker.position = CLLocationCoordinate2DMake(item.latitude,item.longitude)
            providerMarker.isTappable = true
            providerMarker.isFlat = true
            providerMarker.appearAnimation = GMSMarkerAnimation.pop
            providerMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            providerMarker.iconView = markerView
            providerMarker.map = self.mapView
            //providerMarker.userData = providerDetail
            
//            if isSelectedMarker {
//
//                providerMarker.zIndex = 0
//
//            } else {
//
//                providerMarker.zIndex = 1
//            }
//
//            self.dictOfMarkers[providerDetail.providerId] = providerMarker
        })
        
    }
    
    
//    func removeMarker(marker:GMSMarker) {
//
//        DispatchQueue.main.async(execute: {() -> Void in
//
//            marker.map = self.mapView
//            marker.map = nil
//        })
        
    
    
    
    /// Create Musician Marker view
    ///
    /// - Parameters:
    ///   - isSelectedMarker: currently selected musician or not
    ///   - providerDetail: providerDetail model
    /// - Returns: created musicina marker view
    func createCustomMarker(isSelectedMarker:Bool, item: PharmacyItems) -> UIView {
        
        var markerView:UIView!
        var markerImageViewInner:UIImageView!
        var markerOnlineImageView:UIImageView!
        
        
        markerView = UIView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        markerOnlineImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        markerImageViewInner = UIImageView(frame: CGRect(x: 2, y: 2, width: 48, height: 48))
        
        markerOnlineImageView.layer.cornerRadius = 52 / 2
        markerView.layer.cornerRadius = 52 / 2
        markerImageViewInner.layer.cornerRadius = 48 / 2
        
        markerView.backgroundColor = UIColor.clear
        
        markerView.clipsToBounds = true
        markerOnlineImageView.clipsToBounds = true
        markerImageViewInner.clipsToBounds = true
        
        
//        if isSelectedMarker {
//
//            markerView.transform = CGAffineTransform(scaleX: 1.25, y:1.25)
//
//        } else {
//
//            markerView.transform = .identity
//
//        }
        
//        markerImageViewInner.tag = ProviderMarkerSubViews.providerImageView
//        markerOnlineImageView.tag = ProviderMarkerSubViews.providerOnlineImageView
        
        
//        if providerDetail.status == 0 {
//
//            markerOnlineImageView.image = #imageLiteral(resourceName: "offline_image")
//
//        } else {
        
            markerOnlineImageView.image = #imageLiteral(resourceName: "online_image")
//        }
        
        
//        if providerDetail.profilePic.length > 0 {
//
//            markerImageViewInner.kf.setImage(with: URL(string: providerDetail.profilePic),
//                                             placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
//                                             options: [.transition(ImageTransition.fade(1))],
//                                             progressBlock: { receivedSize, totalSize in
//            },
//                                             completionHandler: { image, error, cacheType, imageURL in
//
//            })
//
//        } else {
        
//            markerImageViewInner.image = #imageLiteral(resourceName: "myevent_profile_default_image")
//        }
        
        
        markerView.addSubview(markerOnlineImageView)
        markerView.addSubview(markerImageViewInner)
        
        return markerView
        
    }
    
    
    /// Update each musician details on Mapview
    ///
    /// - Parameters:
    ///   - providerDetail: providerDetail model
    ///   - isSelected: currently selected musician or not
//    func updatedProviderMarkerDetails(providerDetail:ProviderDetailsModel , isSelected:Bool) {
//
//        let providerMarker:GMSMarker = dictOfMarkers[providerDetail.providerId] as! GMSMarker
//        var isProfileImageChanged:Bool = false
//
//        if let userPrevData = providerMarker.userData as? ProviderDetailsModel {
//
//            if userPrevData.profilePic.length > 0 && userPrevData.profilePic != providerDetail.profilePic {
//
//                isProfileImageChanged = true
//            }
//        }
//
//        providerMarker.userData = providerDetail
//
//        CATransaction.begin()
//        CATransaction.setAnimationDuration(2.0)
//        providerMarker.position = CLLocationCoordinate2DMake(providerDetail.latitude,providerDetail.longitude)
//        CATransaction.commit()
//
//        if isSelected {
//
//            providerMarker.iconView?.transform = CGAffineTransform(scaleX: 1.25, y:1.25)
//            providerMarker.zIndex = 1
//
//        } else {
//
//            providerMarker.iconView?.transform = .identity
//            providerMarker.zIndex = 0
//        }
//
//
//        if let markerImageViewInner:UIImageView = providerMarker.iconView?.viewWithTag(ProviderMarkerSubViews.providerImageView) as? UIImageView {
//
//            if isProfileImageChanged {
//
//                markerImageViewInner.kf.setImage(with: URL(string: providerDetail.profilePic),
//                                                 placeholder:nil,
//                                                 options: [.transition(ImageTransition.fade(1))],
//                                                 progressBlock: { receivedSize, totalSize in
//                },
//                                                 completionHandler: { image, error, cacheType, imageURL in
//
//                })
//
//            }
//
//        }
//
//        if let markerOnlineImageView:UIImageView = providerMarker.iconView?.viewWithTag(ProviderMarkerSubViews.providerOnlineImageView) as? UIImageView {
//
//            if providerDetail.status == 0 {
//
//                markerOnlineImageView.image = #imageLiteral(resourceName: "offline_image")
//
//            } else {
//
//                markerOnlineImageView.image = #imageLiteral(resourceName: "online_image")
//            }
//
//        }
//
//    }
    
//}
}

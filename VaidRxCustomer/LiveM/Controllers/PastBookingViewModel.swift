//
//  PastBookingViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class PastBookingViewModel {
    
    let disposebag = DisposeBag()
    var methodName = ""
    
    func getBookingDetailsAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxBookingAPI = BookingAPI()
        
        rxBookingAPI.getBookingDetailsServiceAPICall(methodName: methodName)
        
        if !rxBookingAPI.getBookingDetails_Response.hasObservers {
            
            rxBookingAPI.getBookingDetails_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
}


//
//  vaiDRx_EnterPasswordVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class vaiDRx_EnterPasswordVC: UIViewController, KeyboardDelegate {
    
    // MARK: - Outlets -
    
    //TextFeilds
    @IBOutlet weak var passwordTextFeild: UITextField!
    
    //Layout Constraint
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Variable Decleration -
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var forgotPasswordViewModel = ForgotPasswordViewModel()
    var loginViewModel: LoginViewModel!
    var countryCode: String!
    var phoneNumber:String!
    var forgotPasswordWithEmail:Bool = false
    let disposeBag = DisposeBag()
    var socialMediaEmail = ""
    var showPass = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.hideNavBarShadow(vc: self)
        addObserveToVariables()
//        passwordTextFeild.becomeFirstResponder()
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        passwordTextFeild.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDelegate?.keyboardDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        appDelegate?.keyboardDelegate = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tappedShowPass(_ sender: UIButton) {
        
        if showPass {
             passwordTextFeild.isSecureTextEntry = true
            sender.isSelected = false
            showPass = false
        } else {
            passwordTextFeild.isSecureTextEntry = false
            sender.isSelected = true
            showPass = true
        }
        
    }
    
}

//
//  LiveTrackVCGoogleMapMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps
import Kingfisher

extension LiveTrackViewController {
    
    func showInitialMapViewProperties() {
        
        self.mapView.settings.myLocationButton = true
        
        showAppointmentLocationMarker()
        showProviderMarker()
        
        markersToFitInMapView()
        
        for object: UIView in (mapView?.subviews)! {
            
            if String(describing: object.classForCoder) == "GMSUISettingsPaddingView" {
                
                for ob: UIView in object.subviews {
                    
                    if String(describing: ob.classForCoder) == "GMSUISettingsView" {
                        
                        for button in ob.subviews {
                            
                            if String(describing: button.classForCoder) == "GMSx_QTMButton" {
                                
                                (button as? UIButton)?.addTarget(self, action: #selector(self.markersToFitInMapView), for: .touchUpInside)
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    @objc func markersToFitInMapView() {
        
        if providerMarker != nil {
            
            var bounds = GMSCoordinateBounds()
            bounds = bounds.includingCoordinate(appointmentLocMarker.position)
            bounds = bounds.includingCoordinate(providerMarker.position)
            
//            mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50))
            
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets.init(top: 55, left: 55, bottom: 55, right:55)))
            
        }
    }
    
    func showAppointmentLocationMarker() {
        
        if appointmentLocMarker == nil {
            
            appointmentLocMarker = GMSMarker()
            appointmentLocMarker.isFlat = true
            appointmentLocMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            appointmentLocMarker.icon = #imageLiteral(resourceName: "appointment_mappin")
            
            appointmentLocMarker.title = "Appointment Location"
            appointmentLocMarker.snippet = bookingDetailModel.address1
            
            appointmentLocMarker.position = CLLocationCoordinate2DMake(CLLocationDegrees(bookingDetailModel.appointmentLat), CLLocationDegrees(bookingDetailModel.appointmentLong))
            
            appointmentLocMarker.map = mapView
            
//            self.mapView.selectedMarker = appointmentLocMarker
            
        }
        
    }
    
    func showProviderMarker() {
        
        if providerMarker == nil {
            
            providerMarker = GMSMarker()
            providerMarker.isFlat = true
            providerMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            
//            providerMarker.iconView = createCustomMarker(bookingDetailModel.providerImageURL)
            providerMarker.icon = #imageLiteral(resourceName: "map_pin")
            providerMarker.position = CLLocationCoordinate2DMake(CLLocationDegrees(bookingDetailModel.providerLat), CLLocationDegrees(bookingDetailModel.providerLong))
            
            distanceAndETAManager.getDistanceAndETA(appointmentLat: bookingDetailModel.appointmentLat,
                                                    appointmentLong: bookingDetailModel.appointmentLong,
                                                    musicianLat: bookingDetailModel.providerLat,
                                                    musicianLong: bookingDetailModel.providerLong)
            
            providerMarker.snippet = "Distance: \(Helper.getDistanceDependingMileageMetricFromServer(distance: bookingDetailModel.distance, mileageMatric: bookingDetailModel.mileageMatric))\nETA: 1 MIN"
            
            distanceValueLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: bookingDetailModel.distance, mileageMatric: bookingDetailModel.mileageMatric)
            
            etaValueLabel.text = "1 MIN"
            
            providerMarker.map = mapView
            
            self.mapView.selectedMarker = providerMarker
            
        }
        
    }
    
    /// Method to show musician location in MapView
    func focusOnProviderMarkerOnMapView(mapZoomLevel:Float) {
        
        if providerMarker != nil {
            
            let camera = GMSCameraUpdate.setTarget(self.providerMarker.position, zoom: mapZoomLevel)
        
            self.mapView.animate(with: camera)
            
        }
        
    }

    
    func createCustomMarker(_ providerImageUrl: String) -> UIView {
        
        var markerView:UIView!
        var markerImageViewInner:UIImageView!
        
        
        markerView = UIView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        markerImageViewInner = UIImageView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        
        
        
        markerView.layer.cornerRadius = 52 / 2
        markerImageViewInner.layer.cornerRadius = 52 / 2
        
        
        markerImageViewInner.tag = ProviderMarkerSubViews.providerImageView
        
        markerView.clipsToBounds = true
        markerImageViewInner.clipsToBounds = true
        
        markerView.backgroundColor = UIColor.clear
        
        
        if bookingDetailModel.providerImageURL.length > 0 {
            
            markerImageViewInner.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
                                             placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                             options: [.transition(ImageTransition.fade(1))],
                                             progressBlock: { receivedSize, totalSize in
            },
                                             completionHandler: nil)
            
        } else {
            
            markerImageViewInner.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        markerView.addSubview(markerImageViewInner)
        
        return markerView
        
    }
    
    func updatedProviderMarkerDetails(isProfileImageChanged:Bool) {
        
        distanceAndETAManager.getDistanceAndETA(appointmentLat: bookingDetailModel.appointmentLat,
                                                appointmentLong: bookingDetailModel.appointmentLong,
                                                musicianLat: bookingDetailModel.providerLat,
                                                musicianLong: bookingDetailModel.providerLong)
        self.mapView.selectedMarker = providerMarker
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        providerMarker.position = CLLocationCoordinate2DMake(bookingDetailModel.providerLat, bookingDetailModel.providerLong)
        CATransaction.commit()
        
        self.focusOnProviderMarkerOnMapView(mapZoomLevel: self.mapView.camera.zoom)
        
        
//        if isProfileImageChanged {
//            
//            if let markerImageViewInner:UIImageView = providerMarker.iconView?.viewWithTag(ProviderMarkerSubViews.providerImageView) as? UIImageView {
//                
//                if bookingDetailModel.providerImageURL.length > 0 {
//                    
//                    markerImageViewInner.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
//                                                     placeholder:nil,
//                                                     options: [.transition(ImageTransition.fade(1))],
//                                                     progressBlock: { receivedSize, totalSize in
//                    },
//                                                     completionHandler: { image, error, cacheType, imageURL in
//                                                        
//                    })
//                    
//                }
//            }
//            
//        }
        
    }
}

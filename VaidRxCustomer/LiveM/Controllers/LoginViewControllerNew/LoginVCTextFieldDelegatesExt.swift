//
//  LoginVCTextFieldDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


// MARK: - Text Field Delegate
extension LoginViewController : UITextFieldDelegate {
    
    /**
     *   This method will called when the Textfield begin to editing
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == emailTextField {
            
            emailTFBottomLineView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)//UIColor.init(hex: "#3B5998")
        }
        else {
            
            passwordTFBottomLineView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
        }
        
        return true
    }
    
    /**
     *   This method will called when the user clicked on clear button in textfield
     */
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    /**
     *   This method will called when the user textfield text is changed
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var textFieldText = ""
        
        if range.length == 0 {//Entered New Character
            
            textFieldText = textField.text! + string
        }
        else {//Entered BackSpace
            
            textFieldText = String(textField.text!.dropLast())
        }
        
        DDLogDebug(textFieldText)
        
        animateButtonLoading(checkFieldsAreValid(textFieldText: string, textField: textField))
        
        
        return true
    }
    
    
    /**
     *   This method will called when the textfield editing is ends
     */
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == emailTextField{
            emailTFBottomLineView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1) //UIColor.init(hex: "#BABABA")
            PhoneMissingConst.constant = 0
        }else {
            passMissingCons.constant = 0
            passwordTFBottomLineView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
        }
    }
    
    /**
     *   This method will called when the user clicked on Next button in Keyboard
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        if textField == emailTextField{
            passwordTFBottomLineView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            
            if !Helper.isValidEmail(testStr: emailTextField.text!) {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.EmailOrPhoneNumberInvalid)
                emailTextField.becomeFirstResponder()
            }
            else {
                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute: {
                    self.passwordTextfield.becomeFirstResponder()
//                })
                
            }
            
            return false
            
        }else {
            
            passwordTFBottomLineView.backgroundColor = #colorLiteral(red: 0.9852504134, green: 0.2963537574, blue: 0.3118605018, alpha: 1)
            self.view.endEditing(true)
            validateAllFields()
        }
        
        return true
    }
    
    func checkFieldsAreValid(textFieldText:String, textField: UITextField) -> Int {
        
        var validFieldCount:Int = 0
        
        if Helper.isValidEmail(testStr: emailTextField.text!) {
            
            validFieldCount = validFieldCount + 1
        }
        
        if  textField == passwordTextfield {
            if (passwordTextfield.text?.length)! > 0 && !textFieldText.isEmpty {
                
                validFieldCount = validFieldCount + 1
            }
        }else if (passwordTextfield.text?.length)! > 0{
            validFieldCount = validFieldCount + 1
        }
        
        return validFieldCount
        
    }
    
    
}

//
//  VaiDRx_AddNewCardVCViewModel.swift
//  vaiDRx
//
//  Created by 3Embed on 24/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class VaidRx_AddNewCardViewModel{
    let rxGetStripeKeyAPI = GetStripekeyAPI()
    let disposeBag = DisposeBag()
    
    func getStripeKeyAPICall(completion:@escaping (Int,String?,Any?) -> ()){
        rxGetStripeKeyAPI.getStripeKeyServiceAPICall()
        if !rxGetStripeKeyAPI.getStripe_Response.hasObservers{
            rxGetStripeKeyAPI.getStripe_Response.subscribe(onNext: {(response) in
                if response.data[SERVICE_RESPONSE.Error] != nil {
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    //HelperVaidRx.showAlertMessage(title: ALERT_VAIDRX.Error, message: response.data[SERVICE_RESPONSE_VAIDRX.Error] as! String)
                    return
                }
                completion(response.httpStatusCode,response.data[SERVICE_RESPONSE.ErrorMessage] as? String,response.data[SERVICE_RESPONSE.DataResponse])
            }, onError: {(error) in
                
            }).disposed(by: disposeBag)
        }
    }
}

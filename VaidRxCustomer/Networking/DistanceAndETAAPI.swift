//
//  DistanceAndETAAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class DistanceAndETAAPI {
    
    let disposebag = DisposeBag()
    let getDistanceAndETA_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call Get support details service API
    func getDistanceAndETAServiceAPICall(appointmentLat:Double,
                                         appointmentLong:Double,
                                         musicianLat:Double,
                                         musicianLong:Double) {
        
        let strURL = String(format:API.METHOD.GET_DISTANCE_ETA_GOOGLE,appointmentLat,appointmentLong,musicianLat,musicianLong,Google.ServerKey)
        
        DDLogVerbose("Google ETA And Distance Request:\(strURL)")
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                DDLogVerbose("Google ETA And Distance API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetDistanceAndETA)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
//                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
                
            case HTTPSResponseCodes.BadRequest.rawValue:
                
                Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
                break
                
            case HTTPSResponseCodes.InternalServerError.rawValue:
                
                Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                    
                }
                break
                
            default:
                
                let responseModel:APIResponseModel!
                responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
                
                switch requestType {
                    
                    case .GetDistanceAndETA:
                        
                        self.getDistanceAndETA_Response.onNext(responseModel)
                    
                    default:
                        break
                }
                
                break
        }
    }
    
}


//
//  vaiDRx_SocialLoginDelegateExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_SocialLoginVC:facebookLoginDelegate {
    
    /// This is the dalegate method called from FBLoginHandler When got facebook user details successfully
    ///
    /// - Parameter userInfo: facebook user details Dictionary
    
    func didFacebookUserLogin(withDetails userInfo: [String : Any]) {
        
        DDLogVerbose("FBData = \(userInfo)")
        
        fbUserDataModel = UserDataModel.init(facebookDetails: userInfo)
        
        if fbUserDataModel.mailId.length == 0 && fbUserDataModel.phoneNumber.length == 0 {
            
//            emailTextField.text = ""
            loginViewModel.emailText.value = ""
            
//            passwordTextfield.text = ""
            loginViewModel.passwordText.value = ""
            
//            emailTextField.becomeFirstResponder()
        }
        else {
            
            isFBLogin = true
            isGoogleLogin = false
            
            if fbUserDataModel.mailId.length > 0 {
                
//                emailTextField.text = fbUserDataModel.mailId
                loginViewModel.emailText.value = fbUserDataModel.mailId
                
            } else if fbUserDataModel.phoneNumber.length > 0 {
                
//                emailTextField.text =
                loginViewModel.emailText.value = fbUserDataModel.phoneNumber
                
            } else {
                
//                emailTextField.text = ""
                loginViewModel.emailText.value = ""
//                emailTextField.becomeFirstResponder()
            }
            
            if fbUserDataModel.userId.length > 0 {
                
//                passwordTextfield.text =
                loginViewModel.passwordText.value = fbUserDataModel.userId
                
            } else {
                
//                passwordTextfield.text = ""
                loginViewModel.passwordText.value = ""
//                passwordTextfield.becomeFirstResponder()
            }
            
//            validateAllFields()
            sendServiceToMakeLogin(loginType: LoginType.Facebook)
            
        }
        
    }
    
    func didFailWithError(_ error: Error?) {
        
        
        Helper.alertVC(title: ALERTS.Message, message: (error?.localizedDescription)!)
    }
    
    func didUserCancelLogin() {
        
        DDLogDebug("User Cancelled Facebook Login")
    }

}

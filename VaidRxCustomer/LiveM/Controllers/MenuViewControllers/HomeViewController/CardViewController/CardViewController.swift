//
//  CardViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 27/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MFCard
import Stripe

protocol CardViewControllerDelegate {
    func didSelectCardType (name : String, id: String,brand: String)
}


class CardViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var cardsAdded = [AnyObject]()
    var delegate : CardViewControllerDelegate? = nil
    var cardDet = [CardsType]()
    let manager = SelectCardManager.shared
    var flagMenu = true
    let defaults = UserDefaults.standard
    var timer = Timer()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        if manager.cards.isEmpty {
            Helper.showPI()
            sendRequestToGetCards()
        }else {
            cardDet = self.manager.cards
            sendRequestToGetCards()
        }
        
        if !flagMenu {
            self.backButton.setImage(#imageLiteral(resourceName: "pickup_back_icon_off"), for: .normal)
        }else{
            self.backButton.setImage(#imageLiteral(resourceName: "home_menu_icon_off"), for: .normal)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        if flagMenu {
            sideMenuVC.toggleMenu()
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func addCardButtonClicked(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "addCardSegue", sender: self)
        //        let AddCardpopUp = AddCardPopup.shared
        //        AddCardpopUp.showPopup(window: UIApplication.shared.keyWindow!)
    }
    
    // MARK: - SendService for GetCards
    
    func sendRequestToGetCards() {
        
        let url = API.METHOD.CARDS + "/me"
        
        print("sendRequestToGetCards.............params",url)
        NetworkHelper.requestGETURL(method: url, success: { (response) in
            
            print(response)
            print("sendRequestToGetCards.............response",response)
            
            Helper.hidePI()
            let errFlag: ErrorFlagType = ErrorFlagType(rawValue: Int(response["errFlag"].numberValue))!
            switch errFlag
            {
            case .Success:
                
                
                let statuscode: StatCodeCheck = StatCodeCheck(rawValue: Int(response["statusCode"].numberValue))!
                
                switch statuscode
                {
                case .Expire:
                    
                    self.defaults.removeObject(forKey: USER_DEFAULTS.TOKEN.SESSION)
                    self.defaults.removeObject(forKey: Utility.myChannel)
                    
                    let pubnubObj: VNHPubNubWrapper = VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper
                    
                    pubnubObj.unsubscribeFromAllChannel()
                    pubnubObj.unsubscribeFromMyChannel()
                    pubnubObj.deleteInstance()
                    
                    self.timer.invalidate()
                    DispatchQueue.main.async(execute: {
                        let mainVcIntial = kConstantObj.SetIntialMainViewController("Splash")
                        let window = UIApplication.shared.keyWindow
                        window?.rootViewController = mainVcIntial
                    })
                    break
                    
                case .ServerError:
                    
                    break
                    
                default:
                    
                    self.cardsAdded = (response["data"].arrayObject as [AnyObject]?)!
                    self.cardDet = []
                    for cardDetail in self.cardsAdded {
                        if let carObj = cardDetail as? [String:Any]{
                            let brand = carObj["brand"] as? String
                            let exp_month = carObj["exp_month"] as? NSNumber
                            let exp_year = carObj["exp_year"] as? NSNumber
                            let id = carObj["id"] as? String
                            let last4 = carObj["last4"] as? String
                            
                            self.cardDet.append(CardsType(exp_year: String(describing: exp_year!) ,
                                                          last4: last4!,
                                                          id: id!,
                                                          brand: brand!,
                                                          exp_month: String(describing:exp_month!)))
                            self.tableView.reloadData()
                        }
                    }
                    self.manager.cards = self.cardDet
                    break
                }
                
                break
                
            case .Failure:
                break
            }
        },
                                    failure:
            {
                (Error) in
        })
    }
    
    func sendRequestToDeleteCard(index: Int, controller:UIViewController) {
        
        Helper.showPI()
        
        let params : [String : Any] = ["cardId"     :String(describing:self.cardDet[index].id)]
        
        print("sendRequestToDeleteCard request parameters :",params)
        
        NetworkHelper.requestDELETEURL(method: API.METHOD.CARD, success: { (response) in
            print("sendRequestToDeleteCard response",response)
            
            Helper.hidePI()
            let errFlag: ErrorFlagType = ErrorFlagType(rawValue: Int(response["errFlag"].numberValue))!
            switch errFlag
            {
            case .Success:
                
                let statuscode: StatCodeCheck = StatCodeCheck(rawValue: Int(response["statusCode"].numberValue))!
                
                switch statuscode
                {
                case .Expire:
                    
                    self.defaults.removeObject(forKey: USER_DEFAULTS.TOKEN.SESSION)
                    self.defaults.removeObject(forKey: Utility.myChannel)
                    
                    let pubnubObj: VNHPubNubWrapper = VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper
                    
                    pubnubObj.unsubscribeFromAllChannel()
                    pubnubObj.unsubscribeFromMyChannel()
                    pubnubObj.deleteInstance()
                    
                    self.timer.invalidate()
                    DispatchQueue.main.async(execute: {
                        let mainVcIntial = kConstantObj.SetIntialMainViewController("Splash")
                        let window = UIApplication.shared.keyWindow
                        window?.rootViewController = mainVcIntial
                    })
                    break
                    
                case .ServerError:
                    
                    break
                    
                default:
                    
                    controller.present(Helper.alertVC(title: ALERTS.Message as NSString, message: ALERTS.deleteCard as NSString), animated: true, completion: nil)
                    
                    self.cardDet.remove(at: index)
                    //tableView.deleteRows(at: [index], with: UITableViewRowAnimation.automatic)
                    self.tableView.reloadData()
                    break
                }
                
                break
                
            case .Failure:
                break
            }
        },
                                       failure:
            {
                (Error) in
        })
    }
    
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let stripe =  segue.destination as? StripeViewController
        {
            stripe.delegate = self
        }else if segue.identifier == "AllCardToCardDetails" {
            let btn = sender as! UIButton
            let controller = segue.destination as? CardDetailsViewController
            controller?.cardData = cardDet[btn.tag]
            controller?.CardDetailsDelegate = self
        }
    }
}
extension CardViewController : CardDetailsDelegate {
    func didDelete() {
        sendRequestToGetCards()
        self.present(Helper.alertVC(title: ALERTS.Message as NSString, message: ALERTS.deleteCard as NSString), animated: true, completion: nil)
    }
}

extension CardViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cardDet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"addCardTableViewCell") as! AddCardTableViewCell!
        cell?.cardNumber.text = "XXXX XXXX XXXX " + self.cardDet[indexPath.row].last4
        if manager.selected_card?.id == self.cardDet[indexPath.row].id {
            cell?.tickMark.isHidden = false
            cell?.cardNumber?.textColor = #colorLiteral(red: 0.137254902, green: 0.7725490196, blue: 0.5647058824, alpha: 1)
        }else {
            cell?.tickMark.isHidden = true
            cell?.cardNumber?.textColor = #colorLiteral(red: 0.4078176022, green: 0.407827884, blue: 0.4078223705, alpha: 1)
        }
        cell?.cardImage.image = Helper.cardImage(with: self.cardDet[indexPath.row].brand)
        return cell!
    }
}

extension CardViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if flagMenu {
            let btn = UIButton()
            btn.tag = indexPath.row
            self.performSegue(withIdentifier: "AllCardToCardDetails", sender: btn)
        }else{
            _ = self.navigationController?.popViewController(animated: true)
            delegate?.didSelectCardType(name: self.cardDet[indexPath.row].last4, id: self.cardDet[indexPath.row].id, brand: self.cardDet[indexPath.row].brand)
            manager.selected_card = CardsType(exp_year: self.cardDet[indexPath.row].exp_year,
                                              last4: self.cardDet[indexPath.row].last4,
                                              id: self.cardDet[indexPath.row].id,
                                              brand: self.cardDet[indexPath.row].brand,
                                              exp_month: self.cardDet[indexPath.row].exp_month)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            print("deleted index path",indexPath.row)
            self.sendRequestToDeleteCard(index: indexPath.row, controller: self)
        }
    }
}


extension CardViewController : StripeViewControllerDelegate {
    
    func didCardAdded() {
        sendRequestToGetCards()
    }
}

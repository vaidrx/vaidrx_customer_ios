//
//  PendingBookingVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension PendingBookingViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "bookingDetailsCell", for: indexPath) as! PBBookingDetailsCell
        
        cell.titleLabel.text = arrayOfData[indexPath.row]
        
        
        let startDate = Date(timeIntervalSince1970: TimeInterval(bookingEventModel.bookingStartTime))
        
        let dateFormat = DateFormatter()
        
        
        switch indexPath.row {
            
        case 0:
            
            dateFormat.dateFormat = "d MMM yyyy"
            cell.valueLabel.text = dateFormat.string(from: startDate)
            
            break
            
        case 1:
            
            cell.valueLabel.text = GenericUtility.strForObj(object:bookingEventModel.eventDict["name"])
            
            break
            
        case 2:
            
            dateFormat.dateFormat = "hh:mm a"
            cell.valueLabel.text = dateFormat.string(from: startDate)
            
            break
            
        case 3:
            
            cell.valueLabel.text = String(format:"%@ %@", GenericUtility.strForObj(object:bookingEventModel.gigTimeDict["name"]), GenericUtility.strForObj(object:bookingEventModel.gigTimeDict["unit"]))
            
            
            break
            
        case 4:
            
            cell.valueLabel.text = String(format:"%@ %@",bookingEventModel.currencySymbol, GenericUtility.strForObj(object:bookingEventModel.gigTimeDict["price"]))
            
            break
            
        case 5:
            
//            cell.valueLabel.text = bookingEventModel.paymentType.capitalized
            
            break
            
        default:
            break
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
}


//
//  HomeScreenViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 31/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import Kingfisher





class HomeScreenViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,BookingApiDelegate, disconnetCallDelegate{
    func BackToPreviousScreen() {
        self.bookingId = BookingStatusResponseManager.sharedInstance().bookingId
        getBookingDetailsAPI()
    }
    
   
    
    
    
    var bookingDetailModel:BookingDetailsModel!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var rightNavigationButton: UIButton!
    @IBOutlet weak var bookingButtonsView: UIView!
    @IBOutlet weak var leftNavButton: UIButton!
    
    @IBOutlet weak var mapPinBackView: UIView!
    @IBOutlet weak var bookingDetailsView: UIView!
    
    var CBModel:ConfirmBookingModel = ConfirmBookingModel.sharedInstance()
    
    let confirmBookingViewModel = ConfirmBookingViewModel()
    
    var bookingId:Int64!
    var bookingStatus:Int!
    var vaidType = 0 //1- Now//2- Today
    
    let bookingFlowViewModel = BookingFlowViewModel()
    
    var flag = 0
   
    @IBOutlet weak var doctorDetailHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var doctorDetailBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileImageButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var profileImageView: UIImageView!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var topAddressBackView: UIViewCustom!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet var showListScreenButtonBackView: UIViewCustom!
    
    @IBOutlet var collectionView: iCarousel!
    
    @IBOutlet weak var locationButtonOutlet: UIButton!
    @IBOutlet weak var listButtonOutlet: UIButton!
    
    @IBOutlet weak var mainButtonHeight: NSLayoutConstraint!
    
    @IBOutlet var collectionBackViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var topAddressBackViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var providerListCollectionViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var etaBackViewAlignCenterYContraint: NSLayoutConstraint!
    
    //Schedule DatePicker
    
    @IBOutlet var datePickerBackgroundView: UIView!
    
    @IBOutlet var eventStartBackView: UIView!
    
    @IBOutlet var datepickerCancelButton: UIButtonCustom!
    
    @IBOutlet var datepickerSaveButton: UIButtonCustom!
    
    @IBOutlet var datePickerBackBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var eventStartCollectionView: UICollectionView!
    
    @IBOutlet var datePickerView: UIDatePicker!
    
    
    
    //Later booking View
    
    @IBOutlet var selectedBookLaterBackView: UIView!
    
    @IBOutlet var selectedBookLaterDateLabel: UILabel!
    
    @IBOutlet var selectedBookLaterButton: UIButton!
    
    @IBOutlet var selectedBookLaterCloseButton: UIButton!
    
    
    @IBOutlet weak var noMusiciansMessageBackView: UIView!
    
    @IBOutlet weak var noMusiciansMessageLabel: UILabel!
    
    @IBOutlet weak var showListBackViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var currentLocationButtonBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var mapViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var discriptionLabel: UILabel!
    
    
    let catransitionAnimationClass = CatransitionAnimationClass()
    let locationObj = LocationManager.sharedInstance()
    
    var delegate: LeftMenuProtocol?
    
   
    
    var isAddressManuallyPicked = false
    
    var isFocusedMarkerPicked = false
    
    var dictOfMarkers:[String:Any] = [:]
    
    var arrayOfProvidersModel:[MusicianDetailsModel] = []
    var arrayOfMusicians:[Any] = []
    var selectedProviderTag = 0
    var prevSelectedMarkerTag = 0
    var selectedProviderId = ""
    var isComimgFrom = ""
    var appointmentAvail1 = ""
    var disconnectCall = DisconnectCallViewModel()
    
    let mqttModel = MQTT.sharedInstance()
    let musiciansListManager = MusiciansListManager.sharedInstance()
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    var selectedView:ProviderCollectionView!
    
    let appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
    
    var cancelBookingScreen:CancelBookingScreen!
    
    var currentLat = 0.0
    var currentLong = 0.0
    
    var selectedEventStartTag = 0
    var emergencyMessageDelegate: EmergencyMessageDelegate? = nil
    
    
    var scheduleDate:Date!
    var selectedDate:Date!
    //    var noArtistMessageToShow = true
   // var noAeristAlertController:UIAlertController!
    var needToShowProgress = false
    
    var isVCisDispappeared = false
    
    // Shared instance object for gettting the singleton object
    static var obj:HomeScreenViewController? = nil
    
    class func sharedInstance() -> HomeScreenViewController {
        
        return obj!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setInitialMapViewProperties()
        uiSetUp(value: appointmentAvail1)
        
        
        tableView.reloadData()
        if SCREEN_HEIGHT == 812 { //iPhoneX
            
            self.etaBackViewAlignCenterYContraint.constant = 24
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveBookingAcception),
                                               name: NSNotification.Name(rawValue: "gotNewBooking"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeBooking), name: NSNotification.Name(rawValue: "canceledBooking"), object: nil)
        
        cancelButton.layer.cornerRadius = cancelButton.frame.height / 2
        
        //doctorDetailHeightConstraint.constant = CGFloat(self.view.frame.size.height - 64)
       // doctorDetailHeightConstraint.constant = CGFloat(0.0)
        doctorDetailBottomConstraint.constant = CGFloat(0.0)
        Helper.showStatusBarBackView()
        
        HomeScreenViewController.obj = self
        
        self.collectionView.type = iCarouselType.linear
        
        initializeMQTTMethods(initial: true)
        
        showPrevProviderDetails()
        
        self.selectedBookLaterBackView.isHidden = true
        
        if scheduleDate == nil {
            
            scheduleDate = TimeFormats.roundDate(Date(timeIntervalSinceNow: 3600 * 2))
            datePickerView.minimumDate = scheduleDate
        }
        
        selectedDate = scheduleDate
        datePickerView.date = selectedDate
        
        if Helper.SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(version: "11.0") {
            
            self.mapViewTopConstraint.constant = -NavigationBarHeight
            mapView.layoutIfNeeded()
        }
        
            PendingReviewsManager.sharedInstance.getPendingReviews()
    }
    
    
    
    func uiSetUp(value: String){
        if value == "1"{
            tableView.isHidden = true
            bookingDetailsView.isHidden = false
            mapView.isHidden = true
            mapPinBackView.isHidden = true
            bookingButtonsView.isHidden = true
            topAddressBackView.isHidden = true
            self.bookingDetailsView.setGradientBackgroundToView(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
            self.showNavigation()
        
        } else {
            bookingDetailsView.isHidden = true
            mapView.isHidden = false
            mapPinBackView.isHidden = false
            bookingButtonsView.isHidden = false
            topAddressBackView.isHidden = false
            hideNavigation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showBookLaterDetails()
        checkLocationIsChanged()
        getBookings()
        acessClass.acessDelegate = self
        locationObj.delegate = self
        
        initializeMQTTMethods(initial: false)
        isVCisDispappeared = false
        getUpdatedMusiciansListFromMQTTManager()
        //        topAddressBackViewTopConstraint.constant = 20 + NavigationBarHeight
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        BookingStatusResponseManager.sharedInstance().delegate = self
        //        showProfileImageView()

//        hideNavigation()
        self.bookingDetailsView.setGradientBackgroundToView(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    
    func refresh() {
         getBookings()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.frame = SCREEN_RECT
    }
    
    @objc func removeBooking() {
        bookingButtonsView.isHidden = false
        bookingDetailsView.isHidden = true
        self.navigationController?.popViewController(animated: false)
       
    }
    
    @objc func didReceiveBookingAcception() {
        uiSetUp(value: "1")
        self.bookingId = BookingStatusResponseManager.sharedInstance().bookingId
        getBookingDetailsAPI()
    }
    
    func RefreshBookingAPI(type: String) {
        print(type)
        liveBookinAPI(mode: type)
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    
    func hideNavigation() {
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        self.rightNavigationButton.isHidden = true
        self.title = ""
        leftNavButton.setImage(UIImage(named: "back_green"), for: .normal)
    }
    
    func showNavigation() {
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
//        self.navigationController?.view?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.title = "Vaid Accepted"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
        self.rightNavigationButton.isHidden = false
        leftNavButton.setImage(UIImage(named: "back_green"), for: .normal)
        
    }
    
    //    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    //        super.viewWillTransition(to: size, with: coordinator)
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        uninitializeMQTTMethods()
        pauseAllYoutubeVideo()
        
        locationObj.delegate = self
        
        acessClass.acessDelegate = nil
        
        isVCisDispappeared = true
        //        NotificationCenter.default.removeObserver(self)
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
////        self.navigationController?.view?.backgroundColor = UIColor.clear //UIColor(hex: "05C1C9") //UIColor.white
//        self.navigationController?.navigationBar.backgroundColor = UIColor.clear //UIColor(hex: "05C1C9") //UIColor.white
        
    }
    
    @IBAction func swipeUpAction(_ sender: UISwipeGestureRecognizer) {
        
        let x = CGFloat(133 - (self.view.frame.size.height - 64))
//        showNavigation()
        switch doctorDetailBottomConstraint.constant {
        case x :
            UIView.animate(withDuration: 0.6) {
                self.doctorDetailBottomConstraint.constant = 0
                self.view.layoutIfNeeded()
                self.showNavigation()
            }
            
        default:
            break
        }
    }
    
    @IBAction func swipeDownAction(_ sender: UISwipeGestureRecognizer) {
        
        let x = CGFloat(133 - (self.view.frame.size.height - 64))
//        hideNavigation()
        switch doctorDetailBottomConstraint.constant {
            
        case 0:
            UIView.animate(withDuration: 0.6) {
                self.doctorDetailBottomConstraint.constant = x
                self.view.layoutIfNeeded()
                self.hideNavigation()
            }
            
        default:
            break
        }
    }
    
    func call911()  {
//        let cancelBookingView = EmergencyMessageView.sharedInstance
////        cancelBookingView.changeTitle(message: "In case of a life threatening emergency please contact 911 for immediate action")
//
//        cancelBookingView.delegate = self
//        WINDOW_DELEGATE??.addSubview(cancelBookingView)
//
//        cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
//
//
//        UIView.animate(withDuration: 0.5,
//                       delay: 0.0,
//                       options: UIViewAnimationOptions.beginFromCurrentState,
//                       animations: {
//
//                        cancelBookingView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
//
//        }) { (finished) in
//
//        }
        //vani
        self.showAlert(head: "Message", message: "In case of life threatening emergency please dial 911.")
        
    }
    //vani
    func showAlert(head:String, message: String){
        let alertController = UIAlertController(title: head, message: message, preferredStyle: .alert)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        // Create the actions
        let okAction = UIAlertAction(title: ALERTS.Ok, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.okPressed()
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
        }
        // Add the actions
        alertController.addAction(okAction)
        
        newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func bookNewVaid(_ sender: UIButton) {
//        liveBookinAPI()
        call911()
//        Helper.showAlert(head: "", message: "In case of a life threatening emergency please contact 911 for immediate action")

    }
    
    func liveBookinAPI(mode:String) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        confirmBookingViewModel.CBModel = CBModel
        confirmBookingViewModel.CBModel.bookingModel = 1
        confirmBookingViewModel.CBModel.bookingMode = mode
        confirmBookingViewModel.CBModel.bookingType = self.vaidType//1
        confirmBookingViewModel.CBModel.appointmentLocationModel = self.appointmentLocationModel
        confirmBookingViewModel.CBModel.appointmentLocationModel.bookingType = BookingType.Default
        confirmBookingViewModel.liveBookingAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.liveBooking)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode {
            
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if let dataRes = dataResponse as? String {
                
                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
                self.apiTag = requestType.rawValue
                
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
                case .liveBooking:
                    
                    progressMessage = PROGRESS_MESSAGE.ConfirmBooking
                    
                default:
                    
                    break
                }
                
                self.acessClass.getAcessToken(progressMessage: progressMessage)
                
            }
            
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
            }
            break
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType {
                
            case RequestType.liveBooking:
                
                if let dataRes = dataResponse as? [String:Any] {
                    
                    if let bookingId = dataRes["bookingId"] as? Int64 {
                                                
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let bookingViewController = storyboard.instantiateViewController(withIdentifier: "BookingVC") as! vaiDRx_BookingVC
                        
                        bookingViewController.isComingFrom = isComimgFrom
                        bookingViewController.modalPresentationStyle = .fullScreen
                        if let countDown = dataRes["bookingExpirySecond"] as? Int {
                            
                            bookingViewController.countDown = countDown
                        }
                        
                        
                        self.present(bookingViewController, animated: true, completion: nil)
                        
                        self.bookingId = bookingId
                        
                        
                        //                        MixPanelManager.sharedInstance.LiveBookingEvent(bookingId: String(bookingId))
                        
                    }
                    
                }
                
                AppoimtmentLocationModel.sharedInstance.bookingRequested = true
                
                UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.NewBookingisCreated)
                UserDefaults.standard.synchronize()
                //                self.navigationController?.popToRootViewController(animated: false)
                //                LeftMenuTableViewController.sharedInstance().changeViewController(LeftMenu.myEvent)
                
                break
            case RequestType.disconnectBookingCall:
                getBookingDetailsAPI()
                break
                
            case RequestType.getParticularApptDetails:
                
                if dataResponse != nil {
                    tableView.isHidden = false
                    if let bookings = dataResponse as? [String:Any]{
                        if bookings.count > 0 {
                            bookingDetailModel = BookingDetailsModel.init(bookingDetails: dataResponse!)
                            if bookingDetailModel.bookingStatus == 9{//job started
                                
                                cancelButton.isHidden = true
                                discriptionLabel.isHidden = true
                                
                            }
                            else{
                                appointmentAvail = "1"
                                cancelButton.isHidden = false
                                discriptionLabel.isHidden = false
                            }
                print(bookingDetailModel.cancelTime * 60000)
                print(Helper.getTimePlusOneHr(timeStamp: Int64(bookingDetailModel.bookingStartTime)))
                            var totalCancelationTime:Int = 0
                            if bookingDetailModel.bookingType == 1 {
                                totalCancelationTime = (bookingDetailModel.bookingStartTime) - (bookingDetailModel.cancelTime * 3600/60) + 3600
                                
                            } else {
                                totalCancelationTime = (bookingDetailModel.bookingStartTime) - (bookingDetailModel.cancelTime * 3600/60)
                            }
                            
                            
//                            totalCancelationTime = Helper.getOnlyTimeFromTimeStamp(timeStamp: Int64(totalCancelationTime))
                            
                            let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Chalkduster", size: 18.0)! ]
                            let myString = NSMutableAttributedString(string: "", attributes: myAttribute )
                            let attrString = NSAttributedString(string: "If you want to cancel the appointment, you need to do it by \(Helper.getOnlyTimeFromTimeStamp(timeStamp: Int64(totalCancelationTime))) else cancellation charges will apply")
                           myString.append(attrString)
                            let myRange = NSRange(location: 60, length: 8) // range starting at location 17 with a lenth of 7: "Strings"
                            myString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: myRange)
                        
                            
                            discriptionLabel.attributedText = myString
//                            discriptionLabel.text = "cancellation time should be 30 min before the appointment."

                            if bookingDetailModel.bookingStatus == 9{
                                
                                if bookingDetailModel.bookingMode == 1{
                                    
                                    if bookingDetailModel.callDisconnected != "1"{
                                        var refreshAlert = UIAlertController(title: "Message", message: "Telehealth visit is started.Do you want to join the meeting ?", preferredStyle: UIAlertController.Style.alert)

                                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [self] (action: UIAlertAction!) in
                                          
                                            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoVC") as? VideoVC {
                                                vc.roomId = "\(String(describing:bookingDetailModel?.bookingId))"
                                                vc.bookingId = bookingDetailModel?.bookingId
                                                vc.delegate = self
                                            self.navigationController?.pushViewController(vc, animated: false)
                                            }
                                            
                                            
                                          }))

                                        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                                          
                                            self.disconnectBookingCall()
                                            
                                          }))

                                        present(refreshAlert, animated: true, completion: nil)
                                    }
                                }
                                discriptionLabel.isHidden = true
                                cancelButton.isHidden = true
                            } else {
                                discriptionLabel.isHidden = false
                                cancelButton.isHidden = false
                            }
                            
                            if bookingDetailModel.bookingStatus == 1 {
                                if (bookingDetailModel.vaidTodayStatus == 2) && (bookingDetailModel.bookingType == 2) {
//                                     Helper.alertVC(title: "Vaid Accepted", message: "Wait few minutes for appointment time")
//                                    BlockerAlert.instance.hide()
                                        self.showBookingDetails()
                                        discriptionLabel.isHidden = true
                                        cancelButton.isHidden = true
                                        BlockerAlert.instance.show()
                                   
                                } else {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    
                                    let bookingViewController = storyboard.instantiateViewController(withIdentifier: "BookingVC") as! vaiDRx_BookingVC
                                    bookingViewController.isComingFrom = isComimgFrom
                                    bookingViewController.modalPresentationStyle = .fullScreen
                                    let countDown = bookingDetailModel.countDown
                                    if countDown > 0 {
                                        
                                        if UserDefaults.standard.value(forKey: "RUNNINGTIMER") as? Int != nil{
                                            
                                            let currentDate = Date()
                                            let currentTimeStamp = currentDate.toMillis()
                                            
          let goneValue = Int(Double(currentTimeStamp!)) - bookingDetailModel.bookingStartTime
                                            
                                            bookingViewController.countDown = countDown - goneValue
                                            
                                        }
                                        else{
                                            bookingViewController.countDown = countDown
                                        }
                                        
                                        
                                    }
                                    self.present(bookingViewController, animated: true, completion: nil)
                                }
                               
                                
                            } else {
                                if bookingDetailModel.bookingType == 2 && bookingDetailModel.vaidTodayStatus == 0 {
                                    
                                    let vaidTodayView = ConfirmVaidToday.sharedInstance
                                    
                                    
                                    
                                    
                                    
                                    
                                    vaidTodayView.changeTitle(message: "\(bookingDetailModel.providerName) has an appointment available at \(Helper.changeDateFormate(timeStamp: "\(bookingDetailModel.bookingStartTime)")).Please accept the appointment within 5 min.")
                                //    vaidTodayView.changeTitle(message: "Please accept the appointment within 5 min.")
                                    
                                    
                                    vaidTodayView.delegate = self
                                    if vaidTodayView.isVisible != true {
                                        vaidTodayView.isVisible = true
                                        vaidTodayView.bookingId = bookingDetailModel.bookingId
                                        WINDOW_DELEGATE??.addSubview(vaidTodayView)
                                        
                                        vaidTodayView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                                        
                                        UIView.animate(withDuration: 0.5,
                                                       delay: 0.0,
                                                       options: UIView.AnimationOptions.beginFromCurrentState,
                                                       animations: {
                                                        
                                                        vaidTodayView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                                                        
                                        }) { (finished) in
                                            
                                        }
                                        
                                    }
                                    bookingButtonsView.isHidden = true
                                } else {
                                    
                                    self.showBookingDetails()
                                }
                            }
                            self.bookingId = bookingDetailModel.bookingId
                            
                        } else {
                            bookingButtonsView.isHidden = false
                            bookingDetailsView.isHidden = true
                        }
                    }
                    if bookingDetailModel != nil {
                        print(bookingDetailModel.bookingStatus)
//                        self.refresh()
                        if bookingDetailModel.bookingStatus == 1 {
                            flag = flag + 1
                            if flag < 2 {
                                self.refresh()
                            }
                        }
                        self.tableView.reloadData()
                    }
                   
                } else {
                    bookingButtonsView.isHidden = false
                    bookingDetailsView.isHidden = true
                }
                break
            default:
                break
            }
            break
        default:
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
            }
            break
        }
        
    }
    
    
    func disconnectBookingCall() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {

            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        disconnectCall.disconnectBookingCallAPICall(bookingid: bookingDetailModel!.bookingId) { (statCode, errMsg, dataResp) in

            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.disconnectBookingCall)
        }
    }
    
    @IBAction func bookPastVaidAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let pastViewController = storyboard.instantiateViewController(withIdentifier: "pastProvidersVC") as! vaiDRx_PastProvidersVC
        pastViewController.vaidType = self.vaidType
        pastViewController.isComingfromTitle = isComimgFrom
        let navController = UINavigationController(rootViewController: pastViewController)
        
        // Creating a navigation controller with pastViewController at the root of the navigation stack.
        self.navigationController?.pushViewController(pastViewController, animated: true)
        
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        cancelBookingScreen = CancelBookingScreen.sharedInstance
        
        cancelBookingScreen.bookingID = bookingId
        
        WINDOW_DELEGATE??.addSubview(cancelBookingScreen)
        
        cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options:UIView.AnimationOptions(rawValue: UIView.AnimationOptions.beginFromCurrentState.rawValue),
                       animations: {
                        
                        self.cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { (finished) in
            
            self.cancelBookingScreen.getCancelReasonsAPI()
            
        }
        
    }
    
}

extension HomeScreenViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getAllMusicians.rawValue:
            
            initializeMQTTMethods(initial: false)
            
            break
            
        default:
            break
        }
    }
    
}

extension HomeScreenViewController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let gigTimeCell:MDGigTimeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "gigTimeCell", for: indexPath) as! MDGigTimeCollectionViewCell
        
        gigTimeCell.timeLabel.text = "\(30*(indexPath.row+1)) MIN."
        
        
        if selectedEventStartTag == indexPath.row {
            
            gigTimeCell.timeLabel.textColor = UIColor.red
            gigTimeCell.selectedDivider.isHidden = false
            
            gigTimeCell.timeLabel.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            
            UIView.animate(withDuration: 0.8,
                           delay: 0.2,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 5,
                           options: [],
                           animations: {
                            
                            gigTimeCell.timeLabel.transform = .identity
                            
            }) { (success) in
                
                
            }
            
            
        } else {
            
            gigTimeCell.timeLabel.textColor = UIColor.black
            gigTimeCell.selectedDivider.isHidden = true
            
        }
        
        return gigTimeCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sizeOfEachCell:CGSize = CGSize.init(width: (collectionView.bounds.size.width - 30)/3.5, height: 35)
        
        return sizeOfEachCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedEventStartTag = indexPath.row
        appointmentLocationModel.selectedEventStartTag = selectedEventStartTag
        self.eventStartCollectionView.reloadData()
        
    }
    
}


extension HomeScreenViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,1:
            return 1
        default:
            if bookingDetailModel != nil {
                
                if bookingDetailModel.paymentType == 0 {
                    return 4
                } else {
                    if bookingDetailModel.bookingStatus == 1{
                        return 3
                    }
                    else{
                        return 4
                    }
                    
                }
            } else {
                
                return 4
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_Home_DoctorDetail.self)) as! vaiDRx_Home_DoctorDetail
            if self.bookingDetailModel != nil {
                if !bookingDetailModel.providerImageURL.isEmpty {
                  
                    cell.docImage.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
                                              placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                              options: [.transition(ImageTransition.fade(1))],
                                              progressBlock: { receivedSize, totalSize in
                    },
                                              completionHandler: nil)
                }else {
                    cell.docImage.image = #imageLiteral(resourceName: "myevent_profile_default_image")
                }
                
                cell.docName.text = bookingDetailModel.providerName
                if bookingDetailModel.address2.contains(bookingDetailModel.address1){
                    cell.clinicAddress.text = bookingDetailModel.address2
                }
                else{
                    cell.clinicAddress.text = bookingDetailModel.address1 + bookingDetailModel.address2
                }
            } else {
                cell.docName.text = " "
                cell.clinicAddress.text = " "
                cell.docImage.image = #imageLiteral(resourceName: "myevent_profile_default_image")
            }
            
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_Home_BookingIDCell.self)) as! vaiDRx_Home_BookingIDCell

//            var str = "BookingId: 1234"
//            var range = NSMakeRange(9, (str.count - 9))
//
//            if bookingDetailModel != nil {
//                str = "BookingId: \(bookingDetailModel.bookingId)"
//                range = NSMakeRange(9, (str.count - 9))
//            }
//
//            cell.bookingId.attributedText = Helper.attributedString(from: str, nonBoldRange: range)
            if bookingDetailModel != nil {
                if bookingDetailModel.address2.contains(bookingDetailModel.address1){
                    cell.bookingId.text = bookingDetailModel.address2
                }
                else{
                    cell.bookingId.text = bookingDetailModel.address1 + bookingDetailModel.address2
                }
                if bookingDetailModel.bookingStatus == 9 {
                    cell.distanceLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: 0.0, mileageMatric: 1)
                } else {
                    print("mybookingdist \(bookingDetailModel.distance)")
                    if bookingDetailModel.distance > 0.0 {
                        cell.distanceLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: bookingDetailModel.distance, mileageMatric: 1)
                    }
                    
                }
                
                
//                cell.bookingId.text = bookingDetailModel.address1
            } else {
                
                cell.bookingId.text = " "
               
            }
//            cell.bookingId.text = bookingDetailModel.address1
            return cell
            
        case 2:
            
//            let Keys = ["Booking Status", "Doctor needed for", "Date", "Booking Time","Appointement upto", "Insurance", "Payment"]
//            let values = ["Vaid Accepted", "Now", "Friday, 16 Mar 2016", "05:30 PM", "06:30 PM", "Banner Life", "Insurance"]
            
            
            var Keys = ["Office number:","Vaid confirmed:", "Date:", "Arrive by:"]
            let values = [ "00000","Now", "Friday, 16 Mar 2016", "05:30 PM"]
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: vaiDRx_Home_DataCell.self)) as! vaiDRx_Home_DataCell
            
            switch indexPath.row {
            case 0:
                cell.key.text = Keys[indexPath.row]
                if bookingDetailModel != nil {
               
                    cell.value.text = bookingDetailModel.providerPhoneNumber
                } else {
                    cell.value.text = values[indexPath.row]
                    cell.value.textColor = UIColor.blue
                }
            case 1:
                cell.key.text = Keys[indexPath.row]
                cell.value.textColor = UIColor.white
                if bookingDetailModel != nil {
                    
                    if bookingDetailModel.bookingType == 1 {
                        
                        cell.value.text = "Now"
                    } else {
                        
                        cell.value.text = "Today"
                    }
                    
                } else {
                    
                    cell.value.text = values[indexPath.row]
                }
                
            case 2:
                cell.key.text = Keys[indexPath.row]
                
                if bookingDetailModel != nil {
                    
                    cell.value.text = Helper.getDayAndDateFromTimeStamp(timeStamp: Int64(bookingDetailModel.bookingStartTime))
                    
                } else {
                    
                    cell.value.text = values[indexPath.row]
                }
                
                
            case 3:
                cell.key.text = Keys[indexPath.row]
                if bookingDetailModel != nil {
                    if bookingDetailModel.bookingType == 1 {
                       if bookingDetailModel.bookingStatus == 9 {
                            cell.key.text = "Visit Status"
                            cell.value.text = "Started"
                        } else {
                            cell.key.text = "Arrive by:"
                            cell.value.text = Helper.getTimePlusOneHr(timeStamp: Int64(bookingDetailModel.bookingStartTime))
                        }
                        
                    } else {
                       if bookingDetailModel.bookingStatus == 9 {
                            cell.key.text = "Visit Status"
                            cell.value.text = "Started"
                            cell.key.isHidden = false
                            cell.value.isHidden = false
                        } else {
                            if bookingDetailModel.bookingStatus == 1{
                                cell.key.isHidden = true
                                cell.value.isHidden = true
                            }
                            else{
                                cell.key.text = "Appointment time:"
                                cell.value.text = Helper.getOnlyTimeFromTimeStamp(timeStamp: Int64(bookingDetailModel.bookingStartTime))
                                cell.key.isHidden = false
                                cell.value.isHidden = false
                            }
                            
                        }
                        
                    }
//                    cell.key.text = Keys[indexPath.row]
                
                } else {
                    cell.value.text = values[indexPath.row]
                }
//
//            case 3:
//                if bookingDetailModel != nil {
//                    cell.key.text = "Office phone no:"
//                    cell.value.text = bookingDetailModel.providerPhoneNumber
//                } else {
//                    cell.key.text = "Office phone no:"
//                    cell.value.text = "000"
//                }
                
                
            default:
                cell.key.text = Keys[indexPath.row]
                cell.value.text = values[indexPath.row]
                
            }
            return cell
            
        default:
            break
        }
        
        return UITableViewCell()
    }
    
}

extension HomeScreenViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 95
        case 1:
            return 90
        default:
            if (indexPath.row == 2) {
                if cancelButton.isHidden {
                    return 0
                }
            }
            return 65
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 2 {
            if indexPath.row == 0 {
                if let url = NSURL(string: "tel://\(bookingDetailModel.providerPhoneNumber)"), UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        if let height = heightAtIndexPath.object(forKey: indexPath) as? NSNumber {
//
//            return CGFloat(height.floatValue)
//
//        } else {
//
//            return UITableViewAutomaticDimension
//        }
//    }
}

extension HomeScreenViewController: VaidTodayProtocol {
    
    func bookingAccepted() {
       
        getBookings()
//        self.showBookingDetails()
        
    }
    
    func isPatientMinor(value: Int) {
        CBModel.ageType = value
       // liveBookinAPI()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let bookingViewController = storyboard.instantiateViewController(withIdentifier: "BookingTypeVC") as! vaiDRx_BookingTypeVC
        bookingViewController.delegateForApi = self
        bookingViewController.isComingFrom = isComimgFrom
        self.present(bookingViewController, animated: true, completion: nil)
        
        
    }
}

extension HomeScreenViewController: EmergencyMessageDelegate {
    func okPressed() {
        let vaidTodayView = ConfirmVaidToday.sharedInstance
        vaidTodayView.change(source: "Booking")
        //        vaidTodayView.isComingFor = "Booking"
        vaidTodayView.changeTitle(message: "Is the patient  above 18 years?")
        vaidTodayView.delegate = self
        
        
        
        WINDOW_DELEGATE??.addSubview(vaidTodayView)
        
        vaidTodayView.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options:UIView.AnimationOptions(rawValue: UIView.AnimationOptions.beginFromCurrentState.rawValue),
                       animations: {
                        
                        vaidTodayView.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { (finished) in
            
        }
    }
}



//
//  HomeVCBookLaterMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

extension HomeScreenViewController {

    func showBookLaterDetails() {
        
        if appointmentLocationModel.bookingRequested {
            
            appointmentLocationModel.bookingRequested = false
            
            Helper.clearLaterBookingRequestDetails()
            hideSelectedBookLaterDetails()
            selectedDate = scheduleDate
            datePickerView.date = selectedDate
            selectedEventStartTag = 0
            
            appointmentLocationModel.pickupLatitude = locationObj.latitute
            appointmentLocationModel.pickupLatitude = locationObj.longitude
            appointmentLocationModel.pickupAddress = locationObj.address
            self.addressLabel.text = locationObj.address
            
            self.showCurrentLocation()
            
        } else {
            
            if appointmentLocationModel.bookingType == BookingType.Schedule {
                
                selectedDate = appointmentLocationModel.scheduleDate
                selectedEventStartTag = appointmentLocationModel.selectedEventStartTag
                datePickerView.date = selectedDate
                showSelectedBookLaterDetails()
                
            } else {
                
                hideSelectedBookLaterDetails()
                selectedDate = scheduleDate
                datePickerView.date = selectedDate
                selectedEventStartTag = 0
            }
        }
        
        self.eventStartCollectionView.reloadData()
    }
    
    func updateSelectedBookLaterDetails(){
        
        if selectedDate != nil {
            
            let dateformat = DateFormatter()
            dateformat.dateFormat = "dd MMM yyyy hh:mm a"
            
            self.selectedBookLaterDateLabel.text = dateformat.string(from:selectedDate)
        }
        
    }

    func showSelectedBookLaterDetails(){
        
        updateSelectedBookLaterDetails()
        self.selectedBookLaterBackView.isHidden = false
    }
    
    func hideSelectedBookLaterDetails(){
        
        self.selectedBookLaterBackView.isHidden = true
    }
    

}
extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)/1000
    }
}

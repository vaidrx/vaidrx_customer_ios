//
//  KeyboardWrapperClass.swift
//  LiveM
//
//  Created by Apple on 07/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

@objc protocol KeyboardDelegate {
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    
    @objc func keyboardWillShow(notification: NSNotification)
    
    
    /**
     *  Keyboard Close Delegate
     *
     *  @param notification Keyboard notification details
     */
    
    @objc func keyboardWillHide(notification: NSNotification)
    
}


extension AppDelegate {
    
    func registerForKeyboardNotifications(){
        
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShown(notification:)), name: NSNotification.Name.init(UIResponder.keyboardWillShowNotification.rawValue), object: nil)
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide(notification:)), name: NSNotification.Name.init(UIResponder.keyboardWillHideNotification.rawValue), object: nil)
    }
    
    /// Unsubscribe Notification methods
    func deregisterFromKeyboardNotifications(){
        
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init(UIResponder.keyboardWillShowNotification.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init(UIResponder.keyboardWillHideNotification.rawValue), object: nil)
    }
    
    /// called when keyboard triggerd
    ///
    /// - Parameter notification: Notification
    @objc func keyboardShown(notification: NSNotification){
        
        if (self.keyboardDelegate != nil) {
            
            self.keyboardDelegate?.keyboardWillShow(notification: notification)
        }
        
    }
    
    /// called when keyboard Close
    ///
    /// - Parameter notification: NOtification
    @objc func keyboardHide(notification: NSNotification){
       
        if (self.keyboardDelegate != nil) {
            
            self.keyboardDelegate?.keyboardWillHide(notification: notification)
        }

    }

    
}

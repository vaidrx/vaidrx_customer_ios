//
//  vaiDRx_AddNewCard_ButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import Stripe

extension vaiDRx_AddNewCardVC {
    
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func scanCardAction(_ sender: UIButton) {
        let scanCardViewController = CardIOPaymentViewController.init(paymentDelegate: self as CardIOPaymentViewControllerDelegate)
        scanCardViewController?.hideCardIOLogo = true
        scanCardViewController?.disableManualEntryButtons = true
        scanCardViewController?.modalPresentationStyle = UIModalPresentationStyle.formSheet
        scanCardViewController?.title = "Scan Card"
        scanCardViewController?.navigationController?.isNavigationBarHidden = false
        
        self.present(scanCardViewController!, animated: true, completion: nil)
    }
    
    
    @IBAction func getCountryAction(_ sender: UIButton) {
        
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        if !paymentTextField.isValid {
            
            Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.MissingCardDetails)
        }
            
        else if stripeManager.defaultPublishableKey().length == 0 {
            
            Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.MissingStripeKey)
        }
//        else if zipCodeTextField.text!.length == 0 {
//
//            Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.ZipcodeMissing)
//        }
        else {
            let cps = paymentTextField?.cardParams
            Helper.showPI(_message: PROGRESS_MESSAGE.Adding)
            let cardParameters = STPCardParams()
            
            cardParameters.number =  cps?.number
            cardParameters.expMonth = cps?.expMonth as! UInt
            cardParameters.expYear = cps?.expYear as! UInt
            cardParameters.cvc = cps?.cvc
            
            stripeManager.createStripeCardToken(cardParams: cardParameters, completion: { (tokenId, error) in
                
                if (error != nil) {
                    
                    Helper.hidePI()
                    Helper.showAlert(head: ALERTS.Error, message:(error!.localizedDescription))
                }
                else {
                    Helper.hidePI()
                    print("Card Token: \(String(describing: tokenId))")
                    self.cardToken = tokenId
                    self.registerViewModel.cardToken = self.cardToken
                    //self.serviceRequestToMakeRegister()
                    //                    self.addCardAPI(tokenId: tokenId!)
                    self.performSegue(withIdentifier: "toTermsAndConditions", sender: self)
                }
                
            })
            
        }
    }
    
}

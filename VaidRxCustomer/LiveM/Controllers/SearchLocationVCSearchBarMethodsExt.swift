//
//  SearchLocationVCSearchBarMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GooglePlaces
import GoogleMaps
import Alamofire

extension SearchLocationViewController:UISearchBarDelegate {
    
    //MARK: - Autocomplete SearchBar methods -
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        autoCompleteTimer.invalidate()
        searchAutocompleteLocations()
        searchBar.resignFirstResponder()
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let searchWordProtection: String = searchBar.text!.replacingOccurrences(of: " ", with: "")
        
        if searchWordProtection.length > 0 {
             searchAutocompleteLocations()
//            runScript()
        }
        else {
            
            loadsManageAndPreviouslySelectedAddress()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        searchString = searchBar.text
        searchString = searchString.replacingOccurrences(of: " ", with: "+")
        searchString = (searchString as NSString).replacingCharacters(in: range, with: text)
        if searchString.hasPrefix("+") && searchString.length > 1 {
            searchString = (searchString as NSString?)?.substring(from: 1)
        }
        return true
    }
    
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    
    //MARK: - Search Methods -
    
    /// Method to maintain some interval between each search
    func runScript() {
        
        autoCompleteTimer.invalidate()
        autoCompleteTimer = Timer.scheduledTimer(timeInterval: 0.15, target: self, selector: #selector(self.searchAutocompleteLocations), userInfo: nil, repeats: false)
        
    }
    
    
    /// Intermediate method to check the search word is already having some suggestions (previous search word or not)
    @objc func searchAutocompleteLocations() {
        
        if searchString.count == 0 {
            return
        }
        
        var urlForSearchLocation = ""
         let text = searchString.replacingOccurrences(of: " ", with: "")
        
        
        if loadedLocationLat != nil {
            urlForSearchLocation = String(format: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&location=%f,%f&radius=500&amplanguage=%@&key=%@" ,text,loadedLocationLat!,loadedLocationLong!,"en","AIzaSyBZtlDnhkrvFgsU6Smk9R8S71JnOrxpffo")
//            "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchString!)&location=\(loadedLocationLat!),\(loadedLocationLong!)&radius=20000&amplanguage=en@&key=AIzaSyBZtlDnhkrvFgsU6Smk9R8S71JnOrxpffo"
        } else {
            urlForSearchLocation = String(format: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&radius=500&amplanguage=%@&key=%@" ,text,"en","AIzaSyBZtlDnhkrvFgsU6Smk9R8S71JnOrxpffo")
//            "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchString!)&amplanguage=en@&key=AIzaSyBZtlDnhkrvFgsU6Smk9R8S71JnOrxpffo"
        }
        
        print(urlForSearchLocation)
//        Alamofire.request(urlForSearchLocation,
//                          method: .get,
//                          parameters: nil ).validate().responseJSON { response in
//                            if let result = response.result.value {
//                                let resultArr = result as! NSDictionary
//                                let statusMsg = "\(resultArr.value(forKey: "status") ?? "")"
//                                if  statusMsg == "OK" {
//                                    let predictionArr = resultArr.value(forKey: "predictions") as! Array<Any>
//                                    print(predictionArr)
//                                    self.arrayOfCurrentSearchResult += predictionArr
//
//                                } else {
//                                    let errorMessage = "\(resultArr.value(forKey: "error_message") ?? "")"
//                                    Helper.showAlert(head: statusMsg, message: errorMessage)
//                                }
//
//                            }
//                     self.tableView.reloadData()
//        }
        NetworkHelper.requestPOST(urlString: urlForSearchLocation,
                                  success: { (response) in
                                    
                                    if response.isEmpty == false {
                                        if let array = response["predictions"].arrayObject {
                                            
                                            self.arrayOfCurrentSearchResult = array
                                        }
                                    }
                                    self.tableView.reloadData()
        },
                                  failure: { (Error) in
                                    // self.showErrorMessage(message: Error.localizedDescription)
        })
        
        
        //        let containtsBoolValue:Bool = arrayOfPastSearchWords.contains(where: { ($0 as! String) == searchString })
        
        //        if !containtsBoolValue {
        //
        //            arrayOfCurrentSearchResult.removeAll()
        //            tableView.reloadData()
        //            arrayOfPastSearchWords.append(searchString)
        //
        //
        //
        //            //            autoComplete(usingGMS:searchString, withCompletion: {(_ results: [Any]) -> Void in
        //            //
        //            //                if results.count > 0 {
        //            //
        //            //                    self.arrayOfCurrentSearchResult += results
        //            //                     print(self.arrayOfCurrentSearchResult)
        //            //                    let searchResult: [String: Any] = [
        //            //                        "keyword": self.searchString,
        //            //                        "results": results
        //            //                    ]
        //            //
        //            //                    self.arrayOfPastSearchResults.append(searchResult)
        //            //                    self.tableView.reloadData()
        //            //                }
        //            //                else {
        //            //
        //            //                }
        //            //            })
        //
        //
        //        } else if containtsBoolValue {
        //
        //            arrayOfCurrentSearchResult.removeAll()
        //
        //            for pastSearchResult in arrayOfPastSearchResults {
        //
        //                if ((pastSearchResult as! [String:Any])["keyword"] as! String) == searchString {
        //
        //                    arrayOfCurrentSearchResult += ((pastSearchResult as! [String:Any])["results"] as! [Any])
        //                    tableView.reloadData()
        //                }
        //            }
        //
        //        } else {
        //
        //            loadsManageAndPreviouslySelectedAddress()
        //        }
    }
    
    
    
    /// Method to get Autocomplete addresses using Google Places
    ///
    /// - Parameters:
    ///   - searchWord: searh word user as entered
    ///   - complete: array list of suggestion addresses
    func autoComplete(usingGMS searchWord: String,
                      withCompletion complete: @escaping (_ result: [Any]) -> Void) {
        
        
        NSObject.cancelPreviousPerformRequests(withTarget: placesClient, selector: #selector(GMSPlacesClient.findAutocompletePredictions(fromQuery:filter:sessionToken:callback:)), object: self)
        
        
        
        
        let emptyArray = [Any]()
        
        if (searchWord.count ) > 0 {
            
            let filter = GMSAutocompleteFilter()
            filter.type =  GMSPlacesAutocompleteTypeFilter.noFilter
            
            let visibleRegion: GMSVisibleRegion = GMSVisibleRegion.init(nearLeft: CLLocationCoordinate2DMake(currentLatitude, currentLongitude), nearRight: CLLocationCoordinate2DMake(currentLatitude, currentLongitude), farLeft: CLLocationCoordinate2DMake(currentLatitude, currentLongitude), farRight: CLLocationCoordinate2DMake(currentLatitude, currentLongitude))
            
            let bounds = GMSCoordinateBounds(coordinate: visibleRegion.farLeft, coordinate: visibleRegion.nearRight)
            
            placesClient.findAutocompletePredictions(fromQuery: searchWord, filter: filter, sessionToken: nil) { (results, error) in
                if error != nil {
                    
                    DDLogDebug("Search Address Autocomplete Error: \(String(describing: error?.localizedDescription))")
                    
                    complete(emptyArray)
                    
                    return
                }
                if (results?.count)! > 0 {
                    
                    complete(results!)
                }
                else {
                    
                    complete(emptyArray)
                }
            }
            
            
        } else {
            
            complete(emptyArray)
        }
    }
    
    
}


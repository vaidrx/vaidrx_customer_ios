////
////  MQTTModule.swift
////  MQTT Chat Module
////
////  Created by Rahul Sharma on 11/07/17.
////  Copyright © 2017 Rahul Sharma. All rights reserved.
////
//
//import UIKit
//import MQTTClient
//import Foundation
//
///// This class is a model for interacting with the SwiftMQTT library.
//class MQTT {
//
//    struct Constants {
//
//        ///Ask server guy for host and port.
//        fileprivate static let host = "18.188.165.175"//"45.77.190.140"
//        fileprivate static let port:UInt32 = 1883
//    }
//
//    var sessionConnected = false
//    var sessionError = false
//    var sessionReceived = false
//    var sessionSubAcked = false
//
//    /// Shared instance object for gettting the singleton object
////    static let sharedInstance = MQTT()
//
//    ///This flag will tell you that you are connected or not.
//    var isConnected : Bool = false
//
//    ///current session object will going to store in this.
//    var mqttSession: MQTTSession!
//
//    ///Used for running the task in the background.
//    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
//
//    /// MQTT delegate object, its going to store the objects of the delegate receiver class.
//    let mqttMessageDelegate = MQTTDelegate()
//
//    /// Shared instance object for gettting the singleton object
//    static var obj:MQTT? = nil
//
//    class func sharedInstance() -> MQTT {
//
//        if obj == nil {
//
//            obj = MQTT()
//        }
//
//        return obj!
//    }
//
//
//    var connectionTimer:Timer!
//
//    func startConnectionTimer() {
//
//         if Utility.customerId.length > 0 {
//
//            connectionTimer = Timer.scheduledTimer(timeInterval: 5,
//                                                     target: self,
//                                                     selector: #selector(checkConnection),
//                                                     userInfo: nil,
//                                                     repeats: true)
//         } else {
//
//            stopConnectionTimer()
//        }
//    }
//
//    func stopConnectionTimer() {
//
//        if connectionTimer != nil && self.connectionTimer?.isValid == true {
//
//            connectionTimer.invalidate()
//            connectionTimer = nil
//        }
//
//    }
//
//    @objc func checkConnection() {
//
//        if self.isConnected == false {
//
//            createConnection()
//
//        } else {
//
//            stopConnectionTimer()
//        }
//    }
//
//
//    /// Used for creating the initial connection.
//    func createConnection() {
//
//        /// Observer for app coming in foreground.
////        NotificationCenter.default.addObserver(self, selector: #selector(MQTT.reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
//
////        registerBackgroundTask()
//
//
//        ///creating connection with the proper client ID.
//        if Utility.customerId.length > 0 {
//
//            self.connect(withClientId: Utility.customerId)
//        }
//        else {
//
//            stopConnectionTimer()//If any connetion timer is running is closed
//        }
//
//    }
//
//    /// Used for subscribing the different channels.
//    func subscrtibeToChannels() {
//
//        MQTTOnDemandAppManager.sharedInstance().subScribeToInitialTopics()
//
////        self.subscribeChannel(withChannelName: "\(MQTT_TOPIC.ListAllProviders)\(Utility.customerId)")
////        self.subscribeChannel(withChannelName: "\(MQTT_TOPIC.FCMTopic)" + "\(Utility.fcmTopic)")
//    }
//
//
//
//
//    /// Used for subscribing the channel
//    ///
//    /// - Parameter channelName: current channel which you want to subscribe.
//    func subscribeChannel(withChannelName channelName : String ) {
//        let topicToSubscribe = channelName
//        self.subscribe(topic: "\(topicToSubscribe)", withDelivering: .exactlyOnce)
//    }
//
//
//
//    /// Used for Unsubscribing the channel
//    ///
//    /// - Parameter channelName: current channel which you want to Unsubscribing.
//    func unsubscribeTopic(topic : String) {
//
//        mqttSession.unsubscribeTopic(topic)
//
//        mqttSession.unsubscribeTopic(topic) { (error) in
//            print(error ?? "Unsubscribe Succesfully")
//        }
//
//    }
//
//    func closeMQTTConnection() {
//
//        if mqttSession != nil && isConnected {
//
//            mqttSession.close()
//        }
//
//    }
//
//    /// Used for subscribing the channel
//    ///
//    /// - Parameters:
//    ///   - topic: name of the current topic (It should contain the name of the topic with saperators)
//    ///
//    /// eg- Message/UserName
//    ///   - Delivering: Type of QOS // can be 0,1 or 2.
//    fileprivate func subscribe(topic : String, withDelivering Delivering : MQTTQosLevel) {
//        mqttSession.subscribe(toTopic: topic, at: Delivering) { (error, subscriptionArray) in
//            print(error ?? "", subscriptionArray ?? "")
//        }
//    }
//
//
//    /// Used for reinstate the background task
//    @objc func reinstateBackgroundTask() {
//        if (backgroundTask == UIBackgroundTaskInvalid) {
//            registerBackgroundTask()
//        }
//    }
//
//    ///Here I am registering for the background task.
//    func registerBackgroundTask() {
//        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
//            self?.endBackgroundTask()
//        }
//        assert(backgroundTask != UIBackgroundTaskInvalid)
//    }
//
//    ///Before background task ending this method is going to be called.
//    func endBackgroundTask() {
//        print("Background task ended.")
//        UIApplication.shared.endBackgroundTask(backgroundTask)
//        backgroundTask = UIBackgroundTaskInvalid
//    }
//
//
//
//    /// Used for pubishing the data in between channels.
//    ///
//    /// - Parameters:
//    ///   - jsonData: Data in JSON format.
//    ///   - channel: current channel name to publish the data to.
//    ///   - messageID: current message ID (this ID should be unique)
//    ///   - Delivering: Type of QOS // can be 0,1 or 2.
//    ///   - retain: true if you wanted to retain the messages or False if you don't
//    ///   - completion: This will going to return MQTTSessionCompletionBlock.
//
//    func publishData(wthData jsonData: Data, onTopic topic : String, retain : Bool, withDelivering delivering : MQTTQosLevel) {
//        mqttSession.publishData(jsonData, onTopic: topic, retain:retain , qos: delivering) { (error) in
//            if let error = error {
//                print("failed with error",error)
//            }
//        }
//    }
//
//    func connect() {
//        guard let newSession = MQTTSession() else {
//            fatalError("Could not create MQTTSession")
//        }
//
//        newSession.connect(toHost: Constants.host, port: Constants.port, usingSSL: false)
//        while !sessionConnected && !sessionError {
//            RunLoop.current.run(until: Date(timeIntervalSinceNow: 1))
//        }
//
//        newSession.publishData("sent from Xcode using Swift".data(using: String.Encoding.utf8, allowLossyConversion: false),
//                               onTopic: "MQTTSwift",
//                               retain: false,
//                               qos: .atMostOnce)
//        newSession.close()
//    }
//
//    /// Used for connecting with the server.
//    ///
//    /// - Parameter clientId: current Client ID.
//    func connect(withClientId clientId :String) {
//
//        let host = Constants.host
//        let port: UInt32 = Constants.port
//        guard let newSession = MQTTSession.init(clientId: clientId) else {
//            fatalError("Could not create MQTTSession")
//        }
//
//        mqttSession = newSession
//        newSession.delegate = mqttMessageDelegate
//        newSession.keepAliveInterval = 20000
//        newSession.clientId = clientId
//        newSession.cleanSessionFlag = false
//        newSession.connect(toHost: host, port: port, usingSSL: false) { (error) in
//
//            if let error = error {
//
//                print("MQTT Session is unable to connect",error)
//
//                if self.isConnected {
//
//                    self.isConnected = false
//                    self.startConnectionTimer()
//
//                } else {
//
//                    self.isConnected = false
//                    if self.connectionTimer == nil && self.connectionTimer?.isValid == false {
//
//                        self.startConnectionTimer()
//                    }
//                }
//
//
//
//            } else {
//
//                print("MQTT Connected")
//                self.isConnected = true
//                self.stopConnectionTimer()
//
//                ///Subscribing Acknowledgment Channel
//                self.subscrtibeToChannels()
//                MQTTOnDemandAppManager.sharedInstance().MQTTServerisConnected()
//            }
//        }
//    }
//
//    func pingPongActive() {
//
//    }
//}
//
//  MQTTModule.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 11/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import Foundation

/// This class is a model for interacting with the SwiftMQTT library.
class MQTT {
    
    struct Constants {
        
        ///Ask server guy for host and port.
//        fileprivate static let host = "18.222.49.19"
//        fileprivate static let port:Int = 2052
        
        fileprivate static let host = "3.16.144.147"
        fileprivate static let port:Int = 2052
    }
    
    /// Shared instance object for gettting the singleton object
    //    static let sharedInstance = MQTT()
    
    ///This flag will tell you that you are connected or not.
    var isConnected : Bool = false 
    
    ///current session object will going to store in this.
    var mqttSessionManager: MQTTSessionManager!
    
    ///Used for running the task in the background.
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    
    /// MQTT delegate object, its going to store the objects of the delegate receiver class.
    let mqttMessageDelegate = MQTTDelegate()
    
    /// Shared instance object for gettting the singleton object
    static var obj:MQTT? = nil
    
    class func sharedInstance() -> MQTT {
        
        if obj == nil {
            
            obj = MQTT()
        }
        
        return obj!
    }
    
    /// Used for creating the initial connection.
    func createConnection() {
        
        ///creating connection with the proper client ID.
        if Utility.customerId.length > 0 {
            
            self.connect(withClientId: Utility.customerId)
            
            if self.isConnected {
                
                self.subscrtibeToChannels()
            }
        }
        
        
    }
    
    func closeMQTTConnection() {
        
        if mqttSessionManager != nil {
            
            mqttSessionManager.disconnect { (error) in
                
                self.mqttSessionManager = nil
            }
        }
        
    }
    
    /// Used for subscribing the different channels.
    func subscrtibeToChannels() {
        
        MQTTOnDemandAppManager.sharedInstance().subScribeToInitialTopics()
        
        //        self.subscribeChannel(withChannelName: "\(MQTT_TOPIC.ListAllProviders)\(Utility.customerId)")
        //        self.subscribeChannel(withChannelName: "\(MQTT_TOPIC.FCMTopic)" + "\(Utility.fcmTopic)")
    }
    
    
    
    
    /// Used for subscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to subscribe.
    func subscribeChannel(withChannelName channelName : String ) {
        let topicToSubscribe = channelName
        self.subscribe(topic: "\(topicToSubscribe)", withDelivering: .exactlyOnce)
    }
    
    
    /// Used for reinstate the background task
    @objc func reinstateBackgroundTask() {
        if (backgroundTask == UIBackgroundTaskIdentifier.invalid) {
            registerBackgroundTask()
        }
    }
    
    ///Here I am registering for the background task.
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskIdentifier.invalid)
    }
    
    ///Before background task ending this method is going to be called.
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskIdentifier.invalid
    }
    
    
    /// Used for subscribing the channel
    ///
    /// - Parameters:
    ///   - topic: name of the current topic (It should contain the name of the topic with saperators)
    ///
    /// eg- Message/UserName
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    fileprivate func subscribe(topic : String, withDelivering Delivering : MQTTQosLevel) {
        if (mqttSessionManager != nil) {
            if (mqttSessionManager.subscriptions != nil) {
                var subscribeDict = mqttSessionManager.subscriptions!
                if (subscribeDict[topic] == nil) {
                    subscribeDict[topic] = Delivering.rawValue as NSNumber
                }
                self.mqttSessionManager.subscriptions = subscribeDict
            } else {
                let subcription = [topic : Delivering.rawValue as NSNumber]
                self.mqttSessionManager.subscriptions = subcription
            }
            self.mqttSessionManager.connect { (error) in
                
            }
        }
    }
    
    
    /// Used for Unsubscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to Unsubscribing.
    func unsubscribeTopic(topic : String) {
        
        if (mqttSessionManager != nil) {
            
            var unsubscribeDict = mqttSessionManager.subscriptions
            if unsubscribeDict?[topic] != nil {
                unsubscribeDict?.removeValue(forKey:topic)
            }
            self.mqttSessionManager.subscriptions = unsubscribeDict
            self.mqttSessionManager.connect { (error) in
                
            }
            
        }
    }
    
    
    /// Used for pubishing the data in between channels.
    ///
    /// - Parameters:
    ///   - jsonData: Data in JSON format.
    ///   - channel: current channel name to publish the data to.
    ///   - messageID: current message ID (this ID should be unique)
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    ///   - retain: true if you wanted to retain the messages or False if you don't
    ///   - completion: This will going to return MQTTSessionCompletionBlock.
    
    func publishData(wthData jsonData: Data, onTopic topic : String, retain : Bool, withDelivering delivering : MQTTQosLevel) {
        
        if (self.mqttSessionManager != nil) {
            
            mqttSessionManager.send(jsonData, topic: topic, qos: delivering, retain: retain)
        }
        else {
            
            if Utility.customerId.length > 0 {
                
                connect(withClientId: Utility.customerId)
            }
        }
    }
    
    /// Used for connecting with the server.
    ///
    /// - Parameter clientId: current Client ID.
    func connect(withClientId clientId :String) {
        
        if (self.mqttSessionManager == nil) {
            
            self.mqttSessionManager = MQTTSessionManager()
            let host = Constants.host
            let port: Int = Constants.port
            
            mqttSessionManager.delegate = mqttMessageDelegate
            
            mqttSessionManager.connect(to: host,
                                       port: port,
                                       tls: false,
                                       keepalive: 60,
                                       clean: false,
                                       auth: false,
                                       user: nil,
                                       pass: nil,
                                       will: false,
                                       willTopic: nil,
                                       willMsg: nil,
                                       willQos: .atMostOnce,
                                       willRetainFlag: false,
                                       withClientId: clientId,
                                       securityPolicy: nil,
                                       certificates: nil,
                                       protocolLevel: .version311,
                                       connectHandler: { (error) in
                                     print(error)
            })
            
        } else {
            
            self.mqttSessionManager.connect { (error) in
                
            }
            
        }
    }
}

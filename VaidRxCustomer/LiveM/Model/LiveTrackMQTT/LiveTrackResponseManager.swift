//
//  LiveTrackProviderMQTTModel.swift
//  LiveM
//
//  Created by Raghavendra V on 07/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

protocol LiveTrackResponseManagerDelegate {
   
    
    /// Delegate to show musician livetrack details
    ///
    /// - Parameter liveTrackDetails: liveTrack details model
    func liveTrackDetailsFromMQTT(liveTrackDetails:LiveTrackDetailsModel)
    
}


class LiveTrackResponseManager {
    
    static var obj:LiveTrackResponseManager? = nil
    
    class func sharedInstance() -> LiveTrackResponseManager {
        
        if obj == nil {
            
            obj = LiveTrackResponseManager()
        }
        
        return obj!
    }
    
    var delegate:LiveTrackResponseManagerDelegate? = nil
    
    
    
    /// Method to maintain LiveTrack details getting from MQTT
    ///
    /// - Parameters:
    ///   - liveTrackMessage: liveTrack message recieved from MQTT
    ///   - topicName: name of topic the live track message as recieved
    func liveTrackMessageFromMQTT(liveTrackMessage:[String:Any], topicName:String) {
        
        if delegate != nil && Helper.getCurrentVC() is LiveTrackViewController {
        
            let liveTrackDetail = LiveTrackDetailsModel.init(liveTrackDetails: liveTrackMessage)
            
            if delegate != nil {
                
                delegate?.liveTrackDetailsFromMQTT(liveTrackDetails: liveTrackDetail)
                
            } else {
                
                if MQTT.sharedInstance().isConnected {
                    
                    MQTT.sharedInstance().unsubscribeTopic(topic: topicName)
                }
            }

        } else {
            
            if MQTT.sharedInstance().isConnected {
                
                MQTT.sharedInstance().unsubscribeTopic(topic: topicName)
            }

        }
        
    }
    
    
    
    /// Method to subscribe particular musician topic
    ///
    /// - Parameter providerId: musician id
    func subscribeToParticularProvider(providerId:String) {
        
        if MQTT.sharedInstance().isConnected {
            
            MQTT.sharedInstance().subscribeChannel(withChannelName: "\(MQTT_TOPIC.ProviderLiveTrack)" + "\(providerId)")
        }
        
    }
    
    
    
    /// Method to unsubscribe particular musician topic
    ///
    /// - Parameter providerId: musician id
    func unsubscribeToParticularProvider(providerId:String) {
        
        if MQTT.sharedInstance().isConnected {
            
            MQTT.sharedInstance().unsubscribeTopic(topic:"\(MQTT_TOPIC.ProviderLiveTrack)" + "\(providerId)")
        }
        
    }
    
}

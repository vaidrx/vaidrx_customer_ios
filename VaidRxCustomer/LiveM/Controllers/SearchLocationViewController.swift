//
//  SearchLocationViewController.swift
//  LiveM
//
//  Created by Raghavendra V on 28/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GooglePlaces
import Alamofire


protocol SearchAddressDelegate {
    
    /// Search Address Delegate method this will when user selected any address
    ///
    /// - Parameter addressDetails: selected address information
    func searchAddressDelegateMethod(_ addressModel:AddressModel, descript:String)
    
    func searchAddressCurrentLocationButtonClicked()
    
}


class SearchLocationViewController:UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var addressSearchBar: UISearchBar!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var navigationBackButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var poweredByGoogleImageView: UIImageView!
    
    var delegate:SearchAddressDelegate? = nil
    
    var previouslySelectedAddress:[Any] = []
    var managedAddress:[Any] = []
    let placesClient:GMSPlacesClient = GMSPlacesClient.shared()
    
    var arrayOfPastSearchWords:[Any] = []
    var arrayOfPastSearchResults:[Any] = []
    
    var arrayOfCurrentSearchResult:[Any] = []
    
    let locationObj = LocationManager.sharedInstance()
    
    var autoCompleteTimer:Timer = Timer()
    var searchString:String!
    
    var selectedIndexPathToDeleteAddress:IndexPath!
    
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!
    
    var selectedAddressIdToDelete = ""
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    let searchLocationViewModel = SearchLocationViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentLatitude = locationObj.latitute
        currentLongitude = locationObj.longitude
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        loadsManageAndPreviouslySelectedAddress()
        acessClass.acessDelegate = self
        setupGestureRecognizer()
        
        self.addressSearchBar.becomeFirstResponder()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 38, green: 170, blue: 167) //UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        Helper.setShadowFor(topView, andWidth:topView.frame.size.width, andHeight:44)
        Helper.setShadowFor(poweredByGoogleImageView, andWidth:poweredByGoogleImageView.frame.size.width, andHeight:44)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
        self.addressSearchBar.resignFirstResponder()
    }
    
    
    
    
    @IBAction func navigationBackButtonAction(_ sender: Any) {
        
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionReveal,
//                                                                  subType: kCATransitionFromBottom,
//                                                                  for: (self.navigationController?.view)!,
//                                                                  timeDuration: 0.3)
//
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func currentLocationButtonActions(_ sender: Any) {
        
        if  delegate != nil {
            
            delegate?.searchAddressCurrentLocationButtonClicked()
        }
        
        navigationBackButtonAction(navigationBackButton)
    }
    
}

extension SearchLocationViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.deleteAddress.rawValue:
            
            deleteAddressAPI(addressId: selectedAddressIdToDelete)
            
            break
            
        default:
            break
        }
    }
    
}

extension SearchLocationViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension SearchLocationViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}

extension SearchLocationViewController {
    
    func getPlaceInformation(placeId:String, des:String) {
        
        let placeAPI = String(format: "https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&fields=address_component,geometry&key=%@",placeId, "AIzaSyBZtlDnhkrvFgsU6Smk9R8S71JnOrxpffo")
        Alamofire.request(placeAPI,
                          method: .get,
                          parameters: nil ).validate().responseJSON { response in
                            print(response)
                            if let result = response.result.value {
                                let resultArr = result as! NSDictionary
                                self.addSelectedAddressIntoCouchDB(selectedAddressDetails: AddressModel.forSearchAddress(addressDetails: resultArr,addressid:placeId, descript: des), descript: des)
                            }
                            
        }
        
        
        
        
        
        //        placesClient.lookUpPlaceID(placeId) { (result, error) in
        //
        //            if self.delegate != nil {
        //
        //                if error != nil {
        //
        //                    DDLogDebug("Place Details error : \(String(describing: error?.localizedDescription))");
        //                    return;
        //
        //                } else if result != nil {
        //
        //                    self.addSelectedAddressIntoCouchDB(selectedAddressDetails: AddressModel.forSearchAddress(addressDetails: result!))
        //
        //                } else {
        //
        //                    DDLogDebug("No place details for PleaceId: \(placeId)")
        //                    return;
        //                }
        //
        //            }
        //        }
    }
    
    func addSelectedAddressIntoCouchDB(selectedAddressDetails:[String:Any],descript:String) {
        
        //For Current Search Selected Address
        let addressModel:AddressModel = AddressModel.init(addressDetails: selectedAddressDetails)
        
        if previouslySelectedAddress.count > 0 {
            
            //Checking this Address Details Already Exist in Database
            let predicate = NSPredicate(format: "addressId = %@", selectedAddressDetails[ADDRESS.AddressId] as! CVarArg)
            let results: [Any] = previouslySelectedAddress.filter { predicate.evaluate(with: $0) }
            
            if results.count == 0 {
                
                //Adding The Address Details To Database
                previouslySelectedAddress.append(selectedAddressDetails)
                
                AddressCouchDBManager.sharedInstance.updateSearchAddressDetailsToCouchDBDocument(searchAddressArray: previouslySelectedAddress)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.passTheSelectedAddressToDelegate(addressModel: addressModel, des: descript)
                }
                
                
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.passTheSelectedAddressToDelegate(addressModel: addressModel, des: descript)
                }
            }
            
        } else {
            
            //Adding The Address Details To Database
            previouslySelectedAddress.append(selectedAddressDetails)
            
            AddressCouchDBManager.sharedInstance.updateSearchAddressDetailsToCouchDBDocument(searchAddressArray: previouslySelectedAddress)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.passTheSelectedAddressToDelegate(addressModel: addressModel, des: descript)
            }
        }
        
    }
    
    func deletePreviousAddress() {
        
        if self.addressSearchBar.text?.length == 0 && selectedIndexPathToDeleteAddress.section == 0 {
            
            let addressModel:AddressModel = AddressModel.init(manageAddressDetails: managedAddress[selectedIndexPathToDeleteAddress.row])
            
            selectedAddressIdToDelete = addressModel.addressId
            
            deleteAddressAPI(addressId:addressModel.addressId)
        }
        else
        {
            if self.addressSearchBar.text?.length == 0 && selectedIndexPathToDeleteAddress.section == 1 {
                
                //For Previously Selected Address
                let addressId = (previouslySelectedAddress[selectedIndexPathToDeleteAddress.row] as! [String:Any])[ADDRESS.AddressId]
                
                let predicate = NSPredicate(format: "addressId = %@", addressId as! CVarArg)
                previouslySelectedAddress = previouslySelectedAddress.filter { !predicate.evaluate(with: $0) }
                
                AddressCouchDBManager.sharedInstance.updateSearchAddressDetailsToCouchDBDocument(searchAddressArray: previouslySelectedAddress)
                
            }
            
            loadsManageAndPreviouslySelectedAddress()
            
        }
        
    }
    
}

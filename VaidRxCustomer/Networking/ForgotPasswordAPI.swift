//
//  ForgotPasswordAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 22/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class ForgotPasswordAPI {
    
    let disposebag = DisposeBag()
    let forgotPassword_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call forgot password service API
    ///
    /// - Parameters:
    ///   - forgotPasswordWithEmail: Bool value for forgot password with email or phone number
    ///   - emailText: email address
    ///   - phoneNumberText: phone number text
    ///   - countryCode: country code text
    func forgotPasswordServiceAPICall(forgotPasswordWithEmail:Bool, emailText:String, phoneNumberText:String, countryCode:String) {
        
        let strURL = API.BASE_URL + API.METHOD.FORGET_PASSWORD
        
        var requestParams:[String:Any] = [
            SERVICE_REQUEST.ForgetPassword.Email_Phone:forgotPasswordWithEmail ? emailText.lowercased() : phoneNumberText,
            SERVICE_REQUEST.ForgetPassword.processType: forgotPasswordWithEmail ? 2 : 1,
            SERVICE_REQUEST.ForgetPassword.userType:1
        ]
        
        if !forgotPasswordWithEmail {
            
            requestParams[SERVICE_REQUEST.CountryCode] = countryCode
            
            Helper.showPI(_message: PROGRESS_MESSAGE.ValidatingPhoneNumber)
            
        } else {
            
            Helper.showPI(_message: PROGRESS_MESSAGE.ValidatingEmail)
        }
        
        
        
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: nil)
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    func checkResponse(statusCode:Int, responseDict: [String:Any]){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.forgotPassword_Response.onNext(responseModel)
            break
        }
    }
    
}

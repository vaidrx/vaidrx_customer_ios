//
//  SearchLocationVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces


extension SearchLocationViewController:UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if addressSearchBar.text?.length == 0 {
            
            return 2
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if addressSearchBar.text?.length == 0 {
            
            switch section {
                
            case 0:
                //                return managedAddress.count
                return 0
                
            default:
                //                return previouslySelectedAddress.count
                return 0
            }
            
        }
        return arrayOfCurrentSearchResult.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let searchAddressCell:SearchLocationTableViewCell = tableView.dequeueReusableCell(withIdentifier:TABLEVIEW_CELL_IDENTIFIERS.SearchLocation) as! SearchLocationTableViewCell
        
        searchAddressCell.removeAddressButton.tag = indexPath.row
        
        searchAddressCell.removeAddressButton.addTarget(self, action: #selector(self.removeAddressButtonAction), for: .touchUpInside)
        
        var height1:CGFloat
        var height2:CGFloat
        
        if(addressSearchBar.text?.length == 0)
        {
            if(indexPath.section == 0)
            {
                //For Managed Address Cell
                let addressModel:AddressModel = AddressModel.init(manageAddressDetails: managedAddress[indexPath.row])
                
                if addressModel.tagAddress.length > 0 {
                    
                    searchAddressCell.addressLabel1?.text = "\(addressModel.tagAddress)"
                }
                else{
                    
                    searchAddressCell.addressLabel1?.text = ""
                }
                
                if addressModel.addressLine1.length > 0 {
                    
                    searchAddressCell.addressLabel2?.text = "\(addressModel.addressLine1)"
                    height2 = Helper.measureHeightLabel(searchAddressCell.addressLabel2, width: SCREEN_WIDTH! - 97)
                    
                }
                else
                {
                    searchAddressCell.addressLabel2?.text = ""
                    height2 = 0
                }
                
                
            }
            else//For Previously Selected Address Cell
            {
                let addressModel:AddressModel = AddressModel.init(addressDetails: previouslySelectedAddress[indexPath.row])
                
                searchAddressCell.addressLabel1?.text = addressModel.addressLine1
                searchAddressCell.addressLabel2?.text = addressModel.addressLine2
                
                height2 = Helper.measureHeightLabel(searchAddressCell.addressLabel2, width: SCREEN_WIDTH! - 97)
            }
            
            searchAddressCell.removeAddressButton.isHidden = false
            searchAddressCell.removeAddressButtonWidthConstraint.constant = 30
            
            height1 = Helper.measureHeightLabel(searchAddressCell.addressLabel1, width: SCREEN_WIDTH! - 97)
            
            searchAddressCell.addressLabel1HeightConstraint.constant = height1 + 4
            
            searchAddressCell.addressImageButton.setImage(#imageLiteral(resourceName: "reecent_search_pin"), for: UIControl.State.normal)
            
        }
        else//For Currently Searched Address Cell
        {
            //            let searchResult:GMSAutocompletePrediction = arrayOfCurrentSearchResult[indexPath.row] as! GMSAutocompletePrediction
            //
            //
            //            print(searchResult)
            //            print(searchResult.attributedPrimaryText.string)
            //            print(searchResult.attributedSecondaryText?.string)
            //
            //            searchAddressCell.addressLabel1?.text = searchResult.attributedPrimaryText.string
            //            searchAddressCell.addressLabel2?.text = searchResult.attributedSecondaryText?.string
            
            let searchResult = arrayOfCurrentSearchResult[indexPath.row] as! NSDictionary
            let structureDict = searchResult.value(forKey: "structured_formatting") as! NSDictionary
            searchAddressCell.addressLabel1?.text = structureDict.value(forKey: "main_text") as? String
            searchAddressCell.addressLabel2?.text = structureDict.value(forKey: "secondary_text") as? String
            
            searchAddressCell.removeAddressButton.isHidden = true
            searchAddressCell.removeAddressButtonWidthConstraint.constant = 0
            
            height1 =  Helper.measureHeightLabel(searchAddressCell.addressLabel1, width: SCREEN_WIDTH! - 67)
            
            searchAddressCell.addressLabel1HeightConstraint.constant = height1 + 4
            
            height2 = Helper.measureHeightLabel(searchAddressCell.addressLabel2, width: SCREEN_WIDTH! - 67)
            
            searchAddressCell.addressImageButton.setImage(#imageLiteral(resourceName: "current_search_pin"), for: UIControl.State.normal)
            
        }
        
        
        
        Helper.setShadowFor(searchAddressCell.topView, andWidth:SCREEN_WIDTH!-20, andHeight:height1 + height2 + 30 - 10)
        
        return searchAddressCell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let searchAddressCell:SearchLocationTableViewCell = tableView.dequeueReusableCell(withIdentifier:TABLEVIEW_CELL_IDENTIFIERS.SearchLocation) as! SearchLocationTableViewCell
        
        var height1:CGFloat
        var height2:CGFloat
        
        if(addressSearchBar.text?.length == 0)
        {
            //For Managed Address Cell
            if(indexPath.section == 0)
            {
                //For Managed Address Cell
                let addressModel:AddressModel = AddressModel.init(manageAddressDetails: managedAddress[indexPath.row])
                
                if addressModel.tagAddress.length > 0 {
                    
                    searchAddressCell.addressLabel1?.text = "\(addressModel.tagAddress)"
                }
                else{
                    
                    searchAddressCell.addressLabel1?.text = ""
                }
                
                height1 = Helper.measureHeightLabel(searchAddressCell.addressLabel1, width: self.view.frame.size.width - 97)
                
                if addressModel.addressLine1.length > 0 {
                    
                    searchAddressCell.addressLabel2?.text = "\(addressModel.addressLine1)"
                    
                    height2 = Helper.measureHeightLabel(searchAddressCell.addressLabel2, width: SCREEN_WIDTH! - 97)
                }
                else
                {
                    searchAddressCell.addressLabel2?.text = ""
                    height2 = 0
                }
                
            }
            else//For Previously Selected Address Cell
            {
                let addressModel:AddressModel = AddressModel.init(addressDetails: previouslySelectedAddress[indexPath.row])
                
                searchAddressCell.addressLabel1?.text = addressModel.addressLine1
                searchAddressCell.addressLabel2?.text = addressModel.addressLine2
                
                height1 = Helper.measureHeightLabel(searchAddressCell.addressLabel1, width: SCREEN_WIDTH! - 97)
                height2 = Helper.measureHeightLabel(searchAddressCell.addressLabel2, width: SCREEN_WIDTH! - 97)
            }
            
        }
        else//For Currently Searched Address Cell
        {
            //            let searchResult:GMSAutocompletePrediction = arrayOfCurrentSearchResult[indexPath.row] as! GMSAutocompletePrediction
            //            searchAddressCell.addressLabel1?.text = searchResult.attributedPrimaryText.string
            //            searchAddressCell.addressLabel2?.text = searchResult.attributedSecondaryText?.string
            
            let searchResult = arrayOfCurrentSearchResult[indexPath.row] as! NSDictionary
            searchAddressCell.addressLabel1?.text = searchResult.value(forKey: "main_text") as? String
            searchAddressCell.addressLabel2?.text = searchResult.value(forKey: "secondary_text") as? String
            
            height1 = Helper.measureHeightLabel(searchAddressCell.addressLabel1, width: SCREEN_WIDTH! - 67)
            height2 = Helper.measureHeightLabel(searchAddressCell.addressLabel2, width: SCREEN_WIDTH! - 67)
            
        }
        
        searchAddressCell.layoutIfNeeded()
        
        return height1+height2 + 50
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        let searchResult = arrayOfCurrentSearchResult[indexPath.row] as! NSDictionary
        let description = searchResult.value(forKey: "description") as? String
        if addressSearchBar.text?.length == 0 && indexPath.section == 0 {
            
            let addressModel:AddressModel = AddressModel.init(manageAddressDetails: managedAddress[indexPath.row])
            
            //For Managed Address
            passTheSelectedAddressToDelegate(addressModel: addressModel,des:description ?? "")
        }
        else
        {
            var placeID :String!
            
            if addressSearchBar.text?.length == 0 && indexPath.section == 1 {
                
                //For Previously Selected Address
                let addressModel:AddressModel = AddressModel.init(addressDetails: previouslySelectedAddress[indexPath.row])
                passTheSelectedAddressToDelegate(addressModel: addressModel,des: description ?? "")
            }
            else
            {
                //For New Search Address
                //                let searchResult:GMSAutocompletePrediction = arrayOfCurrentSearchResult[indexPath.row] as! GMSAutocompletePrediction
                //                placeID = searchResult.placeID
                
                let searchResult = arrayOfCurrentSearchResult[indexPath.row] as! NSDictionary
                placeID = searchResult.value(forKey: "place_id") as? String
                self.getPlaceInformation(placeId: placeID,des: description ?? "")
            }
            
            //            addressSearchBar.resignFirstResponder()
            
        }
        
        
    }
    
    func passTheSelectedAddressToDelegate(addressModel:AddressModel,des:String) {
        
        if  delegate != nil {
            print(addressModel.latitude)
            delegate?.searchAddressDelegateMethod(addressModel, descript:des)
            OperationQueue.main.addOperation {
                self.navigationBackButtonAction(self.navigationBackButton)
            }
        }
        
    }
    
    
    @objc func removeAddressButtonAction(removeAddressButton:UIButton) {
        
        let buttonPosition = removeAddressButton.convert(CGPoint.zero, to: tableView)
        let indexPath: IndexPath? = tableView.indexPathForRow(at: buttonPosition)
        
        selectedIndexPathToDeleteAddress = indexPath
        
//        let alertController = UIAlertController(title: ALERTS.RemoveAddress,
//                                                message:ALERTS.RemoveAddressMessage,
//                                                preferredStyle: .alert)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        
//        alertController.addAction(UIAlertAction(title: ALERTS.NO,
//                                                style: .default,
//                                                handler: {(_ action: UIAlertAction) -> Void in
//                                                    
//                                                    newAlertWindow.resignKey()
//                                                    newAlertWindow.removeFromSuperview()
//        }))
        
//        alertController.addAction(UIAlertAction(title: ALERTS.YES,
//                                                style: .default,
//                                                handler: {(_ action: UIAlertAction) -> Void in
//                                                    //Delete Address
//                                                    newAlertWindow.resignKey()
//                                                    newAlertWindow.removeFromSuperview()
//                                                    self.deletePreviousAddress()
//        }))
        
//        newAlertWindow.rootViewController?.present(alertController, animated: true)
//
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        //        cell.contentView.transform = CGAffineTransform(scaleX: 1.4, y:1.4)
        //
        //        UIView.animate(withDuration: 0.8,
        //                       delay: 0,
        //                       usingSpringWithDamping: 0.5,
        //                       initialSpringVelocity: 3,
        //                       options: [],
        //                       animations: {
        //
        //                        cell.contentView.transform = .identity
        //
        //        }, completion: { (completed) in
        //
        //
        //        })
        
        
    }
    
}

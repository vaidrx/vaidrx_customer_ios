//
//  ChangeEmailAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ChangeEmailMobileViewController {
    
    func sendRequestToChangeEmail() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        changeEmailMobileViewModel.changeEmailAPICall { (statCode, errMsg, dataResp) in
           
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.updateEmail)
        }
        
    }
    
    func sendRequestToChangeMobile() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        changeEmailMobileViewModel.countryCode = countryCodeLabel.text!
        
        changeEmailMobileViewModel.changeMobileAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.updateMobile)
          
        }
        
    }
    
    func sendRequestToValidateEmailAddress() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        changeEmailMobileViewModel.validateEmailAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.verifyEmail)
        }
        
    }

    
    func sendRequestToValidatePhoneNumber() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        changeEmailMobileViewModel.countryCode = countryCodeLabel.text!
        changeEmailMobileViewModel.triggerValue = 2
        changeEmailMobileViewModel.validatePhoneNumberAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.verifyMobileNumber)
        }
        
    }
    
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        if HTTPSResponseCodes.ChangeEmailMobile(rawValue: statusCode) != nil {
            
            let responseCodes : HTTPSResponseCodes.ChangeEmailMobile = HTTPSResponseCodes.ChangeEmailMobile(rawValue: statusCode)!
            
            switch responseCodes
            {
                case .TokenExpired:
                    
                    if dataResponse != nil {
                        
                        AppDelegate().defaults.set(GenericUtility.strForObj(object: dataResponse), forKey: USER_DEFAULTS.TOKEN.ACCESS)
                        
                        self.apiTag = requestType.rawValue
                        
                        var progressMessage = PROGRESS_MESSAGE.Loading
                        
                        switch requestType {
                            
                            case .updateEmail:
                                
                                progressMessage = PROGRESS_MESSAGE.ChangingEmail
                            
                            case .updateMobile:
                                
                                progressMessage = PROGRESS_MESSAGE.ChangingPhoneNumber
                                
                            case .verifyEmail:
                                
                                progressMessage = PROGRESS_MESSAGE.ValidatingEmail
                                
                            case .verifyMobileNumber:
                                
                                progressMessage = PROGRESS_MESSAGE.ValidatingPhoneNumber
                            
                            default:
                                
                                break
                        }
                        
                        self.acessClass.getAcessToken(progressMessage: progressMessage)
                        
                    }
                    
                    
                    
                case .EmailMobileAlreadyExist://Email or Mobile Already Exist
                    
                    switch requestType {
                        
                    case  RequestType.verifyEmail:
                        Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                        mobileEmailTF.text = ""
                        changeEmailMobileViewModel.emailText.value = ""
                        mobileEmailTF.becomeFirstResponder()
                        
                        
                    case RequestType.verifyMobileNumber:
                        
                        Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                        phoneNumberTF.text = ""
                        changeEmailMobileViewModel.phoneNumberText.value = ""
                        phoneNumberTF.becomeFirstResponder()
                        
                        
                    default:
                        
                        break
                    }
                    
                    
                case .SuccessResponse:
                    
                    switch requestType
                    {
                        
                        case  RequestType.verifyEmail:
                            sendRequestToChangeEmail()
                            
                            
                        case RequestType.updateEmail:
                            delegate?.recallApi(sucess: true)
                            navigationController?.popViewController(animated: true)
                            break
                            
                        case RequestType.updateMobile:
                            
                            if let dataRes = dataResponse as? [String:Any] {
                                
                                let params:[String:Any] = [
                                    
                                    USER_DEFAULTS.USER.COUNTRY_CODE:countryCodeLabel.text!,
                                    USER_DEFAULTS.USER.MOBILE:phoneNumberTF.text!,
                                    SERVICE_RESPONSE.Sid : GenericUtility.strForObj(object: dataRes[SERVICE_RESPONSE.Sid])
                                ]
                                OperationQueue.main.addOperation {
                                     self.performSegue(withIdentifier: SEgueIdetifiers.changeEMToVerify , sender: params)
                                }
                                
                            }
                            
                            break
                            
                            
                        case RequestType.verifyMobileNumber:
                            
                            sendRequestToChangeMobile()
                            
                        default:
                            
                            break
                    }
                    
                    break
                    
                    
            }
            
        } else {
    
            if errorMessage != nil {
    
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
        }

    
    }

}

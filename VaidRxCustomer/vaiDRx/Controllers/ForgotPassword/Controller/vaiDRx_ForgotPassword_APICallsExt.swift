//
//  vaiDRx_ForgotPassword_APICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 19/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_ForgotPasswordVC {
    
    //MARK: - WebService Call -
    
    func sendRequestToForgotPasswordUsingEmail() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        forgotPasswordViewModel.countryCode = countryCode.text!
        forgotPasswordViewModel.forgotPasswordWithEmail = forgotPasswordWithEmail
        
        
        forgotPasswordViewModel.forgotPasswordAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.forgetPassword)
        }
        
    }
    
    
    //MARK: - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        if HTTPSResponseCodes.ForgotPassword(rawValue: statusCode) != nil {
            
            let responseCodes : HTTPSResponseCodes.ForgotPassword = HTTPSResponseCodes.ForgotPassword(rawValue: statusCode)!
            
            switch responseCodes
            {
            case .EmailMobileNotExist, .WrongEmail, .WrongPhoneNumber, .ToManyRequest:
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                
                emailTF.text = ""
                forgotPasswordViewModel.emailText.value = ""
                phonenumberTF.text = ""
                forgotPasswordViewModel.phoneNumberText.value = ""
                
                if forgotPasswordWithEmail {
                    
                    emailTF.becomeFirstResponder()
                    
                } else {
                    
                    phonenumberTF.becomeFirstResponder()
                }
                
                
            case .SuccessResponse:
                
                switch requestType {
                    
                case RequestType.forgetPassword:
                    
                    if forgotPasswordWithEmail {
                        
                        Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                        backButtonMethod()
                        
                    }else {
                        
                        if let dataRes = dataResponse as? [String:Any] {
                            
                            if let sid = dataRes[SERVICE_RESPONSE.Sid] as? String {
                                
                                DDLogDebug("SID:\(sid)")
                                
                                let params:[String:Any] = [
                                    USER_DEFAULTS.USER.COUNTRY_CODE:countryCode.text!,
                                    USER_DEFAULTS.USER.MOBILE:phonenumberTF.text!,
                                    SERVICE_RESPONSE.Sid :sid
                                ]
                                
                                performSegue(withIdentifier: SEgueIdetifiers.forgetPassToOtp, sender: params)
                            }
                            
                        }
                        
                    }
                    break
                    
                    
                default:
                    break
                }
                
                break
                
            case .WrongPassword:
                break
            }
            
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
        }
        
        
    }
    
}

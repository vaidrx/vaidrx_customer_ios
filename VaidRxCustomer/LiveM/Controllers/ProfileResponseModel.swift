//
//  ProfileResponseModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 23/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

var paymentTypeMethod = 0

class ProfileResponseModel {
    
    var firstName = ""
    var lastName = ""
    var email = ""
    var dateOfBirth = ""
    var countryCode = ""
    var phoneNumber = ""
    var about = ""
    var profilePic = ""
    var preferredPayment = 0
    //    var genres = [Any]()
    var musicGenres = ""
    var insuranceName = ""
    var insuranceId = ""
    var addLine1 = ""
    var addLine2 = ""
    var pharmacyName = ""
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var isPharmacyApproved = false
    
    
    
    
    /// Creating User Profile model from user details
    ///
    /// - Parameter profileDetails: user details
    init(profileDetails: Any) {
        
        if let profileDetail = profileDetails as? [String:Any] {
            
            firstName = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.FirstName])
            lastName = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.LastName])
            email = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.Email])
            dateOfBirth = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.Dob])
            countryCode = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.CountryCode])
            
            phoneNumber = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.PhoneNumber])
            about = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.About])
            profilePic = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.ProfilePic])
            preferredPayment = GenericUtility.intForObj(object: profileDetail[SERVICE_RESPONSE.PaymentType])
            paymentTypeMethod = preferredPayment
            //UserDefaults.standard.setValue(preferredPayment, forKey: "PaymentMethod")
            addLine1 = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.AddLine1])
            addLine2 = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.AddLine2])
            pharmacyName = GenericUtility.strForObj(object: profileDetail[SERVICE_RESPONSE.PharmacyName])
            latitude = profileDetail[SERVICE_RESPONSE.Latitude] as? Double ?? 0.0
            longitude = profileDetail[SERVICE_RESPONSE.Longitude] as? Double ?? 0.0
            isPharmacyApproved = profileDetail["isPharmacyApproved"] as? Bool ?? false
            
            
            let insuranceDetails = GenericUtility.dictionaryForObj(object: profileDetail[SERVICE_RESPONSE.Insurance])
            insuranceName = GenericUtility.strForObj(object: insuranceDetails[SERVICE_RESPONSE.insuranceName])
            insuranceId = GenericUtility.strForObj(object: insuranceDetails[SERVICE_RESPONSE.insuranceId])
            
//            if let paymentType = profileDetail[SERVICE_RESPONSE.PaymentType] as? Int {
//
//                preferredPayment = paymentType
//            }
            
            //            if let musGen = profileDetail[SERVICE_RESPONSE.Genres] as? String {
            //
            //                musicGenres = musGen
            //            }
            //            genres = GenericUtility.arrayForObj(object: profileDetail[SERVICE_RESPONSE.Genres]) as! [[String : Any]]
            
        }
    }
    
    
}

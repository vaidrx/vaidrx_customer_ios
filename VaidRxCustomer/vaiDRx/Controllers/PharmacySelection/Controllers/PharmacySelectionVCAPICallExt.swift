//
//  PharmacySelectionVCAPICallExt.swift
//  vaiDRx
//
//  Created by 3Embed on 08/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
extension PharmacySelectionViewController{
    func getPharmacyData(){
        
        if !NetworkHelper.sharedInstance.networkReachable(){
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
            //Helper.showAlertMessage(title: ALERT_VAIDRX.Error, message: ALERT_VAIDRX.NoInternet)
            return
        }
        if isComingFrom == "Profile"{
            getPharmacyViewModel.latitude = profileResponseModel.latitude
            getPharmacyViewModel.longitude = profileResponseModel.longitude
        }
        else{
            getPharmacyViewModel.latitude = registerViewModel.latitude
            getPharmacyViewModel.longitude = registerViewModel.longitude
        }
        
        getPharmacyViewModel.getPharmacyAPICall { (statusCode, errMsg, response) in
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response,requestType:RequestType.getPharmacy)
        }
        
//        getPharmacyViewModel.getPharmacyAPICall { (statusCode, errMsg, response) in
//            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response)
//        }
    }
    func updatePharmacyData(){
        
//        let alertController = UIAlertController(title: "", message: "Are you sure to edit your pharmacy ?", preferredStyle: .alert)
//        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) -> Void in
//            print("ayes")
        
        
            if !NetworkHelper.sharedInstance.networkReachable(){
                Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
                
                return
            }
            self.getPharmacyViewModel.updatePharmacyAPICall { (statusCode, errMsg, response) in
                self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response,requestType:RequestType.updateProfileData)
            }
        
        
//        }
//        let noAction = UIAlertAction(title: "No", style: .default) { (action) -> Void in
//            self.navigationController?.popViewController(animated: true)
//        }

//        alertController.addAction(yesAction)
//        alertController.addAction(noAction)
//        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResp:Any?,requestType:RequestType){
        switch statusCode {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            switch requestType{
            case .updateProfileData:
                self.navigationController?.popViewController(animated: true)
                break
            case .getPharmacy:
                if dataResp != nil{
                    pharmacyData = PharmacyResponseModel.init(pharmacyResponseDetails:dataResp! )
                    
                    pharmacyItems = pharmacyData.pharmacyItemsModel
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                    
//                    print("pharmacyItems",pharmacyItems)
                    
                    
//                    print("data response",dataResp ?? [:])
                    
                    if pharmacyItems != nil{
                        showMarkersInMapView()
                    }
                    
                }
                break
            default:
                break
            }
            
            
            
        default:
            break
        }
    }
}

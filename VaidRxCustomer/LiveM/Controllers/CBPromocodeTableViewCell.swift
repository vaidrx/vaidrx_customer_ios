//
//  CBPromocodeTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class CBPromocodeTableViewCell:UITableViewCell {
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var promocodeTextField: UITextField!
    
    @IBOutlet var applyButton: UIButton!
    
    
    func showPromocodeDetails(CBModel:ConfirmBookingModel) {
        
        if CBModel.promoCodeText.length == 0 {
            
            promocodeTextField.text = ""
            applyButton.setTitle("Apply", for: UIControl.State.normal)
            applyButton.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
            applyButton.isUserInteractionEnabled = false
            
        } else {
            
            promocodeTextField.text = CBModel.promoCodeText
            applyButton.setTitle("Clear", for: UIControl.State.normal)
            applyButton.setTitleColor(UIColor.red, for: UIControl.State.normal)
            applyButton.isUserInteractionEnabled = false
        }
        
    }

}

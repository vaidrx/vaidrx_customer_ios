//
//  vaiDRx_HomeVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 13/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import CoreLocation
//import GoogleMaps

class vaiDRx_HomeVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    
    var CBModel:ConfirmBookingModel = ConfirmBookingModel.sharedInstance()
    let locationObj = LocationManager.sharedInstance()
    let confirmBookingViewModel = ConfirmBookingViewModel()
    let appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
    var vaidType = 0 //1-Now//2-Today
    override func viewDidLoad() {
        super.viewDidLoad()
        locationObj.start()
        appointmentLocationModel.pickupLatitude = locationObj.latitute
        appointmentLocationModel.pickupLongitude = locationObj.longitude
//        appointmentLocationModel.pickupAddress = locationObj.address
        menuButton.layer.borderWidth = 1
        menuButton.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x67AFF7).cgColor
//        tableView.estimatedRowHeight = 250
//        tableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // Add a background view to the table view
        let backgroundImage = #imageLiteral(resourceName: "default")
        let imageView = UIImageView(image: backgroundImage)
        self.tableView.backgroundView = imageView
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = UIColor.white
//        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = true
        
        // center and scale background image
        imageView.contentMode = .scaleAspectFit 
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        super.viewWillDisappear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func menuAction(_ sender: UIButton) {
   
        let leftMenu = slideMenuController()
        leftMenu?.openLeft()
    }
    
}

//extension vaiDRx_HomeVC: LocationManagerDelegate {
//
//    func didFailToUpdateLocation() {
//        appointmentLocationModel.pickupLatitude = locationObj.latitute
//        appointmentLocationModel.pickupLongitude = locationObj.longitude
//        appointmentLocationModel.pickupAddress = locationObj.address
//    }
//
//
//    /// Get Location Details From Lat & Long
//    ///
//    /// - Parameter coordinate: current map position lat & Long
//    func getLocationDetails(_ coordinate: CLLocationCoordinate2D) {
//
//        var positionChanged = false
//
//        if appointmentLocationModel.pickupLatitude != coordinate.latitude || appointmentLocationModel.pickupLongitude != coordinate.longitude {
//
//            positionChanged = true
//        }
//
//        appointmentLocationModel.pickupLatitude = coordinate.latitude
//        appointmentLocationModel.pickupLongitude =  coordinate.longitude
//
////        if positionChanged {
////
////            if appointmentLocationModel.bookingType == BookingType.Schedule {
////
////                musiciansListManager.publish(isIinitial: false)
////
////            } else {
////
////                musiciansListManager.publish(isIinitial: true)
////            }
////        }
//
//
//        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: { (gmsAddressResponse, error) in
//
//            if let addressDetails = gmsAddressResponse?.firstResult() {
//
//                // Address Details
//                DDLogVerbose("Add New Address Address Details:\(addressDetails)")
//
//
//                // Current Address
//                if let currentAddress = addressDetails.lines {
//
//
//                    DDLogVerbose("Current Address: \(currentAddress)")
//
//                    var address = (currentAddress.joined(separator: ", "))
//
//                    if address.hasPrefix(", ") {
//
//                        address = address.substring(2)
//                    }
//
//                    if address.hasSuffix(", ") {
//
//                        let endIndex = address.index(address.endIndex, offsetBy: -2)
//
//                        address = address.substring(to: endIndex)
//                    }
//
//                    self.appointmentLocationModel.pickupAddress = address
//
//                }
//
//            } else {
//
//                DDLogError("Home Class Address Fetch Error: \(String(describing: error?.localizedDescription))")
//            }
//
//        })
//
//    }
//}


//
//  EmergencyMessageView.swift
//  vaiDRx
//
//  Created by Rahul Sharma on 04/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

protocol EmergencyMessageDelegate {
    func okPressed()
}
class EmergencyMessageView: UIView {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    var delegate: EmergencyMessageDelegate? = nil
    private static var share: EmergencyMessageView? = nil
    
    static var sharedInstance: EmergencyMessageView {
        
        if share == nil {
            
            share = Bundle(for: self).loadNibNamed("911MessageView",
                                                   owner: nil,
                                                   options: nil)?.first as? EmergencyMessageView
            
            share?.frame = (WINDOW_DELEGATE??.frame)!
 
            
        }

        
        return share!
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        topView.layer.cornerRadius = 10
        //custom
    }
    
    @IBAction func okAction(_ sender: UIButton) {
        
        self.delegate?.okPressed()
        topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        
        UIView.animate(withDuration: 0.6,
                       delay: 0.2,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                        
        }) { (finished) in
            
            
            self.removeFromSuperview()
            EmergencyMessageView.share = nil
            
        }

    }
    
}

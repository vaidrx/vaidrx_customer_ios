//
//  vaiDRx_SocialLoginTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class vaiDRx_SocialLoginTableViewCell: UITableViewCell {
    
    //Label
    @IBOutlet weak var socialLoginTitle: UILabel!
    
    //ImageView
    @IBOutlet weak var socialLoginIcon: UIImageView!
    
    @IBOutlet weak var bottomView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  vaiDRxLandingButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRxLandingVCViewController {
    
    @IBAction func pickCountryAction(_ sender: UIButton) {
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
        mainViewBottomConstraint.constant = 0
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
        nextButton.isHidden     = true
        nextButton.isEnabled    = false
        phoneView.isHidden      = true
        titleLabel.text         = "Login/Register to vaiDRx"
        logoImage.isHidden      = false
        phoneNumberTextFeild.resignFirstResponder()
        mainView.backgroundColor = UIColor.white
        phoneView.backgroundColor = UIColor.white
        phoneNumberTextFeild.textColor = APP_COLOR
        countryCodeLabel.textColor = APP_COLOR
        titleLabel.textColor = APP_COLOR
        enterYourNameLabel.textColor = APP_COLOR
        if phoneNumberTextFeild.text?.count != 0 {
            enterYourNameLabel.isHidden = true
        }
        else{
            enterYourNameLabel.isHidden = false
        }
        self.view.layoutIfNeeded()
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        checkFortheField()

    }
    
    
    @IBAction func swipeGestActionUP(_ sender: UISwipeGestureRecognizer) {
        
        backButton.isHidden = false
        logoButton.isHidden = false
        
        switch mainViewBottomConstraint.constant {
            
        case 0:
            phoneNumberTextFeild.becomeFirstResponder()
            mainViewBottomConstraint.constant = (self.view.frame.size.height - 244)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            nextButton.isHidden     = false
            nextButton.isEnabled    = true
            phoneView.isHidden      = false
            titleLabel.text         = "Enter your mobile number"
            logoImage.isHidden      = true
            self.view.layoutIfNeeded()
        default:
            break
        }
        
        UIView.animate(withDuration: 0.6) {
            self.view.layoutIfNeeded()
            self.mainView.backgroundColor = UIColor.clear
            self.phoneView.backgroundColor = UIColor.white
            self.phoneNumberTextFeild.textColor = UIColor.white
            self.countryCodeLabel.textColor = .white
            self.titleLabel.textColor = .white
            self.enterYourNameLabel.textColor = .white
        }
    }
    
    
    @IBAction func swipeGestActionDOWN(_ sender: UISwipeGestureRecognizer) {
        
        backButton.isHidden = true
        logoButton.isHidden = true
        switch mainViewBottomConstraint.constant {
        case self.view.frame.size.height - 244:
            mainViewBottomConstraint.constant = 0
//            self.navigationController?.setNavigationBarHidden(true, animated: true)
            nextButton.isHidden     = true
            nextButton.isEnabled    = false
            phoneView.isHidden      = true
            titleLabel.text         = "Login/Register to vaiDRx"
            logoImage.isHidden      = false
            phoneNumberTextFeild.resignFirstResponder()
            self.view.layoutIfNeeded()
        default:
            break
        }
        
        UIView.animate(withDuration: 0.6) {
            self.view.layoutIfNeeded()
            self.mainView.backgroundColor = UIColor.white
            self.phoneView.backgroundColor = UIColor.white
            self.phoneNumberTextFeild.textColor = APP_COLOR //UIColor.black
            self.countryCodeLabel.textColor = APP_COLOR //.black
            self.titleLabel.textColor = APP_COLOR
            self.enterYourNameLabel.textColor = APP_COLOR
        }
        
    
    }
    
    
    @IBAction func socialLoginAction(_ sender: UIButton) {
        
        performSegue(withIdentifier: "toSocialSIgnIn", sender: self)
    }

}

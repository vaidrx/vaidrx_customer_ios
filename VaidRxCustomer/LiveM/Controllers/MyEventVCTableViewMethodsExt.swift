//
//  MyEventVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension MyEventViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch tableView {
            
            case self.pendingTableView:
                
                let pendingBookingVC:PendingBookingViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.pendingBookingVC) as! PendingBookingViewController
                
                pendingBookingVC.bookingEventModel = arrayOfPendingBookings[indexPath.row]
                
                TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
                
                self.navigationController!.pushViewController(pendingBookingVC, animated: false)
                
                break
                
            case self.upcomingTableView:
                
                let bookingFlowVC:BookingFlowViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.bookingFlowVC) as! BookingFlowViewController
                
                let bookingModel = arrayOfUpcomingBookings[indexPath.row]
                
                bookingFlowVC.bookingId = bookingModel.bookingId
                bookingFlowVC.bookingStatus = bookingModel.bookingStatus
                
                
                TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
                
                self.navigationController!.pushViewController(bookingFlowVC, animated: false)
                
                break
                
                
            case self.pastTableView:
                
                let bookingModel = arrayOfPastBookings[indexPath.row]
                
                if bookingModel.bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                    
                    let pastBookingVC:PastBookingViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.pastBookingVC) as! PastBookingViewController
                    
                    
                    
                    pastBookingVC.bookingDetailModel = bookingModel
                    
                    self.navigationController!.pushViewController(pastBookingVC, animated: true)
                    
                } else {
                    
                    let declinedBookingVC:DeclinedBookingViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.declinedBookingVC) as! DeclinedBookingViewController
                    
                    declinedBookingVC.bookingDetailModel = bookingModel
                    
                    self.navigationController?.pushViewController(declinedBookingVC, animated: true)

                }
                
                break
                
                
            default:
                
                break
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
    
}


extension MyEventViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch tableView {
            
        case self.pendingTableView:
            
            if arrayOfPendingBookings.count > 0 {
                
                pendingMessageView.isHidden = true
                return 1
            }
            
            pendingMessageView.isHidden = false
            return 0
            
            
        case self.upcomingTableView:
            
            if arrayOfUpcomingBookings.count > 0 {
                
                upcomingMessageView.isHidden = true
                return 1
            }
            
            upcomingMessageView.isHidden = false
            return 0
            
            
        case self.pastTableView:
            
            if arrayOfPastBookings.count > 0 {
                
                pastMessageView.isHidden = true
                return 1
            }
            
            pastMessageView.isHidden = false
            return 0
            
            
        default:
            
            return 0
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
            
        case self.pendingTableView:
            
            return arrayOfPendingBookings.count
            
        case self.upcomingTableView:
            
            return arrayOfUpcomingBookings.count
            
        case self.pastTableView:
            
            return arrayOfPastBookings.count
            
        default:
            
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
            
            case self.pendingTableView:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyEventPendingCell", for: indexPath) as! MyEventPendingTVCell
                cell.showBookingDetails(bookingDetail: arrayOfPendingBookings[indexPath.row])
                
                return cell
                
                
            default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyEventPastCell", for: indexPath) as! MyEventPastTVCell
                
                if tableView == self.upcomingTableView {
                    
                    cell.showBookingDetails(bookingDetail: arrayOfUpcomingBookings[indexPath.row] )
                    
                } else {
                    
                    cell.showBookingDetails(bookingDetail: arrayOfPastBookings[indexPath.row])
                    cell.providerDistanceLabel.text = ""
                }
                
                
                return cell
                
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let pendingBookingCell = cell as? MyEventPendingTVCell {
            
            pendingBookingCell.timer.invalidate()
        }
        
    }
    
}

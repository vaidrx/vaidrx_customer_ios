//
//  vaiDRxVerifyOTPCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_VerifyOTPVC{
    
    func clearTextField(){
        code1.text = ""
        code2.text = ""
        code3.text = ""
        code4.text = ""
        
        otpImage1.isHidden = false
        otpImage2.isHidden = false
        otpImage3.isHidden = false
        otpImage4.isHidden = false
    }
    
    func initiallSetup() {
        
        otpImage1.transform = CGAffineTransform(scaleX: 0, y: 0)
        otpImage2.transform = CGAffineTransform(scaleX: 0, y: 0)
        otpImage3.transform = CGAffineTransform(scaleX: 0, y: 0)
        otpImage4.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
    
    
    func initiallAnimation() {
        
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
            self.otpImage1.transform = .identity
        }) { (true) in
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                self.otpImage2.transform = .identity
            }) { (true) in
                UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                    self.otpImage3.transform = .identity
                }) { (true) in
                    UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                        self.otpImage4.transform = .identity
                    }) { (true) in
                        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                            //                            self.verifyBackView.transform = .identity
                            self.code1.becomeFirstResponder()
                        })
                    }
                }
            }
        }
    }
    //MARK: - WebService Call -
    func resendOTPToMobileNumber() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        verifyPhoneViewModel.userId = userID[SERVICE_RESPONSE.Sid] as! String
        
        if isComingFrom == "signUp" {
            
            verifyPhoneViewModel.triggerValue = 1
            
        }else if isComingFrom == "ChangeEmailPhone" {
            
            verifyPhoneViewModel.triggerValue = 3
            
        }else {
            
            verifyPhoneViewModel.triggerValue = 2
        }
        
        verifyPhoneViewModel.resendOTPAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.ResendOTP)
        }
        
    }
    func signUpVerifyOTP(code:String) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        verifyPhoneViewModel.codeText = code
        verifyPhoneViewModel.userId = userID[SERVICE_RESPONSE.Sid] as! String
        
        verifyPhoneViewModel.registerVerifyOTPAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.verifyMobileNumber)
        }
        
    }
    
    func forgotPasswordVerifyOTP(code:String) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        verifyPhoneViewModel.codeText = code
        verifyPhoneViewModel.userId = userID[SERVICE_RESPONSE.Sid] as! String
        
        if isComingFrom == "signIn" {
            
            verifyPhoneViewModel.triggerValue = 2
        } else if isComingFrom == "SocialSignUp" {
            
             verifyPhoneViewModel.triggerValue = 3
        } else{
            
            verifyPhoneViewModel.triggerValue = 1
        }
        
        
        verifyPhoneViewModel.forgotPasswordVerifyOTPAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.forgetPassword)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            
            
            
            switch requestType {
            case .ResendOTP:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: errorMessage!, message: "")
                }
                DispatchQueue.main.async {
                    self.addTimer()
                }
                
                break
            case .forgetPassword:
                if (isComingFrom == "signIn" || isComingFrom == "SocialSignUp"){
                    
                    let params = [ SERVICE_RESPONSE.Sid : GenericUtility.strForObj(object: userID[SERVICE_RESPONSE.Sid])]
                    
                    performSegue(withIdentifier: "fpCreatePassword", sender: params)
                    
                } else {
                    
                    performSegue(withIdentifier: "toEmailAddress", sender: self)
                    //Verify OTP Success Response
                }
                break
            case .verifyMobileNumber:
                if (isComingFrom == "signIn" || isComingFrom == "SocialSignUp"){
                    
                    let params = [ SERVICE_RESPONSE.Sid : GenericUtility.strForObj(object: userID[SERVICE_RESPONSE.Sid])]
                    
                    performSegue(withIdentifier: "fpCreatePassword", sender: params)
                    
                } else {
                      let vc = storyboard?.instantiateViewController(withIdentifier: "vaiDRx_VerifyEmailVC") as! vaiDRx_VerifyEmailVC
                    vc.registerViewModel = self.registerViewModel
                    self.navigationController?.pushViewController(vc, animated: false)
//                    performSegue(withIdentifier: "toEmailAddress", sender: self)
                    //Verify OTP Success Response
                }
                break
            default:
                break
            }
            
            
            
            break
            
        default:
            
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
            }
            code4.becomeFirstResponder()
            break
            
        }
        
    }
    
    
//    //MARK - Web Service Response -
//    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
//    {
//        switch statusCode
//        {
//        case HTTPSResponseCodes.SuccessResponse.rawValue:
//
//
//            if (isComingFrom == "signIn" || isComingFrom == "SocialSignUp"){
//
//                let params = [ SERVICE_RESPONSE.Sid : GenericUtility.strForObj(object: userID[SERVICE_RESPONSE.Sid])]
//
//                performSegue(withIdentifier: "fpCreatePassword", sender: params)
//
//            } else {
//
//                performSegue(withIdentifier: "toEmailAddress", sender: self)
//                //Verify OTP Success Response
//            }
//            break
//
//        default:
//
//            if errorMessage != nil {
//
//                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
//            }
//            code4.becomeFirstResponder()
//            break
//
//        }
//
//    }
    
    // MARK: - Segue Method -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toEmailAddress" {
            
            if let dstVC = segue.destination as? vaiDRx_VerifyEmailVC{
                
                dstVC.registerViewModel = self.registerViewModel
            }
            
        } else if segue.identifier == "fpCreatePassword" {
            
            if let dstVC = segue.destination as? vaiDRx_CreatePasswordVC{
                
                dstVC.userID = sender as! [String : Any]
                if isComingFrom == "signIn" {
                   dstVC.isComingFrom = "signIn"
                    
                } else if  isComingFrom == "SocialSignUp" {
                    
                    dstVC.registerViewModel = self.registerViewModel
                    dstVC.isComingFrom = "SocialSignUp"
                }
            }
        }
    }

    
    
    /// add Timer
    func addTimer() {
        
        countTimer = 120
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(vaiDRx_VerifyOTPVC.update), userInfo: nil, repeats: true)
    }
    
    /// update Timer
    @objc func update() {
        
        if(countTimer > 0) {
            countTimer -= 1
            let min:Int = countTimer / 60
            let sec:Int = countTimer % 60

            resendCodeButton.setTitle("Resend Code in \(min) : \(sec)", for: .normal)
            resendCodeButton.isEnabled = false
            resendCodeButton.isUserInteractionEnabled = false
            resendCodeButton.setTitleColor(UIColor.white, for: .normal)
        }else {
            resendCodeButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) , for: .normal)
            resendCodeButton.isEnabled = true
            resendCodeButton.isUserInteractionEnabled = true
        }
    }
    
    func verifyCodeAction() {
        
        if (code1.text?.length)! > 0 && (code2.text?.length)! > 0 && (code3.text?.length)! > 0 && (code4.text?.length)! > 0 {
            
            let verificationCode = code1.text! + code2.text! + code3.text! + code4.text!
            
            
            signUpVerifyOTP(code: verificationCode)
            
        } else {
            
            Helper.showAlert(head: ALERTS.Message, message:ALERTS.OPTMissing)
            
        }
        
    }

    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                    self.resendCodeButtonBottomConstraint.constant = (keyboardSize?.height)!
                    self.view.layoutIfNeeded()
        })
    }
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.4) {
            
            self.resendCodeButtonBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
        
    }
}

//
//  HomeListViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 05/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class HomeListViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    // MARK: - Outlets -
    // View
    @IBOutlet weak var buttonBottomView: UIViewCustom!
    
    // Table View
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var addressLabel: UILabel!
    
    // Button
    @IBOutlet weak var ProfileImageButton: UIButton!
    @IBOutlet var profileImageView: UIImageView!
    
   
    @IBOutlet weak var mainButtonHeight: NSLayoutConstraint!
    
    
    //Schedule DatePicker
    
    @IBOutlet var datePickerBackgroundView: UIView!
    
    @IBOutlet var eventStartBackView: UIView!
    
    @IBOutlet var datepickerCancelButton: UIButtonCustom!
    
    @IBOutlet var datepickerSaveButton: UIButtonCustom!
    
    @IBOutlet var datePickerBackBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var eventStartCollectionView: UICollectionView!
    
    @IBOutlet var datePickerView: UIDatePicker!

    
    //Later booking View
    
    @IBOutlet var selectedBookLaterBackView: UIView!
    
    @IBOutlet var selectedBookLaterDateLabel: UILabel!
    
    @IBOutlet var selectedBookLaterButton: UIButton!
    
    @IBOutlet var selectedBookLaterCloseButton: UIButton!
    

    @IBOutlet var scrollBackView: UIView!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var scrollContentView: UIView!
    
    @IBOutlet var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var messageView: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    
    // MARK: - Variable Decleration -
    let catransitionAnimationClass = CatransitionAnimationClass()
    var selectedDirection: UITableView.Direction!
    
    let locationObj = LocationManager.sharedInstance()
    
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    
    var pickupLatitude = 0.0
    var pickupLongitude = 0.0
    
    var scheduleDate:Date!
    var selectedDate:Date!
    
    var selectedEventStartTag = 0

    
    var arrayOfProvidersModel:[MusicianDetailsModel] = []
    var arrayOfMusicians:[Any] = []
    
    let mqttModel = MQTT.sharedInstance()
    let musiciansListManager = MusiciansListManager.sharedInstance()
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    var selectedVideoTag:Int!

    var isAddressChanged:Bool = false
    
    let appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
    
    var isVCisDispappeared = false
        
    // Shared instance object for gettting the singleton object
    static var obj:HomeListViewController? = nil
    
    class func sharedInstance() -> HomeListViewController {
        
        return obj!
    }

    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addressLabel.text = appointmentLocationModel.pickupAddress
        
        HomeListViewController.obj = self
        initiallSetup()
        
        if scheduleDate == nil {
            
            scheduleDate = TimeFormats.roundDate(Date(timeIntervalSinceNow: 3600 * 2))
            datePickerView.minimumDate = scheduleDate
            
        }
        
        showBookLaterDetails()
        
        pickupLatitude = appointmentLocationModel.pickupLatitude
        pickupLongitude = appointmentLocationModel.pickupLongitude
        
        //Update musician list from manager
        self.perform(#selector(self.getUpdatedMusiciansListFromMQTTManager), with: nil, afterDelay: 1)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        Helper.statusBarView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        showNavigation()
        
        showProfileImageView()
        
        acessClass.acessDelegate = self
        
        UIView.animate(withDuration: 0.8, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: [], animations: {
            self.buttonBottomView.transform = .identity
        }) { (success) in
            
        }
        
        isVCisDispappeared = false
        
        initializeMQTTMethods(initial: false)
        
        getUpdatedMusiciansListFromMQTTManager()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        updateScrollViewConstraint()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        uninitializeMQTTMethods()
        pauseAllYoutubeVideo()
        acessClass.acessDelegate = nil
        isVCisDispappeared = true
    }
    
    func showNavigation() {
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.view?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    @objc func getUpdatedMusiciansListFromMQTTManager() {
        
        musiciansListManager.recievedListOfMusiciansFromMQTT(listOfNewMusicians: musiciansListManager.arrayOfMusicians, listOfCurrentMusicians: arrayOfMusicians)
    }
    
}

extension HomeListViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getAllMusicians.rawValue:
            
            initializeMQTTMethods(initial: false)
            
            break
            
        default:
            break
        }
    }
    
}



extension HomeListViewController:LocationManagerDelegate,SearchAddressDelegate {
    
    func didUpdateLocation(location: LocationManager) {
        
        if currentLatitude == 0.0 {
            
            currentLatitude = location.latitute
            currentLongitude = location.longitude
            
            self.addressLabel.text = location.address
            
            appointmentLocationModel.pickupAddress = self.addressLabel.text!

        }
        
        
    }
    
    func didFailToUpdateLocation() {
        
    }
    
    func searchAddressDelegateMethod(_ addressModel:AddressModel,descript:String) {
        
        if appointmentLocationModel.pickupLatitude != Double(addressModel.latitude) {
            
            isAddressChanged = true
        }
        
        appointmentLocationModel.pickupAddress = GenericUtility.strForObj(object:addressModel.fullAddress)
        
        self.addressLabel.text = appointmentLocationModel.pickupAddress

        
        appointmentLocationModel.pickupLatitude = Double(addressModel.latitude)
        appointmentLocationModel.pickupLongitude = Double(addressModel.longitude)
        
    }
    
    func searchAddressCurrentLocationButtonClicked() {
        
        if appointmentLocationModel.pickupLatitude != locationObj.latitute {
            
            isAddressChanged = true
        }
        
        appointmentLocationModel.pickupAddress = locationObj.address
        
        self.addressLabel.text = appointmentLocationModel.pickupAddress

        
        appointmentLocationModel.pickupLatitude = locationObj.latitute
        appointmentLocationModel.pickupLongitude = locationObj.longitude
    }
    
}

extension HomeListViewController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let gigTimeCell:MDGigTimeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "gigTimeCell", for: indexPath) as! MDGigTimeCollectionViewCell
        
        gigTimeCell.timeLabel.text = "\(30*(indexPath.row+1)) MIN."
        
        
        if selectedEventStartTag == indexPath.row {
            
            gigTimeCell.timeLabel.textColor = UIColor.red
            gigTimeCell.selectedDivider.isHidden = false
            
            gigTimeCell.timeLabel.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            
            UIView.animate(withDuration: 0.8,
                           delay: 0.2,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 5,
                           options: [],
                           animations: {
                            
                            gigTimeCell.timeLabel.transform = .identity
                            
            }) { (success) in
                
                
            }
            
            
        } else {
            
            gigTimeCell.timeLabel.textColor = UIColor.black
            gigTimeCell.selectedDivider.isHidden = true
            
        }
        
        return gigTimeCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sizeOfEachCell:CGSize = CGSize.init(width: (collectionView.bounds.size.width - 30)/3.5, height: 35)
        
        return sizeOfEachCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedEventStartTag = indexPath.row
        appointmentLocationModel.selectedEventStartTag = selectedEventStartTag
        self.eventStartCollectionView.reloadData()
        
    }
    
}

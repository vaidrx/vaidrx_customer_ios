//
//  DeclinedBookingVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension DeclinedBookingViewController {
    
    func showBookingDetails() {
        
        eventIdLabel.text = "Event id : \(bookingDetailModel.bookingId)"
        
        let startDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.bookingStartTime))
        
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "d MMM yyyy"
        eventDateLabel.text = dateFormat.string(from: startDate)
        
        
        if bookingDetailModel.providerImageURL.length > 0 {
            
            activityIndicator.startAnimating()
            
            musicianImageView.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
                                          placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        musicianNameLabel.text = bookingDetailModel.providerName
        
        evenNameLabel.text = GenericUtility.strForObj(object:bookingDetailModel.eventDict["name"])
        
        eventLocationLabel.text = bookingDetailModel.address1
        
        gigNameLabel.text = String(format:"%@ %@", GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["name"]), GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["unit"]))
        
        gigValueLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol ,Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))!)
        
        
        let totalValue =  0.0//(Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))! + bookingDetailModel.serviceFee) - bookingDetailModel.discountValue
        
        totalValueLabel.text = bookingDetailModel.currencySymbol + String(format:" %.2f",totalValue)
        
        eventStatusLabel.text = bookingDetailModel.bookingStatusMessage
        topStatusLabel.text = bookingDetailModel.bookingStatusMessage

        if bookingDetailModel.bookingStatusMessage.length > 0 {
            
            topStatusView.isHidden = false
        } else {
            
            topStatusView.isHidden = true
        }
        
        hideCancellationFeeDetails()
        
        switch bookingDetailModel.bookingStatus {
            
            case Booking_Status.Accepted.rawValue,Booking_Status.Ontheway.rawValue,Booking_Status.Arrived.rawValue,Booking_Status.Started.rawValue,Booking_Status.Completed.rawValue:
                
                topStatusView.backgroundColor = BOOKING_COLOR_CODE.UPCOMING
                eventStatusLabel.textColor = BOOKING_COLOR_CODE.UPCOMING
                break
                
            case Booking_Status.Raiseinvoice.rawValue:
                
                topStatusView.backgroundColor = BOOKING_COLOR_CODE.COMPLETED
                eventStatusLabel.textColor = BOOKING_COLOR_CODE.COMPLETED
                break
                
            case Booking_Status.IgnoreOrExpire.rawValue:
                
                topStatusView.backgroundColor = BOOKING_COLOR_CODE.EXPIRED
                eventStatusLabel.textColor = BOOKING_COLOR_CODE.EXPIRED
                break
                
            case Booking_Status.Declined.rawValue:
                
                topStatusView.backgroundColor = BOOKING_COLOR_CODE.DECLINE
                eventStatusLabel.textColor = BOOKING_COLOR_CODE.DECLINE
                break
                
            case Booking_Status.CancelByCustomer.rawValue:
                
                topStatusView.backgroundColor = BOOKING_COLOR_CODE.CANCELLED
                eventStatusLabel.textColor = BOOKING_COLOR_CODE.CANCELLED
                showCancellationFeeDetails()
                break
            
            case Booking_Status.CancelByProvider.rawValue:
            
                topStatusView.backgroundColor = BOOKING_COLOR_CODE.CANCELLED
                eventStatusLabel.textColor = BOOKING_COLOR_CODE.CANCELLED

                break

            
            default:
                
                topStatusView.backgroundColor = BOOKING_COLOR_CODE.UPCOMING
                eventStatusLabel.textColor = BOOKING_COLOR_CODE.UPCOMING
                break
        }
       
        updateScrollContent()
    }
    
    func showCancellationFeeDetails() {
        
        self.cancellationFeeLabelHeightConstraint.constant = 20
        self.cancellationFeeLabelValueHeightConstraint.constant = 20
        self.detailsBackViewHeightConstraint.constant = 85
        
        discountValueLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol, bookingDetailModel.cancellationFee)
        
        totalValueLabel.text = discountValueLabel.text
    }
    
    func hideCancellationFeeDetails() {
        
        self.cancellationFeeLabelHeightConstraint.constant = 0
        self.cancellationFeeLabelValueHeightConstraint.constant = 0
        self.detailsBackViewHeightConstraint.constant = 65
        discountValueLabel.text = ""
    }
    
    func updateScrollContent() {
        
        if bookingDetailModel.cancellationReason.length > 0 {
            
            self.cancellationReasonbackView.isHidden = false
            self.cancellationReasonLabel.text = bookingDetailModel.cancellationReason
            self.updateScrollContentWithCancellationReason()
            
        } else {
            
            self.cancellationReasonbackView.isHidden = true
            self.cancellationReasonLabel.text = ""
            self.updateScrollContentWithOutCancellationReason()

        }
        
    }

    
}

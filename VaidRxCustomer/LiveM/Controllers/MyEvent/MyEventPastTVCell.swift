//
//  MyEventPastTVCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 18/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class MyEventPastTVCell: UITableViewCell {
    
    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var providerNameLabel: UILabel!
    @IBOutlet weak var providerDistanceLabel: UILabel!
    @IBOutlet weak var bookingTimeLabel: UILabel!
    @IBOutlet weak var bookingDateLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func showBookingDetails(bookingDetail:BookingDetailsModel) {
        
        
        if bookingDetail.providerImageURL.length > 0 {
            
            providerImage.kf.setImage(with: URL(string: bookingDetail.providerImageURL),
                                      placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                      options: [.transition(ImageTransition.fade(1))],
                                      progressBlock: { receivedSize, totalSize in
            },
                                      completionHandler: nil)
            
        } else {
            
            providerImage.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        
        providerNameLabel.text = bookingDetail.providerName.capitalized
        
        providerDistanceLabel.text = Helper.getDistanceDependingMileageMetricFromServer(distance: bookingDetail.distance, mileageMatric: bookingDetail.mileageMatric)//String(format:"%.2f miles away",bookingDetail.distance)
        
        
        let startDate = Date(timeIntervalSince1970: TimeInterval(bookingDetail.bookingStartTime))
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "d MMM yyyy"
        
        bookingDateLabel.text = dateFormat.string(from: startDate)
        
        dateFormat.dateFormat = "hh:mm a"
        bookingTimeLabel.text = dateFormat.string(from: startDate)
        
        statusLabel.text = bookingDetail.bookingStatusMessage
        
        
        switch bookingDetail.bookingStatus {
            
            case Booking_Status.Accepted.rawValue,Booking_Status.Ontheway.rawValue,Booking_Status.Arrived.rawValue,Booking_Status.Started.rawValue,Booking_Status.Completed.rawValue:
                
                statusLabel.textColor = BOOKING_COLOR_CODE.UPCOMING
                break
                
            case Booking_Status.Raiseinvoice.rawValue:
                
                statusLabel.textColor = BOOKING_COLOR_CODE.COMPLETED
                break
                
            case Booking_Status.IgnoreOrExpire.rawValue:
                
                statusLabel.textColor = BOOKING_COLOR_CODE.EXPIRED
                break
                
            case Booking_Status.Declined.rawValue:
                
                statusLabel.textColor = BOOKING_COLOR_CODE.DECLINE
                break
                
            case Booking_Status.CancelByCustomer.rawValue,Booking_Status.CancelByProvider.rawValue:
                
                statusLabel.textColor = BOOKING_COLOR_CODE.CANCELLED
                break
                
            default:
                
                statusLabel.textColor = BOOKING_COLOR_CODE.UPCOMING
                break
        }
        
    }
    
}

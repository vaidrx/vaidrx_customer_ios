//
//  ReviewsModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Review details model
class ReviewsModel {
    
    var userPicURL = ""
    var userName = ""
    var comment = ""
    var reviewAt = 0
    var ratings = 0.0
    
    init(reviewDetails:Any) {//Review screen review details model
        
        if let review = reviewDetails as? [String:Any] {
            
            userPicURL = GenericUtility.strForObj(object: review["profilePic"])
            userName = GenericUtility.strForObj(object: review["reviewBy"])
            
            comment = GenericUtility.strForObj(object: review["review"])
            reviewAt = Int(GenericUtility.strForObj(object: review["reviewAt"]))!
//            ratings =  Double(GenericUtility.strForObj(object: review["rating"]))!
            
            if let rat = review["reviewAt"] as? String {
                
                if !rat.isEmpty {
                    
                    ratings =  Double(GenericUtility.strForObj(object: review["rating"]))!
                }
                
            } else if let rat = review["reviewAt"] as? Double {
                
                ratings =  rat
            }
        }
        
    }
    
    init(musicianReviewDetails:Any) {//Musician Details screen review details model
        
        if let review = musicianReviewDetails as? [String:Any] {
            
            userPicURL = GenericUtility.strForObj(object: review["profilePic"])
            
            if review["firstName"] != nil {
                
                userName = GenericUtility.strForObj(object: review["firstName"]) + " " + GenericUtility.strForObj(object: review["lastName"])
            } else {
                
                userName = GenericUtility.strForObj(object: review["reviewBy"])
            }
            
            comment = GenericUtility.strForObj(object: review["review"])
            reviewAt = GenericUtility.intForObj(object: review["reviewAt"])
            
            if let rat = review["reviewAt"] as? String {
                
                if !rat.isEmpty {
                    
                    ratings =  Double(GenericUtility.strForObj(object: review["rating"]))!
                }
                
            } else if let rat = review["reviewAt"] as? Double {
                
                ratings =  rat
            }
            
        }
        
    }


}

//
//  vaiDRx_PastProvidersVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class vaiDRx_PastProvidersVC: UIViewController {
    
    let pastProviderViewModel = PastProviderViewModel()
    var CBModel:ConfirmBookingModel = ConfirmBookingModel.sharedInstance()
    let appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
    let acessClass = AccessTokenRefresh()
    var pastProviderResponseModel: PastProviderResponseModel? = nil
    var confirmBookingViewModel = ConfirmBookingViewModel()
    var bookingId:Int64!
    var isComing = ""
    var isComingfromTitle = ""
    var selectedIndex = 0
    var vaidType = 0//1-Now//2-Today

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        pastProviderViewModel.CBModel = CBModel
        pastProviderViewModel.CBModel.appointmentLocationModel = self.appointmentLocationModel

//        if isComing == "Home"{
//
//            backButton.setImage(#imageLiteral(resourceName: "back_icon"), for: .normal)
//        } else{
        
            backButton.setImage(#imageLiteral(resourceName: "close")
                , for: .normal)
//        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveBookingAcception),
                                               name: NSNotification.Name(rawValue: "dismissPast"),
                                               object: nil)
        sendServiceToGetPastProviders()
//        Helper.editNavigationBar(navigationController!)
        // Do any additional setup after loading the view.
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//         Helper.editNavigationBar(navigationController!)
//        //        BookingStatusResponseManager.sharedInstance().delegate = self
//        //        showProfileImageView()
//    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didReceiveBookingAcception() {
        NotificationCenter.default.removeObserver(self)
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: false)
//        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionReveal, subType: kCATransitionFromBottom, for: (self.navigationController?.view)!, timeDuration: 0.35)
//        self.navigationController?.popViewController(animated: false)
        
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//         Helper.editNavigationBar(navigationController!)
//    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
//        self.navigationController?.view?.backgroundColor = UIColor.clear //UIColor(hex: "05C1C9") //UIColor.white //05C1C9
        self.navigationController?.navigationBar.barTintColor = .white //UIColor(hex: "05C1C9") //UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    

    @IBAction func backAction(_ sender: UIButton) {
        
        NotificationCenter.default.removeObserver(self)
        if isComing == "Home"{
            
            self.navigationController?.popViewController(animated: true)
            
        } else {

            self.dismiss(animated: true, completion: nil)
//            TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionReveal, subType: kCATransitionFromBottom, for: (self.navigationController?.view)!, timeDuration: 0.35)
            self.navigationController?.popViewController(animated: false)
        }
    }
    

}

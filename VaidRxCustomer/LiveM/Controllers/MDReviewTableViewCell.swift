//
//  MDReviewTableViewCell.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

class MDReviewTableViewCell:UITableViewCell {
    
    
    @IBOutlet weak var reviewerImageView: UIImageView!
    
    @IBOutlet var reviewView: UIView!
    
    @IBOutlet weak var reviewerNameLabel: UILabel!
    
    @IBOutlet weak var daysAgoLabel: UILabel!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet var noReviewsBackView: UIView!
    
    @IBOutlet var noReviewsLabel: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    
    func showReviewDetails(reviewModel:ReviewsModel) {
        
        reviewerNameLabel.text = reviewModel.userName
        
        commentLabel.text = reviewModel.comment
        
        ratingView.rating = Float(reviewModel.ratings)

        
        if !reviewModel.userPicURL.isEmpty  {
            
            if reviewModel.userPicURL.length > 0 {
            
                    reviewerImageView.kf.setImage(with: URL(string: reviewModel.userPicURL),
                                             placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                             options: [.transition(ImageTransition.fade(1))],
                                             progressBlock: { receivedSize, totalSize in
                        },
                                             completionHandler: nil)
            
            }
        } else {
            
            reviewerImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        
        
        if reviewModel.reviewAt > 0  {
            
            let reviewDate = Date(timeIntervalSince1970: TimeInterval(reviewModel.reviewAt))
            
            let gregorianCalendar = Calendar.init(identifier: .gregorian)
            
            let components: DateComponents? = gregorianCalendar.dateComponents([.day, .month, .year], from: reviewDate, to: Date())//gregorianCalendar.dateComponents([.day, .month, .year], from: reviewDate)
            DDLogVerbose("components:\(String(describing: components))")
            
            if (components?.year)! > 0 {
                
                if components?.year == 1 {
                    
                    daysAgoLabel.text = "\((components?.year)!) year ago"
                }
                else {
                    
                    daysAgoLabel.text = "\((components?.year)!) years ago"
                }
            }
            else if (components?.month)! > 0 {
                
                if components?.month == 1 {
                    
                    daysAgoLabel.text = "\((components?.month)!) month ago"
                }
                else {
                    
                    daysAgoLabel.text = "\((components?.month)!) months ago"
                }
                
            }
            else {
                
                if components?.day == 0 {
                    
                    daysAgoLabel.text = "Today"
                }
                else if components?.day == 1 {
                    
                    daysAgoLabel.text = "Yesterday"
                }
                else {
                    
                    daysAgoLabel.text = "\((components?.day)!) days ago"
                }
            }
            
            
        } else {
            
            daysAgoLabel.text = "Today"
        }
        
    }
}

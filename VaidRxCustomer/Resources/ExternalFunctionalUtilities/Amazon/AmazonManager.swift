//
//  File.swift
//  AmazonManager
//
//  Created by Apple on 27/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import AWSS3

let AmazonAccessKey  = "AKIAIAKBKN7NMPOZXRJA" //AKIAIBWUDAG6Y6HNBLYQ
let AmazonSecretKey  = "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" //vpGAI1WK8mV2JHUzpJo/3yxhT9bcMDQ+UFUf4VAg
let Bucket      = "vaidrx"
//let AMAZON_URL  = "https://s3.amazonaws.com/"
let AMAZON_URL  = "https://s3.us-east-2.amazonaws.com/"
let AWS_POOL_ID     = "us-east-2:a86c8ee4-0531-4f1d-94cc-66dfe6ab15c6"

protocol AmazonManagerDelegate {
    
    /**
     *  Facebook login is success
     *
     *  @param userInfo Userdict
     */
    func didImageUploadedSuccessfully(withDetails imageURL: String)
    
    
    /**
     *  Login failed with error
     *
     *  @param error error
     */
    func didImageFailtoUpload(_ error: Error?)
    
    
}

class AmazonManager: NSObject {
    
    static var share:AmazonManager?
    var delegate: AmazonManagerDelegate?
    
    class func sharedInstance() -> AmazonManager {
        
        if (share == nil) {
            
            share = AmazonManager.self()
            
        }
        return share!
    }
    
    override init() {
        super.init()
        
    }
    
    
    
    /// Method to set configure AWS Region
    ///
    /// - Parameters:
    ///   - regionType: AWSRegionType value
    ///   - accessKey: accessKey value
    ///   - secretKey: secretKey value
    func setConfigurationWithRegion(_ regionType: AWSRegionType,
                                    accessKey: String,
                                    secretKey: String) {

        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region: regionType, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
//    func setPoolConfigurationWithRegion(_ regionType: AWSRegionType) {
//
//        let poolCredentialProvider = AWSCognitoCredentialsProvider.init(regionType: regionType, identityPoolId: AWS_POOL_ID)
//        let configuration = AWSServiceConfiguration.init(region: regionType, credentialsProvider:poolCredentialProvider)
//        AWSServiceManager.default().defaultServiceConfiguration = configuration
//    }
    
  
    
    /// Method to upload image to AWS server
    ///
    /// - Parameters:
    ///   - image: image to upload
    ///   - imgPath: path of image to upload
    ///   - completion: completion block
    func upload(withImage image: UIImage,
                imgPath:String,
                completion: @escaping(_ success: Bool, _ url: String) -> Void) {
        
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmssa"
        
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [AnyObject]
        let documentsDirectory: String = paths[0] as! String
        
        let photoURLPath = NSURL(fileURLWithPath: documentsDirectory)
        let getImagePath  = photoURLPath.appendingPathComponent("\(formatter.string(from:Date())).png")
        
        let imgPathName = imgPath.appending("\(formatter.string(from:Date())).png")
        
        
        if !FileManager.default.fileExists(atPath: getImagePath!.path) {
            do {
                try image.jpegData(compressionQuality: 1.0)?.write(to: getImagePath!)
                print("file saved")
            }catch {
                print("error saving file")
            }
        }
        else {
            print("file already exists")
        }
        
        AWSS3TransferUtility.default().uploadFile(getImagePath!,
                                                  bucket: Bucket,
                                                  key: imgPathName,
                                                  contentType: "image/png", expression:nil) { (task, error) in
                                                    
                                                    if (error != nil) {
                                                        completion(false, "")
                                                    }
                                                    else {
                                                        let uploadedImageURL = String(format:"%@%@/%@",AMAZON_URL,Bucket,imgPathName)
                                                        completion(true, uploadedImageURL)
                                                    }
                                                    
        }
        
    }
    
    /// Method to upload image to AWS server
    ///
    /// - Parameters:
    ///   - image: image to upload
    ///   - imgPath: path of image to upload
    ///   - completion: completion block
    func uploadChatImage(withImage image: UIImage,
                imgPath:String,
                completion: @escaping(_ success: Bool, _ url: String) -> Void) {
        
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmssa"
        
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [AnyObject]
        let documentsDirectory: String = paths[0] as! String
        
        let photoURLPath = NSURL(fileURLWithPath: documentsDirectory)
        let getImagePath  = photoURLPath.appendingPathComponent("\(formatter.string(from:Date())).png")
        
//        let imgPathName = imgPath.appending("\(formatter.string(from:Date())).png")
        
        
        if !FileManager.default.fileExists(atPath: getImagePath!.path) {
            do {
                try image.jpegData(compressionQuality: 1.0)?.write(to: getImagePath!)
                print("file saved")
            }catch {
                print("error saving file")
            }
        }
        else {
            print("file already exists")
        }
        
        AWSS3TransferUtility.default().uploadFile(getImagePath!,
                                                  bucket: Bucket,
                                                  key: imgPath,
                                                  contentType: "image/png", expression:nil) { (task, error) in
                                                    
                                                    if (error != nil) {
                                                        completion(false, "")
                                                    }
                                                    else {
                                                        let uploadedImageURL = String(format:"%@%@/%@",AMAZON_URL,Bucket,imgPath)
                                                        completion(true, uploadedImageURL)
                                                    }
                                                    
        }
        
    }

}

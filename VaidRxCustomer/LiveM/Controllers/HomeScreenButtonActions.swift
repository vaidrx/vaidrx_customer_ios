//
//  HomeScreenButtonActions.swift
//  LiveM
//
//  Created by Apple on 24/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension HomeScreenViewController {
    
    @IBAction func currentLocationAction(_ sender: Any) {
        
        showCurrentLocation()
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        let leftMenu = slideMenuController()
        leftMenu?.openLeft()
    }
    
    @IBAction func listAction(_ sender: Any) {
        
        flipAnimation(VCIdentifier.homeListVC)
    }
    
    @IBAction func datePickerAction(_ sender: Any) {
        
        datepickerSetUp()
    }
    
    @IBAction func searchLocationButton(_ sender: Any) {
        
    }

}

//
//  PastPharmInfoVCAPICallExt.swift
//  vaiDRx
//
//  Created by 3Embed on 28/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation

extension PastPharmacyInfoViewController{
    func updatePharmacyData(){
        
        if !NetworkHelper.sharedInstance.networkReachable(){
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
            
            return
        }
        pastPharmViewModel.check = isAccepted
        pastPharmViewModel.oldPharmacyTransferCheck = oldPharmacyTransferCheck
        pastPharmViewModel.countryCode = countryCodeLabel.text!
        pastPharmViewModel.rxTodayPharmacyAPICall { (statusCode, errMsg, response) in
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response,requestType:RequestType.updateProfileData)
        }
        
        
    }
    
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResp:Any?,requestType:RequestType){
        switch statusCode {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            performSegue(withIdentifier: "ToSuccessfulMedicationTransferMess", sender: self)
//            switch requestType{
//            case .updateProfileData:
//                self.navigationController?.popViewController(animated: true)
//                break
//            case .getPharmacy:
////                if dataResp != nil{
////                    pharmacyData = PharmacyResponseModel.init(pharmacyResponseDetails:dataResp! )
////
////                    pharmacyItems = pharmacyData.pharmacyItemsModel
////                    DispatchQueue.main.async {
////                        self.collectionView.reloadData()
////                    }
////
////                    print("pharmacyItems",pharmacyItems)
////
////
////                    print("data response",dataResp ?? [:])
////
////                    if pharmacyItems != nil{
////                        showMarkersInMapView()
////                    }
////
////                }
//                break
//            default:
//                break
//            }
            
            break
            
        default:
            break
        }
    }
}

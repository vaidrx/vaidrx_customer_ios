//
//  PharmacyServiceYesOrNo.swift
//  GoTasker
//
//  Created by 3Embed on 21/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class PharmacyServiceYesOrNo: UIViewController {

    @IBOutlet weak var lbYes: UILabel!
    @IBOutlet weak var lbNo: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var imgYes: UIImageView!
    @IBOutlet weak var imgCheckNo: UIImageView!
    var nextButtonStatus = "No"
     var registerViewModel: RegisterViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigation()
    }
    
    func hideNavigation() {
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = .clear //UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = .clear //UIColor(hex: "05C1C9") //UIColor.clear
    }

    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func yesButtonAction(_ sender: UIButton) {
        
        imgYes.image = UIImage(named: "uncheck_unselected")
        imgCheckNo.image = UIImage(named: "check_unselected")
        lbYes.textColor = .black
        lbNo.textColor = .white
        performSegue(withIdentifier: "ToPharmacySelectionVC", sender: self)
    }
    
    @IBAction func noButtonAction(_ sender: UIButton) {
       // nextButtonStatus = "Yes"
        imgYes.image = UIImage(named: "check_unselected")
        imgCheckNo.image = UIImage(named: "uncheck_unselected")
        lbYes.textColor = .white
        lbNo.textColor = .black
        performSegue(withIdentifier: "ToAddCardDetailsVC", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToAddCardDetailsVC" {
            
            if let dstVC = segue.destination as? vaiDRx_AddNewCardVC {
                
                dstVC.registerViewModel = self.registerViewModel
            }
            
        }
        if segue.identifier == "ToPharmacySelectionVC" {
            
            if let dstVC = segue.destination as? PharmacyViewController {
                
                dstVC.registerViewModel = self.registerViewModel
            }
            
        }
    }
  
}

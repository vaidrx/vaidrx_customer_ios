//
//  RxTodayVCAPICallExt.swift
//  vaiDRx
//
//  Created by 3Embed on 01/03/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation

extension RxTodayViewController{
    
    //To check wheteher the user has confirmed to the Pharmacy what he has selected
    func sendServiceToGetProfile() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        profileViewModel.getProfileDetailsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.confirmPharmacy)
        }
        
    }
    
    func transferMedications() {
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        rxToadyViewModel.transferMyCurrentMedicins = transferMedication
        rxToadyViewModel.rxTodayTransferMedicationsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.transferMedications)
        }
    }
    
    func refillCurrentMedications() {
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        rxToadyViewModel.refillMyCurrentMedicins = refillMedicationsYesOrNo //refillMedications
        rxToadyViewModel.rxTodayRefillMedicationsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.RefillCurrentMedications)
        }
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        
        switch statusCode
        {
            
//        case HTTPSResponseCodes.TokenExpired.rawValue:
//
//            if let dataRes = dataResponse as? String {
//
//                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
//
////                self.apiTag = requestType.rawValue
//
//                var progressMessage = PROGRESS_MESSAGE.Loading
//
//                switch requestType {
//
//                case .signOut:
//
//                    progressMessage = PROGRESS_MESSAGE.LogOut
//
//                case .getProfileData:
//
//                    progressMessage = PROGRESS_MESSAGE.Loading
//
//                case .updateProfileData:
//
//                    progressMessage = PROGRESS_MESSAGE.Saving
//                    sendServiceToGetProfile()
//
//                default:
//
//                    break
//                }
//
//                self.acessClass.getAcessToken(progressMessage: progressMessage)
//            }
//
//            break
            
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.confirmPharmacy:
                
//                self.showViews()
                
                if dataResponse != nil {
                    
                    self.profileResponseModel = ProfileResponseModel.init(profileDetails: dataResponse!)
//                    setAllData(data: profileResponseModel!)
                    print(profileResponseModel!.isPharmacyApproved)
                    pharmacyName.text = profileResponseModel?.pharmacyName
                    if  profileResponseModel?.isPharmacyApproved != nil {
                        if profileResponseModel!.isPharmacyApproved == false {
                            
                        }
                    }
                }
                break
            case RequestType.transferMedications:
                print(" done")
                
            case RequestType.RefillCurrentMedications :
                self.smsAlertMessage()
                
            default:
                
                break
            }
            
        default:
            
            if  errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                
            }
            
            
            break
        }
        
    }
}

//
//  HomeVCBookLaterMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

extension HomeScreenViewController {

    func showBookLaterDetails() {
        
        if appointmentLocationModel.bookingRequested {
            
            appointmentLocationModel.bookingRequested = false
            
            Helper.clearLaterBookingRequestDetails()
            hideSelectedBookLaterDetails()
            selectedDate = scheduleDate
            datePickerView.date = selectedDate
            selectedEventStartTag = 0
            
            appointmentLocationModel.pickupLatitude = locationObj.latitute
            appointmentLocationModel.pickupLatitude = locationObj.longitude
            appointmentLocationModel.pickupAddress = locationObj.address
            self.addressLabel.text = locationObj.address
            
            self.showCurrentLocation()
            
        } else {
            
            if appointmentLocationModel.bookingType == BookingType.Schedule {
                
                selectedDate = appointmentLocationModel.scheduleDate
                selectedEventStartTag = appointmentLocationModel.selectedEventStartTag
                datePickerView.date = selectedDate
                showSelectedBookLaterDetails()
                
            } else {
                
                hideSelectedBookLaterDetails()
                selectedDate = scheduleDate
                datePickerView.date = selectedDate
                selectedEventStartTag = 0
            }
        }
        
        self.eventStartCollectionView.reloadData()
    }
    
    func updateSelectedBookLaterDetails(){
        
        if selectedDate != nil {
            
            let dateformat = DateFormatter()
            dateformat.dateFormat = "dd MMM yyyy hh:mm a"
            
            self.selectedBookLaterDateLabel.text = dateformat.string(from:selectedDate)
        }
        
    }

    func showSelectedBookLaterDetails(){
        
        updateSelectedBookLaterDetails()
        self.selectedBookLaterBackView.isHidden = false
    }
    
    func hideSelectedBookLaterDetails(){
        
        self.selectedBookLaterBackView.isHidden = true
    }
    

}

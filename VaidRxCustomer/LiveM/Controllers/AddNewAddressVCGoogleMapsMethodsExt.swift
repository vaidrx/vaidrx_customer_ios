//
//  AddNewAddressVCGoogleMapsMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps

extension AddNewAddressViewController:GMSMapViewDelegate {
    
    /// Set Intial MapView Properties
    func setInitialMapViewProperties() {
        
        currentLatitude = locationObj.latitute
        currentLongitude = locationObj.longitude
        
        if isForEditing {
            
            pickupLatitude = Double(addressDetails.latitude)
            pickupLongitude = Double(addressDetails.longitude)
            
            addressLabel.text = addressDetails.addressLine1
            
        } else {
            
            pickupLatitude = locationObj.latitute
            pickupLongitude = locationObj.longitude
            
            addressLabel.text = locationObj.address
            
        }
        
        if isFromConfirmBookingVC {
            
            mapPinBackViewCenterYConstraint.constant = -22.5
            
        } else {
            
            mapPinBackViewCenterYConstraint.constant = 0
        }
        
        mapPinBackView.layoutIfNeeded()
        
        
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(pickupLatitude),
                                              longitude: CLLocationDegrees(pickupLongitude),
                                              zoom: MAP_ZOOM_LEVEL)
        self.mapView.animate(to: camera)
        
    }
    
    
    // MARK: - GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        if isAddressForEditing {
            
            isAddressForEditing = false
            
        } else if isAddressPickedManually {
            
            isAddressPickedManually = false
            
        } else {
            
            getLocationDetails(position.target)
        }
        
        
    }
    
    
    /// Get Location Details From Lat & Long
    ///
    /// - Parameter coordinate: current map position lat & Long
    func getLocationDetails(_ coordinate: CLLocationCoordinate2D) {
        Helper.showPI(_message: "Loading")
        
        pickupLatitude = coordinate.latitude
        pickupLongitude =  coordinate.longitude
        
//        let geoCoder = CLGeocoder()
//        let location = CLLocation.init(latitude: pickupLatitude, longitude: pickupLongitude)
        
        
        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: { (gmsAddressResponse, error) in
            
            if let addressDetails = gmsAddressResponse?.firstResult() {
                
                // Address Details
                DDLogVerbose("Add New Address Address Details:\(addressDetails)")
                
                
                // Current Address
                if let currentAddress = addressDetails.lines {
                    
                    print("Current Address: \(currentAddress)")
                    
                    var address = (currentAddress.joined(separator: ", "))
                    
                    if address.hasPrefix(", ") {
                        
                        address = address.substring(2)
                    }
                    
                    if address.hasSuffix(", ") {
                        
                        let endIndex = address.index(address.endIndex, offsetBy: -2)
                        
                        address = address.substring(to: endIndex)
                    }
                    
                    

                    self.fullAddressParams  = ["address":address,"latitude":self.pickupLatitude,"logitude":self.pickupLongitude,"taggedAs":self.taggedAs,"city":addressDetails.locality ?? "","state":addressDetails.administrativeArea ?? "","country":addressDetails.country ?? "","pincode":addressDetails.postalCode ?? ""]
                    print("fullAddressParams",self.fullAddressParams)
                    
                    Helper.hidePI()
//                    self.addMyAddressDelegate.addFullAddress(fulladdress: fullAddressParams)
                    self.addressLabel.text = address
                    
                    self.updatedAddressFromMap()
                }
                
                
            } else {
                
                DDLogError("Add New Address Class Address Fetch Error: \(String(describing: error?.localizedDescription))")
            }
            
        })

        
        
        /*geoCoder.reverseGeocodeLocation(location) {
         
            (placemarks, error) -> Void in
         
            if let arrayOfPlaces: [CLPlacemark] = placemarks as [CLPlacemark]! {
         
                // Place details
                if let placemark: CLPlacemark = arrayOfPlaces.first {
                    
                    // Address dictionary
                    print("Address Dict :\(placemark.addressDictionary!)")
                    
                    // Current Address
                    if let currentAddress = placemark.addressDictionary?["FormattedAddressLines"]  {
                        
                        print("Current Address: \(currentAddress)")
                        self.addressLabel.text = (currentAddress as? Array)?.joined(separator: ", ")
                        self.updatedAddressFromMap()
                    }
                    
                }
            }
        }*/
        
    }
    
    
    
}

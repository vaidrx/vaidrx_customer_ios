//
//  LoginAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 21/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class LoginAPI {
    
    let disposebag = DisposeBag()
    let login_Response = PublishSubject<APIResponseModel>()
    
    let location = LocationManager.sharedInstance()
    
    /// Method to call Login Service API
    ///
    /// - Parameter loginModel: Contains Login Request Details
    func loginServiceAPICall(loginModel: LoginModel){
        
        let strURL = API.BASE_URL + API.METHOD.LOGIN
        
        var requestParams:[String:Any] = [
            
            SERVICE_REQUEST.Email_Phone:loginModel.emailText,
            SERVICE_REQUEST.Password:loginModel.passwordText,
            SERVICE_REQUEST.DeviceID:Utility.deviceId,
            //            SERVICE_REQUEST.PushToken:Utility.pushtoken,
            SERVICE_REQUEST.DeviceType:DeviceType.IOS.rawValue,
            SERVICE_REQUEST.DeviceTime:TimeFormats.currentDateTime,
            SERVICE_REQUEST.LoginType:loginModel.loginType.rawValue,
            SERVICE_REQUEST.DeviceModel:UIDevice.current.modelName,
            SERVICE_REQUEST.DeviceMake:Utility.deviceMake,
            SERVICE_REQUEST.AppVersion:Utility.appVersion,
            SERVICE_REQUEST.Lat:location.latitute,
            SERVICE_REQUEST.Long:location.longitude,
            SERVICE_REQUEST.FaceBookID: loginModel.facebookId,
            SERVICE_REQUEST.GoogleId: loginModel.googleId
        ]
        
        print("login parameters",requestParams)
        
        if loginModel.loginType == LoginType.Facebook {
            
            requestParams[SERVICE_REQUEST.FacebookId] = loginModel.facebookId
            Helper.showPI(_message: PROGRESS_MESSAGE.FBLogin)
            
        } else if loginModel.loginType == LoginType.Google {
            
            requestParams[SERVICE_REQUEST.GoogleId] = loginModel.facebookId
            Helper.showPI(_message: PROGRESS_MESSAGE.GoogleLogin)
        } else {
            
            Helper.showPI(_message: PROGRESS_MESSAGE.Login)
        }
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: nil)
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    func checkResponse(statusCode:Int, responseDict: [String:Any]){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.login_Response.onNext(responseModel)
            break
        }
    }
    
}

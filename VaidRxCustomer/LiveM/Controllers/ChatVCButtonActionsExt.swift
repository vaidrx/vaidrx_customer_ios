//
//  ChatVCButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 12/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension ChatViewController {
    
    @IBAction func dismissViewAction(_ sender: Any) {
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    
    @IBAction func showMessage(_ sender: Any) {
        
    }
    
    @IBAction func selectGallery(_ sender: Any) {
        
        chooseFromPhotoGallery()
    }
    
    @IBAction func selectCamera(_ sender: Any) {
        
        chooseFromCamera()
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        
        //        self.canSendLocation = true
        //        self.animateExtraButtons(toHide: true)
        //        if self.checkLocationPermission() {
        //            self.locationManager.startUpdatingLocation()
        //        } else {
        //            self.locationManager.requestWhenInUseAuthorization()
        //        }
    }
    
    @IBAction func showOptions(_ sender: Any) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: ALERTS.SelectImage,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: ALERTS.Cancel,
                                                              style: .cancel) { action -> Void in
                                                                
           // UIWindow.Level.alertAlertWindow.resignKey()
                                                                newAlertWindow.removeFromSuperview()
            
        
        }
        actionSheetController.addAction(cancelActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: ALERTS.Camera,
                                                              style: .default) { action -> Void in
                                                                
                                                                newAlertWindow.resignKey()
                                                                newAlertWindow.removeFromSuperview()
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: ALERTS.Gallery,
                                                            style: .default) { action -> Void in
                                                                
                                                                newAlertWindow.resignKey()
                                                                newAlertWindow.removeFromSuperview()
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        
        newAlertWindow.rootViewController?.present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if let text = self.inputText.text {
            if text.length > 0 {
                self.composeMessage(type: .text, content: self.inputText.text!)
                self.inputText.text = ""
            }
        }
    }
    
}

//
//  vaiDRx_GetName_ButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 08/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_GetNameVC {
    
    
    @IBAction func nextAction(_ sender: UIButton) {
        
        if firstNameTextFeild.text?.count == 0 && lastNameTextFeild.text?.count == 0 {
            
            Helper.alertVC(title: "Message", message: "Please fill all feilds")
        } else {
            
            performSegue(withIdentifier: "toDateOfBirth", sender: self)
            //performSegue(withIdentifier: "toPrefferedPayment", sender: self)
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }

}

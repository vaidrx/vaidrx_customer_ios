//
//  PaymentListVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension PaymentViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as! PaymentTableViewCell
        
        let eachCardDetail:CardDetailsModel = arrayOfCards[indexPath.row]
        
        cell.cardNumberLabel.text = "**** **** **** " + eachCardDetail.last4Digits
        cell.cardImage.image = Helper.cardImage(with: eachCardDetail.brand)
        
        cell.tickImageWidthConstraint.constant = 0
        
        if eachCardDetail.defaultCard == true {
            
            cell.defaultCardLabel.isHidden = false
            
        } else {
            
            cell.defaultCardLabel.isHidden = true
        }
        
        return cell
    }
}


extension PaymentViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isFromConfirmBookingVC {
            
            //Save Selected Card details in Confirm Booking Model
            CBModel.selectedCardModel = arrayOfCards[indexPath.row]
            
            self.navigationController?.popViewController(animated: true)
            
        } else {
            
            //Go to Payment Details VC
            let paymentDetailVC:PaymentDetailsViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.paymentDetailVC) as! PaymentDetailsViewController
            
            paymentDetailVC.cardDetails = arrayOfCards[indexPath.row]
            
            self.navigationController!.pushViewController(paymentDetailVC, animated: true)
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.transform = CGAffineTransform(translationX: -500, y: 0)
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 4,
                       options: [],
                       animations: {
                        
                        cell.contentView.transform = .identity
                        
        }, completion: { (completed) in
            
            
        })
        
        
        //        UIView.animate(withDuration: 0.6) {
        //            cell.contentView.transform = .identity
        //        }
        
    }
    
}

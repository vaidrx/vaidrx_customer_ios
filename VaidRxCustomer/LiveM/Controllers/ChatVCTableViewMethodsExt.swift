//
//  ChatVCTableViewMethods.swift
//  LiveM
//
//  Created by Rahul Sharma on 12/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

//MARK: - UITableview Methods -
extension ChatViewController:UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch self.items[indexPath.row].owner {
            
        case .sender:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
            
            cell.clearCellData()
            
            switch self.items[indexPath.row].type {
                
            case .text:
                
                cell.message.text = self.items[indexPath.row].content as! String
                cell.messageBackground.backgroundColor = RED_COLOR
                cell.message.backgroundColor = RED_COLOR
                cell.message.textColor = Helper.UIColorFromRGB(rgbValue: 0xFFFFFF)
                
                cell.message.isHidden = false
                cell.actionOnImageView.isHidden = true
                cell.messageBackground.isHidden = true
                cell.messageBackground.image = nil
                
            case .photo:
                
                cell.actionOnImageView.isHidden = true
                cell.messageBackground.isHidden = false
                cell.message.isHidden = true
                cell.actionOnImageView.isHidden = false
                cell.actionOnImageView.tag = indexPath.row
                cell.messageBackground.backgroundColor = RED_COLOR
                cell.actionOnImageView.addTarget(self, action: #selector(self.showImage(_:)), for:.touchUpInside)
                cell.messageBackground.kf.setImage(with: URL(string: self.items[indexPath.row].content as! String),
                                                   placeholder:UIImage.init(named: "chat-loading"),
                                                   options: [.transition(ImageTransition.fade(1))],
                                                   progressBlock: { receivedSize, totalSize in
                },
                                                   completionHandler: nil)
                //                        }
                
            case .location:
                
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
                cell.actionOnImageView.isHidden = true
            }
            return cell
            
        case .receiver:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            cell.clearCellData()
            cell.showRecieverImage(imageURL: musicianImageURL)
         
            switch self.items[indexPath.row].type {
                
            case .text:
                
                cell.message.text = self.items[indexPath.row].content as! String
                cell.message.textColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
                cell.messageBackground.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xF0F0F0)
                cell.message.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xF0F0F0)
                
                cell.message.isHidden = false
                cell.actionOnImageView.isHidden = true
                cell.messageBackground.isHidden = true
                cell.messageBackground.image = nil
                
            case .photo:
                
                cell.messageBackground.isHidden = false
                cell.message.isHidden = true
                cell.actionOnImageView.isHidden = false
                cell.actionOnImageView.tag = indexPath.row
                cell.actionOnImageView.addTarget(self, action: #selector(self.showImage(_:)), for:.touchUpInside)
                cell.messageBackground.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xF0F0F0)
                
                cell.messageBackground.kf.setImage(with: URL(string: self.items[indexPath.row].content as! String),
                                                   placeholder:UIImage.init(named: "chat-loading"),
                                                   options: [.transition(ImageTransition.fade(1))],
                                                   progressBlock: { receivedSize, totalSize in
                },
                                                   completionHandler: nil)
                
            case .location:
                
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
                cell.actionOnImageView.isHidden = true
            }
            return cell
        }
    }
    
    @objc func showImage(_ sender : UIButton){
        //        let info = ["viewType" : ShowExtraView.preview, "pic": self.items[sender.tag].content] as [String : Any]
        //        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
        //        self.inputAccessoryView?.isHidden = true
        
        //Go to Chat Image Preview VC
        let chatImagePreviewVC:ChatImagePreviewVC = self.storyboard!.instantiateViewController(withIdentifier: "ChatImagePreviewVC") as! ChatImagePreviewVC
        
        chatImagePreviewVC.showViewType = .preview
        chatImagePreviewVC.imageURL = self.items[sender.tag].content as! String
        
        self.present(chatImagePreviewVC, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.inputText.resignFirstResponder()
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

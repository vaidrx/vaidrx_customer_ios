//
//  vaiDRx_GetNameVC.swift
//  LiveM
//
//  Created by Rahul Sharma on 08/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class vaiDRx_GetNameVC: UIViewController, KeyboardDelegate  {
    
    // MARK: - Outlets -
    
    //TextFeilds
    @IBOutlet weak var firstNameTextFeild: UITextField!
    @IBOutlet weak var lastNameTextFeild: UITextField!
    
    // MARK: - Variable Decleration -
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var registerViewModel: RegisterViewModel!
    let disposeBag = DisposeBag()
    var vaidRx_AddNewCardViewModel = VaidRx_AddNewCardViewModel()
    var stripeKeyResponseModel :StripeKeyResponseModel!

    //Layout Constraint
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.hideNavBarShadow(vc: self)
        addObserveToVariables()
        OperationQueue.main.addOperation {
        self.firstNameTextFeild.becomeFirstResponder()
        }
        getStripeKeyData()
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDelegate?.keyboardDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        appDelegate?.keyboardDelegate = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addObserveToVariables() {
        
        firstNameTextFeild.rx.text
            .orEmpty
            .bind(to: registerViewModel.firstNameText )
            .disposed(by: disposeBag)
        
        lastNameTextFeild.rx.text
            .orEmpty
            .bind(to: registerViewModel.lastNameText )
            .disposed(by: disposeBag)
    }
    
    func getStripeKeyData(){
        
        if !NetworkHelper.sharedInstance.networkReachable(){
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
            //Helper.showAlertMessage(title: ALERT_VAIDRX.Error, message: ALERT_VAIDRX.NoInternet)
            return
        }
        
        vaidRx_AddNewCardViewModel.getStripeKeyAPICall { (statusCode, errMsg, response) in
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response)
        }
    }
    
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResp:Any?){
        switch statusCode {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            stripeKeyResponseModel = StripeKeyResponseModel.init(stripeKEyResponseDetails:dataResp as Any )
            
            //            insuranceItems = insuranceData.insuranceItemsModel
            //            DispatchQueue.main.async {
            //                self.tableView.reloadData()
            //            }
//            publishableKey = stripeKeyResponseModel.publishableKey
            print("insuranceItems",stripeKeyResponseModel)
            ConfigManager.sharedInstance.paymentGatewayKey = stripeKeyResponseModel.publishableKey
            
        //            print("data response",dataResp!)
        default:
            break
        }
    }
}

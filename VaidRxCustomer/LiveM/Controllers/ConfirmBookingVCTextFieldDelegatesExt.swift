//
//  ConfirmBookingVCTextFieldDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmBookingScreen:UITextFieldDelegate {
    
    /**
     *   This method will called when the user textfield text is changed
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var textFieldText = String()
        
        if range.length == 0 {//Entered New Character
            
            textFieldText = textField.text! + string
        }
        else {//Entered BackSpace
            
            textFieldText = String(textField.text!.dropLast())
        }
        
        if textFieldText.length == 0 {
            
            promocodeCell.applyButton.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
            promocodeCell.applyButton.isUserInteractionEnabled = false
            
        } else {
            
            promocodeCell.applyButton.setTitleColor(UIColor.red, for: UIControl.State.normal)
            promocodeCell.applyButton.isUserInteractionEnabled = true
            
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    
    
}

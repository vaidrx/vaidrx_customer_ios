//
//  InvoiceViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class InvoiceViewModel {
    
    let disposebag = DisposeBag()
    var methodName = ""
    var bookingId:Int64 = 0
    var ratingValue = 0.0
    var reviewText = ""
    
    func getBookingDetailsAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxBookingAPI = BookingAPI()
        
        rxBookingAPI.getBookingDetailsServiceAPICall(methodName: methodName)
        
        if !rxBookingAPI.getBookingDetails_Response.hasObservers {
            
            rxBookingAPI.getBookingDetails_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
//    func submitReviewAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
//        
//        let rxBookingAPI = BookingAPI()
//        
//        rxBookingAPI.submitReviewServiceAPICall(bookingId: bookingId,
//                                                ratingValue: ratingValue,
//                                                reviewText: reviewText)
//        
//        if !rxBookingAPI.submitReview_Response.hasObservers {
//            
//            rxBookingAPI.submitReview_Response
//                .subscribe(onNext: {response in
//                    
//                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
//                        
//                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
//                        return
//                    }
//                    
//                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
//                    
//                    
//                }, onError: {error in
//                    
//                }).disposed(by: disposebag)
//            
//        }
//    }

}


//
//  InvoiceVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension InvoiceViewController {
    
    func getBookingDetailsAPI() {
        
        self.scrollView.isHidden = true
        self.submitButtonBackView.isHidden = true
        self.navigationTopView.isHidden = true
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        invoiceViewModel.methodName = API.METHOD.GET_BOOKING_DETAILS_INVOICE + "/" + String(bookingId) //String(format:"%td", bookingId)
        
        invoiceViewModel.getBookingDetailsAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.getParticularApptDetails)
        }

    }
    
    func submitReviewAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        invoiceViewModel.bookingId = bookingId
        invoiceViewModel.ratingValue = Double(ratingView.rating)
        
        if reviewTextView.text == ALERTS.invoiceReviewPlaceholder || reviewTextView.text.length == 0 {
            
            invoiceViewModel.reviewText = ""
            
        } else {
            
            invoiceViewModel.reviewText = reviewTextView.text!
        }
        
       
//        
//        invoiceViewModel.submitReviewAPICall{ (statCode, errMsg, dataResp) in
//            
//            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.reviewAndRating)
//        }
        
    }
    
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .getParticularApptDetails:
                            
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        case .reviewAndRating:
                        
                            progressMessage = PROGRESS_MESSAGE.SubmittingReview

                        
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)


                }
                
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                break
                
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType
                {
                    case RequestType.getParticularApptDetails:
                        
                        if dataResponse != nil {
                            
                            bookingDetailModel = BookingDetailsModel.init(bookingInvoiceDetails: dataResponse!)
                            
                            self.showBookingDetails()
                            self.scrollView.isHidden = false
                            self.submitButtonBackView.isHidden = false
                            self.navigationTopView.isHidden = false
                            
                        }
                        
                        break
                        
                    case RequestType.reviewAndRating:
                        
                        MixPanelManager.sharedInstance.BookingCompletedEvent(bookingId: String(bookingId),
                                                                             rating: ratingView.rating,
                                                                             reviewComment:invoiceViewModel.reviewText)
                        ChatCouchDBManager.sharedInstance.deleteParticularBookingChatCouchDBDocument(bookingId: String(bookingId))
                        
                        self.navigationController?.popToRootViewController(animated: false)
                       
                        UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.BookingReviewed)
                        UserDefaults.standard.synchronize()
                        LeftMenuTableViewController.sharedInstance().changeViewController(LeftMenu.myEvent)
                        BookingStatusResponseManager.sharedInstance().showRemainingCompletedBookingInvoice(bookingId)
                        
                        break
                        
                        
                    default:
                        break
                }
                break
                
                
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }
                
                break
        }
        
    }
    
}

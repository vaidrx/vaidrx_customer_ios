//
//  PaymentAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class PaymentCardAPI {
    
    let disposebag = DisposeBag()
    let getPaymentCardList_Response = PublishSubject<APIResponseModel>()
    let addPaymentCard_Response = PublishSubject<APIResponseModel>()
    let makePaymentCardDefault_Response = PublishSubject<APIResponseModel>()
    let deletePaymentCard_Response = PublishSubject<APIResponseModel>()
    
    
    
    /// Method to call Get List of payment cards that user already added Service API
    func getListOfPaymentCardsServiceAPICall(){
        
        let strURL = API.BASE_URL + API.METHOD.GET_CARDS
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.getCardDetails)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to call Add New Payment card Service API
    ///
    /// - Parameter cardTokenId: card token to add
    func addPaymentCardServiceAPICall(cardTokenId:String){
        
        let strURL = API.BASE_URL + API.METHOD.ADD_CARD
        
        let requestParams: [String: Any] = [
            
            SERVICE_REQUEST.Payment.cardToken:cardTokenId,
            SERVICE_REQUEST.Payment.email:Utility.emailId,
        ]
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Adding)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.addCard)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    
    
    /// Method to call Delete card service API
    ///
    /// - Parameter cardId: card id to delete
    func deletePaymentCardServiceAPICall(cardId:String){
        
        let strURL = API.BASE_URL + API.METHOD.DELETE_CARD
        
        let requestParams: [String: Any] = [
            
            SERVICE_REQUEST.Payment.cardId:cardId
        ]
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Deleting)

        
        RxAlamofire
            .requestJSON(.delete, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.deleteCard)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    
    /// Method to call make card default service API
    ///
    /// - Parameter cardId: card id to make default
    func makePaymentCardDefaultServiceAPICall(cardId:String){
        
        let strURL = API.BASE_URL + API.METHOD.CARD_DEFAULT
        
        let requestParams: [String: Any] = [
            
            SERVICE_REQUEST.Payment.cardId:cardId
        ]

        Helper.showPI(_message: PROGRESS_MESSAGE.Saving)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.cardDefault)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
                case .getCardDetails:
                    self.getPaymentCardList_Response.onNext(responseModel)
                
                case .addCard:
                    self.addPaymentCard_Response.onNext(responseModel)
                
                case .deleteCard:
                    self.deletePaymentCard_Response.onNext(responseModel)
                
                case .cardDefault:
                    self.makePaymentCardDefault_Response.onNext(responseModel)
                
                default:
                    break
            }
            
            break
        }
    }
    
}

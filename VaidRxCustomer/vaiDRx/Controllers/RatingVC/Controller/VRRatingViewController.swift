//
//  VRRatingViewController.swift
//  GoTasker
//
//  Created by 3Embed on 27/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class VRRatingViewController: UIViewController {

    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var ratingView: FloatRatingView!
    let rateProVM = RateProviderViewModel()
    var model: BookingStatusResponseModel!
    var bookingId: Int64 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        confirmButton.layer.cornerRadius = confirmButton.frame.height / 2
        confirmButton.isHidden = true
//        bookingId = bookingId //model.bookingId
        ratingView.delegate = self
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
          self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
//        self.navigationController?.popToRootViewController(animated: true)
         self.submitReviewAPI()
    }
    
    

}

extension VRRatingViewController: FloatRatingViewDelegate {
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        if rating == 0{
            Helper.showAlert(head: "Message", message: "Rate your experience.")
            confirmButton.isHidden = true
            return
        }
        confirmButton.isHidden = false
        rateProVM.updateRating(rating)
        
    }
    
}

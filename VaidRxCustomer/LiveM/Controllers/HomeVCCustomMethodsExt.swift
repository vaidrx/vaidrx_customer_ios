//
//  HomeCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension HomeScreenViewController {
    
    func datepickerSetUp(){
        
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24 * 30)
        let picker = DateTimePicker.show(minimumDate: min, maximumDate: max)
        picker.highlightColor = #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "DONE"
        picker.todayButtonTitle = "Today"
        picker.is12HourFormat = true
        picker.dateFormat = "hh:mm aa dd/MM/YYYY"
        //        picker.isDatePickerOnly = true
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm aa dd/MM/YYYY"
        }
    }
    
    func showProfileImageView() {
//
////        self.profileImageView.layer.borderWidth = 1
////        profileImageView.layer.borderColor = APP_COLOR.cgColor
////        self.profileImageView.layer.masksToBounds = true
////        self.profileImageView.clipsToBounds = true
//
//        if !Utility.profilePic.isEmpty {
//
//            profileImageView.kf.setImage(with: URL(string: Utility.profilePic),
//                                         placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
//                                         options: [.transition(ImageTransition.fade(1))],
//                                         progressBlock: { receivedSize, totalSize in
//            },
//                                         completionHandler: { image, error, cacheType, imageURL in
//
//            })
//
//        } else {
//
//            profileImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
//        }
    }
    
    /// Catransition Animation To View Controller
    ///kCATransitionFromTop
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String) {
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
                                                                  subType: kCATransitionFromTop,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    /// Flip Animation For View Controller
    ///
    /// - Parameter idntifier: Identifier
    func flipAnimation(_ idntifier: String){
        
    }
    
    func savePickupLocationDetails() {
        
        appointmentLocationModel.pickupAddress = self.addressLabel.text!
    }
    
    func initializeMQTTMethods(initial:Bool) {
        
        musiciansListManager.delegate = self
        
        musiciansListManager.navigation = navigationController
        musiciansListManager.viewController = self
        musiciansListManager.timeInterval = Double(ConfigManager.sharedInstance.customerPublishLocationInterval)
        
        if needToShowProgress {
            
            needToShowProgress = false
            Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
            
            if appointmentLocationModel.bookingType == BookingType.Schedule {
                
                musiciansListManager.publish(isIinitial: false)
            } else {
                
                musiciansListManager.publish(isIinitial: true)
            }
            
        }
        else if initial {
            
            musiciansListManager.publish(isIinitial: true)
            
        } else {
            
            musiciansListManager.publish(isIinitial: false)
        }
        
        
        
    }
    
    func uninitializeMQTTMethods() {
        
        musiciansListManager.stopPublish()
        musiciansListManager.navigation = nil
        musiciansListManager.viewController = nil
        musiciansListManager.delegate = nil
    }
    
    func showBookingDetails() {
//        bookingStatus = bookingDetailModel.bookingStatus
        tableView.reloadData()
        bookingButtonsView.isHidden = true
        bookingDetailsView.isHidden = false
        print("got the details")
        showNavigation()
    }
    
    func showNoArtistMessage(message:String) {
        
        // create the alert
        /*if noAeristAlertController != nil {
            
            noAeristAlertController.dismiss(animated: true, completion: nil)
            self.noAeristAlertController = nil
            
            self.perform(#selector(self.showNoArtistMessageInitially(message:)),
                                        with: message,
                                        afterDelay: 0.4)

            
        } else {
            
            showNoArtistMessageInitially(message: message)
        }*/
        
//        self.noMusiciansMessageLabel.text = message
        
//        self.noMusiciansMessageBackView.layoutIfNeeded()
//        hideMusiciansViewConstraints(message:message)
    }
    
    func showNoArtistMessageInitially(message:String) {
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            return
        }

        noAeristAlertController = UIAlertController(title: ALERTS.Message,
                                            message: message,//ALERTS.NoArtistMessage,
                                            preferredStyle: UIAlertControllerStyle.alert)
        
       
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindowLevelAlert + 1
        newAlertWindow.makeKeyAndVisible()
        
        let okAction = UIAlertAction(title: ALERTS.Ok, style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            
            self.noAeristAlertController = nil
            
        }
        
        noAeristAlertController.addAction(okAction)
        
//        if Helper.newAlertWindow == nil {
//
//            Helper.newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
//            Helper.newAlertWindow?.rootViewController = UIViewController()
//            Helper.newAlertWindow?.windowLevel = UIWindowLevelAlert + 1
//            Helper.newAlertWindow?.makeKeyAndVisible()
//            Helper.newAlertWindow?.rootViewController?.present(noAeristAlertController, animated: true, completion: nil)
//
//
//        } else {
        
            newAlertWindow.rootViewController?.present(noAeristAlertController, animated: true, completion: nil)
//        }
        
    }
    
    func checkLocationIsChanged(){
        
        /*if appointmentLocationModel.pickupAddress != self.addressLabel.text {
            
            self.addressLabel.text = appointmentLocationModel.pickupAddress
        }*/
    }
    
    func getBookings() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        bookingFlowViewModel.methodName = API.METHOD.GET_BOOKINGS
        
        bookingFlowViewModel.getBookingDetailsAPICall { (statCode, errMsg, dataResp) in
//            self.showBookingDetails()
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.getParticularApptDetails)
        }
    }
    
    func getBookingDetailsAPI() {

        if !NetworkHelper.sharedInstance.networkReachable() {

            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        bookingFlowViewModel.methodName = API.METHOD.GET_BOOKING_DETAILS + "/" + String(bookingId)

        bookingFlowViewModel.getBookingDetailsAPICall { (statCode, errMsg, dataResp) in

            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.getParticularApptDetails)
        }

    }
    
}

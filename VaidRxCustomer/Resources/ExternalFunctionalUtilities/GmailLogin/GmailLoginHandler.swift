//
//  GmailLoginHandler.swift
//  GmailSwift
//
//  Created by Apple on 05/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import GoogleSignIn

let thumbSize = CGSize(width: CGFloat(200), height: CGFloat(200))

class GmailLoginHandler:NSObject {
    
    static var share:GmailLoginHandler?
    

    class func sharedInstance() -> GmailLoginHandler {
        
        if (share == nil) {
            
            share = GmailLoginHandler.self()
        }
        return share!
    }

    override init() {
        
        super.init()
    }
    
    
    func login(withGmail viewController: UIViewController) {
        
//        GIDSignIn.sharedInstance().uiDelegate = self
//        GIDSignIn.sharedInstance?.presentingViewController = viewController
//        GIDSignIn.sharedInstance?.delegate = self
//        GIDSignIn.sharedInstance.signOut()
//        GIDSignIn.sharedInstance.signIn()

    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error != nil
        {
            //Got Error
            
        }
        else
        {
            //SuccessFully Logged IN
            
//            if GIDSignIn.sharedInstance.currentUser.profile.hasImage {
//                
//                let dimension = round(thumbSize.width * UIScreen.main.scale)
//                let imageURL = user.profile?.imageURL(withDimension: UInt(dimension))
//                print("Image Url:%@",imageURL!)
//            }
//        
//            let gmailUserDataModel = UserDataModel.init(googleDetails: user)
//            //gmailUserDataModel = UserDataModel.initArrayGoogle(str: user)
//            
//            // Perform any operations on signed in user here.
//            let userId = user.userID;                  // For client-side use only!
//            let idToken = user.authentication.idToken; // Safe to send to the server
//            let fullName = user.profile.name;
//            let firstName = user.profile.givenName;
//            let lastName = user.profile.familyName;
//            let email = user.profile.email;
//            
//            print("userId:%@\n,fullName:%@\n,firstName:%@\n,lastName:%@\n,email:%@\n,Token-Id:%@\n",userId!,fullName!,firstName!,lastName!,email!,idToken!);
//            //Return Back This Details
//            
//            
//            
        }
        
    }

    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        
        UIApplication.shared.delegate?.window??.rootViewController?.present(viewController, animated: true, completion: nil)

    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        
        UIApplication.shared.delegate?.window??.rootViewController?.dismiss(animated: true, completion:nil)
    }
    
//    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
//
//
//    }

}

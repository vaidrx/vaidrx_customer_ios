//
//  CancelBookingTableViewMethods.swift
//  LiveM
//
//  Created by Rahul Sharma on 04/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


extension CancelBookingScreen:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfCancelReasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cancelReasonCell = tableView.dequeueReusableCell(withIdentifier: "cancelBookingCell") as? CancelBookingTableViewCell
        
        cancelReasonCell?.cancelReasonLabel.text = arrayOfCancelReasons[indexPath.row].reasonName
        
        if selectedReasonRowTag == indexPath.row {
            
            cancelReasonCell?.cancelmageView.image = #imageLiteral(resourceName: "Ellipse3")
            cancelReasonCell?.cancelReasonLabel.textColor = Helper.UIColorFromRGB(rgbValue: 0x67AFF7)
            
        } else {
            
            cancelReasonCell?.cancelmageView.image = #imageLiteral(resourceName: "Ellipse2")
            cancelReasonCell?.cancelReasonLabel.textColor = UIColor.darkGray
        }
        
        
        return cancelReasonCell!

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedReasonRowTag = indexPath.row
        self.tableView.reloadData()
    }
}

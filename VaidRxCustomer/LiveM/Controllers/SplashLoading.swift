//
//  SplashLoading.swift
//  RippleAnimation
//
//  Created by Vasant Hugar on 05/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class SplashLoading: UIView {
    
    @IBOutlet var gifImageView: UIImageView!
    
    let gifImage = UIImage.gifImageWithName("liveM-Loading")
    
    static var obj: SplashLoading? = nil
    
    
    static var shared: SplashLoading {//Creating static object of SplashLoading View
        if obj == nil {
            if let views = Bundle(for: self).loadNibNamed("SplashLoading", owner: nil, options: nil) {
                obj = views.first as? SplashLoading
                obj?.frame = (WINDOW_DELEGATE??.frame)!
            }
        }
        return obj!
    }
    
    /// Method to show GIF Animation
    func showGifAnimation() {
        
        gifImageView.image = gifImage
    }
    
    
    
    /// Close splah loading view
    func closeView() {
        
        if SplashLoading.obj != nil {
            
            UIView.animate(withDuration: 1.0,
                           delay: 0,
                           usingSpringWithDamping: 0.6,
                           initialSpringVelocity: 2, options: [], animations: {
                            
                self.transform = CGAffineTransform(translationX: -SCREEN_WIDTH!, y: 0)
                                                        
            },completion: { finished in
                
                Helper.splashNewWindow?.resignKey()
                Helper.splashNewWindow?.removeFromSuperview()
                Helper.splashNewWindow = nil
                
               if Utility.sessionToken.length > 0 {
                BookingStatusResponseManager.sharedInstance().showPendingBookingStatusAfterSplashLoading()
                
                
                    if MQTTSimpleSingleChatManager.sharedInstance().chatDetailsToShow != nil {
                    MQTTSimpleSingleChatManager.sharedInstance().showChatMessages(chatMessageDetail: MQTTSimpleSingleChatManager.sharedInstance().chatDetailsToShow)
                    }
                
                    if ZenDeskManager.sharedInstance().needToShowAlert {
                        
                        ZenDeskManager.sharedInstance().showZendeskAlertMessage()
                    }
                
                    if HomeScreenViewController.obj != nil {
                    
                        if HomeScreenViewController.sharedInstance().arrayOfMusicians.count == 0 {
                        
                            HomeScreenViewController.sharedInstance().showNoArtistMessage(message: MusiciansListManager.sharedInstance().errMessageString)
                        }
                    }
                }
                
                SplashLoading.obj = nil
                self.removeFromSuperview()

            })
            
                       
        }
        
    }
    
    
}

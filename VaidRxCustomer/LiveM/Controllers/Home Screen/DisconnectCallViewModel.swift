//
//  DisconnectCallViewModel.swift
//  VaidRxCustomer
//
//  Created by Apple on 12/04/22.
//

import Foundation
import RxCocoa
import RxSwift

class DisconnectCallViewModel {
    
    var methodName = ""
    let disposebag = DisposeBag()
    
    
    func disconnectBookingCallAPICall(bookingid:Int64,completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxBookingAPI = BookingAPI()
        
        print(bookingid)
        rxBookingAPI.disconnectBookingCall(methodName: API.METHOD.DisconnectCall,bookingID: bookingid)
        
        if !rxBookingAPI.disconnectCallResponse.hasObservers {
            
            rxBookingAPI.disconnectCallResponse
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
}

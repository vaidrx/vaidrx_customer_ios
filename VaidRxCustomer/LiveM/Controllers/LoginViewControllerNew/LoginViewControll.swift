////
////  LoginViewController.swift
////  DayRunner
////
////  Created by Rahul Sharma on 23/03/17.
////  Copyright © 2017 Rahul Sharma. All rights reserved.
////
//
//import UIKit
//import GoogleSignIn
//import Firebase
//
//let kConstantObj = kConstant()
//
//class LoginViewController: UIViewController {
//
//    var activeTextField = UITextField()
//
//    @IBOutlet weak var scrollView: UIScrollView!
//
//    @IBOutlet weak var userNameTF: HoshiTextField!
//
//    @IBOutlet weak var passwordTF: HoshiTextField!
//
//    @IBOutlet weak var contentView: UIView!
//
//    @IBOutlet weak var signupOutlet: UIButton!
//
//    @IBOutlet weak var emailCheckMark: UIImageView!
//
//    @IBOutlet weak var passwordCheckMark: UIImageView!
//
//    @IBOutlet weak var bottomView: UIView!
//
//    let fbHandler:FBLoginHandler = FBLoginHandler.sharedInstance()
//    let defaults = UserDefaults.standard
//    let googleHandler:GmailLoginHandler = GmailLoginHandler.sharedInstance()
//    var selectedLoginType : SignInLoginType? = .normal
//    var fbData = UserDataModel()
//
//    var socialMediaEmail = ""
//    var socialMediaPassword = ""
//    var socialMediaId = ""
//
//
//
//    override func viewDidLoad() {
//
//        super.viewDidLoad()
//        //      self.generateToken()
//        self.navigationItem.setHidesBackButton(true, animated:true)
//        updateUI()
//        Helper.shadowView(sender: self.bottomView, width:UIScreen.main.bounds.width, height: 55)
//        self.textFieldDidChange(self.passwordTF)
//        self.textFieldDidChange(self.userNameTF)
//        self.passwordTF.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
//        self.userNameTF.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//
//
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//        view.addGestureRecognizer(tap)
//        if Helper.userDataAlreadyExist(kUsernameKey: USER_DEFAULTS.USER.USER_NAME) {
//            userNameTF.text = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.USER_NAME)
//        }
//        if Helper.userDataAlreadyExist(kUsernameKey: USER_DEFAULTS.USER.USER_PASSWORD) {
//            passwordTF.text = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.USER_PASSWORD)
//        }
//    }
//
//    func generateToken() {
//        let token = FIRInstanceID.instanceID().token()
//        print("InstanceID token: \(String(describing: token))")
//        let defaults = UserDefaults.standard
//        defaults.set(token!, forKey: USER_DEFAULTS.TOKEN.PUSH)
//        defaults.synchronize()
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//
//    }
//
//    override func didReceiveMemoryWarning() {
//
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    func dismissKeyboard() {
//
//        view.endEditing(true)
//    }
//
//
//    func updateUI() {
//
//        emailCheckMark.isHidden = true
//        passwordCheckMark.isHidden = true
//    }
//
//    /*****************************************************************/
//    //MARK: - Validations
//    /*****************************************************************/
//
//    func loginMethod() {
//        userNameTF.resignFirstResponder()
//        passwordTF.resignFirstResponder()
//        if  (userNameTF.text?.isEmpty)! {
//            self.present(Helper.alertVC(title: ALERTS.Missing as NSString , message:ALERTS.Email_Miss as NSString), animated: true, completion: nil)
//            //            Helper.showAlert(head: ALERTS.Missing, message: ALERTS.Email_Miss)
//            return
//        } else if(passwordTF.text?.isEmpty)! {
//            //            Helper.showAlert(head: ALERTS.Missing, message: ALERTS.Pasword)
//            self.present(Helper.alertVC(title: ALERTS.Missing as NSString , message:ALERTS.Pasword as NSString), animated: true, completion: nil)
//            return
//        } else if !Helper.isValidEmail(emailText: userNameTF.text!){
//            if !Helper.validatePhone(value: userNameTF.text!){
//                //                Helper.showAlert(head: ALERTS.Wrong, message: ALERTS.mail)
//                self.present(Helper.alertVC(title: ALERTS.Wrong as NSString , message:ALERTS.mail as NSString), animated: true, completion: nil)
//                return
//            }
//        }
//        self.sendRequestForLogin()
//    }
//
//
//
//
//    /*****************************************************************/
//    //MARK: - Keyboard Methods
//    /*****************************************************************/
//
//    //    func keyboardWillShow(notification: NSNotification) {
//    //
//    //        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
//    //        {
//    //            self .moveViewUp(activeTextField, andKeyboardHeight: Float(keyboardSize.height))
//    //        }
//    //    }
//    //
//    //    func keyboardWillHide(notification: NSNotification) {
//    //
//    //        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
//    //            self.moveViewDown()
//    //        }
//    //    }
//    func keyboardWillAppear(notification: NSNotification){
//        // Do something here
//
//        let info = notification.userInfo!
//        let inputViewFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//        let screenSize: CGRect = UIScreen.main.bounds
//        var frame = self.view.frame
//        frame.size.height = screenSize.height - inputViewFrame.size.height + 55
//        self.view.frame = frame
//    }
//
//    func keyboardWillDisappear(notification: NSNotification){
//        // Do something here
//        UIView.animate(withDuration: 0.2, animations: {() -> Void in
//            let screenSize: CGRect = UIScreen.main.bounds
//            var frame = self.view.frame
//            frame.size.height = screenSize.height
//            self.view.frame = frame
//        })
//    }
//
//    func moveViewDown() {
//
//        self.scrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
//    }
//
//    func moveViewUp(_ textfield: UIView, andKeyboardHeight height: Float) {
//
//        var  textFieldMaxY:Float = Float(textfield.frame.maxY)+10
//
//        var view: UIView? = textfield.superview
//        while view != self.view.superview {
//            textFieldMaxY += Float((view?.frame.minY)!)
//            view = view?.superview
//        }
//
//        let remainder: Float? = Float((self.view.frame.height)) - (textFieldMaxY + height)
//        if remainder! >= 0 {
//
//        } else {
//            UIView.animate(withDuration: 0.4, animations: {() -> Void in
//                self.scrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(-remainder!))
//            })
//        }
//    }
//
//
//    /*****************************************************************/
//    //MARK: - UIButton Actions
//    /*****************************************************************/
//
//    @IBAction func CancelButtonAction(_ sender: AnyObject) {
//
//        _ = self.navigationController?.popViewController(animated: true)
//    }
//
//    @IBAction func SigninButtonAction(_ sender: AnyObject) {
//
//        dismissKeyboard()
//        loginMethod()
//    }
//
//    @IBAction func forgetPasswordAction(_ sender: AnyObject) {
//
//        performSegue(withIdentifier: "ForgetPasswordSegue", sender: self)
//    }
//
//    @IBAction func tapGesture(_ sender: AnyObject) {
//
//        self.view.endEditing(true)
//    }
//
//    @IBAction func faceBookLoginAction(_ sender: AnyObject) {
//
//        fbHandler.delegate = self
//        fbHandler.login(withFacebook: self)
//    }
//
//    @IBAction func googleLoginAction(_ sender: AnyObject) {
//
//        selectedLoginType = .google
//        googleHandler.delegate = self
//        googleHandler.login(withGmail: self)
//    }
//
//    @IBAction func signupAction(_ sender: AnyObject) {
//
//        DispatchQueue.main.async {
//            self.performSegue(withIdentifier: "signupSegue", sender: self)
//        }
//    }
//
//    // MARK: - Navigation
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if segue.identifier == "ForgetPasswordSegue" {
//
//            let nav = segue.destination as! UINavigationController
//            if let forgetPass: ForgetPasswordViewController = nav.viewControllers.first as! ForgetPasswordViewController?
//            {
//                forgetPass.delegate = self
//            }
//        }else if segue.identifier == "signupSegue" {
//            let nav = segue.destination as! UINavigationController
//            let controller = nav.viewControllers.first as! SignUpViewController
//            if self.selectedLoginType != .normal {
//                controller.userData = fbData
//            }
//            controller.selectedLoginType = (selectedLoginType?.rawValue).map { SignUpLoginType(rawValue: $0) }!
//            controller.socialMediaId = socialMediaId
//            selectedLoginType = .normal
//        }
//    }
//
//    /*****************************************************************/
//    //MARK: - Service Methods
//    /*****************************************************************/
//
//    //Sender Request for Login
//
//    func sendRequestForLogin() {
//        //        self.generateToken()
//        Helper.showPI()
//
//        var emailString = ""
//        var passwordString = ""
//        if selectedLoginType == .normal {
//            emailString = userNameTF.text!
//            passwordString = passwordTF.text!
//        }
//        else {
//            emailString = socialMediaEmail
//            passwordString = socialMediaPassword
//        }
//
//        let params : [String : Any] =   ["ent_email_phone"   : emailString,
//                                         "ent_password"      : passwordString,
//                                         "ent_deviceId"      : DEVICE_TOKEN,
//                                         "ent_push_token"    : Utility.pushtoken,
//                                         "ent_appversion"    : Utility.appVersion,
//                                         "ent_devMake"       : "Apple",
//                                         "ent_devModel"      : Utility.deviceName,
//                                         "ent_devtype"       : Utility.userType,
//                                         "ent_device_time"   : Helper.currentDateTime,
//                                         "ent_login_type"    : selectedLoginType!.rawValue,
//                                         "ent_socialMedia_id":socialMediaId]  //facebook ID or Google ID
//
//        print("login request parameters :",params)
//        NetworkHelper.requestPOST(serviceName:API.METHOD.LOGIN,
//                                  params: params,
//                                  success: { (response : [String : Any]) in
//                                    print("login response : ",response)
//                                    Helper.hidePI()
//                                    if response.isEmpty == false {
//
//                                        if let errFlag: Int = response["errFlag"] as? Int {
//
//                                            if errFlag == 0 {
//
//                                                print("Login successful")
//
//                                                if let dict = response["data"] as? NSDictionary {
//
//                                                    let defaults = UserDefaults.standard
//                                                    defaults.set(dict["token"] as! String, forKey: USER_DEFAULTS.TOKEN.SESSION)
//                                                    defaults.set(self.selectedLoginType?.rawValue, forKey: USER_DEFAULTS.USER.LoginType)
//                                                    defaults.set(dict["presence_chn"] as! String, forKey: USER_DEFAULTS.CHANNEL.PRESENCE_CHANNEL)
//                                                    defaults.set(dict["pub_key"] as! String, forKey: USER_DEFAULTS.CHANNEL.PUBLISH_CHANNEL)
//                                                    defaults.set(dict["Name"] as! String, forKey: USER_DEFAULTS.USER.NAME)
//                                                    defaults.set(dict["mobile"] as! String, forKey: Utility.phone)
//                                                    defaults.set(dict["sub_key"] as! String, forKey: USER_DEFAULTS.CHANNEL.SUBSCRIBE_CHANNEL)
//                                                    defaults.set(dict["chn"] as! String, forKey: USER_DEFAULTS.CHANNEL.MY_CHANNEL)
//                                                    defaults.set(dict["server_chn"] as! String, forKey: USER_DEFAULTS.CHANNEL.SERVER_CHANNEL)
//                                                    defaults.set(dict["referralcode"] as! String, forKey: USER_DEFAULTS.USER.REFERAL)
//                                                    defaults.set(self.userNameTF.text!, forKey: USER_DEFAULTS.USER.USER_NAME)
//                                                    defaults.set(self.passwordTF.text!, forKey: USER_DEFAULTS.USER.USER_PASSWORD)
//
//                                                    defaults.set(dict["sid"] as! String, forKey:Utility.sid)
//                                                    defaults.synchronize()
//
//                                                    if Utility.myChannel.isEmpty == false {
//                                                        (VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper).subscribeToMyChannel()
//                                                    }
//
//                                                    let modalForProfile = ModalForProfile()
//                                                    modalForProfile.sendRequestForProfileType()
//                                                    DispatchQueue.main.async(execute: {
//                                                        let mainVcIntial = kConstantObj.SetIntialMainViewController(Storyboard_Identifier.HOME_VC)
//                                                        let window = UIApplication.shared.keyWindow
//                                                        window?.rootViewController = mainVcIntial
//                                                    })
//                                                }
//                                            }
//                                            else if errFlag == 1 {
//
//                                                if self.selectedLoginType != .normal {
////                                                    self.getTypeMakeNModel()
//                                                    //                                                    self.performSegue(withIdentifier: "signupSegue", sender: self)
//                                                    self.signupAction(UIButton())
//                                                    return
//                                                }else {
//                                                    self.present(Helper.alertVC(title: ALERTS.Message, message:response["errMsg"] as! NSString), animated: true, completion: nil)
//                                                }
//                                            }
//                                        } else {
//
//                                            if let statusCode: ERRORS = ERRORS(rawValue: (response["statusCode"] as? Int)!) {
//
//                                                if statusCode == .MISSING {
//
//                                                    self.present(Helper.alertVC(title: ALERTS.Message, message:response["message"] as! NSString), animated: true, completion: nil)
//                                                }
//                                            }
//                                        }
//                                    }
//        }) { (Error) in
//            self.present(Helper.alertVC(title: ALERTS.Message, message:Error.localizedDescription as NSString), animated: true, completion: nil)
//        }
//    }
//
//    /// Get Config Params
//    /// Such as Google Keys, Pubnub Keys, required details to Run and App
//    /// We get them from back office
//    fileprivate func configure() {
//        Config.shared.refresh { (success, config) in
//            if success {
//
//            }
//            else {
//                // Check Session expiry
//                if (config?.isExpired)! {
//                }
//            }
//        }
//    }
//}
//
//// MARK: - UITextFieldDelegate
//
//extension LoginViewController : UITextFieldDelegate {
//
//    //MARK: - Custom Methods -
//    func textFieldDidChange(_ textField: UITextField) {
//        if !(userNameTF.text?.isValidEmail)!{
//            if !Helper.validatePhone(value: userNameTF.text!) {
//                emailCheckMark.isHidden = true
//            }else{
//                emailCheckMark.isHidden = false
//            }
//        }else {
//            emailCheckMark.isHidden = false
//        }
//        if passwordTF.text?.length == 0 {
//            passwordCheckMark.isHidden = true
//        }else {
//            passwordCheckMark.isHidden = false
//        }
//    }
//
//
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//
//        activeTextField = textField
//        return true
//    }
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        //delegate method
//    }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
//
//
//        return true
//    }
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        //delegate method
//
//        return true
//    }
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        //delegate method
//
//        if textField .isEqual(userNameTF) {
//
//            passwordTF.becomeFirstResponder()
//        } else {
//
//            dismissKeyboard()
//        }
//        return true
//    }
//}
//
//extension LoginViewController : facebookLoginDelegate {
//
//    func didFacebookUserLogin(withDetails userInfo:NSDictionary) {
//        let modalData = UserDataModel()
//        fbData = modalData.initArray(str: userInfo as! [String : AnyObject])
//        print("FBDetails =",userInfo)
//        socialMediaPassword = ((userInfo["id"]! as AnyObject) as? String)!
//        socialMediaId = ((userInfo["id"]! as AnyObject) as? String)!
//        //        userNameTF?.text = (userInfo["email"]! as AnyObject) as? String
//        //        passwordTF?.text = (userInfo["id"]! as AnyObject) as? String
//        if let temp = userInfo["email"] as? String {
//            socialMediaEmail = temp
//        }else{
//            //            self.present(Helper.alertVC(title: ALERTS.Missing as NSString , message:ALERTS.NoEmailPermission as NSString), animated: true, completion: nil)
//            //            return
//            socialMediaEmail = socialMediaPassword
//        }
//        socialMediaPassword = ((userInfo["id"]! as AnyObject) as? String)!
//        socialMediaId = ((userInfo["id"]! as AnyObject) as? String)!
//
//        selectedLoginType = .facebook
//        sendRequestForLogin()
//    }
//    func didFailWithError(_ error: Error?) {
//
//    }
//    func didUserCancelLogin() {
//
//    }
//}
//
//
//extension LoginViewController : GmailLoginHandlerDelegate {
//
//    func didGetDetailsFromGoogle(email: String, name: String, token: String, userId: String, profile: GIDProfileData) {
//        let modalData = UserDataModel()
//        fbData = modalData.initArrayGoogle(str: profile, userIdReached: userId)
//        //        userNameTF.text = email
//        //        passwordTF.text = userId
//
//        socialMediaEmail = email
//        socialMediaPassword = userId
//        socialMediaId = userId
//
//        selectedLoginType = .google
//        self.sendRequestForLogin()
//    }
//}
//
//extension LoginViewController : ForgetPasswordViewControllerDelegate {
//    func changedPassword() {
//
//    }
//
//}

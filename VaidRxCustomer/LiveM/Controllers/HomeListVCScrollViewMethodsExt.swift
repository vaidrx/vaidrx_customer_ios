//
//  HomeListVCScrollViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

extension HomeListViewController:UIScrollViewDelegate {

    func updateScrollViewConstraint(){
        
        if SCREEN_HEIGHT! * 0.45 * CGFloat(self.arrayOfProvidersModel.count) > self.scrollView.bounds.size.height {

            scrollContentViewHeightConstraint.constant = (SCREEN_HEIGHT! * 0.45 * CGFloat(self.arrayOfProvidersModel.count)) - self.scrollView.bounds.size.height

        } else {

            self.scrollContentViewHeightConstraint.constant = 2
        }
        
//        self.scrollView.contentSize = CGSize.init(width: self.scrollView.bounds.size.width, height: self.view.bounds.size.height * 0.45 * CGFloat(self.arrayOfProvidersModel.count))
        
        
        self.scrollView.layoutIfNeeded()
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == self.scrollView {
            
            if !decelerate {
                
                pauseInvisibleYoutubeVideo()
            }
            
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.scrollView {
            
            pauseInvisibleYoutubeVideo()
            
        }
        
    }
    
    
    func pauseInvisibleYoutubeVideo(){
        
        var topRowValue = self.tableView.indexPathForRow(at: self.scrollView.contentOffset)?.row
        var bottomRowValue = self.tableView.indexPathForRow(at: CGPoint.init(x: 0, y:self.scrollView.contentOffset.y + self.scrollView.frame.size.height))?.row
        
        if arrayOfProvidersModel.count > 0 {
            
            if topRowValue == nil {
                
                topRowValue = 0
            }
            
            if bottomRowValue == nil {
                
                bottomRowValue = arrayOfProvidersModel.count - 1
            }
            
            DDLogDebug("\nTopRowValue = \(topRowValue!)\nBottomRowValue = \(bottomRowValue!)\n")
            
            for i in 0..<arrayOfProvidersModel.count {
                
                if i < topRowValue! || i > bottomRowValue! {
                    
                    DDLogDebug("\nrow = \(i)\n")
                    
                    if let musicianCell = self.tableView.cellForRow(at: IndexPath.init(row: i, section: 0)) as? HomeListTableViewCell {
                        
//                        musicianCell.youtubePlayerView.pauseVideo()
                    }
                }
                
            }

            
        }
        
        
        
    }
    
    func pauseAllYoutubeVideo(){
        
        for i in 0..<arrayOfProvidersModel.count {
            
            if let musicianCell = self.tableView.cellForRow(at: IndexPath.init(row: i, section: 0)) as? HomeListTableViewCell {
                    
//                musicianCell.youtubePlayerView.pauseVideo()
            }
        }
        
    }

}

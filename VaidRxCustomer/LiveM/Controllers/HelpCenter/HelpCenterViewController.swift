//
//  HelpCenterViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


class HelpCenterViewController: UIViewController {
    
    // MARK: - Outlets -
    // TextField
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var emailTextFiled: UITextField!
    
    @IBOutlet weak var subjectTextField: UITextField!
    
    //TextView
    @IBOutlet weak var messageTextView: UITextView!
    
    //Constraint
    @IBOutlet weak var loadingViewHeight: NSLayoutConstraint!
    
    //Views
    @IBOutlet weak var nameBottomView: UIView!
    
    @IBOutlet weak var emailBottomView: UIView!
    
    @IBOutlet weak var subjectBottomView: UIView!
    
    @IBOutlet weak var messageBottomView: UIView!
    
    @IBOutlet weak var sendBottomView: UIViewCustom!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var scrollContentView: UIView!
    
    @IBOutlet var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    
    // MARK: - Variable Decleratons -
    weak var delegate: LeftMenuProtocol?
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        Helper.editNavigationBar(navigationController!)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        setupGestureRecognizer()
        appDelegate?.keyboardDelegate = self
        initiallSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        initiallAnimations()
        updateScrollViewContent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        appDelegate?.keyboardDelegate = nil
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateScrollViewContent() {
        
        if((messageBottomView.frame.size.height + messageBottomView.frame.origin.y) + 110 > scrollView.frame.size.height) {
            
            scrollContentViewHeightConstraint.constant = (messageBottomView.frame.size.height + messageBottomView.frame.origin.y) + 110 - scrollView.frame.size.height;
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0
        }
        
    }

    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    // MARK: - Action methods -
    @IBAction func backAction(_ sender: Any) {
//        delegate?.changeViewController(LeftMenu.searchArtist)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendAction(_ sender: Any) {
        
        //Validate All fields
    }
    
}

extension HelpCenterViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension HelpCenterViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}


extension HelpCenterViewController: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        textView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
        
        messageBottomView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)
        
        if textView.text! == ALERTS.HelpCenterMessageBoxPlaceHolder {
            
            textView.text = ""
        }
        
        return true
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        self.scrollView.scrollRectToVisible(CGRect.init(x: self.scrollView.contentSize.width - 1, y: self.scrollView.contentSize.height - 80, width: 1, height: 1), animated: true)
        
    }

    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ALERTS.HelpCenterMessageBoxPlaceHolder || textView.text.length == 0 {
            
            textView.text = ALERTS.HelpCenterMessageBoxPlaceHolder
            textView.textColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8039215686, alpha: 1)
            
        } else {
            
            textView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
        }
        
         messageBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}


extension HelpCenterViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == nameTextField{
            //            animateButtonLoading(0)
            nameBottomView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)//UIColor.init(hex: "#3B5998")
        }else if textField == emailTextFiled{
            //            animateButtonLoading(0)
            emailBottomView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)//UIColor.init(hex: "#3B5998")
        }else if textField == subjectTextField{
            //            animateButtonLoading(0)
            subjectBottomView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)//UIColor.init(hex: "#3B5998")
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == nameTextField{
            if textField.text?.length != 0 {
                //                animateButtonLoading(1)
            }
            nameBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1) //UIColor.init(hex: "#BABABA")
        }else if textField == emailTextFiled{
            if textField.text?.length != 0 {
                //                animateButtonLoading(1)
            }
            emailBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
        }else if textField == subjectTextField{
            if textField.text?.length != 0 {
                //                animateButtonLoading(1)
            }
            subjectBottomView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == nameTextField{
            
            emailTextFiled.becomeFirstResponder()
            emailBottomView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)
            return false
        
        }else if textField == emailTextFiled{
            
            subjectTextField.becomeFirstResponder()
            subjectBottomView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)
            return false
            
        }else if textField == subjectTextField{
            
            messageTextView.becomeFirstResponder()
            messageBottomView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1)
            return false
        }
        
        return true
    }
}


extension HelpCenterViewController {
    
    /// Initiall SetUP For Animations
    func initiallSetup() {
        nameBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        emailBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        subjectBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        messageBottomView.transform = CGAffineTransform(scaleX: 0, y: 0)
        sendBottomView.transform = CGAffineTransform(translationX: 0, y: 1000)
    }
    
    /// Initiall Animations after the view did Appear
    func initiallAnimations() {
        UIView.animate(withDuration: 0.8, animations: {
            self.nameBottomView.transform = .identity
            self.emailBottomView.transform = .identity
            self.subjectBottomView.transform = .identity
            self.messageBottomView.transform = .identity
        }) { (true) in
            UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .allowAnimatedContent, animations: {
                self.sendBottomView.transform = .identity
            })
        }
    }
    
    
}

// MARK: - Keyboard Methods -

extension HelpCenterViewController:KeyboardDelegate {
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        let inputViewFrame: CGRect? = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        
        let inputViewFrameInView: CGRect = view.convert(inputViewFrame!, from: nil)
        
        let intersection: CGRect = scrollView.frame.intersection(inputViewFrameInView)
        let ei: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: intersection.size.height, right: 0.0)
        scrollView.scrollIndicatorInsets = ei
        scrollView.contentInset = ei
        
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
        }
        
    }
    
}



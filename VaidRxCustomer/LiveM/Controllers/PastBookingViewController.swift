//
//  PastBookingViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

class PastBookingViewController:UIViewController {
    
    @IBOutlet var navigationLeftButton: UIButton!
    
    @IBOutlet var eventIdLabel: UILabel!
    
    @IBOutlet var eventDateLabel: UILabel!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var scrollContentView: UIView!
    
    @IBOutlet var amountLabel: UILabel!
    
    @IBOutlet var musicianImageView: UIImageView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var musicianNameLabel: UILabel!
    
    @IBOutlet var evenNameLabel: UILabel!
    
    @IBOutlet var reviewLabel: UILabel!
    
    @IBOutlet var eventLocationLabel: UILabel!
    
    @IBOutlet var gigNameLabel: UILabel!
    
    @IBOutlet var gigValueLabel: UILabel!
    
    @IBOutlet var serviceFeeLabel: UILabel!
    
    @IBOutlet var serviceFeeValueLabel: UILabel!
    
    @IBOutlet var discountLabel: UILabel!
    
    @IBOutlet var discountValueLabel: UILabel!
    
    @IBOutlet var totalLabel: UILabel!
    
    @IBOutlet var totalValueLabel: UILabel!
    
    @IBOutlet var paymentTypeBackView: UIView!
    
    @IBOutlet var paymentTypeImageView: UIImageView!
    
    @IBOutlet var paymentTypeLabel: UILabel!
    
    @IBOutlet var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var paymentImageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var paymentImageViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var eventStatusLabel: UILabel!
    
    @IBOutlet weak var topStatusView: UIView!
    
    @IBOutlet weak var topStatusLabel: UILabel!
    
    
    var bookingDetailModel:BookingDetailsModel!
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!
    let pastBookingViewModel = PastBookingViewModel()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    
    // Shared instance object for gettting the singleton object
    static var obj:PastBookingViewController? = nil
    
    class func sharedInstance() -> PastBookingViewController {
        
        return obj!
    }
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PastBookingViewController.obj = self
        
        getBookingDetailsAPI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isTranslucent = false
        acessClass.acessDelegate = self
        setupGestureRecognizer()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        updateScrollContent()
    }
    
    func updateScrollContent() {
        
        if (paymentTypeBackView.frame.size.height + paymentTypeBackView.frame.origin.y) + 25 > scrollView.frame.size.height {
            
            scrollContentViewHeightConstraint.constant = (paymentTypeBackView.frame.size.height + paymentTypeBackView.frame.origin.y) + 25 - scrollView.frame.size.height;
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0
        }
        
        scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
        
        self.view.layoutIfNeeded()
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
    }

    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension PastBookingViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getParticularApptDetails.rawValue:
            
            getBookingDetailsAPI()
            
            break
            
        default:
            break
        }
    }
    
}

extension PastBookingViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension PastBookingViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}

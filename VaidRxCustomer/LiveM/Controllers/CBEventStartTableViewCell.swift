//
//  MDGigTimeTableVieCell.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class CBEventStartTableViewCell:UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var noEventStartTimeBackView: UIView!
    @IBOutlet var noEventStartTimeLabel: UILabel!
    
    
    var CBModel:ConfirmBookingModel!
    
    var isConfirmBooking:Bool = false
    
}

extension CBEventStartTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let gigTimeCell:MDGigTimeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "gigTimeCell", for: indexPath) as! MDGigTimeCollectionViewCell
        
        gigTimeCell.timeLabel.text = "IN \(30*(indexPath.row+1)) MIN."
        
        
        if CBModel.selectedEventStartTag == indexPath.row {
            
            gigTimeCell.timeLabel.textColor = UIColor.red
            gigTimeCell.selectedDivider.isHidden = false
            
            gigTimeCell.timeLabel.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            
            UIView.animate(withDuration: 0.8,
                           delay: 0.2,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 5,
                           options: [],
                           animations: {
                            
                            gigTimeCell.timeLabel.transform = .identity
                            
            }) { (success) in
                
                
            }
            
        } else {
            
            gigTimeCell.timeLabel.textColor = UIColor.black
            gigTimeCell.selectedDivider.isHidden = true

        }
        
        return gigTimeCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sizeOfEachCell:CGSize = CGSize.init(width: (collectionView.bounds.size.width - 30)/3.5, height: 50)
        
        return sizeOfEachCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.CBModel.selectedEventStartTag != indexPath.row {
                
            self.CBModel.selectedEventStartTag = indexPath.row
            self.collectionView.reloadData()
        }
        
    }

    
    
}


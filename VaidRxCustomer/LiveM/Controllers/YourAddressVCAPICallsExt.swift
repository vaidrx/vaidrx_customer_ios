//
//  YourAddressVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension YourAddressViewController {
    
    func getAddressAPI() {
        
        self.tableView.isHidden = true
        self.addNewAddressButtonBackView.isHidden = true
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        yourAddressViewModel.getListOfAddressesAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.GetAddress)
        }
        
    }
    
    func deleteAddressAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        let addressModel = arrayOfAddress[selectedAddressTag]
        
        yourAddressViewModel.addressIdToDelete = addressModel.addressId
        
        yourAddressViewModel.deleteAddressAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.deleteAddress)
        }

    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode {
            
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if let dataRes = dataResponse as? String {
                
                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
            
                self.apiTag = requestType.rawValue
            
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
                    case .GetAddress:
                    
                        progressMessage = PROGRESS_MESSAGE.Loading

                    case .deleteAddress:
                        
                        progressMessage = PROGRESS_MESSAGE.Deleting
                        
                    default:
                        
                        break
                }
                
                self.acessClass.getAcessToken(progressMessage: progressMessage)
            }
            
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
            }
            
            break
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType {
                
            case RequestType.GetAddress:
                
                if let dataRes:[Any] = dataResponse as? [Any] {
                    
                    arrayOfAddress = []
                    addressArrayFromCouchDB = dataRes
                    
                    updateDocumentDetailsToCouchDB()
                    
                    for addressDetail in dataRes {
                        
                        let addressDetailModel = AddressModel.init(manageAddressDetails: addressDetail)
                        arrayOfAddress.append(addressDetailModel)
                    }
                    
                    self.tableView.isHidden = false
                    self.addNewAddressButtonBackView.isHidden = false
                    
                    isAddressListChanged = false
                    
                    self.tableView.reloadData()
                    
                    self.showSpringAnimation()
                }
                break
                
            case RequestType.deleteAddress:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                arrayOfAddress.remove(at: selectedAddressTag)
                addressArrayFromCouchDB.remove(at: selectedAddressTag)
                updateDocumentDetailsToCouchDB()
                self.tableView.reloadData()
                
            default:
                break
            }
            
            break
            
        default:
            
            if errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
            }
            
            
            break
        }
        
    }
    
    func showSpringAnimation(){
        
        addNewAddressButtonBackView.transform = CGAffineTransform(translationX: 0, y: 100)
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 3,
                       options: [],
                       animations: {
                        
                        self.addNewAddressButtonBackView.transform = .identity
                        
        }, completion: { (completed) in
            
            
        })
        
    }
    
}

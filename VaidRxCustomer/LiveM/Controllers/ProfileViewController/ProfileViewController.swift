//
//  ProfileViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

var ispharEdited = false
var pharName = ""
var pharId = ""

class ProfileViewController: UIViewController, AccessTokeDelegate,SelectedPharmacyDelegate,AddMyAddressFromMapToModel {
    
    // MARK: - Outlet -
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var scrollContentView: UIView!
    
    @IBOutlet weak var profilePhoto: UIImageView!
    
    @IBOutlet weak var countryImage: UIImageView!
    
    @IBOutlet weak var firstNameBottomView: UIView!
    @IBOutlet weak var lastNameBottomView: UIView!
    @IBOutlet weak var aboutMeBottomView: UIView!
    @IBOutlet weak var musicGenreBottomView: UIView!
    @IBOutlet weak var emailBottomView: UIView!
    @IBOutlet weak var phoneNumBottomView: UIView!
    @IBOutlet weak var dobBottomView: UIView!
    @IBOutlet weak var paymentMethodBottomView: UIView!
    @IBOutlet weak var addressBottomView: UIView!
    
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var logOutButton: UIButton!
    
    @IBOutlet weak var changeButton: UIButtonCustom!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var dobButton: UIButton!
    @IBOutlet weak var imageButtonOutlet: UIButton!
    @IBOutlet weak var musicGenreButton: UIButton!
    @IBOutlet weak var countryPickerButton: UIButton!
    
    
    @IBOutlet weak var photoActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var changePasswordBGViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dobImageHeight: NSLayoutConstraint!
    
   // @IBOutlet weak var aboutMeCountHeight: NSLayoutConstraint!
    @IBOutlet weak var musicGenreButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var countryPickerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imageButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var dobPickerBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var musicGenreTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var pharmacyTextField: UITextField!
    @IBOutlet weak var billingAddressTextField: UITextField!
    @IBOutlet weak var aboutMeTextView: UITextView!
    
    
    @IBOutlet weak var aboutMeCountLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var pickerDateLabel: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var pharmacyLabel: UILabel!
    @IBOutlet weak var birthdayPicker: UIDatePicker!
    
    
    @IBOutlet var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var newDatePicker: UIDatePicker!
    @IBOutlet weak var newDatePickerView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var datePickerBackView: UIView!
    @IBOutlet weak var changeEmailButton: UIButton!
    @IBOutlet weak var changePhoneNumButton: UIButton!
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var pharmacyButton: UIButton!
    
    var getPharmacyViewModel = GetPharmacyViewModel()
    
//    var pharmacyData : PharmacyResponseModel!
//    var pharmacyItems = Array<PharmacyItems>()
//    var profileResponseModel: ProfileResponseModel!
//    
//    var selectedPharmacyDelegate : SelectedPharmacyDelegate?
//    var registerViewModel: RegisterViewModel!
    
    // MARK: - Variable Decleration -
    
    var rotationAngle: CGFloat!
    var refreshMonthCount : Int!
    var monthI: Int!
    var croppingEnabled: Bool = false
    var libraryEnabled: Bool = true
    let catransitionAnimationClass = CatransitionAnimationClass()
    var selectedCountry: Country!
    var activeField: UITextField?
    
    var countriesFiltered = [Country]()
    weak var delegate: LeftMenuProtocol?
    var profilePicURL: String!
    var musicGenreApi: String = ""
    var landingViewController: UIViewController!
    let acessClass = AccessTokenRefresh()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var apiTag: Int!
    var profileAlternatePhoto = 0
    var profileResponseModel: ProfileResponseModel? = nil
    var isEmail: Bool = false
    var musicGenre: Bool = false
    var genresArray = [MusicGenreModel]()
    var selectedDOB:Date!
    
    let profileViewModel = ProfileViewModel()
    var profileRequestModel : ProfileRequestModel!
    let disposeBag = DisposeBag()

    let imagePicker = UIImagePickerController()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    func addMyAddress(address: String) {
        //
    }
    
    func addFullAddress(fulladdress: [String : Any]) {
        print("Full adress",fulladdress)
        
        self.billingAddressTextField.text = fulladdress["address"] as? String
        
        profileViewModel.longitude = fulladdress["logitude"] as! Double
        profileViewModel.latitude = fulladdress["latitude"] as! Double
        profileViewModel.addLine1 = fulladdress["address"] as! String
        profileViewModel.taggedAs = fulladdress["taggedAs"] as! String
        profileViewModel.city = fulladdress["city"] as! String
        profileViewModel.state = fulladdress["state"] as! String
        profileViewModel.country = fulladdress["country"] as! String
        profileViewModel.pincode = fulladdress["pincode"] as! String
//        serviceRequestToUpdateProfile()
        serviceRequestToUpdateAddress()
        
    }
    
    func addCorrectAddress(correctAddress: AddressModel,taggedAs:String){
        print("address contailn model",correctAddress)
        
        profileViewModel.latitude = Double(correctAddress.latitude)
        profileViewModel.longitude = Double(correctAddress.longitude)
        profileViewModel.addLine1 = correctAddress.addressLine1
        profileViewModel.addLine2 = correctAddress.addressLine2
        profileViewModel.city = correctAddress.city
        profileViewModel.state = correctAddress.state
        profileViewModel.pincode = correctAddress.zipcode
        profileViewModel.country = correctAddress.country
        profileViewModel.placeName = correctAddress.addressLine1
        profileViewModel.taggedAs = taggedAs
        profileViewModel.placeId = correctAddress.addressId
        profileViewModel.country = correctAddress.country
//        profileViewModel.placeId = correctAddress.
        if correctAddress.addressLine2.contains(correctAddress.addressLine1) {
            self.billingAddressTextField.text = correctAddress.addressLine2
        }
        else {
            self.billingAddressTextField.text = correctAddress.addressLine1 + correctAddress.addressLine2
        }
    
//        serviceRequestToUpdateProfile()
        serviceRequestToUpdateAddress()
        
        print("got full address")
    }
    
    // MARK: - Default Class Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ispharEdited = false
        newDatePickerView.isHidden = true
        pharName = ""
        pharId = ""
//        self.aboutMeBottomView.frame.origin.y + self.aboutMeBottomView.frame.size.height 
        self.profilePhoto.image = PROFILE_DEFAULT_IMAGE
        
        acessClass.acessDelegate = self
        
//        Helper.editNavigationBar(navigationController!)
        initiallSetupForAnimation()
        animations()
        photoActivityIndicator.isHidden = true
        profilePhoto.clipsToBounds = true
        
        self.title = ""
        
        if !musicGenre {
            
            sendServiceToGetProfile()
            musicGenre = false
            
        }
        
       // birthdayPicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
       // selectedDOB = birthdayPicker.date
        
     //   dateChanged(birthdayPicker)
        UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.PAYMENT_TYPE_CHANGED)
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    @IBAction func dobAction(_ sender: UIButton) {
//
//        newDatePickerView.isHidden = false
//
//    }
    @IBAction func DoneButtonAction(_ sender: UIButton) {
        
        newDatePickerView.isHidden = true
    }
    
    
    @IBAction func ChangeDateAction(_ sender: UIDatePicker) {
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        dobLabel.text = dateFormat.string(from: sender.date)
        selectedDOB = sender.date
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if ispharEdited {
            pharmacyTextField.text = pharName
        }
        
        appDelegate?.keyboardDelegate = self
        appDelegate?.accessTokenDelegate = self
        
//        Helper.hideNavBarShadow(vc: self)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view?.backgroundColor = UIColor.clear //UIColor(hex: "05C1C9")
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear //UIColor(hex: "05C1C9")
        
        USER_DEFAULTS.API_CALL.profileScreen = false
//        sendServiceToGetProfile()
        addObserveToVariables()
        
        imagePicker.delegate = self
        
//        setupGestureRecognizer()
        
        isEmail = false
        checkPaymentType()
        
    }
    
    
        
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (musicGenreBottomView.frame.size.height + musicGenreBottomView.frame.origin.y) + 100 > scrollView.frame.size.height {
            
            scrollContentViewHeightConstraint.constant = (musicGenreBottomView.frame.size.height + musicGenreBottomView.frame.origin.y) + 100 - scrollView.frame.size.height;
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0
        }
        
        scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
        
        self.view.layoutIfNeeded()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
//        self.navigationController?.view?.backgroundColor = UIColor.clear //UIColor(hex: "05C1C9") //UIColor.white //05C1C9
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear //UIColor(hex: "05C1C9") //UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()

        
        self.view.endEditing(true)
        
        appDelegate?.keyboardDelegate = nil
        appDelegate?.accessTokenDelegate = nil
        acessClass.acessDelegate = nil
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        pickerDateLabel.text = dateFormat.string(from: sender.date)
        
    }
    
    func checkPaymentType() {
        let paymentData = UserDefaults.standard.bool(forKey: USER_DEFAULTS.USER.PAYMENT_TYPE_CHANGED)
        if paymentData  {
            let isInsurance = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.PAYMENTTYPE)
           musicGenreTextField.text = UserDefaults.standard.string(forKey:  USER_DEFAULTS.USER.INSURANCE)
            if (isInsurance)! == "Insurance"{
                if  UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.INSURANCEID)! != "" {
                        profileViewModel.insurance = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.INSURANCEID)!
                    paymentTypeMethod = PaymentType.Insurance.rawValue
                    profileViewModel.paymentType = PaymentType.Insurance
                }
                else {
                    profileViewModel.insurance = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.INSURANCE)!
                    profileViewModel.paymentType = PaymentType.CustomInsurance
                }
                    
//                profileViewModel.paymentType = PaymentType.Insurance
            } else {
              musicGenreTextField.text = "Self pay"
              paymentTypeMethod = PaymentType.SelfPay.rawValue
            }
            
        }
        
    }
        
        
    
    /// to recall the api
    @objc func recallApi() {
        switch apiTag {
        case RequestType.signOut.rawValue:
            logOut()
        case RequestType.getProfileData.rawValue:
            sendServiceToGetProfile()
        case RequestType.updateProfileData.rawValue:
            serviceRequestToUpdateProfile()
        default:
            break
        }
    }
    
    func addObserveToVariables() {
        
        nameTextField.rx.text
            .orEmpty
            .bind(to: profileViewModel.firstNameText)
            .disposed(by: disposeBag)
        
        lastNameTextField.rx.text
            .orEmpty
            .bind(to: profileViewModel.lastNameText)
            .disposed(by: disposeBag)
        
        emailTextField.rx.text
            .orEmpty
            .bind(to: profileViewModel.emailText)
            .disposed(by: disposeBag)
        
        musicGenreTextField.rx.text
            .orEmpty
            .bind(to: profileViewModel.musicGenereText)
            .disposed(by: disposeBag)
        
        mobileNumberTextField.rx.text
            .orEmpty
            .bind(to: profileViewModel.phoneNumberText)
            .disposed(by: disposeBag)
        
        
//        aboutMeTextView.rx.text
//            .orEmpty
//            .bind(to: profileViewModel.aboutMeText)
//            .disposed(by: disposeBag)
    
    }

    
}



extension ProfileViewController:KeyboardDelegate {
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        let inputViewFrame: CGRect? = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        let inputViewFrameInView: CGRect = view.convert(inputViewFrame!, from: nil)
        let intersection: CGRect = scrollView.frame.intersection(inputViewFrameInView)
        let ei: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: intersection.size.height, right: 0.0)
        scrollView.scrollIndicatorInsets = ei
        scrollView.contentInset = ei
        
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
        }
        
    }
    
}

// MARK: - CountrySelectedDelegate -
extension ProfileViewController: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImage.image = UIImage(named: imagePath)
        
        countryCodeLabel.text =  self.selectedCountry.dial_code
    }
    
    /// Filter Country Name
    ///
    /// - Parameter searchText: To search text in country name
    func filtercountry(_ searchText: String) {
        let picker = CountryNameViewController()
        picker.initialSetUp()
        let contry: Country
        contry = picker.localCountryName(searchText)
        let imagestring = contry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImage.image = UIImage(named: imagePath)
    }
    
}

extension ProfileViewController: MusicGenreViewDelegate {
    
    func setGenresData(data: [MusicGenreModel]) {
        
        genresArray = []
        
        var displayGenresName: String = ""
        var displayGenresId: String = ""
        
        for eachGenresDataModel in data {
            
            genresArray.append(eachGenresDataModel)
            
            if displayGenresName.length == 0 {
                
                displayGenresName = "\(eachGenresDataModel.musicGenreName)"
                displayGenresId = "\(eachGenresDataModel.musicGenreId)"
                
            }else {
                
                displayGenresName = "\(displayGenresName), \(eachGenresDataModel.musicGenreName)"
                displayGenresId = "\(displayGenresId),\(eachGenresDataModel.musicGenreId)"
            }
        }
        musicGenreApi = displayGenresId
        musicGenreTextField.text = displayGenresName
    }
    
}

extension ProfileViewController: changeEmailMobileDelegate {
    func recallApi(sucess: Bool) {
        if sucess {
            sendServiceToGetProfile()
        }
    }
}

extension ProfileViewController: setPasswordDelegate {
    func passwordUpdated(sucess: Bool) {
        if sucess {
            sendServiceToGetProfile()
        }
    }
}

extension ProfileViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
      
//        aboutMeTextView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
//
//        if aboutMeTextView.text! == ALERTS.aboutMe {
//
//            aboutMeTextView.text = ""
//        }
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
//        aboutMeTextView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
//
//        if aboutMeTextView.text! == ALERTS.aboutMe {
//
//            aboutMeTextView.text = ""
//        }
        
        return true

    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.text.length > 250 {
            
            if text == "" {
                adjustUITextViewHeight(textView: textView)
                return true
            }
            
            return false
            
        } else if text == "\n" {
            
            textView.resignFirstResponder()
            return false
        }
        
        adjustUITextViewHeight(textView: textView)
        return true
    }

    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ALERTS.aboutMe || textView.text.length == 0 {
            
            textView.text = ALERTS.aboutMe
            //aboutMeTextView.textColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8039215686, alpha: 1)
            
        } else {
            
            //aboutMeTextView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
        }
        
    }
    
}


extension ProfileViewController {
    func uploadProfileImageToAmazon() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        Helper.showPI(_message: PROGRESS_MESSAGE.Saving)
        if profileAlternatePhoto == 0 ||  profileAlternatePhoto == 2 {
            profilePhoto.image  = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        AmazonManager.sharedInstance().upload(withImage: profilePhoto.image!,
                                              imgPath: "ProfileImages/") { (result, imageUrl) in
//                                                Helper.hidePI()
                                                self.profilePicURL = imageUrl
                                                self.updateUserDefault()
                                                self.serviceRequestToUpdateProfile()
        }
    }
}

extension ProfileViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if scrollView.contentOffset.y >= 64 {
            
            //            navigationController?.navigationBar.isTranslucent = false
            //            navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
            //            navigationController?.view?.backgroundColor = UIColor.white
            //            navigationController?.navigationBar.backgroundColor = UIColor.white
            //
            //            self.title = nameTextField.text
            
        } else {
            
            //            navigationController?.navigationBar.isTranslucent = true
            //            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            //            navigationController?.view?.backgroundColor = UIColor.clear
            //            navigationController?.navigationBar.backgroundColor = UIColor.clear
            //
            //            self.title = ""
        }
        
    }
    
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(info)
        if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
            profilePhoto.image = chosenImage
            profilePhoto.layer.borderWidth = 1
            profilePhoto.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x67AFF7).cgColor
    //            RED_COLOR.cgColor
            profileAlternatePhoto = 1
         //self.uploadPriscription(data: self.selectImage)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
}

/*extension ProfileViewController {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
//        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
    
}

extension ProfileViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}*/



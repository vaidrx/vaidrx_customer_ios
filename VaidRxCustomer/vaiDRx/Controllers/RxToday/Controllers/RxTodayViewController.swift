//
//  RxTodayViewController.swift
//  GoTasker
//
//  Created by 3Embed on 29/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class RxTodayViewController: UIViewController {
    
    
    @IBOutlet weak var popOverView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var pharmacyName: UILabel!
    @IBOutlet weak var transferMedicinsButton: UIButton!
    @IBOutlet weak var refillCheckButton: UIButton!
    
    let profileViewModel = ProfileViewModel()
    var profileResponseModel: ProfileResponseModel? = nil
    
    var rxToadyViewModel = RxToadyViwModel()
    var transferMedication = 0
    var refillMedications = 0
    var transferMedicationYesOrNo = 0
    var refillMedicationsYesOrNo = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sendServiceToGetProfile()
        
        popOverView.layer.cornerRadius = 10
        confirmButton.layer.cornerRadius = confirmButton.frame.height / 2
//        pharmacyName.text = UserDefaults.standard.string(forKey: USER_DEFAULTS.USER.PHARMACY_NAME)!
        // Do any additional setup after loading the view.
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
    }
    
    @IBAction func CANCELBUTTONACTION(_ sender: UIButton) {
        //self.navigationController?.popViewController(animated: true)
       dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        
//        
        if transferMedication == 1 {
            alertMessageForTransferMedications()
        }
        else if refillMedications == 1 {
            alertMessageForRefillMedications()
        }
        else {
            confirmButtonAlertMessage()
        }
    }
    
    @IBAction func transferMedicinsAction(_ sender: UIButton) {
        if transferMedication == 0 {
            transferMedication = 1
            transferMedicinsButton.setImage(UIImage(named: "check"), for: .normal)
            refillMedications = 0
            refillCheckButton.setImage(UIImage(named: "unCheck"), for: .normal)
            
        }
        else{
            transferMedication = 0
            transferMedicinsButton.setImage(UIImage(named: "unCheck"), for: .normal)
        }
    }
    
    @IBAction func refillMedicButtonAction(_ sender: UIButton) {
//        self.popOverView.isHidden = true
//        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TransferCurtMedVC") as! TransferCurrtMedViewController
//
//        popOverVC.modalPresentationStyle = .overCurrentContext
//        popOverVC.modalTransitionStyle = .crossDissolve
//
//
//        popOverVC.providesPresentationContextTransitionStyle = true
//        popOverVC.definesPresentationContext = true
//
//        popOverVC.view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
//
//        present(popOverVC, animated: true, completion: nil)
        
        if refillMedications == 0 {
            refillMedications = 1
            refillCheckButton.setImage(UIImage(named: "check"), for: .normal)
            transferMedication = 0
            transferMedicinsButton.setImage(UIImage(named: "unCheck"), for: .normal)
            
        }
        else{
            refillMedications = 0
            refillCheckButton.setImage(UIImage(named: "unCheck"), for: .normal)
        }
    }
    
    
    func alertMessageForTransferMedications(){
        DispatchQueue.main.async {[weak self] in
            
            let alertController = UIAlertController(title: "", message: "Would you like to transfer all your medication(s) to vaiDRx pharmacy at no extra charge?", preferredStyle: .alert)
            
            // Initialize Actions
            let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) -> Void in
                print("ayes")
                
                self?.transferMedicationYesOrNo = 1
                
                // MARK : - Dismiss the modally presented viewcontroller before navigating. when you popup from next viewcontrollers you get refference to root view controller
                self?.dismiss(animated: false, completion: nil)
                
                // MARK : - perform segue after dissmissing this popover viewcotroller
                self?.performSegue(withIdentifier: "ToPastPharmacy", sender: self)
            }
            let noAction = UIAlertAction(title: "No", style: .default) { (action) -> Void in
                print("No")
                
                self?.transferMedicationYesOrNo = 0
                // MARK : - Dismiss the modally presented viewcontroller before navigating. when you popup from next viewcontrollers you get refference to root view controller
                self?.dismiss(animated: false, completion: nil)
                
                // MARK : - perform segue after dissmissing this popover viewcotroller
                self?.performSegue(withIdentifier: "ToPastPharmacy", sender: self)
            }
//            let noAction = UIAlertAction(title: "No", style: .default,handler:nil)
            
            // Add Actions
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            
            // Present Alert Controller
            self?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func alertMessageForRefillMedications(){
        DispatchQueue.main.async {[weak self] in
            
            let alertController = UIAlertController(title: "", message: " Moving forward, do you want to authorize VaidRx pharmacy to refill your prescriptions  automatically.", preferredStyle: .alert)
            
            // Initialize Actions
            let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) -> Void in
                print("yes")
                self?.refillMedicationsYesOrNo = 1
                self?.refillCurrentMedications()
//                self?.smsAlertMessage()
                
                // MARK : - Dismiss the modally presented viewcontroller before navigating. when you popup from next viewcontrollers you get refference to root view controller
//                self?.dismiss(animated: false, completion: nil)
                
                // MARK : - perform segue after dissmissing this popover viewcotroller
//                self?.performSegue(withIdentifier: "ToPastPharmacy", sender: self)
            }
            let noAction = UIAlertAction(title: "No", style: .default) { (action) -> Void in
                print("No")
                self?.refillMedicationsYesOrNo = 0
                self?.refillCurrentMedications()
                
            }
//            let noAction = UIAlertAction(title: "No", style: .default,handler:nil)
            
            // Add Actions
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            
            // Present Alert Controller
            self?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func smsAlertMessage(){
        DispatchQueue.main.async {[weak self] in
            
            self?.popOverView.isHidden = true
            
            let alertController = UIAlertController(title: " ", message: "A message has been sent to your selected pharmacy for refill", preferredStyle: .alert)
            
            // Initialize Actions
            let yesAction = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                print("yes")
                
                
                
                // MARK : - Dismiss the modally presented viewcontroller before navigating. when you popup from next viewcontrollers you get refference to root view controller
                self?.dismiss(animated: false, completion: nil)
                
                // MARK : - perform segue after dissmissing this popover viewcotroller
                //                self?.performSegue(withIdentifier: "ToPastPharmacy", sender: self)
            }
            
//            let noAction = UIAlertAction(title: "No", style: .default,handler:nil)
            
            // Add Actions
            alertController.addAction(yesAction)
//            alertController.addAction(noAction)
            
            // Present Alert Controller
            self?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func confirmButtonAlertMessage(){
        DispatchQueue.main.async {[weak self] in
            
            let alertController = UIAlertController(title: "Alert", message: "Select medication transfer option", preferredStyle: .alert)
            
            // Initialize Actions
            let yesAction = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                print("yes")
                
                
                
                // MARK : - Dismiss the modally presented viewcontroller before navigating. when you popup from next viewcontrollers you get refference to root view controller
//                self?.dismiss(animated: false, completion: nil)
                
                // MARK : - perform segue after dissmissing this popover viewcotroller
                //                self?.performSegue(withIdentifier: "ToPastPharmacy", sender: self)
            }
            
            //            let noAction = UIAlertAction(title: "No", style: .default,handler:nil)
            
            // Add Actions
            alertController.addAction(yesAction)
            //            alertController.addAction(noAction)
            
            // Present Alert Controller
            self?.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToPastPharmacy" {
            let navVC = segue.destination as? UINavigationController
            let dstVC = navVC?.viewControllers.first as! PastPharmacyInfoViewController
//            if let dstVC = segue.destination as? PastPharmacyInfoViewController {//PastPharmacyInfoViewController
                dstVC.oldPharmacyTransferCheck = transferMedicationYesOrNo //transferMedication
//            }
//            let dstVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PastPharmInfoVC") as! PastPharmacyInfoViewController
//            dstVC.oldPharmacyTransferCheck = transferMedication
        }
    }

}

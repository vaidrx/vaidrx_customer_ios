//
//  VerifyPhoneAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 23/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class VerifyPhoneAPI {
    
    let disposebag = DisposeBag()
    let resendOTP_Response = PublishSubject<APIResponseModel>()
    let registerVerifyOTP_Response = PublishSubject<APIResponseModel>()
    let forgotPasswordVerifyOTP_Response = PublishSubject<APIResponseModel>()
    
    
    
    /// Method to call Resend OTP Service Call
    ///
    /// - Parameters:
    ///   - triggerValue: integer value (1 - Register,2 - Forgot Password,3-change number)
    ///   - userId: customer id to resend OTP
    func resendOTPServiceAPICall(triggerValue:Int,
                                 userId:String) {
        
        let strURL = API.BASE_URL + API.METHOD.RESEND_OTP
        
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.OTP.userId: userId,
            SERVICE_REQUEST.OTP.userType:1,
            SERVICE_REQUEST.OTP.trigger:triggerValue
        ]
        
               
        
        Helper.showPI(_message: PROGRESS_MESSAGE.SendingOTP)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.signUpOTP)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to call Verify OTP Service Call in Register flow
    ///
    /// - Parameters:
    ///   - verificationCode: user entered verification code
    ///   - userId: customer id to verify
    func registerVerifyOTPServiceAPICall(verificationCode:String,
                                                    userId:String) {
        
        let strURL = API.BASE_URL + API.METHOD.SIGNUP_VERIFYI_OTP
        
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.OTP.code:verificationCode,
            SERVICE_REQUEST.OTP.userId: userId
        ]
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.VerifyingOTP)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: nil)
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.verifyOTP)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to call verify OTP Service Call in forgotPassword and change password flow
    ///
    /// - Parameters:
    ///   - verificationCode: user entered verification code
    ///   - triggerValue: integer value (2 - Forgot Password,3-change number)
    ///   - userId: customer id to verify OTP
    func forgotPasswordVerifyOTPServiceAPICall(verificationCode:String,
                                               triggerValue:Int,
                                               userId:String) {
        
        let strURL = API.BASE_URL + API.METHOD.FORGET_PASSWORD_VERIFY_OTP
        
        let requestParams:[String:Any] = [
//            SERVICE_REQUEST.OTP.userType:1,
            SERVICE_REQUEST.OTP.code:verificationCode,
            SERVICE_REQUEST.OTP.userId: userId,
            SERVICE_REQUEST.OTP.trigger:triggerValue
        ]
        
            
        Helper.showPI(_message: PROGRESS_MESSAGE.VerifyingOTP)
        
        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.forgotPasswordVerifyOTP)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
            case .signUpOTP:
                
                self.resendOTP_Response.onNext(responseModel)
                
            case .verifyOTP:
                
                self.registerVerifyOTP_Response.onNext(responseModel)
                
            case .forgotPasswordVerifyOTP:
                
                self.forgotPasswordVerifyOTP_Response.onNext(responseModel)
                
            default:
                break
            }
            
            break
        }
    }
    
}

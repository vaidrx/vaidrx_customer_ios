//
//  ChatVCImageMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 12/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension ChatViewController {
    
    //MARK: - open Photo Gallary
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePickerObj.allowsEditing = true
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    //MARK: - Open Camera
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerController.SourceType.camera
            imagePickerObj.cameraCaptureMode = .photo
            imagePickerObj.allowsEditing = true
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
        }
    }
    
    func adjustFrames() {
        inputText.layoutIfNeeded()
        heightOFContainer.constant = inputText.contentSize.height
        inputText.layoutIfNeeded()
    }
    
    
    //************ Resize the width and height of image *****************//
    
    func resizeImage(image: UIImage) -> UIImage {
        
        var actualHeight  = image.size.height;
        var actualWidth = image.size.width;
        let maxHeight = 600.0;
        let maxWidth = 800.0;
        var imgRatio  = actualWidth  / actualHeight
        let maxRatio = maxWidth / maxHeight
        let compressionQuality = 0.5;
        
        if ( Double(actualHeight) > maxHeight) || ( Double(actualWidth) > maxWidth){
            if(Double(imgRatio) < maxRatio){
                
                imgRatio = CGFloat(maxHeight / Double(actualHeight));
                actualWidth = imgRatio * actualWidth;
                actualHeight = CGFloat(maxHeight);
            }else if (Double(imgRatio) > maxRatio){
                imgRatio = CGFloat(maxWidth / Double(actualWidth));
                actualHeight = imgRatio * actualHeight;
                actualWidth = CGFloat(maxWidth);
            }else{
                actualHeight = CGFloat(maxHeight);
                actualWidth = CGFloat(maxWidth);
            }
        }
        let rect = CGRect(x:0.0,y:0.0,width:actualWidth,height:actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
}


//MARK: - Imagepicker delegate -
extension ChatViewController {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage.rawValue] as? UIImage {
            self.composeMessage(type: .photo, content: self.resizeImage(image: pickedImage))
        } else {
            let pickedImage = info[UIImagePickerController.InfoKey.originalImage.rawValue] as! UIImage
            self.composeMessage(type: .photo, content:  self.resizeImage(image: pickedImage))
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }

}

extension UIImage {
    
    func imageWithColor(tintColor: UIColor) -> UIImage {
        
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        tintColor.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
        
    }
}

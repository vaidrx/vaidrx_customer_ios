//
//  TermsAndConditionViewController.swift
//  LiveM
//
//  Created by Apple on 18/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class TermsAndConditionViewController:UIViewController {
    
    @IBOutlet weak var termsAndConditionBackView: UIView!
    
    @IBOutlet weak var termsAndConditionButton: UIButton!
    
    @IBOutlet weak var privacyPolicyBackView: UIView!
    
    @IBOutlet weak var privacyPolicyButton: UIButton!
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupGestureRecognizer()
    }
    
    // MARK: - Custom Methods -
    func initiallSetup(){
        
        termsAndConditionBackView.layer.borderColor = TEXTFIELD_PLACEHOLDER_DEFAULT_COLOR.cgColor
        privacyPolicyBackView.layer.borderColor = TEXTFIELD_PLACEHOLDER_DEFAULT_COLOR.cgColor
    }
    
    
    // MARK: - Action Methods -
    @IBAction func backButtonAction(_ sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func termsAndConditionButtonAction(_ sender: Any) {
        
        performSegue(withIdentifier: SEgueIdetifiers.termsOfServiceToWebView, sender: "TermsAndCondition")
    }
    
    @IBAction func privacyPolicyButtonAction(_ sender: Any) {
        
        performSegue(withIdentifier: SEgueIdetifiers.termsOfServiceToWebView, sender: "PrivacyPolicy")
    }
    
    // MARK: - Segue Method -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEgueIdetifiers.termsOfServiceToWebView {
            
            if let termsWebView = segue.destination as? TermsWebViewController {
                
                if (sender as! String) == "TermsAndCondition" {
                    
                    termsWebView.isForTermsAndConditions = true
                    termsWebView.webURL = Links.TermsAndCondition
                    
                } else {
                    
                    termsWebView.isForTermsAndConditions = false
                    termsWebView.webURL = Links.PrivacyPolicy
                }
                
            }
        }
    }
    
}

extension TermsAndConditionViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension TermsAndConditionViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}

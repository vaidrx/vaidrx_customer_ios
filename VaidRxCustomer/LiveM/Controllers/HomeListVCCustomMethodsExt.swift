//
//  HomeListVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension HomeListViewController {
    
    func initializeMQTTMethods(initial:Bool) {
        
        musiciansListManager.delegate = self
        musiciansListManager.navigation = navigationController
        musiciansListManager.viewController = self
        musiciansListManager.timeInterval = Double(ConfigManager.sharedInstance.customerPublishLocationInterval)
        
        if isAddressChanged {

            isAddressChanged = false
            Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
            if appointmentLocationModel.bookingType == BookingType.Schedule {
                
                musiciansListManager.publish(isIinitial: false)
                
            } else {
                
                musiciansListManager.publish(isIinitial: true)
            }
            
        } else {
            
            if initial {
                
                musiciansListManager.publish(isIinitial: true)
                
            } else {
                
                musiciansListManager.publish(isIinitial: false)
            }
        }
        
        

    }
    
    func uninitializeMQTTMethods() {
        
        musiciansListManager.stopPublish()
        musiciansListManager.navigation = nil
        musiciansListManager.viewController = nil
        musiciansListManager.delegate = nil
    }
    
    /// initiall Setup For Controller
    func initiallSetup() {
        
        selectedDirection = UITableView.Direction.top(useCellsFrame: true)
        buttonBottomView.transform = CGAffineTransform(translationX: 0, y: buttonBottomView.frame.origin.y + 80 )
        self.tableView.reloadData(with: .simple(duration: 1.5, direction: self.selectedDirection, constantDelay: 0), reversed: false)
        
    }
    
    func showProfileImageView() {
        
        profileImageView.layer.borderColor = RED_COLOR.cgColor
        self.profileImageView.layer.borderWidth = 1
        
        if !Utility.profilePic.isEmpty {
            
            profileImageView.kf.setImage(with: URL(string: Utility.profilePic),
                                         placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
            },
                                         completionHandler: nil)
            
        } else {
            
            profileImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
    }
    
    func datepickerSetUp(){
        
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24 * 30)
        let picker = DateTimePicker.show(minimumDate: min, maximumDate: max)
        picker.highlightColor = #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "DONE"
        picker.todayButtonTitle = "Today"
        picker.is12HourFormat = true
        picker.dateFormat = "hh:mm aa dd/MM/YYYY"
        //        picker.isDatePickerOnly = true
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm aa dd/MM/YYYY"
        }
    }
    
    
    /// Flip Animation For View Controller
    ///
    /// - Parameter idntifier: Identifier
    func flipAnimation(_ idntifier: String){
        let search = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        UIView.beginAnimations("animation", context: nil)
        UIView.setAnimationDuration(1.0)
        self.navigationController!.pushViewController(search, animated: false)
        UIView.setAnimationTransition(UIView.AnimationTransition.flipFromLeft, for: self.navigationController!.view, cache: false)
        UIView.commitAnimations()
    }
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        
        
        TransitionAnimationWrapperClass.CATransitionForViewsinSameVCAnimation(for: CATransitionType.moveIn.rawValue, subType: CATransitionSubtype.fromTop.rawValue, timeFunctionType: "", for: (self.navigationController?.view)!, timeDuration: 0.35)
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }

    
}

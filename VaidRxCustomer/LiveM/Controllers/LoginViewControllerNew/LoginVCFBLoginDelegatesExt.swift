//
//  LoginVCFacebookLoginDelegateExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension LoginViewController:facebookLoginDelegate {
    
    /// This is the dalegate method called from FBLoginHandler When got facebook user details successfully
    ///
    /// - Parameter userInfo: facebook user details Dictionary
    
    func didFacebookUserLogin(withDetails userInfo: [String : Any]) {
        
        DDLogVerbose("FBData = \(userInfo)")
        
        fbUserDataModel = UserDataModel.init(facebookDetails: userInfo)
        
        if fbUserDataModel.mailId.length == 0 && fbUserDataModel.phoneNumber.length == 0 {
            
            emailTextField.text = ""
            loginViewModel.emailText.value = ""
            
            passwordTextfield.text = ""
            loginViewModel.passwordText.value = ""
            
            emailTextField.becomeFirstResponder()
        }
        else {
            
            isFBLogin = true
            
            if fbUserDataModel.mailId.length > 0 {
                
                emailTextField.text = fbUserDataModel.mailId
                loginViewModel.emailText.value = emailTextField.text!
                
            } else if fbUserDataModel.phoneNumber.length > 0 {
                    
                emailTextField.text = fbUserDataModel.phoneNumber
                loginViewModel.emailText.value = emailTextField.text!
                
            } else {
                
                emailTextField.text = ""
                loginViewModel.emailText.value = ""
                emailTextField.becomeFirstResponder()
            }
            
            if fbUserDataModel.userId.length > 0 {
                
                passwordTextfield.text = fbUserDataModel.userId
                loginViewModel.passwordText.value = passwordTextfield.text!
                
            } else {
                
                passwordTextfield.text = ""
                loginViewModel.passwordText.value = ""
                passwordTextfield.becomeFirstResponder()
            }
            
            validateAllFields()
            
        }
        
    }
    
    func didFailWithError(_ error: Error?) {
        
        
        Helper.alertVC(title: ALERTS.Message, message: (error?.localizedDescription)!)
    }
    
    func didUserCancelLogin() {
        
        DDLogDebug("User Cancelled Facebook Login")
    }
    
}

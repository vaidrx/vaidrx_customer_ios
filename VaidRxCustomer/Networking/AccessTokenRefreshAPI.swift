//
//  AccessTokenRefreshAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 25/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class AccessTokenRefreshAPI {
    
    let disposebag = DisposeBag()
    let accessTokenRefresh_Response = PublishSubject<APIResponseModel>()
    
    
    
    /// Method to call refresh access token service API
    ///
    /// - Parameter progressMessage: progress message to show
    func accessTokenRefreshServiceAPICall(progressMessage:String){
        
        let strURL = API.BASE_URL + API.METHOD.ACESS_TOKEN
        
        if progressMessage.length > 0 {
            
            Helper.showPI(_message: progressMessage)
        }
        
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAceessTokenAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                DDLogVerbose("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    func checkResponse(statusCode:Int, responseDict: [String:Any]){
        
        switch statusCode {
            
            case HTTPSResponseCodes.BadRequest.rawValue:
                
                Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
                break
                
            case HTTPSResponseCodes.InternalServerError.rawValue:
                
                Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                    
                }
                break
                
                
            default:
                
                let responseModel:APIResponseModel!
                responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
                self.accessTokenRefresh_Response.onNext(responseModel)
                break
        }
    }
    
}

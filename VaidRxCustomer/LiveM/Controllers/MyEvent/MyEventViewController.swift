//
//  MyEventViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 18/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

enum eventType {
    case pending ,upComming , past
}


class MyEventViewController: UIViewController {

    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var upcomingButton: UIButton!
    @IBOutlet weak var pastButton: UIButton!
    
    @IBOutlet weak var pendingTableView: UITableView!
    @IBOutlet weak var upcomingTableView: UITableView!
    @IBOutlet weak var pastTableView: UITableView!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var pendingMessageView: UIView!
    
    @IBOutlet var upcomingMessageView: UIView!
    
    @IBOutlet var pastMessageView: UIView!
    
    @IBOutlet weak var indicatorViewLeadingConstraint: NSLayoutConstraint!
    
    
    var arrayOfPendingBookings:[BookingDetailsModel] = []
    var arrayOfUpcomingBookings:[BookingDetailsModel] = []
    var arrayOfPastBookings:[BookingDetailsModel] = []
    
    
    weak var delegate: LeftMenuProtocol?
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    
    let myEventViewModel = MyEventViewModel()
    
    var gotoPastBooking = false
    var gotoPendingBooking = false
    var gotoUpcomingBooking = false
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    var initialSelfViewFrame:CGRect!
    
    // Shared instance object for gettting the singleton object
    static var obj:MyEventViewController? = nil
    
    class func sharedInstance() -> MyEventViewController {
        
        return obj!
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground(colorTop: UIColor(red: 38, green: 170, blue: 167), colorBottom: UIColor(red: 40, green: 178, blue: 211))
        MyEventViewController.obj = self
            
        pendingTableView.backgroundView = pendingMessageView
        upcomingTableView.backgroundView = upcomingMessageView
        pastTableView.backgroundView = pastMessageView
        
        getBookingsAPI()
        
        initialSelfViewFrame = SCREEN_RECT
        initialSelfViewFrame.origin.y = NavigationBarHeight
        initialSelfViewFrame.size.height = initialSelfViewFrame.size.height - NavigationBarHeight

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Utility.newBookingisCreated {
            
            gotoPendingBooking = true
            gotoPastBooking = false
            UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.NewBookingisCreated)
            UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.GoToUpcomingBookings)
            UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.BookingCancelled)
            UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.BookingReviewed)
            UserDefaults.standard.synchronize()
            getBookingsAPI()
            
        } else if Utility.bookingCancelled || Utility.bookingReviewed {
            
            gotoPendingBooking = false
            gotoPastBooking = true
            UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.NewBookingisCreated)
            UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.GoToUpcomingBookings)
            UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.BookingCancelled)
            UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.BookingReviewed)
            UserDefaults.standard.synchronize()
            getBookingsAPI()
            
        }
        
        Helper.editNavigationBar(navigationController!)
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        setupGestureRecognizer()
        
        acessClass.acessDelegate = self
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.frame = initialSelfViewFrame
    }

    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
        
        if arrayOfPendingBookings.count > 0 {
            
            for i in 0..<arrayOfPendingBookings.count {
                
                stopTimerInPendingBookings(index: i)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
         if Utility.goToUpcomingBooking {
            
            UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.GoToUpcomingBookings)
            UserDefaults.standard.synchronize()
            
            if !upcomingButton.isSelected {
                
                upcomingButton.isSelected = true
                upcommingAction(upcomingButton)
            }
            
        }else if gotoPastBooking {
            
            gotoPastBooking = false
            
            if !pastButton.isSelected {
                
                pastButton.isSelected = true
                pastAction(pastButton)
            }
            
        } else if gotoPendingBooking {
            
            gotoPendingBooking = false
            
            if !pendingButton.isSelected {
                
                pendingButton.isSelected = true
                pendingAction(pendingButton)
            }
            
         }
        
        pendingTableView.reloadData() //because of booking accept timer

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Action methods -
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func pendingAction(_ sender: Any) {
        var frame: CGRect = self.mainScrollView.bounds
        frame.origin.x = 0
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }

    @IBAction func upcommingAction(_ sender: Any) {
        var frame: CGRect = self.mainScrollView.bounds
        frame.origin.x = SCREEN_WIDTH!
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    @IBAction func pastAction(_ sender: Any) {
        var frame: CGRect = self.mainScrollView.bounds
        frame.origin.x = SCREEN_WIDTH! * 2
        mainScrollView.scrollRectToVisible(frame, animated: true)
        buttonEnable()
    }
    
    
    @IBAction func bookNowButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
 
}

extension MyEventViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension MyEventViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}


extension MyEventViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getAllApptDetails.rawValue:
            
            getBookingsAPI()
            
            break
            
        default:
            break
        }
    }
    
}


extension MyEventViewController {
    
    func buttonEnable() {
        
//        switch indicatorViewLeadingConstraint.constant {
//
//            case 0:
//
//                pendingButton.isSelected = true
//                upcomingButton.isSelected = false
//                pastButton.isSelected = false
//
//            case SCREEN_WIDTH!/3:
//
//                pendingButton.isSelected = false
//                upcomingButton.isSelected = true
//                pastButton.isSelected = false
//
//
//            case (SCREEN_WIDTH!) * 2:
//
//                pendingButton.isSelected = false
//                upcomingButton.isSelected = false
//                pastButton.isSelected = true
//
//
//            default:
//
//                pendingButton.isSelected = false
//                upcomingButton.isSelected = true
//                pastButton.isSelected = false
//
//        }
        
        if indicatorViewLeadingConstraint.constant <= 0 {
            
            pendingButton.isSelected = true
            upcomingButton.isSelected = false
            pastButton.isSelected = false
            
        } else if indicatorViewLeadingConstraint.constant >= (SCREEN_WIDTH!/3) * 2 {
            
            pendingButton.isSelected = false
            upcomingButton.isSelected = false
            pastButton.isSelected = true
            
        } else {
            
            pendingButton.isSelected = false
            upcomingButton.isSelected = true
            pastButton.isSelected = false
        }
    }
}

extension MyEventViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == mainScrollView {
            
            indicatorViewLeadingConstraint.constant = scrollView.contentOffset.x / 3
//            indicatorView.frame = CGRect(x: scrollView.contentOffset.x / 3, y: indicatorView.frame.origin.y, width: self.view.frame.size.width/3, height: 1)
            buttonEnable()
        }
    }
    
}

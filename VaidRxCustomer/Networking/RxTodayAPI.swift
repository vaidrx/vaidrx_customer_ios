//
//  RxTodayAPI.swift
//  vaiDRx
//
//  Created by 3Embed on 28/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class RxTodayAPI {
    
    let disposebag = DisposeBag()
    let rxToday_Response = PublishSubject<APIResponseModel>()
    let rxTOday_TransferMedications = PublishSubject<APIResponseModel>()
    let rxTOday_RefillMedications = PublishSubject<APIResponseModel>()
    
//    let location = LocationManager.sharedInstance()
    
    /// Method to call Login Service API
    ///
    /// - Parameter loginModel: Contains Login Request Details
    func rxTodayServiceAPICall(oldPharmAdd:String,phoneNumber:String,oldPharmacyCountryCode:String,oldPharmacyCheck:String,oldPharmacyTransferCheck:Int,rxTodayModel: RxTodayModel){
        
        let strURL = API.BASE_URL + API.METHOD.UPDATEPROFILE
        print(rxTodayModel.addLine1)
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.OldPharmacyAddLine1 : rxTodayModel.addLine1,
            SERVICE_REQUEST.OldPharmacyPhone : phoneNumber,
            SERVICE_REQUEST.OldPharmacyCountryCode : oldPharmacyCountryCode, //"+91",
            SERVICE_REQUEST.OldPharmacyCheck : oldPharmacyCheck,
            SERVICE_REQUEST.oldPharmacyTransferCheck : oldPharmacyTransferCheck,
            SERVICE_REQUEST.OldPharmacyCity : rxTodayModel.city,
            SERVICE_REQUEST.OldPharmacyState : rxTodayModel.state,
            SERVICE_REQUEST.OldPharmacyCountry : rxTodayModel.country,
            SERVICE_REQUEST.OldPharmacyPlaceId : rxTodayModel.placeId,
            SERVICE_REQUEST.OldPharmacyPincode : rxTodayModel.pincode,
            SERVICE_REQUEST.OldPharmacyLatitude : rxTodayModel.latitude,
            SERVICE_REQUEST.OldPharmacyLongitude : rxTodayModel.longitude
        ]
        
        Helper.showPI(_message: "Requesting....")
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict,requestType: RequestType.updateProfileData)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    func rxTodayTransferMedicationsServiceAPICall(transferMedicins:Int){
        let strURL = API.BASE_URL + API.METHOD.UPDATEPROFILE
        
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.oldPharmacyTransferCheck : transferMedicins
            
        ]
        
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict,requestType: RequestType.transferMedications)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    func rxTodayRefillMedicationsServiceAPICall(refillMedicins:Int){
        let strURL = API.BASE_URL + API.METHOD.UPDATEPROFILE
        
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.RefillCurrentMedications : String(refillMedicins)
            
        ]
        
        Helper.showPI(_message: "Requesting....")
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict,requestType: RequestType.RefillCurrentMedications)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    func checkResponse(statusCode:Int, responseDict: [String:Any],requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            
            break
            
            
        default:
            
            let responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
            case .updateProfileData :
                    self.rxToday_Response.onNext(responseModel)
            case .transferMedications:
                self.rxTOday_TransferMedications.onNext(responseModel)
            case .RefillCurrentMedications :
                self.rxTOday_RefillMedications.onNext(responseModel)
            default :
                break
            }
            
            break
        }
    }
    
}

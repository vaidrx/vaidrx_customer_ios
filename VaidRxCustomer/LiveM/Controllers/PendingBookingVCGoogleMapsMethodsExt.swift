//
//  PendingBookingVCGoogleMapsMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher
import GoogleMaps

extension PendingBookingViewController {
    
    
    func showInitialMapViewProperties() {
        
        self.mapView.settings.myLocationButton = true
        
        showAppointmentLocationMarker()
        showProviderMarker()
        
        markersToFitInMapView()
        
        for object: UIView in (mapView?.subviews)! {
            
            if String(describing: object.classForCoder) == "GMSUISettingsPaddingView" {
                
                for ob: UIView in object.subviews {
                    
                    if String(describing: ob.classForCoder) == "GMSUISettingsView" {
                        
                        for button in ob.subviews {
                            
                            if String(describing: button.classForCoder) == "GMSx_QTMButton" {
                                
                                (button as? UIButton)?.addTarget(self, action: #selector(self.markersToFitInMapView), for: .touchUpInside)
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    @objc func markersToFitInMapView() {
        
        if providerMarker != nil {
            
            var bounds = GMSCoordinateBounds()
            bounds = bounds.includingCoordinate(appointmentLocMarker.position)
            bounds = bounds.includingCoordinate(providerMarker.position)
            
            mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 55))
        }
    }
    
    func showAppointmentLocationMarker() {
        
        if appointmentLocMarker == nil {
            
            appointmentLocMarker = GMSMarker()
            appointmentLocMarker.isFlat = true
            appointmentLocMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            appointmentLocMarker.icon = #imageLiteral(resourceName: "appointment_mappin")
            
            appointmentLocMarker.position = CLLocationCoordinate2DMake(CLLocationDegrees(bookingEventModel.appointmentLat), CLLocationDegrees(bookingEventModel.appointmentLong))
            
            appointmentLocMarker.map = mapView
            
            
        }
        
    }
    
    func showProviderMarker() {
        
        if providerMarker == nil {
            
            providerMarker = GMSMarker()
            providerMarker.isFlat = true
            providerMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
//            providerMarker.iconView = createCustomMarker(bookingEventModel.providerImageURL)
            providerMarker.icon = #imageLiteral(resourceName: "map_pin")

            providerMarker.position = CLLocationCoordinate2DMake(CLLocationDegrees(bookingEventModel.providerLat), CLLocationDegrees(bookingEventModel.providerLong))
            
            providerMarker.map = mapView
            
        }
        
    }
    
    func createCustomMarker(_ providerImageUrl: String) -> UIView {
        
        var markerView:UIView!
        var markerImageViewInner:UIImageView!
        
        
        markerView = UIView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        markerImageViewInner = UIImageView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        
        
        
        markerView.layer.cornerRadius = 52 / 2
        markerImageViewInner.layer.cornerRadius = 52 / 2
        
        
        markerImageViewInner.tag = ProviderMarkerSubViews.providerImageView
        
        markerView.clipsToBounds = true
        markerImageViewInner.clipsToBounds = true
        
        
        markerView.backgroundColor = UIColor.clear
        
        
        if bookingEventModel.providerImageURL.length > 0 {
            
            markerImageViewInner.kf.setImage(with: URL(string: bookingEventModel.providerImageURL),
                                             placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                             options: [.transition(ImageTransition.fade(1))],
                                             progressBlock: { receivedSize, totalSize in
            },
                                             completionHandler: nil)
            
        } else {
            
            markerImageViewInner.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        
        markerView.addSubview(markerImageViewInner)
        
        return markerView
        
    }
    
    
}

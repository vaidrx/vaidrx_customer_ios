//
//  SetPasswordAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension SetPasswordController {
    
    func changePassword()  {
        
        self.view.endEditing(true)
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        setPasswordViewModel.userId = userID[SERVICE_RESPONSE.Sid] as? String ?? ""
        
        setPasswordViewModel.changePasswordBeforeLoginAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.changePassword)
        }
        
    }
    
    func updatePassword() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        guard reEnterPassTF.text! == newPassTF.text! else {
            Helper.showAlert(head: ALERTS.Message, message:ALERTS.ChangePassword)
            return
        }
        
        apiName = RequestType.updatePassword.rawValue
        
        setPasswordViewModel.changePasswordAfterLoginAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.updatePassword)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        if HTTPSResponseCodes.SetPassword(rawValue: statusCode) != nil {
        
            let responseCodes : HTTPSResponseCodes.SetPassword = HTTPSResponseCodes.SetPassword(rawValue: statusCode)!
            
            switch responseCodes
            {
                case .TokenExpired:
                    
                    if let dataRes = dataResponse as? String {
                        
                        AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                        
                        self.apiName = requestType.rawValue
                        var progressMessage = PROGRESS_MESSAGE.Loading
                        
                        switch requestType {
                            
                            case .changePassword:
                            
                                progressMessage = PROGRESS_MESSAGE.ChangingPassword
                            
                            case .updatePassword:
                            
                                progressMessage = PROGRESS_MESSAGE.ChangingPassword
                            
                            default:
                            
                                break
                        }
                        
                        self.acessClass.getAcessToken(progressMessage: progressMessage)

                    }
                    
                    break
                    
                           
                case .SuccessResponse:
                    
                    self.view.endEditing(true)
                    
                    switch requestType
                    {
                        case RequestType.changePassword:
                            
                            Helper.showAlert(head: ALERTS.Message, message: ALERTS.UpdatePassword)
                            
                            let viewControllers: [UIViewController] = self.navigationController?.viewControllers ?? [UIViewController()]
                            
                            for aViewController in viewControllers {
                                
                                if(aViewController is LoginViewController){
                                    self.navigationController!.popToViewController(aViewController, animated: true);
                                }
                            }
                        
                        
                        case RequestType.updatePassword:
                            
                            Helper.showAlert(head: ALERTS.Message, message: ALERTS.UpdatePassword)
                            
                            self.delegate?.passwordUpdated(sucess: true)
                                self.navigationController?.popViewController(animated: true)
                            
                            MixPanelManager.sharedInstance.changePasswordEvent()
                        
                            
                        default:
                            break
                    }
                    
                case .UserNotExist:
                    
                    if errorMessage != nil {
                        
                        Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                        
                    }
                    
                    break
            }
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
        }

    }
    
}

//
//  FilterPriceRangeTVCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 04/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class FilterPriceRangeTVCell: UITableViewCell {

    @IBOutlet var stockSlider:UIXRangeSlider?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        stockSlider?.leftValue = 0.0
        stockSlider?.rightValue = 100.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        var image = UIImage(named: "filter_sliding_dot_icon_off")
        
        
        stockSlider?.setImage(image, forElement:.leftThumb.self, forControlState: UIControl.State())
        stockSlider?.setTint(#colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1), forElement: .rightThumb.self, forControlState: UIControl.State())
        
        
        image = UIImage(named: "filter_sliding_dot_icon_off")
        image = image?.withRenderingMode(.alwaysTemplate)
        stockSlider?.setImage(image, forElement: .leftThumb.self, forControlState: UIControl.State())
        stockSlider?.setTint(#colorLiteral(red: 0.968627451, green: 0.1960784314, blue: 0.2470588235, alpha: 1), forElement: .rightThumb.self, forControlState: UIControl.State())
        
                // Configure the view for the selected state
    }
    

}

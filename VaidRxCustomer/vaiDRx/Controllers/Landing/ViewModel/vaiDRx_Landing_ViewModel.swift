//
//  vaiDRx_Landing_ViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class LandingViewModel {
    
    var landingModel:Landing!
    var loginType = LoginType.Default
    var triggerValue = 0
    
    var phoneNumberText = Variable<String>("")
    var countryCode = ""
    
    let disposebag = DisposeBag()
    
    let rxRegisterAPICall = RegisterAPI()
    
    func createLandingModel() {
        
        landingModel = Landing()
        landingModel.countryCode = countryCode
        landingModel.phoneNumberText = phoneNumberText.value
        landingModel.triggerValue = triggerValue
    }
    
    func validatePhoneNumberAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxRegisterAPICall.validatePhoneNumberServiceAPICall(countryCode: countryCode, phoneNumber: phoneNumberText.value,triggerValue: triggerValue)
        
        if !rxRegisterAPICall.validatePhoneNumber_Response.hasObservers {
            
            rxRegisterAPICall.validatePhoneNumber_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
    }
    
}

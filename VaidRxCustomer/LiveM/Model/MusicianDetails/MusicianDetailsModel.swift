//
//  HMProviderDetailsModel.swift
//  LiveM
//
//  Created by Apple on 07/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


/// Musician Details Model
class MusicianDetailsModel {
    
    var firstName = ""
    var lastName = ""
    var profilePic = ""
    var bannerImage = ""
    var providerId = ""
    var latitude = 0.0
    var longitude = 0.0
    var status = 0
    var amount = 0.0
    
    var email = ""
    var noOfReviews = 0
    var overallRating = 0.0
    var countryCode = ""
    var mobileNumber = ""
    var about = ""
    var eventsArray:[Any] = []
    var musicGeneres = ""
    var rules = ""
    var instrument = ""
    var reviewsArray:[Any] = []
    var totalReviewCount = 0
    var gigTimeArray:[Any] = []
    
    var youtubeVideoLink = ""
    
    var distance = 0.0
    var currencySymbol = ""
    
    var mileageMatric = 0
    
    init(musicianDetails:Any?) {//Musician details to show on Home screen and list screen, recieved from MQTT
     
        if let musician = musicianDetails as? [String:Any] {
            
            
            if let id = musician["id"] as? String {
                
                providerId = id
            }
            
            if let fName = musician["firstName"] as? String {
                
                firstName = fName.capitalized
            }
            
            if let lName = musician["lastName"] as? String {
                
                lastName = lName.capitalized
            }
            
            if let pic = musician["image"] as? String {
                
                profilePic = pic
            }
            
            if let banner = musician["bannerImage"] as? String {
                
                bannerImage = banner
            }
            
            if let statusValue = musician["status"] as? Int {
                
                status = statusValue
            }
            
            if let distanceValue = musician["distance"] as? Double {
            
                distance = distanceValue
            }
            
            if let youtubeVideoLinkValue = musician["youtubeUrlLink"] as? String {
                
                youtubeVideoLink = youtubeVideoLinkValue
            }
            
            if let currencySym = musician["currencySymbol"] as? String {
                
                currencySymbol = currencySym
            }
            
//            amount = Double(GenericUtility.strForObj(object: musician["amount"]))!

            
//            overallRating = Double(GenericUtility.floatForObj(object:musician["averageRating"]))
            
            
            if let coordinate = musician["location"] as? [String:Any] {
                
                latitude = Double(GenericUtility.strForObj(object: coordinate["latitude"]))!
                longitude = Double(GenericUtility.strForObj(object: coordinate["longitude"]))!
            }
            
            if let mileageMatricValue = musician["mileageMatric"] as? Int {
                
                mileageMatric = mileageMatricValue
            }
        
        }
    }
    
    init(bookingFlowMusicianDetails:Any?) {//Booking flow Musician details
        
        if let musician = bookingFlowMusicianDetails as? [String:Any] {
            
            
            if let id = musician["providerId"] as? String {
                
                providerId = id
            }
            
            if let fName = musician["firstName"] as? String {
                
                firstName = fName.capitalized
            }
            
            if let lName = musician["lastName"] as? String {
                
                lastName = lName.capitalized
            }
            
            if let pic = musician["profilePic"] as? String {
                
                profilePic = pic
            }
            
            if let statusValue = musician["proStatus"] as? Int {
                
                status = statusValue
            }
            
            if let distanceValue = musician["distance"] as? Double {
                
                distance = distanceValue
            }
            
            if let currencySym = musician["currencySymbol"] as? String {
                
                currencySymbol = currencySym
            }
            
            if let coordinate = musician["proLocation"] as? [String:Any] {
                
                latitude = Double(GenericUtility.strForObj(object: coordinate["latitude"]))!
                longitude = Double(GenericUtility.strForObj(object: coordinate["longitude"]))!
            }
            
        }
    }
    
    init(musicianFullDetail:Any?) {//Musician Full details recieved from Service API
        
        if let musician = musicianFullDetail as? [String:Any] {
            
            firstName = GenericUtility.strForObj(object:musician["firstName"]).capitalized
            lastName = GenericUtility.strForObj(object:musician["lastName"]).capitalized
            profilePic = GenericUtility.strForObj(object:musician["profilePic"])
            bannerImage = GenericUtility.strForObj(object:musician["bannerImage"])
            providerId = GenericUtility.strForObj(object:musician["id"])
            
            email = GenericUtility.strForObj(object:musician["email"])
            about = GenericUtility.strForObj(object:musician["about"])
            noOfReviews = GenericUtility.intForObj(object:musician["noOfReview"])
            overallRating = Double(GenericUtility.floatForObj(object:musician["rating"]))
            countryCode = GenericUtility.strForObj(object:musician["countryCode"])
            mobileNumber = GenericUtility.strForObj(object:musician["mobile"])
            eventsArray = GenericUtility.arrayForObj(object:musician["events"])
            rules = GenericUtility.strForObj(object:musician["rules"])
            musicGeneres = GenericUtility.strForObj(object:musician["musicGenres"])
            instrument = GenericUtility.strForObj(object:musician["instrument"])
            reviewsArray = GenericUtility.arrayForObj(object:musician["review"])
            gigTimeArray = GenericUtility.arrayForObj(object:musician["services"])
            
            if let youtubeVideoLinkValue = musician["youtubeUrlLink"] as? String {
                
                youtubeVideoLink = youtubeVideoLinkValue
            }

            if let currencySym = musician["currencySymbol"] as? String {
                
                currencySymbol = currencySym
            }
            
            if let coordinate = musician["location"] as? [String:Any] {
                
                latitude = Double(GenericUtility.strForObj(object: coordinate["latitude"]))!
                longitude = Double(GenericUtility.strForObj(object: coordinate["longitude"]))!
            }
            
            if let mileageMatricValue = musician["mileageMatric"] as? Int {
                
                mileageMatric = mileageMatricValue
            }
            
            if let reviewCount = musician["reviewCount"] as? Int {
                
                totalReviewCount = reviewCount
            }
            
        }
    }

}

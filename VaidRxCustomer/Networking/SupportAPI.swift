//
//  FAQAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 25/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class SupportAPI {
    
    let disposebag = DisposeBag()
    let getSupport_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call Get support details service API
    func getSupportServiceAPICall() {
        
        let strURL = API.BASE_URL + API.METHOD.SUPPORT
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        let requestParams : [String:Any] = [
            "len" : "en",
            "userType" : 1
        ]
        
//        RxAlamofire
//            .requestJSON(.get, strURL ,
//                         parameters:nil,
//                         encoding:JSONEncoding.default,
//                         headers: NetworkHelper.sharedInstance.getAOTHHeader()) //NetworkHelper.sharedInstance.getAOTHHeader())
//            .subscribe(onNext: { (r, json) in
//                
//                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
//                if  let dict  = json as? [String:Any]{
//                    
//                    let statuscode:Int = r.statusCode
//                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetSupportDetails)
//                }
//                
//                Helper.hidePI()
//                
//            }, onError: {  (error) in
//                
//                print("API Response \(strURL)\nError:\(error.localizedDescription)")
//                
//                Helper.hidePI()
//                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
//                
//            }).disposed(by: disposebag)
        let str = "\(strURL + "?" + Helper.convertDicToString(sender: requestParams as! [String : AnyObject]))"
        RxAlamofire
        .requestJSON(.get,
                     str,
                     parameters: nil,
                     encoding: JSONEncoding.default,
                     headers: NetworkHelper.sharedInstance.getAOTHHeader())
        .debug()
        .subscribe(onNext: { (r, json) in
            
              print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                            if  let dict  = json as? [String:Any]{
            
                                let statuscode:Int = r.statusCode
                                self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetSupportDetails)
                            }
            
                            Helper.hidePI()
            
        }, onError: {  (error) in
            Helper.hidePI()
            Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
        })
        .disposed(by: disposebag)
    }
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
            case .GetSupportDetails:
                
                self.getSupport_Response.onNext(responseModel)
                
            default:
                break
            }
            
            break
        }
    }
    
}

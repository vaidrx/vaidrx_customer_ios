//
//  HomeListMusicianDetailsView.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit
//import youtube_ios_player_helper


class HomeListMusicianDetailsView:UIView {
    
    // MARK: - Outlets -
    
    @IBOutlet weak var topView: UIViewCustom!
    
    @IBOutlet weak var youtubePlayerBackView: UIView!
    
//    @IBOutlet var youtubePlayerView: YTPlayerView!
    
    @IBOutlet var defaultImageView: UIImageView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var bottomView: UIView!
    
    
    @IBOutlet weak var musicianImageView: UIImageView!
    
    @IBOutlet weak var statusImageView: UIImageView!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    // Array Of star Images
    
    // label
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
}

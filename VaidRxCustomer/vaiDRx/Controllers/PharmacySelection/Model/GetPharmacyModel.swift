//
//  GetPharmacyModel.swift
//  vaiDRx
//
//  Created by 3Embed on 08/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation
class GetPharmacyModel {
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var ipAddress = ""
    var authorization = Utility.sessionToken
}

class PharmacyItems {
    var pharmacyId = ""
    var image = ""
    var distance = 0.0
    var location : [String:Any] = [:]
    var pharmacyName = ""
    var num = 0
    var latitude = 0.0
    var longitude = 0.0
}

class PharmacyResponseModel{
    
    var pharmacyItemsModel = Array<PharmacyItems>()
    init(pharmacyResponseDetails:Any) {
        
        print("pharmacyResponseDetails",pharmacyResponseDetails)
        if let pharmacyResponse = pharmacyResponseDetails as? Array<Dictionary<String,Any>>{
            print("insuranceResponse",pharmacyResponse)
            for item in pharmacyResponse{
                
                var dataItems = PharmacyItems()
                dataItems.pharmacyId = GenericUtility.strForObj(object: item["id"])
                dataItems.pharmacyName = GenericUtility.strForObj(object: item["pharmacyName"])
                dataItems.distance = item["distance"] as? Double ?? 0.0
                dataItems.image = GenericUtility.strForObj(object: item["image"])
                dataItems.location = item["location"] as! [String:Any]
                dataItems.latitude = dataItems.location["latitude"] as? Double ?? 0.0 //GenericUtility.strForObj(object: dataItems.location["latitude"])
                dataItems.longitude = dataItems.location["longitude"] as? Double ?? 0.0 //GenericUtility.strForObj(object: dataItems.location["longitude"])
                // dataItems.num = item["num"] as! Int
                
                
                pharmacyItemsModel.append(dataItems)
                
            }
            
        }
        
        
    }
}

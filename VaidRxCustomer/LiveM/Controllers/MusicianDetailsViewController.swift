//
//  MusicianDetailsViewController.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
//import youtube_ios_player_helper
import ESPullToRefresh

class MusicianDetailsViewController:UIViewController {
    
    @IBOutlet weak var navigationBackButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableHeaderView: UIView!
    
    @IBOutlet var youtubeBackView: UIView!
    
//    @IBOutlet var youtubePlayerView: YTPlayerView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var defaultImageView: UIImageView!
    
    @IBOutlet weak var nameDetailsBackView: UIView!

    @IBOutlet weak var musicianNameLabel: UILabel!
    
    @IBOutlet weak var numberOfReviewsLabel: UILabel!
    
    @IBOutlet weak var ratView: FloatRatingView!
    
    @IBOutlet weak var milesLabel: UILabel!
    
    @IBOutlet weak var musicianImageView: UIImageView!

    @IBOutlet weak var musicianOnlineImageView: UIImageView!
    
    @IBOutlet weak var bookButtonBackView: UIView!
    
    @IBOutlet weak var bookButton: UIButtonCustom!
    
    @IBOutlet var blurrView: UIView!
    
    @IBOutlet var navigationTopView: UIView!
    
    @IBOutlet var navigationTitleView: UIView!
    
    @IBOutlet var navigationTitleLabel: UILabel!
    
    @IBOutlet var bookButtonBackViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var blurrViewTopConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var navigationTopViewHeightConstraint: NSLayoutConstraint!
    
    
    
    let locationObj = LocationManager.sharedInstance()
    
    var musicianFullDetailsModel:MusicianDetailsModel!
    var providerDetailFromPrevController:MusicianDetailsModel!
    
    var CBModel:ConfirmBookingModel!
    
    var arrayOfHeaderTitles:[String] = []
    
    var isFromLiveTrackVC:Bool = false
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!
    
    var gigTimeCell: MDGigTimeTableViewCell!
    var eventsCell: MDEventsTableViewCell!
    
    var tableHeaderHeight:CGFloat = 0.0
    
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    let musicianDetailsViewModel = MusicianDetailsViewModel()
    
    var footer: ESRefreshProtocol & ESRefreshAnimatorProtocol = ESRefreshFooterAnimator.init(frame: CGRect.zero)

    var pageNo: Int = 0
    var states : Array<Bool>!
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        self.tableView.estimatedRowHeight = 120

        self.addTableViewBottomRefresh()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        acessClass.acessDelegate = self
                
//        tableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
        self.tableView.scrollRectToVisible(CGRect.init(x: 0, y: 0, width: 1, height: 1), animated: false)

        
        setupGestureRecognizer()
        hideNavigation()
        
//        self.view.bringSubview(toFront: self.navigationTopView)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
        
        showNavigation()
        
//        if youtubePlayerView.playerState() == .playing {
//
//            youtubePlayerView.pauseVideo()
//        }

    }
    
    func addTableViewBottomRefresh() {
        
        self.tableView.es.addInfiniteScrolling(animator: footer) { [weak self] in
            
            self?.handleRefresh()
        }
        
    }
    
    func handleRefresh() {
        
        pageNo = pageNo + 1
        getReviewsAndRating()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 90){
            
            self.tableView.es.stopLoadingMore()
        }
        
    }
    

    @IBAction func navigationBackButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func bookButtonAction(_ sender: Any) {
        
        if CBModel.providerFullDetalsModel.gigTimeArray.count > 0 {
            
            let confirmBookingVC:ConfirmBookingScreen = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.confirmBookingVC) as! ConfirmBookingScreen
            
            confirmBookingVC.CBModel = CBModel
            
            
            self.navigationController!.pushViewController(confirmBookingVC, animated: true)
            
        } else {
            
            Helper.showAlert(head: ALERTS.Message, message: ALERTS.MUSICIAN_DETAILS.MissingGigTime)
            
        }

    }
}

extension MusicianDetailsViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getProviderDetails.rawValue:
            
            sendRequestTogetProviderDetails()
            
            break
            
        case RequestType.GetReviews.rawValue:
            
            getReviewsAndRating()
            
            break
            
        default:
            break
        }
    }
    
}

extension MusicianDetailsViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }

    
}

extension MusicianDetailsViewController:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let yOffset = scrollView.contentOffset.y
        
//        if SCREEN_HEIGHT == 812 {//iPhoneX
//            
//            yOffset = yOffset + NavigationBarHeight
//        }
        
        if yOffset > 0 {
            
            if yOffset < tableHeaderView.frame.size.height - NavigationBarHeight {
                
                //Scroll to Top
                if blurrView.frame.size.height <= NavigationBarHeight {
                    
                    //show Navigation
                    self.navigationTopView.isHidden = false
                    self.navigationTitleView.isHidden = false
                    
                    blurrViewTopConstraint.constant = tableHeaderView.frame.size.height - NavigationBarHeight - 1
                    blurrView.alpha = 0
                    
                    DDLogDebug("\nNo - 1, y = \(yOffset)")
                    
                } else {
                    
                    hideNavigation()
                    blurrViewTopConstraint.constant = yOffset
                    blurrView.alpha = yOffset/(tableHeaderView.frame.size.height - NavigationBarHeight)
                    
                    DDLogDebug("\nNo - 2, y = \(yOffset)")
                    
                }
                
            } else {
                
                self.navigationTopView.isHidden = false
                self.navigationTitleView.isHidden = false
                
                blurrViewTopConstraint.constant = tableHeaderView.frame.size.height - NavigationBarHeight - 1
                blurrView.alpha = 0
                DDLogDebug("\nNo - 3, y = \(yOffset)")
                
            }
            
        } else {
            
            hideNavigation()
            blurrViewTopConstraint.constant = 0
            blurrView.alpha = 0
            
            DDLogDebug("\nNo - 4, y = \(yOffset)")
        }
        
        if scrollView == self.tableView && (musicianFullDetailsModel != nil) {
            
            if musicianFullDetailsModel.noOfReviews > 0 {
                
                if yOffset > 50 {
                    
                    self.tableView.bounces = true
                    
                } else {
                    
                    self.tableView.bounces = false
                }
            }
            
        }
        
        
    }

    
    func showNavigation() {
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.view?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }

    
    func hideNavigation() {
        
        self.navigationTopView.isHidden = true
        self.navigationTitleView.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.view?.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
//        Helper.statusBarView.backgroundColor = UIColor.clear
        
    }

}


extension MusicianDetailsViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

//extension MusicianDetailsViewController:YTPlayerViewDelegate {
//
//    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
//
//
//    }
//
//    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
//
//    }
//
//    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
//
//    }
//
//    func playerView(_ playerView: YTPlayerView, didChangeTo quality: YTPlaybackQuality) {
//
//    }
//
//    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
//
//        defaultImageView.isHidden = true
//        activityIndicator.stopAnimating()
//        youtubePlayerView.isHidden = false
//
//    }
//}

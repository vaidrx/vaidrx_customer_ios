//
//  AddInsuranceView.swift
//  LiveM
//
//  Created by Rahul Sharma on 23/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

protocol customInsuranceDelegate {
    
    func addedNewInsurance(model: RegisterViewModel?,orString: String)
}

class AddInsuranceView: UIView, KeyboardDelegate {

    @IBOutlet weak var topViewCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    var registerViewModel: RegisterViewModel!
    var delegate: customInsuranceDelegate? = nil
    var isComing = ""
    
    @IBOutlet weak var insuranceTextFeild: UITextField!
    
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    
    private static var share: AddInsuranceView? = nil
    
    static var sharedInstance: AddInsuranceView {
        
        if share == nil {
            
            share = Bundle(for: self).loadNibNamed("AddInsurance",
                                                   owner: nil,
                                                   options: nil)?.first as? AddInsuranceView
            
            share?.frame = (WINDOW_DELEGATE??.frame)!
            
        }

        
        return share!
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        appDelegate?.keyboardDelegate = self
        insuranceTextFeild.becomeFirstResponder()
        //custom logic goes here
    }
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        let topViewVSP =  self.topView.frame.origin.y
        let topViewHeight = self.topView.frame.size.height
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        let bottomHeight = screenHeight - ( topViewVSP + topViewHeight)
        
        if (keyboardSize?.height)! > bottomHeight {
            
            let increasedHeight = (keyboardSize?.height)! - bottomHeight
            
            
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            
                self.topViewCenterConstraint.constant = -(increasedHeight + 20.0)

                self.layoutIfNeeded()
            })

        }
        

    }
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.4) {
            
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            
                            self.topViewCenterConstraint.constant = 0
                            
                            self.layoutIfNeeded()
            })
        }
        
    }

    @IBAction func closeAction(_ sender: UIButton) {
        
        appDelegate?.keyboardDelegate = nil
        
        topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        
       UIView.animate(withDuration: 0.6,
                       delay: 0.2,
                       options: UIView.AnimationOptions(rawValue: UIView.AnimationOptions.beginFromCurrentState.rawValue),
                       animations: {
                        
                        self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                        
        }) { (finished) in
            
            self.removeFromSuperview()
            AddInsuranceView.share = nil
            
        }
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        if insuranceTextFeild.text?.count == 0{
            
            Helper.alertVC(title: "Message", message: "You didn't provided any insurance name!")
        } else {
            if isComing != "Profile" {
                registerViewModel.categoryId = insuranceTextFeild.text!
                delegate?.addedNewInsurance(model: self.registerViewModel, orString: " ")
            } else {
                delegate?.addedNewInsurance(model: nil, orString: insuranceTextFeild.text!)
            }
            
            appDelegate?.keyboardDelegate = nil
            
            topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
            
            UIView.animate(withDuration: 0.6,
                           delay: 0.2,
                           options: UIView.AnimationOptions(rawValue: UIView.AnimationOptions.beginFromCurrentState.rawValue),
                           animations: {
                            
                            self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                            
            }) { (finished) in
                
                self.removeFromSuperview()
                AddInsuranceView.share = nil
                
            }

        }
    
    }
    
    @IBAction func viewWhenTaped(_ sender: UITapGestureRecognizer) {
    
        self.endEditing(true)
    }
}

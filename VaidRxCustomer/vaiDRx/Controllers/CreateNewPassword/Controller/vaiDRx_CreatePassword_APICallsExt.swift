//
//  vaiDRx_CreatePassword_APICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 13/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_CreatePasswordVC {
    
    func changePassword()  {
        
        self.view.endEditing(true)
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        setPasswordViewModel.userId = userID[SERVICE_RESPONSE.Sid] as! String
        
        setPasswordViewModel.changePasswordBeforeLoginAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.changePassword)
        }
        
    }
    
    func getStripeKeyData(){
        
        if !NetworkHelper.sharedInstance.networkReachable(){
            Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
            //Helper.showAlertMessage(title: ALERT_VAIDRX.Error, message: ALERT_VAIDRX.NoInternet)
            return
        }
        
        vaidRx_AddNewCardViewModel.getStripeKeyAPICall { (statusCode, errMsg, response) in
            self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response)
        }
    }
    
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResp:Any?){
        switch statusCode {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            stripeKeyResponseModel = StripeKeyResponseModel.init(stripeKEyResponseDetails:dataResp as Any )
            
            //            insuranceItems = insuranceData.insuranceItemsModel
            //            DispatchQueue.main.async {
            //                self.tableView.reloadData()
            //            }
            //            publishableKey = stripeKeyResponseModel.publishableKey
            print("insuranceItems",stripeKeyResponseModel)
            ConfigManager.sharedInstance.paymentGatewayKey = stripeKeyResponseModel.publishableKey
            
        //            print("data response",dataResp!)
        default:
            break
        }
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        if HTTPSResponseCodes.SetPassword(rawValue: statusCode) != nil {
            
            let responseCodes : HTTPSResponseCodes.SetPassword = HTTPSResponseCodes.SetPassword(rawValue: statusCode)!
            
            switch responseCodes
            {
            case .TokenExpired:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
//                    self.apiName = requestType.rawValue
//                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                    case .changePassword:
                        break
                        
//                        progressMessage = PROGRESS_MESSAGE.ChangingPassword
                        
                    case .updatePassword:
                        break
//                        progressMessage = PROGRESS_MESSAGE.ChangingPassword
                        
                    default:
                        
                        break
                    }
                    
//                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
                
                
            case .SuccessResponse:
                
                self.view.endEditing(true)
                
                switch requestType
                {
                case RequestType.changePassword:
                    
                    Helper.showAlert(head: ALERTS.Message, message: ALERTS.UpdatePassword)
                    
                    
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;

                    for aViewController in viewControllers {

                        if(aViewController is vaiDRxLandingVCViewController){
                            self.navigationController!.popToViewController(aViewController, animated: true);
                        }
                    }
                    
                    
//                case RequestType.updatePassword:
//                    
//                    Helper.showAlert(head: ALERTS.Message, message: ALERTS.UpdatePassword)
//                    
////                    self.delegate?.passwordUpdated(sucess: true)
//                    self.navigationController?.popViewController(animated: true)
//                    
//                    MixPanelManager.sharedInstance.changePasswordEvent()
                    
                    
                default:
                    break
                }
                
            case .UserNotExist:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                    
                }
                
                break
            }
        } else {
            
            if errorMessage != nil {
                
                Helper.alertVC(title: ALERTS.Error , message: errorMessage!)
            }
        }
        
    }

}

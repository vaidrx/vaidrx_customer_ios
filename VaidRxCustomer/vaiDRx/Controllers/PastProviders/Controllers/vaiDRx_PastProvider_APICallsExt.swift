//
//  vaiDRx_PastProvider_APICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 15/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension vaiDRx_PastProvidersVC {
    
    func sendServiceToGetPastProviders() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
                
        pastProviderViewModel.getPastProviderAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.PastProviders)
        }
        
    }
    
    func pastBookinAPI(mode:String) {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        

        pastProviderViewModel.CBModel.bookingModel = 2
        pastProviderViewModel.CBModel.bookingType = self.vaidType
        pastProviderViewModel.CBModel.appointmentLocationModel.bookingType = BookingType.Default
        pastProviderViewModel.CBModel.bookingMode = mode

        pastProviderViewModel.liveBookingAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.liveBooking)
        }
        
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        
        switch statusCode
        {
            
        case HTTPSResponseCodes.TokenExpired.rawValue:
            
            if let dataRes = dataResponse as? String {
                
                AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
//                self.apiTag = requestType.rawValue
                
                var progressMessage = PROGRESS_MESSAGE.Loading
                
                switch requestType {
                    
//                case .signOut:
//                    
//                    progressMessage = PROGRESS_MESSAGE.LogOut
//                    
                case .PastProviders:
                    
                    progressMessage = PROGRESS_MESSAGE.Loading
//
//                case .updateProfileData:
//
//                    progressMessage = PROGRESS_MESSAGE.Saving
                    
                    
                default:
                    
                    break
                }
                
                self.acessClass.getAcessToken(progressMessage: progressMessage)
            }
            
            break
            
            
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
            case RequestType.PastProviders:
                
//                self.showViews()
                
                if dataResponse != nil {
                    
                    self.pastProviderResponseModel = PastProviderResponseModel.init(providerDetails: dataResponse!)
                    
                    tableView.reloadData()
//                    setAllData(data: profileResponseModel!)
                    
                }
                
                
//            case RequestType.updateProfileData :
//                DDLogDebug("Sucess")
//                
//                
//            case RequestType.signOut :
//                Helper.logOutMethod()
//                DDLogDebug("Sucess")
            case RequestType.liveBooking :
                if let dataRes = dataResponse as? [String:Any] {
                    
                    if let bookingId = dataRes["bookingId"] as? Int64 {
                        
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)

                        let bookingViewController = storyboard.instantiateViewController(withIdentifier: "BookingVC") as! vaiDRx_BookingVC
                        bookingViewController.isComingFrom = isComingfromTitle
                        bookingViewController.modalPresentationStyle = .fullScreen
//                        let bookingViewController = self.storyboard!.instantiateViewController(withIdentifier: "BookingVC") as! vaiDRx_BookingVC
                        if let countDown = dataRes["bookingExpirySecond"] as? Int {
                            
                            bookingViewController.countDown = countDown
                        }
                        bookingViewController.pastProviderResponseModel = self.pastProviderResponseModel
                        bookingViewController.selectedIndex = self.selectedIndex
//                        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
//                                                                                  subType: kCATransitionFromTop,
//                                                                                  for: (self.navigationController?.view)!,
//                                                                                  timeDuration: 0.35)
//
//
//                        navigationController?.pushViewController(bookingViewController, animated: false)
                        self.present(bookingViewController, animated: true, completion: nil)
                        
                        self.bookingId = bookingId
                        
                    }
                    
                }
                
            default:
                
                break
            }
            
        default:
            
            if  errorMessage != nil {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                
            }
            
            
            break
        }
        
    }
}

//
//  ProfileVCButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ProfileViewController  {
//    func insuranceSeleceted(selectedInsurance: [String : Any]) {
//        self.musicGenreTextField.text = selectedInsurance["paymentType"] as? String
//    }
    
    
    
    @IBAction func addressButtonAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVc = storyboard.instantiateViewController(withIdentifier: "AddNewAddressVC") as! AddNewAddressViewController
        nextVc.addMyAddressDelegate = self 
        //nextVc.registerViewModel = self.registerViewModel
        //        let storyboard = UIStoryboard(name: "vaiDRx_Main", bundle: nil)
        //        let nextVc = storyboard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddMyAddressViewController
        nextVc.isComingFrom = "Profile"
        self.navigationController?.pushViewController(nextVc, animated: false)
    }
    
    @IBAction func pharamacyButtonAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "vaiDRx_Main", bundle: nil)
        let nextVc = storyboard.instantiateViewController(withIdentifier: "PharmacySelection") as! PharmacySelectionViewController
        nextVc.selectedPharmacyDelegate = self
        nextVc.isComingFrom = "Profile"
        nextVc.profileResponseModel = self.profileResponseModel
        //nextVc.is
        //nextVc.registerViewModel = self.registerViewModel
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    // MARK: - Action methods -
    @IBAction func backAction(_ sender: Any) {
        if firstNameBottomView.transform != .identity {
            
            self.navigationController?.popViewController(animated: true)
        } else {
            
            // create the alert
            
            let alertController = UIAlertController(title: ALERTS.Message, message: ALERTS.SaveDetails, preferredStyle: UIAlertController.Style.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
            
            let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
            newAlertWindow.rootViewController = UIViewController()
            newAlertWindow.windowLevel = UIWindow.Level.alert + 1
            newAlertWindow.makeKeyAndVisible()
            
            let DestructiveAction = UIAlertAction(title: ALERTS.NO, style: UIAlertAction.Style.destructive) {
                (result : UIAlertAction) -> Void in
                
                newAlertWindow.resignKey()
                newAlertWindow.removeFromSuperview()
                
                self.navigationController?.popViewController(animated: true)
            }
            
            // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
            
            let okAction = UIAlertAction(title: ALERTS.YES, style: UIAlertAction.Style.default) {
                (result : UIAlertAction) -> Void in
                
                newAlertWindow.resignKey()
                newAlertWindow.removeFromSuperview()
                
                DDLogDebug("Destructive")
//                self.navigationController?.popViewController(animated: true)
            }
            
            alertController.addAction(DestructiveAction)
            alertController.addAction(okAction)

            newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)

        }
        //        delegate?.changeViewController(LeftMenu.searchArtist)
        
    }
    
    @IBAction func imageAction(_ sender: Any) {
        musicGenre = true
        libraryEnabled = !libraryEnabled
        croppingEnabled = !croppingEnabled
        alertView()
    }
    
    @IBAction func musicGenreAction(_ sender: Any) {
//        musicGenre = true
//        catransitionAnimation(idntifier: VCIdentifier.musicGenre)
//        let storyboard = UIStoryboard(name: "vaiDRx_Main", bundle: nil)
//        let nextVc = storyboard.instantiateViewController(withIdentifier: "InsuranceTypes") as! TypesInsuranceViewController
//        nextVc.insuranceDelegate = self
//        self.navigationController?.pushViewController(nextVc, animated: true)
        
        performSegue(withIdentifier: "profileToChangePayment", sender: self)
    }
    
    @IBAction func countryPickerAction(_ sender: Any) {
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    
    
    @IBAction func changeEmailAction(_ sender: Any) {
        isEmail = true
        performSegue(withIdentifier: SEgueIdetifiers.profileChnageEM, sender: nil)
    }
    
    
    @IBAction func changeMobileAction(_ sender: Any) {
        isEmail = false
        performSegue(withIdentifier: SEgueIdetifiers.profileChnageEM, sender: nil)
    }
    
    
    
    @IBAction func editAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if firstNameBottomView.transform != .identity {
            
            editButtonAnimation()
            UIView.transition(with: editButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.editButton.setTitle(ALERTS.save, for: .normal)
            })
            
            if (musicGenreBottomView.frame.size.height + musicGenreBottomView.frame.origin.y) + 100 > scrollView.frame.size.height {
                
                scrollContentViewHeightConstraint.constant = (musicGenreBottomView.frame.size.height + musicGenreBottomView.frame.origin.y) + 100 - scrollView.frame.size.height;
            }
            else
            {
                self.scrollContentViewHeightConstraint.constant = 0
            }
            
        }else {
            
            if ispharEdited {
                if !NetworkHelper.sharedInstance.networkReachable(){
                    Helper.showAlert(head: ALERTS.Error, message: ALERTS.NoNetwork)
                    
                    return
                }
                self.getPharmacyViewModel.updatePharmacyAPICall { (statusCode, errMsg, response) in
                    self.webServiceResponse(statusCode: statusCode, errorMessage: errMsg, dataResp: response,requestType:RequestType.updateProfileData)
                }
            }
            
            
            
            if nameTextField.text?.length == 0 {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.FirstNameMissing)
                
            }
//            else if lastNameTextField.text?.length == 0 {
//                
//                Helper.alertVC(title: ALERTS.Message, message: ALERTS.LastNameMissing)
//                
//            }
            else {
                
                if  profileAlternatePhoto == 1 ||  profileAlternatePhoto == 2 {
                    
                    uploadProfileImageToAmazon()
                    
                }else {
                    UserDefaults.standard.set(false, forKey: USER_DEFAULTS.USER.PAYMENT_TYPE_CHANGED)
                    self.updateUserDefault()
                    self.serviceRequestToUpdateProfile()
                }
                
                
                UIView.transition(with: editButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.editButton.setTitle(ALERTS.edit, for: .normal)
                })
                
                initiallSetupForAnimation()
                animations()
                
//                if (musicGenreBottomView.frame.size.height + musicGenreBottomView.frame.origin.y) + 100 > scrollView.frame.size.height {
//
//                    scrollContentViewHeightConstraint.constant = (musicGenreBottomView.frame.size.height + musicGenreBottomView.frame.origin.y) + 100 - scrollView.frame.size.height;
//                }
//                else
//                {
//                    self.scrollContentViewHeightConstraint.constant = 0
//                }
                
                scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
                
                self.view.layoutIfNeeded()
                
            }
            
        }
     }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        self.performSegue(withIdentifier: SEgueIdetifiers.profileToChangePass, sender: nil)
    }
    
    @IBAction func logOutAction(_ sender: Any) {
        // create the alert
        
        let alertController = UIAlertController(title: ALERTS.Message, message: ALERTS.LogOut, preferredStyle: UIAlertController.Style.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindow.Level.alert + 1
        newAlertWindow.makeKeyAndVisible()
        
        let DestructiveAction = UIAlertAction(title: ALERTS.NO, style: UIAlertAction.Style.destructive) {
            (result : UIAlertAction) -> Void in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            
            DDLogDebug("Destructive")
        }
        
        // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        
        let okAction = UIAlertAction(title: ALERTS.YES, style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            
            self.logOut()
        }
        
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        //        self.present(alertController, animated: true, completion: nil)
        
//        if Helper.newAlertWindow == nil {
//
//            Helper.newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
//            Helper.newAlertWindow?.rootViewController = UIViewController()
//            Helper.newAlertWindow?.windowLevel = UIWindowLevelAlert + 1
//            Helper.newAlertWindow?.makeKeyAndVisible()
//            Helper.newAlertWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
//
//
//        } else {
//
            newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
//        }
        
    }
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func dobAction(_ sender: Any) {
        
//        self.birthdayPicker.setDate(selectedDOB, animated: true)
//
//        let dateFormat = DateFormatter()
//        dateFormat.dateFormat = "MM/dd/yyyy"
//        pickerDateLabel.text = dateFormat.string(from: selectedDOB)
//
//        dobPickerBottomConstraint.constant = 0
//        UIView.animate(withDuration: 0.75, animations: {() -> Void in
//            self.view.layoutIfNeeded()
//        })
        
        self.newDatePickerView.isHidden=true
        
        
    }

    
    
    @IBAction func cancelPickerAction(_ sender: Any) {
        
        dobPickerBottomConstraint.constant = -500
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func donePickerAction(_ sender: Any) {
        
        selectedDOB = birthdayPicker.date
        
        dobLabel.text = Helper.getDobStringDate(date: selectedDOB)
        
        dobPickerBottomConstraint.constant = -500
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
        })
    }
    
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResp:Any?,requestType:RequestType){
        switch statusCode {
        case HTTPSResponseCodes.SuccessResponse.rawValue:
            switch requestType{
            case .updateProfileData:
                self.navigationController?.popViewController(animated: true)
                break
            case .getPharmacy:
//                if dataResp != nil{
//                    pharmacyData = PharmacyResponseModel.init(pharmacyResponseDetails:dataResp! )
//
//                    pharmacyItems = pharmacyData.pharmacyItemsModel
//                    DispatchQueue.main.async {
//                        self.collectionView.reloadData()
//                    }
//
//                    //                    print("pharmacyItems",pharmacyItems)
//
//
//                    //                    print("data response",dataResp ?? [:])
//
//                    if pharmacyItems != nil{
//                        showMarkersInMapView()
//                    }
//
//                }
                break
            default:
                break
            }
            
            
            
        default:
            break
        }
    }
    
}

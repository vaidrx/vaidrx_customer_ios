//
//  CBPaymentMethodCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class CBPaymentMethodCell:UITableViewCell {
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var cardAndCashBackView: UIView!
    
    @IBOutlet var cardButton: UIButton!
    
    @IBOutlet var cardSelectedView: UIView!
    
    @IBOutlet var cashButton: UIButton!
    
    @IBOutlet var cashSelectedView: UIView!
    
    @IBOutlet var cardNumberLabel: UITextField!
    
    @IBOutlet var cardDetailsbackView: UIView!
    
    @IBOutlet var addCardButton: UIButton!
    
    func showPaymentMethodDetails(CBModel:ConfirmBookingModel) {
        
        if CBModel.paymentmethodTag == 0 {
            
            cardDetailsbackView.isHidden = false
            
            cardButton.isSelected = true
            cashButton.isSelected = false
            
            cardSelectedView.isHidden = false
            cashSelectedView.isHidden = true
            
            if CBModel.selectedCardModel != nil {
                
                if CBModel.selectedCardModel.last4Digits.length > 0 {
                 
                    cardNumberLabel.text = "**** **** **** " + CBModel.selectedCardModel.last4Digits
                }
                else {
                    
                    cardNumberLabel.text = ""
                }
                
                
            } else {
                
                cardNumberLabel.text = ""
            }
            
        } else {
            
            cardDetailsbackView.isHidden = true
            
            cardButton.isSelected = false
            cashButton.isSelected = true
            
            cardSelectedView.isHidden = true
            cashSelectedView.isHidden = false
            
            cardNumberLabel.text = ""
        }
        
    }
}

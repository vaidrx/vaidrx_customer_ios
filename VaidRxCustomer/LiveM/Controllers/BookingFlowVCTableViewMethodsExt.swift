//
//  BookingFlowTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension BookingFlowViewController:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if bookingDetailModel != nil {
            
            return 7
            
        } else {
            
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 1://Accepted
            
            if bookingStatus >= Booking_Status.Accepted.rawValue {
                
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "acceptCell", for: indexPath) as! BookingFlowAcceptedTableViewCell
                
                cell.callButton.addTarget(self, action: #selector(callButtonAction), for: .touchUpInside)
                cell.messageButton.addTarget(self, action: #selector(messageButtonAction), for: .touchUpInside)
                
                cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistAccepted
                
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "hh:mm\na"
                
                let acceptedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.acceptedDate))
                
                if bookingStatus == Booking_Status.Accepted.rawValue {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.darkGray
                    cell.timeLabel.text = dateFormat.string(from: acceptedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                    cell.statusNameLabel.textColor = UIColor.darkGray
                    
                } else {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.red
                    cell.timeLabel.text = dateFormat.string(from: acceptedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                    cell.statusNameLabel.textColor = UIColor.red
                    
                }
                
                
                cell.acceptedMessagelabel.text = bookingDetailModel.providerName + " has accepted your request."
                
                if bookingDetailModel.providerImageURL.length > 0 {
                    
                    cell.musicianImageView.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
                                                       placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                                       options: [.transition(ImageTransition.fade(1))],
                                                       progressBlock: { receivedSize, totalSize in
                    },
                                                       completionHandler: nil)
                    
                } else {
                    
                    cell.musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
                }
                
                
                return cell
                
            } else {
                
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! BookingFlowNormalTableViewCell
                
                cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistAccepted
                
                cell.timeLabel.isHidden = true
                cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                cell.statusNameLabel.textColor = UIColor.lightGray
                
                return cell
                
            }
            
            
        case 2://Ontheway
            
            if bookingStatus == Booking_Status.Ontheway.rawValue {
                
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "liveTrackCell", for: indexPath) as! BookingFlowLiveTrackTableViewCell
                
                cell.liveTrackButton.addTarget(self, action: #selector(liveTrackButtonAction), for: .touchUpInside)
                
                cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistOnTheWay
                
                cell.liveTrackButton.layer.borderColor = UIColor.red.cgColor
                
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "hh:mm\na"
                
                let onTheWayDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.onThewayDate))
                
//                if bookingStatus == Booking_Status.Ontheway.rawValue {
                
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.darkGray
                    cell.timeLabel.text = dateFormat.string(from: onTheWayDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                    cell.statusNameLabel.textColor = UIColor.darkGray
                    
//                }
                
                
                return cell
                
            } else {
                
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! BookingFlowNormalTableViewCell
                
                cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistOnTheWay
                
                if bookingStatus > Booking_Status.Ontheway.rawValue {
                    
                    let dateFormat = DateFormatter()
                    dateFormat.dateFormat = "hh:mm\na"
                    
                    let onTheWayDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.onThewayDate))
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.red
                    cell.timeLabel.text = dateFormat.string(from: onTheWayDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                    cell.statusNameLabel.textColor = UIColor.red
                    
                } else {
                    
                    cell.timeLabel.isHidden = true
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                    cell.statusNameLabel.textColor = UIColor.lightGray
                }
                
                
                
                return cell
                
            }
            
        default:
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! BookingFlowNormalTableViewCell
            
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "hh:mm\na"
            
            switch indexPath.row {
                
            case 0://Requested
                
                cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistRequested
                
                let requestedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.bookingStartTime))
                
                if bookingStatus < Booking_Status.Requested.rawValue {
                    
                    cell.timeLabel.isHidden = true
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                    cell.statusNameLabel.textColor = UIColor.lightGray
                    
                } else if bookingStatus == Booking_Status.Requested.rawValue {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.darkGray
                    cell.timeLabel.text = dateFormat.string(from: requestedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                    cell.statusNameLabel.textColor = UIColor.darkGray
                    
                } else {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.red
                    cell.timeLabel.text = dateFormat.string(from: requestedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                    cell.statusNameLabel.textColor = UIColor.red
                    
                }
                
                break
                
            case 3://Arrived
                
                cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistArrived
                
                let arrivedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.arrivedDate))
                
                if bookingStatus < Booking_Status.Arrived.rawValue {
                    
                    cell.timeLabel.isHidden = true
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                    cell.statusNameLabel.textColor = UIColor.lightGray
                    
                } else if bookingStatus == Booking_Status.Arrived.rawValue {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.darkGray
                    cell.timeLabel.text = dateFormat.string(from: arrivedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                    cell.statusNameLabel.textColor = UIColor.darkGray
                    
                } else {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.red
                    cell.timeLabel.text = dateFormat.string(from: arrivedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                    cell.statusNameLabel.textColor = UIColor.red
                    
                }
                
                
                break
                
            case 4://Started
                
                cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.EventStarted
                
                
                let startedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.startedDate))
                
                if bookingStatus < Booking_Status.Started.rawValue {
                    
                    cell.timeLabel.isHidden = true
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                    cell.statusNameLabel.textColor = UIColor.lightGray
                    
                    
                } else if bookingStatus == Booking_Status.Started.rawValue {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.darkGray
                    cell.timeLabel.text = dateFormat.string(from: startedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                    cell.statusNameLabel.textColor = UIColor.darkGray
                    
                    
                } else {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.red
                    cell.timeLabel.text = dateFormat.string(from: startedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                    cell.statusNameLabel.textColor = UIColor.red
                    
                }
                
                
                break
                
            case 5://Completed
                
                cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.EventCompleted
                
                let completedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.completedDate))
                
                if bookingStatus < Booking_Status.Completed.rawValue {
                    
                    cell.timeLabel.isHidden = true
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                    cell.statusNameLabel.textColor = UIColor.lightGray
                    
                } else if bookingStatus == Booking_Status.Completed.rawValue {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.darkGray
                    cell.timeLabel.text = dateFormat.string(from: completedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                    cell.statusNameLabel.textColor = UIColor.darkGray
                    
                } else {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.red
                    cell.timeLabel.text = dateFormat.string(from: completedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                    cell.statusNameLabel.textColor = UIColor.red
                    
                }
                
                break
                
            case 6://Invoice Raised
                
                cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.InvoiceRaised
                
                let invoiceRaisedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.invoiceRaisedDate))
                
                if bookingStatus < Booking_Status.Raiseinvoice.rawValue {
                    
                    cell.timeLabel.isHidden = true
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                    cell.statusNameLabel.textColor = UIColor.lightGray
                    
                } else if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                    
                    //                            cell.timeLabel.isHidden = false
                    //                            cell.timeLabel.textColor = UIColor.darkGray
                    //                            cell.timeLabel.text = dateFormat.string(from: invoiceRaisedDate)
                    //                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                    //                            cell.statusNameLabel.textColor = UIColor.darkGray
                    //
                    //                        } else {
                    
                    cell.timeLabel.isHidden = false
                    cell.timeLabel.textColor = UIColor.red
                    cell.timeLabel.text = dateFormat.string(from: invoiceRaisedDate)
                    cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                    cell.statusNameLabel.textColor = UIColor.red
                    
                }
                
                break
                
            default:
                break
                
            }
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
            
        case 1://Accepted
            
            if bookingStatus >= Booking_Status.Accepted.rawValue {
                
                return 150
                
            } else {
                
                return 70
            }
            
        case 2://Ontheway
            
            if bookingStatus == Booking_Status.Ontheway.rawValue {
                
                return 120
                
            } else {
                
                return 70
            }
            
        default:
            
            return 70
            
        }
        
    }
    
    func scrollTableViewToShowCusrrentBookingStatus() {
        
        var indexPath = IndexPath.init(row: 0, section: 0)
        
        switch bookingStatus {
            
        case Booking_Status.Accepted.rawValue:
            
            indexPath.row = 1
            break
            
            
        case Booking_Status.Ontheway.rawValue:
            
            indexPath.row = 2
            break
            
            
        case Booking_Status.Arrived.rawValue:
            
            indexPath.row = 3
            break
            
            
        case Booking_Status.Started.rawValue:
            
            indexPath.row = 4
            break
            
            
        case Booking_Status.Completed.rawValue:
            
            indexPath.row = 5
            break
            
            
        case Booking_Status.Raiseinvoice.rawValue:
            
            indexPath.row = 6
            break
            
            
        default:
            break
        }
        
        self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.middle, animated: true)
        
    }
    
}

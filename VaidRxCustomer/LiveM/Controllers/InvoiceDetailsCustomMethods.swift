//
//  InvoiceDetailsCustomMethods.swift
//  LiveM
//
//  Created by Rahul Sharma on 06/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension InvoiceDetailsPopUpScreen {
    
    
    /// Method to show Invoice details
    func showInvoiceDetails() {
        
//        gigNameLabel.text = String(format:"%@ %@ / Gig", GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["name"]), GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["unit"]))
        
         gigNameLabel.text = String(format:"%@ %@", GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["name"]), GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["unit"]))
        
        gigValueLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol ,Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))!)
        
        serviceFeeValueLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol,bookingDetailModel.serviceFee)
        
        discountValueLabel.text = String(format:"%@ %.2f",bookingDetailModel.currencySymbol, bookingDetailModel.discountValue)
        
        eventIdLabel.text = "Event Id: \(bookingDetailModel.bookingId)"
        
        
        let totalValue =  (Double(GenericUtility.strForObj(object:bookingDetailModel.gigTimeDict["price"]))! + bookingDetailModel.serviceFee) - bookingDetailModel.discountValue
        
        totalValueLabel.text = bookingDetailModel.currencySymbol + String(format:" %.2f",totalValue)
        
        if bookingDetailModel.signatureURL.length > 0 {
            
            activityIndicator.startAnimating()
            
            signatureImageView.kf.setImage(with: URL(string: bookingDetailModel.signatureURL),
                                          placeholder:nil,
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: nil)
            
        } else {
            
            signatureImageView.image = nil
        }

        
    }
    
}
